/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Local
#include "RichCalibUtils.h"

using namespace Rich::Calib::Utils;

//=============================================================================

StatusCode CalibAlg::initialize() {
  return Rich::Future::Rec::HistoAlgBase::initialize().andThen( [&] {
    // Can we write to the configured paths ?
    // note as can change path for logs, needs to be first.
    if ( !createDir( m_summaryPath.value(), false ) ) {
      const auto old_path = m_summaryPath.value();
      const auto curPath  = boost::filesystem::current_path().string();
      m_summaryPath       = curPath + "/summaries";
      calib_message( MSG::WARNING, "Cannot write to '", old_path, "' -> Resetting to '", m_summaryPath.value(),
                     "/summaries'" );
    }

    divider();
    calib_message( MSG::INFO, "Initializing" );

    // Partition
    const char* partition = getenv( "PARTITION" );
    if ( partition ) {
      const std::string part{ partition };
      calib_message( MSG::INFO, "Environment variable PARTITION = '", part, "'" );
      m_okToPublish =
          m_okToPublish.value() && ( part != "RICH" && part != "RICH1" && part != "RICH2" && part != "FEST" );
    } else {
      calib_message( MSG::WARNING, "Environment variable PARTITION is not set" );
    }
    if ( !m_okToPublish ) {
      calib_message( MSG::WARNING, "Calibration writing and publishing is DISABLED" );
    } else {
      calib_message( MSG::INFO, "Calibration writing and publishing is ENABLED" );
    }

    // ROOT style for PDFs
    if ( m_createPDFsummary ) { setStyle(); }

    if ( m_enableRunByRunHists ) { calib_message( MSG::INFO, "Run-by-run histograms are enabled" ); }

    calib_message( MSG::INFO, "Writing summary data to ", m_summaryPath.value() );

    calib_message( MSG::VERBOSE, "Task output level is ", msgLevel() );

    resetCalibCounters();
  } );
}

//=============================================================================

std::unique_ptr<TH1D> CalibAlg::makePullPlot( TH1& hist, TF1& func ) const {
  // make the new plot with the same bins and range
  std::ostringstream id;
  id << hist.GetName() << "Pull";
  auto h = std::make_unique<TH1D>( id.str().c_str(), "", //
                                   hist.GetNbinsX(), hist.GetXaxis()->GetXmin(), hist.GetXaxis()->GetXmax() );
  h->GetXaxis()->SetTitle( "" );
  h->GetYaxis()->SetTitle( "Pull" );
  h->GetXaxis()->SetTitleSize( 0.08 );
  h->GetYaxis()->SetTitleSize( 0.08 );
  h->GetXaxis()->SetLabelSize( 0.06 );
  h->GetYaxis()->SetLabelSize( 0.06 );
  h->GetYaxis()->SetTitleOffset( 0.3 );
  h->SetStats( false );

  // set the bins. Note numbering starts at 1 for bins, not 0 :(
  for ( int i = 1; i <= hist.GetNbinsX(); ++i ) {
    // bin center
    const auto binCen = hist.GetBinCenter( i );
    // only make pull entries for points in function range
    if ( binCen < func.GetXmax() && binCen > func.GetXmin() ) {
      // raw bin content
      const auto rawCont = hist.GetBinContent( i );
      // bin error
      const auto rawErr = fabs( hist.GetBinError( i ) );
      // function value
      const auto funcVal = func.Eval( binCen );
      // pull
      const auto pull = ( rawErr > 0 ? ( rawCont - funcVal ) / rawErr : 0.0 );
      // set the bin content and error in the pull plots
      h->SetBinContent( i, pull );
      h->SetBinError( i, 1.0 ); // Correct value if you neglect error on fitted function
    }
  }

  // return
  return h;
}

//=============================================================================

bool CalibAlg::writeToLoggerFile( const MSG::Level level, const std::string& mess ) const {
  // Open the summary file in append mode
  std::ofstream file( loggerFile().c_str(), std::ios_base::app );
  const bool    OK = file.is_open();
  if ( OK ) {
    file << getDateTimeString( "%Y-%m-%d %H:%M:%S" ) << " | ";
    if ( MSG::INFO == level ) {
      file << "INFO  | ";
    } else if ( MSG::WARNING == level ) {
      file << "WARN  | ";
    } else if ( MSG::ERROR == level ) {
      file << "ERROR | ";
    } else if ( MSG::FATAL == level ) {
      file << "FATAL | ";
    } else if ( MSG::DEBUG == level ) {
      file << "DEBUG | ";
    } else if ( MSG::VERBOSE == level ) {
      file << "VERBO | ";
    } else if ( MSG::ALWAYS == level ) {
      file << "ALWYS | ";
    }
    file << mess << std::endl;
    file.close();
  } else {
    // do not convert to calib_message( MSG::WARNING
    warning() << "Problem writing logger summary file " << loggerFile() << endmsg;
  }
  return OK;
}

//=============================================================================

void CalibAlg::printCanvas( const std::string& tag ) const {

  if ( !m_createPDFsummary ) { return; }

  const std::string imageType = "pdf";

  // get the PDF file name for this run
  auto& pdfFile = m_pdfFile[runNumber()];

  // Opening file ?
  if ( "[" == tag ) {

    // If file name is not empty (so repeat fit for a run), delete file
    if ( !pdfFile.empty() ) { removeFile( pdfFile ); }

    // Directory to save summaries to
    const boost::filesystem::path dir( summaryPath() );

    // Create directory if it does not exist
    const bool ok = createDir( dir );
    if ( ok ) {

      // File name
      const boost::filesystem::path filename( "Run-" + std::to_string( runNumber() ) + "." + imageType );
      const boost::filesystem::path full_path = dir / filename;

      // If PDF exists, remove before remaking
      removeFile( full_path );

      // cache image file name for this run
      pdfFile = full_path.string();

      // Make a new canvas
      deleteCanvas();

      // Open new PDF
      canvas()->Print( ( pdfFile + tag ).c_str(), imageType.c_str() );

    } else {
      calib_message( MSG::ERROR, "Failed to create PDF file ", pdfFile );
      deleteCanvas();
      pdfFile = "";
    }

  }
  // Closing the file ?
  else if ( "]" == tag ) {

    // Close the file
    canvas()->Print( ( pdfFile + tag ).c_str(), imageType.c_str() );

    // Set group write permissions
    // setPerms( pdfFile );

    // send a message
    calib_message( MSG::INFO, "Created  ", pdfFile );

    // delete canvas
    deleteCanvas();

    // limit the PDF file name map to N entries
    while ( m_pdfFile.size() > m_runNumHistSize + 10 ) { m_pdfFile.erase( m_pdfFile.begin() ); }

  } else if ( "" == tag ) {

    // Add time/date watermark
    canvas()->cd();
    TText text;
    text.SetNDC();
    text.SetTextSize( 0.012 );
    text.SetTextColor( 13 );
    text.DrawText( 0.01, 0.01, watermark().c_str() );

    // Just print
    canvas()->Print( pdfFile.c_str(), imageType.c_str() );

  } else if ( "DELETE" == tag ) {
    if ( !pdfFile.empty() ) { removeFile( pdfFile ); }
  }
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ServiceLocatorHelper.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Local
#include "RichCalibUtils.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "Event/ODIN.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

namespace Rich::Future::Rec::Counting {

  // Use the functional framework
  using namespace Gaudi::Functional;
  // Calib Utils
  using namespace Rich::Calib::Utils;
  using namespace Rich::Utils;

  namespace {
    using PhotonYield = ValueWithError<double>;
  } // namespace

  /** @class PhotonCounting RichPhotonCounting.h
   *
   *  Runs the RICH photon counting directly from photon objects
   *
   *  @author Kang Yang
   *  @date   2024-03-15
   */

  class PhotonCounting final : public Consumer<void( const LHCb::ODIN&,                         //
                                                     const LHCb::Track::Selection&,             //
                                                     const Summary::Track::Vector&,             //
                                                     const Relations::PhotonToParents::Vector&, //
                                                     const LHCb::RichTrackSegment::Vector&,     //
                                                     const CherenkovAngles::Vector&,            //
                                                     const SIMDCherenkovPhoton::Vector& ),
                                               LHCb::DetDesc::usesBaseAndConditions<CalibAlg>> {

  public:
    /// Standard constructor
    PhotonCounting( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                      KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default } } ) {
      setProperty( "NBins1DHistos", 150 ).ignore();
      setProperty( "NBins2DHistos", 100 ).ignore();
      m_taskType   = "PhotonCounting";
      m_minEntries = 100;
    }

  public:
    /// Functional operator
    void operator()( const LHCb::ODIN&                         odin,          //
                     const LHCb::Track::Selection&             tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons ) const override;

    /// Initialization
    StatusCode initialize() override {
      // for debug for testing
      // setProperty( "OutputLevel", MSG::DEBUG ).ignore();
      return Consumer::initialize().andThen( [&] {
        // background model info
        if ( !m_enableQuadraticBckModel ) {
          calib_message( MSG::INFO, "Linear background model is ENABLED" );
        } else {
          calib_message( MSG::INFO, "Quadratic background model is ENABLED" );
        }
        // write to summary file
        calib_message( MSG::INFO, "Initialized" );
      } );
    }

    /// Print run summaries
    void runSummaries( const MSG::Level level = MSG::INFO ) const {
      if ( msgLevel( level ) && !m_processedRuns.empty() ) {
        calib_message( level, "+-----------+-------------+-------------+" );
        calib_message( level, "|    Run    | Used Events | Late Events |" );
        calib_message( level, "+-----------+-------------+-------------+" );
        for ( const auto& [run, stats] : m_processedRuns ) {
          calib_message( level, boost::format( "| %8u  |  %9u  |  %9u  |" ) //
                                    % run % stats.used % stats.late );
        }
        calib_message( level, "+-----------+-------------+-------------+" );
      }
    }

    /// Start
    StatusCode start() override {
      divider();
      calib_message( MSG::INFO, "Starting" );
      return Consumer::start();
    }

    /// Stop
    StatusCode stop() override {
      divider();
      calib_message( MSG::INFO, "Stopping" );
      // Do not run counting in stop() during QMT tests. Defer final one
      // to finalize() which is then included in the test ref comparisons.
      if ( m_allowStopCounting ) {
        // use periodic here incase task starts up again with the same run
        runPhotonCounting( "Stop" );
      }
      calib_message( MSG::INFO, "Stopped" );
      return Consumer::stop();
    }

    /// Finalize
    StatusCode finalize() override {
      divider();
      calib_message( MSG::INFO, "Finalizing" );
      // In principle counting not needed here, as I do not think its
      // possible to be here without having stop() called first, but just
      // to be sure try again. At minimum it will trigger the resets.
      // Also needed for when running in a QMT test as there stop() countings are not run.
      runPhotonCounting();
      runSummaries();
      // cleanup
      deleteCanvas();
      // write to summary file
      calib_message( MSG::INFO, "Finalized" );
      return Consumer::finalize();
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      // Loop over radiators
      // Loop over radiators
      auto setLabels = [&]( const auto& h ) {
        if ( h.get() && h->GetXaxis() ) { h->GetXaxis()->SetTitle( "# Cherenkov Photons" ); }
        if ( h.get() && h->GetYaxis() ) { h->GetYaxis()->SetTitle( "Entries" ); }
      };
      for ( const auto rich : activeDetectors() ) {
        const auto rad = radType( rich );
        // book histogram for counting
        // Not using Gaudi mechanisms here as we do not want this to be changed by UpdateAndReset
        h_ckPhotonYield[rich] = std::make_unique<TH1D>( ( "ckPhotonYield" + Rich::text( rad ) ).c_str(), //
                                                        ( Rich::text( rad ) + " Photon Yield" ).c_str(), //
                                                        40, -100, 200 );
        // work around Root's stupid ownership. Without this, if ever someone has open a root file
        // somewhere, it will own the histogram (yes !)
        h_ckPhotonYield[rich]->SetDirectory( nullptr );
        setLabels( h_ckPhotonYield[rich] );
        ok &= ( h_ckPhotonYield[rich].get() != nullptr );
      } // active rad loop
      return StatusCode{ ok };
    }

  private:
    /// Reset histograms following running a counting
    void resetHistograms() const {
      calib_message( MSG::VERBOSE, "Resetting histograms" );
      // reset run event count
      m_nEventsThisRun = 0;
      // reset histograms
      for ( const auto rich : activeDetectors() ) {
        if ( h_ckPhotonYield[rich].get() ) { h_ckPhotonYield[rich]->Reset(); }
      }
    }

    /// Initialise extra histos
    bool initExtraHists( const unsigned int RunNumber = 0 ) const {
      bool ok = true;
      if ( !m_lastRunExtraHists.has_value() || RunNumber != m_lastRunExtraHists.value() ) {
        m_lastRunExtraHists = RunNumber;
        calib_message( MSG::DEBUG, "Resetting run-by-run histograms for run ", RunNumber );
        const auto runT  = ( RunNumber > 0 ? " | Run " + std::to_string( RunNumber ) : "" );
        const auto runID = ( RunNumber > 0 ? "Runs/" + std::to_string( RunNumber ) + "/" : "" );
        for ( const auto rich : activeDetectors() ) {
          const auto rad = radType( rich );
          ok &= initHist( h_ckThetaRec_run[rich].emplace_back(), HID( runID + "ckThetaRec", rad ), "Rec CKTheta" + runT,
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(), "Cherenkov theta / rad" );
          ok &= initHist( h_ckThetaExp_run[rich].emplace_back(), HID( runID + "ckThetaExp", rad ), "Exp CKTheta" + runT,
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(), "Cherenkov theta / rad" );
          ok &= initHist( h_ckThetaRes_run[rich].emplace_back(), HID( runID + "ckThetaRes", rad ), //
                          "Rec-Exp CKTheta" + runT,                                                //
                          -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),                      //
                          "delta(Cherenkov theta) / rad" );
          ok &= initHist( h_tkPtot_run[rich].emplace_back(), HID( runID + "tkPtot", rad ), "Track Momentum" + runT,
                          m_minP[rich], std::min( m_maxP[rich], 120 * Gaudi::Units::GeV ), nBins1D(),
                          "Track Momentum / MeV" );
          for ( const auto q : quadrants() ) {
            ok &= initHist( h_ckThetaRes_quadrants_run[rich][q].emplace_back(),
                            HID( runID + "ckThetaResQuad" + std::to_string( q ), rad ),
                            "Rec-Exp CKTheta" + runT + quadString( q ), -m_ckResRange[rich], m_ckResRange[rich],
                            nBins1D(), "delta(Cherenkov theta) / rad" );
          }
          // track (x,y) plots
          const double xSize = ( Rich::Rich1Gas == rad ? 500 : 2000 );
          const double ySize = ( Rich::Rich1Gas == rad ? 500 : 2000 );
          ok &= initHist( h_tkEntryXY_run[rich].emplace_back(), HID( runID + "trackEntryXY", rad ), //
                          "Track Radiator Entry (x,y)" + runT,                                      //
                          -xSize, xSize, nBins2D(),                                                 //
                          -ySize, ySize, nBins2D(),                                                 //
                          "Track Entry X / mm", "TrackEntry Y / mm" );
          ok &= initHist( h_tkSegMomVSPhotonYield_run[rich].emplace_back(),
                          HID( runID + "trackSegMomVSPhotonYield", rad ), "Track Seg Momentum Photon Yield" + runT,
                          -100, 200, 40, 5000, 50000, 40, "Detected Cherenkov Photons", "Segment Momentum / MeV" );
          ok &= initHist( h_tkpathLengthVSPhotonYield_run[rich].emplace_back(),
                          HID( runID + "trackpathLengthVSPhotonYield", rad ), "Track Path Length Photon Yield" + runT,
                          -100, 200, 40, 1000, 1250, 40, "Detected Cherenkov Photons", "Track Path Length / mm" );

          ok &= initHist( h_ckPhotonSig_run[rich].emplace_back(), HID( runID + "ckPhotonSigYield", rad ),
                          "Detected CKPhoton Yield" + runT, -100, 200, 40, "Detected Cherenkov Photons" );
          ok &= initHist( h_ckPhotonSigNoSub_run[rich].emplace_back(), HID( runID + "ckPhotonSigNoSub", rad ),
                          "Detected CKPhoton SigNoSub" + runT, -100, 400, 40, "Detected Cherenkov Photons" );
          ok &= initHist( h_ckPhotonBckLow_run[rich].emplace_back(), HID( runID + "ckPhotonBckLow", rad ),
                          "Detected CKPhoton BckLow" + runT, -100, 200, 40, "Detected Cherenkov Photons" );
          ok &= initHist( h_ckPhotonBckHigh_run[rich].emplace_back(), HID( runID + "ckPhotonBckHigh", rad ),
                          "Detected CKPhoton BckHigh" + runT, -100, 200, 40, "Detected Cherenkov Photons" );

          for ( const auto q : quadrants() ) {
            ok &= initHist( h_ckPhotonSig_quadrants_run[rich][q].emplace_back(),
                            HID( runID + "ckPhotonSigYieldQuad" + std::to_string( q ), rad ),
                            "Detected CKPhoton Yield" + runT + quadString( q ), //
                            -100, 200, 40, "Detected Cherenkov Photons" );
            ok &= initHist( h_ckPhotonSigNoSub_quadrants_run[rich][q].emplace_back(),
                            HID( runID + "ckPhotonSigNoSubQuad" + std::to_string( q ), rad ),
                            "Detected CKPhoton SigNoSub" + runT + quadString( q ), //
                            -100, 400, 40, "Detected Cherenkov Photons" );
            ok &= initHist( h_ckPhotonBckLow_quadrants_run[rich][q].emplace_back(),
                            HID( runID + "ckPhotonBckLowQuad" + std::to_string( q ), rad ),
                            "Detected CKPhoton BckLow" + runT + quadString( q ), //
                            -100, 200, 40, "Detected Cherenkov Photons" );
            ok &= initHist( h_ckPhotonBckHigh_quadrants_run[rich][q].emplace_back(),
                            HID( runID + "ckPhotonBckHighQuad" + std::to_string( q ), rad ),
                            "Detected CKPhoton BckHigh" + runT + quadString( q ), //
                            -100, 200, 40, "Detected Cherenkov Photons" );
          }
        }
      }
      return ok;
    }

    /// Run the photon counting ...
    void runPhotonCounting( const std::string& type = "Final" ) const;

    /// Write summary logs
    bool writeLogs( const std::string& rad, const PhotonYield& ckPhotonYield ) const;

    // Quadratic background calulation
    double GetQuadraticYield( const Rich::DetectorType rich, const double bkgNQuadB1, const double bkgNQuadB2,
                              const double bkgNQuadB3, const double bkgNQuadSig ) const;

    double EvalIntegralLimit( const double& x2, const double& x1, const int& p ) const;

  private:
    // properties

    /// The histogram ID to use
    Gaudi::Property<std::string> m_ResPlot{ this, "ResPlot", "ckResAll" };

    /// Allow counting during stop()
    Gaudi::Property<bool> m_allowStopCounting{ this, "AllowCountingAtStop", true };

    /// Min VP nHits cut
    Gaudi::Property<DetectorArray<double>> m_minvphits{ this, "MinVPHits", { 8.0, 8.0 }, "Minimum VP nHits" };

    /// Background model
    Gaudi::Property<bool> m_enableQuadraticBckModel{ this, "QuadraticBckModel", true };

  private:
    // cached data

    // Define the region boundary
    const DetectorArray<double> m_bkgWindow1Low  = { -0.008, -0.0035 };
    const DetectorArray<double> m_bkgWindow1High = { -0.003, -0.002 };
    const DetectorArray<double> m_bkgWindow2Low  = { 0.0035, 0.002 };
    const DetectorArray<double> m_bkgWindow2High = { 0.008, 0.0035 };

    // Define the region boundary - quadratic
    const DetectorArray<double> m_bkgQuad1Low  = { -0.008, -0.0035 };
    const DetectorArray<double> m_bkgQuad1Mid  = { -0.005, -0.0025 };
    const DetectorArray<double> m_bkgQuad1High = { -0.003, -0.002 };
    const DetectorArray<double> m_bkgQuad2Low  = { 0.0035, 0.002 };
    const DetectorArray<double> m_bkgQuad2High = { 0.008, 0.0035 };

    /// Lock for main event processing
    mutable std::mutex m_mutex;

    // CK theta histograms for photon counting
    Hist::DetArray<std::unique_ptr<TH1D>> h_ckPhotonYield = { {} };

    // Current active run-by-run histograms
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_ckThetaRec_run            = { {} };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_ckThetaExp_run            = { {} };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_ckThetaRes_run            = { {} };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_tkPtot_run                = { {} };
    mutable Hist::DetArray<Hist::QuadArray<RunByRunHists<Hist::H1D<>>>> h_ckThetaRes_quadrants_run  = { { {} } };
    mutable Hist::DetArray<RunByRunHists<Hist::H2D<>>>                  h_tkEntryXY_run             = { {} };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_ckPhotonSig_run           = { {} };
    mutable Hist::DetArray<Hist::QuadArray<RunByRunHists<Hist::H1D<>>>> h_ckPhotonSig_quadrants_run = { { {} } };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<unsigned int>>>      h_ckPhotonSigNoSub_run      = { {} };
    mutable Hist::DetArray<Hist::QuadArray<RunByRunHists<Hist::H1D<unsigned int>>>> h_ckPhotonSigNoSub_quadrants_run = {
        { {} } };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<unsigned int>>>                  h_ckPhotonBckLow_run = { {} };
    mutable Hist::DetArray<Hist::QuadArray<RunByRunHists<Hist::H1D<unsigned int>>>> h_ckPhotonBckLow_quadrants_run = {
        { {} } };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<unsigned int>>>                  h_ckPhotonBckHigh_run = { {} };
    mutable Hist::DetArray<Hist::QuadArray<RunByRunHists<Hist::H1D<unsigned int>>>> h_ckPhotonBckHigh_quadrants_run = {
        { {} } };
    mutable Hist::DetArray<RunByRunHists<Hist::H2D<>>> h_tkSegMomVSPhotonYield_run     = { {} };
    mutable Hist::DetArray<RunByRunHists<Hist::H2D<>>> h_tkpathLengthVSPhotonYield_run = { {} };
  };

} // namespace Rich::Future::Rec::Counting

using namespace Rich::Future::Rec::Counting;

//-----------------------------------------------------------------------------

void PhotonCounting::operator()( const LHCb::ODIN&                         odin,          //
                                 const LHCb::Track::Selection&             tracks,        //
                                 const Summary::Track::Vector&             sumTracks,     //
                                 const Relations::PhotonToParents::Vector& photToSegPix,  //
                                 const LHCb::RichTrackSegment::Vector&     segments,      //
                                 const CherenkovAngles::Vector&            expTkCKThetas, //
                                 const SIMDCherenkovPhoton::Vector&        photons ) const {

  // Only one thread can be running from this point on
  std::scoped_lock lock{ m_mutex };

  // First event printout
  if ( 0 == m_nEvts ) {
    calib_message( MSG::INFO, "First Event Seen" );
    // set last time to now.
    m_timeLastCalib = time( nullptr );
  }

  // Current run number
  const auto RunNumber = odin.runNumber();

  // Send info on new runs
  if ( RunNumber != runNumber() ) {
    // check to see if this is one we have seen in the past.
    // protects against 'flip flopping' run numbers online that can be
    // seen at run changes.
    auto runStats = m_processedRuns.find( RunNumber );
    if ( runStats == m_processedRuns.end() ) {
      calib_message( MSG::INFO, "==================================================> New Run ", RunNumber,
                     " <==================================================" );
    } else {
      // Have we gone back in time... ?
      if ( RunNumber < runNumber() ) {
        ++runStats->second.late;
        return;
      }
    }
    // before updating cached run number, check if new run-by-run histograms are required
    if ( m_enableRunByRunHists && RunNumber != runNumber() ) { initExtraHists( RunNumber ); }
  }

  // Check to see if a counting should be run due to a new run being detected
  if ( runNumber() != 0 && runNumber() != RunNumber ) { runPhotonCounting(); }

  // update cached run number
  m_runNumber = RunNumber;
  // count events
  ++m_nEvts;

  // count events since last counting was made
  ++m_nEventsSinceCalib;

  // count events this run
  ++m_nEventsThisRun;

  // =================================================================================
  // Fill the histograms
  // =================================================================================

  // The PID type to assume. Just use Pion here.
  const auto pid = Rich::Pion;

  // loop over tracks
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {

    // Photon level parameters
    // background subtraction estimation regions
    DetectorArray<unsigned int> bkgNLinB1  = { 0, 0 };
    DetectorArray<unsigned int> bkgNLinB2  = { 0, 0 };
    DetectorArray<unsigned int> bkgNLinSig = { 0, 0 };
    // background subtraction estimation regions - quadratic
    DetectorArray<double> bkgNQuadB1  = { 0, 0 };
    DetectorArray<double> bkgNQuadB2  = { 0, 0 };
    DetectorArray<double> bkgNQuadB3  = { 0, 0 };
    DetectorArray<double> bkgNQuadSig = { 0, 0 };
    DetectorArray<bool>   valid_entry = { false, false };

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // RICH
      const auto rich = seg.rich();
      if ( !richIsActive( rich ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );

      // selection cuts
      if ( beta < m_minBeta[rich] || beta > m_maxBeta[rich] ) { continue; }

      // Remove clones
      if ( tk->nVPHits() < m_minvphits[rich] ) { continue; }

      // expected CK theta in SIMD form
      const SIMDCherenkovPhoton::SIMDFP thetaExp( expCKangles[pid] );

      // SIMD delta theta
      const auto deltaTheta = phot.CherenkovTheta() - thetaExp;

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries and fill
        if ( phot.validityMask()[i] ) {
          if ( fabs( deltaTheta[i] ) > 0.01 ) { continue; }
          // Counting photons in bins for linear bkg subtraction
          if ( deltaTheta[i] >= m_bkgWindow1Low[rich] && deltaTheta[i] < m_bkgWindow1High[rich] ) {
            ++bkgNLinB1[rich];
          } else if ( deltaTheta[i] >= m_bkgWindow2Low[rich] && deltaTheta[i] < m_bkgWindow2High[rich] ) {
            ++bkgNLinB2[rich];
          } else if ( deltaTheta[i] >= m_bkgWindow1High[rich] && deltaTheta[i] < m_bkgWindow2Low[rich] ) {
            ++bkgNLinSig[rich];
          }
          // Counting photons in bins for quadratic bkg subtraction
          if ( deltaTheta[i] >= m_bkgQuad1Low[rich] && deltaTheta[i] < m_bkgQuad1Mid[rich] ) {
            ++bkgNQuadB1[rich];
          } else if ( deltaTheta[i] >= m_bkgQuad1Mid[rich] && deltaTheta[i] < m_bkgQuad1High[rich] ) {
            ++bkgNQuadB2[rich];
          } else if ( deltaTheta[i] >= m_bkgQuad2Low[rich] && deltaTheta[i] < m_bkgQuad2High[rich] ) {
            ++bkgNQuadB3[rich];
          } else if ( deltaTheta[i] >= m_bkgQuad1High[rich] && deltaTheta[i] < m_bkgQuad2Low[rich] ) {
            ++bkgNQuadSig[rich];
          }
          valid_entry[rich] = true;
        }
      }

      // detailed run by run plots
      if ( m_enableRunByRunHists ) {
        // Segment (x,y) entry point to radiator
        const auto seg_x = seg.entryPoint().x();
        const auto seg_y = seg.entryPoint().y();
        const auto q     = quadrant( seg_x, seg_y );
        fillRunByRunHist( h_tkEntryXY_run[rich], seg_x, seg_y );
        // Loop over scalar entries in SIMD photon
        for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
          // Select valid entries and fill
          if ( phot.validityMask()[i] ) {
            fillRunByRunHist( h_ckThetaRec_run[rich], phot.CherenkovTheta()[i] );
            fillRunByRunHist( h_ckThetaExp_run[rich], thetaExp[i] );
            fillRunByRunHist( h_ckThetaRes_run[rich], deltaTheta[i] );
            fillRunByRunHist( h_ckThetaRes_quadrants_run[rich][q], deltaTheta[i] );
          }
        }
      }

    } // photons

    for ( const auto& seg : segments ) {

      // RICH and radiator
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) { continue; }
      const auto rich = ( Rich::Rich1Gas == rad ? Rich::Rich1 : Rich::Rich2 );

      // Check if valid
      if ( valid_entry[rich] ) {

        // Segment (x,y) entry point to radiator
        const auto seg_x = seg.entryPoint().x();
        const auto seg_y = seg.entryPoint().y();
        // Segment momentum
        const auto pTot    = seg.bestMomentumMag();
        const auto pathLen = seg.pathLength();

        // Calculations for linear background subtractions
        const auto x1 = m_bkgWindow1High[rich] - m_bkgWindow1Low[rich];
        const auto x2 = m_bkgWindow2Low[rich] - m_bkgWindow1High[rich];
        const auto x3 = m_bkgWindow2High[rich] - m_bkgWindow2Low[rich];

        const auto cBl = ( ( x2 + x3 ) / ( x1 + 2 * x2 + x3 ) ) * ( ( x2 ) / ( x1 ) );
        const auto cBh = ( ( x1 + x2 ) / ( x1 + 2 * x2 + x3 ) ) * ( ( x2 ) / ( x3 ) );

        const auto bkgSubN = bkgNLinSig[rich] - ( bkgNLinB1[rich] * cBl + bkgNLinB2[rich] * cBh );

        // Calculations for quadratic background subtractions
        const auto bkgSubN_Q =
            GetQuadraticYield( rich, bkgNQuadB1[rich], bkgNQuadB2[rich], bkgNQuadB3[rich], bkgNQuadSig[rich] );

        // Fill photon yield according to used background model
        h_ckPhotonYield[rich]->Fill( m_enableQuadraticBckModel ? bkgSubN_Q : bkgSubN );

        if ( m_enableRunByRunHists ) {
          fillRunByRunHist( h_ckPhotonSig_run[rich], bkgSubN );
          fillRunByRunHist( h_ckPhotonSigNoSub_run[rich], bkgNLinSig[rich] );
          fillRunByRunHist( h_ckPhotonBckLow_run[rich], bkgNLinB1[rich] );
          fillRunByRunHist( h_ckPhotonBckHigh_run[rich], bkgNLinB2[rich] );
          fillRunByRunHist( h_tkSegMomVSPhotonYield_run[rich], bkgSubN, pTot );
          fillRunByRunHist( h_tkpathLengthVSPhotonYield_run[rich], bkgSubN, pathLen );
          // Quad 1-4
          const auto q = ( seg_x > 0 ? ( seg_y > 0 ? Quadrant::Q0 : Quadrant::Q2 ) //
                                     : ( seg_y > 0 ? Quadrant::Q1 : Quadrant::Q3 ) );
          fillRunByRunHist( h_ckPhotonSig_quadrants_run[rich][q], bkgSubN );
          fillRunByRunHist( h_ckPhotonSigNoSub_quadrants_run[rich][q], bkgNLinSig[rich] );
          fillRunByRunHist( h_ckPhotonBckLow_quadrants_run[rich][q], bkgNLinB1[rich] );
          fillRunByRunHist( h_ckPhotonBckHigh_quadrants_run[rich][q], bkgNLinB2[rich] );
        }
      }
    }
  } // tracks

  // detailed run by run plots
  if ( m_enableRunByRunHists ) {
    for ( const auto& seg : segments ) {
      // RICH info
      const auto rich = seg.rich();
      if ( !richIsActive( rich ) ) { continue; }
      // Segment momentum
      const auto pTot = seg.bestMomentumMag();
      // beta
      const auto beta = richPartProps()->beta( pTot, pid );
      if ( beta < m_minBeta[rich] || beta > m_maxBeta[rich] ) { continue; }
      fillRunByRunHist( h_tkPtot_run[rich], pTot );
    }
  }
}

void PhotonCounting::runPhotonCounting( const std::string& type ) const {

  // check run number is reasonable
  if ( 0 == runNumber() ) {
    // if we have also seen events, something is wrong...
    if ( m_nEventsSinceCalib > 0 ) {
      calib_message( MSG::WARNING, "Undefined run number !! -> ", type, " counting aborted" );
    }
    resetHistograms();
    resetCalibCounters();
    return;
  }

  // Only do anything if some events have been seen since last time.
  if ( 0 == m_nEventsSinceCalib ) {
    calib_message( MSG::WARNING, "No events processed for run ", runNumber(), " -> ", type, " counting aborted" );
    resetHistograms();
    resetCalibCounters();
    return;
  }

  // Is this a previously counted run
  const auto iRunCalData = m_processedRuns.find( runNumber() );
  if ( iRunCalData != m_processedRuns.end() ) {
    // Do we now have more events to count with than last time ?
    if ( iRunCalData->second.used >= m_nEventsThisRun ) {
      calib_message( MSG::DEBUG, "Already counted run ", runNumber(), " -> Processing skipped" );
      resetCalibCounters();
      return;
    } else {
      calib_message( MSG::INFO, "Run ", runNumber(), " : Previous #Events = ", iRunCalData->second.used,
                     " New #Events = ", m_nEventsThisRun );
    }
  }

  // Print message on the number of events seen and the rate
  // current time
  m_timeCurrentCalibStart = time( nullptr );
  // time since last counting
  const time_t deltaT = ( m_timeCurrentCalibStart - m_timeLastCalib );
  if ( deltaT > 0 ) {
    struct tm*         timeinfo = localtime( &deltaT );
    std::ostringstream messageS;
    messageS << "Seen " << m_nEventsSinceCalib << " events in past ";
    if ( timeinfo->tm_hour - 1 > 0 ) { messageS << timeinfo->tm_hour - 1 << " hours "; }
    if ( timeinfo->tm_min > 0 ) { messageS << timeinfo->tm_min << " mins "; }
    messageS << timeinfo->tm_sec << " secs ";
    messageS << "( " << (double)( m_nEventsSinceCalib ) / (double)( deltaT ) << " Evt/s )";
    calib_message( MSG::INFO, messageS.str() );
  }

  calib_message( MSG::INFO, "Starting ", type, " photon counting for run ", runNumber() );

  bool ok           = true;
  bool canvasIsOpen = false;

  // counting type
  const bool isFinal = ( type == "Final EOR" || type == "Final" );

  // Loop over radiators
  for ( const auto& rad : m_rads ) {

    // which RICH and Rad
    const auto iRad  = ( "Rich1Gas" == rad ? Rich::Rich1Gas : Rich::Rich2Gas );
    const auto iRich = richType( iRad );

    // try and load the histogram
    auto radPhotonYieldHist = h_ckPhotonYield[iRich].get();
    if ( radPhotonYieldHist ) {

      if ( checkCKThetaStats( radPhotonYieldHist ) ) {

        if ( m_createPDFsummary ) {
          // If need be open output file
          if ( !canvasIsOpen ) {
            printCanvas( "[" );
            canvasIsOpen = true;
          }
          // Draw to canvas
          radPhotonYieldHist->Draw( "e" );
          // Add run number
          TText text;
          text.SetNDC();
          text.SetTextSize( 0.025 );
          const auto runS = "Run " + std::to_string( runNumber() );
          text.DrawText( 0.14, 0.840, runS.c_str() );
          // print just the raw histogram, no fit etc.
          printCanvas();
        }

        // Get the result
        if ( radPhotonYieldHist->GetEntries() > 0 ) {
          const PhotonYield yield{ radPhotonYieldHist->GetMean(),
                                   radPhotonYieldHist->GetRMS() / std::sqrt( radPhotonYieldHist->GetEntries() ) };
          // info message for logging
          calib_message( MSG::INFO, rad, " photon yield for Run ", runNumber(), " = ", yield );
          // If enabled write to photon yield summary
          if ( isFinal && m_okToPublish ) { ok &= writeLogs( rad, yield ); }
        }
      }

    } else {
      calib_message( MSG::WARNING, "Failed to load histogram for '", rad, "'" );
    }

  } // Loop over radiators

  if ( canvasIsOpen ) { printCanvas( "]" ); }

  if ( ok ) {
    // save this run in the processed list with number of events used.
    m_processedRuns[runNumber()].used = m_nEventsThisRun;
  }

  calib_message( MSG::INFO, "Finished ", type, " photon counting for run ", runNumber() );

  // reset various counters for the next run
  resetCalibCounters();
  if ( isFinal ) { resetHistograms(); }

  // Make sure we finally remove any canvas created
  deleteCanvas();
}

//=============================================================================

bool PhotonCounting::writeLogs( const std::string& rad, const PhotonYield& ckPhotonYield ) const {

  // Path to the summary file
  const boost::filesystem::path dir( m_summaryPath + "/RichCalibSummaries/" );
  const boost::filesystem::path filename( rad + "Yield" + m_runByrunLog );
  const boost::filesystem::path full_path = dir / filename;

  bool ok = createDir( dir );
  if ( ok ) {

    // First time ?
    const bool firstTime = !boost::filesystem::exists( full_path );

    // open the file in append mode
    std::ofstream file( full_path.string().c_str(), std::ios_base::app );
    ok = file.is_open();
    if ( ok ) {

      // Add column headers if first time
      if ( firstTime ) { file << "Run DateTime PhotonYield PhotonYieldErr" << std::endl; }

      // get DateTime string
      const auto dateTime = getDateTimeString();

      // write the data for this run
      file << runNumber() << " " << dateTime << " " << ckPhotonYield.value << " " << ckPhotonYield.error << std::endl;

      // close the file
      file.close();

    } else {
      calib_message( MSG::WARNING, "Problem writing run by run summary file ", full_path );
    }
  }

  return ok;
}

double PhotonCounting::GetQuadraticYield( const Rich::DetectorType rich,       //
                                          const double             bkgNQuadB1, //
                                          const double             bkgNQuadB2, //
                                          const double             bkgNQuadB3, //
                                          const double             bkgNQuadSig ) const {
  // Get predefined sideband boundary
  const auto bkgQ1H = m_bkgQuad1High[rich];
  const auto bkgQ1M = m_bkgQuad1Mid[rich];
  const auto bkgQ1L = m_bkgQuad1Low[rich];
  const auto bkgQ2H = m_bkgQuad2High[rich];
  const auto bkgQ2L = m_bkgQuad2Low[rich];

  // System of Linear Equations in three variables using Cramer’s Rule -
  // https://www.geeksforgeeks.org/system-linear-equations-three-variables-using-cramers-rule/
  // Calculate the matrix elements of the four matrices. Three equations and three unkonwns - a,b,c.
  // We know the area of three sidebands - S = [a/3*x^3 + b/2*x^2 + cx] evl at region boundary x2 - x1 etc.
  Gaudi::Vector9 InputData_D( EvalIntegralLimit( bkgQ1M, bkgQ1L, 3 ), EvalIntegralLimit( bkgQ1M, bkgQ1L, 2 ),
                              EvalIntegralLimit( bkgQ1M, bkgQ1L, 1 ), EvalIntegralLimit( bkgQ1H, bkgQ1M, 3 ),
                              EvalIntegralLimit( bkgQ1H, bkgQ1M, 2 ), EvalIntegralLimit( bkgQ1H, bkgQ1M, 1 ),
                              EvalIntegralLimit( bkgQ2H, bkgQ2L, 3 ), EvalIntegralLimit( bkgQ2H, bkgQ2L, 2 ),
                              EvalIntegralLimit( bkgQ2H, bkgQ2L, 1 ) );

  Gaudi::Vector9 InputData_D_1(
      bkgNQuadB1, EvalIntegralLimit( bkgQ1M, bkgQ1L, 2 ), EvalIntegralLimit( bkgQ1M, bkgQ1L, 1 ), bkgNQuadB2,
      EvalIntegralLimit( bkgQ1H, bkgQ1M, 2 ), EvalIntegralLimit( bkgQ1H, bkgQ1M, 1 ), bkgNQuadB3,
      EvalIntegralLimit( bkgQ2H, bkgQ2L, 2 ), EvalIntegralLimit( bkgQ2H, bkgQ2L, 1 ) );

  Gaudi::Vector9 InputData_D_2(
      EvalIntegralLimit( bkgQ1M, bkgQ1L, 3 ), bkgNQuadB1, EvalIntegralLimit( bkgQ1M, bkgQ1L, 1 ),
      EvalIntegralLimit( bkgQ1H, bkgQ1M, 3 ), bkgNQuadB2, EvalIntegralLimit( bkgQ1H, bkgQ1M, 1 ),
      EvalIntegralLimit( bkgQ2H, bkgQ2L, 3 ), bkgNQuadB3, EvalIntegralLimit( bkgQ2H, bkgQ2L, 1 ) );

  Gaudi::Vector9 InputData_D_3(
      EvalIntegralLimit( bkgQ1M, bkgQ1L, 3 ), EvalIntegralLimit( bkgQ1M, bkgQ1L, 2 ), bkgNQuadB1,
      EvalIntegralLimit( bkgQ1H, bkgQ1M, 3 ), EvalIntegralLimit( bkgQ1H, bkgQ1M, 2 ), bkgNQuadB2,
      EvalIntegralLimit( bkgQ2H, bkgQ2L, 3 ), EvalIntegralLimit( bkgQ2H, bkgQ2L, 2 ), bkgNQuadB3 );

  Gaudi::Matrix3x3 Matrix_D( InputData_D.begin(), InputData_D.end() );
  Gaudi::Matrix3x3 Matrix_D_1( InputData_D_1.begin(), InputData_D_1.end() );
  Gaudi::Matrix3x3 Matrix_D_2( InputData_D_2.begin(), InputData_D_2.end() );
  Gaudi::Matrix3x3 Matrix_D_3( InputData_D_3.begin(), InputData_D_3.end() );
  // Calculate the determinant of each matrix
  double det_A, det_A_1, det_A_2, det_A_3;
  Matrix_D.Det( det_A );
  Matrix_D_1.Det( det_A_1 );
  Matrix_D_2.Det( det_A_2 );
  Matrix_D_3.Det( det_A_3 );
  // Get the coeffient of the function - Cramer’s Rule
  const auto a = 3 * det_A_1 / det_A;
  const auto b = 2 * det_A_2 / det_A;
  const auto c = det_A_3 / det_A;
  // Get the quadratic photon yield - substract the estimated background in signal region
  const auto bkgSubN_Q =
      bkgNQuadSig - ( c * EvalIntegralLimit( bkgQ2L, bkgQ1H, 1 ) + b * EvalIntegralLimit( bkgQ2L, bkgQ1H, 2 ) / 2 +
                      a * EvalIntegralLimit( bkgQ2L, bkgQ1H, 3 ) / 3 );

  return bkgSubN_Q;
}

double PhotonCounting::EvalIntegralLimit( const double& x2, const double& x1, const int& p ) const {
  return std::pow( x2, p ) - std::pow( x1, p );
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PhotonCounting )

//-----------------------------------------------------------------------------

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Local
#include "RichCalibUtils.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "Event/ODIN.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rich Detector
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// Publish to DIM
#include "GaudiKernel/IPublishSvc.h"
#include "GaudiKernel/ServiceLocatorHelper.h"

// Dim Objects
#include "RichMonitoringTools/RichDimClasses.h"

// CK resolution fitter
#include "RichRecUtils/RichCKResolutionFitter.h"

namespace Rich::Future::Rec::Calib {

  // Use the functional framework
  using namespace Gaudi::Functional;
  // Calib Utils
  using namespace Rich::Calib::Utils;
  using namespace Rich::Utils;

  namespace {
    using ScaleFactor       = ValueWithError<double>;
    using ScaleFactorsCache = DetectorArray<ScaleFactor>;
  } // namespace

  /** @class RefIndexCalib RichRefIndexCalib.h
   *
   *  Runs the RICH refractive index calibration directly from photon objects
   *
   *  @author Chris Jones
   *  @date   2021-02-15
   */

  class RefIndexCalib final : public Consumer<void( const LHCb::ODIN&,                         //
                                                    const LHCb::Track::Selection&,             //
                                                    const Summary::Track::Vector&,             //
                                                    const Relations::PhotonToParents::Vector&, //
                                                    const LHCb::RichTrackSegment::Vector&,     //
                                                    const CherenkovAngles::Vector&,            //
                                                    const SIMDCherenkovPhoton::Vector&,        //
                                                    const ScaleFactorsCache& ),
                                              LHCb::DetDesc::usesBaseAndConditions<CalibAlg, ScaleFactorsCache>> {

  public:
    /// Standard constructor
    RefIndexCalib( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                      KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                      KeyValue{ "ScaleFactorsCache", DeRichLocations::derivedCondition( name + "-DataCache" ) } } ) {

      declareProperty( "nPolFull", m_ckFitter.params().RichNPol );
      declareProperty( "StartNPol", m_ckFitter.params().RichStartPol );
      declareProperty( "MaxShiftError", m_ckFitter.params().MaxShiftError );
      declareProperty( "MaxSigmaError", m_ckFitter.params().MaxSigmaError );
      declareProperty( "RichFitMin", m_ckFitter.params().RichFitMin );
      declareProperty( "RichFitMax", m_ckFitter.params().RichFitMax );
      declareProperty( "RichFitTypes", m_ckFitter.params().RichFitTypes );

      setProperty( "NBins1DHistos", 150 ).ignore();
      setProperty( "NBins2DHistos", 100 ).ignore();

      m_taskType = "RefIndex";
    }

  public:
    /// Functional operator
    void operator()( const LHCb::ODIN&                         odin,          //
                     const LHCb::Track::Selection&             tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons,       //
                     const ScaleFactorsCache&                  scaleFactors ) const override;

    /// Initialization
    StatusCode initialize() override {

      // for debug for testing
      // setProperty( "OutputLevel", MSG::DEBUG ).ignore();

      return Consumer::initialize().andThen( [&] {
        StatusCode sc = StatusCode::SUCCESS;

        // Derived conditions
        Detector::Rich1::addConditionDerivation( this );
        Detector::Rich2::addConditionDerivation( this );
        addConditionDerivation(
            { Detector::Rich1::DefaultConditionKey,     //
              Detector::Rich2::DefaultConditionKey },   //
            inputLocation<ScaleFactorsCache>(),         //
            [parent = this]( const Detector::Rich1& r1, //
                             const Detector::Rich2& r2 ) {
              const ScaleFactorsCache scaleFs{ ScaleFactor{ r1.radiator().currentRefIndexScaleFactor(), 0.0 },
                                               ScaleFactor{ r2.radiator().currentRefIndexScaleFactor(), 0.0 } };
              parent->calib_message( MSG::DEBUG, "Current Scalefactors : R1Gas=", scaleFs[Rich::Rich1],
                                     " R2Gas=", scaleFs[Rich::Rich2] );
              return scaleFs;
            } );

        if ( !createDir( m_conditionsDbPath.value(), false ) ) {
          const auto curPath = boost::filesystem::current_path().string();
          calib_message( MSG::WARNING, "Cannot write to '", m_conditionsDbPath.value(), "' -> Resetting to '", curPath,
                         "/conditions'" );
          m_conditionsDbPath = curPath + "/conditions";
        }
        calib_message( MSG::INFO, "Writing conditions data to ", m_conditionsDbPath.value() );

        // Fix file permissions ...
        // fixFilePermissions();

        // Publish service
        if ( !m_okToPublish ) { m_disableDIMpublish = true; }
        if ( !m_disableDIMpublish ) {
          // Is the DIM DNS node defined ?
          const char* dimDnsNode = getenv( "DIM_DNS_NODE" );
          if ( !dimDnsNode ) {
            calib_message( MSG::WARNING, "DIM_DNS_NODE not defined. Disabling DIM publishing" );
            m_disableDIMpublish = true;
          } else {
            calib_message( MSG::INFO, "Using DIM_DNS_NODE '", dimDnsNode, "'" );
            // Initialise scale factors to last known values
            m_Rich1RefIndex = getLastRefIndexSF( "Rich1Gas" ).first;
            m_Rich2RefIndex = getLastRefIndexSF( "Rich2Gas" ).first;
            // Initialise the publish service
            m_pPublishSvc = serviceLocator()->service<IPublishSvc>( "LHCb::PublishSvc", true );
            if ( m_pPublishSvc ) {
              m_pPublishSvc->declarePubItem( m_Rich1TaskName.value(), m_Rich1PubString );
              m_pPublishSvc->declarePubItem( m_Rich2TaskName.value(), m_Rich2PubString );
              m_pPublishSvc->declarePubItem( m_Rich1ScaleTaskName.value(), m_Rich1RefIndex );
              m_pPublishSvc->declarePubItem( m_Rich2ScaleTaskName.value(), m_Rich2RefIndex );
              calib_message( MSG::INFO, "LHCb::PublishSvc successfully initialized" );
            } else {
              calib_message( MSG::ERROR, "Failed to instanciate LHCb::PublishSvc" );
              sc = StatusCode::FAILURE;
            }
          }
        }

        // Forced run numnber
        if ( m_forcedRunNumber > 0 ) {
          calib_message( MSG::WARNING, "Run number override enabled, forced to '", m_forcedRunNumber.value(), "'" );
        }

        // write to summary file
        writeToDimSummaryFile( "Initialized" );
        calib_message( MSG::INFO, "Initialized" );

        return sc;
      } );
    }

    /// Print run summaries
    void runSummaries( const MSG::Level level = MSG::INFO ) const {
      if ( msgLevel( level ) && !m_processedRuns.empty() ) {
        calib_message( level, "+-----------+-------------+-------------+------------+------------+" );
        calib_message( level, "|    Run    | Used Events | Late Events |  Rich1 Ver |  Rich2 Ver |" );
        calib_message( level, "+-----------+-------------+-------------+------------+------------+" );
        const auto& R1Vers = m_runCalibVersions["Rich1Gas"];
        const auto& R2Vers = m_runCalibVersions["Rich2Gas"];
        for ( const auto& [run, stats] : m_processedRuns ) {
          const auto iR1V = R1Vers.find( run );
          const auto iR2V = R2Vers.find( run );
          const auto R1V  = ( iR1V != R1Vers.end() ? str( boost::format( "%8u" ) % iR1V->second ) : "     N/A" );
          const auto R2V  = ( iR2V != R2Vers.end() ? str( boost::format( "%8u" ) % iR2V->second ) : "     N/A" );
          calib_message( level, boost::format( "| %8u  |  %9u  |  %9u  | %s   | %s   |" ) //
                                    % run % stats.used % stats.late % R1V % R2V );
        }
        calib_message( level, "+-----------+-------------+-------------+------------+------------+" );
      }
    }

    /// Start
    StatusCode start() override {
      divider();
      writeToDimSummaryFile( "Starting" );
      calib_message( MSG::INFO, "Starting" );
      return Consumer::start();
    }

    /// Stop
    StatusCode stop() override {
      divider();
      calib_message( MSG::INFO, "Stopping" );
      // Do not run calibration in stop() during QMT tests. Defer final one
      // to finalize() which is then included in the test ref comparisons.
      if ( m_allowStopCalib ) {
        // use periodic here incase task starts up again with the same run
        runCalibration( "Stop" );
      }
      writeToDimSummaryFile( "Stopped" );
      calib_message( MSG::INFO, "Stopped" );
      return Consumer::stop();
    }

    /// Finalize
    StatusCode finalize() override {
      divider();
      calib_message( MSG::INFO, "Finalizing" );
      // In principle calibration not needed here, as I do not think its
      // possible to be here without having stop() called first, but just
      // to be sure try again. At minimum it will trigger the resets.
      // Also needed for when running in a QMT test as there stop() calibrations are not run.
      runCalibration();
      runSummaries();
      // cleanup
      deleteCanvas();
      // write to summary file
      writeToDimSummaryFile( "Finalized" );
      calib_message( MSG::INFO, "Finalized" );
      return Consumer::finalize();
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok        = true;
      auto initTHist = [&]( auto* h ) {
        bool ok = false;
        if ( h ) {
          ok = true;
          // Set labels
          if ( h->GetXaxis() ) { h->GetXaxis()->SetTitle( "delta(Cherenkov Theta) / rad" ); }
          if ( h->GetYaxis() ) { h->GetYaxis()->SetTitle( "Entries" ); }
          // work around Root's stupid ownership. Without this, if ever someone has open a root file
          // somewhere, it will own the histogram (yes !)
          h->SetDirectory( nullptr );
        } else {
          calib_message( MSG::ERROR, "Cannot initialise a NULL ROOT histogram pointer" );
        }
        return ok;
      };
      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {
        const auto rad = radType( rich );
        // create histograms for calibration
        // Note not using Gaudi mechanisms here as we want these to be completely unknown to
        // UpdateAndReset to prevent that resetting them at times we do not want.
        h_ckResAll[rich] = std::make_unique<TH1D>( ( "ckResAll" + Rich::text( rad ) ).c_str(),                     //
                                                   ( Rich::text( rad ) + " Rec-Exp CK Theta Resolution" ).c_str(), //
                                                   nBins1D(), -m_ckResRange[rich], m_ckResRange[rich] );
        ok &= initTHist( h_ckResAll[rich].get() );
        if ( m_enableQuadFits[rich] ) {
          for ( const auto q : quadrants() ) {
            h_ckResAllQuads[rich][q] = std::make_unique<TH1D>(
                ( "ckResAll" + Rich::text( rad ) + "Q" + std::to_string( q ) ).c_str(),           //
                ( Rich::text( rad ) + " Rec-Exp CK Theta Resolution" + quadString( q ) ).c_str(), //
                nBins1D(), -m_ckResRange[rich], m_ckResRange[rich] );
            ok &= initTHist( h_ckResAllQuads[rich][q].get() );
          }
        }
      } // active rad loop
      if ( !m_enableRunByRunHists ) { initExtraHists(); }
      return StatusCode{ ok };
    }

  private:
    /// Test if a fit retry should be tried for given radiator
    inline bool canRetryFit( const Rich::DetectorType rich ) const {
      return ( m_failedFitCount[rich] > 0 && m_failedFitCount[rich] < m_maxfailedFitRetries );
    }

    /// Test if a fit retry should be tried for either radiator
    inline bool canRetryFit() const { return ( canRetryFit( Rich::Rich1 ) || canRetryFit( Rich::Rich2 ) ); }

    /// Reset the failed fit counts
    void resetFailedfitCounts( const Rich::DetectorType rich ) const { m_failedFitCount[rich] = 0; }

    /// Reset histograms following running a calibration
    void resetHistograms() const {
      calib_message( MSG::VERBOSE, "Resetting histograms" );
      // reset run event count
      m_nEventsThisRun = 0;
      // reset calibration fit histograms
      auto reset_h = []( auto* h ) {
        if ( h ) { h->Reset(); }
      };
      for ( const auto rich : activeDetectors() ) {
        reset_h( h_ckResAll[rich].get() );
        if ( m_enableQuadFits[rich] ) {
          for ( auto& h : h_ckResAllQuads[rich] ) { reset_h( h.get() ); }
        }
        // reset failed fit counts
        resetFailedfitCounts( rich );
      }
    }

    /// Run the n-1 calibration ...
    void runCalibration( const std::string& type = "Final" ) const;

    /// Initialise extra histos
    bool initExtraHists( const unsigned int RunNumber = 0 ) const {
      bool ok = true;
      if ( !m_lastRunExtraHists.has_value() || RunNumber != m_lastRunExtraHists.value() ) {
        m_lastRunExtraHists = RunNumber;
        calib_message( MSG::DEBUG, "Resetting run-by-run histograms for run ", RunNumber );
        const auto runT  = ( RunNumber > 0 ? " | Run " + std::to_string( RunNumber ) : "" );
        const auto runID = ( RunNumber > 0 ? "Runs/" + std::to_string( RunNumber ) + "/" : "" );
        for ( const auto rich : activeDetectors() ) {
          const auto rad = radType( rich );
          // reset pointers to new histos for this run
          ok &= initHist( h_ckThetaRec_run[rich].emplace_back(),                  //
                          HID( runID + "ckThetaRec", rad ), "Rec CKTheta" + runT, //
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),      //
                          "Cherenkov theta / rad", "Entries" );
          ok &= initHist( h_ckThetaExp_run[rich].emplace_back(),                  //
                          HID( runID + "ckThetaExp", rad ), "Exp CKTheta" + runT, //
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),      //
                          "Cherenkov theta / rad", "Entries" );
          ok &= initHist( h_ckThetaRes_run[rich].emplace_back(),              //
                          HID( runID + "ckThetaRes", rad ),                   //
                          "Rec-Exp CKTheta" + runT,                           //
                          -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                          "delta(Cherenkov theta) / rad", "Entries" );
          for ( const auto q : quadrants() ) {
            ok &= initHist( h_ckThetaRes_quads_run[rich][q].emplace_back(),             //
                            HID( runID + "ckThetaResQuad" + std::to_string( q ), rad ), //
                            "Rec-Exp CKTheta" + quadString( q ) + runT,                 //
                            -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                            "delta(Cherenkov theta) / rad", "Entries" );
          }
          // track (x,y) plots
          const double xSize = ( Rich::Rich1Gas == rad ? 500.0 : 2000.0 );
          const double ySize = ( Rich::Rich1Gas == rad ? 500.0 : 2000.0 );
          ok &= initHist( h_tkEntryXY_run[rich].emplace_back(), //
                          HID( runID + "trackEntryXY", rad ),   //
                          "Track Radiator Entry (x,y)" + runT,  //
                          -xSize, xSize, nBins2D(),             //
                          -ySize, ySize, nBins2D(),             //
                          "Track Entry X / mm", "TrackEntry Y / mm", "Entries" );
        }
      }
      return ok;
    }

  private:
    // properties

    /// Minimum number of events for a calibration
    Gaudi::Property<unsigned long long> m_minEventsForCalib{ this, "MinEventsForCalib", 100 };

    /// Time interval to send periodic calibration updates
    Gaudi::Property<int> m_minTimeBetweenCalibs{ this, "MinTimeBetweenCalibs", 16 * 60 }; // 16 mins

    /// Time interval for failed fit retries
    Gaudi::Property<int> m_failedFitRetryPeriod{ this, "FailedFitRetryPeriod", 20 }; // 20 secs

    /// Max number of failed fit retries
    Gaudi::Property<unsigned int> m_maxfailedFitRetries{ this, "MaxFailedFitRetries", 10 };

    /// RICH1 task name
    Gaudi::Property<std::string> m_Rich1TaskName{ this, "Rich1TaskName", "Rich1/Environment/Gas" };

    /// RICH2 task name
    Gaudi::Property<std::string> m_Rich2TaskName{ this, "Rich2TaskName", "Rich2/Environment/Gas" };

    /// RICH1 name for n-1 scale factor DIM service
    Gaudi::Property<std::string> m_Rich1ScaleTaskName{ this, "Rich1ScaleTaskName", "Rich1RefIndex" };

    /// RICH2 name for n-1 scale factor DIM service
    Gaudi::Property<std::string> m_Rich2ScaleTaskName{ this, "Rich2ScaleTaskName", "Rich2RefIndex" };

    /// Flag to turn on the creation of a Preliminary calibration for each run
    Gaudi::Property<bool> m_createPrelimCalib{ this, "CreatePreliminaryCalibration", true };

    /// The ROOT file directory to use for the calibration (the Gaudi Monitor)
    Gaudi::Property<std::string> m_HistBase{ this, "HistBase", "RICH/RiCKResLongTight/" };

    /// The histogram ID to use
    Gaudi::Property<std::string> m_ResPlot{ this, "ResPlot", "ckResAll" };

    /// CK Resolution fitter
    Rich::Rec::CKResolutionFitter m_ckFitter{ this };

    /// slope from the shift
    Gaudi::Property<std::array<double, 2>> m_RefIndexSlope{ this, "RefIndexSlope", { 38.1, 65.25 } };

    /// Precision for YML file parameters
    Gaudi::Property<unsigned int> m_Precision{ this, "Precision", 8 };

    /// LHCB DB to write to path
    Gaudi::Property<std::string> m_conditionsDbPath{ this, "conditionsDbPath", "/group/online/hlt/conditions.run3" };

    /// Create direct run conditions
    Gaudi::Property<bool> m_makeDBRunFile{ this, "MakeDBRunFile", false };

    /// log file to list all previous YML versions
    Gaudi::Property<std::string> m_ymlVersionLog{ this, "ymlVersionLog", "version.txt" };

    /// log file for previous scale factors
    Gaudi::Property<std::string> m_RefIndexSFLog{ this, "RefIndexSFLog", "refIndexSF.txt" };

    /// DIM Summary file name
    Gaudi::Property<std::string> m_dimSummaryFile{ this, "DIMSummaryFile", "DIMUpdateSummary.txt" };

    /// Flag to turn on/off publishing to DIM
    Gaudi::Property<bool> m_disableDIMpublish{ this, "DisableDIMPublish", false };

    /// maximum cached calibrations to allow in a row
    Gaudi::Property<unsigned int> m_maxCachedCalibs{ this, "MaxCachedCalibsInARow", 5 };

    /// Use a running average for the scale factors ?
    Gaudi::Property<bool> m_useRunningAv{ this, "UseRunningAverage", true };

    /// Allow calibrations during stop()
    Gaudi::Property<bool> m_allowStopCalib{ this, "AllowCalibAtStop", true };

    /// Min CK res for good fit
    Gaudi::Property<DetectorArray<double>> m_minCKTRes{ this, "MinCKThetaRes", { 0.0005, 0.0002 } };
    /// Max CK res for good fit
    Gaudi::Property<DetectorArray<double>> m_maxCKTRes{ this, "MaxCKThetaRes", { 0.0020, 0.0010 } };
    /// Max tolerance on fitted mean value
    Gaudi::Property<DetectorArray<double>> m_maxCKTMean{ this, "MaxCKThetaMean", { 0.001, 0.0006 } };

    /// Override for run number (e.g. for testing to merge runs)
    Gaudi::Property<unsigned int> m_forcedRunNumber{ this, "ForcedRunNumber", 0 };

    /// Include quadrant fits in PDFs
    Gaudi::Property<DetectorArray<bool>> m_enableQuadFits{ this, "EnableQuadrantFits", { true, true } };

  private:
    // cached datax

    /// Lock for main event processing
    mutable std::mutex m_mutex;

    // CK theta histograms for calibration fits
    Hist::DetArray<std::unique_ptr<TH1D>>                  h_ckResAll      = { {} };
    Hist::DetArray<Hist::QuadArray<std::unique_ptr<TH1D>>> h_ckResAllQuads = { { {} } };

    // Monitoring histos
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_ckThetaRec_run       = { {} };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_ckThetaExp_run       = { {} };
    mutable Hist::DetArray<RunByRunHists<Hist::H1D<>>>                  h_ckThetaRes_run       = { {} };
    mutable Hist::DetArray<Hist::QuadArray<RunByRunHists<Hist::H1D<>>>> h_ckThetaRes_quads_run = { { {} } };
    mutable Hist::DetArray<RunByRunHists<Hist::H2D<>>>                  h_tkEntryXY_run        = { {} };

    /// Publishing service
    IPublishSvc* m_pPublishSvc = nullptr;

    /// String to publish for new calibrations for RICH1
    mutable std::string m_Rich1PubString{ "" };

    /// String to publish for new calibrations for RICH2
    mutable std::string m_Rich2PubString{ "" };

    /// The RICH1 scale factor value to publish
    mutable double m_Rich1RefIndex{ 1.0 };

    /// The RICH2 scale factor value to publish
    mutable double m_Rich2RefIndex{ 1.0 };

    /** Keep a count of the number of calibrations in a row, per radiator,
     *  for which a cached value from a previous run is used */
    mutable std::unordered_map<std::string, unsigned int> m_cachedCalibCount;

    /// Map of condition version numbers for each run
    mutable std::unordered_map<std::string, std::unordered_map<unsigned int, unsigned long long>> m_runCalibVersions;

    /// The scale factors for the current data collection
    mutable ScaleFactorsCache m_scaleFs;

    /// Current failed fit count
    mutable DetectorArray<unsigned int> m_failedFitCount{ { 0, 0 } };

  private:
    /// Fit Result object
    using FitResult = Rich::Rec::CKResolutionFitter::FitResult;

    /// Set the currently used scale factors
    inline void setCurrentScaleFactors( const ScaleFactorsCache& scaleFs ) const noexcept { m_scaleFs = scaleFs; }

    // access the current scale factors cached in the writer
    inline const auto& currentScaleFactors() const noexcept { return m_scaleFs; }

    //=============================================================================
    /// Fit the histogram
    //=============================================================================
    FitResult fitCKThetaHistogram( TH1* hist, const std::string& rad, bool silent = false ) const;

    //=============================================================================
    /// Fit result sanity check
    //=============================================================================
    inline bool fitSanityCheck( const Rich::DetectorType rich, const FitResult& fitRes ) const {
      bool OK = true;
      // Check CK theta resolution
      const auto ckRes = fitRes.ckResolution;
      if ( ckRes < m_minCKTRes[rich] || ckRes > m_maxCKTRes[rich] ) {
        calib_message( MSG::WARNING, rich, " CK Theta Res ", ckRes, //
                       " FAILED range check ", m_minCKTRes[rich], " to ", m_maxCKTRes[rich] );
        info() << " -> FAILED checks" << endmsg;
        OK = false;
      }
      // Check fitted mean
      const auto ckMean = fitRes.ckShift;
      if ( fabs( ckMean ) > m_maxCKTMean[rich] ) {
        calib_message( MSG::WARNING, rich, " CK Theta Mean ", ckMean, " FAILED range check <", m_maxCKTMean[rich] );
        OK = false;
      }
      if ( OK ) { calib_message( MSG::DEBUG, "Fit passed sanity checks" ); }
      return OK;
    }

    //=============================================================================
    /// Function to calculate scale factor
    //=============================================================================
    inline auto nScaleFromShift( const double       shift,     //
                                 const double       shift_err, //
                                 const std::string& rad,       //
                                 bool               silent = false ) const {
      silent &= !msgLevel( MSG::DEBUG );
      // which RICH
      const auto iR = ( "Rich1Gas" == rad ? Rich::Rich1 : Rich::Rich2 );
      // Calculate and return the scale factor
      const auto  scaleFcorrection     = ( 1.0 + ( shift * m_RefIndexSlope[iR] ) );
      const auto  newScale             = m_scaleFs[iR].value * scaleFcorrection;
      const auto  scaleFcorrection_err = ( shift_err * m_RefIndexSlope[iR] );
      const auto  newScale_err         = m_scaleFs[iR].value * scaleFcorrection_err;
      ScaleFactor scale{ newScale, newScale_err };

      // apply a running average ?
      if ( m_useRunningAv.value() ) {

        const auto last_scale = getLastRefIndexSF( rad );
        // In case last scale not available (i.e. 0) just return
        if ( last_scale.first < 0.1 ) { return scale; }

        const auto time_delta = m_timeCurrentCalibStart - last_scale.second;
        // just in case ...
        if ( time_delta <= 0 ) { return scale; }

        // Weight for past scale factor
        // 0.5 for 10 mins old
        // 0.1 for 60 mins old
        // linearly decreasing for older times.
        // No larger than 0.7, no smaller than 0
        auto weight = []( const auto dt ) {
          const std::time_t t1 = 10 * 60;
          const std::time_t t2 = 60 * 60;
          const double      w1 = 0.50;
          const double      w2 = 0.10;
          const double      m  = ( w1 - w2 ) / ( t1 - t2 );
          const double      c  = ( ( w2 * t1 ) - ( w1 * t2 ) ) / ( t1 - t2 );
          return std::clamp( ( ( m * dt ) + c ), 0.0, std::max( w1, w2 ) );
        };

        // Form new running scale from weighted average
        const auto w         = weight( time_delta );
        const auto new_scale = ( w * last_scale.first ) + ( ( 1.0 - w ) * scale.value );

        if ( !silent ) {
          calib_message( MSG::INFO, rad, " Running Scale | Last=", last_scale.first, " dT=", time_delta, " weight=", w,
                         " New=", scale.value, " | WeightedAv=", new_scale );
        }

        scale.value = new_scale;
      }

      return scale;
    }

    //=============================================================================
    // Function to get ref index scale from previous run
    //=============================================================================
    std::pair<double, std::time_t> getLastRefIndexSF( const std::string& rad ) const;

    //=============================================================================
    /// Function to update ref index scale
    //=============================================================================
    void updateRefIndexSF( const std::string& rad, //
                           const double       scale ) const;

    //=============================================================================
    /// Function to write out yml file
    //=============================================================================
    bool ymlWriter( const std::string& type,  //
                    const double       scale, //
                    const std::string& rad ) const;

    //=============================================================================
    /// Function to set version number for current run
    //=============================================================================
    std::pair<bool, unsigned long long> setVersion( const std::string& rad ) const;

    /// DIM summary file location
    inline std::string dimSummaryFile() const { return summaryPath() + m_dimSummaryFile; }

    /// Write a message to the DIM summary file
    bool writeToDimSummaryFile( const std::string& mess ) const {
      bool OK = true;
      if ( m_okToPublish ) {
        // Open the summary file in append mode
        std::ofstream file( dimSummaryFile().c_str(), std::ios_base::app );
        OK = file.is_open();
        if ( OK ) {
          file << getDateTimeString( "%Y-%m-%d %H:%M:%S" ) << " | " << mess << std::endl;
          file.close();
          // setPerms( dimSummaryFile() );
        } else {
          calib_message( MSG::WARNING, "Problem writing DIM summary file ", dimSummaryFile() );
        }
      }
      return OK;
    }

    /// Write summary logs
    bool writeLogs( const std::string& rad, //
                    const FitResult&   fitResult ) const;

    /// Radiator name to task name
    inline auto radToTaskName( const std::string& rad ) const noexcept {
      const std::string task = ( "Rich1Gas" == rad ? "Rich1/Calib" : //
                                     "Rich2Gas" == rad ? "Rich2/Calib"
                                                       : //
                                     "" );
      if ( task.empty() ) { calib_message( MSG::WARNING, "Unknown radiator '", rad, "'" ); }
      return task;
    }
  };

} // namespace Rich::Future::Rec::Calib

using namespace Rich::Future::Rec::Calib;

//-----------------------------------------------------------------------------

void RefIndexCalib::operator()( const LHCb::ODIN&                         odin,          //
                                const LHCb::Track::Selection&             tracks,        //
                                const Summary::Track::Vector&             sumTracks,     //
                                const Relations::PhotonToParents::Vector& photToSegPix,  //
                                const LHCb::RichTrackSegment::Vector&     segments,      //
                                const CherenkovAngles::Vector&            expTkCKThetas, //
                                const SIMDCherenkovPhoton::Vector&        photons,       //
                                const ScaleFactorsCache&                  scaleFactors ) const {

  // Only one thread can be running from this point on
  std::scoped_lock lock{ m_mutex };

  // First event printout
  if ( 0 == m_nEvts ) {
    calib_message( MSG::INFO, "First Event Seen" );
    // set last time to now.
    m_timeLastCalib = time( nullptr );
  }

  // Current run number
  const auto RunNumber = ( m_forcedRunNumber.value() > 0 ? m_forcedRunNumber.value() : odin.runNumber() );

  // Send info on new runs
  if ( RunNumber != runNumber() ) {
    // check to see if this is one we have seen in the past.
    // protects against 'flip flopping' run numbers online that can be
    // seen at run changes.
    auto runStats = m_processedRuns.find( RunNumber );
    if ( runStats == m_processedRuns.end() ) {
      calib_message( MSG::INFO, "==================================================> New Run ", RunNumber,
                     " <==================================================" );
    } else {
      // Have we gone back in time... ?
      if ( RunNumber < runNumber() ) {
        ++runStats->second.late;
        return;
      }
    }
  }

  // Check to see if a calibration should be run due to a new run being detected
  if ( runNumber() != 0 ) {
    if ( runNumber() != RunNumber ) {
      runCalibration();
    } else if ( ( currentScaleFactors()[Rich::Rich1].value != scaleFactors[Rich::Rich1].value ||
                  currentScaleFactors()[Rich::Rich2].value != scaleFactors[Rich::Rich2].value ) &&
                m_nEvts > 0 ) {
      // scale factors have changed but no run change ?
      calib_message( MSG::WARNING, "Scale factors changed for run ", RunNumber, " | Old=", currentScaleFactors(),
                     " New=", scaleFactors );
      runCalibration();
    }
  }

  // Before updating run number check if new run-by-run histograms are required
  if ( m_enableRunByRunHists && RunNumber != runNumber() ) { initExtraHists( RunNumber ); }

  // update cached run number
  m_runNumber = RunNumber;
  // count events
  ++m_nEvts;

  // update cached scale factors in writer for current run
  setCurrentScaleFactors( scaleFactors );

  // run periodic calibration ?
  if ( m_nEventsSinceCalib >= m_minEventsForCalib && m_minTimeBetweenCalibs > 0 ) {
    // time since last calibration
    const time_t deltaT = ( time( nullptr ) - m_timeLastCalib );
    if ( deltaT >= m_minTimeBetweenCalibs ) {
      // run periodic calibration, no histogram reset...
      runCalibration( "Periodic" );
    }
  }

  // run a fit retry due to a previous failure ?
  if ( canRetryFit() ) {
    // time since last calibration
    const time_t deltaT = ( time( nullptr ) - m_timeLastCalib );
    if ( deltaT >= m_failedFitRetryPeriod ) { runCalibration( "Retry" ); }
  }

  // count events since last calibration was made
  ++m_nEventsSinceCalib;

  // count events this run
  ++m_nEventsThisRun;

  // =================================================================================
  // Fill the histograms
  // =================================================================================

  // The PID type to assume. Just use Pion here.
  const auto pid = Rich::Pion;

  // loop over tracks
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {
    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // RICH info
      const auto rich = seg.rich();
      if ( !richIsActive( rich ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );

      // selection cuts
      if ( beta < m_minBeta[rich] || beta > m_maxBeta[rich] ) { continue; }

      // expected CK theta in SIMD form
      const SIMDCherenkovPhoton::SIMDFP thetaExp( expCKangles[pid] );

      // SIMD delta theta
      const auto deltaTheta = phot.CherenkovTheta() - thetaExp;

      // Segment (x,y) entry point to radiator
      const auto seg_x = seg.entryPoint().x();
      const auto seg_y = seg.entryPoint().y();
      const auto q     = quadrant( seg_x, seg_y );
      fillRunByRunHist( h_tkEntryXY_run[rich], seg_x, seg_y );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries and fill
        if ( phot.validityMask()[i] ) {
          // plots used for condition update and PDFs
          h_ckResAll[rich]->Fill( deltaTheta[i] );
          if ( m_enableQuadFits[rich] ) { h_ckResAllQuads[rich][q]->Fill( deltaTheta[i] ); }
          // these are for monitoring (Monet)
          fillRunByRunHist( h_ckThetaRec_run[rich], phot.CherenkovTheta()[i] );
          fillRunByRunHist( h_ckThetaExp_run[rich], thetaExp[i] );
          fillRunByRunHist( h_ckThetaRes_run[rich], deltaTheta[i] );
          fillRunByRunHist( h_ckThetaRes_quads_run[rich][q], deltaTheta[i] );
        }
      }

    } // photons
  }   // tracks
}

void RefIndexCalib::runCalibration( const std::string& type ) const {

  // check run number is reasonable
  if ( 0 == runNumber() ) {
    // if we have also seen events, something is wrong...
    if ( m_nEventsSinceCalib > 0 ) {
      calib_message( MSG::WARNING, "Undefined run number !! -> ", type, " calibration aborted" );
    }
    resetHistograms();
    resetCalibCounters();
    return;
  }

  // Only do anything if some events have been seen since last time.
  if ( 0 == m_nEventsSinceCalib ) {
    calib_message( MSG::WARNING, "No events processed for run ", runNumber(), " -> ", type, " calibration aborted" );
    resetHistograms();
    resetCalibCounters();
    return;
  }

  // Is this a previously calibrated run
  const auto iRunCalData = m_processedRuns.find( runNumber() );
  if ( iRunCalData != m_processedRuns.end() ) {
    // Do we now have more events to calibrate with than last time ?
    if ( iRunCalData->second.used >= m_nEventsThisRun ) {
      calib_message( MSG::DEBUG, "Already calibrated run ", runNumber(), " -> Processing skipped" );
      resetCalibCounters();
      return;
    } else {
      calib_message( MSG::INFO, "Run ", runNumber(), " : Previous #Events = ", iRunCalData->second.used,
                     " New #Events = ", m_nEventsThisRun );
    }
  }

  // Print message on the number of events seen and the rate
  // current time
  m_timeCurrentCalibStart = time( nullptr );
  // time since last calibration
  const time_t deltaT = ( m_timeCurrentCalibStart - m_timeLastCalib );
  if ( deltaT > 0 ) {
    struct tm*         timeinfo = localtime( &deltaT );
    std::ostringstream messageS;
    messageS << "Seen " << m_nEventsSinceCalib << " events in past ";
    if ( timeinfo->tm_hour - 1 > 0 ) { messageS << timeinfo->tm_hour - 1 << " hours "; }
    if ( timeinfo->tm_min > 0 ) { messageS << timeinfo->tm_min << " mins "; }
    messageS << timeinfo->tm_sec << " secs ";
    messageS << "( " << (double)( m_nEventsSinceCalib ) / (double)( deltaT ) << " Evt/s )";
    calib_message( MSG::INFO, messageS.str() );
  }

  calib_message( MSG::INFO, "Starting ", type, " n-1 calibration for run ", runNumber() );

  bool ok           = true;
  bool canvasIsOpen = false;

  // calibration type
  const bool isFinal = ( type == "Final EOR" || type == "Final" );
  const bool isRetry = ( type == "Retry" );

  // Loop over radiators
  for ( const auto& rad : m_rads ) {

    // which RICH and Rad
    const auto iRad  = ( "Rich1Gas" == rad ? Rich::Rich1Gas : Rich::Rich2Gas );
    const auto iRich = richType( iRad );

    // is this a retry ?
    if ( isRetry && !canRetryFit( iRich ) ) {
      calib_message( MSG::INFO, "No fit retry needed for ", rad );
      continue;
    }

    calib_message( MSG::DEBUG, "Attempting to calibrate ", rad );

    // try and load the histogram
    auto radHist = h_ckResAll[iRich].get();
    if ( radHist ) {

      // save fit result;
      FitResult savedFitResult;

      // default scale same as condition value for current run
      ScaleFactor scale = m_scaleFs[iRich];

      bool hasNewScaleF = false;

      if ( checkCKThetaStats( radHist ) ) {

        // If need be open output file
        if ( !canvasIsOpen ) {
          printCanvas( "[" );
          canvasIsOpen = true;
        }

        // run the fit
        const auto fitresult = fitCKThetaHistogram( radHist, rad );
        if ( fitresult.fitOK ) {
          savedFitResult          = fitresult;
          m_cachedCalibCount[rad] = 0; // Normal calibration, so reset count to 0
          // compute scale factor from fitted shift
          scale = nScaleFromShift( fitresult.ckShift, fitresult.ckShiftErr, rad );
          calib_message( MSG::INFO, rad, " Scale Factor | Current ", m_scaleFs[iRich], " -> Update ", scale );
          // reset failed fit count
          resetFailedfitCounts( iRich );
          hasNewScaleF = true;
        } else {
          ++m_cachedCalibCount[rad]; // Count cached calibrations
          scale = ScaleFactor{ getLastRefIndexSF( rad ).first, 0.0 };
          calib_message( MSG::WARNING, "Fit FAILED. Using scale from previous run ", scale );
          // increment failed fit count for this radiator
          ++m_failedFitCount[iRich];
          calib_message( MSG::WARNING, "Incremented ", rad, " failed fit count to ", m_failedFitCount[iRich] );
          if ( m_failedFitCount[iRich] >= m_maxfailedFitRetries ) {
            calib_message( MSG::WARNING, "Maximum number of fit retries reached for ", rad,
                           ". No more will be attempted" );
          }
        }
      } else {
        // not enough statistics, take the one from previous run
        ++m_cachedCalibCount[rad]; // Count cached calibrations
        scale = ScaleFactor{ getLastRefIndexSF( rad ).first, 0.0 };
        calib_message( MSG::INFO, "Not enough statistics for ", rad, " -> Using scale from previous run ", scale );
      }

      // if a final OK calibration, write to CK resolution and scale summary
      if ( savedFitResult.fitOK && isFinal && m_okToPublish ) { ok &= writeLogs( rad, savedFitResult ); }

      // publish results. Only if histogram did not have 0 entries
      if ( 0 != radHist->GetEntries() ) {
        if ( m_cachedCalibCount[rad] > m_maxCachedCalibs ) {
          calib_message( MSG::WARNING, "Too many (", m_cachedCalibCount[rad], ") cached calibrations in a row." );
        }
        ok = ymlWriter( type, scale.value, rad );
        if ( !ok ) { calib_message( MSG::WARNING, "Failed to write YML file for ", rad ); }
      } else {
        calib_message( MSG::INFO, "YML Writing is DISABLED for this dataset" );
      }

      calib_message( MSG::DEBUG, "Scale factor for ", rad, ": ", scale );

      // Add quadrant plots to PDF if main fit was OK
      if ( m_enableQuadFits[iRich] && hasNewScaleF ) {
        for ( auto& h : h_ckResAllQuads[iRich] ) {
          if ( h.get() ) {
            if ( checkCKThetaStats( h.get() ) ) {
              const auto fitr_q = fitCKThetaHistogram( h.get(), rad, true );
              if ( !fitr_q.fitOK ) {
                calib_message( MSG::WARNING, "Quadrant fit for '", h.get()->GetTitle(), "' FAILED" );
              }
            }
          }
        }
      }

      // Update cached scale factor for 'last' run if a final fit and OK
      if ( hasNewScaleF && isFinal ) { updateRefIndexSF( rad, scale.value ); }

    } else {
      calib_message( MSG::WARNING, "Failed to load histogram for '", rad, "'" );
    }

  } // Loop over radiators

  if ( canvasIsOpen ) { printCanvas( "]" ); }

  if ( ok ) {
    // save this run in the processed list with number of events used.
    m_processedRuns[runNumber()].used = m_nEventsThisRun;
  }

  calib_message( MSG::INFO, "Finished ", type, " n-1 calibration for run ", runNumber() );

  // reset various counters for the next run
  resetCalibCounters();
  if ( isFinal ) { resetHistograms(); }

  // Make sure we finally remove any canvas created
  deleteCanvas();
}

//=============================================================================

std::pair<double, std::time_t> RefIndexCalib::getLastRefIndexSF( const std::string& rad ) const {

  std::pair<double, std::time_t> scale_time{ 0, 0 };
  auto&                          scale      = scale_time.first;
  auto&                          write_time = scale_time.second;
  unsigned int                   run        = 0;

  const auto subDet = radToTaskName( rad );
  if ( !subDet.empty() ) {
    const boost::filesystem::path dir( m_summaryPath + "/" + subDet );
    const boost::filesystem::path filename( m_RefIndexSFLog.value() );
    const boost::filesystem::path full_path = dir / filename;
    if ( !boost::filesystem::exists( full_path ) ) {
      // first time write new file
      try {
        auto ok = createDir( dir );
        if ( ok ) {
          std::ofstream logging( full_path.string().c_str() );
          ok = logging.is_open();
          if ( ok ) {
            logging << run << " " << std::setprecision( m_Precision ) << scale << "\n";
            logging.close();
            // set write time to 'now'
            write_time = time( nullptr );
          } else {
            calib_message( MSG::ERROR, "Failed to open ", full_path );
          }
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to create file ", full_path, " ", expt.what() );
      }

    } else {
      // open existing file

      // Get last write time
      write_time = boost::filesystem::last_write_time( full_path );

      std::fstream file( full_path.string().c_str(), std::ios::in );
      if ( file.is_open() ) {
        file >> run >> scale;
        file.close();
      } else {
        calib_message( MSG::ERROR, "Failed to open ", full_path );
      }

      calib_message( MSG::DEBUG, "Read ", rad, " scale factor (", scale, ") from previous run cache" );
    }
  }

  return scale_time;
}

//=============================================================================

void RefIndexCalib::updateRefIndexSF( const std::string& rad, //
                                      const double       scale ) const {

  if ( m_okToPublish ) {
    const auto subDet = radToTaskName( rad );
    bool       OK     = !subDet.empty();
    if ( OK ) {
      const boost::filesystem::path dir( m_summaryPath + "/" + subDet );
      const boost::filesystem::path filename( m_RefIndexSFLog.value() );
      const boost::filesystem::path full_path = dir / filename;
      try {
        OK = createDir( dir );
        if ( OK ) {
          std::ofstream ofile( full_path.string().c_str() );
          OK = ofile.is_open();
          if ( OK ) {
            ofile << runNumber() << " " << std::setprecision( m_Precision ) << scale << "\n";
            ofile.close();
            calib_message( MSG::DEBUG, "Cached ", rad, " scale factor ", scale, " for run ", runNumber() );
          } else {
            calib_message( MSG::ERROR, "Failed to open ", full_path );
          }
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to open file ", full_path, " ", expt.what() );
        OK = false;
      }
    }
    if ( !OK ) { calib_message( MSG::WARNING, "Failed to update ", rad, " ref index scale" ); }
  }
}

//=============================================================================

bool RefIndexCalib::ymlWriter( const std::string& type,  //
                               const double       scale, //
                               const std::string& rad ) const {

  // Only write YML if we are going to publish the result
  if ( !m_okToPublish ) { return false; }

  // Get version
  const auto version = setVersion( rad );
  bool       OK      = version.first;
  if ( !OK ) { return OK; }
  calib_message( MSG::DEBUG, "YML Writing for run ", runNumber(), " ", rad, " Version ", version.second );
  const auto versionS = std::to_string( version.second );

  const bool isR1             = ( "Rich1Gas" == rad );
  const bool isR2             = ( "Rich2Gas" == rad );
  const bool isFinal          = ( type == "Final EOR" || type == "Final" );
  bool       scaleIsDifferent = false;

  std::string subDet;
  if ( isR1 ) {
    subDet           = "Rich1";
    m_Rich1PubString = std::to_string( runNumber() ) + " v" + versionS;
    scaleIsDifferent = m_Rich1RefIndex != scale;
    m_Rich1RefIndex  = scale;
  } else if ( isR2 ) {
    subDet           = "Rich2";
    m_Rich2PubString = std::to_string( runNumber() ) + " v" + versionS;
    scaleIsDifferent = m_Rich2RefIndex != scale;
    m_Rich2RefIndex  = scale;
  } else {
    calib_message( MSG::WARNING, "Unknown radiator '", rad, "'" );
    return false;
  }

  const boost::filesystem::path conditions_dir( m_conditionsDbPath + "/lhcb-conditions-database/Conditions/" + subDet +
                                                "/Environment/Gas.yml" );
  const boost::filesystem::path pool_dir = conditions_dir / ".pool";
  const boost::filesystem::path conditions_file( "v" + versionS );
  const boost::filesystem::path full_conditions_path = pool_dir / conditions_file;

  calib_message( MSG::VERBOSE, "Writing conditions to ", full_conditions_path );

  removeFile( full_conditions_path );

  try {
    OK = createDir( pool_dir );
    if ( OK ) {
      std::ofstream logging( full_conditions_path.string().c_str() );
      OK = logging.is_open();
      if ( OK ) {
        logging << "RefractivityScaleFactor:\n";
        logging << "  CurrentScaleFactor: " << std::setprecision( m_Precision ) << scale << "\n";
        logging.close();
        calib_message( MSG::INFO, "Successfully wrote ", rad, " Scale factor to ", full_conditions_path );
        if ( m_makeDBRunFile ) {
          const boost::filesystem::path run_db_file = conditions_dir / std::to_string( runNumber() );
          removeFile( run_db_file );
          copyFile( full_conditions_path, run_db_file );
        }
      } else {
        calib_message( MSG::ERROR, "Failed to open ", full_conditions_path );
      }
    }
  } catch ( const boost::filesystem::filesystem_error& expt ) {
    calib_message( MSG::ERROR, "Failed to open file ", full_conditions_path, " ", expt.what() );
    OK = false;
  }

  // Publish to DIM
  if ( OK ) {
    if ( m_pPublishSvc ) {
      if ( isR1 ) {
        calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich1TaskName.value(), " = '", m_Rich1PubString, "'" );
        m_pPublishSvc->updateItem( m_Rich1TaskName.value() );
        if ( isFinal && scaleIsDifferent ) {
          calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich1ScaleTaskName.value(), " = ", m_Rich1RefIndex );
          m_pPublishSvc->updateItem( m_Rich1ScaleTaskName.value() );
        }
      } else if ( isR2 ) {
        calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich2TaskName.value(), " = '", m_Rich2PubString, "'" );
        m_pPublishSvc->updateItem( m_Rich2TaskName.value() );
        if ( isFinal && scaleIsDifferent ) {
          calib_message( MSG::INFO, "Publishing to DIM : ", m_Rich2ScaleTaskName.value(), " = ", m_Rich2RefIndex );
          m_pPublishSvc->updateItem( m_Rich2ScaleTaskName.value() );
        }
      }
    }
    // update DIM summary file
    std::ostringstream dimmess;
    dimmess << "Run " << runNumber() << " |";
    if ( isR1 ) { dimmess << " " << m_Rich1TaskName.value() << " '" << m_Rich1PubString << "'"; }
    if ( isR2 ) { dimmess << " " << m_Rich2TaskName.value() << " '" << m_Rich2PubString << "'"; }
    OK = writeToDimSummaryFile( dimmess.str() );
  } else {
    calib_message( MSG::ERROR, rad, " YML conditions NOT published for Run ", runNumber() );
  }

  return OK;
}

//=============================================================================

bool RefIndexCalib::writeLogs( const std::string& rad, //
                               const FitResult&   fitResult ) const {

  // Path to the summary file
  const boost::filesystem::path dir( m_summaryPath + "/RichCalibSummaries/" );
  const boost::filesystem::path filename( rad + m_runByrunLog );
  const boost::filesystem::path full_path = dir / filename;

  bool ok = createDir( dir );
  if ( ok ) {

    // First time ?
    const bool firstTime = !boost::filesystem::exists( full_path );

    // open the file in append mode
    std::ofstream file( full_path.string().c_str(), std::ios_base::app );
    ok = file.is_open();
    if ( ok ) {

      // Add column headers if first time
      if ( firstTime ) {
        file << "Run DateTime CKThetaRes CKThetaResErr ScaleFactor ScaleFactorErr NSignal NBackground" << std::endl;
      }

      // get DateTime string
      const auto dateTime = getDateTimeString();

      // get the scale parameter
      const auto scale = nScaleFromShift( fitResult.ckShift, fitResult.ckShiftErr, rad, true );

      // get S and B
      const auto& sigF = fitResult.signalFitFunc;
      const auto& bckF = fitResult.bkgFitFunc;
      const auto  x    = fitResult.ckShift;
      const auto  S    = ( sigF.get() ? sigF->Eval( x ) : 0.0 );
      const auto  B    = ( bckF.get() ? bckF->Eval( x ) : 0.0 );

      // write the data for this run
      file << runNumber() << " " << dateTime << " " << fitResult.ckResolution << " " << fitResult.ckResolutionErr //
           << " " << scale.value << " " << scale.error << " " << S << " " << B << std::endl;

      // close the file
      file.close();

    } else {
      calib_message( MSG::WARNING, "Problem writing run by run summary file ", full_path );
    }
  }

  return ok;
}

//=============================================================================

std::pair<bool, unsigned long long> //
RefIndexCalib::setVersion( const std::string& rad ) const {

  // Do we already have this run in the map
  // if so we reuse the conditions version to avoid creating too many files
  const auto iV = m_runCalibVersions[rad].find( runNumber() );
  if ( iV != m_runCalibVersions[rad].end() ) {
    calib_message( MSG::INFO, "Re-using ", rad, " conditions version ", iV->second, " for Run ", runNumber() );
    return std::make_pair( true, iV->second );
  }

  unsigned long long version = 0;

  // Test the root directory is accessible
  bool OK = createDir( m_summaryPath.value() );
  if ( !OK ) {
    calib_message( MSG::ERROR, "Cannot access alignment directory ", m_summaryPath );
  } else {

    const auto subDet = radToTaskName( rad );
    OK                = !subDet.empty();
    if ( OK ) {

      const boost::filesystem::path dir( m_summaryPath + "/" + subDet );
      const boost::filesystem::path filename( m_ymlVersionLog.value() );
      const boost::filesystem::path full_path = dir / filename;

      unsigned int run( 0 );

      calib_message( MSG::VERBOSE, "Version file: ", full_path.string() );

      if ( !boost::filesystem::exists( full_path ) ) {
        // First time file is created
        try {
          version = 0;
          OK      = createDir( dir );
          if ( OK ) {
            std::ofstream logging( full_path.string().c_str() );
            OK = logging.is_open();
            if ( OK ) {
              logging << runNumber() << " " << version << "\n";
              logging.close();
            } else {
              calib_message( MSG::ERROR, "Failed to open ", full_path );
            }
          }
        } catch ( const boost::filesystem::filesystem_error& expt ) {
          calib_message( MSG::ERROR, "Failed to open file ", full_path, " ", expt.what() );
          OK = false;
        }

      } else {
        // reuse existing file
        std::fstream file( full_path.string().c_str(), std::ios::in | std::ios::out );
        if ( file.is_open() ) {
          std::istringstream lastLine( getLastLine( file ) );

          lastLine >> run >> version;

          if ( runNumber() > run ) {
            // increase version number and add to cache
            ++version;
            file << runNumber() << " " << version << "\n";
          }

          file.close();
        } else {
          OK = false;
          calib_message( MSG::ERROR, "Failed to open ", full_path );
        }
      }
    }

    // Save in map
    calib_message( MSG::INFO, "Setting ", rad, " conditions version ", version, " for Run ", runNumber() );
    m_runCalibVersions[rad][runNumber()] = version;
  }

  return std::make_pair( OK, version );
}

//=============================================================================
// Fitting histogram
//=============================================================================
RefIndexCalib::FitResult //
RefIndexCalib::fitCKThetaHistogram( TH1* hist, const std::string& rad, bool silent ) const {

  silent &= !msgLevel( MSG::DEBUG );

  RefIndexCalib::FitResult fitRes;

  if ( !hist ) { return fitRes; }

  if ( !silent ) { calib_message( MSG::INFO, "Starting CK theta fit for '", hist->GetTitle(), "'" ); }

  const auto irad  = ( "Rich1Gas" == rad ? Rich::Rich1Gas : Rich::Rich2Gas );
  const auto irich = ( "Rich1Gas" == rad ? Rich::Rich1 : Rich::Rich2 );

  // Draw to canvas before fit
  hist->Draw( "e" );

  // Do the fit
  try {
    fitRes = m_ckFitter.fit( *hist, irad );
  } catch ( const std::exception& excpt ) {
    calib_message( MSG::ERROR, "std::exception whilst performing CK theta fit '", excpt.what(), "'" );
    fitRes = RefIndexCalib::FitResult{};
  }

  // shortcuts to the results
  auto& fitOK    = fitRes.fitOK;
  auto& bestFitF = fitRes.overallFitFunc;
  auto& backFunc = fitRes.bkgFitFunc;
  auto& sigFunc  = fitRes.signalFitFunc;
  if ( !bestFitF.get() || !backFunc.get() || !sigFunc.get() ) {
    calib_message( MSG::ERROR, "NULL function pointer in fit result" );
    fitRes = RefIndexCalib::FitResult{};
  }

  // where multi attempts used by the fitter ?
  if ( fitRes.fitAttempts > 1 ) {
    calib_message( MSG::WARNING, "CK theta fitter used multiple (", fitRes.fitAttempts, ") fit attempts" );
  }

  // Sanity check on fit results
  fitOK &= fitSanityCheck( irich, fitRes );

  // randomly fail fits for testing
  // fitOK &= ( int( bestFitF.GetParameter( 0 ) ) % 2 == 0 );

  // print parameters
  try {
    if ( fitOK ) {
      if ( !silent ) {
        // Print fitted parameters
        calib_message( MSG::INFO, rad, " fit SUCCESSFUL :-" );
        for ( int i = 0; i < bestFitF->GetNpar(); ++i ) {
          const auto name  = bestFitF->GetParName( i );
          const auto param = bestFitF->GetParameter( i );
          const auto err   = bestFitF->GetParError( i );
          calib_message( MSG::INFO, boost::format( "  %s %|15t|= %+7.2e +- %7.2e" ) % name % param % err );
        }
      }
    } else {
      calib_message( MSG::WARNING, rad, " fit FAILED" );
    }
  } catch ( const std::exception& excpt ) {
    calib_message( MSG::ERROR, "std::exception whilst printing fit parameters '", excpt.what(), "'" );
  }

  if ( !fitOK ) { return fitRes; }

  // Do we need to draw the results ?
  try {
    if ( m_createPDFsummary ) {

      // make the pads for the plots
      canvas()->cd();
      auto upPad = std::make_unique<TPad>( "U", "U", 0., 0.25, 1., 1. );
      auto dnPad = std::make_unique<TPad>( "L", "L", 0., 0., 1., 0.25 );
      // upPad->SetTopMargin(0.05);
      upPad->SetBottomMargin( 0.1 );
      dnPad->SetTopMargin( 0.05 );
      dnPad->SetBottomMargin( 0.22 );
      upPad->Draw();
      dnPad->Draw();

      // Draw the histogram
      canvas()->cd();
      upPad->cd();
      hist->Draw( "e" );

      // Draw vertical line at x=0
      canvas()->Update();
      auto line = std::make_unique<TLine>( 0, upPad->GetUymin(), 0, upPad->GetUymax() );
      line->SetLineStyle( 3 );
      line->Draw();

      // Add fitted functions to PDFs
      if ( fitOK ) {

        // Draw full fit
        bestFitF->SetLineColor( kYellow + 3 );
        bestFitF->Draw( "SAME" );

        // Add scale factor
        const auto old_scale = m_scaleFs[irich];
        const auto new_scale = nScaleFromShift( bestFitF->GetParameter( 1 ), bestFitF->GetParError( 1 ), rad, true );
        TText      text;
        text.SetNDC();
        text.SetTextSize( 0.025 );
        const auto runS = "Run " + std::to_string( runNumber() );
        const auto sfS  = "SF  " + std::to_string( old_scale.value ) + " -> " + std::to_string( new_scale.value );
        text.DrawText( 0.14, 0.840, runS.c_str() );
        text.DrawText( 0.14, 0.814, sfS.c_str() );

        // Draw background only fit
        backFunc->SetLineColor( kRed + 3 );
        backFunc->Draw( "SAME" );

        // Draw signal only fit
        upPad->cd();
        auto overlay = std::make_unique<TPad>( "overlay", "overlay", 0, 0, 1, 1 );
        overlay->SetFillStyle( 4000 );
        overlay->SetFillColor( 0 );
        overlay->SetFrameFillStyle( 4000 );
        overlay->Draw();
        overlay->cd();
        sigFunc->SetMaximum( 1.5 * sigFunc->Eval( fitRes.ckShift ) );
        sigFunc->SetLineColor( kBlue + 2 );
        sigFunc->Draw( "AL" );

        // Draw the pull
        auto pull = makePullPlot( *hist, *bestFitF.get() );
        canvas()->cd();
        dnPad->cd();
        pull->Draw( "e" );

        // print canvas to file
        // note needs to be in this scope as otherwise not everything is included
        printCanvas();

      } else {
        // print just the histogram
        printCanvas();
      }

    } // create PDF
  } catch ( const std::exception& excpt ) {
    calib_message( MSG::ERROR, "std::exception creating PDF '", excpt.what(), "'" );
  }

  // return final fit parameter for shift
  if ( !silent ) { calib_message( MSG::DEBUG, "CK theta fit complete for ", rad ); }
  return fitRes;
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RefIndexCalib )

//-----------------------------------------------------------------------------

/*****************************************************************************\
* (c) Copyright 2000-2025 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// boost
#include "boost/algorithm/string.hpp"
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

// ROOT
#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1.h>
#include <TLine.h>
#include <TMath.h>
#include <TPad.h>
#include <TSystem.h>
#include <TText.h>

// local
#include "rootstyle.h"

// STL
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <ctime>
#include <deque>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <sstream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

namespace Rich::Calib::Utils {

  template <typename TYPE>
  using RunByRunHists = std::deque<TYPE>;

  /// Type for value+-error
  template <typename TYPE>
  struct ValueWithError {
    TYPE value{ 1 };
    TYPE error{ 0 };

    friend inline std::ostream& operator<<( std::ostream& os, const ValueWithError& ve ) {
      os << "[ " << boost::format( "%10.5e" ) % ve.value;
      if ( ve.error > 0 ) { os << " +- " << boost::format( "%8.3e" ) % ve.error; }
      return os << " ]";
    }
  };

  struct RunStats {
    unsigned long long used{ 0 }; ///< #Events used for calibration
    unsigned long long late{ 0 }; ///< #Events that arrived late
  };

  /// Base class for calibration algorithms
  class CalibAlg : public Rich::Future::Rec::HistoAlgBase {

  public:
    // Inherit base constructors
    using HistoAlgBase::HistoAlgBase;

    /// Initialise
    StatusCode initialize() override;

  protected:
    // data

    std::string m_taskType{ "UNDEFINED" };

    /// Current TCanvas for printing
    mutable std::unique_ptr<TCanvas> m_canvas;

    /// Run number for last processed run
    mutable unsigned int m_runNumber{ 0 };

    /// PDF file for each run
    mutable std::map<unsigned int, std::string> m_pdfFile;

    /// The time the last calibration was run
    mutable time_t m_timeLastCalib{ 0 };

    /// The time the current ongoing calibration started
    mutable time_t m_timeCurrentCalibStart{ 0 };

    /// Total overall number of events seen
    mutable unsigned long long m_nEvts{ 0 };

    /// Number of events seen since last calibration was run...
    mutable unsigned long long m_nEventsSinceCalib{ 0 };

    /// Total number of events in current run
    mutable unsigned long long m_nEventsThisRun{ 0 };

    /** List of all previously calibrated runs for the current process
     *  with the number of events used for that calibration */
    mutable std::map<unsigned int, RunStats> m_processedRuns;

    /// Last run number for 'extra histograms'
    mutable std::optional<unsigned int> m_lastRunExtraHists{};

  protected:
    // properties

    /// minimum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_minBeta{ this, "MinBeta", { 0.9999f, 0.9999f } };

    /// maximum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_maxBeta{ this, "MaxBeta", { 999.99f, 999.99f } };

    /// Overall Min monentum cut
    Gaudi::Property<DetectorArray<double>> m_minP{
        this, "MinP", { 0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV }, "Minimum momentum (GeV/c)" };

    /// Overall Max monentum cut
    Gaudi::Property<DetectorArray<double>> m_maxP{
        this, "MaxP", { 9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV }, "Maximum momentum (GeV/c)" };

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<DetectorArray<float>> m_ckResRange{ this, "CKResHistoRange", { 0.0055f, 0.0035f } };

    /// Min theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMin{ this, "ChThetaRecHistoLimitMin", { 0.040f, 0.020f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMax{ this, "ChThetaRecHistoLimitMax", { 0.065f, 0.035f } };

    /// Is it is OK to publish calibrations
    Gaudi::Property<bool> m_okToPublish{ this, "OKToPublish", true };

    /// Task log file name
    Gaudi::Property<std::string> m_loggerFile{ this, "LoggerFile", "Task.log" };

    /// Summary file path
    Gaudi::Property<std::string> m_summaryPath{ this, "summaryPath", "/group/online/alignment" };

    /// Min entries for a fit
    Gaudi::Property<unsigned int> m_minEntries{ this, "minEntries", 100000 };

    /// Flag to turn on/off the saving of summary PDF files
    Gaudi::Property<bool> m_createPDFsummary{ this, "CreatePDFSummary", true };

    /// Max run number history
    Gaudi::Property<unsigned int> m_runNumHistSize{ this, "RunNumHistSize", 100 };

    /// Detailed run-by-run debugging histos
    Gaudi::Property<bool> m_enableRunByRunHists{ this, "RunByRunHists", false };

    // Log file from run by run summary
    Gaudi::Property<std::string> m_runByrunLog{ this, "RunByRunSummary", "RunByRunSummary.txt" };

    /// The radiators to calibration
    Gaudi::Property<std::vector<std::string>> m_rads{ this, "RichRads", { "Rich1Gas", "Rich2Gas" } };

  protected:
    /// End of calibration reset
    void resetCalibCounters() const {
      calib_message( MSG::VERBOSE, "Resetting counters" );
      m_nEventsSinceCalib = 0;               // reset event count for next time
      m_runNumber         = 0;               // correctly determine the next run number
      m_timeLastCalib     = time( nullptr ); // reset the last calibration time to now
    }

    /// Get the current run number
    inline auto runNumber() const noexcept { return m_runNumber; }

    //=============================================================================
    /// Function to check whether enough entries
    //=============================================================================
    inline bool checkCKThetaStats( const TH1* hist ) const {
      bool OK = false;
      if ( !hist ) {
        calib_message( MSG::WARNING, "checkCKThetaStats: NULL histogram !" );
      } else {
        OK = ( hist->GetEntries() >= m_minEntries );
        if ( !OK ) {
          calib_message( MSG::DEBUG, "CK theta histogram '", hist->GetTitle(), "' does not have enough stats for fit (",
                         hist->GetEntries(), "<", m_minEntries.value(), ")" );
        }
      }
      return OK;
    }

    /// Create directory
    template <class DIR>
    inline bool createDir( const DIR& dir, const bool errorOnFail = true ) const {
      bool ok = true;
      try {
        if ( !boost::filesystem::exists( dir ) ) {
          boost::filesystem::create_directories( dir );
          calib_message( MSG::DEBUG, "Created ", dir );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        if ( errorOnFail ) { calib_message( MSG::ERROR, "FAILED to create directory ", dir, " '", expt.what(), "'" ); }
        ok = false;
      }
      return ok;
    }

    /// get DateTime string
    inline auto getDateTimeString( const char* fmt = "%Y%m%d:%H%M%S" ) const {
      const std::time_t t = std::time( nullptr );
      char              mbstr[100];
      return std::string{ std::strftime( mbstr, sizeof( mbstr ), fmt, std::localtime( &t ) ) ? mbstr : "" };
    }

    /// Get year/month/day as a string
    inline auto getDateString() const { return getDateTimeString( "%Y/%m/%d" ); }

    /// Get watermark string
    inline auto watermark() const { return getDateTimeString( "%c" ); }

    /// Get the path to write summaries to
    inline auto summaryPath() const {
      const std::string dir( m_summaryPath + "/RichCalibSummaries/" + getDateString() + "/" + m_taskType + "/" );
      createDir( dir );
      return dir;
    }

    /// logger file location
    inline std::string loggerFile() const { return summaryPath() + m_loggerFile; }

    bool writeToLoggerFile( const MSG::Level level, const std::string& mess ) const;

    template <typename... Args>
    inline void calib_message( const MSG::Level level, Args&&... args ) const {
      if ( msgLevel( level ) ) {
        std::ostringstream mess;
        ( mess << ... << args );
        msgStream( level ) << mess.str() << endmsg;
        // Only write to log file if publishing enabled.
        if ( m_okToPublish ) { writeToLoggerFile( level, mess.str() ); }
      }
    }

    //=============================================================================
    /// Functions to get the last line
    //=============================================================================
    inline std::istream& ignoreline( std::fstream&           in, //
                                     std::fstream::pos_type& pos ) const {
      pos = in.tellg();
      return in.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
    }

    //=============================================================================
    /// Get the last line from an input file stream
    //=============================================================================
    inline auto getLastLine( std::fstream& in ) const {
      std::fstream::pos_type pos = in.tellg();
      std::fstream::pos_type lastPos;
      while ( in >> std::ws && ignoreline( in, lastPos ) ) { pos = lastPos; }
      in.clear();
      in.seekg( pos );
      std::string line;
      std::getline( in, line );
      return line;
    }

    /// Fix file permissions
    inline void fixFilePermissions() const {
      using namespace boost::filesystem;
      const path                   dir( m_summaryPath + "/RichCalibSummaries" );
      recursive_directory_iterator end_itr;
      for ( recursive_directory_iterator i( dir ); i != end_itr; ++i ) { setPerms( i->path() ); }
    }

    /// Set file permissions on a file
    template <class FILE>
    inline void setPerms( const FILE& file ) const {
      try {
        if ( boost::filesystem::exists( file ) ) {
          calib_message( MSG::VERBOSE, "Setting permissions for ", file );
          using namespace boost::filesystem;
          permissions( file, add_perms | owner_write | group_write );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::WARNING, "Failed to set permissions for ", file, " ", expt.what() );
      }
    }

    /// Remove a file
    template <class FILE>
    inline bool removeFile( const FILE& file ) const {
      bool ok = true;
      try {
        if ( boost::filesystem::exists( file ) ) {
          calib_message( MSG::DEBUG, "Removing ", file );
          boost::filesystem::remove( file );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to delete file ", file, " ", expt.what() );
        ok = false;
      }
      return ok;
    }

    /// Copy a file
    template <class FILEFROM, class FILETO>
    inline bool copyFile( const FILEFROM& from, const FILETO& to ) const {
      bool ok = true;
      try {
        ok = boost::filesystem::exists( from );
        if ( ok ) {
          calib_message( MSG::DEBUG, "Copying ", from, " -> ", to );
          ok = removeFile( to );
          if ( ok ) { boost::filesystem::copy_file( from, to ); }
        } else {
          calib_message( MSG::ERROR, "Failed to copy file ", from, " to ", to, " as source file does not exist" );
        }
      } catch ( const boost::filesystem::filesystem_error& expt ) {
        calib_message( MSG::ERROR, "Failed to copy file ", from, " to ", to, " ", expt.what() );
        ok = false;
      }
      return ok;
    }

    /// Print a divider
    inline void divider() const noexcept {
      calib_message( MSG::INFO, "=======================================================",
                     "===============================================================" );
    }

    /// Access and create on demand the current TCanvas
    inline auto canvas() const {
      if ( !m_canvas.get() ) {
        const std::string name = m_taskType + "Canvas";
        calib_message( MSG::VERBOSE, "Creating TCanvas '", name, "'" );
        m_canvas = std::make_unique<TCanvas>( name.c_str(), name.c_str(), 1200, 1200 );
      }
      return m_canvas.get();
    }

    /// Delete the current TCanvas
    inline void deleteCanvas() const {
      if ( m_canvas.get() ) { calib_message( MSG::VERBOSE, "Deleting TCanvas '", m_canvas->GetName(), "'" ); }
      m_canvas.reset( nullptr );
    }

    /// Print Canvas
    void printCanvas( const std::string& tag = "" ) const;

    //=============================================================================
    // Make a pull plot from a histogram and a given function
    //=============================================================================
    std::unique_ptr<TH1D> makePullPlot( TH1& hist, TF1& func ) const;

    /// Fill the 'last' run by run histogram
    template <typename TYPE, typename... Args>
    inline void fillRunByRunHist( RunByRunHists<TYPE>& hists, Args&&... args ) const {
      assert( !hists.empty() );
      if ( !hists.empty() ) {
        // Always fill the last histogram added to the container
        ++hists.back()[{ std::forward<Args>( args )... }];
      }
    }
  };

}; // namespace Rich::Calib::Utils

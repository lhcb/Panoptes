/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichRecUtils/RichDetParams.h"
#include "RichUtils/RichDAQDefinitions.h"

// ROOT
#include <ROOT/TBufferMerger.hxx>
#include <TFile.h>
#include <TROOT.h>
#include <TString.h>
#include <TTree.h>

// Rich DAQ
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// event
#include "Event/ODIN.h"

// Boost
#include "boost/algorithm/string.hpp"
#include <boost/filesystem.hpp>

// STD
#include <array>
#include <cmath>
#include <cstdint>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

namespace Rich::Future::Mon {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class HitMaps HitMaps.h
   *
   *  Produces a data for the RICH and ODIN data.
   *
   *  @author Chris Jones
   *  @date   2021-05-20
   */

  class DataTuple final : public Consumer<void( const EventContext&,                                      //
                                                const LHCb::ODIN&,                                        //
                                                const DAQ::DecodedData&,                                  //
                                                const Rich::Utils::RichSmartIDs&,                         //
                                                const DAQ::Tel40CableMapping&,                            //
                                                const DAQ::PDMDBEncodeMapping& ),                         //
                                          LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase,              //
                                                                               Rich::Utils::RichSmartIDs, //
                                                                               DAQ::Tel40CableMapping,    //
                                                                               DAQ::PDMDBEncodeMapping>> {

  private:
    /// get output file name
    auto tupleFileName() const { return Form( "%sRichDataTuple_%u.root", m_path.value().c_str(), m_number.value() ); }

  public:
    /// Standard constructor
    DataTuple( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                      KeyValue{ "DecodedDataLocation", DAQ::DecodedDataLocation::Default },
                      // input conditions data
                      KeyValue{ "RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey },
                      KeyValue{ "Tel40CableMapping", DAQ::Tel40CableMapping::DefaultConditionKey + "-" + name },
                      KeyValue{ "PDMDBEncodeMapping", DAQ::PDMDBEncodeMapping::DefaultConditionKey + "-" + name } } ) {}

    /// Initialize
    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        StatusCode sc = StatusCode::SUCCESS;

        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
        DAQ::Tel40CableMapping::addConditionDerivation( this );
        DAQ::PDMDBEncodeMapping::addConditionDerivation( this );

        // Ntuple
        if ( !boost::filesystem::exists( m_path.value() ) ) {
          warning() << m_path << " does not exist. Will write to current directory instead" << endmsg;
          m_path = "";
        }
        m_file = std::unique_ptr<TFile>( TFile::Open( tupleFileName(), "RECREATE" ) );
        if ( !m_file->IsOpen() ) {
          sc = StatusCode::FAILURE;
        } else {
          m_tree = std::make_unique<TTree>( "RichData", "RichData" );
          m_tree->Branch( "nHits", &m_nHits );
          m_tree->Branch( "nHitsRich1", &m_nHitsRich[Rich::Rich1] );
          m_tree->Branch( "nHitsRich2", &m_nHitsRich[Rich::Rich2] );
          m_tree->Branch( "rich", &m_rich );
          m_tree->Branch( "side", &m_side );
          m_tree->Branch( "col", &m_col );
          m_tree->Branch( "pdm", &m_pdm );
          m_tree->Branch( "ec", &m_ec );
          m_tree->Branch( "pmt", &m_pmt );
          m_tree->Branch( "anode", &m_anode );
          m_tree->Branch( "locPixX", &m_localPixX );
          m_tree->Branch( "locPixY", &m_localPixY );
          m_tree->Branch( "gloPixX", &m_globalPixX );
          m_tree->Branch( "gloPixX2", &m_globalPixX2 );
          m_tree->Branch( "gloPixY", &m_globalPixY );
          m_tree->Branch( "spaceLocX", &m_spaceLocX );
          m_tree->Branch( "spaceLocY", &m_spaceLocY );
          m_tree->Branch( "spaceGloX", &m_spaceGloX );
          m_tree->Branch( "spaceGloY", &m_spaceGloY );
          m_tree->Branch( "spaceGloZ", &m_spaceGloZ );
          m_tree->Branch( "tell40bit", &m_tell40bit );
          m_tree->Branch( "evID", &m_evID, "evID/l" );
          m_tree->Branch( "stepID", &m_stepID, "stepID/i" );
          m_tree->Branch( "taeWindow", &m_taeWindow, "taeWindow/i" );
          m_tree->Branch( "taeCentral", &m_taeCentral, "taeCentral/i" );
          m_tree->Branch( "taeFirst", &m_taeFirst, "taeFirst/i" );
          m_tree->Branch( "thDAC_ID", &m_thDAC_ID, "thDAC_ID/i" );
          m_tree->Branch( "nBunch", &m_nBunch, "nBunch/i" );
          m_tree->Branch( "slotID", &m_slotID, "slotID/i" );
          m_tree->Branch( "triggerType", &m_triggerType, "triggerType/i" );
          m_tree->Branch( "eventType", &m_eventType, "eventType/i" );
          m_tree->Branch( "bxidType", &m_bxidType, "bxidType/i" );
          m_start_scan = 0;
        }

        return sc;
      } );
    }

    /// Finalize
    StatusCode finalize() override {
      info() << "Writing DataTuple: " << tupleFileName() << endmsg;
      m_file->Write();
      m_file->Close();
      // following needed only because of ROOTs bad memory management
      m_tree.release();
      m_file.release();
      // delete m_file.get();
      // m_file.reset();
      return Consumer::finalize();
    }

  private:
    /// ROOT file
    std::unique_ptr<TFile> m_file;

    /// TTree
    std::unique_ptr<TTree> m_tree;

    /// mutex lock
    mutable std::mutex m_updateLock;

    // Tuple data
    mutable std::uint32_t                      m_nHits{ 0 };
    mutable Rich::DetectorArray<std::uint32_t> m_nHitsRich{ {} };
    mutable std::vector<std::uint16_t>         m_rich;
    mutable std::vector<std::uint16_t>         m_side;
    mutable std::vector<std::uint16_t>         m_col;
    mutable std::vector<std::uint16_t>         m_pdm;
    mutable std::vector<std::uint16_t>         m_ec;
    mutable std::vector<std::uint16_t>         m_pmt;
    mutable std::vector<std::uint16_t>         m_anode;
    mutable std::vector<std::uint16_t>         m_localPixX;
    mutable std::vector<std::uint16_t>         m_localPixY;
    mutable std::vector<std::int32_t>          m_globalPixX;
    mutable std::vector<std::int32_t>          m_globalPixX2;
    mutable std::vector<std::int32_t>          m_globalPixY;
    mutable std::vector<float>                 m_spaceLocX;
    mutable std::vector<float>                 m_spaceLocY;
    mutable std::vector<float>                 m_spaceGloX;
    mutable std::vector<float>                 m_spaceGloY;
    mutable std::vector<float>                 m_spaceGloZ;
    mutable std::vector<std::uint32_t>         m_tell40bit;
    mutable unsigned long long                 m_evID{ 0 };
    mutable unsigned int                       m_stepID{ 0 };
    mutable unsigned int                       m_thDAC_ID{ 0 };
    mutable unsigned int                       m_nBunch{ 0 };
    mutable unsigned int                       m_slotID{ 0 };
    mutable unsigned int                       m_taeWindow{ 0 };
    mutable unsigned int                       m_taeCentral{ 0 };
    mutable unsigned int                       m_taeFirst{ 0 };
    unsigned int                               m_start_scan{ 0 };
    unsigned int                               m_stop_scan{ 0 };
    mutable unsigned int                       m_eventType{ 0 };
    mutable unsigned int                       m_triggerType{ 0 };
    mutable unsigned int                       m_bxidType{ 0 };

    Gaudi::Property<std::string>  m_path{ this, "OutputPath", "/hist/RICHMon/" };
    Gaudi::Property<unsigned int> m_number{ this, "RunNumber", 0 };

  public:
    /// Functional operator
    void operator()( const EventContext& /*    evtCtx*/,              //
                     const LHCb::ODIN&                odin,           //
                     const DAQ::DecodedData&          data,           //
                     const Rich::Utils::RichSmartIDs& smartIDsHelper, //
                     const DAQ::Tel40CableMapping&    tel40Maps,      //
                     const DAQ::PDMDBEncodeMapping&   pdmdbMaps ) const override {

      std::lock_guard lock( m_updateLock );

      m_nHits     = 0;
      m_nHitsRich = { 0, 0 };
      m_rich.clear();
      m_side.clear();
      m_col.clear();
      m_pdm.clear();
      m_ec.clear();
      m_pmt.clear();
      m_anode.clear();
      m_localPixX.clear();
      m_localPixY.clear();
      m_globalPixX.clear();
      m_globalPixX2.clear();
      m_globalPixY.clear();
      m_spaceLocX.clear();
      m_spaceLocY.clear();
      m_spaceGloX.clear();
      m_spaceGloY.clear();
      m_spaceGloZ.clear();
      m_tell40bit.clear();

      // info() << "ODIN " << odin << endmsg;
      m_evID        = odin.eventNumber();
      m_eventType   = odin.eventType();
      m_triggerType = odin.triggerType();
      m_bxidType    = (unsigned short int)odin.bunchCrossingType();
      if ( odin.timeAlignmentEventFirst() || !odin.isTAE() ) {
        m_slotID = 0;
      } else {
        ++m_slotID;
      }

      //// Get bxID
      m_nBunch     = odin.bunchId();
      m_taeCentral = odin.timeAlignmentEventCentral();
      m_taeFirst   = odin.timeAlignmentEventFirst();
      m_taeWindow  = odin.timeAlignmentEventIndex();
      m_slotID     = std::abs( (int)m_nBunch - 1024 ) % 23;
      //// Get value of stepID
      m_stepID = odin.calibrationStep();

      // Get value of thDAC_ID according to scan values
      if ( m_start_scan > m_stop_scan ) {
        m_thDAC_ID = m_start_scan - m_stepID;
      } else if ( m_start_scan < m_stop_scan ) {
        m_thDAC_ID = m_stepID + m_start_scan;
      }

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        // data for this RICH
        const auto& rD = data[rich];
        // sides per RICH
        for ( const auto& pD : rD ) {
          // PD modules per side
          for ( const auto& mD : pD ) {
            // PDs per module
            for ( const auto& PD : mD ) {

              // PD ID
              const auto pdID = PD.pdID();
              if ( pdID.isValid() ) {

                // Vector of SmartIDs
                const auto& rawIDs = PD.smartIDs();

                // Do we have any hits
                if ( !rawIDs.empty() ) {

                  // loop over hits
                  for ( const auto id : rawIDs ) {

                    // get the DB anode data for this hit
                    const auto& anodeData = pdmdbMaps.anodeData( id );
                    // info() << "PDMDB " << anodeData << endmsg;
                    // get the DB tel40 data
                    const auto& tel40Data = tel40Maps.tel40Data( id, anodeData.pdmdb, anodeData.frame );
                    // info() << "Tel40 " << tel40Data << endmsg;

                    // save to tuple data
                    m_rich.emplace_back( id.rich() );
                    m_side.emplace_back( id.panel() );
                    m_col.emplace_back( id.panelLocalModuleColumn() + 1 );
                    m_pdm.emplace_back( id.columnLocalModuleNum() );
                    m_ec.emplace_back( id.elementaryCell() );
                    m_pmt.emplace_back( id.pdNumInEC() );
                    m_anode.emplace_back( id.anodeIndex() );
                    m_localPixX.emplace_back( id.ecLocalPMTFrameX() );
                    m_localPixY.emplace_back( id.ecLocalPMTFrameY() );
                    m_globalPixX.emplace_back( id.ecGlobalPMTFrameX() );
                    m_globalPixX2.emplace_back( id.ecGlobalPMTFrameX2() );
                    m_globalPixY.emplace_back( id.ecGlobalPMTFrameY() );
                    m_tell40bit.emplace_back(
                        100 * ( 24 * ( tel40Data.sourceID.data() & 0x3F ) + tel40Data.connector.data() ) +
                        anodeData.bit.data() );
                    const auto gPos = smartIDsHelper.globalPosition( id );
                    const auto lPos = smartIDsHelper.globalToPDPanel( gPos );
                    m_spaceLocX.emplace_back( lPos.X() );
                    m_spaceLocY.emplace_back( lPos.Y() );
                    m_spaceGloX.emplace_back( gPos.X() );
                    m_spaceGloY.emplace_back( gPos.Y() );
                    m_spaceGloZ.emplace_back( gPos.Z() );
                    // JJ
                  } // hit loop

                  m_nHitsRich[rich] += rawIDs.size();

                } // PD has hits

              } // PDID is valid

            } // PDs
          }   // modules
        }     // panels
      }       // RICHes

      m_nHits = m_nHitsRich[Rich::Rich1] + m_nHitsRich[Rich::Rich2];

      // fill the TTree
      m_tree->Fill();
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DataTuple )

} // namespace Rich::Future::Mon

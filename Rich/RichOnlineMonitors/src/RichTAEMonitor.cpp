/*****************************************************************************\

* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/MergingTransformer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichRecUtils/RichDetParams.h"
#include "RichUtils/RichDAQDefinitions.h"

// Rich DAQ
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// event
#include "Event/ODIN.h"
#include "Event/TAEUtils.h"

// ConditionAccessor
#include "DetDesc/GenericConditionAccessorHolder.h"

// ConditionAccessor
#include "DetDesc/GenericConditionAccessorHolder.h"

// STD
#include <array>
#include <cmath>
#include <memory>
#include <vector>

namespace Rich::Future::Mon {

  /** @class TAEMonitor TAEMonitor.h
   *
   *  Produces RICH hit maps.
   *
   *  @author Federico Betti
   *  @date   2024-03-19
   */

  using ODINVector        = Gaudi::Functional::vector_of_const_<LHCb::ODIN const*>;
  using DecodedDataVector = Gaudi::Functional::vector_of_const_<DAQ::DecodedData const*>;

  template <typename... Args>
  using MergingConsumer = LHCb::Algorithm::MergingConsumer<
      void( Args const&... ),
      LHCb::Algorithm::Traits::BaseClass_t<LHCb::DetDesc::ConditionAccessorHolder<HistoAlgBase>>>;

  class TAEMonitor final : public MergingConsumer<ODINVector, DecodedDataVector> {

  public:
    /// Standard constructor
    TAEMonitor( const std::string& name, ISvcLocator* pSvcLocator )
        : MergingConsumer<ODINVector, DecodedDataVector>(
              name, pSvcLocator,
              // input data
              { KeyValues{ "ODINsLocation", {} }, KeyValues{ "DecodedDatasLocation", {} } } ) {
      // setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins2DHistos", 500 ).ignore();
    }

    /// Initialize
    StatusCode initialize() override {
      return MergingConsumer<ODINVector, DecodedDataVector>::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        DAQ::Tel40CableMapping::addConditionDerivation( this, m_tel40CableMapping.key() );
        DAQ::PDMDBEncodeMapping::addConditionDerivation( this, m_pdmdbEncodeMapping.key() );
      } );
    }

  private:
    LHCb::TAE::Handler m_taeHandler{ this };

    // histograms
    /// Occupancy vs taeID
    mutable Hist::DetArray<Hist::H2D<>> h_NHitsVsTaeID = { {} };
    /// taeID vs tell40 link (i.e. 100 bits)
    mutable Hist::DetArray<Hist::H2D<>> h_TaeIDVsBitID = { {} };

    ConditionAccessor<DAQ::Tel40CableMapping>  m_tel40CableMapping{ this, DAQ::Tel40CableMapping::DefaultConditionKey +
                                                                             "-" + name() };
    ConditionAccessor<DAQ::PDMDBEncodeMapping> m_pdmdbEncodeMapping{
        this, DAQ::PDMDBEncodeMapping::DefaultConditionKey + "-" + name() };

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      const BinLabels TAEaxisLabels = { "Prev4", "Prev3", "Prev2", "Prev1", "Central",
                                        "Next1", "Next2", "Next3", "Next4" };

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        ok &= initHist( h_NHitsVsTaeID[rich], Rich::HistogramID( "NHitsVsTaeID", rich ), //
                        "# of hits Vs Tae ID",                                           //
                        -4.5, 4.5, 9,                                                    //
                        -0.5, 1.25e5 - 0.5, 1.25e3,                                      //
                        "TAE ID", "# of hits", "",                                       //
                        TAEaxisLabels );
        ok &= initHist( h_TaeIDVsBitID[rich], Rich::HistogramID( "TaeIDVsBitID", rich ), //
                        "Tae ID Vs global bit ID",                                       //
                        -0.5, 2e5 - 0.5, 2e3,                                            //
                        -4.5, 4.5, 9,                                                    //
                        "bit ID", "TAE ID", "",                                          //
                        BinLabels(), TAEaxisLabels );
      }

      return StatusCode{ ok };
    }

  public:
    /// Functional operator
    void operator()( ODINVector const& odinVector, DecodedDataVector const& decodedDataVector ) const override {

      auto const& tel40Maps = m_tel40CableMapping.get();
      auto const& pdmdbMaps = m_pdmdbEncodeMapping.get();

      auto taeEvents = m_taeHandler.arrangeTAE( odinVector, decodedDataVector, -1 );
      // In case something went wrong
      if ( taeEvents.empty() ) { return; }

      // local buffers
      auto hb_TaeIDVsBitID = h_TaeIDVsBitID.buffer();
      auto hb_NHitsVsTaeID = h_NHitsVsTaeID.buffer();

      // Loop over taeEvents
      for ( auto const& element : taeEvents ) {

        int                     offset = element.first; // 0 is central, negative for Prev, positive for Next
        LHCb::ODIN const&       odin   = element.second.first;
        DAQ::DecodedData const& data   = element.second.second;

        // Loop over RICHes
        for ( const auto rich : Rich::detectors() ) {
          // data for this RICH
          unsigned int nHits = 0;
          const auto&  rD    = data[rich];
          // sides per RICH
          for ( const auto side : Rich::sides() ) {
            const auto& pD = rD[side];
            // PD modules per side
            for ( const auto& mD : pD ) {
              // PDs per module
              for ( const auto& PD : mD ) {

                // PD ID
                const auto pdID = PD.pdID();
                if ( pdID.isValid() ) {
                  // Vector of SmartIDs
                  const auto& rawIDs = PD.smartIDs();

                  // Do we have any hits
                  if ( !rawIDs.empty() ) {

                    // loop over hits
                    for ( const auto id : rawIDs ) {
                      ++nHits;
                      const auto& anodeData = pdmdbMaps.anodeData( id );
                      const auto& tel40Data = tel40Maps.tel40Data( id, anodeData.pdmdb, anodeData.frame );
                      if ( offset == 0 || odin.bunchCrossingType() == LHCb::ODIN::BXTypes::NoBeam ) {
                        ++hb_TaeIDVsBitID[rich][{
                            1e5 * static_cast<int>( id.panel() ) +
                                100 * ( 24 * ( tel40Data.sourceID.data() & 0x3F ) + tel40Data.connector.data() ) +
                                anodeData.bit.data(),
                            offset }];
                      }
                    } // hit loop

                  } // PD has hits

                } // PDID is valid

              } // PDs
            }   // modules
          }     // panels
          if ( ( offset == 0 || odin.bunchCrossingType() == LHCb::ODIN::BXTypes::NoBeam ) && ( nHits > 0 ) ) {
            ++hb_NHitsVsTaeID[rich][{ offset, nHits }];
          }
        } // RICHes
      }
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TAEMonitor )

} // namespace Rich::Future::Mon

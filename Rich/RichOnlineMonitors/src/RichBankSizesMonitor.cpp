/*****************************************************************************\

* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/RichDAQDefinitions.h"

// event
#include "Event/RawEvent.h"

// STD
#include <array>
#include <fstream>
#include <map>
#include <string>

namespace Rich::Future::Mon {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class BankSizes BankSizes.h
   *
   *  Produces bank size vs sourceID monitor.
   *
   *  @author Federico Betti
   *  @date   2024-04-30
   */

  class BankSizes final : public Consumer<void( const LHCb::RawBank::View& ),                   //
                                          LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase>> { //

  public:
    /// Standard constructor
    BankSizes( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "RawBanks", "DAQ/RawBanks/Rich" } } ) {
      setProperty( "NBins2DHistos", 500 ).ignore();
    }

    /// Initialize
    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { return StatusCode::SUCCESS; } );
    }

  private:
    /// Bank size vs sourceID histogram
    mutable Hist::DetArray<Hist::PanelArray<Hist::H2D<>>> h_BankSizes = { {} };

    /// Invalid SourceID
    mutable ErrorCounter m_invalidSourceID{ this, "Invalid Tel40 SourceID" };

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        for ( const auto side : Rich::sides() ) {
          double xMin( -999. ), xMax( -999. );
          if ( rich == Rich::Rich1 ) {
            // split equally range 0x2000 - 0x27FF
            xMin = ( side == Rich::top ? 8192. : 9216. );
            xMax = ( side == Rich::top ? 9215. : 10239. );
          }
          if ( rich == Rich::Rich2 ) {
            // split equally range 0x4800 - 0x4FFF
            xMin = ( side == Rich::aside ? 18432. : 19456. );
            xMax = ( side == Rich::aside ? 19455. : 20479. );
          }
          ok &= initHist( h_BankSizes[rich][side],                               //
                          Rich::HistogramID( "BankSizeVsSourceID", rich, side ), //
                          "Bank size vs SourceID",                               //
                          xMin - 0.5, xMax + 0.5, xMax - xMin + 1,               //
                          -0.5, 300.5, 301,                                      //
                          "Source ID", "Bank size [B]" );                        //
        }
      }

      return StatusCode{ ok };
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RawBank::View& richBanks ) const override {

      auto hb_BankSizes = h_BankSizes.buffer();

      for ( const auto* const bank : richBanks ) {
        if ( bank ) {
          const auto                source_id = bank->sourceID();
          const Rich::DAQ::SourceID tel40ID( source_id );
          if ( !tel40ID.isValid() ) {
            ++m_invalidSourceID;
            std::ostringstream mess;
            mess << "Invalid Source ID " << tel40ID;
            throw Rich::Exception( mess.str() );
          }
          const std::size_t bankSize = bank->size();
          const auto        rich     = tel40ID.rich();
          const auto        side     = tel40ID.side();
          ++hb_BankSizes[rich][side][{ source_id, bankSize }];
        }
      }
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( BankSizes )

} // namespace Rich::Future::Mon

/*****************************************************************************\
* (c) Copyright 2000-2026 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichRecUtils/RichDetParams.h"
#include "RichUtils/RichDAQDefinitions.h"

// Rich DAQ
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// event
#include "Event/ODIN.h"

// ROOT
#include "TString.h"

// STD
#include <array>
#include <cmath>
#include <memory>
#include <mutex>
#include <vector>

namespace Rich::Future::Mon {

  // Use the functional framework
  using namespace Gaudi::Functional;

  class BXTypeMonitors final : public Consumer<void( const LHCb::ODIN&, const DAQ::DecodedData& ),
                                               LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase>> {

  public:
    /// Standard constructor
    BXTypeMonitors( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                      KeyValue{ "DecodedDataLocation", DAQ::DecodedDataLocation::Default } } ) {}

  private:
    // histograms

    /// Pixel based hit map
    mutable Hist::DetArray<Hist::H2D<>> h_pixelMapBeamCrossing  = { {} };
    mutable Hist::DetArray<Hist::H2D<>> h_pixelMap2BeamCrossing = { {} };
    mutable Hist::DetArray<Hist::H2D<>> h_pixelMapBeamGas       = { {} };
    mutable Hist::DetArray<Hist::H2D<>> h_pixelMap2BeamGas      = { {} };
    mutable Hist::DetArray<Hist::H2D<>> h_pixelMapEmptyEmpty    = { {} };
    mutable Hist::DetArray<Hist::H2D<>> h_pixelMap2EmptyEmpty   = { {} };
    // nHits
    mutable Hist::DetArray<Hist::H1D<>>                   h_nHitsBXIDBeamCrossing      = { {} };
    mutable Hist::DetArray<Hist::PanelArray<Hist::H1D<>>> h_nHitsBXIDBeamCrossing_side = { {} };
    mutable Hist::DetArray<Hist::H1D<>>                   h_nHitsBXIDBeamGas           = { {} };
    mutable Hist::DetArray<Hist::H1D<>>                   h_nHitsBXIDEmptyEmpty        = { {} };
    // MaPmts hits (for occupancy)
    mutable Hist::DetArray<Hist::WH1D<>> h_Occupancy_bb   = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_Occupancy_ee   = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_Occupancy_ebbe = { {} };

  private:
    static constexpr float weight_PMThit = 1.0 / 64.0;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {

      bool ok = true;

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        const auto xRange = LHCb::RichSmartID::MaPMT::PDGlobalViewRangeX[rich] + 20;
        const auto xBins  = ( 2 * xRange ) + 1;
        const auto yRange = LHCb::RichSmartID::MaPMT::PDGlobalViewRangeY[rich] + 20;
        const auto yBins  = ( 2 * yRange ) + 1;
        const auto xMax   = xRange + 0.5;
        const auto yMax   = yRange + 0.5;
        ok &= initHist( h_pixelMapBeamCrossing[rich],                      //
                        Rich::HistogramID( "PixelMapBeamCrossing", rich ), //
                        "Global Pixel Map Beam Crossing",                  //
                        -xMax, xMax, xBins,                                //
                        -yMax, yMax, yBins,                                //
                        "Global Pixel X", "Global Pixel Y" );
        ok &= initHist( h_pixelMapBeamGas[rich],                      //
                        Rich::HistogramID( "PixelMapBeamGas", rich ), //
                        "Global Pixel Map Beam Gas",                  //
                        -xMax, xMax, xBins,                           //
                        -yMax, yMax, yBins,                           //
                        "Global Pixel X", "Global Pixel Y" );
        ok &= initHist( h_pixelMapEmptyEmpty[rich],                      //
                        Rich::HistogramID( "PixelMapEmptyEmpty", rich ), //
                        "Global Pixel Map Empty Empty",                  //
                        -xMax, xMax, xBins,                              //
                        -yMax, yMax, yBins,                              //
                        "Global Pixel X", "Global Pixel Y" );
        ok &= initHist( h_nHitsBXIDBeamCrossing[rich],                      //
                        Rich::HistogramID( "nHitsBXIDBeamCrossing", rich ), //
                        "Number of hits from ID trigger (Beam Crossing)",   //
                        0, 35000, 3500,                                     //
                        "nHitsBXIDBeamCrossing" );                          //
        ok &= initHist( h_nHitsBXIDBeamGas[rich],                           //
                        Rich::HistogramID( "nHitsBXIDBeamGas", rich ),      //
                        "Number of hits from ID trigger (Beam Gas)",        //
                        0, 35000, 3500,                                     //
                        "nHitsBXIDBeamGas" );                               //
        ok &= initHist( h_nHitsBXIDEmptyEmpty[rich],                        //
                        Rich::HistogramID( "nHitsBXIDEmptyEmpty", rich ),   //
                        "Number of hits from ID trigger (Empty Empty)",     //
                        0, 35000, 3500,                                     //
                        "nHitsBXIDEmptyEmpty" );                            //
        ok &= initHist( h_pixelMap2BeamCrossing[rich],                      //
                        Rich::HistogramID( "PixelMap2BeamCrossing", rich ), //
                        "Global Pixel Map 2 Beam Crossing",                 //
                        -xMax, xMax, xBins,                                 //
                        -yMax, yMax, yBins,                                 //
                        "Global Pixel X", "Global Pixel Y" );
        ok &= initHist( h_pixelMap2BeamGas[rich],                      //
                        Rich::HistogramID( "PixelMap2BeamGas", rich ), //
                        "Global Pixel Map 2 Beam Gas",                 //
                        -xMax, xMax, xBins,                            //
                        -yMax, yMax, yBins,                            //
                        "Global Pixel X", "Global Pixel Y" );
        ok &= initHist( h_pixelMap2EmptyEmpty[rich],                      //
                        Rich::HistogramID( "PixelMap2EmptyEmpty", rich ), //
                        "Global Pixel Map 2 Empty Empty",                 //
                        -xMax, xMax, xBins,                               //
                        -yMax, yMax, yBins,                               //
                        "Global Pixel X", "Global Pixel Y" );
        ok &= initHist( h_Occupancy_bb[rich],                        //
                        Rich::HistogramID( "Occupancy_bb", rich ),   //
                        "MaPMT nHits",                               //
                        0, 2600, 2600,                               //
                        "PMT index" );                               //
        ok &= initHist( h_Occupancy_ee[rich],                        //
                        Rich::HistogramID( "Occupancy_ee", rich ),   //
                        "MaPMT nHits",                               //
                        0, 2600, 2600,                               //
                        "PMT index" );                               //
        ok &= initHist( h_Occupancy_ebbe[rich],                      //
                        Rich::HistogramID( "Occupancy_ebbe", rich ), //
                        "MaPMT nHits",                               //
                        0, 2600, 2600,                               //
                        "PMT index" );                               //
        for ( const auto side : Rich::sides() ) {
          ok &= initHist( h_nHitsBXIDBeamCrossing_side[rich][side],
                          Rich::HistogramID( Form( "nHitsBXIDBeamCrossing_RICH%d_Side%d", rich, side ), rich, side ),
                          "Number of hits from ID triggers per side (Beam Crossing)", 0, 35000, 3500,
                          "nHitsBXIDBeamCrossing_side" );
        }
      }
      return StatusCode{ ok };
    }

  public:
    /// Functional operator
    void operator()( const LHCb::ODIN& odin, const DAQ::DecodedData& data ) const override {

      // local buffers
      auto hb_Occupancy_bb          = h_Occupancy_bb.buffer();
      auto hb_Occupancy_ebbe        = h_Occupancy_ebbe.buffer();
      auto hb_Occupancy_ee          = h_Occupancy_ee.buffer();
      auto hb_pixelMapBeamCrossing  = h_pixelMapBeamCrossing.buffer();
      auto hb_pixelMap2BeamCrossing = h_pixelMap2BeamCrossing.buffer();
      auto hb_pixelMapBeamGas       = h_pixelMapBeamGas.buffer();
      auto hb_pixelMap2BeamGas      = h_pixelMap2BeamGas.buffer();
      auto hb_pixelMapEmptyEmpty    = h_pixelMapEmptyEmpty.buffer();
      auto hb_pixelMap2EmptyEmpty   = h_pixelMap2EmptyEmpty.buffer();

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        // data for this RICH
        unsigned int nHits = 0;
        const auto&  rD    = data[rich];
        // sides per RICH
        for ( const auto side : Rich::sides() ) {
          const auto& pD = rD[side];
          // data for this side
          unsigned int nHitsSide = 0;
          // PD modules per side
          for ( const auto& mD : pD ) {
            // PDs per module
            for ( const auto& PD : mD ) {

              // PD ID
              const auto pdID = PD.pdID();
              if ( pdID.isValid() ) {

                // Vector of SmartIDs
                const auto& rawIDs = PD.smartIDs();

                // Do we have any hits
                if ( !rawIDs.empty() ) {

                  // loop over hits
                  for ( const auto id : rawIDs ) {

                    ++nHits;
                    ++nHitsSide;
                    // assign a bin to each MaPMT assuming RICH1 numbering
                    const unsigned int pmtbin =
                        ( 13 * 6 * 4 * 4 * ( id.panel() ) + 6 * 4 * 4 * ( id.panelLocalModuleColumn() ) +
                          4 * 4 * id.columnLocalModuleNum() + 4 * id.elementaryCell() + id.pdNumInEC() );
                    if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                      hb_Occupancy_bb[rich][pmtbin] += weight_PMThit;
                    } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                      hb_Occupancy_ebbe[rich][pmtbin] += weight_PMThit;
                    } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                      hb_Occupancy_ee[rich][pmtbin] += weight_PMThit;
                    }

                    const auto iGlobalX  = id.ecGlobalPMTFrameX() - id.panel() * 220;
                    const auto iGlobalX2 = id.ecGlobalPMTFrameX2() - id.panel() * 220;
                    const auto iGlobalY  = id.ecGlobalPMTFrameY();
                    if ( id.isHTypePMT() ) {
                      // For large H type PMTs fill a group of 4 corresponding to the effective smaller pixels
                      for ( int i = 0; i < 2; ++i ) {
                        for ( int j = 0; j < 2; ++j ) {
                          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                            ++hb_pixelMapBeamCrossing[rich][{ iGlobalX + i, iGlobalY + j }];
                            ++hb_pixelMap2BeamCrossing[rich][{ iGlobalX2 + i, iGlobalY + j }];
                          } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                      odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                            ++hb_pixelMapBeamGas[rich][{ iGlobalX + i, iGlobalY + j }];
                            ++hb_pixelMap2BeamGas[rich][{ iGlobalX2 + i, iGlobalY + j }];
                          } else if ( odin.bunchCrossingType() ==
                                      LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                            ++hb_pixelMapEmptyEmpty[rich][{ iGlobalX + i, iGlobalY + j }];
                            ++hb_pixelMap2EmptyEmpty[rich][{ iGlobalX2 + i, iGlobalY + j }];
                          }
                        }
                      }
                    } else {
                      if ( Rich::Rich1 == rich ) {
                        if ( 0 == side ) {
                          // FixMe : This lambda should be properly defined as a method someone and all
                          // the magic numbers explained obvious its just a meaningless horrid conversion
                          // no one in the future will understand.
                          auto convertX = []( const auto x ) { return ( -1 * ( x + 3 - 89 - 21 ) ) + 89 + 21; };
                          auto convertY = []( const auto y ) { return -( y + 1 ); };
                          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                            ++hb_pixelMapBeamCrossing[rich][{ convertY( iGlobalY ), convertX( iGlobalX ) }];
                            ++hb_pixelMap2BeamCrossing[rich][{ convertY( iGlobalY ), convertX( iGlobalX2 ) }];
                          } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                      odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                            ++hb_pixelMapBeamGas[rich][{ convertY( iGlobalY ), convertX( iGlobalX ) }];
                            ++hb_pixelMap2BeamGas[rich][{ convertY( iGlobalY ), convertX( iGlobalX2 ) }];
                          } else if ( odin.bunchCrossingType() ==
                                      LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                            ++hb_pixelMapEmptyEmpty[rich][{ convertY( iGlobalY ), convertX( iGlobalX ) }];
                            ++hb_pixelMap2EmptyEmpty[rich][{ convertY( iGlobalY ), convertX( iGlobalX2 ) }];
                          }

                        } else {
                          // FixMe : These lambdas should be properly defined as a method someone and all
                          // the magic numbers explained obvious its just a meaningless horrid conversion
                          // no one in the future will understand.
                          auto convertX = []( const auto x ) { return x + 3; };
                          auto convertY = []( const auto y ) { return y + 1; };
                          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                            ++hb_pixelMapBeamCrossing[rich][{ convertY( iGlobalY ), convertX( iGlobalX ) }];
                            ++hb_pixelMap2BeamCrossing[rich][{ convertY( iGlobalY ), convertX( iGlobalX2 ) }];
                          } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                      odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                            ++hb_pixelMapBeamGas[rich][{ convertY( iGlobalY ), convertX( iGlobalX ) }];
                            ++hb_pixelMap2BeamGas[rich][{ convertY( iGlobalY ), convertX( iGlobalX2 ) }];
                          } else if ( odin.bunchCrossingType() ==
                                      LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                            ++hb_pixelMapEmptyEmpty[rich][{ convertY( iGlobalY ), convertX( iGlobalX ) }];
                            ++hb_pixelMap2EmptyEmpty[rich][{ convertY( iGlobalY ), convertX( iGlobalX2 ) }];
                          }
                        }
                      } else {
                        auto convertX = []( const auto x ) { return x + 1; };
                        auto convertY = []( const auto y ) { return y; };
                        if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
                          ++hb_pixelMapBeamCrossing[rich][{ convertX( iGlobalX ), convertY( iGlobalY ) }];
                          ++hb_pixelMap2BeamCrossing[rich][{ convertX( iGlobalX2 ), convertY( iGlobalY ) }];
                        } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
                                    odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
                          ++hb_pixelMapBeamGas[rich][{ convertX( iGlobalX ), convertY( iGlobalY ) }];
                          ++hb_pixelMap2BeamGas[rich][{ convertX( iGlobalX2 ), convertY( iGlobalY ) }];
                        } else if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
                          ++hb_pixelMapEmptyEmpty[rich][{ convertX( iGlobalX ), convertY( iGlobalY ) }];
                          ++hb_pixelMap2EmptyEmpty[rich][{ convertX( iGlobalX2 ), convertY( iGlobalY ) }];
                        }
                      }
                    }

                  } // hit loop

                } // PD has hits

              } // PDID is valid

            } // PDs
          }   // modules

          if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
            ++h_nHitsBXIDBeamCrossing_side[rich][side][nHitsSide];
          }

        } // panels

        if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
          ++h_nHitsBXIDBeamCrossing[rich][nHits];
        }
        if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam1 ||
             odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::Beam2 ) {
          ++h_nHitsBXIDBeamGas[rich][nHits];
        }
        if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::NoBeam ) {
          ++h_nHitsBXIDEmptyEmpty[rich][nHits];
        }

      } // RICHes
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( BXTypeMonitors )

} // namespace Rich::Future::Mon

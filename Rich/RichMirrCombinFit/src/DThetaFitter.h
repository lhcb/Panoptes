/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// C++
#include <sstream>
#include <string>
// ROOT
#include <TH1D.h>
// RooFit
#include <RooDataHist.h>
#include <RooRealVar.h>
#include <RooWorkspace.h>

class DThetaFitter {

  template <class typeToString>
  std::string makeString( const typeToString& thingToString ) {
    std::stringstream ss;
    ss << thingToString;
    return ss.str();
  }

  RooWorkspace* _wspace     = nullptr;
  RooDataHist*  _dThetaHist = nullptr;
  double        _dThetaMin{ 0 };
  double        _dThetaMax{ 0 };
  int           _fitShape{ 0 };

  void buildPdfBifurGauss( int backgroundOrder );
  void buildPdfGauss( int backgroundOrder );

  int fit();

public:
  DThetaFitter( float dThetaMin = -0.007, float dThetaMax = 0.007, int fitShape = 0, int backgroundOrder = 2 );

  double getMean() const;
  double getMeanErr() const;

  double getWidth() const;
  double getWidthErr() const;

  void loadHistogram( std::string filePath, std::string histogramPath = "RICH/RichAlignMoniR1Gas/deltaThetaUnamb" );
  void loadHistogram( TH1D* histogram );

  double plot( std::string plotDirectory, double plotIfChi2GreaterThan = 0.0 );

  void setFitShape( int fitShape ) { _fitShape = fitShape; }

  void setMeanLimits( double min, double max ) {
    RooRealVar* dTheta = _wspace->var( "gaussMean" );
    if ( min != -1.0 ) dTheta->setMin( min );
    if ( max != -1.0 ) dTheta->setMax( max );
  }
  void setWidthLimits( double min, double max ) {
    RooRealVar* dTheta = _wspace->var( "gaussWidth" );
    if ( min != -1.0 ) dTheta->setMin( min );
    if ( max != -1.0 ) dTheta->setMax( max );
  }

  void setConstantMean( double mean );
  void setMean( double mean );
  void setWidth( double width );

  ~DThetaFitter();
};

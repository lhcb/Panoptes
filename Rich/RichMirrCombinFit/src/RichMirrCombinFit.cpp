/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local
#include "IRichMirrCombFit.h"
#include "LHCbStyle.h"
#include "MirrCombFitter.h"
// C++
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
// ROOT
#include <Math/MinimizerOptions.h>

using namespace std;

//------------------------------------------------------------------------------

int main( int argc, char* argv[] ) {
  cout << "There were " << argc << " arguments passed to RichMirrCombinFit" << endl;

  // scenario 1:
  //    whatever.exe rich2
  //    then argc = 2, and justGlobalFit is false
  // scenario 2:
  //    whatever.exe rich2 just_global_fit
  //    then argc = 3, and justGlobalFit is true
  // scenario 3:
  //    whatever.exe rich2 priYn
  //    then argc = 3, and we are looking at a calibration-tilted root file

  LHCbStyle( true );
  gStyle->SetPalette( kBlueYellow );

  // extract the rich number as a string, e.g. "2":
  string rich = string( argv[1] );
  cout << "this is " + rich << endl;
  string rN = regex_replace( rich, regex( "rich([1/2])" ), "$1" );

  // find out if the the program is asked to do only just a global fit:
  bool justGlobalFit = false;
  if ( argc == 3 && string( argv[2] ) == "just_global_fit" ) {
    justGlobalFit = true;
    cout << "It is global fit: justGlobalFit = " << justGlobalFit << "  argc = " << argc << endl;
  } else
    cout << "It is normal fit: justGlobalFit = " << justGlobalFit << "  argc = " << argc << endl;

  // if a calibration-tilted root file is to be processed, what is the tiltName:
  string tiltName = "";
  if ( ( argc == 3 && string( argv[2] ) == "priYn" ) || ( argc == 3 && string( argv[2] ) == "priYp" ) ||
       ( argc == 3 && string( argv[2] ) == "priZn" ) || ( argc == 3 && string( argv[2] ) == "priZp" ) ||
       ( argc == 3 && string( argv[2] ) == "secYn" ) || ( argc == 3 && string( argv[2] ) == "secYp" ) ||
       ( argc == 3 && string( argv[2] ) == "secZn" ) || ( argc == 3 && string( argv[2] ) == "secZp" ) ) {
    tiltName = string( argv[2] );
    cout << "Tilt Name provided: tiltName = " << tiltName << "  argc = " << argc << endl;
  }

  // Initialize an instance of ROOT::Math::MinimizerOptions
  ROOT::Math::MinimizerOptions SetThese;

  // Set options for Minimizer Algorithm, Type, and Strategy
  // SetThese.SetDefaultMinimizer( "Minuit", "Migrad" ); // Minuit and Migrad, use this as first option always
  // SetThese.SetDefaultStrategy( 1 );                   // Strategy 1, good for regular fits.
  // For many-parameter fits we switch to Strategy 0. See below.

  SetThese.SetDefaultMinimizer( "Minuit2" ); // Minuit2    (causes more fit fails)
  SetThese.SetDefaultStrategy( 2 );          // Strategy 2 (causes more fit fails)

  // Set options for MaxFunctionCalls and MaxIterations for Minuit
  int MaxFunctionCalls = 2000000; // Call limits seem to be at most 75000, let's use about 20x that
  int MaxIterations    = 20000;   // TMinuit uses 500 as default, let's use 20x that
  SetThese.SetDefaultMaxFunctionCalls( MaxFunctionCalls );
  SetThese.SetDefaultMaxIterations( MaxIterations );

  // Print everything so we can see it
  cout << "DefaultMinimizerType    " << SetThese.DefaultMinimizerType() << endl;
  cout << "DefaultMinimizerAlgo    " << SetThese.DefaultMinimizerAlgo() << endl;
  cout << "DefaultStrategy         " << SetThese.DefaultStrategy() << endl;
  cout << "DefaultMaxFunctionCalls " << SetThese.DefaultMaxFunctionCalls() << endl;
  cout << "DefaultMaxIterations    " << SetThese.DefaultMaxIterations() << endl;
  cout << endl;

  IRichMirrCombFit* richMirrCombFit = new MirrCombFitter( rN, tiltName );
  cout << "Constructed RichMirrCombFit(\"" + rN + "\",\"" + tiltName + "\")" << endl;

  richMirrCombFit->ReadConfFile();

  double shift = 0.0;
  richMirrCombFit->SetInitialSinusoidShift( shift );

  cout << "Monitoring Histos" << endl; // plots for Data Manager, only the untilted one is really useful though
                                       // richMirrCombFit->GlobalFit();

  // Switch to Strategy 0 (strategy zero is good for many-parameter systems)
  int Strategy2D = 0;
  SetThese.SetDefaultStrategy( Strategy2D );
  // SetThese.SetDefaultMinimizer("Minuit", "Minimize"); // Use MINIMIZE[see Minuit manual] (maybe fewer fits will fail)

  cout << "Minuit options have changed for Fit Histos!" << endl;

  cout << "DefaultMinimizerType    " << SetThese.DefaultMinimizerType() << endl;
  cout << "DefaultMinimizerAlgo    " << SetThese.DefaultMinimizerAlgo() << endl;
  cout << "DefaultStrategy         " << SetThese.DefaultStrategy() << endl;
  cout << "DefaultMaxFunctionCalls " << SetThese.DefaultMaxFunctionCalls() << endl;
  cout << "DefaultMaxIterations    " << SetThese.DefaultMaxIterations() << endl;
  cout << endl;

  cout << "Fitting Histos..." << endl; // The important stuff & additional plots for RICH Piquet (not made yet)
  richMirrCombFit->FitHistos();

  cout << "Write Output File" << endl;
  richMirrCombFit->PrintResults();

  richMirrCombFit->CombinFitDecision(); // Creates a file that lets you know if mirror combination's tilts
                                        // exceed m_warningFactor*stopTolerance.
  return 0;
}

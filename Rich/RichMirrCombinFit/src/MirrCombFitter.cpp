/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MirrCombFitter.h"
// local
#include "DThetaFitter.h"
#include "FitSurf.h"
// ROOT
#include <Math/MinimizerOptions.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TF2.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TPRegexp.h>
// boost
#include <boost/format.hpp>

#include <algorithm>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

using boost::format;
using namespace std;

//------------------------------------------------------------------------------

MirrCombFitter::MirrCombFitter( string rN, string tiltName ) {
  cout << "Constructing MirrCombFitter(\"" + rN + "\")" << endl;
  cout << "Creating map of MirrComb objects" << endl;
  this->CreateMapCombNameCombObj();
  m_rN       = rN;
  m_tiltName = tiltName;
}

//------------------------------------------------------------------------------

MirrCombFitter::~MirrCombFitter() {}

//------------------------------------------------------------------------------

void MirrCombFitter::GetFitParams( TH2D* histo ) {
  m_histo = histo;

  // m_histo->Print();
  if ( m_combinFitMethod == 1 )
    FitBySlice();
  else if ( m_combinFitMethod == 3 )
    FitSurface( false ); // unifyWidths == false
  else if ( m_combinFitMethod == 5 )
    FitSurface( true ); // unifyWidths == true
  else { cout << "method not recognized" << endl; }
}

//------------------------------------------------------------------------------

void MirrCombFitter::FitSurface( bool unifyWidths = false ) {
  int phiBins = m_histo->GetNbinsX();

  cout << "phiBins " << phiBins << endl;

  TF2* fitSurface;

  int corrUniWid = 0;

  if ( !unifyWidths ) {
    FitSurfBM* functorBM = new FitSurfBM( phiBins ); // The functor for independent Gaussian widths in each bin
    fitSurface           = new TF2( "fitSurface", functorBM, &FitSurfBM::FitFcnBM, 0., 6.28, m_begRange, m_endRange,
                                    3 + phiBins * 5, "FitSurfBM", "FitFcnBM" );
  } else {
    corrUniWid = -phiBins + 1; // due to single unified sigma parameter (4) for BMUW, otherwise there are phiBins sigmas

    FitSurfBMUW* functorBMUW = new FitSurfBMUW( phiBins ); // The functor for unified  Gaussian widths
    fitSurface = new TF2( "fitSurface", functorBMUW, &FitSurfBMUW::FitFcnBMUW, 0., 6.28, m_begRange, m_endRange,
                          4 + phiBins * 4, "FitSurfBMUW", "FitFcnBMUW" );
  }

  // fitSurface->Print();

  int theBins = m_histo->GetNbinsY();

  cout << "theBins " << theBins << endl;

  TAxis* theAxis = m_histo->GetYaxis();
  int    begBin  = theAxis->FindBin( m_begRange );
  int    endBin  = theAxis->FindBin( m_endRange ) - 1;

  cout << "beg bin: " << begBin << "  endbin: " << endBin << endl;
  // double minAverageBinPop = 6; // now it's a member variable
  cout << "minAverageBinPop: " << m_minAverageBinPop << endl;

  // Set the parameters

  fitSurface->SetParameter( 0, 0.0001 ); // total tilt around Y in rad
  fitSurface->SetParameter( 1, 0.0001 ); // total tilt around Z in rad
  fitSurface->SetParameter( 2, 0.0 );    // sinusoid shift      in rad

  fitSurface->SetParLimits( 0, m_begRange + 0.001, m_endRange - 0.001 );        // total tilt around Y in rad limits
  fitSurface->SetParLimits( 1, m_begRange + 0.001, m_endRange - 0.001 );        // total tilt around Z in rad limits
  fitSurface->SetParLimits( 2, -m_nominSigmRad * 0.25, m_nominSigmRad * 0.25 ); // sinusoid shift      in rad limits

  if ( m_fixSinusoidShift ) {
    fitSurface->FixParameter( 2, m_sinusoidShift ); // sinusoid shift      in rad is constant
  }

  if ( unifyWidths ) {
    fitSurface->SetParameter( 3, m_nominSigmRad );                             // sigma in rad unified
    fitSurface->SetParLimits( 3, m_nominSigmRad * 0.5, m_nominSigmRad * 3.0 ); // sigma in rad unified       limits
  }

  for ( int phibin = 1; phibin <= phiBins; phibin++ ) { // e.g. 1 - 20
    TH1D*  slice    = m_histo->ProjectionY( "", phibin, phibin );
    double avHeight = slice->Integral( begBin, endBin ) / ( endBin - begBin + 1 );

    // int edge_cut = 2;
    // int edge_use = 3;
    //
    // double lo_sum = 0.0;
    // for(int j=edge_cut; j<edge_use+edge_cut; j++) {
    // lo_sum += slice->GetBinContent(j);
    //}
    // double hi_sum = 0.0;
    // for(int j=theBins-edge_use-edge_cut; j<theBins-edge_cut; j++) {
    //  hi_sum += slice->GetBinContent(j);
    //}
    // double bkg_est = (lo_sum + hi_sum)/(2*edge_use);
    // double a1_est  = (hi_sum/edge_use - lo_sum/edge_use)/(slice->GetBinCenter(theBins  - (int)(1 + edge_use/2))
    //                                                    - slice->GetBinCenter(           (int)(1 + edge_use/2)));
    // double a0_est  =  lo_sum/edge_use -            a1_est*slice->GetBinCenter(edge_cut + (int)(1 + edge_use/2));
    // double maxBinValue  = slice->GetBinContent(slice->GetMaximumBin());
    // double maxBinCenter = slice->GetBinCenter( slice->GetMaximumBin());

    if ( avHeight > (double)m_minAverageBinPop ) {
      if ( !unifyWidths ) {
        fitSurface->SetParameter( 2 + phibin, m_nominSigmRad ); // sigma in rad
      }
      fitSurface->SetParameter( 2 + phiBins + corrUniWid + phibin, avHeight * 0.5 );     // const before gauss
      fitSurface->SetParameter( 2 + phiBins * 2 + corrUniWid + phibin, avHeight * 0.3 ); // constant  background
      fitSurface->SetParameter( 2 + phiBins * 3 + corrUniWid + phibin, 0.0 );            // linear    background
      fitSurface->SetParameter( 2 + phiBins * 4 + corrUniWid + phibin, 0.0 );            // quadratic background

      if ( !unifyWidths ) {
        fitSurface->SetParLimits( 2 + phibin, m_nominSigmRad * 0.5, m_nominSigmRad * 3.0 ); // sigma in rad       limits
      }
      fitSurface->SetParLimits( 2 + phiBins + corrUniWid + phibin, 0.0, 1.0e7 ); // const before gauss limits

      // fitSurface->SetParLimits( 2 + phiBins*2 + corrUniWid+phibin,    0.0, 1.0e7 );    // constant  background limits
      // fitSurface->SetParLimits( 2 + phiBins*3 + corrUniWid+phibin, -1.0e5, 1.0e5 );    // linear    background limits
      // fitSurface->SetParLimits( 2 + phiBins*4 + corrUniWid+phibin, -1.0e7, 0.0   );    // quadratic background limits

    } else { // low-populated slice
      if ( !unifyWidths ) {
        fitSurface->FixParameter( 2 + phibin, 1000. ); // sigma in rad is constant
      }
      fitSurface->FixParameter( 2 + phiBins + corrUniWid + phibin, 0.0 );     // const before gauss   is constant
      fitSurface->FixParameter( 2 + phiBins * 2 + corrUniWid + phibin, 0.0 ); // constant  background is constant
      fitSurface->FixParameter( 2 + phiBins * 3 + corrUniWid + phibin, 0.0 ); // linear    background is constant
      fitSurface->FixParameter( 2 + phiBins * 4 + corrUniWid + phibin, 0.0 ); // quadratic background is constant

      for ( int thebin = 1; thebin <= theBins; thebin++ ) { // e.g. 1 - 20
        m_histo->SetBinContent( phibin, thebin, 0.0 );
        m_histo->SetBinError( phibin, thebin, 0.0 );
      }
    }
  } // end of loop over phibin

  // PN - Printout Limits
  cout << "MirrCombFitter::FitSurface(): Fitting." << endl;
  cout << "Limits on total tilts are now " << m_begRange + 0.001 << " and " << m_endRange - 0.001 << "." << endl;

  // call  TH2D::Fit with the name of the TF2 object
  // m_histo->TH2D::Fit("fitSurface", "Q");

  string histoName = m_histo->GetName();
  // This next line from Claire removes extraneous directory information if running Online, otherwise erases nothing
  histoName = regex_replace( histoName, regex( "RICH/RichAlignMoniR" + m_rN + "Gas/Rich" + m_rN + "Gas/" ), "" );
  cout << "fitSurface fits " << histoName << endl;

  // "R"  Use the Range specified in the function range
  // "S"  The result of the fit is returned in the TFitResultPtr
  TFitResultPtr fitResult = m_histo->TH2D::Fit( "fitSurface", "RS" );
  // TFitResultPtr fitResult = m_histo->TH2D::Fit( "fitSurface", "R" );
  bool   goodFit = fitResult->IsValid();
  double fitProb = fitResult->Prob();
  if ( !goodFit ) {
    cout << "The fit using MIGRAD is not valid." << endl;
    cout << "The Probability of Chi-squared is: " << fitProb << endl;
    cout << "Trying again with MINIMIZE." << endl;
    ROOT::Math::MinimizerOptions SetTheseAgain;
    SetTheseAgain.SetMinimizerAlgorithm( "Minimize" );
    fitResult = m_histo->TH2D::Fit( "fitSurface", "RS" );
    goodFit   = fitResult->IsValid();
    fitProb   = fitResult->Prob();

    if ( !goodFit ) {
      cout << "The fit using MINIMIZE is also not valid." << endl;
      cout << "The Probability of Chi-squared is: " << fitProb << endl;
    } else {
      cout << "The fit using MINIMIZE is valid." << endl;
      cout << "The Probability of Chi-squared is: " << fitProb << endl;
    }
    SetTheseAgain.SetMinimizerAlgorithm( "Migrad" );
  } else {
    cout << "The fit using MIGRAD is valid." << endl;
    cout << "The Probability of Chi-squared is: " << fitProb << endl;
  }
  // TF2* fitFcnSurf = h2->GetFunction("fitSurface");

  // Code from Sam, modified by PN to allow the limits to expand and re-fit,
  // if the total tilts are at their limit.

  bool atLimit = 0;

  // old:
  // double limit = fabs(m_begRange+0.001);
  // if ( fabs(fitSurface->GetParameter(0)) > (limit - 0.0002) ) atLimit = true;
  // if ( fabs(fitSurface->GetParameter(1)) > (limit - 0.0002) ) atLimit = true;

  double lowerLimit = m_begRange + 0.001;
  double upperLimit = m_endRange - 0.001;

  // Limit expansion of the range to a factor of 2
  double minLowerLimit = 2 * lowerLimit; // assumes lower limit is negative
  double maxUpperLimit = 2 * upperLimit; // assumes upper limit is positive

  double extendLimit = 0.002; // this is for RICH1 (for RICH2 = 0.001)
  if ( m_rN == "2" ) extendLimit = 0.001;

  if ( fitSurface->GetParameter( 0 ) < ( lowerLimit + 0.0002 ) ) atLimit = true;
  if ( fitSurface->GetParameter( 1 ) < ( lowerLimit + 0.0002 ) ) atLimit = true;

  if ( fitSurface->GetParameter( 0 ) > ( upperLimit - 0.0002 ) ) atLimit = true;
  if ( fitSurface->GetParameter( 1 ) > ( upperLimit - 0.0002 ) ) atLimit = true;

  while ( atLimit ) {

    atLimit = false;

    lowerLimit -= extendLimit;
    upperLimit += extendLimit;

    cout << "MirrCombFitter.cpp: One of the total tilts is at its limit." << endl;
    cout << "                  Limits are now " << lowerLimit << " and " << upperLimit << ". Re-fitting." << endl;

    // fitSurface->SetParLimits( 0,     -limit,      limit );
    // fitSurface->SetParLimits( 1,     -limit,      limit );

    fitSurface->SetParLimits( 0, lowerLimit, upperLimit );
    fitSurface->SetParLimits( 1, lowerLimit, upperLimit );

    // "R"  Use the Range specified in the function range
    // "S"  The result of the fit is returned in the TFitResultPtr
    fitResult = m_histo->TH2D::Fit( "fitSurface", "RS" );
    // fitResult = m_histo->TH2D::Fit("fitSurface", "R");
    goodFit = fitResult->IsValid();
    fitProb = fitResult->Prob();
    if ( !goodFit ) {
      cout << "The fit using MIGRAD is not valid." << endl;
      cout << "The Probability of Chi-squared is: " << fitProb << endl;
      cout << "Trying again with MINIMIZE." << endl;
      ROOT::Math::MinimizerOptions SetTheseAgain;
      SetTheseAgain.SetMinimizerAlgorithm( "Minimize" );
      fitResult = m_histo->TH2D::Fit( "fitSurface", "RS" );
      goodFit   = fitResult->IsValid();
      fitProb   = fitResult->Prob();
      if ( !goodFit ) {
        cout << "The fit using MINIMIZE is also not valid." << endl;
        cout << "The Probability of Chi-squared is: " << fitProb << endl;
      } else {
        cout << "The fit using MINIMIZE is valid." << endl;
        cout << "The Probability of Chi-squared is: " << fitProb << endl;
      }
      SetTheseAgain.SetMinimizerAlgorithm( "Migrad" );
    } else {
      cout << "The fit using MIGRAD is valid." << endl;
      cout << "The Probability of Chi-squared is: " << fitProb << endl;
    }

    // if ( fabs(fitSurface->GetParameter(0)) >      (limit - 0.0002) ) atLimit = true;
    // if ( fabs(fitSurface->GetParameter(1)) >      (limit - 0.0002) ) atLimit = true;

    if ( fitSurface->GetParameter( 0 ) < ( lowerLimit + 0.0002 ) ) atLimit = true;
    if ( fitSurface->GetParameter( 1 ) < ( lowerLimit + 0.0002 ) ) atLimit = true;

    if ( fitSurface->GetParameter( 0 ) > ( upperLimit - 0.0002 ) ) atLimit = true;
    if ( fitSurface->GetParameter( 1 ) > ( upperLimit - 0.0002 ) ) atLimit = true;

    // break out of loop if we've made the window too large
    if ( lowerLimit < minLowerLimit ) atLimit = false;
    if ( upperLimit > maxUpperLimit ) atLimit = false;
  }

  // Get MirrComb
  regex  combNameRegex{ "p[\\d]{2}s[\\d]{2}" };
  smatch combNameSmatch;
  regex_search( histoName, combNameSmatch, combNameRegex );
  string combName = combNameSmatch.str();

  m_mapCombNameCombObj[combName].Y( fitSurface->GetParameter( 0 ) );
  m_mapCombNameCombObj[combName].YErr( fitSurface->GetParError( 0 ) );
  m_mapCombNameCombObj[combName].Z( fitSurface->GetParameter( 1 ) );
  m_mapCombNameCombObj[combName].ZErr( fitSurface->GetParError( 1 ) );
  m_mapCombNameCombObj[combName].Shift( fitSurface->GetParameter( 2 ) );
  m_mapCombNameCombObj[combName].ShiftErr( fitSurface->GetParError( 2 ) );

  // Code provided by Sam to make plots for FitSurface()
  m_histo->Draw( "colz HIST" );

  TF1* sinusoid = new TF1( "sinusoid", "[0]*cos(x) + [1]*sin(x) + [2]", 0.0, 2.0 * 3.141 ); // Anatoly added shift
  sinusoid->SetParameter( 0, fitSurface->GetParameter( 0 ) );
  sinusoid->SetParameter( 1, fitSurface->GetParameter( 1 ) );
  sinusoid->SetParameter( 2, fitSurface->GetParameter( 2 ) ); // Anatoly added shift
  sinusoid->Draw( "SAME" );

  string iN = "_i" + to_string( m_iterNumber );
  if ( m_plotOutputLevel > 0 )
    // gPad->SaveAs( ( m_plotDir+"/Rich"+m_rN+histoName+"_"+m_variant+iN+".png" ).c_str() );
    gPad->SaveAs( ( m_plotDir + "/" + m_variant + "_" + histoName + iN + ".png" ).c_str() );

  delete sinusoid;
}

//------------------------------------------------------------------------------

void MirrCombFitter::FitBySlice() {
  // int NumEntries = static_cast<int>( m_histo->GetEntries() );

  TGraphErrors* cleanPlot = new TGraphErrors();

  FitSlices( cleanPlot );

  Fit2D( cleanPlot );

  delete cleanPlot;
}

//------------------------------------------------------------------------------

void MirrCombFitter::Fit2D( TGraphErrors* cleanPlot ) {
  // here starts the sinusoidal fit of the Gauss's-mean-in-slice dependence on phi

  // Define fit function
  TF1* sinusoid = new TF1( "sinusoid", "[0]*cos(x) + [1]*sin(x) + [2]", 0, 2.0 * TMath::Pi() );

  sinusoid->SetParName( 0, "CosCoeff" );
  sinusoid->SetParName( 1, "SinCoeff" );
  sinusoid->SetParName( 2, "Shift" );

  int phiBins = (int)m_histo->GetNbinsX();
  // double phiWidth = 2.*TMath::Pi()/phiBins;
  // int theBins = (int)m_histo->GetNbinsY();

  // Fit TGraphErrors with sinusoid

  // Estimate cos amplitude
  double CosAmpEst;
  double x, y1, y2, sy1, sy2;
  cleanPlot->GetPoint( int( phiBins / 4 ), x, y1 );
  cleanPlot->GetPoint( int( phiBins / 4 * 3 ), x, y2 );
  sy1       = cleanPlot->GetErrorY( int( phiBins / 4 ) );
  sy2       = cleanPlot->GetErrorY( int( phiBins / 4 * 3 ) );
  CosAmpEst = ( y1 / sy1 - y2 / sy2 ) / ( 1. / sy1 + 1. / sy2 );

  sinusoid->SetParameter( 0, CosAmpEst ); // total tilt around Y in rad

  // Estimate sin amplitude
  double SinAmpEst;
  double y3, y4, y5, sy3, sy4, sy5;
  cleanPlot->GetPoint( 0, x, y3 );
  cleanPlot->GetPoint( int( phiBins / 2 ), x, y4 );
  cleanPlot->GetPoint( phiBins - 1, x, y5 );
  sy3       = cleanPlot->GetErrorY( 0 );
  sy4       = cleanPlot->GetErrorY( int( phiBins / 2 ) );
  sy5       = cleanPlot->GetErrorY( phiBins - 1 );
  SinAmpEst = ( y3 / sy3 - y4 / sy4 + y5 / sy5 ) / ( 1. / sy3 + 1. / sy4 + 1. / sy5 );

  sinusoid->SetParameter( 1, SinAmpEst ); // total tilt around Z in rad

  // Fix sinusoid shift when m_fixSinusoidShift == true
  if ( m_fixSinusoidShift ) { sinusoid->FixParameter( 2, m_sinusoidShift ); }

  // Fit TGraphErrors
  cleanPlot->Fit( sinusoid, "R" );

  // cos    = sinusoid->GetParameter(0);
  // cosErr = sinusoid->GetParError( 0);
  // sin    = sinusoid->GetParameter(1);
  // sinErr = sinusoid->GetParError( 1);

  string histoName = m_histo->GetName();
  histoName        = regex_replace( histoName, regex( "RICH/RichAlignMoniR" + m_rN + "Gas/Rich" + m_rN + "Gas/" ), "" );

  string iN          = "_i" + to_string( m_iterNumber );
  bool   writehistos = true;
  if ( writehistos ) {
    m_histo->Draw( "colz" );
    cleanPlot->Draw( "SAME" );
    if ( m_plotOutputLevel > 0 )
      gPad->SaveAs( ( m_plotDir + "/" + m_variant + "_" + histoName + iN + ".png" ).c_str() );
  }

  // Get MirrComb
  regex  combNameRegex{ "p[\\d]{2}s[\\d]{2}" };
  smatch combNameSmatch;
  regex_search( histoName, combNameSmatch, combNameRegex );
  string combName = combNameSmatch.str();

  m_mapCombNameCombObj[combName].Y( sinusoid->GetParameter( 0 ) );
  m_mapCombNameCombObj[combName].YErr( sinusoid->GetParError( 0 ) );
  m_mapCombNameCombObj[combName].Z( sinusoid->GetParameter( 1 ) );
  m_mapCombNameCombObj[combName].ZErr( sinusoid->GetParError( 1 ) );
  m_mapCombNameCombObj[combName].Shift( sinusoid->GetParameter( 2 ) );
  m_mapCombNameCombObj[combName].ShiftErr( sinusoid->GetParError( 2 ) );
}

//------------------------------------------------------------------------------

void MirrCombFitter::FitSlices( TGraphErrors* cleanPlot ) {
  string histoName = m_histo->GetName();
  // This next line from Claire removes extraneous directory information if running Online, otherwise erases nothing
  histoName = regex_replace( histoName, regex( "RICH/RichAlignMoniR" + m_rN + "Gas/Rich" + m_rN + "Gas/" ), "" );

  int phiBins = m_histo->GetNbinsX();
  // int theBins = m_histo->GetNbinsY();

  double phiWidth = 2. * TMath::Pi() / phiBins;

  TH1D chi2Histo( "chi2", "chi2", 30, 0., 10. );

  for ( int i = 1; i <= phiBins; i++ ) {
    // Get projection and save in a RooDataHist
    TH1D*  stripe( (TH1D*)m_histo->ProjectionY( "stripe", i, i, "o" ) );
    double photonCount = stripe->GetEntries();

    cout << "This slice contains " << photonCount << " photons." << endl;

    DThetaFitter dThetaFitter( m_begRange, m_endRange, 0, m_backgroundOrder );
    dThetaFitter.setMean( m_sinusoidShift );
    dThetaFitter.setWidth( m_nominSigmRad );
    dThetaFitter.loadHistogram( stripe );

    // Add mean values to 2D plot to fit
    cleanPlot->SetPoint( i - 1, ( i - 0.5 ) * phiWidth, dThetaFitter.getMean() );
    cleanPlot->SetPointError( i - 1, 0.5 * phiWidth / sqrt( photonCount ), dThetaFitter.getMeanErr() );

    string plotName = (string)stripe->GetName() + "_" + histoName + "_" + to_string( i );

    double chi2 = 0.0;
    if ( m_plotOutputLevel > 1 )
      chi2 = dThetaFitter.plot( m_plotDir + "/" + plotName, 3.0 ); // only plot when chi2 greater than 3
    if ( m_plotOutputLevel > 2 ) chi2 = dThetaFitter.plot( m_plotDir + "/" + plotName ); // plot all the time
    chi2Histo.Fill( chi2 );
  }

  string iN           = "_i" + to_string( m_iterNumber );
  string chi2PlotName = m_variant + "_" + histoName + "_chi2" + iN + ".png";

  TCanvas canvas( "chiCanvas", "chiCanvas", 1000, 1000 );
  chi2Histo.Draw();
  if ( m_plotOutputLevel > 2 ) canvas.Print( ( m_plotDir + "/" + chi2PlotName ).c_str() );
}

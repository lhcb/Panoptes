/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DThetaFitter.h"

// C++
#include <cmath>
#include <iostream>
// ROOT
#include <TCanvas.h>
#include <TFile.h>
#include <TLatex.h>
#include <TLine.h>
#include <TPad.h>
#include <TTree.h>
// RooFit
#include <RooAbsData.h>
#include <RooAbsPdf.h>
#include <RooAddPdf.h>
#include <RooArgList.h>
#include <RooArgSet.h>
#include <RooBifurGauss.h>
#include <RooChebychev.h>
#include <RooDataSet.h>
#include <RooFit.h>
#include <RooFitResult.h>
#include <RooGaussian.h>
#include <RooHist.h>
#include <RooPlot.h>

using namespace std;

DThetaFitter::DThetaFitter( float dThetaMin, float dThetaMax, int fitShape, int backgroundOrder )
    : _wspace( 0 ), _dThetaHist( 0 ), _dThetaMin( dThetaMin ), _dThetaMax( dThetaMax ), _fitShape( fitShape ) {
  cout << "Hello from the DThetaFitter() constructor!" << endl;
  // create workspace to contain all roofit objects
  _wspace = new RooWorkspace( "myWS" );

  // create fit variable and import to the workspace
  RooRealVar dTheta( "dTheta", "#Delta#theta", dThetaMin, dThetaMax, "rad" );
  _wspace->import( dTheta );

  if ( _fitShape == 0 )
    this->buildPdfGauss( backgroundOrder );
  else if ( _fitShape == 1 )
    this->buildPdfBifurGauss( backgroundOrder );
  else
    cout << "  ERROR: The chosen fit shape does not exist" << endl;
}

void DThetaFitter::loadHistogram( string filePath, string histogramPath ) {
  TFile* file = new TFile( filePath.c_str(), "read" );
  TH1D*  hist = new TH1D( *(TH1D*)file->Get( histogramPath.c_str() ) );

  RooRealVar* dTheta = _wspace->var( "dTheta" );

  if ( _dThetaHist != 0 ) delete _dThetaHist;

  cout << "Making RooDataHist..." << endl;
  _dThetaHist = new RooDataHist( "dThetaHist", "#Delta#theta", RooArgList( *dTheta ), hist, 1.0 );
  cout << "done!" << endl;

  delete file;
  delete hist;

  this->fit();
}

void DThetaFitter::loadHistogram( TH1D* histogram ) {
  RooRealVar* dTheta = _wspace->var( "dTheta" );

  if ( _dThetaHist != 0 ) delete _dThetaHist;
  _dThetaHist = new RooDataHist( "dThetaHist", "#Delta#theta", RooArgList( *dTheta ), histogram, 1.0 );

  this->fit();
}

void DThetaFitter::buildPdfGauss( int backgroundOrder ) {
  RooRealVar* dTheta = _wspace->var( "dTheta" );

  RooRealVar  gaussMean( "gaussMean", "gaussMean", 0.0, _dThetaMin, _dThetaMax );
  RooRealVar  gaussWidth( "gaussWidth", "gaussWidth", _dThetaMax / 4.0, _dThetaMax / 15.0, _dThetaMax / 2.0 );
  RooGaussian signal( "signal", "signal", *dTheta, gaussMean, gaussWidth );

  RooRealVar    chebyP0( "chebyP0", "chebyP0", 0.0, -0.1, 0.1 );
  RooRealVar    chebyP1( "chebyP1", "chebyP1", 0.0, -0.1, 0.1 );
  RooRealVar    chebyP2( "chebyP2", "chebyP2", 0.0, -0.1, 0.1 );
  RooChebychev* background = 0;
  if ( backgroundOrder == 1 )
    background = new RooChebychev( "background", "background", *dTheta, RooArgList( chebyP0 ) );
  if ( backgroundOrder == 2 )
    background = new RooChebychev( "background", "background", *dTheta, RooArgList( chebyP0, chebyP1 ) );
  if ( backgroundOrder == 3 )
    background = new RooChebychev( "background", "background", *dTheta, RooArgList( chebyP0, chebyP1, chebyP2 ) );

  RooRealVar fraction( "fraction", "fraction", 0.9, 0.05, 0.95 );

  RooAddPdf totalPdf( "totalPdf", "totalPdf", RooArgList( signal, *background ), fraction );

  _wspace->import( totalPdf );
}

void DThetaFitter::buildPdfBifurGauss( int backgroundOrder ) {
  RooRealVar* dTheta = _wspace->var( "dTheta" );

  RooRealVar    gaussMean( "gaussMean", "gaussMean", 0.0, _dThetaMin, _dThetaMax );
  RooRealVar    gaussWidthL( "gaussWidthL", "gaussWidthL", _dThetaMax / 4.0, _dThetaMax / 15.0, _dThetaMax / 2.0 );
  RooRealVar    gaussWidthR( "gaussWidthR", "gaussWidthR", _dThetaMax / 4.0, _dThetaMax / 15.0, _dThetaMax / 2.0 );
  RooBifurGauss signal( "signal", "signal", *dTheta, gaussMean, gaussWidthL, gaussWidthR );

  RooRealVar    chebyP0( "chebyP0", "chebyP0", 0.0, -0.1, 0.1 );
  RooRealVar    chebyP1( "chebyP1", "chebyP1", 0.0, -0.1, 0.1 );
  RooRealVar    chebyP2( "chebyP2", "chebyP2", 0.0, -0.1, 0.1 );
  RooChebychev* background = 0;
  if ( backgroundOrder == 1 )
    background = new RooChebychev( "background", "background", *dTheta, RooArgList( chebyP0 ) );
  if ( backgroundOrder == 2 )
    background = new RooChebychev( "background", "background", *dTheta, RooArgList( chebyP0, chebyP1 ) );
  if ( backgroundOrder == 3 )
    background = new RooChebychev( "background", "background", *dTheta, RooArgList( chebyP0, chebyP1, chebyP2 ) );

  RooRealVar fraction( "fraction", "fraction", 0.15, 0.05, 0.95 );

  RooAddPdf totalPdf( "totalPdf", "totalPdf", RooArgList( signal, *background ), fraction );

  _wspace->import( totalPdf );
}

int DThetaFitter::fit() {
  RooAbsPdf* totalPdf = _wspace->pdf( "totalPdf" );
  cout << "fitting" << endl;
  totalPdf->fitTo( *_dThetaHist );

  return 1;
}

double DThetaFitter::getMean() const {
  RooRealVar* gaussmean = _wspace->var( "gaussMean" );
  return gaussmean->getVal();
}

double DThetaFitter::getMeanErr() const {
  RooRealVar* gaussmean = _wspace->var( "gaussMean" );
  return gaussmean->getError();
}

double DThetaFitter::getWidth() const {
  RooRealVar* gausswidth = _wspace->var( "gaussWidth" );
  return gausswidth->getVal();
}

double DThetaFitter::getWidthErr() const {
  RooRealVar* gausswidth = _wspace->var( "gaussWidth" );
  return gausswidth->getError();
}

double DThetaFitter::plot( string plotDirectory, double plotIfChi2GreaterThan ) {
  RooAbsPdf*  totalPdf  = _wspace->pdf( "totalPdf" );
  RooRealVar* dTheta    = _wspace->var( "dTheta" );
  RooRealVar* gaussMean = _wspace->var( "gaussMean" );

  RooPlot* dThetaPlot = dTheta->frame( 100 );

  _dThetaHist->plotOn( dThetaPlot, RooFit::MarkerColor( kBlack ), RooFit::MarkerSize( 0.1 ), RooFit::Name( "data" ) );

  totalPdf->plotOn( dThetaPlot, RooFit::Components( "background" ), RooFit::LineColor( kRed ),
                    RooFit::LineStyle( kDashed ) );
  totalPdf->plotOn( dThetaPlot, RooFit::Components( "signal" ), RooFit::LineColor( kMagenta ),
                    RooFit::LineStyle( kDashed ) );
  totalPdf->plotOn( dThetaPlot, RooFit::LineColor( kRed ), RooFit::Name( "pdf" ) );

  _dThetaHist->plotOn( dThetaPlot, RooFit::MarkerColor( kBlack ), RooFit::MarkerSize( 0.1 ) );

  RooHist* dThetaPullHist = dThetaPlot->pullHist( "data", "pdf", true );
  dThetaPullHist->SetFillColor( kBlack );
  dThetaPullHist->SetLineColor( kWhite );

  RooPlot* dThetaPullPlot = dTheta->frame( 100 );
  dThetaPullPlot->addObject( dThetaPullHist, "B" );
  dThetaPullPlot->SetMinimum( -5 );
  dThetaPullPlot->SetMaximum( +5 );
  dThetaPullPlot->GetYaxis()->SetTitle( "n_{#sigma}" );
  dThetaPullPlot->GetYaxis()->CenterTitle( true );

  double chi2   = dThetaPlot->chiSquare( "pdf", "data" );
  int    nBin   = dThetaPullPlot->GetNbinsX();
  int    params = totalPdf->getParameters( _dThetaHist )->selectByAttrib( "Constant", kFALSE )->getSize();

  double chi2DOF = chi2 * (double)nBin / (double)( nBin - params );

  if ( plotIfChi2GreaterThan > chi2DOF ) return chi2DOF;

  TString chi2DOFstr = makeString<double>( chi2DOF );
  TLatex  latex( 0.04, 0.02, ( TString ) "Chi2/DOF = " + chi2DOFstr );
  latex.SetNDC( kTRUE );

  TString meanString = makeString<double>( getMean() );
  TString meanError  = makeString<double>( getMeanErr() );
  TLatex  latexMean( 0.5, 0.8, ( TString ) "#mu = " + meanString + " #pm " + meanError );
  latexMean.SetNDC( kTRUE );

  TString widthString = makeString<double>( getWidth() );
  TString widthError  = makeString<double>( getWidthErr() );
  TLatex  latexWidth( 0.5, 0.7, ( TString ) "#sigma = " + widthString + " #pm " + widthError );
  latexWidth.SetNDC( kTRUE );

  TLine* dThetaPullLine = new TLine( _dThetaMin, 0.0, _dThetaMax, 0.0 );
  TLine* dThetaMeanLine = new TLine( gaussMean->getVal(), 0.0, gaussMean->getVal(), dThetaPlot->GetMaximum() );
  dThetaMeanLine->SetLineStyle( 2 );

  string filetype = ".png";

  TCanvas myCanvas( "canvas", "canvas", 1000, 1000 );
  myCanvas.Divide( 1, 2 );

  myCanvas.cd( 1 );
  dThetaPlot->Draw();
  dThetaMeanLine->Draw( "SAME" );
  latexMean.Draw( "SAME" );
  latexWidth.Draw( "SAME" );

  myCanvas.cd( 2 );
  dThetaPullPlot->Draw();
  latex.Draw( "SAME" );

  myCanvas.Print( ( plotDirectory + filetype ).c_str() );

  delete dThetaPullLine;
  delete dThetaMeanLine;

  return chi2DOF;
}

void DThetaFitter::setMean( double mean ) {
  RooRealVar* gaussmean = _wspace->var( "gaussMean" );
  gaussmean->setVal( mean );
}

void DThetaFitter::setConstantMean( double mean ) {
  RooRealVar* gaussmean = _wspace->var( "gaussMean" );
  gaussmean->setVal( mean );
  gaussmean->setConstant( kTRUE );
}

void DThetaFitter::setWidth( double width ) {
  RooRealVar* gausswidth = _wspace->var( "gaussWidth" );
  gausswidth->setVal( width );
}

DThetaFitter::~DThetaFitter() {
  delete _wspace;
  if ( _dThetaHist != 0 ) delete _dThetaHist;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// C++
#include <string>

class MirrComb {
public:
  MirrComb() {
    m_pri = 0;
    m_sec = 0;
  }

  MirrComb( int /*pri*/, int /*sec*/ );

  MirrComb( std::string /*combName*/ );

  double Z();
  void   Z( double z );

  double Y();
  void   Y( double y );

  double ZErr();
  void   ZErr( double zErr );

  double YErr();
  void   YErr( double yErr );

  double Shift();
  void   Shift( double shift );

  double ShiftErr();
  void   ShiftErr( double shiftErr );

  int Pri();
  int Sec();

private:
  double m_z;
  double m_y;
  double m_zErr;
  double m_yErr;
  double m_sinusoidShift;
  double m_sinusoidShiftErr;

  int m_pri;
  int m_sec;
};

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// C++
#include <string>

class IRichMirrCombFit {
public:
  virtual ~IRichMirrCombFit() {}

  virtual void ReadConfFile()                          = 0;
  virtual void GlobalFit()                             = 0;
  virtual void SetInitialSinusoidShift( double shift ) = 0;
  virtual void FitHistos()                             = 0;
  virtual void PrintResults()                          = 0;
  virtual void CombinFitDecision()                     = 0;
};

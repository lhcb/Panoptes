/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local
#include "RichMirrCombFit.h"
#include "DThetaFitter.h"

#include "yaml-cpp/yaml.h"

#include <fstream>
#include <iostream>
#include <regex>
#include <string>

#include <TKey.h>
#include <TList.h>
#include <TPRegexp.h>
#include <TString.h>
#include <boost/format.hpp>

using boost::format;
using namespace std;

//------------------------------------------------------------------------------

RichMirrCombFit::RichMirrCombFit() { cout << "Constructing RichMirrCombFit()" << endl; }

//------------------------------------------------------------------------------

RichMirrCombFit::~RichMirrCombFit() {}

//------------------------------------------------------------------------------

void RichMirrCombFit::FetchRichOpts() {
  cout << "Opening file with RICH reco options: " + m_richOptsFile << endl;
  YAML::Node     opts = YAML::LoadFile( m_richOptsFile );
  vector<string> combsSubset;
  for ( const auto& pair : opts[m_timeStamp]["reco_opts"]["PrebookHistos"] ) {
    string pri  = pair[0].as<string>();
    string sec  = pair[1].as<string>();
    auto   comb = pri + sec;
    combsSubset.push_back( comb );
  }
  sort( combsSubset.begin(), combsSubset.end() );
  /*
  for ( auto& comb : mirrCombsSubset )
    cout << comb << endl;
  */
  m_mirrCombsSubset = combsSubset;
  cout << "Got m_mirrCombsSubset vector" << endl;
}

//------------------------------------------------------------------------------

string RichMirrCombFit::MirrCombName( int pri, int sec ) {
  auto priStr = to_string( pri );
  auto secStr = to_string( sec );
  if ( pri < 10 ) priStr = "0" + priStr;
  if ( sec < 10 ) secStr = "0" + secStr;
  auto combStr = "p" + priStr + "s" + secStr;
  return combStr;
}

//------------------------------------------------------------------------------

void RichMirrCombFit::CreateMapCombNameCombObj() {
  cout << "Creating complete map of mirror-combination-named objects:" << endl;
  for ( const auto& combName : m_mirrCombsSubset ) {
    MirrComb combObj( combName );
    m_mapCombNameCombObj[combName] = combObj;
  }
}

//------------------------------------------------------------------------------

void RichMirrCombFit::HistosList() {
  // Get list of all histograms in the input file.
  // Cross check these histograms against the list of
  // mirror combinations, and keep any that match.
  TList* listOfKeys = gDirectory->GetListOfKeys();
  TIter  next( listOfKeys );
  smatch comb_inH2Name;
  regex  comb_regex( "p[\\d]{2}s[\\d]{2}" );
  // loop over all histograms
  while ( TKey* key = (TKey*)next() ) {
    // work only with TH2D histograms:
    TString className( Form( "%s", key->GetClassName() ) );
    if ( className != "TH2D" ) { continue; }
    // get histogram name and add it to the m_histoNames list
    // if the mirror combination there
    // is in the list of chosen mirror combinations
    string h2Name = (string)Form( "%s", key->GetName() );
    regex_search( h2Name, comb_inH2Name, comb_regex );
    for ( const auto& c : m_mirrCombsSubset ) {
      if ( comb_inH2Name.str() == c ) {
        m_histoNames.push_back( h2Name );
        break;
      }
    }
  }
  // here is the sorted list of all relevant histograms for fitting:
  sort( m_histoNames.begin(), m_histoNames.end() );
}

//------------------------------------------------------------------------------

void RichMirrCombFit::OpenTFile() {
  // Open file and go to directory with ROOT files.
  f = TFile::Open( m_inputHistosFile.c_str() );
  f->cd( ( "/RICH/MirrorAlignLong/Rich" + m_rN + "Gas" ).c_str() );
}

//------------------------------------------------------------------------------

void RichMirrCombFit::FitHistos() {
  // open root file
  this->OpenTFile();
  // get list of histograms
  this->HistosList();
  // if the m_useGlobalFitMean == true, run a global dTheta fit,
  // and use it as initial (possibly fixed) value of shift in the full dPhi dTheta fit
  if ( m_useGlobalFitMean ) {
    TH1D* histoGlobalDeltaTheta = (TH1D*)gDirectory->Get( "../deltaThetaUnamb" );
    this->SetInitialSinusoidShift( this->GetGlobalFitMean( histoGlobalDeltaTheta ) );
  }
  for ( const auto& name : m_histoNames ) {
    smatch sm;
    if ( !regex_search( name, sm, ( regex ) "dTheta" ) ) { continue; }

    cout << name << endl;
    TH2D* histo = (TH2D*)gDirectory->Get( (TString)name );
    if ( m_phiBinFactor != 1 ) { histo->RebinX( m_phiBinFactor ); }
    histo->Print();
    this->BinCounter( histo );
    GetFitParams( histo );
  }
  f->Close();
}

//------------------------------------------------------------------------------

void RichMirrCombFit::BinCounter( TH2D* histoIn ) {
  int   safeBins = 0;
  TH1D* projx    = histoIn->ProjectionX();
  // actual number of bins in phi (should be 20, but could be more or less, depends on version Rich/MirrorAlign)
  int xbins = projx->GetNbinsX();
  // number of bins in deltaTheta (should be 50)
  int ybins = histoIn->GetNbinsY();
  // Now, ideally we want
  // OLD
  //   double want = 16.0;
  //   of every
  //   double have = 20.0;
  // (m_minFracPhiBinsPopulated * 100) percent of the
  // bins in phi to have at least minPhotonsPerPhiBin photons per phi bin.
  // we now set this threshold via minAverageBinPop in the mirror alignment settings file
  // as well as minFracPhiBinsPopulated
  double minPhotonsPerPhiBin = m_minAverageBinPop * ybins;
  // OLD
  // double minPhotonsPerPhiBin = 300.0;

  // Loop over the phi bins, count the number of photons in each phi bin.
  for ( int binNr = 1; binNr <= xbins; binNr++ ) {
    double binCont = projx->GetBinContent( binNr );
    if ( binCont >= minPhotonsPerPhiBin ) {
      safeBins++;
      cout << "Bin " << binNr << " has " << binCont << " entries." << endl;
    } else {
      cout << "Bin " << binNr << " has " << binCont << " entries. ";
      cout << "This is lower than the " << minPhotonsPerPhiBin << " photons needed to include this bin in the fit."
           << endl;
    }
  }
  cout << " " << safeBins << " out of " << xbins << " are populated with ''enough'' (" << minPhotonsPerPhiBin
       << ") photons.";
  double frac = ( (double)safeBins ) / xbins;
  // OLD
  // double threshold = (want/have) - 0.0001; // (the minus 0.01% is just for a safety margin)
  double threshold = m_minFracPhiBinsPopulated - 0.0001; // (the minus 0.01% is just for a safety margin)
  if ( frac < threshold ) {
    cout << " The number of phi bins populated sufficiently is low; this may be not_good_enough for mirror alignment.";
  }
  cout << endl;
}

//------------------------------------------------------------------------------

void RichMirrCombFit::GlobalFit() {
  this->OpenTFile();

  TH1D* histoGlobalDeltaTheta = (TH1D*)gDirectory->Get( "../deltaThetaUnamb" );
  cout << "Getting global delta theta histogram " << histoGlobalDeltaTheta << endl;

  DThetaFitter dThetaFitter( m_begRange, m_endRange );
  cout << "Making fitter " << histoGlobalDeltaTheta << endl;

  if ( m_zeroGlobalFitMean ) { dThetaFitter.setConstantMean( 0.0 ); }

  dThetaFitter.loadHistogram( histoGlobalDeltaTheta );
  cout << "Loading histogram" << endl;

  dThetaFitter.plot( m_plotDir + "Rich" + m_rN + "globalDThetaFit" );

  f->Close();
}

double RichMirrCombFit::GetGlobalFitMean( TH1D* histo ) {
  DThetaFitter dThetaFitter( m_begRange, m_endRange );
  dThetaFitter.loadHistogram( histo );
  if ( m_plotOutputLevel > 0 ) dThetaFitter.plot( m_plotDir + "Rich" + m_rN + "globalDThetaFit_fromGetGlobalFitMean" );

  return dThetaFitter.getMean();
}

//------------------------------------------------------------------------------

void RichMirrCombFit::CombinFitDecision() {
  // Write a file to say if we are going to stop or continue

  double stopTolerance;

  // prepare stopTolerance depending on the m_stopToleranceMode
  if ( m_stopToleranceMode == 0 ) {
    stopTolerance = m_nominalResolutionSigma * m_stopSigmaFraction;
  } else {
    stopTolerance = m_stopTolerance;
  }

  string combinfitdecision( "STOP" );

  for ( auto& it : m_mapCombNameCombObj ) {

    auto sWhere = find( m_mirrCombsSubset.begin(), m_mirrCombsSubset.end(), it.first );
    if ( sWhere == m_mirrCombsSubset.end() ) { continue; }

    if ( fabs( it.second.Y() ) * 1000. > m_warningFactor * stopTolerance ) { // is Y misalignment larger than
                                                                             // m_warningFactor*stopTolerance mrad?
      cout << "WARNING! WARNING! mirror combination " << it.first
           << format( " Y tilt relative to stopTolerance = %| 7.2f|" ) % ( it.second.Y() * 1000. / stopTolerance )
           << endl;
      if ( combinfitdecision == "STOP" || combinfitdecision == "CONTINUE" ) { combinfitdecision = "PAUSE"; };
    } else if ( fabs( it.second.Y() ) * 1000. > stopTolerance ) { // is Y misalignment larger than stopTolerance
                                                                  // mrad?
      cout << "NOTE:             mirror combination " << it.first
           << format( " Y tilt relative to stopTolerance = %| 7.2f|" ) % ( it.second.Y() * 1000. / stopTolerance )
           << endl;
      if ( combinfitdecision == "STOP" ) { combinfitdecision = "CONTINUE"; };
    } else { // Y misalignment is smaller than stopTolerance mrad!
      cout << "OK:               mirror combination " << it.first
           << format( " Y tilt relative to stopTolerance = %| 7.2f|" ) % ( it.second.Y() * 1000. / stopTolerance )
           << endl;
    }

    if ( fabs( it.second.Z() ) * 1000. > m_warningFactor * stopTolerance ) { // is Z misalignment larger than
                                                                             // m_warningFactor*stopTolerance mrad?
      cout << "WARNING! WARNING! mirror combination " << it.first
           << format( " Z tilt relative to stopTolerance = %| 7.2f|" ) % ( it.second.Z() * 1000. / stopTolerance )
           << endl;
      if ( combinfitdecision == "STOP" || combinfitdecision == "CONTINUE" ) { combinfitdecision = "PAUSE"; };
    } else if ( fabs( it.second.Z() ) * 1000. > stopTolerance ) { // is Z misalignment larger than stopTolerance
                                                                  // mrad?
      cout << "NOTE:             mirror combination " << it.first
           << format( " Z tilt relative to stopTolerance = %| 7.2f|" ) % ( it.second.Z() * 1000. / stopTolerance )
           << endl;
      if ( combinfitdecision == "STOP" ) { combinfitdecision = "CONTINUE"; };
    } else { // Z misalignment is smaller than stopTolerance mrad!
      cout << "OK:               mirror combination " << it.first
           << format( " Z tilt relative to stopTolerance = %| 7.2f|" ) % ( it.second.Z() * 1000. / stopTolerance )
           << endl;
    }
  }

  ofstream combinfitdecision_txt;
  combinfitdecision_txt.open( ( m_workDir + "Rich" + m_rN + "_combinfitdecision.txt" ).c_str() );
  combinfitdecision_txt << combinfitdecision << endl;
  combinfitdecision_txt.close();
}

//------------------------------------------------------------------------------

void RichMirrCombFit::ReadConfFile() {

  string rich = "rich" + m_rN;
  cout << "Loading " + rich + "_session_timestamp.yml" << endl;
  const YAML::Node& ts = YAML::LoadFile( rich + "_session_timestamp.yml" );
  m_timeStamp          = ts.as<string>();
  cout << "m_timeStamp = " + m_timeStamp << endl;

  cout << "Loading " + rich + "_fit_align_confs.yml" << endl;
  const YAML::Node& conf = YAML::LoadFile( rich + "_fit_align_confs.yml" );

  m_backgroundOrder         = conf[m_timeStamp]["fit_conf"]["backgroundOrder"].as<int>();
  m_combinFitMethod         = conf[m_timeStamp]["fit_conf"]["combinFitMethod"].as<int>();
  m_deltaThetaWindow        = conf[m_timeStamp]["fit_conf"]["deltaThetaWindow"].as<double>();
  m_fixSinusoidShift        = conf[m_timeStamp]["fit_conf"]["fixSinusoidShift"].as<bool>();
  m_minAverageBinPop        = conf[m_timeStamp]["fit_conf"]["minAverageBinPop"].as<double>();
  m_minFracPhiBinsPopulated = conf[m_timeStamp]["fit_conf"]["minFracPhiBinsPopulated"].as<double>();
  m_nominalResolutionSigma  = conf[m_timeStamp]["fit_conf"]["nominalResolutionSigma"].as<double>();
  m_phiBinFactor            = conf[m_timeStamp]["fit_conf"]["phiBinFactor"].as<int>();
  m_plotDir                 = conf[m_timeStamp]["fit_conf"]["plotDir"].as<string>();
  m_plotOutputLevel         = conf[m_timeStamp]["fit_conf"]["plotOutputLevel"].as<int>();
  m_sinusoidShift           = conf[m_timeStamp]["fit_conf"]["sinusoidShift"].as<double>();
  m_stopSigmaFraction       = conf[m_timeStamp]["fit_conf"]["stopSigmaFraction"].as<double>();
  m_stopTolerance           = conf[m_timeStamp]["fit_conf"]["stopTolerance"].as<double>();
  m_stopToleranceMode       = conf[m_timeStamp]["fit_conf"]["stopToleranceMode"].as<int>();
  m_useGlobalFitMean        = conf[m_timeStamp]["fit_conf"]["useGlobalFitMean"].as<bool>();
  m_warningFactor           = conf[m_timeStamp]["fit_conf"]["warningFactor"].as<double>();
  m_workDir                 = conf[m_timeStamp]["fit_conf"]["workDir"].as<string>();
  m_zeroGlobalFitMean       = conf[m_timeStamp]["fit_conf"]["zeroGlobalFitMean"].as<bool>();

  m_begRange = -m_deltaThetaWindow / 1000.;
  m_endRange = m_deltaThetaWindow / 1000.;

  m_nominSigmRad = m_nominalResolutionSigma / 1000.;

  cout << "Loading " + rich + "_variant.yml" << endl;
  const YAML::Node& var = YAML::LoadFile( rich + "_variant.yml" );
  m_variant             = var.as<string>();
  cout << "m_variant = " + m_variant << endl;

  cout << "Loading " + rich + "_iter_number.yml" << endl;
  const YAML::Node& iter = YAML::LoadFile( rich + "_iter_number.yml" );
  m_iterNumber           = iter.as<int>();
  cout << "m_iterNumber = " + to_string( m_iterNumber ) << endl;

  cout << "m_tiltName = " + m_tiltName << endl;

  string iN = "";
  if ( m_tiltName == "" ) {
    iN = "_i" + to_string( m_iterNumber );
  } else {
    iN = "_" + m_tiltName + "_i" + to_string( m_iterNumber );
  }

  m_richOptsFile = rich + "_reco_opts.yml"; // file with subset of mirror combinations
  // get list of mirror combinations
  cout << "Get FetchRichOpts from m_richOptsFile = " + m_richOptsFile << endl;
  this->FetchRichOpts();

  m_inputHistosFile          = m_variant + "_histos" + iN + ".root"; // input ROOT file
  m_mirrCombinFitResultsFile = m_variant + "_fit_res" + iN + ".txt"; // output: histos fit results

  cout << "m_inputHistosFile = " + m_inputHistosFile << endl;
  cout << "m_mirrCombinFitResultsFile = " + m_mirrCombinFitResultsFile << endl;
}
//------------------------------------------------------------------------------

void RichMirrCombFit::PrintResults() {
  ofstream outputFile( m_mirrCombinFitResultsFile.c_str() );
  if ( outputFile.is_open() ) {
    for ( const auto& comb : m_mirrCombsSubset ) {

      cout << comb << "  " << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].Y() * 1000. ) << "  "
           << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].YErr() * 1000. ) << "  "
           << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].Z() * 1000. ) << "  "
           << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].ZErr() * 1000. ) << "  "
           << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].Shift() * 1000. ) << "  "
           << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].ShiftErr() * 1000. ) << endl;

      outputFile << comb << "  " << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].Y() * 1000. ) << "  "
                 << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].YErr() * 1000. ) << "  "
                 << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].Z() * 1000. ) << "  "
                 << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].ZErr() * 1000. ) << "  "
                 << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].Shift() * 1000. ) << "  "
                 << format( "%| 10.4f|" ) % ( m_mapCombNameCombObj[comb].ShiftErr() * 1000. ) << endl;
    }
  }
}

//------------------------------------------------------------------------------

void RichMirrCombFit::SetInitialSinusoidShift( double shift ) { m_sinusoidShift = shift; }

//------------------------------------------------------------------------------

void RichMirrCombFit::Print( ostream& out ) const {
  out << "test" << endl;
  /*
  for ( auto& it : m_mapCombNameCombObj ) {
    out
    <<it.first<<"  "
    <<format("%| 10.4f|")%(it.second.Y()    *1000.)<<"  "<<format("%| 10.4f|")%(it.second.YErr()    *1000.)<<"  "
    <<format("%| 10.4f|")%(it.second.Z()    *1000.)<<"  "<<format("%| 10.4f|")%(it.second.ZErr()    *1000.)<<"  "
    <<format("%| 10.4f|")%(it.second.Shift()*1000.)<<"  "<<format("%| 10.4f|")%(it.second.ShiftErr()*1000.);
  }
  */
}

//------------------------------------------------------------------------------

ostream& operator<<( ostream& out, const RichMirrCombFit& fit ) {
  fit.Print( out );
  return out;
}

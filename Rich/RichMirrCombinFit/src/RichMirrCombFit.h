/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Local
#include "IRichMirrCombFit.h"
#include "MirrComb.h"
// C++
#include <map>
#include <ostream>
#include <string>
#include <vector>
// ROOT
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

typedef std::map<std::string, MirrComb> MapCombNameCombObj;

class RichMirrCombFit : public IRichMirrCombFit {
public:
  RichMirrCombFit();
  virtual ~RichMirrCombFit();

  void ReadConfFile() override;
  void GlobalFit() override;
  void SetInitialSinusoidShift( double shift ) override;
  void FitHistos() override;
  void PrintResults() override;
  void CombinFitDecision() override;

  void         BinCounter( TH2D* );
  void         FetchRichOpts();
  virtual void GetFitParams( TH2D* ) = 0;
  double       GetGlobalFitMean( TH1D* histo );
  void         HistosList();
  std::string  MirrCombName( int pri, int sec );
  void         Print( std::ostream& out ) const;

private:
  void OpenTFile();

  TFile* f = nullptr;

protected:
  void CreateMapCombNameCombObj();
  // ATTENTION: for observability, please any future
  // additions below keep in the alphabetic order !!!
  int                      m_backgroundOrder{ 0 };
  double                   m_begRange{ 0 };
  int                      m_combinFitMethod{ 0 };
  double                   m_deltaThetaWindow{ 0 };
  double                   m_endRange{ 0 };
  bool                     m_fixSinusoidShift{ 0 };
  std::vector<std::string> m_histoNames;
  std::string              m_inputHistosFile;
  int                      m_iterNumber{ 0 };
  MapCombNameCombObj       m_mapCombNameCombObj;
  int                      m_minAverageBinPop{ 0 };
  double                   m_minFracPhiBinsPopulated{ 0 };
  std::string              m_mirrCombinFitResultsFile;
  std::vector<std::string> m_mirrCombsSubset;
  double                   m_nominSigmRad{ 0 };
  double                   m_nominalResolutionSigma{ 0 };
  std::string              m_outputResultsFile;
  int                      m_phiBinFactor{ 0 };
  std::string              m_plotDir;
  int                      m_plotOutputLevel{ 0 };
  std::string              m_rN;
  std::string              m_richOptsFile;
  double                   m_sinusoidShift{ 0 };
  double                   m_stopSigmaFraction{ 0 };
  int                      m_stopToleranceMode{ 0 };
  double                   m_stopTolerance{ 0 };
  std::string              m_tiltName;
  std::string              m_timeStamp;
  bool                     m_useGlobalFitMean{ 0 };
  std::string              m_variant;
  double                   m_warningFactor{ 0 };
  std::string              m_workDir;
  bool                     m_zeroGlobalFitMean{ 0 };
};

std::ostream& operator<<( std::ostream& out, const RichMirrCombFit& fit );

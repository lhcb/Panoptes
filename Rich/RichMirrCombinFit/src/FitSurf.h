/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// ROOT
#include <TMath.h>
/*
  double fitSurf( double* x, double* params ) {

  double sinusoidGaussMean = params[0] * TMath::Cos(x[0]
                           + params[1] * TMath::Sin(x[0])
                           + params[2];

  double gauss = 0.0;
  gauss = params[4]
        / ( params[3]*TMath::Sqrt(2.0*TMath::Pi()) )
        * TMath::Exp( -0.5*TMath::Power((x[1] - sinusoidGaussMean)/params[3], 2.0) );

  double backgrPolynomSurf = params[5]
                           + params[6]  * x[0]
                           + params[7]  * x[1]
                           + params[8]  * TMath::Power(x[0], 2.0)
                           + params[9]  * TMath::Power(x[1], 2.0)
                           + params[10] * x[0] * x[1];

  double fitVal = backgrPolynomSurf + gauss;
  return fitVal;
}
*/
// Different Gaussian Widths for each phi bin

class FitSurfBM {
  // fit dTheta vs Phi distribution: all slices are fitted simultaneously
  // with their Gaussian means lying on a sinusoid(phi) aka "bound means"
  // while all other parameters are individual for each slice
public:
  // constructor
  FitSurfBM( int numberOfPhiBins ) : phiBins( numberOfPhiBins ) {}
  // the fitting function
  double FitFcnBM( double* x, double* params ) const {
    double sinusoidGaussMean = params[0] * TMath::Cos( x[0] ) + params[1] * TMath::Sin( x[0] ) + params[2];

    double binWid = 2.0 * TMath::Pi() / ( (double)phiBins );
    int    phibin = (int)TMath::Floor( x[0] / binWid ) + 1; // e.g. from 1 to 20

    double gauss = 0.0;
    gauss        = params[2 + phiBins + phibin] // constant terms follow phiBins sigmas
                                                //    / ( params[2+phibin]*TMath::Sqrt(2.0*TMath::Pi()) )
            * TMath::Exp( -0.5 * TMath::Power( ( x[1] - sinusoidGaussMean ) / params[2 + phibin], 2.0 ) );

    double backgrPolynom = params[2 + phiBins * 2 + phibin] + params[2 + phiBins * 3 + phibin] * x[1] +
                           params[2 + phiBins * 4 + phibin] * TMath::Power( x[1], 2.0 );

    double fitVal = gauss + backgrPolynom;
    return fitVal;
  }

  int phiBins{ 0 };
};

// unified Gaussian Widths for all phi bins

class FitSurfBMUW {
  // fit dTheta vs Phi distribution: all slices are fitted simultaneously
  // with their Gaussian means lying on a sinusoid(phi) aka "bound means"
  // while all other parameters are individual for each slice
  // except Gaussian widths that are all one common parameter
  // aka "unified widths"
public:
  // constructor
  FitSurfBMUW( int numberOfPhiBins ) : phiBins( numberOfPhiBins ) {}
  // the fitting function
  double FitFcnBMUW( double* x, double* params ) const {
    double sinusoidGaussMean = params[0] * TMath::Cos( x[0] ) + params[1] * TMath::Sin( x[0] ) + params[2];

    double binWid = 2.0 * TMath::Pi() / ( (double)phiBins );
    int    phibin = (int)TMath::Floor( x[0] / binWid ) + 1; // e.g. from 1 to 20

    double gauss = 0.0;
    gauss        = params[3 + phibin] // constant terms follow unified sigma [3]
                                      //    / ( params[3]*TMath::Sqrt(2.0*TMath::Pi()) )
            * TMath::Exp( -0.5 * TMath::Power( ( x[1] - sinusoidGaussMean ) / params[3], 2.0 ) );

    double backgrPolynom = params[3 + phiBins + phibin] + params[3 + phiBins * 2 + phibin] * x[1] +
                           params[3 + phiBins * 3 + phibin] * TMath::Power( x[1], 2.0 );

    double fitVal = gauss + backgrPolynom;
    return fitVal;
  }

  int phiBins{ 0 };
};

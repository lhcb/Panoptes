#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import getopt
import os
import sys
from subprocess import *

# print sys.argv[0:]

varOpts = getopt.getopt(
    sys.argv[1:],
    "h:p:u:v:o:w:r:d:i:f:",
    [
        "home=",
        "pathToScripts=",
        "userReleaseArea=",
        "setupProjectPanoptesVersion=",
        "setupProjectPanoptesOptions=",
        "workDir=",
        "rich=",
        "deltaThetaWindow=",
        "histoFile=",
        "outputFile=",
    ],
)
# print varOpts

for varOptKey, varOptValue in varOpts[0]:
    if varOptKey in ["-h", "--home"]:
        home = varOptValue
    elif varOptKey in ["-p", "--pathToScripts"]:
        pathToScripts = varOptValue
    elif varOptKey in ["-u", "--userReleaseArea"]:
        userReleaseArea = varOptValue
    elif varOptKey in ["-v", "--setupProjectPanoptesVersion"]:
        setupProjectPanoptesVersion = varOptValue
    elif varOptKey in ["-o", "--setupProjectPanoptesOptions"]:
        setupProjectPanoptesOptions = varOptValue
    elif varOptKey in ["-w", "--workDir"]:
        workDir = varOptValue
    elif varOptKey in ["-r", "--rich"]:
        rich = varOptValue
    elif varOptKey in ["-d", "--deltaThetaWindow"]:
        deltaThetaWindow = varOptValue
    elif varOptKey in ["-i", "--histoFile"]:
        histoFile = varOptValue
    elif varOptKey in ["-f", "--outputFile"]:
        FitResultsFile = varOptValue

p = Popen(
    "export   HOME="
    + home
    + ";"
    + "source "
    + pathToScripts
    + "/LbLogin.sh;"
    + "export   User_release_area="
    + userReleaseArea
    + ";"
    + "source  `which SetupProject.sh`  Panoptes "
    + setupProjectPanoptesVersion
    + " "
    + setupProjectPanoptesOptions
    + ";"
    + "$RICHMIRRCOMBINFITROOT/$CMTCONFIG/RichHPDFit.exe "
    + rich
    + " "
    + deltaThetaWindow
    + " "
    + histoFile
    + " "
    + FitResultsFile
    + " > "
    + workDir
    + "/Rich"
    + rich
    + "HPDFit.out",
    shell=True,
    executable="/bin/bash",
)
sts = os.waitpid(p.pid, 0)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from subprocess import *

reactivate()

userReleaseArea = os.getenv("User_release_area")
home = "/afs/cern.ch/user/a/asolomin"
setupProjectPanoptesVersion = "HEAD"
setupProjectPanoptesOptions = "--nightly lhcb-prerelease"
# setupProjectPanoptesOptions = "--nightly lhcb-prerelease Wed"

pathToScripts = "/afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts"

p = Popen(
    "set HOME =                                "
    + home
    + ";"
    + "source "
    + pathToScripts
    + "/LbLogin.sh;"
    + "set User_release_area =                   "
    + userReleaseArea
    + ";"
    + "source `which SetupProject.sh`            Panoptes "
    + setupProjectPanoptesVersion
    + " "
    + setupProjectPanoptesOptions
    + ";"
    + "rm                                     -f env_var_settings.txt;"
    + "echo   $RICHMIRRORALIGNMENTGANGAROOT    > env_var_settings.txt ",
    shell=True,
    executable="/bin/bash",
)
sts = os.waitpid(p.pid, 0)

env_var_settings_txt = open("env_var_settings.txt")
RICHMIRRORALIGNMENTGANGAROOT = env_var_settings_txt.readline().strip()
env_var_settings_txt.close()

scriptOfEverything = RICHMIRRORALIGNMENTGANGAROOT + "/job/RichMirrorAlignmentGanga.py"

workDir = "/afs/cern.ch/user/a/asolomin/public/rich_align/workdir"

# dDDB_tag                         = "head-20100119"
dDDB_tag = "head-20100407"
# dDDB_tag                         = "head-20100906"

# localCondDB_tag                  = "rich-20100113"

condDB_tag = "sim-20100412-vc-md100"
# condDB_tag                       = "sim-20100831-vc-md100"

# setupProjectBrunelOpts           = "--use-grid"
setupProjectBrunelOpts = ""

# eventSelectorInputFile           =  RICHMIRRORALIGNMENTGANGAROOT+"/options/EventSelectorInput_R2Sel100f7TeV_MC10.py"
eventSelectorInputFile = "EventSelectorInput_R2Sel100f7TeV_MC10.py"

# brunelEvtMax                     =  10
# brunelPrintFreq                  =  1
# splitMaxFiles                    =  1
# brunelEvtMax                     =  100
# brunelPrintFreq                  =  10
# splitMaxFiles                    =  6
brunelEvtMax = -1
brunelPrintFreq = 100
splitMaxFiles = 1000

# splitFilesPerJob                 =  1
# splitFilesPerJob                 =  4
splitFilesPerJob = 2

# backEnd                          = "LSF_8nm"
# backEnd                          = "LSF_1nh"
# backEnd                          = "local"
backEnd = "dirac"

# diracCPUTime                     =  1200
# diracCPUTime                     =  2400
# diracCPUTime                     =  9600
diracCPUTime = 19200

brunelInitialInputType = "DST"
messageSvcOutputLevel = 3

# mirrCombinSubset                 = "10m12c_p0p2fix" # required for RICH1
# mirrCombinSubset                 = "48m85c_p12p43fix"
# mirrCombinSubset                 = "48m85c_p12p43s08s31fix"
mirrCombinSubset = "48m48c_p12p43s09s30fix"

thisCase = "100fSurB"

dataVariant = "MC10"

groupSlices = 1
groupBins = 1
# minAverageBinPop                 =  25
minAverageBinPop = 9
deltaThetaWindow = 4
# deltaThetaWindow                 =  3 # mrad

coeffCalibrTilt = 0.3

# useTruth                         = "MCTruP"
useTruth = ""

useOffsetsFromMC = "false"
# useOffsetsFromMC                 = "true"

verOffsetsFromMC = ""
# verOffsetsFromMC                 = "100fSurBMC10"

usePremisaligned = "false"
# usePremisaligned                 = "true"

verPremisaligned = ""
# verPremisaligned                 = "Pre" # premisaligned but fixed mirrors are
# zeros
# verPremisaligned                 = "Prf" # premisaligned and fixed mirrors'
# tilts at iteration 0 are equal to
# initial tilts
# verPremisaligned                 = "Pri" # premisaligned and all initial values
# at iteration 0 are equal to initial
# tilts

# combinFitVariant                 = "slices"
combinFitVariant = "surfaceB"

maximumNumberOfIterations = 1
# maximumNumberOfIterations        =  3
# maximumNumberOfIterations        =  5

startFromIteration = 0
# startFromIteration               =  1

richDetector = 2  # 1: RICH1, 2: RICH2

# 0: universal magnification coefficients for all pairs
#    fixed, i.e. averaged from MC
#    indepedent of the sign of rotation
# 1: individual for each pair
#    fixed i.e. evaluated from MC
#    depedent on sign of rotation
magnifCoeffMode = 2  # 2: individual for each pair
#    evaluated on-fly for each iteration
#    depedent on sign of rotation

# 0: Minuit
solutionMethod = 1  # 1: algebraic

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from subprocess import *

reactivate()

userReleaseArea = os.getenv("User_release_area")
RICHMIRRORALIGNMENTGANGAROOT = os.getenv("RICHMIRRORALIGNMENTGANGAROOT")
RICHMIRRALIGNROOT = os.getenv("RICHMIRRALIGNROOT")
RICHMIRRCOMBINFITROOT = os.getenv("RICHMIRRCOMBINFITROOT")
SQLITEDBPATH = os.getenv("SQLITEDBPATH")

home = "/afs/cern.ch/user/a/auser"
setupProjectPanoptesVersion = "v4r1"
# setupProjectPanoptesOptions = "--nightly lhcb-prerelease"
# setupProjectPanoptesOptions  = "--nightly lhcb-prerelease Sun"
setupProjectPanoptesOptions = ""

pathToScripts = "/afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts"

# os.system("rm                                     -f env_var_settings.txt")
# os.system("echo   $RICHMIRRORALIGNMENTGANGAROOT    > env_var_settings.txt")
p = Popen(
    "export HOME="
    + home
    + ";"
    #         +"export CMTPROJECTPATH=/afs/cern.ch/sw/lcg/external/xqilla/2.2.4/x86_64-slc5-gcc43-opt/lib:$CMTPROJECTPATH;"
    + "source "
    + pathToScripts
    + "/LbLogin.sh;"
    + "export User_release_area="
    + userReleaseArea
    + ";"
    + "source `which SetupProject.sh`           Panoptes "
    + setupProjectPanoptesVersion
    + " "
    + setupProjectPanoptesOptions
    + ";"
    + "source $User_release_area/Panoptes_"
    + setupProjectPanoptesVersion
    + "/Rich/RichMirrAlign/cmt/setup.sh;"
    + "rm                                     -f env_var_settings.txt;"
    + "echo   $RICHMIRRORALIGNMENTGANGAROOT    > env_var_settings.txt ",
    shell=True,
    executable="/bin/bash",
)
sts = os.waitpid(p.pid, 0)

env_var_settings_txt = open("env_var_settings.txt")
RICHMIRRORALIGNMENTGANGAROOT = env_var_settings_txt.readline().strip()
env_var_settings_txt.close()

scriptOfEverything = RICHMIRRORALIGNMENTGANGAROOT + "/job/RichMirrorAlignmentGanga.py"

workDir = "/afs/cern.ch/user/a/auser/scratch0/workdir_R1"

dDDB_tag = "head-20111102"
# dDDB_tag                         = "head-20100119"
# dDDB_tag                         = "head-20100624" # EventSelectorInput_R2SelEnh_Collision10.py
# dDDB_tag                         = "head-20101026" # EventSelectorInput_R2md198f_Collision10.py
# dDDB_tag                          = "head-20101026" # EventSelectorInput_R2Sel199f_Collision10.py

condDB_tag = "head-20111111"
# condDB_tag                       = "head-20100325"
# condDB_tag                       = "head-20100303"
# condDB_tag                       = "head-20100715" # EventSelectorInput_R2SelEnh_Collision10.py
# condDB_tag                       = "head-20101106" # EventSelectorInput_R2md198f_Collision10.py
# condDB_tag                        = "head-20101106" # EventSelectorInput_R2Sel199f_Collision10.py

# setupProjectBrunelOpts           = "--use-grid"
setupProjectBrunelOpts = ""

# eventSelectorInputFile           =  RICHMIRRORALIGNMENTGANGAROOT+"/options/EventSelectorInput_R2SelEnh_Collision10.py"
# eventSelectorInputFile           = "EventSelectorInput_R2SelEnh_Collision10.py"
# eventSelectorInputFile           = "EventSelectorInput_R2md198f_Collision10.py"
eventSelectorInputFile = "LFN-MagUpMagDown_2011.py"

# brunelEvtMax                     =  10
# brunelPrintFreq                  =  1
# splitMaxFiles                    =  1
# brunelEvtMax                     =  100
# brunelPrintFreq                  =  10
# splitMaxFiles                    =  6
brunelEvtMax = 1500
brunelPrintFreq = 500
splitMaxFiles = 200

# splitFilesPerJob                 =  1
splitFilesPerJob = 2
# splitFilesPerJob                 =  4

# backEnd                          = "LSF_8nm"
# backEnd                          = "LSF_8nh"
# backEnd                          = "local"
backEnd = "dirac"
# backEnd                           = "interactive"

# diracCPUTime                     =  1200
# diracCPUTime                     =  2400
# diracCPUTime                     =  9600
diracCPUTime = 19200

brunelInitialInputType = "RAW"
messageSvcOutputLevel = "3"

richDetector = 1  # 1: RICH1, 2: RICH2

mirrCombinSubset = ""
if richDetector == 2:
    mirrCombinSubset = "48m48c_p12p43s09s30fix"
    os.system(
        "cp $RICHMIRRORALIGNMENTGANGAROOT/files/Rich2MirrCombinList_48m48c_p12p43s09s30fix.txt ."
    )

if richDetector == 1:
    mirrCombinSubset = "Rich1MirrCombinList_10m12c_p0p2fix.txt"
    os.system(
        "cp $RICHMIRRORALIGNMENTGANGAROOT/files/Rich1MirrCombinList_10m12c_p0p2fix.txt ."
    )

# mirrCombinSubset                 = "10m12c_p0p2fix" # required for RICH1
# mirrCombinSubset                 = "48m85c_p12p43fix"
# mirrCombinSubset                 = "48m85c_p12p43s08s31fix"
# mirrCombinSubset                  = "48m48c_p12p43s09s30fix"

# thisCase                         = "R1SelMd"      # Rich1
# thisCase                         = "R2SelEnhSurB" # Rich2
# thisCase                         = "198fSurB"     # Rich2
thisCase = "R1MU"  # Rich1

dataVariant = "Collision11"

groupSlices = 1
groupBins = 1
# minAverageBinPop                 =  25
minAverageBinPop = 9

deltaThetaWindow = 4.0  # 4
if richDetector == 2:
    deltaThetaWindow = 3.0  # 2

coeffCalibrTilt = 0.3

useTruth = ""

useOffsetsFromMC = "false"
# useOffsetsFromMC                 = "true"

verOffsetsFromMC = ""
# verOffsetsFromMC                 = "100fSurBMC10"

usePremisaligned = "false"

combinFitVariant = 1  # "slices"
if richDetector == 2:
    combinFitVariant = 3  # "surfaceB"   RICH2

startFromIteration = 0

maximumNumberOfIterations = 5

# 0: universal magnification coefficients for all pairs
#    fixed, i.e. averaged from MC
#    indepedent of the sign of rotation
# 1: individual for each pair
#    fixed i.e. evaluated from MC
#    depedent on sign of rotation
magnifCoeffMode = 0  # 2: individual for each pair
#    evaluated on-fly for each iteration
#    depedent on sign of rotation

if magnifCoeffMode == 0:
    os.system("cp $RICHMIRRORALIGNMENTGANGAROOT/files/MagCoeffs/* .")
    # 0: Minuit
solutionMethod = 1  # 1: algebraic

date = "1341100800000000000"

alignHPDs = "false"

AddedDB = [""]

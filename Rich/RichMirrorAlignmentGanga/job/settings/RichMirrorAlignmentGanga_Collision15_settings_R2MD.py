###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from subprocess import *


# ===============================================================================
# defs
# ===============================================================================
# stdReporter returns stdout and stderr from Popen.communicate()
def stdReporter(stdout, stderr):
    print("The shell script gave some STANDARD OUTPUT:")
    print(stdout)
    if stderr:
        print("The shell script also gave some STANDARD ERROR:")
        print(stderr)
    else:
        print("The shell script ran fine.")


# ===============================================================================

########################################################
# Search "Important" to find things you had better check
# ("Important" things should mostly be in this section
########################################################

# This is an example file for 2015. Databases below may not correct! Please check before running!

### Important: What is the backend
backEnd = "dirac"
# backEnd                           = "online"

### Important: Set your home directory
home = "/usera/naik/claire"  # point this to your home directory
if backEnd.lower() == "online":
    home = ""  # point this to the online home

### Important: Choose Panoptes Version (HEAD is usually fine, for Offline Alignment, given that you run from one of the nightly builds)
setupProjectPanoptesVersion = "HEAD"
# setupProjectPanoptesVersion  = "v5r4"
if backEnd.lower() == "online":
    setupProjectPanoptesVersion = "v5r5"  # you want a fixed version online

### Important: Choose Panoptes Options

setupProjectPanoptesOptions = "--nightly lhcb-head Sat"  # Safest to specify a day that you know is working; try the previous day in general
# setupProjectPanoptesOptions = ""
if backEnd.lower() == "online":
    setupProjectPanoptesOptions = ""

### Important: Define a working directory. If it does not exist, make it.
workDir = "/usera/naik/claire/RichAlignment/2015/output/2015Down_R2"  # Point this to your working directory
if backEnd.lower() == "online":
    workDir = ""
if not os.path.isdir(workDir):
    print("*** DOES NOT EXIST: workDir = " + workDir)
    print("*** Making " + workDir)
    os.makedirs(workDir)

### Important: Get the database tags right
dDDB_tag = "dddb-20150526"
condDB_tag = "cond-20150601"

### Important: Set the LHCb and Brunel version and Brunel extra options
LHCbVersion = ""
if backEnd.lower() == "online":
    #    LHCbVersion                       = 'v36r2'
    LHCbVersion = ""
brunelVersion = "v47r5"
setupProjectBrunelOpts = ""

### Important: Where is the data
ioHelperInputFile = "/afs/cern.ch/user/p/pnaik/ReduceRawFiles/2015CalibrationMagDown.py"  # make sure this exists
if backEnd.lower() == "online":
    ioHelperInputFile = ""  # we will be using the data from the HLT

### Important: What is the input type of your data
brunelInitialInputType = "RAW"
if backEnd.lower() == "online":
    brunelInitialInputType = "RAW"

### Important: Which RICH?
richDetector = 2  # 1: RICH1, 2: RICH2

### Important: The name you choose for this alignment (This is not critical, but it helps in identification.)
thisCase = "2015Down_R2"  # Rich2

### Important: The dataVariant (e.g. "Collision12")
dataVariant = "Collision15"

### Important: Make sure this is correct for whatever you are aligning... (PN: not sure yet how critical this is)
date = "1433376000000000000"

### Important: What are the Added DBs? (NOTE: reading local DBs is currently untested.... please do sometime, then remove this note)
AddedDB = [""]

### Important: Do you want to start from an existing alignment?
useSpecificAlignment = "false"
specificAlignment = (
    "/var/nwork/pckw/naik/claire/RichAlignment/2012/jobs/CondDB_IOV4_R2.xml"
)

########################################################
# Standard items that could be changed, if need be
# Tread carefully
########################################################

### This is standard, do not change
userReleaseArea = os.getenv("User_release_area")
pathToScripts = "/afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts"
if backEnd.lower() == "online":
    pathToScripts = ""

### This gets the environment variables we will need automatically, based on the above
p = Popen(
    "export HOME="
    + home
    + ";"
    + "source "
    + pathToScripts
    + "/LbLogin.sh;"
    + "export User_release_area="
    + userReleaseArea
    + ";"
    #         +"export CMTPROJECTPATH=/afs/cern.ch/sw/lcg/external/xqilla/2.2.4/x86_64-slc5-gcc43-opt/lib:$CMTPROJECTPATH;" # probably obsolete
    + "source `which SetupProject.sh` Panoptes "
    + setupProjectPanoptesVersion
    + " "
    + setupProjectPanoptesOptions
    + ";"
    + "rm                                     -f env_var_settings_mirroralign.txt;"
    + "echo   $RICHMIRRALIGNROOT               > env_var_settings_mirroralign.txt; "  # Line 1
    + "echo   $RICHMIRRCOMBINFITROOT          >> env_var_settings_mirroralign.txt; "  # Line 2
    + "echo   $RICHMIRRORALIGNMENTGANGAROOT   >> env_var_settings_mirroralign.txt; "  # Line 3
    + "echo   $SQLITEDBPATH                   >> env_var_settings_mirroralign.txt; "  # Line 4
    + "echo $SHELL",
    shell=True,
    executable="/bin/bash",
    stdout=PIPE,
    stderr=PIPE,
)
(stdout, stderr) = p.communicate()
stdReporter(stdout, stderr)
env_var_settings_mirroralign_txt = open("env_var_settings_mirroralign.txt")
RICHMIRRALIGNROOT = env_var_settings_mirroralign_txt.readline().strip()  # Line 1
RICHMIRRCOMBINFITROOT = env_var_settings_mirroralign_txt.readline().strip()  # Line 2
RICHMIRRORALIGNMENTGANGAROOT = (
    env_var_settings_mirroralign_txt.readline().strip()
)  # Line 3
SQLITEDBPATH = env_var_settings_mirroralign_txt.readline().strip()  # Line 4
env_var_settings_mirroralign_txt.close()
os.system("rm -f env_var_settings_mirroralign.txt")

### This adds a separator (os.path.sep) to the directory paths, if there is no separator there already.
if not workDir.endswith(os.path.sep):
    workDir += os.path.sep
if not home.endswith(os.path.sep):
    home += os.path.sep
if not userReleaseArea.endswith(os.path.sep):
    userReleaseArea += os.path.sep
if not pathToScripts.endswith(os.path.sep):
    pathToScripts += os.path.sep
if not RICHMIRRALIGNROOT.endswith(os.path.sep):
    RICHMIRRALIGNROOT += os.path.sep
if not RICHMIRRCOMBINFITROOT.endswith(os.path.sep):
    RICHMIRRCOMBINFITROOT += os.path.sep
if not RICHMIRRORALIGNMENTGANGAROOT.endswith(os.path.sep):
    RICHMIRRORALIGNMENTGANGAROOT += os.path.sep
if not SQLITEDBPATH.endswith(os.path.sep):
    SQLITEDBPATH += os.path.sep

### To run over all events, we set brunelEvtMax to -1
brunelEvtMax = -1

### Standard: These numbers need to be adjusted carefully!
brunelPrintFreq = 500
splitMaxFiles = 1000
splitFilesPerJob = 5

### If we use Dirac
diracCPUTime = 19200

### Standard
messageSvcOutputLevel = "3"

### Automatic choice of mirrCombinSubset file
mirrCombinSubset = ""
if richDetector == 2:
    mirrCombinSubset = "Rich2CombAndMirrSubsets_96m94c_p12p43fix.txt"  # not yet available in PANOPTES HEAD, so I don't try to read it using os.system on the next line
    # os.system("cp "+RICHMIRRORALIGNMENTGANGAROOT+"files/Rich2CombAndMirrSubsets_96m94c_p12p43fix.txt .")
    os.system(
        "cp "
        + RICHMIRRALIGNROOT
        + "Test/Rich2CombAndMirrSubsets_96m94c_p12p43fix.txt "
        + workDir
    )  # Requires RICHMIRRALIGNROOT to be defined
if richDetector == 1:
    mirrCombinSubset = "Rich1CombAndMirrSubsets_20m16c_p0p1p2p3fix.txt"
    # os.system("cp "+RICHMIRRORALIGNMENTGANGAROOT+"files/Rich1CombAndMirrSubsets_20m16c_p0p1p2p3fix.txt .")
    os.system(
        "cp "
        + RICHMIRRALIGNROOT
        + "Test/Rich1CombAndMirrSubsets_20m16c_p0p1p2p3fix.txt "
        + workDir
    )  # Requires RICHMIRRALIGNROOT to be defined

### Standard RICH mirror Alignment parameters, do not touch unless you are an expert
groupSlices = 1
groupBins = 1
# minAverageBinPop                  =  6 # 6 * 50 deltaTheta bins in one phi bin slice = 300 photons minimum per phi bin
minAverageBinPop = (
    2.0  # 2 * 50 deltaTheta bins in one phi bin slice = 100 photons minimum per phi bin
)

deltaThetaWindow = 4.0  # 4
if richDetector == 2:
    deltaThetaWindow = 3.0  # 2

coeffCalibrTilt = 0.3
# if (richDetector == 1):
#    coeffCalibrTilt                   =  0.7

useTruth = ""

useOffsetsFromMC = "false"
verOffsetsFromMC = ""

usePremisaligned = "false"

# combinFitVariant                  = 1#"slices"
combinFitVariant = 3  # "slices"
if richDetector == 2:
    combinFitVariant = 3  # "surfaceB"   RICH2

maximumNumberOfIterations = 10

### Change these if something bad happens in an iteration, but it is recoverable (i.e. Ganga jobs failed)
startFromIteration = 0  # which iteration do you want to start from? NEED TO CHECK/CONFIRM THIS WORKS FOR ANYTHING OTHER THAN 0
skipFirstRecoJob = (
    0  # currently only for iteration 0, skip submitting Brunel jobs to grid
)
skipFirstMergeJob = (
    0  # currently only for iteration 0, skip the merging Brunel histograms
)
if skipFirstRecoJob == 0:
    if skipFirstMergeJob == 1:
        skipFirstMergeJob == 0
# if you are not skipping brunel jobs, you do not want to skip the merging of the brunel jobs!
# if you want to merge the jobs from a previous run of the script you must provide the job numbers
previousJobNumbers = list(range(57, 66))

skipFirstCombinFit = (
    0  # currently only for iteration 0, skip the combinFit before mirrAlign
)

### Standard RICH mirror Alignment parameters, do not touch unless you are an expert

# 0: universal magnification coefficients for all pairs
#    fixed, i.e. averaged from MC
#    indepedent of the sign of rotation
# 1: individual for each pair
#    fixed i.e. evaluated from MC
#    depedent on sign of rotation
magnifCoeffMode = 2  # 2: individual for each pair
#    evaluated on-fly for each iteration
#    depedent on sign of rotation
if magnifCoeffMode == 0:
    os.system(
        "cp " + RICHMIRRORALIGNMENTGANGAROOT + "/files/MagCoeffs/* " + workDir
    )  # Requires RICHMIRRORALIGNMENTGANGAROOT to be defined

    # 0: Minuit
solutionMethod = 1  # 1: algebraic

### Standard RICH mirror Alignment parameter, do not touch unless you are an expert (PN: We no longer align HPDs)
alignHPDs = "false"

# Line for experts (for now)
justGlobalFit = 0  # 0 is typically what you want.
# 1 is used to produce the combined fit to the sum of all delta(theta) histograms.

# Don't need these lines anymore, but keep them commented here for now
# scriptOfEverything = RICHMIRRORALIGNMENTGANGAROOT+"job/RichMirrorAlignmentGanga.py"
# print scriptOfEverything

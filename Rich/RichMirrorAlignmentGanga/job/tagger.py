###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys

from CondDBUI import CondDB

CondDB_tag = sys.argv[1]
# print CondDB_tag
this = sys.argv[2]
# print this
thisDB = sys.argv[3]
# print thisDB
condDB = CondDB("sqlite_file:" + this + ".db/" + thisDB, readOnly=False)
condDB.recursiveTag("/", CondDB_tag)

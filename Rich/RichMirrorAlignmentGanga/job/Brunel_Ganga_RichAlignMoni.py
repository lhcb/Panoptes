###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# ===============================================================================
# Example ganga job submission file
# ===============================================================================
#
# Set ganga environment first with (e.g. lxplus)
#  > GangaEnv
# Then choose your ganga version (default version is normally the best option)
#
# NB : This file is for the ganga 5 releases
#
# To submit you start ganga with the command "ganga" and then
# at the ganga prompt type
#
# [In] 1 : ganga Brunel_Ganga.py
#
# As an alternative you can also submit the job directly from
# the Unix prompt with the line
#
# [user@lxplus]~% ganga Brunel_Ganga.py
#
# but in this way you will have the overhead of starting Ganga
# for each job you submit.
#
# ===============================================================================
#
# For more information on ganga see http://cern.ch/ganga
#
# In particular, if you think you have found a bug, or you wish to suggest
# some improvement in functionality, please report this at
# https://savannah.cern.ch/projects/ganga/
#
# For Dirac jobs, you can monitor their status at
# http://lhcb.pic.es/DIRAC/Monitoring/Analysis
#
# For any ganga object used below, more detailed information can be found by
# running help(XXX) at the ganga prompt. I.e.
#  > help(Dirac)
# For more information on the Dirac backend
#
# ===============================================================================
import os
from subprocess import *

"""
setupProjectPanoptesVersion = "HEAD"
setupProjectPanoptesOptions = "--nightly lhcb2"
user_release_area           =  os.getenv("User_release_area")

p = Popen("setenv User_release_area "+user_release_area+";"
         +"SetupProject Panoptes    "+setupProjectPanoptesVersion+" "+setupProjectPanoptesOptions+";"
         +"rm                                 -f  env_var_settings.txt;"
         +"echo  $RICHMIRRORALIGNMENTGANGAROOT >> env_var_settings.txt",
           shell=True,
           executable="/bin/tcsh"
         )
sts = os.waitpid(p.pid, 0)

env_var_settings_txt = open(  "env_var_settings.txt")
RICHMIRRORALIGNMENTGANGAROOT = env_var_settings_txt.readline().strip()
env_var_settings_txt.close()
"""
RICHMIRRORALIGNMENTGANGAROOT = "/afs/cern.ch/user/a/asolomin/public/rich_align/Panoptes_HEAD/Rich/RichMirrorAlignmentGanga"
panoptesOptsPath = RICHMIRRORALIGNMENTGANGAROOT + "/options"

# thisCase = '2008_OnDIGI_WithMC_False'
# thisCase = '2008_OnDST_WithMC_True_reco'
# thisCase = '2008_OnDST_WithMC_True_total'
# thisCase = '2008_OnDST_WithMC_True_production'
thisCase = "2010-Collisions"

print(thisCase)

brunel = Brunel()

# brunel.version = "HEAD"
# brunel.setupProjectOptions = '--nightly lhcb-prerelease'
brunel.setupProjectOptions = "--use-grid"

brunel.optsfile = [
    File(panoptesOptsPath + "/Brunel_RichAlignMoni_" + thisCase + "-DST.py"),
    #                   File( panoptesOptsPath+'/'+thisCase+'_test.py') ]
    #                   File( panoptesOptsPath+'/2008-Files_minbias_digi.py') ]
    #                   File( '/afs/cern.ch/user/t/thampson/public/2010-files/MagDownPFN.py') ]
    File("Rich2Events.py"),
]  # //May have to change manually to Rich1Events
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# Make a new job object for Brunel
# -------------------------------------------------------------------------------
j = Job()
j.application = brunel
# -------------------------------------------------------------------------------
# Define name for identifying the job (this name appears in the ganga job list)
# -------------------------------------------------------------------------------
j.name = "BrunelOnDst"
# -------------------------------------------------------------------------------
# The job configuration
# -------------------------------------------------------------------------------
# Define the configuration file(s) to use

# Extra options
# Appended to the end of the main options to override default settings
# j.application.extraopts = ''
# -------------------------------------------------------------------------------

# -------------------------------------------------------------------------------
# Input data
# Can either be specified in the main configuration files (DaVinci.py etc.) or
# locally here as an LHCbDataset
# -------------------------------------------------------------------------------
# j.inputdata = LHCbDataset( files = [
#'LFN:/lhcb/production/DC06/phys-v2-lumi2/00001820/DIGI/0000/00001820_00000001_4.digi' ] )

# -------------------------------------------------------------------------------
# Define a job splitter (very useful for many input data files)
# -------------------------------------------------------------------------------
# Use a single job, no splitting
j.splitter = None

# Split jobs into 1 input file per job. max of 5 files in total
# j.splitter = SplitByFiles ( filesPerJob = 1, maxFiles = 5 )
# This Splitter will query the Logical File Catalog (LFC) to find
# at which sites a particular file is stored. Subjobs will be created
# so that all the data required for each subjob is stored in
# at least one common location. This prevents the submission of jobs that
# are unrunnable due to data availability.
# Useful when using the Dirac backend (see below)
# j.splitter = DiracSplitter ( filesPerJob = 1, maxFiles = 5 )
# -------------------------------------------------------------------------------

j.splitter = SplitByFiles(filesPerJob=3, maxFiles=200)  # Good for '1nh'
j.splitter = SplitByFiles(filesPerJob=1, maxFiles=1)  # Good for '1nh'
# j.splitter = SplitByFiles( filesPerJob =  3, maxFiles =   400 ) # Good for '1nh'
# j.splitter = SplitByFiles( filesPerJob =  3, maxFiles =  1000 ) # Good for '1nh'

# -------------------------------------------------------------------------------
# Job merging (merge output from sub-jobs from job splitter, if used)
# -------------------------------------------------------------------------------
# No merging
# j.merger = None

# Root file merging. See help( RootMerger ) for more details
# j.merger = RootMerger( files = ['histos.root'], ignorefailed = True )

# SmartMerger - Able to handle various file formats (including root files)
#               See help( SmartMerger ) for more details
# j.merger = SmartMerger( files = ['histos.root'], ignorefailed = True )
# -------------------------------------------------------------------------------

j.merger = SmartMerger(
    files=["RichRecQCHistos_" + thisCase + ".root"], ignorefailed=True, overwrite=True
)

# -------------------------------------------------------------------------------
# Add here any special input files. Normally not needed.
# -------------------------------------------------------------------------------
j.inputsandbox = []
# -------------------------------------------------------------------------------

# -------------------------------------------------------------------------------
# Define where to run
# -------------------------------------------------------------------------------
# Run interactively on the local machine
# j.backend    = Interactive()
# Run directly on the local machine, but in the background
j.backend = Local()
# Submit to an LSF batch system, using the 8nm queue
# j.backend    = LSF( queue = '1nh')
# Submit to a condor batch system
# j.backend    = Condor()
# Submit to a PBS batch system
# j.backend    = PBS()
# Submit to the grid.
# j.backend    = Dirac()
# -------------------------------------------------------------------------------

# -------------------------------------------------------------------------------
# Submit the job
# -------------------------------------------------------------------------------
j.submit()
# -------------------------------------------------------------------------------

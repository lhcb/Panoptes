###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# New, combined script for both Online and Offline Rich Mirror Alignment, reorganized by Paras Naik on 6 May 2015
# Original script by Anatoly Solomin
#
# OFFLINE
#
# ssh -Y lxplus.cern.ch
# SetupProject Ganga pytools    # Sets up Ganga with pytools, necessary to provide module lxml to this script
# cd workingdir                 # where workingdir is whatever directory you wish to be working in
# lhcb-proxy-init -v 96:00      # make a 96 hour grid proxy
# kinit -r 7d                   # creates a renewable AFS ticket valid for 7 days
# ganga                         # Start Ganga
# RichInfoName = ('xxxxxxxx')   # where xxxxxxxx is whatever it needs to be to get this code to run (see below)
# execfile('yyyyyyyy')          # where yyyyyyyy is this script
#
# See the TWiki and keep it up-to-date!!
# https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbRichMirrorAlign

print(
    "Python will give a warning about the line 'from Communicator import *', either during compiling or running if not compiled."
)
print(
    "This does not matter for offline. For online it also should work, python just doesn't like it."
)

import builtins
import distutils
import importlib
import math
import os
import re
import shutil
import sys
from distutils import dir_util
from os.path import expandvars
from subprocess import *
from time import gmtime, sleep, strftime, time


# ===============================================================================
# defs
# ===============================================================================
# stdReport returns stdout and stderr from Popen.communicate()
def stdReport(stdout, stderr):
    print("The shell script gave some STANDARD OUTPUT:")
    print(stdout)
    if stderr:
        print("The shell script also gave some STANDARD ERROR:")
        print(stderr)
    else:
        print("The shell script ran fine.")


# ===============================================================================
# exists checks to see if the name is in the locals/globals/builtin OR in obj if obj is not none)
def exists(name, obj=None):
    if obj is None:
        return (
            name in sys._getframe(1).f_locals  # caller's locals
            or name in sys._getframe(1).f_globals  # caller's globals
            or name in vars(__builtin__)  # built-in
        )
    else:
        return hasattr(obj, name)


# ===============================================================================
# staticVar reads in variables from a settings file
def staticVar(varname, value):
    def decorate(func):
        setattr(func, varname, value)
        return func

    return decorate


@staticVar("VarName", {})
def getSettingsFile(InfoStr):
    if InfoStr in getSettingsFile.VarName:
        return getSettingsFile.VarName[InfoStr]
    # print "InfoStr: %s" % InfoStr;
    # SettingsFile = '/path/to/%s_python_file.py' % InfoStr
    # SettingsFile = '/home/pnaik/cmtuser/Panoptes_v5r2/Rich/RichMirrorAlignmentOnline/python/PyMirrAlignOnline/RMA_%s.py' % InfoStr
    SettingsFile = "%s" % InfoStr
    print(("SettingsFile: %s" % SettingsFile))
    directory, filename = os.path.split(SettingsFile)
    # print "directory: %s" % directory
    # print "filename: %s" % filename
    name, extension = os.path.splitext(filename)
    # print "name: %s" % name
    # print "extension: %s" % extension
    sys.path.insert(1, directory)
    VarName = importlib.import_module(name)
    sys.path.remove(directory)
    getSettingsFile.VarName[InfoStr] = VarName
    return VarName


# different one, for patching from the Online Analyzer if need be
@staticVar("PatchName", {})
def getPatchFile(InfoStr):
    if InfoStr in getPatchFile.PatchName:
        return getPatchFile.PatchName[InfoStr]
    # print "InfoStr: %s" % InfoStr;
    PatchFile = "%s" % InfoStr
    print(("PatchFile: %s" % PatchFile))
    directory, filename = os.path.split(PatchFile)
    # print "directory: %s" % directory
    # print "filename: %s" % filename
    name, extension = os.path.splitext(filename)
    # print "name: %s" % name
    # print "extension: %s" % extension
    sys.path.insert(1, directory)
    PatchName = importlib.import_module(name)
    sys.path.remove(directory)
    getPatchFile.PatchName[InfoStr] = PatchName
    return PatchName


# ===============================================================================
# wait_until_exists is currently NOT USED as far as PN knows
# it only shows up in rm_subjobs_except_0_last
def wait_until_exists(j, filename, timeout=10 * 60, sleep_period=3):
    print("RMAG is in wait_until_exists")
    import os
    from time import sleep

    while not (os.path.exists(filename)) and (timeout > 0):
        sleep(sleep_period)
        timeout -= sleep_period
        if timeout < 0:
            return False
    return True


# ===============================================================================
# rm_subjobs_except_0_last is currently NOT USED as far as PN knows
def rm_subjobs_except_0_last(
    j, rootFile, brunelVer, backEnd="dirac", timeout=10 * 60, sleep_period=3
):
    print("RMAG is in rm_subjobs_except_0_last")
    global wait_until_exists
    sjLast = len(j.subjobs.ids()) - 1
    for sj in j.subjobs:
        if not (sj.status in ["failed", "killed"]):
            if (
                backEnd == "dirac"
                and wait_until_exists(
                    sj, sj.outputdir + "std.out", timeout, sleep_period
                )
                and wait_until_exists(
                    sj, sj.outputdir + "stdout", timeout, sleep_period
                )
                and wait_until_exists(
                    sj,
                    sj.outputdir + "Step1_Ganga_Brunel_" + brunelVer + ".log",
                    timeout,
                    sleep_period,
                )
            ) or (
                backEnd != "dirac"
                and wait_until_exists(
                    sj, sj.outputdir + "__jobstatus__", timeout, sleep_period
                )
                and wait_until_exists(
                    sj, sj.outputdir + "__syslog__", timeout, sleep_period
                )
                and wait_until_exists(
                    sj, sj.outputdir + "stderr", timeout, sleep_period
                )
                and wait_until_exists(
                    sj, sj.outputdir + "stdout", timeout, sleep_period
                )
            ):
                sjId = sj.id
                if sjId != 0 and sjId != sjLast:
                    j.peek("../" + str(sjId), "rm -rf")
                else:
                    sj.peek("../output/" + rootFile, "rm  -f")


# ===============================================================================
# create_coeff_calibration_xml_files: using most recent Mirrors files,
# manufacture from them modified Mirrors files
# for evaluation of current magnification coefficients
#
# Paras changed this to take in the richDetector as input,
# as in a python program this variable would otherwise not be in this scope.
# def create_coeff_calibration_xml_files( inputFile=None, coeffCalibrTilt=0.5 ) :
def create_coeff_calibration_xml_files(
    inputFile=None, coeffCalibrTilt=0.5, richDetector=2
):
    import re
    import sys
    from array import array

    import fpformat
    from lxml import etree  # Important! You need SetupProject Ganga pytools
    # or some other method (maybe modify python path) to get this module

    print("RMAG is in create_coeff_calibration_xml_files")

    # first cut inputFile into two parts: inputFileLeftPart and inputFileRightPart
    # and separate them
    inputFilePartsRE = re.compile(r"([a-zA-Z0-9_\.]+)(_i[0-9]+\.xml)")
    inputFileParts = inputFilePartsRE.match(inputFile)
    inputFileLeftPart = inputFileParts.group(1)
    inputFileRightPart = inputFileParts.group(2)
    """
  print "inputFileLeftPart  "+inputFileLeftPart
  print "inputFileRightPart "+inputFileRightPart
  """
    #
    # prepare dictionary of sign names
    signName = dict({"-": "neg", "+": "pos"})

    # prepare dictionary of sign-to-shift
    signToShift = dict({"-": -coeffCalibrTilt, "+": coeffCalibrTilt})

    # prepare dictionary of mirror type names
    mirrTypeName = dict({"pri": "Sph", "sec": "Sec"})

    # prepare dictionary of total mirror numbers
    if richDetector == 2:
        totMirrNum = dict({"pri": 56, "sec": 40})
    if richDetector == 1:
        totMirrNum = dict({"pri": 4, "sec": 16})

    # prepare dictionary of rotation vector's term sequence number depending on axis
    seqNum = dict({"Y": 1, "Z": 2})

    # prepare regular expression
    rotRE = re.compile(r"[\-|\+]?[0-9]+\.?[0-9]*|[\-|\+]?\.[0-9]+")
    # such an expression matches e.g. all of the following:  -000  +20.  2.000  +0005.9000  .0  -.98  +.30000
    """
  for f in [-000,  +20.,  2.000,  +0005.9000,  .0,  -.98,  +.30000] :
      print str( float( str( f ) ) )
  """
    #
    """
  0.0
  20.0
  2.0
  5.9
  0.0
  -0.98
  0.3
  """
    # array for matches in strings
    rotValues = []

    ## parse the tree and create DOM of it
    # docOriginal = parse( inputFile )

    # loop over types of mirrors
    for mirrType in ["pri", "sec"]:
        # loop over axes
        for axis in ["Y", "Z"]:
            # loop over signs
            for sign in ["-", "+"]:
                ## make a working copy in order not to parse each anew
                # doc = docOriginal
                parser = etree.XMLParser(remove_blank_text=True)
                RichAlignmentConditions = etree.parse(inputFile, parser)
                """
        print( etree.tostring( RichAlignmentConditions, encoding='iso-8859-1', xml_declaration=True, pretty_print=True, with_tail=False ) )
        """
                # loop over all mirrors of current type (primary/secondary)
                # total numbers of primary (spheric) and secondary (flat) mirrors are different
                for mirrNum in range(totMirrNum[mirrType]):
                    # array for matches in float
                    floatRotValues = array("f")

                    # Different xml alignment names for the two Rich detectors
                    if richDetector == 1:
                        # store the whole element in dRotXYZ
                        dRotXYZ = RichAlignmentConditions.xpath(
                            "//condition[@name='"
                            + mirrTypeName[mirrType]
                            + "Mirror"
                            + str(mirrNum)
                            + "_Align']/paramVector[@name='dRotXYZ']"
                        )

                    if richDetector == 2:
                        # store the whole element in dRotXYZ
                        dRotXYZ = RichAlignmentConditions.xpath(
                            "//condition[@name='Rich"
                            + str(richDetector)
                            + mirrTypeName[mirrType]
                            + "Mirror"
                            + str(mirrNum)
                            + "_Align']/paramVector[@name='dRotXYZ']"
                        )
                    """
          print(etree.tostring(dRotXYZ[0], pretty_print=True))
          print(etree.tostring(dRotXYZ[0], pretty_print=True, method="text"))
          """
                    #
                    # store all three rotation correction values in the floatRotValues (float) array
                    rotValues = rotRE.findall(
                        etree.tostring(dRotXYZ[0], pretty_print=True, method="text")
                    )
                    """
          for i in range (3) :
             print rotValues[i]
          #
          """
                    for i in range(3):
                        floatRotValues.append(float(rotValues[i]))
                    """
          for i in range (3) :
            print str( floatRotValues[i] )
          """
                    #
                    # re-write the body of the vector of rotation corrections
                    # with the value of one of them shifted by +/- coeffCalibrTilt mrad
                    floatRotValues[seqNum[axis]] += signToShift[sign]

                    dRotXYZ[0].text = (
                        fpformat.fix(floatRotValues[0], 5)
                        + "*mrad "
                        + fpformat.fix(floatRotValues[1], 5)
                        + "*mrad "
                        + fpformat.fix(floatRotValues[2], 5)
                        + "*mrad"
                    )
                    """
          print(etree.tostring(dRotXYZ[0], pretty_print=True, encoding='iso-8859-1'))
          """
                    #
                if axis == "Y":
                    signCombinName = signName[sign] + "YzerZ"
                else:
                    signCombinName = "zerY" + signName[sign] + "Z"

                newXML_File = open(
                    inputFileLeftPart
                    + "_"
                    + mirrType
                    + "_"
                    + signCombinName
                    + inputFileRightPart,
                    "w",
                )
                newXML_File.write(
                    etree.tostring(
                        RichAlignmentConditions,
                        encoding="iso-8859-1",
                        xml_declaration=True,
                        pretty_print=True,
                        with_tail=False,
                    )
                )
                newXML_File.close()


# ===============================================================================
# just_prettify
def just_prettify(inputFile=None):
    print("RMAG is in just_prettify")
    from lxml import etree

    parser = etree.XMLParser(remove_blank_text=True)
    RichAlignmentConditions = etree.parse(inputFile, parser)
    newXML_File = open(inputFile, "w")
    newXML_File.write(
        etree.tostring(
            RichAlignmentConditions,
            encoding="iso-8859-1",
            xml_declaration=True,
            pretty_print=True,
            with_tail=False,
        )
    )
    newXML_File.close()


# ===============================================================================
# mirroralign retrieves necessary environment variables, and does the work of alignment:
def mirroralign(RichInfoSettings="Something_Went_Wrong"):
    # The next line is a legacy line from when this code was also being used for Online alignment
    # def mirroralign(RichInfoSettings = "/home/pnaik/cmtuser/Panoptes_v5r5/Rich/RichMirrorAlignmentOnline/python/PyMirrAlignOnline/RichMirrorAlignmentSettings.py") :
    # def mirroralign() :
    print(("RMAG is in mirroralign(" + RichInfoSettings + ")"))
    print((strftime("%Y-%m-%d %H:%M:%S", gmtime())))
    print("-------------------")
    # Paras - NEW: get settings from file, put them into RichInfo, instead of expecting them to already be in locals()...
    print(("RichInfoSettings: %s" % RichInfoSettings))
    RichInfo = getSettingsFile(RichInfoSettings)

    # OLD VERSION
    # Paras - eventually we want to pass this into the script somehow, to allow us to select the appropriate settings file
    # Paras - for now, if RichInfoName is already defined in globals() before we run this script then we will use that otherwise use the default
    # Paras - for now, RichInfoNameLocal should be the file name with full path information
    # if (exists('RichInfoName')) :
    #     print "RichInfoName: %s" % RichInfoName
    #     RichInfoNameLocal = RichInfoName
    # else :
    #     #RichInfoNameLocal = "./RichMirrorAlignmentGanga_Collision10_settings_R2MD.py"
    #     RichInfoNameLocal = "Something_Went_Wrong"
    #     print "RichInfoNameLocal: %s" % RichInfoNameLocal
    # RichInfo = getSettingsFile(RichInfoNameLocal)

    RichInfoMembers = [
        attr
        for attr in dir(RichInfo)
        if not callable(getattr(RichInfo, attr)) and not attr.startswith("__")
    ]
    print("RichInfoMembers:")
    print(RichInfoMembers)

    # We no longer do HPD alignment, so the following is not important
    HPDiteration = 0

    # check whether the parameters were defined, and assign defaults if - not,
    # and also set an exit flag, if workDir, eventSelectorInputFile,
    # or a few other important variables are not defined:
    exitFlag = False

    # move all of the RichInfo stuff into local variables within mirroralign()
    for key in RichInfoMembers:
        exec(key + " = RichInfo." + key)

    # Paras - check that what we need is among the local, global, and system variables now (of course, they should only be in locals())
    if exists("workDir") == 0 or workDir == "":
        workDir = "sorry, you must define your working directory !"
        exitFlag = True

    if exists("dDDB_tag") == 0:
        dDDB_tag = ""

    if exists("condDB_tag") == 0:
        condDB_tag = ""

    if exists("date") == 0:
        date = ""

    # note that you can set this one if you want to use
    # LocalCondDB_tag=localCondDB_tag instead of LocalCondDB_tag=CondDB_tag
    # And of course, I don't see LocalCondDB_tag actually being used anywhere in this script... candidate for deletion?
    if exists("localCondDB_tag") == 0:
        localCondDB_tag = ""

    if exists("setupProjectBrunelOpts") == 0:
        setupProjectBrunelOpts = ""

    if exists("eventSelectorInputFile") == 0 or eventSelectorInputFile == "":
        if exists("ioHelperInputFile") == 1 and not ioHelperInputFile == "":
            eventSelectorInputFile = ioHelperInputFile
        else:
            eventSelectorInputFile = "sorry, you must define your input !"
            exitFlag = True

    if exists("brunelVersion") == 0 or brunelVersion == "":
        brunelVersion = ""

    if exists("LHCbVersion") == 0 or LHCbVersion == "":
        LHCbVersion = ""

    if exists("brunelEvtMax") == 0 or brunelEvtMax == "":
        brunelEvtMax = "100"

    if exists("brunelPrintFreq") == 0 or brunelPrintFreq == "":
        brunelPrintFreq = "10"

    if exists("brunelInitialInputType") == 0 or brunelInitialInputType == "":
        brunelInitialInputType = "RAW"

    if exists("messageSvcOutputLevel") == 0 or messageSvcOutputLevel == "":
        messageSvcOutputLevel = "7"

    if exists("splitFilesPerJob") == 0 or splitFilesPerJob == "":
        splitFilesPerJob = 1

    if exists("splitMaxFiles") == 0 or splitMaxFiles == "":
        splitMaxFiles = 1

    if exists("backEnd") == 0 or backEnd == "":
        backEnd = "LSF_8nm"

    if exists("diracCPUTime") == 0 or diracCPUTime == "":
        diracCPUTime = 12000

    if exists("mirrCombinSubset") == 0 or mirrCombinSubset == "":
        mirrCombinSubset = "17un18eq"

    if exists("thisCase") == 0:
        thisCase = ""

    if exists("dataVariant") == 0 or dataVariant == "":
        dataVariant = "sorry, you must define your data variant !"
        exitFlag = True

    if exists("minAverageBinPop") == 0 or minAverageBinPop == "":
        minAverageBinPop = 6.0

    if exists("deltaThetaWindow") == 0 or deltaThetaWindow == "":
        deltaThetaWindow = 4.0

    if exists("coeffCalibrTilt") == 0 or coeffCalibrTilt == "":
        coeffCalibrTilt = 0.3

    if exists("useTruth") == 0:
        useTruth = ""

    if exists("useOffsetsFromMC") == 0 or useOffsetsFromMC == "":
        useOffsetsFromMC = "false"

    if exists("verOffsetsFromMC") == 0:
        verOffsetsFromMC = ""

    if exists("usePremisaligned") == 0 or usePremisaligned == "":
        usePremisaligned = "false"

    # leave empty unless needed for usePremisaligned
    if exists("verPremisaligned") == 0:
        verPremisaligned = ""

    if exists("useSpecificAlignment") == 0 or useSpecificAlignment == "":
        useSpecificAlignment = "false"

    # leave empty unless needed for useSpecificAlignment
    if exists("specificAlignment") == 0:
        specificAlignment = ""

    if exists("combinFitVariant") == 0 or combinFitVariant == "":
        combinFitVariant = 3

    if exists("maximumNumberOfIterations") == 0 or maximumNumberOfIterations == "":
        maximumNumberOfIterations = 8

    if exists("startFromIteration") == 0 or startFromIteration == "":
        startFromIteration = 0

    if exists("richDetector") == 0 or richDetector == "":
        richDetector = 2

    if exists("magnifCoeffMode") == 0 or magnifCoeffMode == "":
        magnifCoeffMode = 2

    if exists("solutionMethod") == 0 or solutionMethod == "":
        solutionMethod = 1

    if exists("alignHPDs") == 0 or alignHPDs == "":
        alignHPDs = "false"

    if exists("AddedDB") == 0:
        AddedDB = [""]

    # Paras - These were somehow forgotten before, or new, so I add them here for completeness
    if exists("home") == 0 or home == "":
        home = "sorry, you must define your home !"
        exitFlag = True

    if exists("userReleaseArea") == 0 or userReleaseArea == "":
        userReleaseArea = "sorry, you must define your userReleaseArea !"
        exitFlag = True

    if exists("userReleaseArea") == 0 or userReleaseArea == "":
        userReleaseArea = "sorry, you must define your userReleaseArea !"
        exitFlag = True

    if exists("pathToScripts") == 0 or pathToScripts == "":
        pathToScripts = "sorry, you must define your pathToScripts !"
        exitFlag = True

    if exists("RICHMIRRCOMBINFITROOT") == 0 or RICHMIRRCOMBINFITROOT == "":
        RICHMIRRCOMBINFITROOT = "sorry, you must define your RICHMIRRCOMBINFITROOT !"
        exitFlag = True

    if (
        exists("RICHMIRRORALIGNMENTGANGAROOT") == 0
        or RICHMIRRORALIGNMENTGANGAROOT == ""
    ):
        RICHMIRRORALIGNMENTGANGAROOT = (
            "sorry, you must define your RICHMIRRORALIGNMENTGANGAROOT !"
        )
        exitFlag = True

    if exists("SQLITEDBPATH") == 0 or SQLITEDBPATH == "":
        SQLITEDBPATH = "sorry, you must define your SQLITEDBPATH !"
        exitFlag = True

    if exists("previousJobNumbers") == 0 or previousJobNumbers == "":
        previousJobNumbers = list(range(899, 901))

    if exists("skipFirstCombinFit") == 0 or skipFirstCombinFit == "":
        skipFirstCombinFit = 0

    if exists("skipFirstRecoJob") == 0 or skipFirstRecoJob == "":
        skipFirstRecoJob = 0

    if exists("skipFirstMergeJob") == 0 or skipFirstMergeJob == "":
        skipFirstMergeJob = 0

    if exists("justGlobalFit") == 0 or justGlobalFit == "":
        justGlobalFit = 0

    if exists("stopTolerance") == 0 or stopTolerance == "":
        stopTolerance = 0.1  # in mrad

    if exists("warningFactor") == 0 or warningFactor == "":
        warningFactor = 20

    if exists("phiBinFactor") == 0 or phiBinFactor == "":
        phiBinFactor = 1

    # PN - These are apparently unused in this script
    if exists("APPCONFIGOPTS") == 0:
        APPCONFIGOPTS = ""
    if exists("thisAppConfigBrunelOpts") == 0 or thisAppConfigBrunelOpts == "":
        thisAppConfigBrunelOpts = "Default"

    brunelEvtMax = str(brunelEvtMax)
    brunelPrintFreq = str(brunelPrintFreq)
    messageSvcOutputLevel = str(messageSvcOutputLevel)

    print(
        "### ----------------------------------------------------------------------------"
    )
    print(
        "### OK, here are the parameters that will be used (default, unless you defined"
    )
    print("### them for me in the settings file):")
    # Directory paths, like workDir, should end with os.path.sep
    # Please ensure this is the case in the _settings_ file!
    print("###")
    print("### workDir                      = " + workDir)

    print("### dDDB_tag                     = " + dDDB_tag)
    print("### condDB_tag                   = " + condDB_tag)
    print("### date                         = " + date)
    print("### localCondDB_tag              = " + localCondDB_tag)

    print("### setupProjectBrunelOpts       = " + setupProjectBrunelOpts)

    print("### eventSelectorInputFile       = " + eventSelectorInputFile)
    print("### LHCbVersion                  = " + LHCbVersion)
    print("### brunelVersion                = " + brunelVersion)
    print("### brunelEvtMax                 = " + brunelEvtMax)
    print("### brunelPrintFreq              = " + brunelPrintFreq)
    print("### brunelInitialInputType       = " + brunelInitialInputType)
    print("### messageSvcOutputLevel        = " + messageSvcOutputLevel)

    print("### splitFilesPerJob             = " + str(splitFilesPerJob))
    print("### splitMaxFiles                = " + str(splitMaxFiles))
    print("### backEnd                      = " + backEnd)
    backEnd = str(backEnd.lower())

    if backEnd == "dirac":
        print("### diracCPUTime                 = " + str(diracCPUTime))

    print("### mirrCombinSubset             = " + mirrCombinSubset)
    print("### thisCase                     = " + thisCase)
    print("### dataVariant                  = " + dataVariant)

    print("### minAverageBinPop             = " + str(minAverageBinPop))
    print("### deltaThetaWindow             = " + str(deltaThetaWindow))

    print("### coeffCalibrTilt              = " + str(coeffCalibrTilt))

    print("### useTruth                     = " + useTruth)

    print("### useOffsetsFromMC             = " + useOffsetsFromMC)
    print("### alignHPDs                    = " + alignHPDs)

    print("### AddedDB                      = ")
    print(AddedDB)
    #    print "### AddedDB                   = "+str(len(AddedDB))

    useOffsets = useOffsetsFromMC.lower() == "true" or useOffsetsFromMC.lower() == "yes"
    if useOffsets:
        useOffsetsFromMC = "true"
        print("### verOffsetsFromMC             = " + verOffsetsFromMC)
    else:
        useOffsetsFromMC = "false"

    print("### usePremisaligned             = " + usePremisaligned)
    premisaligned = (
        usePremisaligned.lower() == "true" or usePremisaligned.lower() == "yes"
    )
    if premisaligned:
        usePremisaligned = "true"
        print("### verPremisaligned             = " + verPremisaligned)
    else:
        usePremisaligned = "false"

    print("### useSpecificAlignment            = " + useSpecificAlignment)
    specific = (
        useSpecificAlignment.lower() == "true" or usePremisaligned.lower() == "yes"
    )
    if specific:
        useSpecificAlignment = "true"
        print("### specificAlignment             = " + specificAlignment)
    else:
        useSpecificAlignment = "false"

    print("### combinFitVariant             = " + str(combinFitVariant))

    print("### maximumNumberOfIterations    = " + str(maximumNumberOfIterations))

    print("### startFromIteration           = " + str(startFromIteration))

    print("### richDetector                 = " + str(richDetector))

    print("### solutionMethod               = " + str(solutionMethod))

    # And the additional stuff
    print("### home                         = " + str(home))
    print("### userReleaseArea              = " + str(userReleaseArea))
    print("### pathToScripts                = " + str(pathToScripts))
    print("### RICHMIRRALIGNROOT            = " + str(RICHMIRRALIGNROOT))
    print("### RICHMIRRCOMBINFITROOT        = " + str(RICHMIRRCOMBINFITROOT))
    print("### RICHMIRRORALIGNMENTGANGAROOT = " + str(RICHMIRRORALIGNMENTGANGAROOT))
    print("### SQLITEDBPATH                 = " + str(SQLITEDBPATH))
    print("### previousJobNumbers           = ")
    print(previousJobNumbers)

    print("### skipFirstCombinFit           = " + str(skipFirstCombinFit))
    print("### skipFirstRecoJob             = " + str(skipFirstRecoJob))
    print("### skipFirstMergeJob            = " + str(skipFirstMergeJob))

    print("### justGlobalFit                = " + str(justGlobalFit))

    print("### APPCONFIGOPTS                = " + APPCONFIGOPTS)
    print("### thisAppConfigBrunelOpts      = " + thisAppConfigBrunelOpts)

    print("### stopTolerance                = " + str(stopTolerance))
    print("### warningFactor                = " + str(warningFactor))
    print("### phiBinFactor                 = " + str(phiBinFactor))

    print(
        "### ----------------------------------------------------------------------------"
    )

    if exitFlag:
        print(
            "### ----------------------------------------------------------------------------"
        )
        print(
            "### sorry, not enough information for running: check the settings file content !"
        )
        print(
            "### ----------------------------------------------------------------------------"
        )
        sys.exit(1)

    print("You are currently in this directory:")
    wasHere = os.getcwd()
    print(wasHere)
    print("You are now in the working directory:")
    os.chdir(workDir)
    print(os.getcwd())

    # -------------------------------------------------------------------------------
    # PN - No longer necessary. You should have defined this in your _settings_ file
    #
    # pathToScripts      = "/afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts"
    # if not pathToScripts.endswith(os.path.sep):
    #     pathToScripts += os.path.sep

    # -------------------------------------------------------------------------------
    # just not to forget later...
    RichDetectorStr = str(richDetector)
    iterationCount = startFromIteration
    collisionVariantList = [
        "Collision09",
        "Collision10",
        "Collision11",
        "Collision12",
        "Collision15",
        "Collision15_30",
    ]  # Real Data
    mcVariantList = ["NotUsed"]  # PN - We don't use MC, yet
    theFileNowForOnline = workDir + "Rich" + RichDetectorStr + ".xml"

    # -------------------------------------------------------------------------------
    if dataVariant in collisionVariantList:
        CONDDB = "LHCBCOND"
        # setupProjectBrunelOpts += " --use-grid"
    else:
        CONDDB = "SIMCOND"

    # -------------------------------------------------------------------------------
    DDDB_tag = dDDB_tag
    CondDB_tag = condDB_tag

    # -------------------------------------------------------------------------------
    if localCondDB_tag != "":
        LocalCondDB_tag = localCondDB_tag
    else:
        LocalCondDB_tag = CondDB_tag

    # -------------------------------------------------------------------------------
    # this is the case when the magnification coefficients are independent of the
    # sign of the rotation
    if magnifCoeffMode == 0 or magnifCoeffMode == 1:
        tiltInJobnameDict = dict({"": ""})
        tiltNames = [""]

    # and this is the case when the magnification coefficients are dependent on the
    # sign of the rotation
    if magnifCoeffMode == 2:
        # in case of 2 we must run 9 times over the same data files during each
        # major iteration: one normal and 8 with 8 calibration-modified XML files to
        # evaluate the magnification coefficients, all 9 jobs in parallel, therefore we
        # prepare a convenient dictionary for the job names
        tiltInJobnameDict = dict(
            {
                "": "",
                "pri_negYzerZ": "p_nz_",
                "pri_posYzerZ": "p_pz_",
                "pri_zerYnegZ": "p_zn_",
                "pri_zerYposZ": "p_zp_",
                "sec_negYzerZ": "s_nz_",
                "sec_posYzerZ": "s_pz_",
                "sec_zerYnegZ": "s_zn_",
                "sec_zerYposZ": "s_zp_",
            }
        )
        # ------------------------------------------------------------------------------------------------------------------------------------------------
        tiltNames = [
            "",
            "pri_negYzerZ",
            "pri_posYzerZ",
            "pri_zerYnegZ",
            "pri_zerYposZ",
            "sec_negYzerZ",
            "sec_posYzerZ",
            "sec_zerYnegZ",
            "sec_zerYposZ",
        ]

    # -------------------------------------------------------------------------------
    # also, here is the right place to prepare directories for the main
    # and for the coefficient variants branches
    # for that, we already need proper long names (but without major iteration number)
    thisCaseStr = ""

    thisTuning = "Mp" + str(minAverageBinPop)
    thisTuning += "Wi" + str(deltaThetaWindow)
    thisTuning += "Fv" + str(combinFitVariant)
    thisTuning += "Cm" + str(magnifCoeffMode)
    thisTuning += "Sm" + str(solutionMethod)

    if "" != useTruth:
        thisTuning += "Tr" + useTruth.lower()

    if useOffsets:
        thisTuning += "Of"

    # thisCaseStr    += "_"+thisTuning
    thisCaseStr += thisTuning

    # -------------------------------------------------------------------------------
    if useOffsets and verOffsetsFromMC == "":
        verOffsetsFromMC = thisCase + "_" + dataVariant

    # -------------------------------------------------------------------------------
    if premisaligned:
        thisCaseExtended = thisCase + "_" + verPremisaligned
    else:
        thisCaseExtended = thisCase

    # -------------------------------------------------------------------------------
    # we need thisNameStr for preparation of the misaligned xml files, etc.
    # in case where we do not need any loops over the tilts
    thisNameStr = thisCaseStr + "_" + thisCaseExtended + "_" + dataVariant

    theseNamesWithTilts = {}

    tiltNamesLength = len(tiltNames)

    for tiltName in tiltNames:
        if tiltName == "":
            theseNamesWithTilts[tiltName] = thisNameStr
        else:
            theseNamesWithTilts[tiltName] = (
                thisCaseStr
                + "_"
                + thisCaseExtended
                + "_"
                + dataVariant
                + "_"
                + tiltName
            )

        # clean-up the directories for the branches
        # Paras believes this only needs to be done once, if we are running completely from scratch
        if (
            iterationCount == 0
            and not skipFirstRecoJob == 1
            and not skipFirstMergeJob == 1
            and not skipFirstCombinFit == 1
        ):
            if os.path.exists(
                workDir
                + "Mirrors_rich"
                + RichDetectorStr
                + "_"
                + theseNamesWithTilts[tiltName]
            ):
                dir_util.remove_tree(
                    workDir
                    + "Mirrors_rich"
                    + RichDetectorStr
                    + "_"
                    + theseNamesWithTilts[tiltName]
                )

    # Sam found that we have to rename stuff to get this to work
    # ONLY doing this for magnifCoeffMode == 0 for now.
    if magnifCoeffMode == 0:
        alternateTiltNames = [
            "pri_negYzerZ",
            "pri_posYzerZ",
            "pri_zerYnegZ",
            "pri_zerYposZ",
            "sec_negYzerZ",
            "sec_posYzerZ",
            "sec_zerYnegZ",
            "sec_zerYposZ",
        ]
        print(alternateTiltNames)
        theseNamesWithOtherTilts = {}

        for alternateTiltName in alternateTiltNames:
            theseNamesWithOtherTilts[alternateTiltName] = (
                thisCaseStr
                + "_"
                + thisCaseExtended
                + "_"
                + dataVariant
                + "_"
                + alternateTiltName
            )

        print(theseNamesWithOtherTilts)

        # ultra-rubbish way to deal with this
        if richDetector == 1:
            weHaveFileList = [
                workDir + "Rich1FixedMirrCoeff_pri_negYzerZ_1.0mr.txt",
                workDir + "Rich1FixedMirrCoeff_pri_posYzerZ_1.0mr.txt",
                workDir + "Rich1FixedMirrCoeff_pri_zerYnegZ_1.0mr.txt",
                workDir + "Rich1FixedMirrCoeff_pri_zerYposZ_1.0mr.txt",
                workDir + "Rich1FixedMirrCoeff_sec_negYzerZ_1.0mr.txt",
                workDir + "Rich1FixedMirrCoeff_sec_posYzerZ_1.0mr.txt",
                workDir + "Rich1FixedMirrCoeff_sec_zerYnegZ_1.0mr.txt",
                workDir + "Rich1FixedMirrCoeff_sec_zerYposZ_1.0mr.txt",
            ]
        else:
            weHaveFileList = [
                workDir + "Rich2FixedMirrCoeff_pri_negYzerZ_1.0mr.txt",
                workDir + "Rich2FixedMirrCoeff_pri_posYzerZ_1.0mr.txt",
                workDir + "Rich2FixedMirrCoeff_pri_zerYnegZ_1.0mr.txt",
                workDir + "Rich2FixedMirrCoeff_pri_zerYposZ_1.0mr.txt",
                workDir + "Rich2FixedMirrCoeff_sec_negYzerZ_1.0mr.txt",
                workDir + "Rich2FixedMirrCoeff_sec_posYzerZ_1.0mr.txt",
                workDir + "Rich2FixedMirrCoeff_sec_zerYnegZ_1.0mr.txt",
                workDir + "Rich2FixedMirrCoeff_sec_zerYposZ_1.0mr.txt",
            ]

        weHaveFileListIterator = 0

        for alternateTiltName in alternateTiltNames:
            # length of weHaveFileList better be the same as alternateTiltNames
            if os.path.exists(weHaveFileList[weHaveFileListIterator]):
                mirrAlignExpectsFile = (
                    workDir
                    + "Rich"
                    + RichDetectorStr
                    + "MirrMagnFactors_"
                    + theseNamesWithOtherTilts[alternateTiltName]
                    + "_predefined.txt"
                )
                print(
                    "moving "
                    + weHaveFileList[weHaveFileListIterator]
                    + " to "
                    + mirrAlignExpectsFile
                )
                os.system(
                    "cp "
                    + weHaveFileList[weHaveFileListIterator]
                    + " "
                    + mirrAlignExpectsFile
                )
                # os.remove( weHaveFileList[weHaveFileListIterator] )
            weHaveFileListIterator += 1
        os.system("rm -rf *_1.0mr.txt")

    # -------------------------------------------------------------------------------
    """
    if the iterationCount == 0
    Before we start any major iterations, let us prepare local XML copy of the DB
    """
    # -------------------------------------------------------------------------------
    # Paras believes this only needs to be done once, if we are running completely from scratch
    if (
        iterationCount == 0
        and not skipFirstRecoJob == 1
        and not skipFirstMergeJob == 1
        and not skipFirstCombinFit == 1
    ):
        #    if ( iterationCount == 0 or skipFirstRecoJob == 1 ) :
        if premisaligned:
            print(
                "### creating appropriate local /Alignment branch, like in the official CondDB,"
            )
            print("### and preparing a working copy of the premisaligned Mirrors.xml :")
            print(
                "### ----------------------------------------------------------------------------"
            )
            dir_util.mkpath(
                workDir
                + "Mirrors_rich"
                + RichDetectorStr
                + "_"
                + thisNameStr
                + "/Conditions/Rich"
                + RichDetectorStr
                + "/Alignment"
            )
            # -------------------------------------------------------------------------------
            # create a working [replaceable] copy called "Mirrors_rich2_"+thisNameStr+"_i0.xml"
            # we'd better assume that there is a consistent naming convention here... I have no idea since we don't deal with premisaligned yet... but we are going forward with this:
            os.system(
                "cp  -p  Rich"
                + RichDetectorStr
                + "CondDBUpdate_random_1.xml  Rich"
                + RichDetectorStr
                + "CondDBUpdate_"
                + thisNameStr
                + "_i0.xml"
            )

        else:
            if backEnd != "online":
                print(
                    "### preparing XML copy of the whole Mirrors.xml branch from the official CondDB:"
                )
                print("### and preparing a working copy of Mirrors.xml :")
                print(
                    "### ----------------------------------------------------------------------------"
                )

                if setupProjectBrunelOpts.find("--use-grid") != -1:
                    print('( setupProjectBrunelOpts.find("--use-grid") != -1 ) is True')
                    print(
                        "dump_db_to_files.py  -v  -c CondDB/"
                        + CONDDB
                        + '  -t `date +"%s000000000"`  -T \''
                        + CondDB_tag
                        + "'  -s /Conditions/Rich"
                        + RichDetectorStr
                        + "/Alignment/Mirrors.xml  -d "
                        + workDir
                        + "Mirrors_rich"
                        + RichDetectorStr
                        + "_"
                        + thisNameStr
                    )

                    p = Popen(
                        "export   HOME="
                        + home
                        + ";"
                        + "source "
                        + pathToScripts
                        + "LbLogin.sh;"
                        + "source  `which SetupProject.sh` LHCb "
                        + LHCbVersion
                        + "  --use-grid;"
                        + "dump_db_to_files.py  -v  -c CondDB/"
                        + CONDDB
                        + '  -t `date +"%s000000000"`  -T \''
                        + CondDB_tag
                        + "'  -s /Conditions/Rich"
                        + RichDetectorStr
                        + "/Alignment/Mirrors.xml  -d "
                        + workDir
                        + "Mirrors_rich"
                        + RichDetectorStr
                        + "_"
                        + thisNameStr,
                        shell=True,
                        executable="/bin/bash",
                        stdout=PIPE,
                        stderr=PIPE,
                    )
                    (stdout, stderr) = p.communicate()
                    stdReport(stdout, stderr)

                else:
                    CONDDB1 = CONDDB

                    print(
                        '( setupProjectBrunelOpts.find("--use-grid") != -1 ) is False'
                    )
                    #             print     "dump_db_to_files.py  -v  -c sqlite_file:$SQLITEDBPATH/"+CONDDB1+".db/"+CONDDB+"  -T \'"+     CondDB_tag+"\' -t \'"+date+"\'  -s /Conditions/Rich"+RichDetectorStr+"/Alignment/Mirrors.xml  -d Mirrors_"+thisNameStr
                    print(
                        "dump_db_to_files.py  -v  -c sqlite_file:"
                        + SQLITEDBPATH
                        + CONDDB1
                        + ".db/"
                        + CONDDB
                        + "  -T '"
                        + CondDB_tag
                        + "' -t '"
                        + date
                        + "'  -s /Conditions/Rich"
                        + RichDetectorStr
                        + "/Alignment/Mirrors.xml  -d "
                        + workDir
                        + "Mirrors_rich"
                        + RichDetectorStr
                        + "_"
                        + thisNameStr
                    )

                    p = Popen(
                        "export   HOME="
                        + home
                        + ";"
                        + "source "
                        + pathToScripts
                        + "LbLogin.sh;"
                        + "source  `which SetupProject.sh` LHCb "
                        + LHCbVersion
                        + " ;"
                        #                      +"dump_db_to_files.py  -v  -c sqlite_file:$SQLITEDBPATH/"+CONDDB1+".db/"+CONDDB+"  -T \'"+     CondDB_tag+"\' -t \'"+date+"\' -s /Conditions/Rich"+RichDetectorStr+"/Alignment/Mirrors.xml  -d Mirrors_"+thisNameStr,
                        + "dump_db_to_files.py  -v  -c sqlite_file:"
                        + SQLITEDBPATH
                        + CONDDB1
                        + ".db/"
                        + CONDDB
                        + "  -T '"
                        + CondDB_tag
                        + "' -t '"
                        + date
                        + "' -s /Conditions/Rich"
                        + RichDetectorStr
                        + "/Alignment/Mirrors.xml  -d "
                        + workDir
                        + "Mirrors_rich"
                        + RichDetectorStr
                        + "_"
                        + thisNameStr,
                        shell=True,
                        executable="/bin/bash",
                        stdout=PIPE,
                        stderr=PIPE,
                    )
                    (stdout, stderr) = p.communicate()
                    stdReport(stdout, stderr)

                # -------------------------------------------------------------------------------
                # create a working [replaceable] copy called "Rich"+RichDetectorStr+"CondDBUpdate_"+thisNameStr+"_i0.xml"
                os.system(
                    "cp  -p "
                    + workDir
                    + "Mirrors_rich"
                    + RichDetectorStr
                    + "_"
                    + thisNameStr
                    + "/Conditions/Rich"
                    + RichDetectorStr
                    + "/Alignment/Mirrors.xml  Rich"
                    + RichDetectorStr
                    + "CondDBUpdate_"
                    + thisNameStr
                    + "_i0.xml"
                )
                os.remove(
                    workDir
                    + "Mirrors_rich"
                    + RichDetectorStr
                    + "_"
                    + thisNameStr
                    + "/Conditions/Rich"
                    + RichDetectorStr
                    + "/Alignment/Mirrors.xml"
                )
        # -------------------------------------------------------------------------------
        # if we want to use an existing mirror alignment XML, copy specificAlignment as the zeroth major iteration XML
        # make sure this is *always* the case ONLINE
        if useSpecificAlignment == "true":
            print(
                "copying specificAlignment to replace the current zeroth iteration XML file"
            )
            print(
                "cp  -p "
                + specificAlignment
                + " Rich"
                + RichDetectorStr
                + "CondDBUpdate_"
                + thisNameStr
                + "_i0.xml"
            )
            os.system(
                "cp  -p "
                + specificAlignment
                + " Rich"
                + RichDetectorStr
                + "CondDBUpdate_"
                + thisNameStr
                + "_i0.xml"
            )

        # -------------------------------------------------------------------------------
        # making Rich"+RichDetectorStr+"CondDBUpdate_"+thisNameStr+"_i0.xml pretty formatted
        just_prettify(
            "Rich" + RichDetectorStr + "CondDBUpdate_" + thisNameStr + "_i0.xml"
        )
    # -------------------------------------------------------------------------------

    # define files
    currentMirrorXMLFile = (
        "Rich"
        + RichDetectorStr
        + "CondDBUpdate_"
        + thisNameStr
        + "_i"
        + str(iterationCount)
        + ".xml"
    )
    nextIterationXMLFile = (
        "Rich"
        + RichDetectorStr
        + "CondDBUpdate_"
        + thisNameStr
        + "_i"
        + str(iterationCount + 1)
        + ".xml"
    )
    zeroMirrorXMLFile = (
        "Rich" + RichDetectorStr + "CondDBUpdate_" + thisNameStr + "_i0.xml"
    )
    # -------------------------------------------------------------------------------
    # -------------------------------------------------------------------------------
    # we now create branches of directories for 8 tilts
    for tiltName in tiltNames:
        # if ( tiltName != "" ) :
        if not (
            os.path.exists(
                workDir
                + "Mirrors_rich"
                + RichDetectorStr
                + "_"
                + theseNamesWithTilts[tiltName]
                + "/Conditions/Rich"
                + RichDetectorStr
                + "/Alignment"
            )
        ):
            os.system(
                "mkdir --parents "
                + workDir
                + "Mirrors_rich"
                + RichDetectorStr
                + "_"
                + theseNamesWithTilts[tiltName]
                + "/Conditions/Rich"
                + RichDetectorStr
                + "/Alignment"
            )

    # -------------------------------------------------------------------------------
    # define directory where additional files to use are
    PanoptesJobPath = RICHMIRRORALIGNMENTGANGAROOT + "job"
    # -------------------------------------------------------------------------------
    # define directory where Brunel configuration files to use are
    BrunelOptsPath = RICHMIRRORALIGNMENTGANGAROOT + "options"

    # Paras - OK, now we are getting into Ganga specific stuff. Try to separate this
    #             so if ( backEnd == 'online' ) we don't use Ganga
    # -------------------------------------------------------------------------------
    if backEnd == "local":
        jBackend = Local()
        jSplitter = SplitByFiles(filesPerJob=splitFilesPerJob, maxFiles=splitMaxFiles)
        jSplitter.ignoremissing = True
    elif backEnd == "interactive":
        jBackend = Interactive()
        jSplitter = SplitByFiles(filesPerJob=splitFilesPerJob, maxFiles=splitMaxFiles)
        jSplitter.ignoremissing = True
    elif backEnd == "dirac":
        jBackend = Dirac(settings={"CPUTime": diracCPUTime})
        jSplitter = SplitByFiles(filesPerJob=splitFilesPerJob, maxFiles=splitMaxFiles)
        jSplitter.ignoremissing = True
    elif backEnd == "lsf":
        jBackend = LSF(queue="1nh")
        jSplitter = SplitByFiles(filesPerJob=splitFilesPerJob, maxFiles=splitMaxFiles)
        jSplitter.ignoremissing = True
    elif backEnd == "online":
        jBackend = "online"  # jBackend won't be used for online
        jSplitter = "online"  # jSplitter won't be used for online
    else:
        queuePatt = re.compile(r"lsf_([\w]+)")
        queueMatch = queuePatt.match(backEnd)
        LSF_queue = queueMatch.group(1)
        jBackend = LSF(queue=LSF_queue)
        jSplitter = SplitByFiles(filesPerJob=splitFilesPerJob, maxFiles=splitMaxFiles)
        jSplitter.ignoremissing = True
    # -------------------------------------------------------------------------------

    # -------------------------------------------------------------------------------
    # initialize the verdict variable
    verdict = "CONTINUE"
    # -------------------------------------------------------------------------------
    # PN - We don't run HPD alignment anymore, so this is always false
    runHPDalignment = "false"

    # will need this later
    delthetawin = str(float(deltaThetaWindow) / 1000)

    ###########################################################################
    # We have done all the pre-iterative preparation!
    # Thankful for python variable scoping rules
    ###########################################################################

    command = "xxxxx"  # set a default for OFFLINE
    n_it = 0  # set minor iteration to 0
    minors = tiltNamesLength + 1

    # PN - Initialize the ONLINE communicator.
    if backEnd == "online":
        print("ONLINE")
        # import the communicator (python gives a warning, it is preferred to do this only at top level)
        from Communicator import *

        # Start the communicator, before I do *any* iterative mirror alignment:
        com = Communicator(
            "ALIGNITER"
        )  # com established for the rest of this mirroralign function
        print("com = Communicator('ALIGNITER') STARTED")
        # FSM loop preparation
        state = State.NOT_READY
        com.set_status(state)  # start in state NOT_READY
        print("com.set_status(State.NOT_READY)")

    # Massive while loop, necessary for ONLINE. For OFFLINE this is an innocuous loop like the original while iterationCount < maximumNumberOfIterations loop.
    ## justGlobalFit = 0 is typically what you want.
    ## note: stuff from justGlobalFit = 1 has been included into 0 to provide monitoring
    # if justGlobalFit == 0:
    #    while iterationCount < maximumNumberOfIterations :
    while True:  # so now we need to have the effect of: while iterationCount < maximumNumberOfIterations
        if backEnd == "online":
            # Wait for the FSM to give us a command
            command = com.get_command()
        if command == "configure" and state == State.NOT_READY:
            # Set up some xml configuration here, that the first RUNNING will process.
            # let it be be currentMirrorXMLFile on (n_it == 0 && iterationCount == 0)
            # So we dump it to the file that patchBrunel in the RichAnalyzer will expect it to be picked up from
            os.system("cp " + currentMirrorXMLFile + " " + theFileNowForOnline)

            # We are running online, so write a file that can patch Brunel with key
            # variables that the RichAnalyzer needs. The RichAnalyzer should look for this file.
            # The file name will be fixed here, the output can be copied to the correct filename in the workDir when need be.
            if os.path.exists(workDir + "patch.py"):
                os.system(
                    "rm -rf " + workDir + "patch.p*"
                )  # to get rid of .pyc files, as if the .pyc file is still there it doesn't know that the patch has changed
            patchPyFile = open(workDir + "patch.py", "w")
            patchPyFile.write(
                "FullRichName = 'Rich" + RichDetectorStr + "' \n"
            )  # python will convert \n to os.linesep
            patchPyFile.write("DataFileDir = '" + eventSelectorInputFile + "' \n")
            # hostName = os.getenv('HOSTNAME','unknownHost')
            # onlineHistoFile = "RichRecQCHistos_rich"+RichDetectorStr+"_"+hostName+".root"
            # print "Online ROOT filename ", onlineHistoFile
            # patchPyFile.write('HistoName = \''+workDir+onlineHistoFile+'\' \n')
            patchPyFile.write(
                "DeltaThetaRange = [ 0.04," + delthetawin + ", " + delthetawin + "] \n"
            )
            patchPyFile.write("BrunelEvtMax = " + brunelEvtMax + " \n")
            patchPyFile.write("BrunelPrintFrequency = " + brunelPrintFreq + " \n")
            patchPyFile.write(
                "TheFileNowForOnline = '" + theFileNowForOnline + "' \n"
            )  # the location of XML file to be picked up by the analyzer
            patchPyFile.close()

            state = State.READY
        elif command == "start" and state == State.READY:
            # From READY we can start the Analyzers,
            # but we will throw this and every (minors)th iteration away
            # This is to maintain compatibility with the offline alignment.
            # Unfortunately the alignment will take 1/tiltNamesLength longer
            # This is a performance hit that for now we will have to absorb
            state = State.RUNNING
        elif command == "stop" and state in (State.RUNNING, State.READY):
            # Next time we go to READY the iterations should be done
            state = State.READY
        elif command == "reset":
            # After a stop, we need a reset to break out of the loop
            state = State.NOT_READY
            break
        elif not (
            command == "xxxxx" or (command == "pause" and state == State.RUNNING)
        ):
            # Something remaining that was unexpected
            print("iterator: bad transition from %s to %s" % (state, command))
            state = State.ERROR
            break
        else:  # time to iterate
            if n_it == 0 and backEnd == "online":
                PatchIteratorInfo = getPatchFile(workDir + "patchIterator.py")
                PatchIteratorMembers = [
                    attr
                    for attr in dir(PatchIteratorInfo)
                    if not callable(getattr(PatchIteratorInfo, attr))
                    and not attr.startswith("__")
                ]
                print("PatchIteratorMembers:")
                print(PatchIteratorMembers)
                AnalyzerWorkDir = PatchIteratorInfo.AnalyzerWorkDir

            print("major iteration %d" % iterationCount)
            print("minor iteration %d" % n_it)
            if (
                iterationCount >= maximumNumberOfIterations
            ):  # When State is back to READY the iterations are over
                print("iterator done")
                if backEnd == "online":
                    state = State.PAUSED
                    com.set_status(state)
                    state = State.READY
                else:
                    break
            else:
                if (
                    (n_it % minors) == 0
                ):  # iteration 0[0], 10[2], 20[4], etc... for tiltNamesLength = 9[1]
                    if backEnd == "dirac":
                        os.system(
                            "kinit -R"
                        )  # renews your local AFS ticket [for up to 7 days, given that "kinit -fp -r 7d" was run from the submitting machine, I think]
                        os.system("aklog")  # renews your local AFS token
                        # gridProxy.renew() # reactivate() #  no longer needed, use -v 96:00 when you lhcb-proxy-init

                    # PN - This is never used... should it be?
                    # repeatIteration = False # reset this flag

                    print(
                        "### ----------------------------------------------------------------------------"
                    )
                    print(
                        "### starting major iteration named Iteration No. "
                        + str(iterationCount)
                    )
                    print(
                        "### ----------------------------------------------------------------------------"
                    )
                    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                    print("-------------------")
                    # -------------------------------------------------------------------------------
                    if magnifCoeffMode == 2:
                        # let us create 8 misaligned xml files: modified by +/- coeffCalibrTilt Mirror.xml files
                        # for evaluation of the magnification coefficients on-fly
                        create_coeff_calibration_xml_files(
                            currentMirrorXMLFile, coeffCalibrTilt, richDetector
                        )  # PN - chose not to add workDir in front of XML file... hopefully this is OK as we should be in the workDir anyway. Would like to change this in the future but is not urgent.

                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        print(
                            "### modified by +/- "
                            + str(coeffCalibrTilt)
                            + " mrad Mirror.xml files have just been prepared"
                        )

                    # ============================================================================
                    # === start Brunel for normal case and for evaluation of the coefficients ====
                    # ============================================================================
                    # GANGA: we prepare dictionary and sequence of job objects (9) for each major iteration
                    # ONLINE: these just remain empty
                    jobIds = []
                    tilt_jobId = {}
                    jobId_tilt = {}
                    jobId_submittedSince = {}

                #                if (iterationCount == 0 and skipFirstRecoJob == 1) :
                if skipFirstRecoJob == 1:
                    if (n_it % minors) == 0:
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        print(
                            "### ---------- SKIPPING BRUNEL RECONSTRUCTION FOR MAJOR ITERATION "
                            + str(iterationCount)
                            + " ------------------"
                        )
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        n_it += tiltNamesLength + iterationCount * minors
                        print(
                            "We have just skipped straight to minor iteration %d" % n_it
                        )
                        # COMMENTED OUT, WE CANNOT DO THIS HERE! IT IS NEEDED LATER
                        # skipFirstRecoJob = 0
                else:
                    # run Brunel for the main and for all the tilted variants
                    # for tiltName in tiltNames :
                    if (
                        (n_it % minors) != tiltNamesLength
                    ):  # iterations 0-8[0] for tiltNamesLength = 9[1]
                        tiltName = tiltNames[(n_it - iterationCount) % tiltNamesLength]
                        # -------------------------------------------------------------------------------
                        # create the [replaceable] symlinks to local
                        # Mirrors_rich2_"+theseNamesWithTilts[tiltName]+"_i"+str(iterationCount)+".xml
                        # so only .xml file names contain the iterationCount, while
                        # directories' and dbs' names do not
                        # -------------------------------------------------------------------------------
                        os.system(
                            "ln -sf "
                            + workDir
                            + "Rich"
                            + RichDetectorStr
                            + "CondDBUpdate_"
                            + theseNamesWithTilts[tiltName]
                            + "_i"
                            + str(iterationCount)
                            + ".xml  "
                            + workDir
                            + "Mirrors_rich"
                            + RichDetectorStr
                            + "_"
                            + theseNamesWithTilts[tiltName]
                            + "/Conditions/Rich"
                            + RichDetectorStr
                            + "/Alignment/Mirrors.xml"
                        )
                        # PN - Apparently the current .db file has outlived its usefulness, if it is there, then remove it.
                        #    - A .db file will, of course, not be there for ONLINE
                        if os.path.exists(
                            workDir
                            + "Mirrors_rich"
                            + RichDetectorStr
                            + "_"
                            + theseNamesWithTilts[tiltName]
                            + ".db"
                        ):
                            os.system(
                                "rm -rf "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + theseNamesWithTilts[tiltName]
                                + ".db"
                            )

                        if backEnd != "online":
                            # create local [replaceable] working copy of the conditions database slice
                            # containing only Mirrors.xml branch
                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            print(
                                "### converting the whole Mirrors.xml branch into SQLite format:"
                            )
                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            # prepare CondDB sqlite_file for this tilt, which will be additional layer to the main database
                            print(
                                "copy_files_to_db.py  -s "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + theseNamesWithTilts[tiltName]
                                + "  -c sqlite_file:Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + theseNamesWithTilts[tiltName]
                                + ".db/"
                                + CONDDB
                            )

                            p = Popen(
                                "export   HOME="
                                + home
                                + ";"
                                + "source "
                                + pathToScripts
                                + "LbLogin.sh;"
                                + "source  `which SetupProject.sh` LHCb "
                                + LHCbVersion
                                + " ;"
                                + "copy_files_to_db.py  -s "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + theseNamesWithTilts[tiltName]
                                + "  -c sqlite_file:Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + theseNamesWithTilts[tiltName]
                                + ".db/"
                                + CONDDB
                                + ";"
                                + "python  "
                                + PanoptesJobPath
                                + "/tagger.py  "
                                + CondDB_tag
                                + " "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + theseNamesWithTilts[tiltName]
                                + " "
                                + CONDDB,
                                shell=True,
                                executable="/bin/bash",
                                stdout=PIPE,
                                stderr=PIPE,
                            )
                            (stdout, stderr) = p.communicate()
                            stdReport(stdout, stderr)

                        print(
                            "### ----------------------------------------------------------------------------"
                        )

                        outputHistoFile = (
                            "RichRecQCHistos_rich"
                            + RichDetectorStr
                            + "_"
                            + theseNamesWithTilts[tiltName]
                            + "_i"
                            + str(iterationCount)
                            + ".root"
                        )
                        print("Output ROOT filename ", outputHistoFile)

                        # -------------------------------------------------------------------------------
                        # the job configuration
                        # -------------------------------------------------------------------------------
                        # PN - OK this is where it gets interesting.
                        # There are two parts of the code where we need to
                        # make this split in two directions.
                        # One is "ONLINE" the other is "GANGA"
                        # Ganga sets things up in the usual Offline (Ganga) way
                        # Online needs to conform to the Online Alignment Framwork
                        #
                        # First, ONLINE
                        if backEnd == "online":
                            print("ONLINE")
                            # print "The FSM controls all"
                            # Pause the Analyzers. The histograms from the first running should now be available
                            state = State.PAUSED
                            com.set_status(state)
                            # collect histograms (n_it = 0 is a throwaway probably) and store for later use
                            print(
                                "We need to figure out where the Analyzer should drop the histograms AND copy them to the right place"
                            )

                            # Prepare the xml for the next RichAnalyzer crunching
                            # Dump it to the file that patchBrunel in the RichAnalyzer will expect it to be picked up from
                            theFileNowInTheIterator = (
                                workDir
                                + "Rich"
                                + RichDetectorStr
                                + "CondDBUpdate_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".xml"
                            )
                            os.system(
                                "cp "
                                + theFileNowInTheIterator
                                + " "
                                + theFileNowForOnline
                            )

                            ## Ideally we'd patch Brunel somehow with a new file name, but it is hard at the moment... easier to just copy output to the right place for now
                            # print "We need to prepare the Analyzer with any specific Brunel Options we need for this tiltname"
                            # if ( os.path.exists( workDir+"patch.py" ) ) :
                            #    os.system("rm -rf "+workDir+"patch.p*") # to get rid of .pyc files, as if the .pyc file is still there it doesn't know that the patch has changed
                            # patchPyFile = open(workDir+'patch.py','w')
                            # patchPyFile.write('FullRichName = \'Rich'+RichDetectorStr+'\'\n') # python will convert \n to os.linesep
                            # patchPyFile.write('HistoName = \''+workDir+outputHistoFile+'\'\n')
                            # patchPyFile.write('DeltaThetaRange = [ 0.04,'+delthetawin+', '+delthetawin+']\n')
                            # patchPyFile.close()

                            # Since we can't do the above, just copy the newest file now for online
                            os.system(
                                "cp "
                                + theFileNowInTheIterator
                                + " "
                                + AnalyzerWorkDir
                                + "Rich"
                                + RichDetectorStr
                                + ".xml"
                            )

                        # Second, GANGA (i.e. not ONLINE)
                        else:
                            print("GANGA")

                            gangaVersion = config.System.GANGA_VERSION
                            print("We are using " + gangaVersion)
                            GV61flag = True
                            # Assuming no one is using anything older than Ganga 5
                            if "Ganga-6-0" in gangaVersion:
                                GV61flag = False
                            if "Ganga-5-" in gangaVersion:
                                GV61flag = False
                            if GV61flag == True:
                                print(
                                    "Proceeding using job options for Ganga 6.1 or later."
                                )
                            else:
                                print(
                                    "Proceeding using job options for Ganga 6.0 or earlier."
                                )

                            jAppBrunel = Brunel(version=brunelVersion)
                            brunelVer = jAppBrunel.version
                            # -------------------------------------------------------------------------------
                            """
                            Only during the 0-th major iteration we take input data in the form of
                            configuration and therefore append the file list as a configuration
                            file.

                            For the rest of the major iterations we use Ganga's "inputdata" attribute of
                            Job object; see below.
                            """

                            # jAppBrunel.optsfile = [File( BrunelOptsPath+"/Brunel_RichAlignMoni_"+dataVariant+"-"+brunelInitialInputType+".py")]
                            jAppBrunel.optsfile = [
                                File(
                                    BrunelOptsPath
                                    + "/Brunel_Rich"
                                    + RichDetectorStr
                                    + "AlignMoni_"
                                    + dataVariant
                                    + "-"
                                    + brunelInitialInputType
                                    + ".py"
                                )
                            ]
                            print("### using the following Brunel options file:")
                            # print "### "+BrunelOptsPath+"/Brunel_RichAlignMoni_"+dataVariant+"-"+brunelInitialInputType+".py"
                            print(
                                "### "
                                + BrunelOptsPath
                                + "/Brunel_Rich"
                                + RichDetectorStr
                                + "AlignMoni_"
                                + dataVariant
                                + "-"
                                + brunelInitialInputType
                                + ".py"
                            )

                            DBSlices = []
                            DBSlicesNames = []
                            BrunelOptionsString = ""
                            ThereAreAddedDBSlices = False
                            LFNDBs = True
                            if (
                                len(AddedDB) > 0
                                and len(AddedDB[0]) > 1
                                and backEnd == "dirac"
                            ):  # note: can not use databases outside of dirac
                                ThereAreAddedDBSlices = True
                                for i in range(0, len(AddedDB)):
                                    if not AddedDB[i].startswith("LFN"):
                                        LFNDBs = False  # databases are local
                                    AddedDB2 = AddedDB[i].split("/")
                                    for j in AddedDB2:
                                        if j.endswith(".db"):
                                            DBSlices.append(j)
                                            print(j)
                                            AddedDBName = j.split(".")
                                            for k in AddedDBName:
                                                if not k.endswith("db"):
                                                    DBSlicesNames.append(k)
                                    BrunelOptionsString = (
                                        BrunelOptionsString
                                        + """CondDB().addLayer( CondDBAccessSvc(\'"""
                                        + DBSlicesNames[i]
                                        + """\', ConnectionString=\'sqlite_file:"""
                                        + DBSlices[i]
                                        + """/LHCBCOND\', DefaultTAG=\'HEAD\') )\n"""
                                    )

                            print(BrunelOptionsString)

                            if dataVariant in collisionVariantList:
                                tiltname = theseNamesWithTilts[tiltName]
                                jAppBrunel.extraopts = (
                                    "Brunel().DDDBtag              = '{DDDB_tag}'\n"
                                    "Brunel().CondDBtag            = '{CondDB_tag}'\n"
                                    "Brunel().EvtMax               =  {brunelEvtMax}\n"
                                    "Brunel().PrintFreq            =  {brunelPrintFreq}\n"
                                    "RichAlignmentConf ('OfflineRichMoni_RichAlignmentConf').setProp('Radiators', ['Rich{RichDetectorStr}Gas'])\n"
                                    "MessageSvc().OutputLevel      =   {messageSvcOutputLevel}\n"
                                    "RichAlignmentConf('OfflineRichMoni_RichAlignmentConf').setProp('DeltaThetaRange', [ 0.04,{delthetawin}, {delthetawin}])\n"
                                    "CondDB().addLayer( CondDBAccessSvc('Rich{RichDetectorStr}AlignMirr', ConnectionString='sqlite_file:Mirrors_rich{RichDetectorStr}_{tiltname}.db/{CONDDB}', DefaultTAG='HEAD'))\n"
                                    "from Configurables import EventClockSvc\n"
                                    "EventClockSvc( InitialTime = {date} )\n"
                                    "HistogramPersistencySvc().setProp('OutputFile', 'RichRecQCHistos_rich{RichDetectorStr}_{tiltname}_i{iterationCount}.root')\n"
                                    "{BrunelOptionsString}\n"
                                    "{BrunelOptionsString}\n".format(**locals())
                                )

                            jName = (
                                tiltInJobnameDict[tiltName]
                                + thisCase
                                + "_i"
                                + str(iterationCount)
                            )

                            j = Job(
                                name=jName,
                                application=jAppBrunel,
                                splitter=jSplitter,
                                backend=jBackend,
                            )

                            # For Ganga 6.1 and later, we'll leave this alone for now, hopefully it works
                            try:
                                tryString = eventSelectorInputFile + "abcd"
                            except TypeError:
                                # print "Not a String"
                                for ff in range(0, len(eventSelectorInputFile)):
                                    if ff == 0:
                                        j.inputdata = j.application.readInputData(
                                            eventSelectorInputFile[ff]
                                        )
                                    else:
                                        j.inputdata.extend(
                                            j.application.readInputData(
                                                eventSelectorInputFile[ff]
                                            )
                                        )
                            else:
                                # print "String"
                                j.inputdata = j.application.readInputData(
                                    eventSelectorInputFile
                                )

                            if backEnd != "dirac":
                                print("Not Dirac")

                            if GV61flag == False:
                                j.inputsandbox += [
                                    workDir
                                    + "Mirrors_rich"
                                    + RichDetectorStr
                                    + "_"
                                    + theseNamesWithTilts[tiltName]
                                    + ".db"
                                ]
                            else:
                                j.inputfiles += [
                                    LocalFile(
                                        workDir
                                        + "Mirrors_rich"
                                        + RichDetectorStr
                                        + "_"
                                        + theseNamesWithTilts[tiltName]
                                        + ".db"
                                    )
                                ]

                            if ThereAreAddedDBSlices == True and LFNDBs == True:
                                if GV61flag == False:
                                    j.backend.inputSandboxLFNs = AddedDB
                                    # PN - Now we have some print statements to check that the AddedDBs from the settings file are as expected.
                                    print("### j.backend.inputSandboxLFNs now reads:")
                                    for keys in j.backend.inputSandboxLFNs:
                                        print(keys)
                                else:
                                    print(
                                        "NOTE: This is UNTESTED in Ganga 6.1!!!! If it works, then please remove this print statement."
                                    )
                                    GV61AddedDB = []
                                    for LFNstring in AddedDB:
                                        if LFNstring.startswith("LFN:"):
                                            NewLFNstring = LFNstring[4:]
                                        else:
                                            NewLFNstring = LFNstring
                                        GV61AddedDB.append(DiracFile(lfn=NewLFNstring))
                                    j.inputfiles += GV61AddedDB

                            if ThereAreAddedDBSlices == True and LFNDBs == False:
                                # print "NOTE: This Use Case NEEDS TO BE TESTED if you want to read .db from local files instead of LFNs (If it works, remove this print statement)"
                                if GV61flag == False:
                                    j.inputsandbox += AddedDB
                                else:
                                    GV61AddedDB = []
                                    for xx in AddedDB:
                                        GV61AddedDB.append(LocalFile(xx))
                                    j.inputfiles += GV61AddedDB

                            # For Ganga 6.1 and later, we'll leave this alone for now, hopefully it works
                            j.outputfiles = ["Ntuple_2015-Collisions.root"]

                            j.do_auto_resubmit = True  # this automatically resubmits failed subjobs, *if* at least one of the subjobs completed

                            # Try it later
                            #                            if GV61flag == True:
                            #                               j.parallel_submit = True # Experimental parallel job submission, so we're trying it

                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            print(j)
                            ## Ban sites that fail
                            # j.backend.settings['BannedSites'] = ['LCG.PIC.es','LCG.SARA.nl','LCG.NIKHEF.nl','LCG.CERN.ch']
                            ## Ban "ANY" sites by specifying remaining sites
                            # j.backend.settings['Destination'] = ['LCG.GRIDKA.de','LCG.IN2P3.fr','LCG.CNAF.it']
                            print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                            print("-------------------")
                            # j.submit()                 # Ganga 6 lets you parallelize this!!!!!!!!
                            queues.add(j.submit)
                            print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                            print("-------------------")

                            jobIds.append(j.id)
                            tilt_jobId[tiltName] = j.id
                            jobId_tilt[j.id] = tiltName
                            jobId_submittedSince[j.id] = time()

                        # -------------------------------------------------------------------------------

                if (
                    (n_it % minors) == tiltNamesLength
                ):  # iteration 9,19...[1,3...] for tiltNamesLength = 9[1]
                    # ============================================================================
                    # end of loop over the tilts
                    # ============================================================================
                    # ============================================================================
                    # Wait until enough subjobs have finished until we continue
                    # ============================================================================

                    #                    if iterationCount == 0 and skipFirstMergeJob == 1 :
                    if skipFirstMergeJob == 1:
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        print(
                            "### ---------- SKIPPING MERGE OF BRUNEL HISTOGRAMS FOR MAJOR ITERATION "
                            + str(iterationCount)
                            + " -------------"
                        )
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        skipFirstMergeJob = 0
                    else:
                        # PN: Again, trying to make split this in two directions.
                        # One is "GANGA" the other is "ONLINE"
                        #
                        # First, ONLINE
                        if backEnd == "online":
                            print("ONLINE")
                            # Pause the Analyzers. The histograms from the first running should now be available
                            state = State.PAUSED
                            com.set_status(state)
                            print(
                                "The Analyzers will have finished running Brunel on the data by the time it gets here."
                            )
                            print(
                                "Loop over the tiltnames and merge the histos (maybe automatic)."
                            )
                            print(
                                "TO DO: swap outputdir stuff for where Online puts the histos and where we need our output."
                            )
                            print("Let's not worry about the adding for now")
                            print("Until the FSM control above is actually working")
                            # Set up some xml configuration here, that the next RUNNING will process.
                            ## can't patch yet maybe don't need to so this is commented out
                            # if ( os.path.exists( workDir+"patch.py" ) ) :
                            #    os.system("rm -rf "+workDir+"patch.p*") # to get rid of .pyc files, as if the .pyc file is still there it doesn't know that the patch has changed
                            # patchPyFile = open(workDir+'patch.py','w')
                            # patchPyFile.write('FullRichName = \'Rich'+RichDetectorStr+'\'\n') # python will convert \n to os.linesep
                            # patchPyFile.write('HistoName = \''+workDir+'Dummy2.root\'\n')
                            # patchPyFile.write('DeltaThetaRange = [ 0.04,'+delthetawin+', '+delthetawin+']\n')
                            # patchPyFile.close()

                            # Actually we don't bother changing the xml file, as the next RUNNING will be scrapped

                        # Second, GANGA (i.e. not ONLINE)
                        else:
                            print("GANGA")
                            # we must first wait for the queues threads to finish their work.
                            print(
                                "Waiting for the queues threads to finish submitting all of the jobs."
                            )
                            print("-------------------")

                            while queues.totalNumUserThreads() > 0:
                                print(
                                    "Total number of user threads still active %d"
                                    % queues.totalNumUserThreads()
                                )
                                print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                                print("-------------------")
                                # create an empty file, can check the timestamp in your workDir to see if alignment is still running
                                os.system("touch Mirrors_SubmittingJobs_touch.txt")
                                sleep(
                                    300
                                )  # check the status of the queues approx. every 5 minutes

                            if skipFirstRecoJob != 1:
                                print(
                                    "All jobs have been submitted for major iteration %d. Now we wait for 80 percent of the subjobs from each job to complete."
                                    % iterationCount
                                )
                            else:
                                print(
                                    "Checking jobIds from the Brunel reconstruction of major iteration %d"
                                    % iterationCount
                                )
                            print("-------------------")
                            # print "I am now going to give the command runMonitoring() [if we run this as a script and not from an interactive Ganga session, the monitoring loop does not run automatically]."
                            # print "If the monitoring loop is already running, the runMonitoring() command will give an ERROR. However this script should continue to run."
                            # runMonitoring()
                            print("-------------------")

                            #                          if iterationCount == 0 and skipFirstRecoJob == 1:
                            if skipFirstRecoJob == 1:
                                jobIds = previousJobNumbers
                                for tiltname, jobId in zip(tiltNames, jobIds):
                                    jobId_tilt[jobId] = tiltname

                            minutesCounter = 0

                            while len(jobIds) > 0:
                                if backEnd == "dirac":
                                    os.system(
                                        "kinit -R"
                                    )  # renews your local AFS ticket [for up to 7 days, given that "kinit -fp -r 7d" was run from the submitting machine, I think]
                                    os.system("aklog")  # renews your local AFS token
                                    # gridProxy.renew() # reactivate() #  no longer needed, use -v 96:00 when you lhcb-proxy-init
                                sleep(
                                    60
                                )  #  just wait to conserve CPU cycles, because some jobs are still not finished
                                minutesCounter = minutesCounter + 1
                                if (minutesCounter % 15) == 0:
                                    print(
                                        "Still waiting for 80 percent of subjobs from all jobs to complete."
                                    )
                                    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                                    # create an empty file, can check the timestamp in your workDir to see if alignment is still running
                                    os.system(
                                        "touch Mirrors_WaitingForSubjobs_touch.txt"
                                    )
                                    print("-------------------")

                                for jobId in jobIds:
                                    sjIDs = []
                                    sjIDsCompleted = []

                                    for sj in jobs(jobId).subjobs:
                                        sjIDs.append(sj.id)
                                        if (sj.status == "completed") or (
                                            sj.status == "failed"
                                        ):
                                            sjIDsCompleted.append(sj.id)
                                        if (
                                            sj.status == "new"
                                        ):  # PN - because we did j.do_auto_resubmit=True before, failed jobs get resubmitted, but I don't know if "new" jobs get resubmitted. So we keep this in.
                                            print("Resubmitting subjob " + str(sj.id))
                                            # Ganga 6 lets you parallelize this, but we don't want to as it's not expected to need much overhead.
                                            sj.submit()

                                    if (
                                        float(len(sjIDsCompleted)) / float(len(sjIDs))
                                        > 0.8
                                    ):
                                        # create an empty file when any job has completed, can check the timestamp in your workDir to see if alignment is still running
                                        os.system(
                                            "touch Mirrors_LastJobCompleted_touch.txt"
                                        )

                                        print(
                                            "Job number "
                                            + str(jobId)
                                            + " has "
                                            + str(len(sjIDsCompleted))
                                            + " out of "
                                            + str(len(sjIDs))
                                            + " subjobs completed or failed so will now merge the outputs\n"
                                        )
                                        print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                                        print("-------------------")

                                        rootFileName = (
                                            "RichRecQCHistos_rich"
                                            + RichDetectorStr
                                            + "_"
                                            + theseNamesWithTilts[jobId_tilt[jobId]]
                                            + "_i"
                                            + str(iterationCount)
                                            + ".root"
                                        )
                                        if not (os.path.exists(jobs(jobId).outputdir)):
                                            os.system("mkdir " + jobs(jobId).outputdir)
                                        rootFile = jobs(jobId).outputdir + rootFileName

                                        #                                # merge output histograms
                                        #                                p = Popen("export   HOME="+home+";"
                                        #                                  +"source "+pathToScripts+"LbLogin.sh;"
                                        #                                  +"export   User_release_area="+userReleaseArea+";"
                                        #                                  +"source  `which SetupProject.sh` Gaudi;"
                                        #                                  +"hadd " + jobs(jobId).outputdir + rootFileName + " " + jobs(jobId).outputdir + "../*/output/" + rootFileName + ";",
                                        #                                   shell=True,
                                        #                                   executable="/bin/bash", stdout=PIPE , stderr=PIPE
                                        #                                  )
                                        #                                (stdout, stderr) = p.communicate()
                                        #                                stdReport(stdout,stderr)

                                        # Claire's new merge output histograms
                                        import ROOT

                                        mergesuccess = False
                                        while not mergesuccess:
                                            os.system(
                                                "rm -rf "
                                                + jobs(jobId).outputdir
                                                + rootFileName
                                            )
                                            # merge output histograms
                                            p = Popen(
                                                "export   HOME="
                                                + home
                                                + ";"
                                                + "source "
                                                + pathToScripts
                                                + "LbLogin.sh;"
                                                + "export   User_release_area="
                                                + userReleaseArea
                                                + ";"
                                                + "source  `which SetupProject.sh` Brunel "
                                                + brunelVersion
                                                + " "
                                                + setupProjectBrunelOpts
                                                + ";"
                                                #                                              +"source  `which SetupProject.sh` Gaudi;"
                                                + "hadd "
                                                + jobs(jobId).outputdir
                                                + rootFileName
                                                + " "
                                                + jobs(jobId).outputdir
                                                + "../*/output/"
                                                + rootFileName
                                                + ";",
                                                shell=True,
                                                executable="/bin/bash",
                                                stdout=PIPE,
                                                stderr=PIPE,
                                            )
                                            (stdout, stderr) = p.communicate()
                                            stdReport(stdout, stderr)

                                            smerged = (
                                                jobs(jobId).outputdir + rootFileName
                                            )
                                            fmerged = ROOT.TFile.Open(smerged)
                                            if fmerged.GetStreamerInfoList():
                                                mergesuccess = True
                                            else:
                                                mergesuccess = False
                                            fmerged.Close()

                                        # and now create link
                                        os.system(
                                            "ln -sf " + rootFile + " " + rootFileName
                                        )

                                        # clear the diskspace
                                        # rm_subjobs_except_0_last( jobs(jobId), rootFileName, brunelVer, backEnd )

                                        jobIds.remove(jobId)  # all done for this job

                                        print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                                        print("-------------------")

                                    else:
                                        if (minutesCounter % 15) == 0:
                                            fracDone = math.ceil(
                                                100
                                                * (
                                                    float(len(sjIDsCompleted))
                                                    / float(len(sjIDs))
                                                )
                                            )
                                            print(
                                                "Job number "
                                                + str(jobId)
                                                + " has just under %d percent of its subjobs completed or failed"
                                                % fracDone
                                            )

                    # ============================================================================
                    # ============================================================================
                    # run per-combination fit to obtain the total tilts and the coefficients

                    # Paras put this in to at least allow multiple instances of RichMirrCombinFit to run simultaneously, see later in the code as well
                    ps = []

                    #                    if iterationCount == 0 and skipFirstCombinFit == 1 :
                    if skipFirstCombinFit == 1:
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        print(
                            "### ----------       SKIPPING FITTING OF MERGED HISTOGRAMS ---------------------"
                        )
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        skipFirstCombinFit = 0

                    else:
                        for tiltName in tiltNames:
                            if backEnd == "dirac":
                                os.system(
                                    "kinit -R"
                                )  # renews your local AFS ticket [for up to 7 days, given that "kinit -fp -r 7d" was run from the submitting machine, I think]
                                os.system("aklog")  # renews your local AFS token
                                # gridProxy.renew() # reactivate() #  no longer needed, use -v 96:00 when you lhcb-proxy-init
                            combinFitDir = (
                                workDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFit_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + os.path.sep
                            )
                            combinFitPlotDir = combinFitDir + "plots" + os.path.sep

                            if not (os.path.exists(combinFitPlotDir)):
                                os.system("mkdir --parents " + combinFitPlotDir)

                            combinResultsFile = (
                                combinFitDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFit_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".txt"
                            )
                            fitConfigFile = (
                                combinFitDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFit_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".conf"
                            )
                            fitOutputFile = (
                                combinFitDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFitOut_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".txt"
                            )
                            fitStdOutFile = (
                                combinFitDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFitStdOut_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".txt"
                            )
                            fitStdErrFile = (
                                combinFitDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFitStdErr_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".txt"
                            )

                            # Paras thinks this should be removed. The only time we'd skip this is on major iteration zero if something went wrong
                            # if ( ( os.path.exists( combinResultsFile ) ) ) :
                            #  continue

                            # Paras says, leave this naming convention alone
                            brunelHistoFile = (
                                "RichRecQCHistos_rich"
                                + RichDetectorStr
                                + "_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".root"
                            )

                            if os.path.exists(fitConfigFile):
                                os.remove(fitConfigFile)
                            f = open(fitConfigFile, "w")
                            args = "plotDir            = " + combinFitPlotDir + "\n"
                            args += "richDetector       = " + str(richDetector) + "\n"
                            args += (
                                "mirrCombinSubset   = "
                                + workDir
                                + mirrCombinSubset
                                + "\n"
                            )
                            args += (
                                "minAverageBinPop   = " + str(minAverageBinPop) + "\n"
                            )
                            args += (
                                "deltaThetaWindow   = " + str(deltaThetaWindow) + "\n"
                            )
                            args += (
                                "combinFitVariant   = " + str(combinFitVariant) + "\n"
                            )
                            args += "phiBinFactor       = " + str(phiBinFactor) + "\n"
                            args += "stopTolerance      = " + str(stopTolerance) + "\n"
                            args += "warningFactor      = " + str(warningFactor) + "\n"
                            args += "iterationCount     = " + str(iterationCount) + "\n"
                            args += (
                                "inputHistoFile     = "
                                + str(workDir + brunelHistoFile)
                                + "\n"
                            )
                            args += (
                                "outputResultsFile  = " + str(combinResultsFile) + "\n"
                            )
                            args += "plotOutputLevel    = 2 \n"
                            f.write(args)
                            f.close()

                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            print(
                                "### We are still in major iteration %d"
                                % iterationCount
                            )
                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            print(
                                "### running fitting program that yields total tilts being produced by each pair"
                            )
                            print(
                                "### of primary and secondary mirror segments out of a set of combinations chosen"
                            )
                            print(
                                "### in advance, and also the rotation-to-total-tilt magnification coefficients"
                            )
                            print("### for each mirror in such a pair:")
                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            print(
                                "### Now submitting subprocess for tilt named: "
                                + theseNamesWithTilts[tiltName]
                            )
                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                            print("-------------------")

                            myStdOut = open(fitStdOutFile, "w")
                            myStdErr = open(fitStdErrFile, "w")
                            p = Popen(
                                "export HOME="
                                + home
                                + ";"
                                + "export User_release_area="
                                + userReleaseArea
                                + ";"
                                + "source "
                                + pathToScripts
                                + "LbLogin.sh;"
                                + "source `which SetupProject.sh` Panoptes "
                                + setupProjectPanoptesVersion
                                + " "
                                + setupProjectPanoptesOptions
                                + ";"
                                + "$RICHMIRRCOMBINFITROOT/$CMTCONFIG/RichMirrCombinFit.exe "
                                + fitConfigFile
                                + " "
                                + RichDetectorStr
                                + " 0 > "
                                + fitOutputFile,
                                shell=True,
                                executable="/bin/bash",
                                stdout=myStdOut,
                                stderr=myStdErr,
                            )
                            myStdOut.close()
                            myStdErr.close()
                            # Add p to list of subprocesses
                            ps.append(p)

                    # Now wait for all subprocesses to finish
                    PopenCount = -1
                    while True:
                        sleep(
                            30
                        )  #  just wait to conserve CPU cycles, because some processes are still not finished
                        ps_status = [q.poll() for q in ps]
                        if all([x is not None for x in ps_status]):
                            PopenTotal = 0
                            for l in ps_status:
                                if l is not None:
                                    PopenTotal += 1
                            print(
                                "%d of %d RichMirrCombinFit processes complete"
                                % (PopenTotal, tiltNamesLength)
                            )
                            print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                            print("-------------------")
                            break
                        else:
                            PopenDone = 0
                            for l in ps_status:
                                if l is not None:
                                    PopenDone += 1
                            if PopenDone > PopenCount:
                                print(
                                    "%d of %d RichMirrCombinFit processes complete"
                                    % (PopenDone, tiltNamesLength)
                                )
                                print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                                print("-------------------")
                                PopenCount = (
                                    0 + PopenDone
                                )  # add the 0 so that PopenCount doesn't become a pointer to PopenNow

                    # jid = j.id

                    # wish to copy the output decision
                    # PN - think this is now made after RichMirrAlign
                    # os.system("cp "+j.outputdir+"Rich"+str(richDetector)+"_stop_or_continue.txt .")

                    # Check for 'not_good_enough' which means there are not enough populated phi bins in the histos
                    # NGEOutFile should have 0 lines in it
                    # We only really need to check results from the current major iteration xml, not all of the tilts.
                    # Numbers of photons should be approximately the same for each tilt.
                    combinFitDir_c = (
                        workDir
                        + "Rich"
                        + RichDetectorStr
                        + "MirrCombinFit_"
                        + thisNameStr
                        + "_i"
                        + str(iterationCount)
                        + os.path.sep
                    )
                    fitOutputFile_c = (
                        combinFitDir_c
                        + "Rich"
                        + RichDetectorStr
                        + "MirrCombinFitOut_"
                        + thisNameStr
                        + "_i"
                        + str(iterationCount)
                        + ".txt"
                    )
                    fitNGEFile_c = (
                        combinFitDir_c
                        + "Rich"
                        + RichDetectorStr
                        + "MirrCombinFitNGEOut_"
                        + thisNameStr
                        + "_i"
                        + str(iterationCount)
                        + ".txt"
                    )
                    os.system(
                        "cat "
                        + fitOutputFile_c
                        + ' | grep " not_good_enough " > '
                        + fitNGEFile_c
                    )
                    num_lines = sum(1 for line in open(fitNGEFile_c))
                    # print num_lines
                    if num_lines != 0:
                        #       Uncomment the lines in this if statement, if you are ready to use STOP_NGE to exit the alignment
                        print(
                            "### --------------------------------------------------------------------------------------------"
                        )
                        #            print "### No. "+str(iterationCount)+" major iteration's verdict is:  STOP_NGE!"
                        #            print "### The mirror alignment has stopped, likely because there were not enough events for alignment."
                        print(
                            "### --------------------------------------------------------------------------------------------"
                        )
                        iterations = " major iterations !"
                        if iterationCount + 1 == 1:
                            iterations = " major iteration !"
                        #            print "### RICH"+RichDetectorStr+" mirror alignment is finished after "+str(iterationCount+1)+iterations
                        print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                        print("-------------------")
                        #            os.system('echo "STOP_NGE" > '+"Rich"+RichDetectorStr+"_stop_or_continue.txt")
                        #            break [actually, NOW UNCOMMENTING THIS WON'T WORK... need to find another way to break the alignment]
                        print(
                            "STOP_NGE would have broken the alignment here due to lack of events, but we're just testing this feature right now and it is not implemented yet."
                        )

                    # ============================================================================
                    # ======================= FIND MAGNIFICATION FACTORS =========================
                    # ============================================================================

                    if magnifCoeffMode == 2:
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        print(
                            "### --------------     CALCULATING THE MAGNIFICATION FACTORS     ---------------"
                        )
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                        print("-------------------")

                        pNoTilt = []

                        for tiltNum, tiltName in enumerate(tiltNames):
                            combinFitDir = (
                                workDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFit_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + os.path.sep
                            )
                            combinResultsFile = (
                                combinFitDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrCombinFit_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".txt"
                            )

                            magnificationOutputFile = (
                                workDir
                                + "Rich"
                                + RichDetectorStr
                                + "MirrMagnFactors_"
                                + theseNamesWithTilts[tiltName]
                                + "_i"
                                + str(iterationCount)
                                + ".txt"
                            )

                            # Each line in input file is as follows:  | mirrorCombination | cos coeff | err(cos coeff) | sin coeff | err(sin coeff)
                            pWithTilt = []
                            with open(combinResultsFile) as fitfile:
                                line = fitfile.readline()
                                while line:
                                    pars = line.split()
                                    if len(pars) is not 7:
                                        line = fitfile.readline()
                                        continue

                                    newpars = []
                                    for i, j in enumerate(pars):
                                        # print j
                                        if i == 0:
                                            # newpars.append(j)
                                            newpars.append(str(j))
                                        else:
                                            newpars.append(float(j))

                                    if (
                                        tiltNum == 0
                                    ):  # just main fit results, no tilts for determination of magnification factors
                                        pNoTilt.append(newpars)
                                    else:  # fit results when mirrors were tilted for determination of magnification factors
                                        pWithTilt.append(newpars)

                                    line = fitfile.readline()

                            if tiltNum != 0:  # only when mirrors were tilted
                                with open(magnificationOutputFile, "w") as magfile:
                                    for i, sp in enumerate(pWithTilt):
                                        tiltSign = 1.0
                                        if tiltName.count("neg") > 0:
                                            tiltSign = -1.0
                                        mirrPair = sp[0]
                                        Y = (sp[1] - pNoTilt[i][1]) / (
                                            coeffCalibrTilt * tiltSign
                                        )
                                        Z = (sp[3] - pNoTilt[i][3]) / (
                                            coeffCalibrTilt * tiltSign
                                        )

                                        # magfile.write("{mirrPair:4d}  {Y: 9.5f}  {Z: 9.5f}\n".format(**locals()))
                                        magfile.write(
                                            "{mirrPair:s}  {Y: 9.5f}  {Z: 9.5f}\n".format(
                                                **locals()
                                            )
                                        )

                    # ============================================================================
                    # ========================= START OF PER-MIRROR FIT ==========================
                    # ============================================================================
                    # Rich2MirrAlign: misalignment correction for each mirror segment
                    # ============================================================================

                    richMirrAlignConfFile = (
                        workDir
                        + "Rich"
                        + RichDetectorStr
                        + "MirrAlign_i"
                        + str(iterationCount)
                        + ".conf"
                    )
                    richMirrAlignOutFile = (
                        workDir
                        + "Rich"
                        + RichDetectorStr
                        + "MirrAlignOut_i"
                        + str(iterationCount)
                        + ".txt"
                    )
                    richMirrAlignNanOutFile = (
                        workDir
                        + "Rich"
                        + RichDetectorStr
                        + "MirrAlignNanOut_i"
                        + str(iterationCount)
                        + ".txt"
                    )
                    # Let's not implement these for now
                    # richMirrAlignStdOutFile = "Rich"+RichDetectorStr+"MirrAlignStdOut_i"+str(iterationCount)+".txt"
                    # richMirrAlignStdErrFile = "Rich"+RichDetectorStr+"MirrAlignStdErr_i"+str(iterationCount)+".txt"

                    combinFitDir = (
                        workDir
                        + "Rich"
                        + RichDetectorStr
                        + "MirrCombinFit_"
                        + theseNamesWithTilts[""]
                        + "_i"
                        + str(iterationCount)
                        + os.path.sep
                    )
                    combinResultsFile = (
                        combinFitDir
                        + "Rich"
                        + RichDetectorStr
                        + "MirrCombinFit_"
                        + theseNamesWithTilts[""]
                        + "_i"
                        + str(iterationCount)
                        + ".txt"
                    )

                    if os.path.exists(richMirrAlignConfFile):
                        os.remove(richMirrAlignConfFile)
                    f2 = open(richMirrAlignConfFile, "w")
                    args = "workDir              = " + workDir + "\n"
                    args += "thisVariant          = " + thisNameStr + "\n"
                    #        args += 'thisNameStr        = '+thisNameStr             +'\n'
                    args += "richDetector         = " + str(richDetector) + "\n"
                    args += "magnFactorsMode      = " + str(magnifCoeffMode) + "\n"
                    #        args += 'magnifCoeffMode    = '+str(magnifCoeffMode)    +'\n'
                    args += "solutionMethod       = " + str(solutionMethod) + "\n"
                    args += "usePremisaligned     = " + usePremisaligned + "\n"
                    args += "iterationCount       = " + str(iterationCount) + "\n"
                    args += (
                        "combAndMirrSubsets   = " + workDir + mirrCombinSubset + "\n"
                    )
                    #        args += 'mirrCombinSubset   = '+workDir+mirrCombinSubset+'\n'
                    args += "mirrCombinFitResults = " + combinResultsFile + "\n"
                    #        args += 'outputResultsFile       = '+combinResultsFile+'\n'
                    args += "stopTolerance        = " + str(stopTolerance) + "\n"
                    args += (
                        "zerothIterationXML   = " + workDir + zeroMirrorXMLFile + "\n"
                    )
                    #        args += 'zerothIterationXMLFile    = '+workDir+zeroMirrorXMLFile+'\n'
                    #        args += 'iterationZeroXMLFile    = '+workDir+zeroMirrorXMLFile+'\n'                                #
                    args += (
                        "currentIterationXML  = "
                        + workDir
                        + currentMirrorXMLFile
                        + "\n"
                    )
                    #        args += 'currentIterationXMLFile = '+workDir+currentMirrorXMLFile+'\n'
                    args += (
                        "nextIterationXML     = "
                        + workDir
                        + nextIterationXMLFile
                        + "\n"
                    )
                    #        args += 'nextIterationXMLFile    = '+workDir+nextIterationXMLFile+'\n'
                    f2.write(args)
                    f2.close()
                    """
                    Now we can run the per-mirror fit (Rich2MirrAlign) using
                    Mirrors_rich2_thisNameStr_iterationCount.xml, to which Mirrors.xml was
                    linked and hence used during reconstruction within this major iteration,
                    and then evaluate the absolute value of each correction obtained.

                    If abs values of all of them < stopTolerance*mrad, we stop. That will
                    result in filling file "Rich2_stop_or_continue.txt" with "stop"
                    string. If abs of at least one of them >= stopTolerance*mrad, we will go
                    ahead replacing correction values used in the Mirrors.xml that
                    was used for this major iteration, with the updated corrections = (used
                    corrections) - (newly detected misalignments) which will be used
                    during the next major iteration.

                    We store them in Mirrors_rich2_thisNameStr_iterationCount+1.xml.
                    """

                    print(
                        "### ----------------------------------------------------------------------------"
                    )
                    print("### We are still in major iteration %d" % iterationCount)
                    print(
                        "### ----------------------------------------------------------------------------"
                    )
                    print("### now running program that does the following:")
                    print("###")
                    print(
                        "### 1. finds individual misalignments of all Primary and Secondary mirror"
                    )
                    print("###    segments")
                    print("###")
                    print(
                        "### 2. updates respective conditions in a temporary copy of RICH"
                        + RichDetectorStr
                        + " Mirrors.xml"
                    )
                    print("###")
                    print(
                        "### 3. if in result of this major iteration, absolute values of all calculated"
                    )
                    print(
                        "###    improvements to the current misalignment compensations are < 0.1 mrad,"
                    )
                    print(
                        "###    the program decides that the mirrors are aligned, but of course applies"
                    )
                    print(
                        "###    these final improvements although they are somewhat redundant"
                    )
                    print(
                        "### ----------------------------------------------------------------------------"
                    )
                    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                    print("-------------------")

                    p = Popen(
                        "export   HOME="
                        + home
                        + ";"
                        + "source "
                        + pathToScripts
                        + "LbLogin.sh;"
                        + "export User_release_area="
                        + userReleaseArea
                        + ";"
                        + "source `which SetupProject.sh` Panoptes "
                        + setupProjectPanoptesVersion
                        + " "
                        + setupProjectPanoptesOptions
                        + ";"
                        + "$RICHMIRRALIGNROOT/$CMTCONFIG/RichMirrAlign.exe "
                        + richMirrAlignConfFile
                        + " > "
                        + richMirrAlignOutFile,
                        shell=True,
                        executable="/bin/bash",
                        stdout=PIPE,
                        stderr=PIPE,
                    )
                    (stdout, stderr) = p.communicate()
                    stdReport(stdout, stderr)

                    # ============================================================================
                    if backEnd == "dirac" or backEnd == "online":
                        if os.path.isfile(
                            "Rich"
                            + RichDetectorStr
                            + "CondDBUpdate_"
                            + thisNameStr
                            + "_i"
                            + str(iterationCount + 1)
                            + ".xml"
                        ):
                            just_prettify(
                                "Rich"
                                + RichDetectorStr
                                + "CondDBUpdate_"
                                + thisNameStr
                                + "_i"
                                + str(iterationCount + 1)
                                + ".xml"
                            )
                    # ======================== END OF PER-MIRROR CORRECTIONS =====================
                    """
                    break # <-- this can be used to run only one major iteration
                    """

                    # Check for nan values, NanOutFile should have 0 lines in it
                    os.system(
                        "cat "
                        + richMirrAlignOutFile
                        + ' | grep " nan " > '
                        + richMirrAlignNanOutFile
                    )
                    os.system(
                        "cat "
                        + richMirrAlignOutFile
                        + ' | grep " -nan " > '
                        + richMirrAlignNanOutFile
                    )
                    os.system(
                        "cat "
                        + richMirrAlignOutFile
                        + ' | grep " inf " > '
                        + richMirrAlignNanOutFile
                    )
                    os.system(
                        "cat "
                        + richMirrAlignOutFile
                        + ' | grep " -inf " > '
                        + richMirrAlignNanOutFile
                    )
                    # Check that you didn't accidentally run RichMirrCombinFit before merging histos
                    fitStdErrFileFirst = (
                        combinFitDir
                        + "Rich"
                        + RichDetectorStr
                        + "MirrCombinFitStdErr_"
                        + theseNamesWithTilts[""]
                        + "_i"
                        + str(iterationCount)
                        + ".txt"
                    )
                    os.system(
                        "cat "
                        + fitStdErrFileFirst
                        + ' | grep "root does not exist" > '
                        + richMirrAlignNanOutFile
                    )
                    num_lines = sum(1 for line in open(richMirrAlignNanOutFile))
                    # print num_lines
                    if num_lines != 0:
                        print(
                            "### --------------------------------------------------------------------------------------------"
                        )
                        print(
                            "### No. "
                            + str(iterationCount)
                            + " major iteration's verdict is:  STOP_NAN!"
                        )
                        print(
                            "### The mirror alignment has stopped, likely because there were not enough events for alignment."
                        )
                        print(
                            "### --------------------------------------------------------------------------------------------"
                        )
                        iterations = " major iterations !"
                        if iterationCount + 1 == 1:
                            iterations = " major iteration !"
                        print(
                            "### RICH"
                            + RichDetectorStr
                            + " mirror alignment is finished after "
                            + str(iterationCount + 1)
                            + iterations
                        )
                        print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                        print("-------------------")
                        os.system(
                            'echo "STOP_NAN" > '
                            + "Rich"
                            + RichDetectorStr
                            + "_stop_or_continue.txt"
                        )
                        # break

                    Rich_stop_or_continue_txt = open(
                        "Rich" + RichDetectorStr + "_stop_or_continue.txt"
                    )
                    verdict = Rich_stop_or_continue_txt.readline().strip().upper()
                    Rich_stop_or_continue_txt.close()

                    # The iteration is officially done, now safe to reset skipFirstRecoJob
                    skipFirstRecoJob = 0

                    print(
                        "### ------------------------------------------------------------------------------"
                    )
                    print(
                        "### No. "
                        + str(iterationCount)
                        + " major iteration's verdict is:  "
                        + verdict
                        + "!"
                    )
                    print(
                        "### ------------------------------------------------------------------------------"
                    )
                    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                    print("-------------------")
                    if verdict == "STOP":
                        iterations = " major iterations !"
                        if iterationCount + 1 == 1:
                            iterations = " major iteration !"
                        print(
                            "### RICH"
                            + RichDetectorStr
                            + " mirror alignment is finished after "
                            + str(iterationCount + 1)
                            + iterations
                        )
                        # softlink the final .xml file to be inside the directory structure needed to be converted into a DB slice
                        os.system(
                            "ln -sf "
                            + workDir
                            + "Rich"
                            + RichDetectorStr
                            + "CondDBUpdate_"
                            + thisNameStr
                            + "_i"
                            + str(iterationCount + 1)
                            + ".xml  "
                            + workDir
                            + "Mirrors_rich"
                            + RichDetectorStr
                            + "_"
                            + thisNameStr
                            + "/Conditions/Rich"
                            + RichDetectorStr
                            + "/Alignment/Mirrors.xml"
                        )
                        # PN - Apparently the current .db file has outlived its usefulness, if it is there, then remove it.
                        #    - A .db file will, of course, not be there for ONLINE
                        if os.path.exists(
                            workDir
                            + "Mirrors_rich"
                            + RichDetectorStr
                            + "_"
                            + thisNameStr
                            + ".db"
                        ):
                            os.system(
                                "rm -rf "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + thisNameStr
                                + ".db"
                            )
                        # create a final conditions database slice
                        # containing only the final Mirrors.xml branch
                        # don't need to do this for ONLINE
                        if backEnd != "online":
                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            print(
                                "### converting the whole final Mirrors.xml branch into SQLite format:"
                            )
                            print(
                                "### Note that this replaces "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + thisNameStr
                                + ".db with the final db slice."
                            )
                            print(
                                "### ----------------------------------------------------------------------------"
                            )
                            # prepare CondDB sqlite_file, which will be additional layer to the main database
                            print(
                                "copy_files_to_db.py  -s "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + thisNameStr
                                + "  -c sqlite_file:Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + thisNameStr
                                + ".db/"
                                + CONDDB
                            )

                            p = Popen(
                                "export   HOME="
                                + home
                                + ";"
                                + "source "
                                + pathToScripts
                                + "LbLogin.sh;"
                                + "source  `which SetupProject.sh` LHCb "
                                + LHCbVersion
                                + " ;"
                                + "copy_files_to_db.py  -s "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + thisNameStr
                                + "  -c sqlite_file:Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + thisNameStr
                                + ".db/"
                                + CONDDB
                                + ";"
                                + "python  "
                                + PanoptesJobPath
                                + "/tagger.py  "
                                + CondDB_tag
                                + " "
                                + workDir
                                + "Mirrors_rich"
                                + RichDetectorStr
                                + "_"
                                + thisNameStr
                                + " "
                                + CONDDB,
                                shell=True,
                                executable="/bin/bash",
                                stdout=PIPE,
                                stderr=PIPE,
                            )
                            (stdout, stderr) = p.communicate()
                            stdReport(stdout, stderr)
                        # break

                    if (
                        verdict != "STOP"
                        and verdict != "STOP_NAN"
                        and verdict != "STOP_NGE"
                    ):  # STOP_NGE not implemented yet
                        print(
                            "### updating SQLite copy of the whole Mirrors.xml branch:"
                        )
                        print(
                            "### ----------------------------------------------------------------------------"
                        )
                        (
                            """
                        so by now we have used up the current copy of
                        Mirrors_rich"""
                            + RichDetectorStr
                            + """_thisNameStr_iterationCount.xml that was utilized for the
                        reconstruction in this major iteration, and thus we increment the iterationCount
                        and will now be using in the next major iteration the freshly produced by
                        RichMirrAlign for us Mirrors_rich2_thisNameStr_iterationCount+1.xml
                        """
                        )
                        iterationCount += 1
                        currentMirrorXMLFile = (
                            "Rich"
                            + RichDetectorStr
                            + "CondDBUpdate_"
                            + thisNameStr
                            + "_i"
                            + str(iterationCount)
                            + ".xml"
                        )
                        nextIterationXMLFile = (
                            "Rich"
                            + RichDetectorStr
                            + "CondDBUpdate_"
                            + thisNameStr
                            + "_i"
                            + str(iterationCount + 1)
                            + ".xml"
                        )

                    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                    print("-------------------")

                if backEnd == "online":
                    print("We need to run the Analyzer every minor iteration")
                    state = (
                        State.RUNNING
                    )  # note, we have to actually set this state later
                    if (
                        verdict == "STOP"
                        or verdict == "STOP_NAN"
                        or verdict == "STOP_NGE"
                    ):  # STOP_NGE not implemented yet
                        state = State.READY
                else:
                    if (
                        verdict == "STOP"
                        or verdict == "STOP_NAN"
                        or verdict == "STOP_NGE"
                    ):  # STOP_NGE not implemented yet
                        break

            n_it += 1  # increase minor iteration number

        if backEnd == "online":
            # Set the status
            com.set_status(state)

    # ===============================================================================
    # end of loop over major iterations
    # ===============================================================================
    # the loop ended because either converged or reached the
    # maximum-number-of-major-iterations limit, so:
    if verdict == "CONTINUE":
        print(
            "### ----------------------------------------------------------------------------"
        )
        print(
            "### maximum-number-of-major-iterations limit ("
            + str(maximumNumberOfIterations)
            + ") is reached, and the whole program"
        )
        print(
            "### has to stop, but notice: RICH"
            + RichDetectorStr
            + " mirror alignment has NOT reached the"
        )
        print("### desired accuracy yet.")
    print(
        "### ----------------------------------------------------------------------------"
    )

    print("The whole thing is done")
    print("You are here in the working directory:")
    print(os.getcwd())
    print("Now I have taken you back to your starting directory:")
    os.chdir(wasHere)
    print(os.getcwd())

    if backEnd == "online":
        print("ONLINE")
        # Set our status one last time
        com.set_status(state)
        print("com.set_status(state) one last time")


# =================================================================================
# =================================================================================
# This is the main program here. When this script is executed, it should run this:

print(
    "Hi! All of the functions have now been defined. Time to run the RICH mirror alignment."
)
print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
print("-------------------")

if __name__ == "__main__":
    print(
        "This python file is being run directly, not being imported into another module."
    )
    print("mirroralign() is being called now, to perform the Offline mirror alignment.")
    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    print("-------------------")
    if exists("RichInfoName"):
        mirroralign(RichInfoName)
    else:
        mirroralign()
    print("mirroralign() has been run.")
else:
    print(
        "This python file is being imported into another module, possibly the Online python Iterator."
    )
    print("mirroralign() will not be not run yet.")

print("Now exiting the mirror alignment driver python script.")
print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
print("-------------------")

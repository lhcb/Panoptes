###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os
import re

parser = argparse.ArgumentParser(
    description='Compares two XML files or a single diff between XML files. Usage is "python XMLcompare.py <before.xml> <after.xml>" OR "python XMLcompare.py <diff_between_two_xmls.txt>". Designed only to compare Y and Z tilts for all alignment parameters in the XML file. Does not tell you the name of each parameter, only draws attention to those that differ by more than 0.1 mrad. Tilts are ROUNDED not truncated.'
)
parser.add_argument(
    "files",
    metavar="FILE",
    type=str,
    nargs="+",
    help="the file with the diff OR the two files you want to diff",
)
parser.add_argument(
    "-w",
    action="store",
    default=0.1,
    dest="warningPrecision",
    type=float,
    help="the precision of differences at which warnings are provided, default is 0.1",
)
parser.add_argument(
    "-p",
    action="store",
    default=2,
    dest="displayPrecision",
    type=int,
    help="the number of digits after the decimal point printed for output numbers, default is 2",
)
args = parser.parse_args()

warningPrecision = args.warningPrecision
displayPrecision = str(args.displayPrecision)

# Actually, the parser will tell you that you didn't provide enough arguments, so we don't need this next bit
# if (len(args.files)==0):
#     print "I need files."
#     raise SystemExit("I need files.")
if len(args.files) == 1:
    file = args.files[0]
    print("pre-diffed input file: " + file)
if len(args.files) == 2:
    file = args.files[0]
    file2 = args.files[1]
    tempfile = "temp_XMLcompare.txt"
    print("first XML file to diff: " + file)
    print("second XML file to diff: " + file2)
    os.system("diff " + file + " " + file2 + " > " + tempfile)
    file = tempfile

# what we're gonna do, is search through it line-by-line
# and parse out the numbers, using regular expressions

# what this basically does is, look for any number of characters
# that aren't digits or '-' [^-\d]  ^ means NOT
# then look for 0 or 1 dashes ('-') followed by one or more decimals
# and a dot and decimals again: [\-]{0,1}\d+\.\d+
# and then the same as first..
with open(file, "r") as myfile:
    data2 = myfile.read()

data = re.sub(r"([a-z]+)", r"\n \1", data2)

# print data

# use http://www.myezapp.com/apps/dev/regexp/show.ws to translate
pattern = re.compile(r"[^-\d]*([\-]{0,1}\d+\.\d+)[^-\d]*")

results = []
for line in data.split("\n"):
    match = pattern.match(line)
    if match:
        results.append(match.groups()[0])
# summary: looks for things like 0.3333 or -2.2344 and only keeps those

# print results

sets = []
i = 0
end = len(results)
while i < end - 1:
    sets.append(
        (
            results[i + 0],
            results[i + 1],
            results[i + 2],
            results[i + 3],
            results[i + 4],
            results[i + 5],
        )
    )
    i += 6

print(
    " Y and Z tilt differences for all parameters. Note that numbers are ROUNDED not truncated. "
)
for t in sets:
    #    print t
    a = float(t[4]) - float(t[1])  # Y
    b = float(t[5]) - float(t[2])  # Z
    str1 = "                                           "
    str2 = "                                           "
    for i in (0, int(displayPrecision)):
        str1 += " "
        str2 += " "
    if abs(a) > warningPrecision:
        str1str = (
            ", warning: this Y tilt difference is %7." + displayPrecision + "f mrad"
        )
        str1 = str1str % a
    if abs(b) > warningPrecision:
        str2str = (
            ", warning: this Z tilt difference is %7." + displayPrecision + "f mrad"
        )
        str2 = str2str % b
    printString = (
        " %7." + displayPrecision + "f , %s, %7." + displayPrecision + "f , %s "
    )
    print(printString % (a, str1, b, str2))

if len(args.files) == 2:
    os.system("rm -rf " + tempfile)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This script has to be run in Ganga

import os
import sys


# Obtains an integer from user
def GetLastRun():
    Lastrun = 0
    lastrun = input("Enter last run to use, zero for default (9999999): ")

    if lastrun == "":
        Lastrun = 0
    else:
        Lastrun = int(lastrun)
    if Lastrun == 0:
        Lastrun = 999999999
    return Lastrun


# Obtains an integer from user
def GetFirstRun():
    Firstrun = 0
    firstrun = input("Enter first run to use, zero for default (0): ")

    if firstrun == "":
        Firstrun = 0
    else:
        Firstrun = int(firstrun)
    return Firstrun


# Writes the final file
def WriteFile(ouputFile, files, firstRun, Lastrun, scale):
    f = open(outputFile, "w")
    i = 0

    lines = []
    lines.append("from Gaudi.Configuration import * \n")
    lines.append("from GaudiConf import IOHelper\n")
    lines.append("IOHelper().inputFiles([\n")
    for file in files:
        i = i + 1
        if not (i % scale):  # Take only every 'scale'th file
            filename = file.name
            k = filename.split("/")
            run = int(k[8])  # Get RunNumber
            if (run < Lastrun) and (run > firstRun):  # Upper limit on RunNumber
                lines.append("            'LFN:" + filename + "',\n")
    test = lines[-1].replace(",\n", "")
    del lines[-1]
    lines.append(test)

    lines.append("], clear=True)\n")

    for line in lines:
        f.write(line)

    print("File", outputFile, "written")
    f.close()


# Main part of this script

gridProxy.renew()

Data = str(input("Enter year to align, 2011 or 2012?: "))
if Data != "2011" and Data != "2012":
    print(Data, "You must enter either 2011 or 2012")
    sys.exit()

Mag = input("Enter polarity to align, MagUp or MagDown?: ")
if Mag != "MagUp" and Mag != "MagDown":
    print("You must enter either MagUp or MagDown")
    sys.exit()

DataPath = ""
if Data == "2011":
    DataPath = (
        "/LHCb/Collision11/Beam3500GeV-VeloClosed-" + Mag + "/Real Data/91000000/RAW"
    )
if Data == "2012":
    DataPath = (
        "/LHCb/Collision12/Beam4000GeV-VeloClosed-" + Mag + "/Real Data/91000000/RAW"
    )

FirstRun = GetFirstRun()

Lastrun = GetLastRun()

outputFile = input("OutputFile Name: ")
if outputFile == "":
    outputFile = "LFN-Test.py"

# We will only write every 'scale'th file
# If scale is 1 then we will write every file
scale = int(
    input("Enter Scale Factor (must enter an integer greater than or equal to 1): ")
)

bkq = BKQuery(path=DataPath, type="Path", dqflag="All")
# 2011            path = "/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/91000000/RAW"
# FULL               path = "/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/90000000/RAW",
# EXPRESS               path = "/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/91000000/RAW",

print("Be patient, I'm getting the complete list of data files.")
ds = bkq.getDataset()
print("BKQuery().getDataset().files()")
files = ds.files

print("Got the files. Now writing the data LFNs to " + outputFile)
WriteFile(outputFile, files, FirstRun, Lastrun, scale)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Brunel.Configuration import *

###############################################################################
from GaudiKernel.ProcessJobOptions import importOptions

# importOptions ("$APPCONFIGOPTS/Brunel/earlyData.py")

importOptions("$APPCONFIGOPTS/Brunel/DataType-2012.py")
# importOptions("$APPCONFIGOPTS/UseOracle.py")
# importOptions("$APPCONFIGOPTS/DisableLFC.py")
###############################################################################
from Configurables import MessageSvc

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
# MessageSvc().OutputLevel = 7

from Configurables import Brunel

Brunel().EvtMax = 1000
Brunel().PrintFreq = 10
Brunel().OutputType = "None"

# --Again the Database problem...
# from Configurables import CondDB, LHCbApp
# CondDB(UseOracle = True)
# LHCbApp().DDDBtag ="head-20110302"
# LHCbApp().CondDBtag="head-20110318"
# reconstruct RICH only
# Brunel ().InitSequence      = ["Brunel"]
Brunel().RecoSequence = ["Decoding", "VELO", "TT", "IT", "OT", "Tr", "Vertex", "RICH"]

# Brunel ().RecoSequence      = ["RICH"]
# Brunel().MCCheckSequence   = ["RICH"]          # only when WithTruth
Brunel().Histograms = "Expert"  # propagates down to Alignment
# Brunel ().Histograms        =  "None"
Brunel().DataType = "2012"

# DST
Brunel().WithMC = False  # with MC truth automatically invokes Simulation
Brunel().InputType = "DST"
Brunel().Persistency = "ROOT"
# DST so reconstruct RICH only (note this changes the Recosequence that was defined before in this file, that's OK!]
Brunel().InitSequence = ["Brunel"]
Brunel().RecoSequence = ["RICH"]

from Configurables import RecMoniConf

# RecMoniConf is defined in REC/REC_vXrY/RecSys/python/RecSys/Configuration.py
RecMoniConf().MoniSequence = ["RICH"]
##############################################################################
from Configurables import RichRecSysConf

rConf = RichRecSysConf("RichOfflineRec")
# disable PID
rConf.setProp("PidConfig", "None")

# Minimum track monentum
minP = 20  # in GeV

# DST - PN - Not sure what effect this will have for DST, if any... but leaving it in
# Tweak the tracking to only run the Forward tracking, and to apply
# the min track momentum cut during the track finding.
from Configurables import PatForward, PatForwardTool, TrackSys

TrackSys().setProp("TrackPatRecAlgorithms", ["FastVelo", "Forward"])
PatForward("PatForward").addTool(PatForwardTool)
PatForward("PatForward").PatForwardTool.MinMomentum = minP * 1000  # in MeV

# 2015-04-28 For future reference, these next lines can make *cuts* on Eta and Phi ranges
# We are *not using these now* but may do so in the future, if we choose to emulate
# our HLT line (which would require a two-stage Brunel)
#
# from Configurables import TrackSelector
# PatForward("PatForward").PatForwardTool.TrackSelectorName = "TrackSelector"
# PatForward("PatForward").PatForwardTool.addTool(TrackSelector)
# PatForward("PatForward").PatForwardTool.TrackSelector.MinEtaCut = 2.59
# PatForward("PatForward").PatForwardTool.TrackSelector.MaxEtaCut = 2.97
# PatForward("PatForward").PatForwardTool.TrackSelector.MinPhiCut = -2.69
# PatForward("PatForward").PatForwardTool.TrackSelector.MaxPhiCut = 2.69

# Date: 2010-04-15 09:47              By: Christopher R Jones <jonrob>
# note that as long as you are using the "earlyData" switch (which you
# should be for the moment) the Loose photon selection is the default
# in Brunel
rConf.photonConfig().SelectionMode = "Loose"
# gas radiators only
rConf.Radiators = ["Rich1Gas"]
# only Long tracks with P > 10GeV
rConf.trackConfig().TrackCuts = {
    "Forward": {"Chi2Cut": [0, 5], "PCut": [minP, 9999999]},
    "Match": {"Chi2Cut": [0, 5], "PCut": [minP, 9999999]},
}
# Turn off trackless ring finding
rConf.TracklessRingAlgs = []

# DST: Need to refit the tracks prior to the RICH Reco
rConf.RefitTracks = True

from Configurables import RichRecQCConf

# disable monitors that are enabled by default in "OfflineFull" mode,
# except "AlignmentMonitoring"
RichRecQCConf("OfflineRichMoni").removeMonitor("L1SizeMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("DBConsistencyCheck")
RichRecQCConf("OfflineRichMoni").removeMonitor("HotPixelFinder")
RichRecQCConf("OfflineRichMoni").removeMonitor("PidMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("PixelMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("TrackMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("PhotonMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("TracklessRingAngles")
RichRecQCConf("OfflineRichMoni").removeMonitor("TracklessRingPeakSearch")
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDImageShifts")
RichRecQCConf("OfflineRichMoni").removeMonitor("DataDecodingErrors")
# RichRecQCConf ("OfflineRichMoni").removeMonitor("AlignmentMonitoring"    )
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDIFBMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPixelPositions")
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDHitPlots")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackGeometry")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichGhostTracks")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichCKThetaResolution")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackResolution")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonSignal")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackCKResolutions")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonGeometry")
RichRecQCConf("OfflineRichMoni").removeMonitor("PhotonRecoEfficiency")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonTrajectory")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichStereoFitterTests")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichRayTracingTests")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichDataObjectChecks")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichRecoTiming")

# disable producing NTuple (PN- this was on in the older DST options, but I think we don't need it)
# RichRecQCConf ("OfflineRichMoni").setProp("NTupleProduce", False )

from Configurables import RichAlignmentConf

# choose RICH you want
# RichAlignmentConf("OfflineRichMoni_RichAlignmentConf").setProp("Radiators", ["Rich1Gas"])

RichAlign = RichAlignmentConf("OfflineRichMoni_RichAlignmentConf")
RichAlign.Radiators = ["Rich1Gas"]
RichAlign.HPDList = [[0], [0], [0]]
RichAlign.Histograms = "Expert"
# RichAlign.VetoedHPDs =  [100309]

RichAlign.NTupleProduce = False
RichAlign.R1NTupleProduce = False
# activate long list of segment combination histos to be prebooked for RICH2
# this may propagate from the Brunel Cofiguration, though
# RichAlignmentConf("OfflineRichMoni_RichAlignmentConf").setProp("Histograms", "OfflineExpress")
##############################################################################
# DST
from Configurables import DstConf

# enable DST unpacking: needed to get tracks
ApplicationMgr().ExtSvc += ["DataOnDemandSvc"]
DstConf().EnableUnpack = True
##############################################################################
# db tags
# LHCbApp().DDDBtag   = "head-20100407"
# LHCbApp().CondDBtag = "head-20100408"
##############################################################################
# DST - PN - these were commented out in the DST options. I don't see the harm in leaving them in
from Configurables import (
    ApplicationMgr,
    CondDB,
    CondDBAccessSvc,
    HistogramPersistencySvc,
    LHCbApp,
)

# OutputStream("DstWriter").Output = "DATAFILE='Alignment.dst' TYP='POOL_ROOTTREE' OPT='REC'"
NTupleSvc().Output = [
    "RICHTUPLE1 DATAFILE='Ntuple_2012-Collisions.root' TYP='ROOT' OPT='NEW'"
]

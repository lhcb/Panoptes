###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Brunel.Configuration import *

###############################################################################
from GaudiKernel.ProcessJobOptions import importOptions

# importOptions ("$APPCONFIGOPTS/Brunel/earlyData.py") # comment this to make it work!
importOptions("$APPCONFIGOPTS/Brunel/DataType-2010.py")
# importOptions("$APPCONFIGOPTS/UseOracle.py")
# importOptions("$APPCONFIGOPTS/DisableLFC.py")
###############################################################################
from Configurables import MessageSvc

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
# MessageSvc().OutputLevel = 7

from Configurables import Brunel

Brunel().EvtMax = 10
Brunel().PrintFreq = 1
Brunel().OutputType = "None"
# reconstruct RICH only
Brunel().InitSequence = ["Brunel"]
Brunel().RecoSequence = ["RICH"]
# Brunel().MCCheckSequence   = ["RICH"]          # only when WithTruth
Brunel().Histograms = "OfflineExpress"  # propagates down to Alignment

from Configurables import RecMoniConf

# RecMoniConf is defined in REC/REC_vXrY/RecSys/python/RecSys/Configuration.py
RecMoniConf().MoniSequence = ["RICH"]
##############################################################################
from Configurables import RichRecSysConf

rConf = RichRecSysConf("RichOfflineRec")
# disable PID
rConf.setProp("PidConfig", "None")
# loose cuts
# Date: 2010-04-15 09:47              By: Christopher R Jones <jonrob>
# note that as long as you are using the "earlyData" switch (which you
# should be for the moment) the Loose photon selection is the default
# in Brunel
rConf.photonConfig().SelectionMode = "Loose"
# gas radiators only
rConf.Radiators = ["Rich1Gas"]
# only Long tracks with P > 10GeV
rConf.trackConfig().TrackCuts = {
    "Forward": {"Chi2Cut": [0, 5], "PCut": [10.0, 9999999]},
    "Match": {"Chi2Cut": [0, 5], "PCut": [10.0, 9999999]},
}
# Turn off trackless ring finding
rConf.TracklessRingAlgs = []
# Need to refit the tracks prior to the RICH Reco
rConf.RefitTracks = True

from Configurables import RichRecQCConf

# disable monitors that are enabled by default in "OfflineFull" mode,
# except "AlignmentMonitoring"
RichRecQCConf("OfflineRichMoni").removeMonitor("L1SizeMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("DBConsistencyCheck")
RichRecQCConf("OfflineRichMoni").removeMonitor("HotPixelFinder")
RichRecQCConf("OfflineRichMoni").removeMonitor("PidMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("PixelMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("TrackMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("PhotonMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("TracklessRingAngles")
RichRecQCConf("OfflineRichMoni").removeMonitor("TracklessRingPeakSearch")
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDImageShifts")
RichRecQCConf("OfflineRichMoni").removeMonitor("DataDecodingErrors")
# RichRecQCConf("OfflineRichMoni").removeMonitor("AlignmentMonitoring"    )
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDIFBMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPixelPositions")
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDHitPlots")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackGeometry")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichGhostTracks")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichCKThetaResolution")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackResolution")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonSignal")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackCKResolutions")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonGeometry")
RichRecQCConf("OfflineRichMoni").removeMonitor("PhotonRecoEfficiency")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonTrajectory")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichStereoFitterTests")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichRayTracingTests")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichDataObjectChecks")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichRecoTiming")

# disable producing NTuple
RichRecQCConf("OfflineRichMoni").setProp("NTupleProduce", False)

from Configurables import RichAlignmentConf

# choose RICH you want
# RichAlignmentConf("OfflineRichMoni_RichAlignmentConf").setProp("Radiators", ["Rich1Gas"])
RichAlignmentConf("OfflineRichMoni_RichAlignmentConf").setProp(
    "Radiators", ["Rich1Gas"]
)

# activate long list of segment combination histos to be prebooked for RICH2
# this may propagate from the Brunel Cofiguration, though
# RichAlignmentConf("OfflineRichMoni_RichAlignmentConf").setProp("Histograms", "OfflineExpress")
##############################################################################
from Configurables import DstConf

# enable DST unpacking: needed to get tracks
ApplicationMgr().ExtSvc += ["DataOnDemandSvc"]
DstConf().EnableUnpack = ["Reconstruction"]
##############################################################################
# db tags
# LHCbApp().DDDBtag   = "head-20100407"
# LHCbApp().CondDBtag = "head-20100408"
##############################################################################
from Configurables import CondDB, CondDBAccessSvc, LHCbApp

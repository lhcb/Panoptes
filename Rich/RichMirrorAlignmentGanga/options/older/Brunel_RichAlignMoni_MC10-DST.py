###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Brunel.Configuration import *

###############################################################################
from GaudiKernel.ProcessJobOptions import importOptions

# importOptions("$APPCONFIGOPTS/Brunel/MC09-NoTruth.py")
# importOptions ("$APPCONFIGOPTS/Brunel/MC09-WithTruth.py")
# importOptions("$APPCONFIGOPTS/DisableLFC.py")
importOptions("$APPCONFIGOPTS/Brunel/MC09-WithTruth.py")

###############################################################################
from Configurables import MessageSvc

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
# MessageSvc().OutputLevel = 7

from Configurables import Brunel

# Brunel().DataType    = "MC09"
# Brunel().Simulation  =  True # taken care in MC09-NoTruth.py / MC09-WithTruth.py
Brunel().WithMC = True  # with MC truth automatically invokes Simulation
Brunel().InputType = "DST"
Brunel().EvtMax = 10
Brunel().PrintFreq = 1
Brunel().OutputType = "None"
# reconstruct RICH only
Brunel().InitSequence = ["Brunel"]
Brunel().RecoSequence = ["RICH"]
Brunel().MCCheckSequence = ["RICH"]  # only when WithTruth
Brunel().Histograms = "OfflineExpress"  # propagates down to Alignment

from Configurables import RecMoniConf

# RecMoniConf is defined in REC/REC_vXrY/RecSys/python/RecSys/Configuration.py
RecMoniConf().MoniSequence = ["RICH"]
##############################################################################
# IODataManager().AgeLimit = 3;
##############################################################################
from Configurables import RichRecSysConf

rConf = RichRecSysConf("RichOfflineRec")
# disable PID
rConf.setProp("PidConfig", "None")
# loose cuts
rConf.photonConfig().SelectionMode = "Wide"
# gas radiators only
rConf.Radiators = ["Rich1Gas", "Rich2Gas"]
# only Long tracks with P > 10GeV
rConf.trackConfig().TrackCuts = {
    "Forward": {"Chi2Cut": [0, 5], "PCut": [10.0, 9999999]},
    "Match": {"Chi2Cut": [0, 5], "PCut": [10.0, 9999999]},
}
# Turn off trackless ring finding
rConf.TracklessRingAlgs = []

from Configurables import RichRecQCConf

# disable monitors that are enabled by default in "OfflineFull" mode
# except "AlignmentMonitoring"
RichRecQCConf("OfflineRichMoni").removeMonitor("L1SizeMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("DBConsistencyCheck")
RichRecQCConf("OfflineRichMoni").removeMonitor("HotPixelFinder")
RichRecQCConf("OfflineRichMoni").removeMonitor("PidMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("PixelMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("TrackMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("PhotonMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("TracklessRingAngles")
RichRecQCConf("OfflineRichMoni").removeMonitor("TracklessRingPeakSearch")
# RichRecQCConf("OfflineRichMoni").removeMonitor("AlignmentMonitoring"    )
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDIFBMonitoring")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPixelPositions")
RichRecQCConf("OfflineRichMoni").removeMonitor("HPDHitPlots")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackGeometry")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichGhostTracks")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichCKThetaResolution")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackResolution")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonSignal")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichTrackCKResolutions")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonGeometry")
RichRecQCConf("OfflineRichMoni").removeMonitor("PhotonRecoEfficiency")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichPhotonTrajectory")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichStereoFitterTests")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichRayTracingTests")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichDataObjectChecks")
RichRecQCConf("OfflineRichMoni").removeMonitor("RichRecoTiming")

# disable producing NTuple
RichRecQCConf("OfflineRichMoni").setProp("NTupleProduce", False)

from Configurables import RichAlignmentConf

# choose RICH you want
# RichAlignmentConf("OfflineRichMoni_RichAlignmentConf").setProp("Radiators", ["Rich1Gas"])
RichAlignmentConf("OfflineRichMoni_RichAlignmentConf").setProp(
    "Radiators", ["Rich1Gas", "Rich2Gas"]
)
#
# from Configurables import ( Rich__Rec__MC__AlignmentMonitor )
# RichAlignMoniR1 = Rich__Rec__MC__AlignmentMonitor("RichAlignMoniR1Gas")
# trselname = "TrackSelector"
# RichAlignMoniR1.addTool( RichAlignmentConf ("OfflineRichMoni_RichAlignmentConf").richTools().trackSelector(trselname), name=trselname )
# RichRecQCConf ("OfflineRichMoni").createMonitor(Rich__Rec__MC__AlignmentMonitor,"RichAlignMoniR1Gas",trackType=None,tkCuts="Tight" )
#
# RichAlignMoniR2 = Rich__Rec__MC__AlignmentMonitor("RichAlignMoniR2Gas")
# trselname = "TrackSelector"
# RichAlignMoniR2.addTool( RichAlignmentConf ("OfflineRichMoni_RichAlignmentConf").richTools().trackSelector(trselname), name=trselname )
# RichRecQCConf ("OfflineRichMoni").createMonitor(Rich__Rec__MC__AlignmentMonitor,"RichAlignMoniR2Gas",trackType=None,tkCuts="Tight" )
##RichRecQCConf ("OfflineRichMoni").createMonitor(RichAlignmentConf,"OfflineRichMoni_RichAlignmentConf",trackType=None,tkCuts="Tight" )

# activate long list of segment combination histos to be prebooked for RICH2
# but this propagates from the Brunel Cofiguration
# RichAlignmentConf().setProp("Histograms", "OfflineExpress")

# activate filling histograms that utilize knowledge from MC Truth
# but this propagates from the Brunel Cofiguration
# RichAlignmentConf().setProp("WithMC", True )
##############################################################################
from Configurables import DstConf

# enable DST unpacking: needed to get tracks
ApplicationMgr().ExtSvc += ["DataOnDemandSvc"]
DstConf().EnableUnpack = ["Reconstruction"]
##############################################################################
# db tags
# LHCbApp().DDDBtag   = "MC09-20090602"
# LHCbApp().CondDBtag = "MC09-20090402-vc-md100"
##############################################################################
from Configurables import CondDB, CondDBAccessSvc, LHCbApp

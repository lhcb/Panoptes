###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import glob
import os

from Moore import options

# Get test name
myName = os.getenv("QMTTEST_NAME")

# Start with config from TestFileDB
options.set_input_and_conds_from_testfiledb("rich-decode-2024-panoptes")
# If in Cambridge, use local PFNs
jonPath = "/usera/jonesc/NFS/data/RunIII/Hlt2/LHCb/RefIndexCalib/2024/"
if not myName and os.path.exists(jonPath):
    options.input_files = sorted(glob.glob(jonPath + "/data*.mdf"))
print("Data :-", options.input_files)

options.evt_max = 1000
options.print_freq = 100

# If required force use of trunk geom
if myName and "trunk_geom" in myName:
    options.geometry_version = "run3/trunk"

# Due to issues with hit sorting in FT disable Mat corrections
# See for more details https://gitlab.cern.ch/lhcb/Panoptes/-/issues/7
from PyConf.Algorithms import PrStoreSciFiHits

PrStoreSciFiHits.global_bind(ApplyMatContractionCalibration=False)
# To be removed once the issue is resolved properly.

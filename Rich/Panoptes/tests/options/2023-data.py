###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from Moore import options

# Get test name
myName = os.getenv("QMTTEST_NAME")

options.set_input_and_conds_from_testfiledb("rich-decode-2023")
options.conditions_version = "online"

# If in Cambridge, use local PFNs
jonPath = "/usera/jonesc/NFS/data/RunIII/Hlt2/LHCb/RefIndexCalib/2023/"
if not myName and os.path.exists(jonPath):
    options.input_files = [
        jonPath + "data-{i:04d}.mdf".format(i=i) for i in range(10, 107)
    ]
print("Data :-", options.input_files)

options.evt_max = 1000
options.print_freq = 100

# If required force use of trunk geom
if myName and "trunk_geom" in myName:
    options.geometry_version = "run3/trunk"

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Moore import options
from Moore.qmtest.context import download_digi_inputs_locally

jonPath = "/usera/jonesc/NFS/data/MC/Run3/Download/"

if options.simulation and os.path.exists(jonPath):
    # AVERAGE_EVENT_SIZE = 500 * 1000  # upper limit of average event size
    options.input_files = download_digi_inputs_locally(
        options.input_files, dest_dir=jonPath
    )
    # max_size=options.evt_max * AVERAGE_EVENT_SIZE)

    # avoid later options file changing this property and running more events
    # than were downloaded which usually results in weird errors when processing
    # the last event which was only partially downloaded.
    # options.lockOption('evt_max')
    # options.lockOption('input_files')

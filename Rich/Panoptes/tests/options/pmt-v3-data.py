###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from Configurables import DDDBConf, LHCbApp
from DDDB.CheckDD4Hep import UseDD4Hep
from Moore import options
from PRConfig.TestFileDB import test_file_db

options.input_files = test_file_db["rich-decode-detdesc-compat-old-rich1"].filenames
options.input_type = "ROOT"

if UseDD4Hep:
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    tag = "jonrob/all-pmts-active"
    options.conditions_version = tag
    LHCbApp().CondDBtag = tag  # Work around PyConf bug
    # Use a geometry from before changes to RICH1
    # https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/205
    tag = "run3/before-rich1-geom-update-26052022"
    DDDBConf().GeometryVersion = tag
    options.geometry_version = tag
else:
    options.conddb_tag = "upgrade/sim-20210630-vc-md100"
    options.dddb_tag = "upgrade/dddb-20220111"

options.input_raw_format = 4.3

options.simulation = True

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"

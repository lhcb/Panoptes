###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from Moore import options

# Get test name
myName = os.getenv("QMTTEST_NAME")

options.set_input_and_conds_from_testfiledb("rich-decode-2024-mc")
jonPath = "/usera/jonesc/NFS/data/MC/Run3/30000000/dddb-20231017/sim-20231017-vc-md100/2024-Expected-lumi_2.0e33/DIGI/"
if not myName and os.path.exists(jonPath):
    options.input_files = [
        jonPath + "30000000_42604869_{i:08d}.digi".format(i=i) for i in range(0, 80)
    ]
print("Data :-", options.input_files)

# If required force use of trunk geom
if myName and "trunk_geom" in myName:
    options.geometry_version = "run3/trunk"
else:
    options.geometry_version = "run3/2024.Q1.2-v00.00"

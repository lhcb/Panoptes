###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import LineSkipper
from Moore.qmtest.exclusions import RecPreprocessor, skip_scheduler
from Moore.qmtest.exclusions import ref_preprocessor as moore_ref_preprocessor
from Moore.qmtest.exclusions import remove_known_warnings as moore_remove_known_warnings

remove_known_warnings = moore_remove_known_warnings + LineSkipper(
    regexps=[
        # Known warnings from calibration writer when not running in ONLINE env.
        r".* +WARNING Cannot write to .*",
        r".* +WARNING DIM_DNS_NODE not defined. Disabling DIM publishing",
        r".* +WARNING Environment variable PARTITION is not set",
        # Warnings when camera server not available (not ONLINE)
        r".*CameraTool +WARNING Could not connect to any camera server.*",
        r".*CameraTool +WARNING Above message repeated .*",
        # dd4hep specific warnings
        r".*WARNING TransportSvc is currently incompatible with DD4HEP. Disabling its use and thus any material corrections.",
        r".*WARNING See https://gitlab.cern.ch/lhcb/Rec/-/issues/326 for more details",
        r".*WARNING Source ID .* is missing in LHCb Tel40 Link condition",
        r".*WARNING .* writing and publishing is DISABLED",
        r".*WARNING Failed to write YML file for .*",
        r".*WARNING Run number override enabled, forced to .*",
        # Reported as an ERROR but really should be no more than a WARNING ...
        r"FTRawBankDecoder .*ERROR .*Possibly corrupt data.*",
    ]
)

skip_calib_messages = LineSkipper(
    regexps=[
        # Skip lines with variable information (times, file paths etc.)
        r".* +INFO Previous #Events = .*",
        r".* +INFO Watermark = .*",
        r".* +INFO Created .*",
        r".* +INFO Removing .*",
        r".* +INFO Setting .* conditions version .*",
        r".* +INFO Successfully wrote .* Scale factor to .*",
        r".* +INFO Seen .* events in past .*",
    ]
)

# Exclude anything before finalize
ref_preprocessor = remove_known_warnings + moore_ref_preprocessor + skip_calib_messages
# include all messages
# ref_preprocessor = remove_known_warnings + skip_calib_messages + RecPreprocessor + skip_scheduler

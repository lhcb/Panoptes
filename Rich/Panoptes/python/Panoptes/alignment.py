###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import getpass
import os

from PyConf.Algorithms import (
    Rich__Future__Rec__Moni__SIMDAlignment as MirrorAlign,
)
from PyConf.Algorithms import (
    Rich__Future__Rec__Moni__SIMDPhotonCherenkovAngles as PanelAlignAndMonitoring,
)
from PyConf.Algorithms import (
    TracksToSelection,
)
from PyConf.application import (
    make_odin,
)
from PyConf.tonic import (
    configurable,
)
from PyConf.Tools import (
    TrackSelector,
)
from RecoConf.config import (
    Reconstruction,
)
from RecoConf.hlt2_tracking import (
    make_hlt2_tracks,
    make_hlt2_tracks_without_UT,
)
from RecoConf.rich_data_monitoring import (
    alignment_rich_monitoring_options,
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import (
    default_rich_reco_options,
    get_detector_bool_opts,
    make_rich_pids,
)
from RecoConf.standalone import (
    reco_prefilters,
)


# Configurable that produces `Alignment` histograms ONLY
# This is for the Mirror Alignment monitor's offline tests (e.g. CI tests)
@configurable
def standalone_rich_mirror_align_reco(
    RichGas="Rich1Gas",
    MirrorAlignTasks=["Produce"],
    EventFilter=[],
    noUT=False,
):
    """
    Reconstruction for RICH mirror alignment
    """

    # Make the tracks
    if not noUT:
        hlt2_tracks = make_hlt2_tracks(light_reco=True, fast_reco=False, use_pr_kf=True)
    else:
        hlt2_tracks = make_hlt2_tracks_without_UT()

    # The track type we use
    track_version = "v1"
    tkType = "Long"

    # Get the fitted long tracks to use
    tks = hlt2_tracks["Best" + tkType]

    # Default RICH reco options
    reco_opts = default_rich_reco_options()

    # Radiators
    reco_opts["RichGases"] = [RichGas]

    # Enable online mode for wider CK theta side bands
    reco_opts["PhotonSelection"] = "Online"

    # Enable extra photon info
    reco_opts["SaveMirrorData"] = True

    # As we aren't running PID, we do not need all hypos
    # reco_opts["Particles"] = ["muon","pion","kaon"]

    # Track selection
    tkSel = TracksToSelection(InputLocation=tks[track_version])

    # RICH photon reco
    conf = make_rich_pids(
        track_name=tkType, input_tracks=tkSel.OutputLocation, options=reco_opts
    )

    # The detector options
    det_opts = get_detector_bool_opts(reco_opts, tkType)

    # Data monitoring options
    moni_opts = default_rich_monitoring_options()

    # More monitoring options in case of the RICH alignment monitoring.
    moni_opts.update(alignment_rich_monitoring_options(radiator=RichGas))

    # mirror alignment histograms
    alignMon = MirrorAlign(
        name="MirrorAlign" + tkType,
        Detectors=det_opts,
        RichGases=reco_opts["RichGases"],
        TrackSelector=TrackSelector(MinPCut=moni_opts["MinP4Align"]),
        TracksLocation=tkSel.OutputLocation,
        TrackSegmentsLocation=conf["TrackSegments"],
        CherenkovPhotonLocation=conf["CherenkovPhotons"],
        CherenkovAnglesLocation=conf["SignalCKAngles"],
        SummaryTracksLocation=conf["SummaryTracks"],
        PhotonToParentsLocation=conf["PhotonToParents"],
        PhotonMirrorDataLocation=conf["PhotonMirrorData"],
        PrebookHistos=moni_opts["PrebookHistos"],
        DeltaThetaRange=moni_opts["DeltaThetaRange"],
        NPhiBins=moni_opts["NPhiBins"],
        NThetaBins=moni_opts["NThetaBins"],
        HistoOutputLevel=moni_opts["HistoOutputLevel"],
        MirrorAlignTasks=MirrorAlignTasks,
        PoorestPopulation=moni_opts["PoorestPopulation"],
        MinUsefulTracks=moni_opts["MinUsefulTracks"],
        Variant=moni_opts["Variant"],
    )

    rich = "rich1" if RichGas == "Rich1Gas" else "rich2"

    # additional prefilter can be added,
    # e.g. for selecting events with certain HLT1 line decision
    prefilters = EventFilter + reco_prefilters()

    return Reconstruction(
        rich + "_mirror_align_reco", [make_odin().producer, alignMon], prefilters
    )


# Configurable that produces `PhotonCherenkovAngles` histograms ONLY
# This is for the Panel Alignment (offline/online tests)
@configurable
def standalone_rich_panel_align_reco(
    RichGas="Rich1Gas",
    EventFilter=[],
    noUT=False,
):
    """
    Reconstruction for RICH panel alignment
    """

    # Make the tracks
    if not noUT:
        # hlt2_tracks = make_hlt2_tracks()
        # match Hlt/RecoConf/python/RecoConf/standalone.py in Moore
        hlt2_tracks = make_hlt2_tracks(
            light_reco=True, fast_reco=False, use_pr_kf=False
        )
    else:
        # hlt2_tracks = make_hlt2_tracks_without_UT()
        # match Hlt/RecoConf/python/RecoConf/standalone.py in Moore
        hlt2_tracks = make_hlt2_tracks_without_UT(
            light_reco=True, fast_reco=False, use_pr_kf=True
        )

    # The track type we use
    track_version = "v1"
    tkType = "Long"

    # Get the fitted long tracks to use
    tks = hlt2_tracks["Best" + tkType]

    # Default RICH reco options
    reco_opts = default_rich_reco_options()

    # Radiators
    reco_opts["RichGases"] = [RichGas]

    # Enable "none" mode for even wider CK theta side bands
    reco_opts["PhotonSelection"] = "None"

    # As we aren't running PID, we do not need all hypos
    # reco_opts["Particles"] = ["muon","pion","kaon"]

    # Track selection
    tkSel = TracksToSelection(InputLocation=tks[track_version])

    # RICH photon reco
    conf = make_rich_pids(
        track_name=tkType, input_tracks=tkSel.OutputLocation, options=reco_opts
    )

    # The detector options
    det_opts = get_detector_bool_opts(reco_opts, tkType)

    # Data monitoring options
    moni_opts = default_rich_monitoring_options()

    # panel alignment histograms
    tight_sel = moni_opts["TightTrackSelection"]
    ckMon = PanelAlignAndMonitoring(
        # name="RiCKRes" + tkType + "Tight",
        name="RiCKRes" + conf["TrackName"] + "Tight",
        Detectors=det_opts,
        TrackSelector=TrackSelector(
            MinPCut=tight_sel["MinP"],
            MinPtCut=tight_sel["MinPt"],
            MaxChi2Cut=tight_sel["MaxChi2"],
            MaxGhostProbCut=tight_sel["MaxGhostProb"],
        ),
        CKResHistoRange=moni_opts["CKResHistoRange"],
        TracksLocation=conf["InputTracks"],
        TrackSegmentsLocation=conf["TrackSegments"],
        CherenkovPhotonLocation=conf["CherenkovPhotons"],
        CherenkovAnglesLocation=conf["SignalCKAngles"],
        SummaryTracksLocation=conf["SummaryTracks"],
        PhotonToParentsLocation=conf["PhotonToParents"],
    )

    rich = "rich1" if RichGas == "Rich1Gas" else "rich2"

    # additional prefilter can be added,
    # e.g. for selecting events with certain HLT1 line decision
    prefilters = EventFilter + reco_prefilters()

    return Reconstruction(
        rich + "_panel_align_reco", [make_odin().producer, ckMon], prefilters
    )


# Configurable that produces BOTH `Alignment` and `PhotonCherenkovAngles`
# This is for ONLINE monitoring (a.k.a. Real-time Alignment)
@configurable
def standalone_rich_online_align_reco(
    RichGas="Rich1Gas",
    MirrorAlignTasks=["Produce"],
    EventFilter=[],
    noUT=False,
):
    """
    Reconstruction for RICH mirror alignment & CK angle monitoring
    """

    # Make the tracks - should be the same as from standalone_rich_mirror_align_reco
    if not noUT:
        hlt2_tracks = make_hlt2_tracks(light_reco=True, fast_reco=False, use_pr_kf=True)
    else:
        hlt2_tracks = make_hlt2_tracks_without_UT()

    # The track type we use
    track_version = "v1"
    tkType = "Long"

    # Get the fitted long tracks to use
    tks = hlt2_tracks["Best" + tkType]

    # Default RICH reco options
    reco_opts = default_rich_reco_options()

    # Radiators
    reco_opts["RichGases"] = [RichGas]

    # Enable online mode for wider CK theta side bands
    reco_opts["PhotonSelection"] = "Online"

    # Enable extra photon info
    reco_opts["SaveMirrorData"] = True

    # As we aren't running PID, we do not need all hypos
    # reco_opts["Particles"] = ["muon","pion","kaon"]

    # Track selection
    tkSel = TracksToSelection(InputLocation=tks[track_version])

    # RICH photon reco
    conf = make_rich_pids(
        track_name=tkType, input_tracks=tkSel.OutputLocation, options=reco_opts
    )

    # The detector options
    det_opts = get_detector_bool_opts(reco_opts, tkType)

    # Data monitoring options
    moni_opts = default_rich_monitoring_options()
    # More monitoring options in case of the RICH alignment monitoring.
    moni_opts.update(alignment_rich_monitoring_options(radiator=RichGas))

    # mirror alignment histograms
    alignMon = MirrorAlign(
        name="MirrorAlign" + tkType,
        Detectors=det_opts,
        RichGases=reco_opts["RichGases"],
        TrackSelector=TrackSelector(MinPCut=moni_opts["MinP4Align"]),
        TracksLocation=tkSel.OutputLocation,
        TrackSegmentsLocation=conf["TrackSegments"],
        CherenkovPhotonLocation=conf["CherenkovPhotons"],
        CherenkovAnglesLocation=conf["SignalCKAngles"],
        SummaryTracksLocation=conf["SummaryTracks"],
        PhotonToParentsLocation=conf["PhotonToParents"],
        PhotonMirrorDataLocation=conf["PhotonMirrorData"],
        PrebookHistos=moni_opts["PrebookHistos"],
        DeltaThetaRange=moni_opts["DeltaThetaRange"],
        NPhiBins=moni_opts["NPhiBins"],
        NThetaBins=moni_opts["NThetaBins"],
        HistoOutputLevel=moni_opts["HistoOutputLevel"],
        MirrorAlignTasks=MirrorAlignTasks,
        PoorestPopulation=moni_opts["PoorestPopulation"],
        MinUsefulTracks=moni_opts["MinUsefulTracks"],
        Variant=moni_opts["Variant"],
    )

    # CK res histograms for alignment monitoring
    tight_sel = moni_opts["TightTrackSelection"]
    ckMon = PanelAlignAndMonitoring(
        # name="RiCKRes" + tkType + "Tight",
        name="RiCKRes" + conf["TrackName"] + "Tight",
        Detectors=det_opts,
        TrackSelector=TrackSelector(
            MinPCut=tight_sel["MinP"],
            MinPtCut=tight_sel["MinPt"],
            MaxChi2Cut=tight_sel["MaxChi2"],
            MaxGhostProbCut=tight_sel["MaxGhostProb"],
        ),
        CKResHistoRange=moni_opts["CKResHistoRange"],
        TracksLocation=conf["InputTracks"],
        TrackSegmentsLocation=conf["TrackSegments"],
        CherenkovPhotonLocation=conf["CherenkovPhotons"],
        CherenkovAnglesLocation=conf["SignalCKAngles"],
        SummaryTracksLocation=conf["SummaryTracks"],
        PhotonToParentsLocation=conf["PhotonToParents"],
    )

    rich = "rich1" if RichGas == "Rich1Gas" else "rich2"

    # additional prefilter can be added,
    # e.g. for selecting events with certain HLT1 line decision
    prefilters = EventFilter + reco_prefilters()

    return Reconstruction(
        rich + "_online_align_reco",
        [make_odin().producer, alignMon, ckMon],
        # [make_odin().producer, alignMon, ckMon, rtuple],
        prefilters,
    )

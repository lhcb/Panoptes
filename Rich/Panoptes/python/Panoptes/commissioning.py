###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from MooreOnlineConf.utils import decode_tae, if_then, run_all, update_and_reset
from PyConf.Algorithms import HltRoutingBitsFilter, OdinTypesFilter
from PyConf.Algorithms import Rich__Future__Mon__BankSizes as BankSizes
from PyConf.Algorithms import Rich__Future__Mon__BXTypeMonitors as BXTypeMonitors
from PyConf.Algorithms import Rich__Future__Mon__DataTuple as DataTuple
from PyConf.Algorithms import Rich__Future__Mon__HitMaps as HitMaps
from PyConf.Algorithms import Rich__Future__Mon__TAEMonitor as TAEMonitor
from PyConf.application import default_raw_banks, make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.tonic import configurable
from RecoConf.config import Reconstruction
from RecoConf.rich_reconstruction import default_rich_reco_options, make_rich_pixels

try:
    import OnlineEnvBase as OnlineEnv

    TAE_HALF_WINDOW = OnlineEnv.TAE
except ImportError:
    TAE_HALF_WINDOW = 3


@configurable
def daq_commissioning(
    offlineMode=False,
    enableTuple=False,
    verbosePlots=False,
    offlinePlots=False,
    spatialPlots=False,
    enableTAE=False,
    runNumber=0,
):
    """RICH commissioning"""

    # Default RICH reco options
    reco_opts = default_rich_reco_options()

    # Decoding and pixels
    rich_pixels = make_rich_pixels(reco_opts)

    def make_rich_pixels_forTAE(name=""):
        return make_rich_pixels(reco_opts)["RichDecodedData"]

    # The list of algorithms to run
    to_run = []

    # Online histogram handling
    if not offlineMode:
        # Should be first in the list if required
        to_run += [update_and_reset()]

    rb_lumi_filter = HltRoutingBitsFilter(
        name="RBFilterLumi",
        RawBanks=default_raw_banks("HltRoutingBits"),
        RequireMask=(1 << 1, 0, 0),  # Lumi events
        PassOnError=False,
    )

    rb_phys_filter = HltRoutingBitsFilter(
        name="RBFilterPhys",
        RawBanks=default_raw_banks("HltRoutingBits"),
        RequireMask=(1 << 14, 0, 0),  # Physics events
        PassOnError=False,
    )

    rb_smog_filter = HltRoutingBitsFilter(
        name="RBFilterSMOG",
        RawBanks=default_raw_banks("HltRoutingBits"),
        RequireMask=(1 << 15, 0, 0),  # SMOG events
        PassOnError=False,
    )

    # Hit Maps without filters
    hMaps = HitMaps(
        name="HitMaps",
        VerbosePlots=verbosePlots,
        OfflinePlots=offlinePlots,
        SpatialPlots=spatialPlots,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"],
    )
    to_run += [hMaps]

    # BXType Monitors without filters
    hBXType = BXTypeMonitors(
        name="BXTypeMonitors",
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"],
    )
    to_run += [hBXType]

    # BXType Monitors with Lumi filter
    hBXTypeLumi = BXTypeMonitors(
        name="BXTypeMonitorsLumi",
        allow_duplicate_instances_with_distinct_names=True,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"],
    )
    to_run += [
        CompositeNode(
            "Lumi",
            [rb_lumi_filter, hBXTypeLumi],
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True,
        )
    ]

    # BXType Monitors and Hit Maps with Phys filter
    hMapsPhys = HitMaps(
        name="HitMapsPhys",
        allow_duplicate_instances_with_distinct_names=True,
        VerbosePlots=verbosePlots,
        OfflinePlots=offlinePlots,
        SpatialPlots=spatialPlots,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"],
    )
    hBXTypePhys = BXTypeMonitors(
        name="BXTypeMonitorsPhys",
        allow_duplicate_instances_with_distinct_names=True,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"],
    )
    to_run += [
        CompositeNode(
            "Phys",
            [rb_phys_filter, hBXTypePhys, hMapsPhys],
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True,
        )
    ]

    # BXType Monitors and Hit Maps with SMOG filter
    hMapsSMOG = HitMaps(
        name="HitMapsSMOG",
        allow_duplicate_instances_with_distinct_names=True,
        VerbosePlots=verbosePlots,
        OfflinePlots=offlinePlots,
        SpatialPlots=spatialPlots,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"],
    )
    hBXTypeSMOG = BXTypeMonitors(
        name="BXTypeMonitorsSMOG",
        allow_duplicate_instances_with_distinct_names=True,
        ODINLocation=make_odin(),
        DecodedDataLocation=rich_pixels["RichDecodedData"],
    )
    to_run += [
        CompositeNode(
            "SMOG",
            [rb_smog_filter, hBXTypeSMOG, hMapsSMOG],
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True,
        )
    ]

    # Bank Size Monitors
    hFragSize = BankSizes(name="FragSize", RawBanks=default_raw_banks("Rich"))
    to_run += [hFragSize]

    # Ntuple maker.
    if offlineMode and enableTuple:
        rtuple = DataTuple(
            name="DataTuple",
            ODINLocation=make_odin(),
            DecodedDataLocation=rich_pixels["RichDecodedData"],
            RunNumber=runNumber,
        )
        to_run += [rtuple]

    # TAE monitoring
    if enableTAE:
        is_tae, tae_decoding, tae_odins, tae_data = decode_tae(
            make_rich_pixels_forTAE, half_window=TAE_HALF_WINDOW
        )
        tae_monitor = TAEMonitor(
            name="TAEMonitor",
            ODINsLocation=list(tae_odins.values()),
            DecodedDatasLocation=list(tae_data.values()),
        )
        to_run += [
            if_then("IfTAE", is_tae, run_all("TAE", [tae_decoding, tae_monitor]))
        ]

    return Reconstruction("rich_comissioning", to_run)

###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import Reconstruction
from MooreOnlineConf.utils import update_and_reset
from PyConf import configurable
from PyConf.Algorithms import (
    Rich__Future__MC__TrackToMCParticleRelations as TkToMCPRels,
)
from PyConf.Algorithms import (
    Rich__Future__Rec__MC__Moni__RichPIDTupleCreatorAlg as RichPIDTuple,
)
from PyConf.Algorithms import TracksToSelection
from PyConf.application import make_odin
from PyConf.tonic import configurable
from PyConf.Tools import TrackSelector
from RecoConf.data_from_file import mc_unpacker
from RecoConf.mc_checking import (
    make_links_lhcbids_mcparticles_tracking_system,
    make_links_lhcbids_mcparticles_VP_FT,
    make_links_tracks_mcparticles,
)
from RecoConf.rich_data_monitoring import (
    default_rich_monitoring_options,
    make_rich_pixel_monitors,
    make_rich_track_monitors,
)
from RecoConf.rich_mc_checking import (
    default_rich_checkers,
    default_rich_checking_options,
    make_rich_checkers,
)
from RecoConf.rich_reconstruction import (
    default_rich_reco_options,
    get_detector_bool_opts,
    make_rich_pids,
)
from RecoConf.standalone import reco_prefilters


@configurable
def BrahmaSutraConf(
    offlineMode=True, noUT=False, do_data_monitoring=True, ParticleGun=False
):
    """
    The tracking and RICH reco for the RICH reconstruction from MC data
    """
    # Make the tracks
    if not noUT:
        print("Configuring tracking with UT")
        from RecoConf.hlt2_tracking import make_hlt2_tracks

        hlt2_tracks = make_hlt2_tracks()
    else:
        print("Configuring tracking without UT")
        from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT

        hlt2_tracks = make_hlt2_tracks_without_UT()

    # print("BrahmaSutraConf: Available Track Types", hlt2_tracks.keys())

    # The track type we use
    track_version = "v1"

    tkType = "Long"

    # Get the fitted long tracks to use
    # In ParticleGun events only 'Best' tracks available.

    if not ParticleGun:
        tks = hlt2_tracks["Best" + tkType]
    else:
        tks = hlt2_tracks["Best"]

    # CK Theta res range to use
    # CKResHistoRange = (0.0250, 0.0055, 0.0035)
    CKResHistoRange = (0.0055, 0.0035)

    # Default RICH reco options
    reco_opts = default_rich_reco_options()
    reco_opts["PhotonSelection"] = "Nominal"

    # Track selection

    tkSel = TracksToSelection(InputLocation=tks[track_version])

    # RICH photon reco
    conf = make_rich_pids(
        track_name=tkType, input_tracks=tkSel.OutputLocation, options=reco_opts
    )

    # The detector and radiator options. Not used for now.
    # rad_opts = get_radiator_bool_opts(reco_opts, tkType)
    det_opts = get_detector_bool_opts(reco_opts, tkType)

    algs_to_run = []

    # Online histogram handling. Not used for MC data
    if not offlineMode:
        # Should be first in the list if required
        algs_to_run += [update_and_reset()]

    if do_data_monitoring:
        # Data monitoring options
        moni_opts = default_rich_monitoring_options()
        moni_set = "Standard"
        # Force same CK theta range as counting monitor
        moni_opts["CKResHistoRange"] = CKResHistoRange
        moni_algs = make_rich_track_monitors(
            conf=conf,
            reco_opts=reco_opts,
            moni_opts=moni_opts,
            moni_set=moni_set,
        )
        moni_algs.update(
            make_rich_pixel_monitors(
                conf=conf,
                reco_opts=reco_opts,
                moni_opts=moni_opts,
                moni_set=moni_set,
            )
        )
        algs_to_run += [v for _, v in sorted(moni_algs.items())]

        check_opts = default_rich_checking_options()

        # Force same CK theta range as calibration monitor
        check_opts["CKResHistoRange"] = CKResHistoRange
        conf["OriginalV1Tracks"] = tks[track_version]

        check_algs = make_rich_checkers(
            conf=conf, reco_opts=reco_opts, check_opts=check_opts, moni_set=moni_set
        )

        algs_to_run += [v for _, v in sorted(check_algs.items())]

        if not ParticleGun:
            make_pid_tuple = make_rich_PID_Ntuple(
                conf=conf, reco_opts=reco_opts, check_opts=check_opts, moni_set=moni_set
            )

            algs_to_run += [v for _, v in sorted(make_pid_tuple.items())]

    return Reconstruction("rich_MC_reconstruction", algs_to_run, reco_prefilters())


@configurable
def make_rich_PID_Ntuple(conf, reco_opts, check_opts, moni_set="Standard"):
    """
    Activates PID Ntuple from RICH. Also returns dict of the activated algorithm

    Args:
        conf       (dict): Reconstruction configuration (data) to run monitoring on
        reco_opts  (dict): Reconstruction options
        check_opts (dict): MC checking options
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    # The dict of configured monitors to return
    results = {}

    # Momentum selections for performance plots
    momentumCuts = check_opts["MomentumRanges"]

    # The track name for this configuration
    track_name = conf["TrackName"]

    # The detector and radiator options
    det_opts = get_detector_bool_opts(reco_opts, track_name)
    # rad_opts = get_radiator_bool_opts(reco_opts, track_name)

    # get the list of checkers to activate
    checkers = default_rich_checkers(moni_set)

    # MC Info
    mcps = mc_unpacker("MCParticles")
    richSummaries = mc_unpacker("MCRichDigitSummaries")

    # Track linker stuff
    if check_opts["UseUT"]:
        links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    else:
        links_to_lhcbids = make_links_lhcbids_mcparticles_VP_FT()

    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=conf["OriginalV1Tracks"], LinksToLHCbIDs=links_to_lhcbids
    )

    # Make the RICH Track -> MCParticle relations table
    tkMCPRels = TkToMCPRels(
        name="RichTkToMCPRelations_{hash}",
        TracksLocation=conf["OriginalV1Tracks"],
        MCParticlesLinkLocation=links_to_tracks,
        MCParticlesLocation=mcps,
    )

    # Momentum cuts for plotting etc (by radiator)
    # pCuts = {
    #    "MinP": (0.5 * GeV, 0.5 * GeV, 0.5 * GeV),
    #    "MaxP": (120.0 * GeV, 120.0 * GeV, 120.0 * GeV)
    # }
    pCutsPID = [2.0 * GeV, 100 * GeV]

    # PID performance
    key = "PIDPerformance"
    if key in checkers:
        # print ("BrahmaSutra: Activating PID Ntuple Creation")
        # Make a PID monitor for this selection
        results[key] = RichPIDTuple(
            name="RichPIDTupleCreatorAlg",
            TrackSelector=TrackSelector(MinPCut=pCutsPID[0], MaxPCut=pCutsPID[1]),
            TracksLocation=conf["InputTracks"],
            RichPIDsLocation=conf["RichPIDs"],
            TrackToMCParticlesRelations=tkMCPRels.TrackToMCParticlesRelations,
        )
    return results

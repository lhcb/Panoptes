###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiKernel.SystemOfUnits import GeV
from MooreOnlineConf.utils import update_and_reset
from PyConf.Algorithms import (
    Rich__Future__Rec__Counting__PhotonCounting as PhotonCounting,
)
from PyConf.Algorithms import TracksToSelection
from PyConf.application import make_odin
from PyConf.tonic import configurable
from RecoConf.config import Reconstruction
from RecoConf.rich_data_monitoring import (
    default_rich_monitoring_options,
    make_rich_pixel_monitors,
    make_rich_track_monitors,
)
from RecoConf.rich_reconstruction import (
    default_rich_reco_options,
    get_detector_bool_opts,
    make_rich_pids,
)
from RecoConf.standalone import reco_prefilters


@configurable
def standalone_photon_counting(
    offlineMode=False,
    noUT=False,
    min_p=(30 * GeV, 30 * GeV),
    min_vphits=(8, 8),
    do_data_monitoring=False,
):
    """
    The tracking and RICH reco for the RICH photon counting
    """

    # Make the tracks
    if not noUT:
        print("Configuring tracking with UT")
        from RecoConf.hlt2_tracking import make_hlt2_tracks

        hlt2_tracks = make_hlt2_tracks()
    else:
        print("Configuring tracking without UT")
        from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT

        hlt2_tracks = make_hlt2_tracks_without_UT()
    # print("Available Track Types", hlt2_tracks.keys())

    # The track type we use
    track_version = "v1"
    tkType = "Long"

    # Get the fitted long tracks to use
    tks = hlt2_tracks["Best" + tkType]

    # Default RICH reco options
    reco_opts = default_rich_reco_options()

    # Enable online mode for wider CK theta side bands
    reco_opts["PhotonSelection"] = "Online"

    # Tweak the momentum cuts used for the photon counting
    reco_opts["MinP"] = min_p

    # As we aren't running PID, we do not need all hypos
    # reco_opts["Particles"] = ["muon","pion","kaon"]

    # CK Theta res range to use
    CKResHistoRange = (0.0055, 0.0035)

    # Track selection
    tkSel = TracksToSelection(InputLocation=tks[track_version])

    # RICH photon reco
    conf = make_rich_pids(
        track_name=tkType, input_tracks=tkSel.OutputLocation, options=reco_opts
    )

    # The detector options
    det_opts = get_detector_bool_opts(reco_opts, tkType)

    algs_to_run = []

    # Online histogram handling
    if not offlineMode:
        # Should be first in the list if required
        algs_to_run += [update_and_reset()]

    # Counting algorithm
    count = PhotonCounting(
        name="RichPhotonCounting" + tkType,
        MinP=reco_opts["MinP"],
        MaxP=reco_opts["MaxP"],
        MinVPHits=min_vphits,
        Detectors=det_opts,
        ODINLocation=make_odin(),
        TracksLocation=tkSel.OutputLocation,
        CKResHistoRange=CKResHistoRange,
        TrackSegmentsLocation=conf["TrackSegments"],
        CherenkovPhotonLocation=conf["CherenkovPhotons"],
        CherenkovAnglesLocation=conf["SignalCKAngles"],
        SummaryTracksLocation=conf["SummaryTracks"],
        PhotonToParentsLocation=conf["PhotonToParents"],
    )

    algs_to_run += [make_odin().producer, count]

    if do_data_monitoring:
        # Data monitoring options
        moni_opts = default_rich_monitoring_options()
        moni_set = "RefractiveIndex"  # ToDo add a "PhotonCounting" setting in Moore
        # Force same CK theta range as counting monitor
        moni_opts["CKResHistoRange"] = CKResHistoRange
        moni_algs = make_rich_track_monitors(
            conf=conf,
            reco_opts=reco_opts,
            moni_opts=moni_opts,
            moni_set=moni_set,
        )
        moni_algs.update(
            make_rich_pixel_monitors(
                conf=conf,
                reco_opts=reco_opts,
                moni_opts=moni_opts,
                moni_set=moni_set,
            )
        )
        algs_to_run += [v for _, v in sorted(moni_algs.items())]

    return Reconstruction("rich_photon_counting", algs_to_run, reco_prefilters())

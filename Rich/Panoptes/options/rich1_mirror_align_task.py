###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import multiprocessing
import os
import re

from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiKernel.SystemOfUnits import GeV
from Moore import (
    options,
)
from Panoptes.alignment import (
    standalone_rich_mirror_align_reco,
    standalone_rich_online_align_reco,
)
from PyConf.application import (
    configure,
    configure_input,
)
from RecoConf.rich_data_monitoring import (
    alignment_rich_monitoring_options,
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import (
    default_rich_reco_options,
)
from ruamel.yaml import (
    YAML,
)

tasks = [
    "Produce",  #    # fill the production set of histograms
    #'Monitor',      # add various checking histograms
    #'MapExlore',    # explore tracks and photons for the HLT1 pre-selection line "map"
    #'MapConstruct', # construct exploratory data structure for the HLT1 pre-selection line "map"
    #'Map',          # create data structure to be used in the HLT1 selection line with entire contents
    #'MapUse',       # use the "map" data structure a la in the HLT1 pre-selection line "map"
    #'AllFillCount', # add counters for optimization of the RICH2 mirror combinations subset
    #'AllFillPhi',   # fill 1D phi histos for optimization of the RICH2 mirror combinations subset
    #'MapPhi',       # create data structure to be used in the HLT1 selection line with contents in quantiles
    #'MapPhiUse',    # use the "mapPhi" data structure in the HLT1 pre-selection line "mapPhi"
    #'CheckRestFill',# check filling the rest of RICH2 mirror combinations along with 8 poorest
    #'SkipFilled',   # check filling all RICH2 mirror combinations with skipping when filled
]

# cumulative substring with all tasks chosen
# to be included into histo_file name
histo_tasks = ""
for task in tasks:
    histo_tasks += "_" + task

isQMTest = "QMTTEST_NAME" in os.environ

# Override previously set defaults
# ROOT file reader currently not thread safe :(
options.n_threads = (
    1 if options.input_type == "ROOT" or isQMTest else multiprocessing.cpu_count()
)

yaml = YAML()

# retrieve variant
v_file = os.getenv("QMTTEST_NAME", "rich1") + "_variant.yml"
with open(v_file) as inp:
    variant = yaml.load(inp)

if isQMTest:
    if UseDD4Hep:
        options.evt_max = 5000
        # Don't set here... input data is set in qmt file
        # options.input_raw_format = 0.3
        # options.set_input_from_testfiledb("rich-mirror-align-2022")
else:
    # retrieve number of events
    em = re.findall(r"([\d]+)k", variant)[0]
    iem = int(em)
    if iem == 0:
        options.evt_max = 100
    else:
        options.evt_max = iem * 1000

# configure input
config = configure_input(options)

thisName = (
    "rich1_mirror_align_task_noUT"
    if "_noUT_" in variant
    else f"rich1_mirror_align_task{'_detdesc' if not UseDD4Hep else ''}"
)

myName = os.environ["QMTTEST_NAME"] if isQMTest else thisName

noUT = "noUT" in myName

# prepare updates:
default_reco_opts = {}
align_opts = {}

# start updating the dicts:

default_reco_opts.update({"PhotonSelection": "None"})

align_opts.update({"Variant": variant})

if "subset" in variant:
    central_mirr_combs = {
        "PrebookHistos": [
            ("p00", "s03"),
            ("p01", "s06"),
            ("p02", "s12"),
            ("p03", "s09"),
        ],
    }
    align_opts.update(central_mirr_combs)

if "minp40" in variant:
    align_opts.update({"MinP4Align": 40.0 * GeV})

if "theta2" in variant:
    align_opts.update({"DeltaThetaRange": 0.002})

if "qnt20f60" in variant:
    if "1000k" in variant:
        align_opts.update({"PoorestPopulation": 358.0})
    elif "5000k" in variant:
        align_opts.update({"PoorestPopulation": 1950.0})

if "_phi2" in variant:
    align_opts.update({"MinUsefulTracks": 2})

# prepare part of the overridden options in use
# to be recorded into the YAML file
align_opts_dump = alignment_rich_monitoring_options(
    radiator="Rich1Gas", init_override_opts=align_opts
)

# prepare override of the default_rich_monitoring_options
default_tight_track_sel = default_rich_monitoring_options()["TightTrackSelection"]

default_tight_track_sel["MinP"] = 40.0 * GeV

default_moni_opts = {
    "TightTrackSelection": default_tight_track_sel,
    "UseUT": not noUT,
}

# add overridden default_rich_monitoring_options
# to the part of the options in use to be recorded into the YAML file
align_opts_dump.update(
    default_rich_monitoring_options(init_override_opts=default_moni_opts)
)

# retrieve timestamp from variant, not form file, to ensure consistency
ts = re.findall(r"[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}-[\d]{2}", variant)[0]

# prepare options in use to be recorded into the YAML file
opts_dump = {}
opts_dump[ts] = {}
opts_dump[ts]["reco_opts"] = align_opts_dump

# append YAML file with the part of the options in use
reco_opts_file = os.getenv("QMTTEST_NAME", "rich1") + "_reco_opts.yml"
with open(reco_opts_file, "a") as out:
    yaml.dump(opts_dump, out)

# retrieve iteration number substring
it_num_file = os.getenv("QMTTEST_NAME", "rich1") + "_iter_numer.yml"
with open(it_num_file) as inp:
    iN = "_i" + str(yaml.load(inp))

# prepare histo_file name
if not isQMTest:
    if tasks[0] == "Produce":
        options.histo_file = variant + iN + ".root"
    else:
        options.histo_file = variant + histo_tasks + iN + ".root"

# by default, no additional filtering of the events
event_filter = []

# list of decisions with which events to take
line = []

# when particular decision of HLT1 line about RICH1 or RICH2 is wanted
if "_R1_" in variant or "_R2_" in variant or "_R1M_" in variant or "_R2M_" in variant:
    # prepare filter for selecting events
    # chosen for alignment of mirrors of particular RICH
    import Functors as F
    from PyConf.Algorithms import (
        HltDecReportsDecoder,
        VoidFilter,
    )
    from PyConf.application import (
        default_raw_banks,
        default_raw_event,
    )

    # in principle, there can be any number and combination of them:
    if "_R1_" in variant:
        line.append("Hlt1RICH1AlignmentDecision")
    if "_R2_" in variant:
        line.append("Hlt1RICH2AlignmentDecision")
    if "_R1M_" in variant:
        line.append("Hlt1RICH1MapAlignDecision")
    if "_R2M_" in variant:
        line.append("Hlt1RICH2MapAlignDecision")

    event_filter = []
    # Don't filter in QMT tests
    if not isQMTest:
        with default_raw_event.bind(raw_event_format=0.3):
            hlt1_dec_reports = HltDecReportsDecoder(
                RawBanks=default_raw_banks("HltDecReports"), SourceID="Hlt1"
            )
            if UseDD4Hep:
                hlt1_filter = VoidFilter(
                    name="Streaming_filter",
                    Cut=F.DECREPORTS_FILTER(
                        Lines=line,
                        DecReports=hlt1_dec_reports.OutputHltDecReportsLocation,
                    ),
                )
                event_filter = [hlt1_filter]

# choose the relevant track_maker
if noUT:
    from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT as track_maker
else:
    from RecoConf.hlt2_tracking import make_hlt2_tracks as track_maker

from PyConf.Algorithms import PrForwardTrackingVelo, PrHybridSeeding, PrMatchNN

# Min track momentum for tracking algs
min_p = 10 * GeV

# finally, run the reconstruction and histogram filling
with (
    PrForwardTrackingVelo.bind(MinP=min_p),
    PrMatchNN.bind(MinP=min_p),
    PrHybridSeeding.bind(MinP=min_p),
    track_maker.bind(use_pr_kf=True, light_reco=True, fast_reco=True),
    default_rich_reco_options.bind(init_override_opts=default_reco_opts),
    default_rich_monitoring_options.bind(init_override_opts=default_moni_opts),
    alignment_rich_monitoring_options.bind(
        radiator="Rich1Gas", init_override_opts=align_opts
    ),
):
    reconstruction = standalone_rich_mirror_align_reco(
        RichGas="Rich1Gas", MirrorAlignTasks=tasks, EventFilter=event_filter, noUT=noUT
    )
    config.update(configure(options, reconstruction.node))

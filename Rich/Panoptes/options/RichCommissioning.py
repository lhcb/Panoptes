###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from Moore import options
from Panoptes.commissioning import daq_commissioning
from RecoConf.config import run_reconstruction

isQMTTest = "QMTTEST_NAME" in os.environ

partition = os.getenv("partition")
runNumber = os.getenv("runNumber")
evtMax = os.getenv("evtMax")

if partition and runNumber and not isQMTTest:
    print("Partition", partition, "Run", runNumber)
    options.histo_file = f"/calib/rich/RichCommissioning_{runNumber}.root"

evt_max = evtMax if evtMax else -1
options.evt_max = 1000 if isQMTTest else evt_max

options.n_threads = 1

options.print_freq = 100 if isQMTTest else 1000

verbosePlots = False
offlinePlots = False
spatialPlots = False

dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "Rich1", "Rich2"])
pit_db_loc = "/group/online/hlt/conditions.run3/lhcb-conditions-database"
if os.path.exists(pit_db_loc):
    dd4hep.ConditionsLocation = "file:" + pit_db_loc

with daq_commissioning.bind(
    offlineMode=True,
    enableTuple=False,
    verbosePlots=verbosePlots,
    offlinePlots=offlinePlots,
    spatialPlots=spatialPlots,
    runNumber=runNumber,
):
    run_reconstruction(options, daq_commissioning)

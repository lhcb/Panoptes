###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import multiprocessing
import os

from Gaudi.Configuration import DEBUG, INFO, VERBOSE
from GaudiKernel.SystemOfUnits import GeV
from Moore import options
from Panoptes.photoncounting import standalone_photon_counting
from PyConf.Algorithms import PrForwardTrackingVelo, PrHybridSeeding, PrMatchNN
from PyConf.Algorithms import (
    Rich__Future__Rec__Counting__PhotonCounting as PhotonCounting,
)
from RecoConf.config import run_reconstruction
from ROOT import gROOT

options.output_level = INFO

# Silence ROOT
gROOT.ProcessLine("gErrorIgnoreLevel = kWarning;")

isQMTTest = "QMTTEST_NAME" in os.environ

myName = os.getenv("QMTTEST_NAME", "RichPhotonCounting")

options.histo_file = myName + ".root"

# Override previously set defaults
# ROOT file reader currently not thread safe :(
options.n_threads = (
    1 if options.input_type == "ROOT" or isQMTTest else multiprocessing.cpu_count()
)
# options.n_threads = 1
options.evt_max = 5000 if isQMTTest else -1

# Is UT active in this task ?
noUT = "noUT" in myName

# Enable additional monitoring algorithms ?
do_moni = isQMTTest

# Create run by run histos in photon counting algorithm
run_by_run_histos = True

# Control background model used in photon counting algorithm
quadratic_background_model = True

# Output level for photon counting alg
out_level = INFO if isQMTTest else VERBOSE

# Momentum Cuts
minPRads = (30 * GeV, 30 * GeV)
min_p = min(minPRads)

if noUT:
    from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT as track_maker
else:
    from RecoConf.hlt2_tracking import make_hlt2_tracks as track_maker

with (
    PrForwardTrackingVelo.bind(MinP=min_p),
    PrMatchNN.bind(MinP=min_p),
    PrHybridSeeding.bind(MinP=min_p),
    track_maker.bind(use_pr_kf=True, light_reco=True, fast_reco=True),
    PhotonCounting.bind(
        OutputLevel=out_level,
        CreatePDFSummary=not isQMTTest,
        OKToPublish=not isQMTTest,
        AllowCountingAtStop=not isQMTTest,
        RunByRunHists=run_by_run_histos,
        QuadraticBckModel=quadratic_background_model,
    ),
    standalone_photon_counting.bind(
        noUT=noUT, min_p=minPRads, offlineMode=True, do_data_monitoring=do_moni
    ),
):
    run_reconstruction(options, standalone_photon_counting)

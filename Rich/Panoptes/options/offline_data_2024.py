###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Run me offline like
# partition=calib runNumber=298770 evtMax=2000 gaudirun.py ${PANOPTESROOT}/options/offline_data_2024.py ${PANOPTESROOT}/options/RichCommissioning.py
# note : wihtout partition or runNumber, evtMax should be less than 50000 (single in put file)

import glob
import os

from Configurables import LHCbApp
from Moore import options

partition = os.getenv("partition")
runNumber = os.getenv("runNumber")

if partition and runNumber:
    lrunNumber = "%010d" % int(runNumber)
    if partition == "local":
        path = "/localdisk/rich_tmp_data/runs/"
    elif partition == "daqarea":
        path = "/daqarea51/rich/"
    elif partition == "calib":
        path = "/calib/online/tmpHlt1Dumps/LHCb/%s/" % lrunNumber
    elif partition == "tae":
        path = "/calib/TAE/LHCb/%s/" % lrunNumber
    else:
        path = "/hlt2/objects/%s/%s/" % (partition, lrunNumber)

    files = sorted(glob.glob(path + "*.mdf"), key=os.path.getmtime)
    options.input_files = files
    options.input_type = "MDF"
    LHCbApp().DataType = "Upgrade"
    options.simulation = False
    options.geometry_version = "run3/2024.Q1.2-v00.00"
    options.conditions_version = "master"

else:
    options.set_input_and_conds_from_testfiledb("rich-decode-2024")
    options.geometry_version = "run3/2024.Q1.2-v00.00"

# Due to issues with hit sorting in FT disable Mat corrections
# See for more details https://gitlab.cern.ch/lhcb/Panoptes/-/issues/7
from PyConf.Algorithms import PrStoreSciFiHits

PrStoreSciFiHits.global_bind(ApplyMatContractionCalibration=False)
# To be removed once the issue is resolved properly.

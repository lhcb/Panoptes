###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
RICH comissioning monitor

based on /group/online/dataflow/cmtuser/ONLINE_v7r10/Online/RawBankSizes/options/EventSizeMon.py

@author C.Jones
"""

__version__ = "1.0"
__author__ = "Markus Frank <Markus.Frank@cern.ch>"

import Configurables
import Gaudi.Configuration as Gaudi
import GaudiOnline
import OnlineEnvBase as OnlineEnv
from Configurables import ExecutionReportsWriter, HLTControlFlowMgr, LHCbApp
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from Configurables import Online__AlgFlowManager as AlgFlowManager
from Moore import options
from Panoptes.commissioning import daq_commissioning
from RecoConf.config import run_reconstruction

options.data_type = "Upgrade"

options.output_level = Gaudi.INFO
OnlineEnv.OutputLevel = options.output_level

nThreads = 1
options.n_threads = nThreads
OnlineEnv.NumberOfThreads = nThreads

# options.event_store = 'EvtStoreSvc'

options.geometry_version = OnlineEnv.DDDBTag
options.conditions_version = OnlineEnv.CondDBTag

options.input_type = "Online"
options.output_type = "Online"

options.simulation = False

dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "Rich1", "Rich2"])

# When running online directly access the primary checkout on disc instead
dd4hep.ConditionsLocation = (
    "file:///group/online/hlt/conditions.run3/lhcb-conditions-database"
)
# Conditions that should also have an explicit run-by-run entry
dd4hep.LimitedIOVPaths = ["Conditions/LHCb/Online/Tell40Links.yml"]

run_reconstruction(options, daq_commissioning)

application = GaudiOnline.Application(
    outputLevel=OnlineEnv.OutputLevel,
    partitionName=OnlineEnv.PartitionName,
    partitionID=OnlineEnv.PartitionID,
    classType=GaudiOnline.Class1,
)

# Something wrong with this, as causes configuration to silently
# abort. So comment out for now..
# application.setup_fifolog(format='%-8LEVEL %-24SOURCE', device='fifo')

application.setup_mbm_access("Events", True)
cfm = HLTControlFlowMgr("HLTControlFlowMgr")
application.setup_hive(cfm, 44)

application.setup_monitoring("RichMon")
application.monSvc.DimUpdateInterval = 1
application.config.burstPrintCount = 30000
# Mode selection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.numEventThreads = nThreads
application.config.MBM_numConnections = 1
application.config.MBM_numEventThreads = 1
application.config.execMode = 1
application.updateAndReset.saveHistograms = 1
application.updateAndReset.saverCycle = 180
application.updateAndReset.saveSetDir = "/hist/Savesets"
if OnlineEnv.PartitionName.startswith("TEST"):
    application.updateAndReset.saveSetDir = "Savesets"
application.config.MBM_requests = [
    "EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0"
]

application.config.expandTAE = True

print("Setup complete....")

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
RICH comissioning monitor

based on /group/online/dataflow/cmtuser/ONLINE_v7r10/Online/RawBankSizes/options/EventSizeMon.py

@author C.Jones
"""

__version__ = "1.0"
__author__ = "Markus Frank <Markus.Frank@cern.ch>"

import Configurables
import Gaudi.Configuration as Gaudi
import GaudiOnline
import OnlineEnvBase as OnlineEnv
from Configurables import ExecutionReportsWriter, HLTControlFlowMgr, LHCbApp
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from Configurables import Online__AlgFlowManager as AlgFlowManager
from GaudiKernel.SystemOfUnits import GeV
from Moore import options
from RecoConf.config import run_reconstruction

LHCbApp().DataType = "Upgrade"

options.output_level = Gaudi.INFO
OnlineEnv.OutputLevel = options.output_level

nThreads = 1
options.n_threads = nThreads
OnlineEnv.NumberOfThreads = nThreads

# options.event_store = 'EvtStoreSvc'

options.input_type = "Online"
options.output_type = "Online"
options.simulation = False

options.geometry_version = OnlineEnv.DDDBTag
options.conditions_version = OnlineEnv.CondDBTag

# Is UT active in this task ?
noUT = False
# noUT = "no_ut" in OnlineEnv.HLTType

dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "VP", "FT", "Rich1", "Rich2"])
if not noUT:
    dd4hep.DetectorList += ["UT"]

# When running online directly access the primary checkout on disc instead
dd4hep.ConditionsLocation = (
    "file:///group/online/hlt/conditions.run3/lhcb-conditions-database"
)
# Conditions that should also have an explicit run-by-run entry
dd4hep.LimitedIOVPaths = [
    # Tel40 links condition
    "Conditions/LHCb/Online/Tell40Links.yml",
    # Magnet
    "Conditions/LHCb/Online/Magnet.yml",
    # Beamspot
    "Conditions/LHCb/Online/InteractionRegion.yml",
    # RICH radiator T, P values
    "Conditions/Rich1/Online.yml",
    "Conditions/Rich2/Online.yml",
    # VP alignment
    "Conditions/VP/Motion.yml",
    "Conditions/VP/Alignment/Global.yml",
    "Conditions/VP/Alignment/Ladders.yml",
    "Conditions/VP/Alignment/Modules.yml",
    # FT alignment
    "Conditions/FT/Alignment/FTSystem.yml",
    "Conditions/FT/Alignment/Mats.yml",
    "Conditions/FT/Alignment/Modules.yml",
    "Conditions/FT/Alignment/HalfLayer/FTSystem.yml",
    "Conditions/FT/Alignment/HalfLayer/Mats.yml",
    "Conditions/FT/Alignment/HalfLayer/Modules.yml",
    # UT alignment
    "Conditions/UT/Alignment/Global.yml",
    "Conditions/UT/Alignment/HalfLayers.yml",
    "Conditions/UT/Alignment/Modules.yml",
    "Conditions/UT/Alignment/Staves.yml",
    # RICH alignment
    "Conditions/Rich1/Alignment/Mirrors.yml",
    "Conditions/Rich1/Alignment/PDPanels.yml",
    "Conditions/Rich2/Alignment/Mirrors.yml",
    "Conditions/Rich2/Alignment/PDPanels.yml",
]

from Panoptes.calibration import standalone_rich_ref_index_calib
from PyConf.Algorithms import PrForwardTrackingVelo, PrHybridSeeding, PrMatchNN
from PyConf.Algorithms import Rich__Future__Rec__Calib__RefIndexCalib as RefIndexCalib

if noUT:
    from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT as track_maker
else:
    from RecoConf.hlt2_tracking import make_hlt2_tracks as track_maker

ckFitForm = ["AsymNormal:FreeNPol"]

# polynominal degree in background fits
nPolFull = (3, 3)

# Use running average ?
RunningAv = True

out_level = Gaudi.INFO

# Momentum Cuts
minPRads = (10 * GeV, 20 * GeV)
min_p = min(minPRads)

with (
    PrForwardTrackingVelo.bind(MinP=min_p),
    PrMatchNN.bind(MinP=min_p),
    PrHybridSeeding.bind(MinP=min_p),
    track_maker.bind(use_pr_kf=True, light_reco=True, fast_reco=True),
    RefIndexCalib.bind(
        OutputLevel=out_level,
        CreatePDFSummary=True,
        UseRunningAverage=RunningAv,
        nPolFull=nPolFull,
        RichFitTypes=(ckFitForm, ckFitForm),
    ),
    standalone_rich_ref_index_calib.bind(
        noUT=noUT, do_data_monitoring=False, set_scale_factors_to_one=False
    ),
):
    run_reconstruction(options, standalone_rich_ref_index_calib)

application = GaudiOnline.Application(
    outputLevel=options.output_level,
    partitionName=OnlineEnv.PartitionName,
    partitionID=OnlineEnv.PartitionID,
    classType=GaudiOnline.Class1,
)

# Something wrong with this, as causes configuration to silently
# abort. So comment out for now..
# application.setup_fifolog()

application.setup_mbm_access("Events", True)
flow = AlgFlowManager("EventLoop")
application.setup_hive(flow, 44)

# see https://gitlab.cern.ch/lhcb/MooreOnline/-/blob/15bc29016c0132f9a6034078471d857d568867da/MooreOnlineConf/options/online.py
# HACK: transfer options from HLTControlFlowMgr
cfm = HLTControlFlowMgr("HLTControlFlowMgr")
flow.CompositeCFNodes = cfm.CompositeCFNodes
flow.BarrierAlgNames = cfm.BarrierAlgNames
# HACK: tell the HltDecReports creator to use the online scheduler
# only works because there is exactly one instance of ExecutionReportsWriter
# ExecutionReportsWriter().Scheduler = flow

application.setup_monitoring("RichRefIndexCal")
application.monSvc.DimUpdateInterval = 1
application.config.burstPrintCount = 30000
# Mode selection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.numEventThreads = nThreads
if nThreads == 1:
    application.config.MBM_numConnections = 1
    application.config.MBM_numEventThreads = 1
    application.config.execMode = 0
else:
    application.config.MBM_numConnections = 2
    application.config.MBM_numEventThreads = 1
    application.config.execMode = 1
application.updateAndReset.saveHistograms = 1
application.updateAndReset.saverCycle = 180
application.updateAndReset.saveSetDir = "/hist/Savesets"
application.config.MBM_requests = [
    "EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0"
]

application.outputLevel = options.output_level

print("Setup complete....")

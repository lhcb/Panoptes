###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
###############################################################################
import math
import os

from ruamel.yaml import (
    YAML,
)

isQMTTest = "QMTTEST_NAME" in os.environ

# indiccator of magnet polarity and additionally -- which data
# this is optional
mp = "mu2022data"
# mp = 'md2021mc'

if isQMTTest:
    evt_max = 1000
else:
    # evt_max = 1000
    evt_max = 40000
    # evt_max = 1000000
    # evt_max = 3000000
    # evt_max = 5000000

n_kevts = str(math.trunc(evt_max / 1000)) + "k"

# addinfo = ''
# addinfo = '_' + mp + n_kevts
# addinfo = '_minp60_' + mp + n_kevts
# addinfo = '_minp60_noUT_' + mp + n_kevts
addinfo = "_minp60_noUT_R2_" + mp + n_kevts
# addinfo = '_minp60_noUT_RM2_' + mp + n_kevts
# addinfo = '_minp60_phi2_' + mp + n_kevts
# addinfo = '_minp60_phi_' + mp + n_kevts
# addinfo = '_minp60_qnt20f60_' + mp + n_kevts
# addinfo = '_minp60_qnt20f60_noUT_' + mp + n_kevts
# addinfo = '_minp60_qnt20f60_noUT_R2_' + mp + n_kevts
# addinfo = '_minp60_qnt20f60_rndmpresc_' + mp + n_kevts
# addinfo = '_minp60_subset' + mp + n_kevts
# addinfo = '_minp60_subset_noUT_' + mp + n_kevts
# addinfo = '_minp60_subset_noUT_R2_' + mp + n_kevts
# addinfo = '_minp60_subset_theta2_' + mp + n_kevts

yaml = YAML()

# retrieve the timestamp
ts_file = os.getenv("QMTTEST_NAME", "rich2_session") + "_timestamp.yml"
with open(ts_file) as inp:
    ts = yaml.load(inp)

# form the current variant name
variant = ts + "_rich2" + addinfo

# record variant and tasks names into the YAML file
v_file = os.getenv("QMTTEST_NAME", "rich2") + "_variant.yml"
with open(v_file, "w") as out:
    yaml.dump(variant, out)

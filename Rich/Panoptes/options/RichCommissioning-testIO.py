###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob
import os

from Moore import options
from Panoptes.commissioning import daq_commissioning
from RecoConf.config import run_reconstruction

partition = os.getenv("partition")
runNumber = os.getenv("runNumber")
print(partition, runNumber)
runNumber = "%010d" % int(runNumber)
if partition == "local":
    path = "/localdisk/rich_tmp_data/runs/"
else:
    path = "/hlt2/objects/%s/%s/" % (partition, runNumber)

files = sorted(glob.glob(path + "*.mdf"))
data = ["DATAFILE='PFN:" + file + "'" for file in files]

options.histo_file = "RichCommissioning.root"

options.evt_max = 100000

options.n_threads = 8

# options.conddb_tag = 'upgrade/sim-20210625-vc-md100'
options.conddb_tag = "upgrade/jonrob/rich-disable-link-for-beam-test"
options.dddb_tag = "upgrade/dddb-20210617"

options.input_files = data

options.input_type = "MDF"

options.use_iosvc = True

with daq_commissioning.bind(offlineMode=True):
    run_reconstruction(options, daq_commissioning)

# Swap out the IOSvcMM with IOSvcFileRead
from Configurables import LHCb__MDF__IOAlg, LHCb__MDF__IOSvcFileRead
from Gaudi.Configuration import allConfigurables

iosvc = LHCb__MDF__IOSvcFileRead(
    "LHCb::MDF::IOSvcFileRead",
    Input=options.input_files,
    # BufferNbEvents=20000,
    # NBanksReserve=1200,
)
for n, c in allConfigurables.items():
    if isinstance(c, LHCb__MDF__IOAlg):
        c.IOSvc = iosvc

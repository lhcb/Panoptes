###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV
from PyConf.Algorithms import PrForwardTrackingVelo, PrHybridSeeding, PrMatchNN

min_p = 30 * GeV
PrForwardTrackingVelo.global_bind(MinP=min_p)
PrMatchNN.global_bind(MinP=min_p)
PrHybridSeeding.global_bind(MinP=min_p)

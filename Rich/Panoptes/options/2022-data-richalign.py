###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Configurables import LHCbApp
from Moore import options

# This script is dd4hep only

base_dir = "/calib/align/LHCb/Rich/0000256292/"

absolutely_all_files = True

if absolutely_all_files:
    import glob

    base_dir = "/calib/align/LHCb/Rich/*/"
    dirs = glob.glob(base_dir, recursive=True)
    files = []
    for dir_i in dirs:
        files += glob.glob(dir_i + "*")
else:
    files = [os.path.join(base_dir, f) for f in os.listdir(base_dir)]

# files = files[:1000]
files = files[:25]

options.input_files = files

options.input_type = "MDF"

LHCbApp().DataType = "Upgrade"

options.simulation = False

options.conddb_tag = "master"
options.dddb_tag = "run3/trunk"

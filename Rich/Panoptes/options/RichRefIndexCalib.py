###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import multiprocessing
import os

from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Gaudi.Configuration import DEBUG, INFO, VERBOSE
from GaudiKernel.SystemOfUnits import GeV
from Moore import options
from Panoptes.calibration import standalone_rich_ref_index_calib
from PyConf.Algorithms import PrForwardTrackingVelo, PrHybridSeeding, PrMatchNN
from PyConf.Algorithms import Rich__Future__Rec__Calib__RefIndexCalib as RefIndexCalib
from RecoConf.config import run_reconstruction
from ROOT import gROOT

options.output_level = INFO

# Silence ROOT
gROOT.ProcessLine("gErrorIgnoreLevel = kWarning;")

isQMTTest = "QMTTEST_NAME" in os.environ

myName = os.getenv("QMTTEST_NAME", "RichRefIndexCalib")

options.histo_file = myName + ".root"

# Override previously set defaults
# ROOT file reader currently not thread safe :(
options.n_threads = (
    1 if options.input_type == "ROOT" or isQMTTest else multiprocessing.cpu_count()
)
# options.n_threads = 1
evtsForQMT = 1000 if options.simulation else 5000
evtsForInt = 100000 if options.simulation else 5000000
options.evt_max = evtsForQMT if isQMTTest else evtsForInt

# Extend title and ID string size in output
MessageSvcSink().HistoStringsWidth = 80

# Is UT active in this task ?
noUT = "noUT" in myName

# PDF Writer options
ckFitForm = ["AsymNormal:FreeNPol"]

# Time between periodic calibrations
calibTime = 100000 if isQMTTest else 60
# calibTime = 100000

# Fit retry period
retryPeriod = 20 if isQMTTest else 1

# Auditors (only with 1 thread)
# if not isQMTTest:
#    options.auditors += ["MemoryAuditor"]

# Enable additional monitoring algorithms ?
# do_moni = isQMTTest
do_moni = True
do_det_debug = not isQMTTest
do_mc_checking = options.simulation and do_moni
do_mirr_hists = True

# Create run by run histos in calibration algorithm
run_by_run_histos = False

# start scale factors at 1 ?
set_scale_to_one = False

# Output level for calibration alg
out_level = INFO if isQMTTest else VERBOSE

# polynominal degree in background fits
nPolFull = (3, 3)

# Use running average ?
RunningAv = not isQMTTest

# Override run number for MC samples
forcedRunN = 999999 if options.simulation else 0

# Momentum Cuts
minPRads = (10 * GeV, 20 * GeV)
min_p = min(minPRads)

# Calib at stop ?
# runCalAtStop = not isQMTTest
runCalAtStop = True

# Enable quadrant fits
QuadFits = "trunk_geom" not in myName

if noUT:
    from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT as track_maker
else:
    from RecoConf.hlt2_tracking import make_hlt2_tracks as track_maker

with (
    PrForwardTrackingVelo.bind(MinP=min_p),
    PrMatchNN.bind(MinP=min_p),
    PrHybridSeeding.bind(MinP=min_p),
    track_maker.bind(use_pr_kf=True, light_reco=True, fast_reco=True),
    RefIndexCalib.bind(
        OutputLevel=out_level,
        CreatePDFSummary=not isQMTTest,
        EnableQuadrantFits=(QuadFits, QuadFits),
        OKToPublish=not isQMTTest,
        RichFitTypes=(ckFitForm, ckFitForm),
        UseRunningAverage=RunningAv,
        AllowCalibAtStop=runCalAtStop,
        nPolFull=nPolFull,
        MinTimeBetweenCalibs=calibTime,
        FailedFitRetryPeriod=retryPeriod,
        RunByRunHists=run_by_run_histos,
        ForcedRunNumber=forcedRunN,
    ),
    standalone_rich_ref_index_calib.bind(
        noUT=noUT,
        min_p=minPRads,
        offlineMode=True,
        do_detector_debug=do_det_debug,
        do_data_monitoring=do_moni,
        do_mc_checking=do_mc_checking,
        enable_mirror_hists=do_mirr_hists,
        set_scale_factors_to_one=set_scale_to_one,
    ),
):
    run_reconstruction(options, standalone_rich_ref_index_calib)

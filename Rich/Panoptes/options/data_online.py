###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
from datetime import datetime

from PyConf.application import ApplicationOptions

filename_expr = re.compile(
    r"Run_(?P<run>\d{10})_HLT(?P<node>\d{5})_(?P<time>(?:\d{8})-(?:\d{6}))-(?P<rest>\d{3})\.mdf"
)

options = ApplicationOptions(
    _enabled=False
)  # Not sure if ApplicationOptions are the same as Moore options, so leave these here.
options.simulation = False
options.conddb_tag = "master"
options.dddb_tag = "run3/trunk"
options.input_type = "MDF"

base_dir = "/calib/align/LHCb/Rich/0000256292/"

# base_dir = "/calib/align/LHCb/Rich/0000261947/"


def sort_names(f):
    f = os.path.basename(f)
    m = filename_expr.match(f)
    if m is None:
        return m
    funs = [int, lambda t: int(datetime.strptime(t, "%Y%m%d-%H%M%S").timestamp()), int]
    return tuple(fun(m.group(k)) for fun, k in zip(funs, ("run", "time", "rest")))


absolutely_all_files = True
exclude_2022 = False
exclude_2023 = True
divide_2022_2023 = 259566

if absolutely_all_files and exclude_2022 is False and exclude_2023 is False:
    import glob

    base_dir = "/calib/align/LHCb/Rich/*/"
    dirs = glob.glob(base_dir, recursive=True)
    files = []
    for dir_i in dirs:
        files += glob.glob(dir_i + "*")
elif absolutely_all_files and exclude_2022 is True and exclude_2023 is False:
    import glob
    import re

    base_dir = "/calib/align/LHCb/Rich/0000*/"
    dirs = glob.glob(base_dir, recursive=True)
    files = []
    for dir_i in dirs:
        # Extract the number from the directory string using regular expressions
        num = int(re.search(r"/0000(\d{6})/", dir_i).group(1))
        # Check if the number is greater than or equal to 259
        if num >= divide_2022_2023:
            files += glob.glob(dir_i + "*")
elif absolutely_all_files and exclude_2022 is False and exclude_2023 is True:
    import glob
    import re

    base_dir = "/calib/align/LHCb/Rich/0000*/"
    dirs = glob.glob(base_dir, recursive=True)
    files = []
    for dir_i in dirs:
        # Extract the number from the directory string using regular expressions
        num = int(re.search(r"/0000(\d{6})/", dir_i).group(1))
        # Check if the number is greater than or equal to 259
        if num < divide_2022_2023:
            files += glob.glob(dir_i + "*")
elif absolutely_all_files and exclude_2022 is True and exclude_2023 is True:
    print("You just excluded everything")
else:
    files = [os.path.join(base_dir, f) for f in os.listdir(base_dir)]

# file_size = 0
# for file in files:
#     file_size += os.path.getsize(file)
# print(file_size)
# quit()

# files = files[:1000]
# files = files[:25]

# # files = sorted(files, key=sort_names)

# Files with latest run numbers first, presuming of course that the path naming remains /calib/align/LHCb/Rich/0000######/
files = sorted(files, reverse=True)

options.input_files = files

print(f"{len(options.input_files)} .mdf files...")
file_size = 0
for file in files:
    file_size += os.path.getsize(file)
print("file_size", file_size)

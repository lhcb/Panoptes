###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import glob
import os

from Configurables import DDDBConf, LHCbApp
from DDDB.CheckDD4Hep import UseDD4Hep
from Gaudi.Configuration import *
from GaudiConf import IOHelper

# from Gaudi.Configuration import VERBOSE, DEBUG, INFO
from GaudiKernel.SystemOfUnits import GeV
from Moore import options
from ROOT import gROOT

options.input_type = "ROOT"

LHCbApp().CondDBtag = "sim10/run3-ideal"
DDDBConf().GeometryVersion = "run3/trunk"

options.geometry_version = "run3/trunk"
options.conditions_version = "sim10/run3-ideal"

# options.input_type = "XDIGI"

# options.input_raw_format = 4.3

options.simulation = True

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"

#########################################################

InputArea = "/afs/cern.ch/work/s/seaso/DataFiles/BooleOutput/RichUpgrade/Aug2024/DD4HEP/prod/Run3-trunk/DigiFiles-TimeWindow-25/"

Input_Local_File0 = "Rich-Boole-TimeGateCut-25000-Prod-set-0-SetC-Run3-trunk-Sim10-Ideal-Aug-2024-DD4HEP-Extended.digi"
Input_Local_File1 = "Rich-Boole-TimeGateCut-25000-Prod-set-1-2-SetC-Run3-trunk-Sim10-Ideal-Aug-2024-DD4HEP-Extended.digi"
Input_Local_File2 = "Rich-Boole-TimeGateCut-25000-Prod-set-3-4-SetC-Run3-trunk-Sim10-Ideal-Aug-2024-DD4HEP-Extended.digi"

Input_Local_File3 = "Rich-Boole-TimeGateCut-25000-Prod-set-5-SetC-Run3-trunk-Sim10-Ideal-Aug-2024-DD4HEP-Extended.digi"
Input_Local_File4 = "Rich-Boole-TimeGateCut-25000-Prod-set-6-SetC-Run3-trunk-Sim10-Ideal-Aug-2024-DD4HEP-Extended.digi"
Input_Local_File5 = "Rich-Boole-TimeGateCut-25000-Prod-set-7-8-SetC-Run3-trunk-Sim10-Ideal-Aug-2024-DD4HEP-Extended.digi"
Input_Local_File6 = "Rich-Boole-TimeGateCut-25000-Prod-set-9-10-SetC-Run3-trunk-Sim10-Ideal-Aug-2024-DD4HEP-Extended.digi"

Input_Local_File7 = "Rich-Boole-TimeGateCut-25000-Prod-set-11-12-SetC-Run3-trunk-Sim10-Ideal-Nov-2024-DD4HEP-Extended.digi"
Input_Local_File8 = "Rich-Boole-TimeGateCut-25000-Prod-set-13-15-16-SetC-Run3-trunk-Sim10-Ideal-Nov-2024-DD4HEP-Extended.digi"

# print("InputFiles ",Input_Local_File0," ",Input_Local_File1," ",Input_Local_File2," ",Input_Local_File3,"  ",Input_Local_File4,"  ",Input_Local_File5,"  ",Input_Local_File6)
# print("InputFiles ",Input_Local_File7," ",Input_Local_File8)

options.input_files = [
    InputArea + Input_Local_File0,
    InputArea + Input_Local_File1,
    InputArea + Input_Local_File2,
    InputArea + Input_Local_File3,
    InputArea + Input_Local_File4,
    InputArea + Input_Local_File5,
    InputArea + Input_Local_File6,
    InputArea + Input_Local_File7,
    InputArea + Input_Local_File8,
]

#########################################################

options.evt_max = -1

options.print_freq = 10

############################################################
# Configuring the detectors for reconstruction.

# Define test name
# myName = os.getenv("QMTTEST_NAME", "UNDEFINED")

# Due to issues with hit sorting in FT disable Mat corrections
# See for more details https://gitlab.cern.ch/lhcb/Panoptes/-/issues/7
from PyConf.Algorithms import PrStoreSciFiHits

PrStoreSciFiHits.global_bind(ApplyMatContractionCalibration=False)
# To be removed once the issue is resolved properly.

from PyConf.Algorithms import PrForwardTrackingVelo, PrHybridSeeding, PrMatchNN

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hep = DD4hepSvc(
        DetectorList=[
            "/world",
            "Magnet",
            "VP",
            "FT",
            "UT",
            "Rich1",
            "Rich2",
            "Ecal",
            "Hcal",
        ]
    )

    # if not noUT: dd4hep.DetectorList += ["UT"]

################################################################
# Activate tracking

importOptions("$PANOPTESROOT/options/Tracking.py")

import multiprocessing
import os

from Gaudi.Configuration import DEBUG, INFO, VERBOSE
from GaudiKernel.SystemOfUnits import GeV
from Moore import options, run_reconstruction
from Panoptes.BrahmaSutraConfiguration import BrahmaSutraConf
from ROOT import gROOT

options.output_level = INFO
# options.output_level = DEBUG

# Silence ROOT
gROOT.ProcessLine("gErrorIgnoreLevel = kWarning;")

# isQMTTest = 'QMTTEST_NAME' in os.environ
# myName = os.getenv("QMTTEST_NAME", "RichMCRecon")
myOutputArea = "/afs/cern.ch/work/s/seaso/DataFiles/PanoptesOutput/Dec10-2024/prod/"
myLocalName = "Rich_MC_Recon_Example_DataSet_0_16"
myLocalNtupleName = "Rich_MC_ReconNtuple_Example_DataSet_0_16"

options.histo_file = myOutputArea + myLocalName + ".root"
options.ntuple_file = myOutputArea + myLocalNtupleName + ".root"

# Override previously set defaults
options.n_threads = 1

# Is UT active in this task ?
# noUT = ("noUT" in myName)

# Override run number for MC samples
forcedRunN = 999999 if options.simulation else 0

# Momentum Cuts

minPRads = (2 * GeV, 2 * GeV, 2 * GeV)
min_p = min(minPRads)

# Enable additional monitoring algorithms ?
# do_moni = isQMTTest
do_moni = True

# Output level for photon counting alg
# out_level = (INFO if isQMTTest else VERBOSE)
out_level = INFO
options.output_level = INFO
# Silence ROOT
gROOT.ProcessLine("gErrorIgnoreLevel = kWarning;")

from RecoConf.hlt2_tracking import make_hlt2_tracks as track_maker

with (
    PrForwardTrackingVelo.bind(MinP=min_p),
    PrMatchNN.bind(MinP=min_p),
    track_maker.bind(use_pr_kf=True, light_reco=True, fast_reco=False),
    BrahmaSutraConf.bind(do_data_monitoring=do_moni, ParticleGun=False),
):
    run_reconstruction(options, BrahmaSutraConf)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from DDDB.CheckDD4Hep import UseDD4Hep

# Define test name
myName = os.getenv("QMTTEST_NAME", "UNDEFINED")

# Is UT active in this task ?
noUT = "noUT" in myName

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "VP", "FT", "Rich1", "Rich2"])
    if not noUT:
        dd4hep.DetectorList += ["UT"]

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from ruamel.yaml import YAML

yaml = YAML()
itNr = 0
it_num_file = os.getenv("QMTTEST_NAME", "rich1") + "_iter_numer.yml"
with open(it_num_file, "w") as fout:
    yaml.dump(itNr, fout)

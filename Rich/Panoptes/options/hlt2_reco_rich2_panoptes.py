###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob
import math

from ruamel.yaml import YAML

yaml = YAML()

from GaudiKernel.SystemOfUnits import (
    GeV,
)
from Moore import (
    options,
    run_reconstruction,
)
from Panoptes.alignment import (
    standalone_rich_mirror_align_reco,
)

"""Options for running over data with FT raw bank version 6."""
from RecoConf.decoders import (
    default_ft_decoding_version,
)
from RecoConf.rich_data_monitoring import (
    alignment_rich_monitoring_options,
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import (
    default_rich_reco_options,
)

options.n_threads = 10
# options.n_threads = 40

options.input_files = sorted(
    [
        "mdf:root://eoslhcb.cern.ch/%s" % f
        for f in glob.glob(
            r"/eos/lhcb/grid/prod/lhcb/MC/Upgrade/MDF/00146082/000*/00146082_*"
        )
    ]
)
options.input_type = "MDF"
options.dddb_tag = "dddb-20201211"
options.conddb_tag = "sim-20201218-vc-md100"

if "md" in options.conddb_tag:
    mp = "md"
elif "mu" in options.conddb_tag:
    mp = "mu"

options.evt_max = 100
# options.evt_max = 40000
# options.evt_max = 1000000
# options.evt_max = 2000000
# options.evt_max = 3000000
# options.evt_max = 5000000

n_kevts = str(math.trunc(options.evt_max / 1000)) + "k"

# addinfo = '_minp60_'+mp+n_kevts
# addinfo = '_minp60_phi_'+mp+n_kevts
# addinfo = '_minp60_qnt20f60_'+mp+n_kevts
addinfo = "_minp60_qnt20f60_rndmpresc_" + mp + n_kevts
# addinfo = '_minp60_phi2_'+mp+n_kevts
# addinfo = '_'+mp+n_kevts
# addinfo = '_minp60_subset_'+mp+n_kevts
# addinfo = '_minp60_subset_theta2_'+mp+n_kevts
# addinfo = ''

rich = "rich2"
radiator = "Rich2Gas"

mirror_align_tasks = [
    "Produce",  # fill the production set of histograms
    #'Monitor',       # add various checking histograms
    #'MapExlore',     # explore tracks and photons for the HLT1 pre-selection line "map"
    #'MapConstruct',  # construct exploratory data structure for the HLT1 pre-selection line "map"
    #'Map',           # create data structure to be used in the HLT1 selection line with entire contents
    #'MapUse',        # usa the "map" data structure a la in the HLT1 pre-selection line "map"
    #'MapPhi',        # create data structure to be used in the HLT1 selection line with contents in quantiles
    #'MapPhiUse',     # use the "mapPhi" data structure in the HLT1 pre-selection line "mapPhi"
    #'AllFillCount',  # add counters for optimization of the RICH2 mirror combinations subset
    #'AllFillPhi',    # fill 1D phi histos for optimization of the RICH2 mirror combinations subset
    #'CheckRestFill', # check filling the rest of RICH2 mirror combinations along with 8 poorest
    #'SkipFilled',    # check filling all RICH2 mirror combinations with skipping when filled
    #'CoordSysTrans', # explore influence of RICH1 coordinate systems on distribution shapes
]

default_moni_opts = {}
default_moni_opts_l2 = {}

default_reco_opts = {}

default_reco_opts.update({"PhotonSelection": "None"})

align_opts = {}

# yapf: disable
if 'subset' in addinfo:
    central_mirr_combs = {
        "PrebookHistos": [
            ('p17', 's13'), ('p47', 's35'),
            ('p17', 's09'), ('p47', 's31'),
            ('p16', 's12'), ('p46', 's34'),
            ('p16', 's08'), ('p46', 's30'),
            ('p13', 's10'), ('p43', 's31'),
            ('p13', 's09'), ('p43', 's30'),
            ('p12', 's09'), ('p42', 's30'),
            ('p12', 's08'), ('p42', 's29'),
            ('p09', 's09'), ('p39', 's31'),
            ('p09', 's05'), ('p39', 's27'),
            ('p08', 's08'), ('p38', 's30'),
            ('p08', 's04'), ('p38', 's26'),
        ],
    }
    align_opts.update(central_mirr_combs)

if '_phi' in addinfo:
    optimal_mirr_combs = {
        "PrebookHistos": [
            ('p27', 's19'), ('p55', 's39'),
            ('p26', 's18'), ('p54', 's38'),
            ('p25', 's17'), ('p53', 's37'),
            ('p24', 's16'), ('p52', 's36'),
            ('p23', 's15'), ('p51', 's35'),
            ('p23', 's19'), ('p51', 's39'),
            ('p22', 's14'), ('p51', 's38'),
            ('p21', 's13'), ('p50', 's34'),
            ('p21', 's17'), ('p50', 's37'),
            ('p21', 's18'), ('p49', 's33'),
            ('p20', 's12'), ('p49', 's36'),
            ('p20', 's16'), ('p48', 's32'),
            ('p19', 's15'), ('p47', 's35'),
            ('p19', 's11'), ('p47', 's31'),
            ('p18', 's14'), ('p46', 's34'),
            ('p18', 's10'), ('p46', 's30'),
            ('p17', 's13'), ('p45', 's33'),
            ('p17', 's09'), ('p45', 's29'),
            ('p16', 's12'), ('p44', 's32'),
            ('p16', 's08'), ('p44', 's28'),
            ('p15', 's11'), ('p43', 's30'),
            ('p14', 's10'), ('p43', 's31'),
            ('p14', 's11'), ('p42', 's30'),
            ('p13', 's09'), ('p42', 's29'),
            ('p13', 's10'), ('p41', 's29'),
            ('p12', 's09'), ('p41', 's28'),
            ('p12', 's08'), ('p40', 's28'),
            ('p11', 's07'), ('p39', 's31'),
            ('p11', 's11'), ('p39', 's27'),
            ('p10', 's06'), ('p38', 's26'),
            ('p10', 's10'), ('p38', 's30'),
            ('p09', 's05'), ('p37', 's25'),
            ('p09', 's09'), ('p37', 's29'),
            ('p08', 's08'), ('p36', 's24'),
            ('p08', 's04'), ('p36', 's28'),
            ('p07', 's07'), ('p35', 's27'),
            ('p07', 's03'), ('p35', 's23'),
            ('p06', 's06'), ('p34', 's26'),
            ('p05', 's05'), ('p34', 's22'),
            ('p05', 's02'), ('p34', 's21'),
            ('p04', 's04'), ('p33', 's25'),
            ('p04', 's00'), ('p32', 's24'),
            ('p04', 's01'), ('p32', 's20'),
            ('p03', 's03'), ('p31', 's23'),
            ('p02', 's02'), ('p30', 's22'),
            ('p01', 's01'), ('p29', 's21'),
            ('p00', 's00'), ('p28', 's20'),
        ],
    }
    align_opts.update(optimal_mirr_combs)

if 'qnt20f60' in addinfo:
    optimal_mirr_combs = {
        "PrebookHistos": [
                ('p27', 's19'), ('p55', 's39'),
                ('p26', 's18'), ('p54', 's38'),
                ('p25', 's17'), ('p53', 's37'),
                ('p24', 's16'), ('p52', 's36'),
                ('p23', 's15'), ('p51', 's35'),
                ('p23', 's19'), ('p51', 's39'),
                ('p22', 's14'), ('p51', 's38'),
                ('p21', 's13'), ('p50', 's34'),
                ('p21', 's17'), ('p50', 's37'),
                ('p21', 's18'), ('p49', 's33'),
                ('p20', 's12'), ('p49', 's36'),
                ('p20', 's16'), ('p48', 's32'),
                ('p19', 's15'), ('p47', 's35'),
                ('p19', 's11'), ('p47', 's31'),
                ('p18', 's14'), ('p46', 's34'),
                ('p18', 's10'), ('p46', 's30'),
                ('p17', 's13'), ('p45', 's33'),
                ('p17', 's09'), ('p45', 's29'),
                ('p16', 's12'), ('p44', 's32'),
                ('p16', 's08'), ('p44', 's28'),
                ('p15', 's11'), ('p43', 's30'),
                ('p14', 's10'), ('p43', 's31'),
                ('p14', 's11'), ('p42', 's30'),
                ('p13', 's09'), ('p42', 's29'),
                ('p13', 's10'), ('p41', 's29'),
                ('p12', 's09'), ('p41', 's28'),
                ('p12', 's08'), ('p40', 's28'),
                ('p11', 's07'), ('p39', 's31'),
                ('p11', 's11'), ('p39', 's27'),
                ('p10', 's06'), ('p38', 's26'),
                ('p10', 's10'), ('p38', 's30'),
                ('p09', 's05'), ('p37', 's25'),
                ('p09', 's09'), ('p37', 's29'),
                ('p08', 's08'), ('p36', 's24'),
                ('p08', 's04'), ('p36', 's28'),
                ('p07', 's07'), ('p35', 's27'),
                ('p07', 's03'), ('p35', 's23'),
                ('p06', 's06'), ('p34', 's26'),
                ('p05', 's05'), ('p34', 's22'),
                ('p05', 's02'), ('p34', 's21'),
                ('p04', 's04'), ('p33', 's25'),
                ('p04', 's00'), ('p32', 's24'),
                ('p04', 's01'), ('p32', 's20'),
                ('p03', 's03'), ('p31', 's23'),
                ('p02', 's02'), ('p30', 's22'),
                ('p01', 's01'), ('p29', 's21'),
                ('p00', 's00'), ('p28', 's20'),
        ],
    }
    align_opts.update(optimal_mirr_combs)
# yapf: enable

if "minp60" in addinfo:
    align_opts.update({"MinP4Align": 60.0 * GeV})

if "theta2" in addinfo:
    align_opts.update({"DeltaThetaRange": 0.002})

if not "_phi" in addinfo:
    if "40k" in addinfo:
        align_opts.update({"PoorestPopulation": 10.0})
    elif "1000k" in addinfo:
        align_opts.update({"PoorestPopulation": 341.0})
    elif "5000k" in addinfo:
        align_opts.update({"PoorestPopulation": 2688.0})

if "_phi" in addinfo:
    if "40k" in addinfo:
        align_opts.update({"PoorestPopulation": 1.0})
    elif "1000k" in addinfo:
        align_opts.update({"PoorestPopulation": 20.0})
    elif "5000k" in addinfo:
        align_opts.update({"PoorestPopulation": 113.0})

if "qnt20f60" in addinfo:
    if "5000k" in addinfo:
        align_opts.update({"PoorestPopulation": 310.0})

if "_phi2" in addinfo:
    align_opts.update({"MinUsefulTracks": 2})

with open(rich + "_session_timestamp.yml") as inp:
    ts = yaml.load(inp)

current_variant = ts + "_" + rich + addinfo
variant = {"Variant": current_variant}
align_opts.update(variant)

with open(rich + "_variant.yml", "w") as out:
    yaml.dump(current_variant, out)

align_opts_dump = alignment_rich_monitoring_options(
    radiator=radiator, align_rich_moni_opts_modify=align_opts
)

align_opts_dump.update(
    default_rich_monitoring_options(
        default_rich_moni_opts_modify=default_moni_opts,
        default_rich_moni_opts_modify_level2=default_moni_opts_l2,
    )
)

opts_new = {}
opts_new[ts] = {}
opts_new[ts]["reco_opts"] = align_opts_dump

with open(rich + "_reco_opts.yml", "a") as out:
    yaml.dump(opts_new, out)

# cumulative substring with all tasks chosen
tasks = ""
for task in mirror_align_tasks:
    if task != "Produce":
        tasks += "_" + task.lower()

# itertation number substring
with open(rich + "_iter_number.yml") as inp:
    iN = "_i" + str(yaml.load(inp))

# output root file with histograms
options.histo_file = current_variant + tasks + "_histos" + iN + ".root"

with (
    default_ft_decoding_version.bind(value=6),
    default_rich_reco_options.bind(default_rich_reco_opts_modify=default_reco_opts),
    default_rich_monitoring_options.bind(
        default_rich_moni_opts_modify=default_moni_opts,
        default_rich_moni_opts_modify_level2=default_moni_opts_l2,
    ),
    alignment_rich_monitoring_options.bind(
        radiator=radiator, align_rich_moni_opts_modify=align_opts
    ),
    standalone_rich_mirror_align_reco.bind(
        RichGas=radiator, MirrorAlignTasks=mirror_align_tasks, noUT=True
    ),
):
    run_reconstruction(options, standalone_rich_mirror_align_reco)

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

from GaudiKernel.SystemOfUnits import GeV
from PyConf.Algorithms import (
    PrForwardTrackingVelo,
    PrHybridSeeding,
    PrMatchNN,
    VeloRetinaClusterTrackingSIMD,
    VPRetinaFullClusterDecoder,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_velo_full_clusters,
    make_VeloClusterTrackingSIMD,
)

hltSettings = "UNKNOWN"
try:
    import OnlineEnvBase

    hltSettings = OnlineEnvBase.HLTType
    print("OnlineEnvBase.HLTType=", hltSettings)
except ImportError:
    hltSettings = os.getenv("QMTTEST_NAME", "UNKNOWN")
    print("OnlineEnvBase not available. Assumming hltSettings=", hltSettings)

if not "veloSP" in hltSettings:
    print("Configuring for Velo cluster F/W")
    make_VeloClusterTrackingSIMD.global_bind(algorithm=VeloRetinaClusterTrackingSIMD)
    make_velo_full_clusters.global_bind(make_full_cluster=VPRetinaFullClusterDecoder)
else:
    print("Configuring for Velo SuperPixel F/W")

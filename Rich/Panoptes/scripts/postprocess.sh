###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# hltrx.sh
# Markus Frank & Niko Neufeld
# Simple script to run the event builder chain stand-alone
# Requires CMTPROJECTPATH set and setup.sh sourced in $ONLINETASKSROOT/cmt
#
#
if [ -z DIM_DNS_NODE ]; then
	echo "Please define DIM_DNS_NODE"
	exit 1
fi

 #message are send via the DIM Messageservice
export RICHLOG=/var/tmp/rich/log

# export MSGSVC=MessageSvc
# export MSGSVC=LHCb::DimMessageSvc
export MSGSVC=LHCb::FmcMessageSvc

export test_exe=${ONLINEKERNELROOT}/${CMTCONFIG}/test.exe
export gaudi_run="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineStart "
export gaudi_exe="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineTask -msgsvc=${MSGSVC} -auto"
export gaudi_exe2="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineTask -msgsvc=MessageSvc -auto"


export UTGID=RichPost ; ${gaudi_exe} -opt=$RICHMONITORINGSYSROOT/options/RichOnlineAna2.opts -main=$GAUDIONLINEROOT/options/Main.opts # 1>${RICHLOG}/${UTGID}2.log 2>&1 &

## Convert magnification factors to YAML
### Introduction
# This tool is created to convert a directory containing text files with magnetisation factors for either RICH1 or RICH2 to a yaml object. The yaml object will be written to the file _RichXMirrMagnFactors_ where _X_ refers to the specific RICH detector (1 or 2) and stored in the inputted directory.
### Requirements
# The tool requires a working version of Python 3 with additional packages:
# * ruamel
### CONFIGURATION AND USE
# To use the Conversion tool download mag_parse.py and note the location. Whenever the tool is required is a python script simply add the location to path and import the file. Initiate an instance of the MagConvert class (as shown below) and call the to_yaml method. Pass the absolute path to the directory containing magnification factors and the file _RichXMirrMagnFactors.yml_ will be added to the directory (_X_ refers to the detector number).
#
# **Example:** Importing map_parse and converting the directory _magnification_ which contains magnification factors for RICH 1:
#
#    import mag_parse
#
#    convert = MagConvert()
#    convert.to_yaml("<Path to magnification>/magnification")
#
# This would result in the file _Rich1MirrMagnFactors.yml_ being added to _magnification_.

###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Vidar Marsh, Paras Naik"
__copyright__ = "University of Bristol, August 2021"

import os
import re
import sys

import ruamel.yaml

yaml = ruamel.yaml.YAML()


class convert_magnFactors_txt_YAML:
    def __init__(self):
        pass

    def to_yaml(mag_dir_path, n_it=100):  # n_it = 100 means predefined
        """
        Takes a directory of txt files containing magnification factors
        and stores them in a hashmap that is parsed to yaml. The
        resulting file is stored in the inputted directory as
        """
        # add mag_dir_path to path
        sys.path.append(mag_dir_path)

        # Iterate through magnification factors directory and read each text
        # file. For each file in directory, check that it contains magnification
        # factors by splitting the filename and checking against known
        # parameters. Contents of filename are given by: which_rich, refers
        # to the detector number; rot_mirror, refers to whether the primary
        # of secondary mirror was rotated; rot_axis, refers to the axis of
        # rot the mirror was rotated about and which direction. If any of these don't match
        # use_file is set to False.
        # Variable mag_num is used to track the file number that data is taken
        # from. This is used to check that all files are from the same RICH
        # detector.
        mag_num = 0
        for filename in os.listdir(mag_dir_path):
            if (
                "magnifs" in filename
                and (f"_i{n_it}" in filename or "_predefined" in filename)
                and ("pri" in filename or "sec" in filename)
            ):
                # Extract substring with "rich" in it
                which_rich = re.search(r"_([^_]*rich[^_]*)_", filename).group(1)
                # Extract first three letters of substring with "pri" or "sec" in it
                pri_sec_substring = re.search(
                    r"_([^_]*(pri|sec)[^_]*)_", filename
                ).group(1)
                rot_mirr = pri_sec_substring[:3]
                # Extract last two letters of substring with "pri" or "sec" in it
                rot_axis = pri_sec_substring[-2:]
            else:
                continue
            # Try to determine the RICH detector. The first instance of a
            # valid the detector name is assigned to "rich_detector".
            if mag_num == 0:
                if which_rich in ("rich1", "rich2"):
                    rich_detector = which_rich
                else:
                    continue
            else:
                if which_rich != rich_detector:
                    continue
            if rot_mirr not in ("pri", "sec"):
                continue
            elif rot_axis not in ("Yn", "Yp", "Zn", "Zp"):
                continue
            # Create hashmap under a key given by the name of the rich detector.
            # Create two subsequent layers to sort the mirror
            # rotation types.
            if mag_num == 0:
                mag_map = {
                    rich_detector: {
                        "pri": {"Yn": {}, "Yp": {}, "Zn": {}, "Zp": {}},
                        "sec": {"Yn": {}, "Yp": {}, "Zn": {}, "Zp": {}},
                    }
                }
            mag_num += 1
            # read contents of file line by line
            file_path = os.path.sep.join((mag_dir_path, filename))
            with open(file_path, "r") as f:
                lines = f.readlines()
            # split (and strip) data to obtain individual data points
            for line in lines:
                # Check that the line is non-empty
                line = line.strip()
                if line == "":
                    continue
                datapoints = line.split()
                pri_mirr = datapoints[0][:3]
                sec_mirr = datapoints[0][3:]
                y_rot = datapoints[1]
                z_rot = datapoints[2]
                try:
                    mag_map[rich_detector][rot_mirr][rot_axis][pri_mirr]
                except:
                    mag_map[rich_detector][rot_mirr][rot_axis][pri_mirr] = {}
                try:
                    mag_map[rich_detector][rot_mirr][rot_axis][pri_mirr][sec_mirr]
                except:
                    mag_map[rich_detector][rot_mirr][rot_axis][pri_mirr][sec_mirr] = (
                        None
                    )
                mag_map[rich_detector][rot_mirr][rot_axis][pri_mirr][sec_mirr] = [
                    float(y_rot),
                    float(z_rot),
                ]
        # Create YAML file
        newfile = rich_detector + f"_MirrMagnFactors_predefined.yml"
        if n_it < 99:
            newfile = rich_detector + f"_MirrMagnFactors_i{n_it}.yml"
        newfile_path = os.path.sep.join((mag_dir_path, newfile))
        # while os.path.isfile(newfile_path) == True:
        #    to_newfile, newfile_name = os.path.split(newfile_path)
        #    newfile_name = "new" + newfile_name
        #   newfile_path = os.path.sep.join([to_newfile, newfile_name])
        with open(newfile_path, "w") as f:
            yaml.dump((mag_map), f)

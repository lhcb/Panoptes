###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import getpass
import glob
import os
import re
import sys
from datetime import datetime
from itertools import cycle
from pathlib import Path

import numpy as np
from MooreOnlineConf.utils import (  # may need to check for online user in offline scenario, let's see
    alignment_options,
    distribute_files,
)
from PyConf.application import ApplicationOptions

filename_expr = re.compile(
    r"Run_(?P<run>\d{10})_HLT(?P<node>\d{5})_(?P<time>(?:\d{8})-(?:\d{6}))-(?P<rest>\d{3})\.mdf"
)

options = ApplicationOptions(
    _enabled=False
)  # Not sure if ApplicationOptions are the same as Moore options, so leave these here.
options.simulation = False
options.conddb_tag = "master"
options.dddb_tag = "run3/trunk"
options.input_type = "MDF"

base_dir = "/calib/align/LHCb/Rich1/0000289890/"


def sort_names(f):
    f = os.path.basename(f)
    m = filename_expr.match(f)
    if m is None:
        return m
    funs = [int, lambda t: int(datetime.strptime(t, "%Y%m%d-%H%M%S").timestamp()), int]
    return tuple(fun(m.group(k)) for fun, k in zip(funs, ("run", "time", "rest")))


select_runs_TESTBENCH = True  # use this option and lines near the bottom of this file to select runs in the testbench

import glob

base_dir = "/calib/align/LHCb/Rich1/*/"
dirs = glob.glob(base_dir, recursive=True)
files = []
for dir_i in dirs:
    files += glob.glob(dir_i + "*")

# Only select 2024 data
files_24 = []
for file in files:
    file_id = file.split("/")[-2]
    if int(file_id[4:]) >= 287743:
        files_24.append(file)
files = files_24

# Files with latest run numbers first, presuming of course that the path naming remains /calib/align/LHCb/Rich/0000######/
files = sorted(files, reverse=True)

options.input_files = files

if "online" in getpass.getuser():
    import OnlineEnvBase as OnlineEnv

    online_options = alignment_options(OnlineEnv)
    if (
        OnlineEnv.PartitionName != "TESTALIGNMENT"
    ):  # usually the case for the Run Control
        print("not on TESTALIGNMENT partition")
        sys.stdout.flush()
        print(f"online_options.runs {online_options.runs}")
        sys.stdout.flush()
        INPUT_DATA_PATH = Path(f"/calib/align/LHCb/Rich1/")
        files = [
            sorted((INPUT_DATA_PATH / run).iterdir())
            for run in online_options.runs
            if (INPUT_DATA_PATH / run).exists()
        ]

    else:  # testbench, but we never use it from the online account, so we should never get here
        print("on TESTALIGNMENT partition")
        INPUT_DATA_PATH = Path("input_data")
        files = [sorted(INPUT_DATA_PATH.iterdir())]
    print(f"files {files}")
    sys.stdout.flush()
    print(f"{sum(len(sublist) for sublist in files)} .mdf files...")
    sys.stdout.flush()
    file_size = 0
    for file in [item for sublist in files for item in sublist]:
        file_size += os.path.getsize(file)
    print("file_size", file_size)
    sys.stdout.flush()
    print(f"online_options.nodes {online_options.nodes}")
    sys.stdout.flush()

    def get_file_size(file_path):
        return os.path.getsize(file_path)

    def my_split(arr, n):
        out = [[] for _ in range(n)]
        for v, l in zip(arr, cycle(out)):
            l.append(v)
        return out

    def distribute_files_alt(nodes, files):
        total_files = sum(files, [])  # Flatten the files into a single list
        sorted_files = sorted(total_files, key=get_file_size)
        files_per_node = my_split(sorted_files, len(nodes))
        return dict(zip(nodes, files_per_node))

    files_per_node = distribute_files_alt(online_options.nodes, files)
    print(f"files_per_node {files_per_node}")
    sys.stdout.flush()

    utgid = os.environ["UTGID"]
    worker_id = utgid.split("_")[1]
    try:
        input_files = files_per_node[worker_id]
    except KeyError:
        # When testing we run multiple instances on the same node
        # TODO this should probably be done based on the partition name
        #      and also "nodes" should be renamed to workers everywhere.
        print("Trying multiple instances on same node")  # may not work for mirrAlign
        worker_id = utgid
        input_files = files_per_node[worker_id]

    # TODO handle case where input_files is empty (here and in iterator)
    input_files = [str(f) for f in input_files]
    print(f"input_files {input_files}")
    sys.stdout.flush()
    options.input_files = input_files

if select_runs_TESTBENCH and "online" not in getpass.getuser():
    # select_run_numbers = [28985, 28986, 28987]
    # select_run_numbers = "290034"
    # select_run_numbers = [299620,299673,299681]
    select_run_numbers = [299620]
    if not isinstance(select_run_numbers, list):
        select_run_numbers = [select_run_numbers]
    options.input_files = [
        x
        for x in options.input_files
        if any(str(run_number) in x for run_number in select_run_numbers)
    ]

print(f"{len(options.input_files)} .mdf files...")

sys.stdout.flush()
file_size = 0
for file in options.input_files:
    file_size += os.path.getsize(file)
print("file_size/1024*1024:", file_size / (1024 * 1024), "in MB")
sys.stdout.flush()

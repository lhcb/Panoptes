# /group/rich/sw/alignment/stack/Panoptes/run python /group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/python/PyMirrAlignOnline/unbiased_fitter.py
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import csv
import json
import os
import random
import re
import subprocess
import sys

import numpy as np
from GaudiPython import gbl
from ROOT import (
    TF1,
    TH1D,
    TH2D,
    TCanvas,
    TExec,
    TFile,
    TGraph,
    TLine,
    TMultiGraph,
    gROOT,
    gStyle,
)
from scipy.optimize import minimize

# Set batch mode so histograms aren't printed
# sys.argv.append('-b')
gROOT.SetBatch(True)

parser = argparse.ArgumentParser(description="Run optimisation on RICH panel alignment")
parser.add_argument(
    "--whichRich", type=int, choices=[1, 2], help="Which RICH panel to optimise"
)
args = parser.parse_args()

whichRich = int(args.whichRich)

whichPanel = "both"
prepath = os.getcwd()


class AlignMonitor:
    def __init__(
        self,
        whichRich,
        fitrange=0.0055,
        #            _alignConf,
        #            _maxIt,
    ):
        """
        self.alignConf = _alignConf
        self.nameStr = self.alignConf.getProp('nameStr')
        self.whichRich = self.alignConf.getProp('Rich')
        self.displayMode = self.alignConf.getProp('displayMode')
        self.warningFactor = self.alignConf.getProp('warningFactor')
        self.maxIt = _maxIt  # the maximum labeled number of the iterations that we are looping through
        """
        self.workdir = "./"
        self.whichRich = whichRich
        self.maxIt = 0
        self.prename = f"{prepath}/rich{whichRich}_opt_dd4hep_p{whichPanel}.root"

        # RICH1 settings
        self.maxpri = 3
        self.binToPriMirr = [[2, 3], [0, 1]]
        self.maxsec = 15
        self.binToSecMirr = [
            [10, 11, 14, 15],
            [8, 9, 12, 13],
            [2, 3, 6, 7],
            [0, 1, 4, 5],
        ]
        self.fitMin = -fitrange
        self.fitMax = fitrange
        # if RICH2, use RICH2 settings
        if self.whichRich == 2:
            self.maxpri = 55
            self.binToPriMirr = [
                [3, 2, 1, 0, 31, 30, 29, 28],
                [7, 6, 5, 4, 35, 34, 33, 32],
                [11, 10, 9, 8, 39, 38, 37, 36],
                [15, 14, 13, 12, 43, 42, 41, 40],
                [19, 18, 17, 16, 47, 46, 45, 44],
                [23, 22, 21, 20, 51, 50, 49, 48],
                [27, 26, 25, 24, 55, 54, 53, 52],
            ]
            self.maxsec = 39
            self.binToSecMirr = [
                [3, 2, 1, 0, 23, 22, 21, 20],
                [7, 6, 5, 4, 27, 26, 25, 24],
                [11, 10, 9, 8, 31, 30, 29, 28],
                [15, 14, 13, 12, 35, 34, 33, 32],
                [19, 18, 17, 16, 39, 38, 37, 36],
            ]
            # Original fit range
            self.fitMin = -fitrange
            self.fitMax = fitrange
            # self.fitMin = -0.0025
            # self.fitMax = 0.0025
        self.isInsane = False
        self.finalCKres = None
        self.finalCKresErr = None
        self.finalCKgaussianmean = None

    def performMonitoring(self, p, hist_path=""):
        import os

        gStyle.SetOptFit(1111)
        gStyle.SetOptStat(000000000)

        if hist_path == "":
            pdf_suff = ""
        else:
            pdf_suff = "_" + hist_path.split("/")[-1]

        # File for the RICH piquet AND for the RICH mirror alignment experts
        expertFile = f"TESTING.pdf"
        # File for the Alignment piquet ("duplicating" what goes to the Presenter for the Data Manager [except CK angle res stuff])
        monitorFile = self.workdir + f"Rich{self.whichRich}_AlignMonitor.pdf"
        # File with for testing purposes
        testingFile = self.workdir + f"Rich{self.whichRich}_AlignTesting.pdf"
        # Data Manager can only get histograms sent via self.monSvc

        theCanvas = TCanvas("theCanvas", "MirrAlign")
        theCanvas.Divide(2, 2, 0.001, 0.001)

        theCanvas2 = TCanvas("theCanvas2", "MirrAlignTest")
        theCanvas2.Divide(2, 2, 0.001, 0.001)

        theCanvas2.Clear()
        theCanvas.Clear()

        # generate resHistograms

        if self.maxIt == 0:
            pass
        elif self.maxIt < 2:
            theCanvas.Divide(2, 1, 0.0025, 0.0025)
        elif self.maxIt < 3:
            theCanvas.Divide(3, 1, 0.0025, 0.0025)
        elif self.maxIt < 4:
            theCanvas.Divide(2, 2, 0.0025, 0.0025)
        elif self.maxIt < 6:
            theCanvas.Divide(3, 2, 0.0025, 0.0025)
        elif self.maxIt < 8:
            theCanvas.Divide(4, 2, 0.0025, 0.0025)
        elif self.maxIt < 9:
            theCanvas.Divide(3, 3, 0.0025, 0.0025)
        else:
            theCanvas.Divide(4, 4, 0.0025, 0.0025)

        # reserve spaces for resHistograms
        resHistograms = [None] * (self.maxIt + 1)
        polbkg = [None] * (self.maxIt + 1)
        histFunc = [None] * (self.maxIt + 1)
        hist = [None] * (self.maxIt + 1)
        filename = [None] * (self.maxIt + 1)
        thisFile = [None] * (self.maxIt + 1)
        title = [None] * (self.maxIt + 1)
        fitRes = [None] * (self.maxIt + 1)

        # define resHistoTrend
        resHistoTrend = TH1D(
            "resHistoTrend",
            "RICH"
            + str(self.whichRich)
            + " Cherenkov angle resolution (mrad) per It. ",
            self.maxIt + 1,
            -0.5,
            self.maxIt + 0.5,
        )

        for j in reversed(range(0, self.maxIt + 1)):
            filename[j] = self.prename
            if os.path.exists(filename[j]):
                thisFile[j] = TFile(filename[j], "read")

                if hist_path == "":
                    hist[j] = (
                        thisFile[j].Get(
                            f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                        )
                    ).Clone()
                else:
                    hist[j] = (thisFile[j].Get(f"{hist_path}")).Clone()

                if (j == 0) and (j == self.maxIt):
                    hist[j].SetName("resHisto0")
                    hist[j].SetName("resHistoN")
                elif j == self.maxIt:
                    hist[j].SetName("resHistoN")
                elif j == 0:
                    hist[j].SetName("resHisto0")
                if j != 0:
                    hist[j].SetName("resHisto" + str(j))

                # perform the fit and extract fit models
                fitRes[j], resHistograms[j] = self.fitCherenkovAngle(hist[j])

                histFunc[j] = fitRes[j].overallFitFunc
                polbkg[j] = fitRes[j].bkgFitFunc

                try:
                    ckres = histFunc[j].GetParameter(2) * 1000
                except:
                    print(
                        f"WARNING: Fit failed (NEntries = {hist[j].GetEntries()}). Setting fit results to large numbers and continuing."
                    )

                    if j == self.maxIt:
                        self.finalCKres = 100
                        self.finalCKresErr = 100
                        self.finalCKgaussianmean = 100

                    resHistoTrend.SetBinContent(j + 1, 100)
                    resHistoTrend.SetBinError(j + 1, 100)

                    title[j] = (
                        "RICH"
                        + str(self.whichRich)
                        + " Cherenkov angle resolution It. "
                        + str(j)
                    )
                    hist[j].SetTitle(title[j])
                    hist[j].SetXTitle("#Delta#theta_{Cherenkov} (rad)")
                    hist[j].SetYTitle("Entries")

                    gStyle.SetStatW(0.11)

                    theCanvas2.cd()
                    hist[j].Draw()
                    theCanvas2.Print(expertFile)

                    theCanvas.cd()
                    theCanvas.cd(1 + j)
                    hist[j].Draw()
                    thisFile[j].Close("R")

                    # Make resHistoTrend iteration trend plot
                    resHistoTrend.SetMarkerStyle(8)
                    resHistoTrend.SetXTitle("iteration number")
                    resHistoTrend.SetYTitle(
                        "#splitline{Cherenkov angle resolution (mrad)}{}"
                    )

                    theCanvas2.Clear()
                    theCanvas.Clear()
                    theCanvas.Close()
                    theCanvas2.Close()
                    resHistoTrend.DrawCopy()
                    sys.stdout.flush()

                    return

                ckres_error = histFunc[j].GetParError(2) * 1000
                gaussianmean = histFunc[j].GetParameter(1) * 1000

                resHistoTrend.SetBinContent(j + 1, ckres)
                resHistoTrend.SetBinError(j + 1, ckres_error)

                print(
                    "_i" + str(j) + " CK angle resolution:     " + str(ckres) + " mrad."
                )
                print(
                    "_i"
                    + str(j)
                    + " CK angle resolution err: "
                    + str(ckres_error)
                    + " mrad."
                )
                print(
                    "_i"
                    + str(j)
                    + " CK angle gaussian mean: "
                    + str(gaussianmean)
                    + " mrad."
                )

                if j == self.maxIt:
                    self.finalCKres = ckres
                    self.finalCKresErr = ckres_error
                    self.finalCKgaussianmean = gaussianmean

                title[j] = (
                    "RICH"
                    + str(self.whichRich)
                    + " Cherenkov angle resolution It. "
                    + str(j)
                )
                resHistograms[j].SetTitle(title[j])
                resHistograms[j].SetXTitle("#Delta#theta_{Cherenkov} (rad)")
                resHistograms[j].SetYTitle("Entries")

                histFunc[j].SetLineColor(2)
                polbkg[j].SetLineColor(4)

                gStyle.SetStatW(0.11)

                theCanvas2.cd()
                resHistograms[j].Draw()
                histFunc[j].Draw("SAME")
                polbkg[j].Draw("SAME")

                theCanvas2.Print(expertFile)

                theCanvas.cd()
                theCanvas.cd(1 + j)
                resHistograms[j].Draw()
                histFunc[j].Draw("SAME")
                polbkg[j].Draw("SAME")

                thisFile[j].Close("R")
            else:
                print("WARNING: " + filename[j] + ": does not exist")

        # theCanvas.Print(monitorFile)

        # sys.stdout.flush()

        # Make resHistoTrend iteration trend plot
        resHistoTrend.SetMarkerStyle(8)
        resHistoTrend.SetXTitle("iteration number")
        resHistoTrend.SetYTitle("#splitline{Cherenkov angle resolution (mrad)}{}")

        theCanvas2.Clear()
        theCanvas.Clear()
        theCanvas.Close()
        theCanvas2.Close()
        resHistoTrend.DrawCopy()
        sys.stdout.flush()

    def fitCherenkovAngle(self, hist):
        # get Chris Jones's C++ fitter
        fitter = gbl.Rich.Rec.CKResolutionFitter()

        # set signal (AsymNormal) and background (FreeNPol) models
        fitter.params().RichFitTypes[whichRich - 1].clear()
        fitter.params().RichFitTypes[whichRich - 1].push_back("AsymNormal:FreeNPol")

        # change fit ranges
        fitter.params().RichFitMin[0] = self.fitMin
        fitter.params().RichFitMin[1] = self.fitMin
        fitter.params().RichFitMax[0] = self.fitMax
        fitter.params().RichFitMax[1] = self.fitMax

        res = fitter.fit(hist, whichRich)

        return res, hist


def check_for_reco_issues(finalCKres, finalCKresErr, csv_name):
    issue_present = False
    if os.path.exists(f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv"):
        with open(f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv", "r") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if float(finalCKresErr) == 0.0:
                    issue_present = True
                    print(
                        "INFO: CK resolution error is 0.0. Rerunning reconstruction..."
                    )

                if issue_present:
                    return issue_present

    return issue_present


def perform_one_fit_pipeline(hist_path="", fitrange=0.0055):
    panel = whichPanel
    axis_name = "x"

    alignMonitor = AlignMonitor(whichRich=whichRich, fitrange=fitrange)
    alignMonitor.performMonitoring(panel, hist_path)
    finalCKres = alignMonitor.finalCKres
    finalCKresErr = alignMonitor.finalCKresErr
    finalCKgaussianmean = alignMonitor.finalCKgaussianmean

    return finalCKres, finalCKresErr, finalCKgaussianmean


# finalCKres, finalCKresErr, finalCKgaussianmean = perform_one_fit_pipeline(fitrange=0.0055)
if whichRich == 1:
    finalCKres, finalCKresErr, finalCKgaussianmean = perform_one_fit_pipeline(
        fitrange=0.0055
    )
elif whichRich == 2:
    finalCKres, finalCKresErr, finalCKgaussianmean = perform_one_fit_pipeline(
        fitrange=0.0035
    )

print(finalCKres, finalCKresErr, finalCKgaussianmean)

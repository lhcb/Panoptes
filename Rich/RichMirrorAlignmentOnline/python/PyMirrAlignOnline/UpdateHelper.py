###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = "Paras Naik"
__copyright__ = "University of Bristol, May 2018"

import getpass
import os
import re
import shutil
import sys
import threading
import time
from pathlib import Path
from time import gmtime, sleep, strftime, time

import pydim


def load_opts(f, key_strip="OnlineEnv."):
    from Gaudi.Main import parseOpt

    result = {}
    for line in f:
        line = line.strip()
        if line.startswith("//"):
            continue
        m = re.match(r"^(?P<key>\S+)\s*=\s*(?P<value>.*)\s*;$", line)
        if not m:
            raise ValueError(f"cannot parse {line!r}")
        key = m.group("key")
        if not key.startswith(key_strip):
            raise ValueError(f"key {key!r} does not start with {key_strip!r}")
        key = key[len(key_strip) :]
        result[key] = parseOpt(m.group("value"))
    return result


class UpdateHelper:
    def __init__(self, _alignConf):
        self.alignConf = _alignConf
        self.nameStr = self.alignConf.getProp("nameStr")
        self.tiltNames = self.alignConf.getProp("tiltNames")
        self.workdir = self.alignConf.getProp("WorkDir")
        self.whichRich = self.alignConf.getProp("Rich")
        self.magnFactorsMode = self.alignConf.getProp("magnFactorsMode")
        # self.magnifDir = os.path.join(self.alignConf.getProp('stackDir'),self.alignConf.getProp('magnifDir'))
        if self.alignConf.getProp("testing") or not self.alignConf.getProp(
            "autoUpdate"
        ):
            # if testing, don't send DIM message to correct server, since clara will potentially expect an updated alignment in the online conddb
            self.updateServiceName = (
                f"/Publisher/Rich{self.whichRich}/Alignment/MirrorsTest"
            )
        else:
            self.updateServiceName = (
                f"/Publisher/Rich{self.whichRich}/Alignment/Mirrors"
            )
        # self.messageServiceName = "/Publisher/Rich"+str(self.whichRich)+"/MirrorAlignMessage"
        self.messageServiceName = "/Publisher/AlignmentMessenger"
        self.format = "C"  # "C" means a string
        self.updateString = ""
        self.messageString = ""
        self.serverName = "Rich" + str(self.whichRich) + "MirrorAlign_0"
        # self.serverName = "LHCbA_HLT02_AligDrv_0"
        self.updateSvc = None
        self.messageSvc = None

    def startUpdateDIM(self):
        self.updateSvc = pydim.dis_add_service(
            self.updateServiceName, self.format, self.updateStringTuple, 1
        )
        self.messageSvc = pydim.dis_add_service(
            self.messageServiceName, self.format, self.messageStringTuple, 1
        )
        if not self.updateSvc:
            sys.stderr.write(
                "An error occurred while registering the "
                + self.updateServiceName
                + " DB update DIM service\n"
            )
            sys.exit(1)
        if not self.messageSvc:
            sys.stderr.write(
                "An error occurred while registering the "
                + self.messageServiceName
                + " DB update-message DIM service\n"
            )
            sys.exit(1)
        pydim.dis_start_serving(self.serverName)

        print(
            "INFO: Started DB update DIM service "
            + self.updateServiceName
            + " with server "
            + self.serverName
        )
        print(
            "INFO: Sending initialization DB update code (i.e. just a dummy empty string to start): ",
            self.updateStringTuple(""),
        )
        sys.stdout.flush()
        pydim.dis_update_service(self.updateSvc)

        print(
            "INFO: Started DB update-message DIM service "
            + self.messageServiceName
            + " with server "
            + self.serverName
        )
        print(
            "INFO: Sending initialization DB update-message code (i.e. just a dummy empty string to start): ",
            self.messageStringTuple(""),
        )
        sys.stdout.flush()
        pydim.dis_update_service(self.messageSvc)

    def stopUpdateDIM(self):
        if self.updateSvc:
            pydim.dis_remove_service(self.updateSvc)
        if self.messageSvc:
            pydim.dis_remove_service(self.messageSvc)

    def updateDecision(self, n_it):
        decision = False
        if n_it >= 1:
            decision = True
        return decision

    #  the callback function must return a tuple
    def updateStringTuple(self, tag):
        stringTuple = self.updateString
        return (stringTuple,)

    #  the callback function must return a tuple
    def messageStringTuple(self, tag):
        stringTuple = self.messageString
        return (stringTuple,)

    def updateDB(self, N_DB, decision, message=""):
        import OnlineEnvBase as Online  # import OnlineEnv as Online

        # runs = Online.DeferredRuns
        with open(Path(Online.__file__).parent / "RunList.opts") as f:
            runs = sorted(load_opts(f)["DeferredRuns"])

        minRun = sys.maxsize  # legacy python2 was sys.maxint
        for i in range(0, len(runs)):
            if int(runs[i]) < int(minRun):
                minRun = int(runs[i])
        self.updateString = str(minRun) + " v" + str(N_DB)
        self.messageString = message
        if not self.updateSvc:
            sys.stderr.write(
                "The "
                + self.updateServiceName
                + " DB update DIM service does not exist\n"
            )
            sys.exit(1)
        if not self.messageSvc:
            sys.stderr.write(
                "The "
                + self.messageServiceName
                + " DB update-message DIM service does not exist\n"
            )
            sys.exit(1)
        if decision:
            print("INFO: The decision is to trigger an automatic update of the DB. ")
        else:
            print(
                "INFO: The decision is to not update the DB (the alignment was fine, or autoUpdate is False). "
            )
        print(
            "INFO: Sending the following update code to the "
            + self.updateServiceName
            + " DB update DIM service"
        )
        print("INFO: Sending final DB update code: ", self.updateStringTuple(""))
        pydim.dis_update_service(self.updateSvc)
        print(
            "INFO: Sending the following message code to the "
            + self.messageServiceName
            + " DB update-message DIM service"
        )
        print(
            "INFO: Sending final DB update-message code: ", self.messageStringTuple("")
        )
        pydim.dis_update_service(self.messageSvc)
        print(
            "INFO: We now wait 20 seconds for Online to pick up our DB update and DB update-message."
        )
        sys.stdout.flush()
        sleep(20)  # to give time for the update to be caught

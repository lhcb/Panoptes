###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Claire Prouve, Paras Naik"
__copyright__ = "University of Bristol, November 2022"

import copy
import getpass
import os
import re
import shutil
import subprocess
import sys
import time
from time import gmtime, sleep, strftime, time
from typing import List

from ruamel.yaml import YAML

from PyMirrAlignOnline.convert_magnFactors_txt_YAML import convert_magnFactors_txt_YAML


class RichAlignmentHelper:
    def __init__(self, configuration):
        self.alignConf = configuration
        self.thisNameStr = self.alignConf.getProp("nameStr")
        self.tiltNames = self.alignConf.getProp("tiltNames")
        # self.magnifDir = os.path.join(self.alignConf.getProp('stackDir'),self.alignConf.getProp('magnifDir'))
        self.workdir = self.alignConf.getProp("WorkDir")
        self.whichRich = self.alignConf.getProp("Rich")
        self.deltaThetaWindow = self.alignConf.getProp("deltaThetaWindow")
        self.minAverageBinPop = self.alignConf.getProp("minAverageBinPop")
        self.phiBinFactor = self.alignConf.getProp("phiBinFactor")
        self.minFracPhiBinsPopulated = self.alignConf.getProp("minFracPhiBinsPopulated")
        self.combinFitMethod = self.alignConf.getProp("combinFitMethod")
        self.coeffCalibTilt = self.alignConf.getProp("coeffCalibTilt")
        self.magnFactorsMode = self.alignConf.getProp("magnFactorsMode")
        self.solutionMethod = self.alignConf.getProp("solutionMethod")
        self.fixSinusoidShift = self.alignConf.getProp("fixSinusoidShift")

    def runMirrCombinFit(self, n_it):
        ps = []

        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames

        tiltNamesLength = len(_tiltNames)

        yaml = YAML()
        with open(f"{self.workdir}rich{self.whichRich}_variant.yml") as finp:
            variant = yaml.load(finp)

        yaml = YAML()
        with open(f"{self.workdir}rich{self.whichRich}_session_timestamp.yml") as finp:
            dt = yaml.load(finp)

        for tiltName in _tiltNames:
            if tiltName == "":
                connectStr = ""
            else:
                connectStr = "_"

            combinFitDir = (
                f"{self.workdir}{variant}_histos{connectStr}{tiltName}_i{n_it}/"
            )
            if os.path.exists(combinFitDir):
                print(f"INFO: Removing {combinFitDir}")
                shutil.rmtree(combinFitDir)
            os.makedirs(combinFitDir)

            # combinFitPlotDir = f"{combinFitDir}plots/"
            combinFitPlotDir = f"{self.workdir}plots/"  # PN: All plots will now go into one directory, at top level in the workdir
            # os.makedirs(combinFitPlotDir)
            if not os.path.exists(combinFitPlotDir):
                os.makedirs(combinFitPlotDir)

            combinResultsFile = (
                f"{self.workdir}{variant}_fit_res{connectStr}{tiltName}_i{n_it}.txt"
            )

            # fitConfigFile = combinFitDir + "Rich" + str(
            #    self.whichRich
            # ) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(
            #    n_it) + ".conf"
            """
            histoFile = "RichRecQCHistos_rich" + str(
                self.whichRich
            ) + "_" + self.thisNameStr + connectStr + tiltName + "_i" + str(
                n_it) + ".root"

            if os.path.exists(fitConfigFile):
                os.remove(fitConfigFile)
            f = open(fitConfigFile, 'w')
            args = 'richDetector             = ' + str(self.whichRich) + '\n'
            args += 'plotDir                  = ' + combinFitPlotDir + '\n'
            args += 'workDir                  = ' + self.workdir + '\n'
            args += 'iterationCount           = ' + str(n_it) + '\n'
            args += 'thisVariant              = ' + str(
                self.thisNameStr) + '\n'
            args += 'inputHistosFile          = ' + str(self.workdir +
                                                        histoFile) + '\n'
            args += 'mirrCombinFitResultsFile = ' + str(
                combinResultsFile) + '\n'
            args += 'mirrCombsFile            = ' + self.combAndMirrSubsets + '\n'  # PN - this is now expecting a .py instead of a .txt, but RichMirrAlign expects a .yml, so this will need to be resolved
            args += 'fixSinusoidShift         = ' + str(
                self.fixSinusoidShift) + '\n'
            args += 'minAverageBinPop         = ' + str(
                self.minAverageBinPop) + '\n'
            args += 'phiBinFactor             = ' + str(
                self.phiBinFactor) + '\n'
            args += 'deltaThetaWindow         = ' + str(
                self.deltaThetaWindow) + '\n'
            args += 'combinFitMethod          = ' + str(
                self.combinFitMethod) + '\n'
            args += 'plotOutputLevel          = 2 \n'
            # PN - Anatoly puts stopToleranceMode here. Do we need it? Does it have the same meaning as the one for RichMirrAlign?
            args += 'nominalResolutionSigma   = ' + str(
                self.alignConf.getProp('nominalResolutionSigma')) + '\n'
            # PN - Anatoly puts stopSigmaFraction here. Do we need it? Does it have the same meaning as the one for RichMirrAlign?
            args += 'warningFactor            = ' + str(
                self.alignConf.getProp('warningFactor')) + '\n'
            args += 'minFracPhiBinsPopulated  = ' + str(
                self.minFracPhiBinsPopulated) + '\n'
            f.write(args)
            f.close()
            """

            conf_new = {}

            richFilesDir = self.alignConf.getProp(
                "richFiles"
            )  # '/group/rich/AlignmentFiles/'
            if not os.path.exists(
                f"{self.workdir}/rich{self.whichRich}_fit_align_confs.yml"
            ):
                print(
                    f"INFO: Copying {richFilesDir}FitAlignConfs/rich{self.whichRich}_fit_align_confs.yml to {self.workdir}rich{self.whichRich}_fit_align_confs.yml"
                )
                shutil.copy2(
                    f"{richFilesDir}FitAlignConfs/rich{self.whichRich}_fit_align_confs.yml",
                    f"{self.workdir}rich{self.whichRich}_fit_align_confs.yml",
                )
            with open(
                f"{self.workdir}rich{self.whichRich}_fit_align_confs.yml"
            ) as finp:
                conf = yaml.load(finp)

            last_timestamp = list(conf)[-1]
            copy_last_conf = copy.deepcopy(conf[last_timestamp])
            conf_new[dt] = copy_last_conf
            conf_new[dt]["fit_conf"].update(
                {
                    "plotDir": combinFitPlotDir,
                    "workDir": self.workdir,
                    "fixSinusoidShift": self.fixSinusoidShift,
                    "sinusoidShift": 0.0,
                    "zeroGlobalFitMean": False,
                    "useGlobalFitMean": False,
                    "minAverageBinPop": self.minAverageBinPop,
                    "phiBinFactor": self.phiBinFactor,
                    "minFracPhiBinsPopulated": self.minFracPhiBinsPopulated,
                    "deltaThetaWindow": self.deltaThetaWindow,
                    "combinFitMethod": self.combinFitMethod,
                    "backgroundOrder": 2,
                    "plotOutputLevel": 2,
                    "warningFactor": self.alignConf.getProp("warningFactor"),
                    "stopToleranceMode": self.alignConf.getProp(
                        "stopToleranceMode"
                    ),  # PN: same as for RichMirrAlign
                    "stopTolerance": 0.23,  # PN: I have no idea what this is for
                    "nominalResolutionSigma": self.alignConf.getProp(
                        "nominalResolutionSigma"
                    ),
                    "stopSigmaFraction": self.alignConf.getProp(
                        "stopSigmaFraction"
                    ),  # PN: we used to only provide this to RichMirrAlign, now we provide it to RichMirrCombinFit? - this is currently 0 in the configuration!
                }
            )

            conf_new[dt]["align_conf"].update(
                {
                    "magnFactorsMode": self.magnFactorsMode,
                    "solutionMethod": self.solutionMethod,
                    "usePremisaligned": False,
                    "stopToleranceMode": self.alignConf.getProp("stopToleranceMode"),
                    "stopTolerancePriY": self.alignConf.getProp("stopTolerancePriY"),
                    "stopTolerancePriZ": self.alignConf.getProp("stopTolerancePriZ"),
                    "stopToleranceSecY": self.alignConf.getProp("stopToleranceSecY"),
                    "stopToleranceSecZ": self.alignConf.getProp("stopToleranceSecZ"),
                    "stopDecisionMode": self.alignConf.getProp("stopDecisionMode"),
                    "regularizationMode": self.alignConf.getProp("regularizationMode"),
                    "printMode": 2,
                }
            )

            # print(f"INFO: Last Timestamp: {last_timestamp}")
            # print(f"INFO: Current Timestamp: {dt}")

            if (
                dt != last_timestamp
            ):  # if same timestamp, then we are doing some sort of testing with external root and YAML configuration files OR the .yml was already written for this alignment (i.e. we are in subsequent iterations)
                with open(
                    f"{self.workdir}/rich{self.whichRich}_fit_align_confs.yml", "a"
                ) as fout:
                    yaml.boolean_representation = ["false", "true"]
                    yaml.dump(conf_new, fout)

            fitOutputFile = f"{combinFitDir}Rich{self.whichRich}MirrCombinFitOut_{self.thisNameStr}{connectStr}{tiltName}_i{n_it}.txt"
            fitStdOutFile = f"{combinFitDir}Rich{self.whichRich}MirrCombinFitStdOut_{self.thisNameStr}{connectStr}{tiltName}_i{n_it}.txt"
            fitStdErrFile = f"{combinFitDir}Rich{self.whichRich}MirrCombinFitStdErr_{self.thisNameStr}{connectStr}{tiltName}_i{n_it}.txt"

            myStdOut = open(fitStdOutFile, "w")
            myStdErr = open(fitStdErrFile, "w")
            cmd = f"cd {self.workdir} ; `which RichMirrCombinFit` rich{self.whichRich} {tiltName} > {fitOutputFile} ; cd -"
            print(f"INFO: Starting RichMirrCombinFit.")
            print(f"INFO: cmd = {cmd}")
            sys.stdout.flush()
            p2 = subprocess.Popen(
                cmd,
                shell=True,
                executable="/bin/bash",
                stdout=myStdOut,
                stderr=myStdErr,
            )
            myStdOut.close()
            myStdErr.close()
            ps.append(p2)

        # Now wait for all subprocesses to finish
        PopenCount = -1
        while True:
            sleep(30)
            ps_status = [q.poll() for q in ps]
            if all([x is not None for x in ps_status]):
                PopenTotal = 0
                for l in ps_status:
                    if l is not None:
                        PopenTotal += 1
                print(
                    f"INFO: {PopenTotal} of {tiltNamesLength} RichMirrCombinFit processes complete"
                )
                print(f"INFO: {strftime('%Y-%m-%d %H:%M:%S', gmtime())}")
                print("INFO: -------------------")
                sys.stdout.flush()
                break
            else:
                PopenDone = 0
                for l in ps_status:
                    if l is not None:
                        PopenDone += 1
                if PopenDone > PopenCount:
                    print(
                        f"INFO: {PopenDone} of {tiltNamesLength} RichMirrCombinFit processes complete"
                    )
                    print(f"INFO: {strftime('%Y-%m-%d %H:%M:%S', gmtime())}")
                    print("INFO: -------------------")
                    PopenCount = 0 + PopenDone
                    sys.stdout.flush()

    def runRichAlign(self, n_it):
        pNoTilt = []
        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames

        yaml = YAML()
        with open(f"{self.workdir}/rich{self.whichRich}_variant.yml") as finp:
            variant = yaml.load(finp)

        for tiltNum, tiltName in enumerate(_tiltNames):
            if tiltName == "":
                connectStr = ""
            else:
                connectStr = "_"

            combinFitDir = (
                f"{self.workdir}{variant}_histos{connectStr}{tiltName}_i{n_it}/"
            )
            print(f"INFO: expected combinFitDir {combinFitDir}")
            combinResultsFile = (
                f"{self.workdir}{variant}_fit_res{connectStr}{tiltName}_i{n_it}.txt"
            )
            print(f"INFO: expected combinResultsFile {combinResultsFile}")
            magnificationOutputFile = (
                f"{self.workdir}{variant}_magnifs{connectStr}{tiltName}_i{n_it}.txt"
            )

            sys.stdout.flush()

            p = []
            if not os.path.exists(combinResultsFile):
                print(
                    "ERROR: no combinResultsFile. The Iterator is about to crash. RichMirrorCombinFit did not run, and/or or the ROOT file(s) it needs are missing."
                )
            with open(combinResultsFile) as fitfile:
                for line in fitfile:
                    pars = line.split()
                    if len(pars) != 7:
                        continue
                    newpars = [pars[0]] + [float(j) for j in pars[1:]]
                    p.append(newpars)
                    if tiltNum == 0:
                        pNoTilt.append(newpars)

            with open(magnificationOutputFile, "w") as magfile:
                for i, sp in enumerate(p):
                    tiltSign = 1.0
                    # if tiltName.count('neg') > 0:
                    if tiltName.count("n") > 0:
                        tiltSign = -1.0
                    mirrPair = sp[0]
                    Y = (sp[1] - pNoTilt[i][1]) / (self.coeffCalibTilt * tiltSign)
                    Z = (sp[3] - pNoTilt[i][3]) / (self.coeffCalibTilt * tiltSign)
                    magfile.write(mirrPair + "  " + str(Y) + "  " + str(Z) + "\n")

        if self.magnFactorsMode != 0:
            # YAMLify magnification factors [currently for records only]
            convert_magnFactors_txt_YAML.to_yaml(self.workdir, n_it)

        # richMirrAlignConfFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "MirrAlign_i" + str(n_it) + ".conf"
        richMirrAlignOutFile = (
            f"{self.workdir}Rich{self.whichRich}MirrAlignOut_i{n_it}.txt"
        )
        # richMirrAlignNanOutFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "MirrAlignNanOut_i" + str(n_it) + ".txt"

        combinFitDir = f"{self.workdir}{variant}_histos_i{n_it}/"
        combinResultsFile = f"{self.workdir}{variant}_fit_res_i{n_it}.txt"

        #
        # currentMirrorYAMLFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i" + str(
        #        n_it) + ".yml"
        # nextIterationYAMLFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i" + str(
        #        n_it + 1) + ".yml"
        # zeroMirrorYAMLFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i0.yml"
        """
        if os.path.exists(richMirrAlignConfFile):
            os.remove(richMirrAlignConfFile)
        f = open(richMirrAlignConfFile, 'w')
        args = 'richDetector             = ' + str(self.whichRich) + '\n'
        args += 'workDir                  = ' + self.workdir + '\n'
        args += 'iterationNumber          = ' + str(n_it) + '\n'
        args += 'variant                  = ' + self.thisNameStr + '\n'
        args += 'mirrCombinFitResultsFile = ' + combinResultsFile + '\n'
        args += 'initIterationInputYAML   = ' + zeroMirrorYAMLFile + '\n'
        args += 'thisIterationInputYAML   = ' + currentMirrorYAMLFile + '\n'
        args += 'nextIterationInputYAML   = ' + nextIterationYAMLFile + '\n'
        args += 'mirrCombsFile            = ' + self.combAndMirrSubsets + '\n'  # PN - this is now expecting a .yml instead of a .txt, but RichMirrCombinFit expects a .py, so this will need to be resolved
        args += 'magnFactorsMode          = ' + str(
            self.magnFactorsMode) + '\n'
        args += 'solutionMethod           = ' + str(self.solutionMethod) + '\n'
        args += 'usePremisaligned         = ' + "false" + '\n'
        args += 'stopToleranceMode        = ' + str(
            self.alignConf.getProp('stopToleranceMode')) + '\n'
        args += 'nominalResolutionSigma   = ' + str(
            self.alignConf.getProp('nominalResolutionSigma')) + '\n'
        args += 'stopSigmaFraction        = ' + str(
            self.alignConf.getProp('stopSigmaFraction')) + '\n'
        args += 'stopTolerancePriY        = ' + str(
            self.alignConf.getProp('stopTolerancePriY')) + '\n'
        args += 'stopTolerancePriZ        = ' + str(
            self.alignConf.getProp('stopTolerancePriZ')) + '\n'
        args += 'stopToleranceSecY        = ' + str(
            self.alignConf.getProp('stopToleranceSecY')) + '\n'
        args += 'stopToleranceSecZ        = ' + str(
            self.alignConf.getProp('stopToleranceSecZ')) + '\n'
        args += 'stopDecisionMode         = ' + str(
            self.alignConf.getProp('stopDecisionMode')) + '\n'
        args += 'regularizationMode       = ' + str(
            self.alignConf.getProp('regularizationMode')) + '\n'
        args += 'printMode                = ' + "2" + '\n'  # PN - eventually un-hardcode this and provide it as an option through the Configuration
        f.write(args)
        f.close()
        """

        alignStdOutFile = (
            f"{self.workdir}Rich{self.whichRich}MirrAlignStdOut_i{n_it}.txt"
        )
        alignStdErrFile = (
            f"{self.workdir}Rich{self.whichRich}MirrAlignStdErr_i{n_it}.txt"
        )
        myStdOut = open(alignStdOutFile, "w")
        myStdErr = open(alignStdErrFile, "w")
        cmd = f"cd {self.workdir} ; `which RichMirrAlign` rich{self.whichRich} > {richMirrAlignOutFile} ; cd - "
        print(f"INFO: Starting RichMirrAlign.")
        print(f"INFO: cmd = {cmd}")
        sys.stdout.flush()
        p3 = subprocess.Popen(
            cmd, shell=True, executable="/bin/bash", stdout=myStdOut, stderr=myStdErr
        )
        p3.communicate()
        myStdOut.close()
        myStdErr.close()
        sys.stdout.flush()
        print("INFO: RichMirrAlign complete. ")

    def checkEnoughBinsPopulated(self, n_it):
        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames

        yaml = YAML()
        with open(f"{self.workdir}rich{self.whichRich}_variant.yml") as finp:
            variant = yaml.load(finp)

        for tiltName in _tiltNames:
            if tiltName == "":
                connectStr = ""
            else:
                connectStr = "_"

            combinFitDir = (
                f"{self.workdir}{variant}_histos{connectStr}{tiltName}_i{n_it}/"
            )
            fitOutputFile = f"{combinFitDir}Rich{self.whichRich}MirrCombinFitOut_{self.thisNameStr}{connectStr}{tiltName}_i{n_it}.txt"
            fitNGEFile = f"{combinFitDir}Rich{self.whichRich}MirrCombinFitNGEOut_{self.thisNameStr}{connectStr}{tiltName}_i{n_it}.txt"

            os.system(f"cat {fitOutputFile} | grep ' not_good_enough ' > {fitNGEFile}")

            if not os.path.exists(fitNGEFile):
                print("ERROR: no fitNGEFile.")
                return False

            with open(fitNGEFile) as f:
                num_lines = len(f.readlines())

            if num_lines != 0:
                print(
                    "WARNING: Not enough well populated phi-bins in all mirror combination histograms. Will perform all fits anyway."
                )
                sys.stdout.flush()
                return False

        sys.stdout.flush()
        return True

    def hasConverged(self):
        stop_or_continue_file = (
            self.workdir + f"rich{self.whichRich}_stop_or_continue.txt"
        )
        if not os.path.exists(stop_or_continue_file):
            print(f"ERROR: no {stop_or_continue_file} file.")
            # return False  # PN - if there is no stop_or_continue_file, something is wrong, thus we want it to crash in the next step.

        with open(stop_or_continue_file) as rich_stop_or_continue_txt:
            verdict = rich_stop_or_continue_txt.readline().strip().upper()

        print(f"INFO: Verdict: {verdict}")
        sys.stdout.flush()

        if verdict == "STOP":
            return True
        if verdict not in ["STOP", "STOP_NAN", "STOP_NGE"]:
            return False

    ########## OLD functions below here ###########

    def old_runMirrCombinFit(self, n_it):
        ps = []

        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames

        tiltNamesLength = len(_tiltNames)

        yaml = YAML()
        with open(
            self.workdir + "/" + "rich" + str(self.whichRich) + "_variant.yml"
        ) as finp:
            variant = yaml.load(finp)

        yaml = YAML()
        with open(
            self.workdir + "/" + "rich" + str(self.whichRich) + "_session_timestamp.yml"
        ) as finp:
            dt = yaml.load(finp)

        for tiltName in _tiltNames:
            if tiltName == "":
                connectStr = ""
            else:
                connectStr = "_"

            combinFitDir = (
                self.workdir
                + variant
                + "_histos"
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + "/"
            )
            combinFitPlotDir = combinFitDir + "plots/"
            if os.path.exists(combinFitDir):
                print(f"INFO: Removing {combinFitDir}")
                shutil.rmtree(combinFitDir)
            os.makedirs(combinFitDir)
            os.makedirs(combinFitPlotDir)

            combinResultsFile = (
                self.workdir
                + variant
                + "_fit_res"
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )
            # fitConfigFile = combinFitDir + "Rich" + str(
            #    self.whichRich
            # ) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(
            #    n_it) + ".conf"
            """
            histoFile = "RichRecQCHistos_rich" + str(
                self.whichRich
            ) + "_" + self.thisNameStr + connectStr + tiltName + "_i" + str(
                n_it) + ".root"

            if os.path.exists(fitConfigFile):
                os.remove(fitConfigFile)
            f = open(fitConfigFile, 'w')
            args = 'richDetector             = ' + str(self.whichRich) + '\n'
            args += 'plotDir                  = ' + combinFitPlotDir + '\n'
            args += 'workDir                  = ' + self.workdir + '\n'
            args += 'iterationCount           = ' + str(n_it) + '\n'
            args += 'thisVariant              = ' + str(
                self.thisNameStr) + '\n'
            args += 'inputHistosFile          = ' + str(self.workdir +
                                                        histoFile) + '\n'
            args += 'mirrCombinFitResultsFile = ' + str(
                combinResultsFile) + '\n'
            args += 'mirrCombsFile            = ' + self.combAndMirrSubsets + '\n'  # PN - this is now expecting a .py instead of a .txt, but RichMirrAlign expects a .yml, so this will need to be resolved
            args += 'fixSinusoidShift         = ' + str(
                self.fixSinusoidShift) + '\n'
            args += 'minAverageBinPop         = ' + str(
                self.minAverageBinPop) + '\n'
            args += 'phiBinFactor             = ' + str(
                self.phiBinFactor) + '\n'
            args += 'deltaThetaWindow         = ' + str(
                self.deltaThetaWindow) + '\n'
            args += 'combinFitMethod          = ' + str(
                self.combinFitMethod) + '\n'
            args += 'plotOutputLevel          = 2 \n'
            # PN - Anatoly puts stopToleranceMode here. Do we need it? Does it have the same meaning as the one for RichMirrAlign?
            args += 'nominalResolutionSigma   = ' + str(
                self.alignConf.getProp('nominalResolutionSigma')) + '\n'
            # PN - Anatoly puts stopSigmaFraction here. Do we need it? Does it have the same meaning as the one for RichMirrAlign?
            args += 'warningFactor            = ' + str(
                self.alignConf.getProp('warningFactor')) + '\n'
            args += 'minFracPhiBinsPopulated  = ' + str(
                self.minFracPhiBinsPopulated) + '\n'
            f.write(args)
            f.close()
            """

            conf_new = {}
            if not os.path.exists(
                self.workdir
                + "/"
                + "rich"
                + str(self.whichRich)
                + "_fit_align_confs.yml"
            ):
                shutil.copy2(
                    #                    '/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/workflow_configuration/rich'
                    "/group/rich/AlignmentFiles/FitAlignConfs/rich"
                    + str(self.whichRich)
                    + "_fit_align_confs.yml",
                    self.workdir
                    + "/"
                    + "rich"
                    + str(self.whichRich)
                    + "_fit_align_confs.yml",
                )
            with open(
                self.workdir
                + "/"
                + "rich"
                + str(self.whichRich)
                + "_fit_align_confs.yml"
            ) as finp:
                conf = yaml.load(finp)

            last_timestamp = list(conf)[-1]
            copy_last_conf = copy.deepcopy(conf[last_timestamp])
            conf_new[dt] = copy_last_conf
            conf_new[dt]["fit_conf"]["plotDir"] = (
                combinFitPlotDir  # PN: Need to fix, all plots appear in _i0 directory
            )
            conf_new[dt]["fit_conf"]["workDir"] = self.workdir
            conf_new[dt]["fit_conf"]["fixSinusoidShift"] = self.fixSinusoidShift
            conf_new[dt]["fit_conf"]["sinusoidShift"] = 0.0
            conf_new[dt]["fit_conf"]["zeroGlobalFitMean"] = False
            conf_new[dt]["fit_conf"]["useGlobalFitMean"] = False
            conf_new[dt]["fit_conf"]["minAverageBinPop"] = self.minAverageBinPop
            conf_new[dt]["fit_conf"]["phiBinFactor"] = self.phiBinFactor
            conf_new[dt]["fit_conf"]["minFracPhiBinsPopulated"] = (
                self.minFracPhiBinsPopulated
            )
            conf_new[dt]["fit_conf"]["deltaThetaWindow"] = self.deltaThetaWindow
            conf_new[dt]["fit_conf"]["combinFitMethod"] = self.combinFitMethod
            conf_new[dt]["fit_conf"]["backgroundOrder"] = 2
            conf_new[dt]["fit_conf"]["plotOutputLevel"] = 2
            conf_new[dt]["fit_conf"]["warningFactor"] = self.alignConf.getProp(
                "warningFactor"
            )
            conf_new[dt]["fit_conf"]["stopToleranceMode"] = self.alignConf.getProp(
                "stopToleranceMode"
            )  # PN: same as for RichMirrAlign
            conf_new[dt]["fit_conf"]["stopTolerance"] = (
                0.23  # PN: I have no idea what this is for
            )
            conf_new[dt]["fit_conf"]["nominalResolutionSigma"] = self.alignConf.getProp(
                "nominalResolutionSigma"
            )
            conf_new[dt]["fit_conf"]["stopSigmaFraction"] = self.alignConf.getProp(
                "stopSigmaFraction"
            )  # PN: we used to only provide this to RichMirrAlign, now we provide it to RichMirrCombinFit? - this is currently 0 in the configuration!
            conf_new[dt]["align_conf"]["magnFactorsMode"] = self.magnFactorsMode
            conf_new[dt]["align_conf"]["solutionMethod"] = self.solutionMethod
            conf_new[dt]["align_conf"]["usePremisaligned"] = False
            conf_new[dt]["align_conf"]["stopToleranceMode"] = self.alignConf.getProp(
                "stopToleranceMode"
            )
            conf_new[dt]["align_conf"]["stopTolerancePriY"] = self.alignConf.getProp(
                "stopTolerancePriY"
            )
            conf_new[dt]["align_conf"]["stopTolerancePriZ"] = self.alignConf.getProp(
                "stopTolerancePriZ"
            )
            conf_new[dt]["align_conf"]["stopToleranceSecY"] = self.alignConf.getProp(
                "stopToleranceSecY"
            )
            conf_new[dt]["align_conf"]["stopToleranceSecZ"] = self.alignConf.getProp(
                "stopToleranceSecZ"
            )
            conf_new[dt]["align_conf"]["stopDecisionMode"] = self.alignConf.getProp(
                "stopDecisionMode"
            )
            conf_new[dt]["align_conf"]["regularizationMode"] = self.alignConf.getProp(
                "regularizationMode"
            )
            conf_new[dt]["align_conf"]["printMode"] = 2

            print("INFO: Last Timestamp: ", last_timestamp)
            print("INFO: Current Timestamp: ", dt)
            if (
                dt != last_timestamp
            ):  # if same timestamp, then we are doing some sort of testing with external root and YAML configuration files
                with open(
                    self.workdir
                    + "/"
                    + "rich"
                    + str(self.whichRich)
                    + "_fit_align_confs.yml",
                    "a",
                ) as fout:
                    yaml.boolean_representation = ["false", "true"]
                    yaml.dump(conf_new, fout)

            fitOutputFile = (
                combinFitDir
                + "Rich"
                + str(self.whichRich)
                + "MirrCombinFitOut_"
                + self.thisNameStr
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )
            fitStdOutFile = (
                combinFitDir
                + "Rich"
                + str(self.whichRich)
                + "MirrCombinFitStdOut_"
                + self.thisNameStr
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )
            fitStdErrFile = (
                combinFitDir
                + "Rich"
                + str(self.whichRich)
                + "MirrCombinFitStdErr_"
                + self.thisNameStr
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )
            myStdOut = open(fitStdOutFile, "w")
            myStdErr = open(fitStdErrFile, "w")
            cmd = (
                "cd "
                + self.workdir
                + " ; `which RichMirrCombinFit` "
                + "rich"
                + str(self.whichRich)
                + " "
                + tiltName
                + " > "
                + fitOutputFile
                + " ; cd - "
            )
            print(f"INFO: Starting RichMirrCombinFit.")
            print(f"INFO: cmd = {cmd}")
            sys.stdout.flush()
            p2 = Popen(
                cmd,
                shell=True,
                executable="/bin/bash",
                stdout=myStdOut,
                stderr=myStdErr,
            )
            myStdOut.close()
            myStdErr.close()
            ps.append(p2)

        # Now wait for all subprocesses to finish
        PopenCount = -1
        while True:
            sleep(30)
            ps_status = [q.poll() for q in ps]
            if all([x is not None for x in ps_status]):
                PopenTotal = 0
                for l in ps_status:
                    if l is not None:
                        PopenTotal += 1
                print(
                    "INFO: %d of %d RichMirrCombinFit processes complete"
                    % (PopenTotal, tiltNamesLength)
                )
                print("INFO: ", strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                print("INFO: -------------------")
                sys.stdout.flush()
                break
            else:
                PopenDone = 0
                for l in ps_status:
                    if l is not None:
                        PopenDone += 1
                if PopenDone > PopenCount:
                    print(
                        "INFO: %d of %d RichMirrCombinFit processes complete"
                        % (PopenDone, tiltNamesLength)
                    )
                    print("INFO: ", strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                    print("INFO: -------------------")
                    PopenCount = 0 + PopenDone
                    sys.stdout.flush()

    def old_runRichAlign(self, n_it):
        pNoTilt = []
        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames

        yaml = YAML()
        with open(
            self.workdir + "/" + "rich" + str(self.whichRich) + "_variant.yml"
        ) as finp:
            variant = yaml.load(finp)

        for tiltNum, tiltName in enumerate(_tiltNames):
            if tiltName == "":
                connectStr = ""
            else:
                connectStr = "_"

            combinFitDir = (
                self.workdir
                + variant
                + "_histos"
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + "/"
            )
            print("INFO: expected combinFitDir " + combinFitDir)
            combinResultsFile = (
                self.workdir
                + variant
                + "_fit_res"
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )
            print("INFO: expected combinResultsFile " + combinResultsFile)
            magnificationOutputFile = (
                self.workdir
                + variant
                + "_magnifs"
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )  #
            sys.stdout.flush()

            p = []
            if not os.path.exists(combinResultsFile):
                print(
                    "ERROR: no combinResultsFile. The Iterator is about to crash. RichMirrorCombinFit did not run, and/or or the ROOT file(s) it needs are missing."
                )
            with open(combinResultsFile) as fitfile:
                line = fitfile.readline()
                while line:
                    pars = line.split()
                    # if len(pars) is not 7: # hated by python 3.8
                    if len(pars) != 7:
                        line = fitfile.readline()
                        continue
                    newpars = []
                    for i, j in enumerate(pars):
                        if i == 0:
                            newpars.append(j)
                        else:
                            newpars.append(float(j))
                    p.append(newpars)
                    if tiltNum == 0:
                        pNoTilt.append(newpars)
                    line = fitfile.readline()

            with open(magnificationOutputFile, "w") as magfile:
                for i, sp in enumerate(p):
                    tiltSign = 1.0
                    # if tiltName.count('neg') > 0:
                    if tiltName.count("n") > 0:
                        tiltSign = -1.0
                    mirrPair = sp[0]
                    Y = (sp[1] - pNoTilt[i][1]) / (self.coeffCalibTilt * tiltSign)
                    Z = (sp[3] - pNoTilt[i][3]) / (self.coeffCalibTilt * tiltSign)
                    magfile.write(mirrPair + "  " + str(Y) + "  " + str(Z) + "\n")

        # richMirrAlignConfFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "MirrAlign_i" + str(n_it) + ".conf"
        richMirrAlignOutFile = (
            self.workdir
            + "Rich"
            + str(self.whichRich)
            + "MirrAlignOut_i"
            + str(n_it)
            + ".txt"
        )
        # richMirrAlignNanOutFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "MirrAlignNanOut_i" + str(n_it) + ".txt"

        combinFitDir = self.workdir + variant + "_histos" + "_i" + str(n_it) + "/"
        combinResultsFile = (
            self.workdir + variant + "_fit_res" + "_i" + str(n_it) + ".txt"
        )
        #
        # currentMirrorYAMLFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i" + str(
        #        n_it) + ".yml"
        # nextIterationYAMLFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i" + str(
        #        n_it + 1) + ".yml"
        # zeroMirrorYAMLFile = self.workdir + "Rich" + str(
        #    self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i0.yml"
        """
        if os.path.exists(richMirrAlignConfFile):
            os.remove(richMirrAlignConfFile)
        f = open(richMirrAlignConfFile, 'w')
        args = 'richDetector             = ' + str(self.whichRich) + '\n'
        args += 'workDir                  = ' + self.workdir + '\n'
        args += 'iterationNumber          = ' + str(n_it) + '\n'
        args += 'variant                  = ' + self.thisNameStr + '\n'
        args += 'mirrCombinFitResultsFile = ' + combinResultsFile + '\n'
        args += 'initIterationInputYAML   = ' + zeroMirrorYAMLFile + '\n'
        args += 'thisIterationInputYAML   = ' + currentMirrorYAMLFile + '\n'
        args += 'nextIterationInputYAML   = ' + nextIterationYAMLFile + '\n'
        args += 'mirrCombsFile            = ' + self.combAndMirrSubsets + '\n'  # PN - this is now expecting a .yml instead of a .txt, but RichMirrCombinFit expects a .py, so this will need to be resolved
        args += 'magnFactorsMode          = ' + str(
            self.magnFactorsMode) + '\n'
        args += 'solutionMethod           = ' + str(self.solutionMethod) + '\n'
        args += 'usePremisaligned         = ' + "false" + '\n'
        args += 'stopToleranceMode        = ' + str(
            self.alignConf.getProp('stopToleranceMode')) + '\n'
        args += 'nominalResolutionSigma   = ' + str(
            self.alignConf.getProp('nominalResolutionSigma')) + '\n'
        args += 'stopSigmaFraction        = ' + str(
            self.alignConf.getProp('stopSigmaFraction')) + '\n'
        args += 'stopTolerancePriY        = ' + str(
            self.alignConf.getProp('stopTolerancePriY')) + '\n'
        args += 'stopTolerancePriZ        = ' + str(
            self.alignConf.getProp('stopTolerancePriZ')) + '\n'
        args += 'stopToleranceSecY        = ' + str(
            self.alignConf.getProp('stopToleranceSecY')) + '\n'
        args += 'stopToleranceSecZ        = ' + str(
            self.alignConf.getProp('stopToleranceSecZ')) + '\n'
        args += 'stopDecisionMode         = ' + str(
            self.alignConf.getProp('stopDecisionMode')) + '\n'
        args += 'regularizationMode       = ' + str(
            self.alignConf.getProp('regularizationMode')) + '\n'
        args += 'printMode                = ' + "2" + '\n'  # PN - eventually un-hardcode this and provide it as an option through the Configuration
        f.write(args)
        f.close()
        """

        alignStdOutFile = (
            self.workdir
            + "Rich"
            + str(self.whichRich)
            + "MirrAlignStdOut_i"
            + str(n_it)
            + ".txt"
        )
        alignStdErrFile = (
            self.workdir
            + "Rich"
            + str(self.whichRich)
            + "MirrAlignStdErr_i"
            + str(n_it)
            + ".txt"
        )
        myStdOut = open(alignStdOutFile, "w")
        myStdErr = open(alignStdErrFile, "w")
        cmd = (
            "cd "
            + self.workdir
            + " ; `which RichMirrAlign` "
            + "rich"
            + str(self.whichRich)
            + " > "
            + richMirrAlignOutFile
            + " ; cd - "
        )
        print("INFO: Starting RichMirrAlign. ")
        print("INFO: cmd = " + cmd)
        sys.stdout.flush()
        p3 = Popen(
            cmd, shell=True, executable="/bin/bash", stdout=myStdOut, stderr=myStdErr
        )
        p3.communicate()
        myStdOut.close()
        myStdErr.close()
        print("INFO: RichMirrAlign complete. ")
        sys.stdout.flush()

    def old_checkEnoughBinsPopulated(self, n_it):
        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames

        yaml = YAML()
        with open(
            self.workdir + "/" + "rich" + str(self.whichRich) + "_variant.yml"
        ) as finp:
            variant = yaml.load(finp)

        for tiltName in _tiltNames:
            if tiltName == "":
                connectStr = ""
            else:
                connectStr = "_"

            combinFitDir = (
                self.workdir
                + variant
                + "_histos"
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + "/"
            )
            fitOutputFile = (
                combinFitDir
                + "Rich"
                + str(self.whichRich)
                + "MirrCombinFitOut_"
                + self.thisNameStr
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )
            fitNGEFile = (
                combinFitDir
                + "Rich"
                + str(self.whichRich)
                + "MirrCombinFitNGEOut_"
                + self.thisNameStr
                + connectStr
                + tiltName
                + "_i"
                + str(n_it)
                + ".txt"
            )

            os.system(
                "cat " + fitOutputFile + ' | grep " not_good_enough " > ' + fitNGEFile
            )
            if not os.path.exists(fitNGEFile):
                print("ERROR: no fitNGEFile.")
            num_lines = sum(1 for line in open(fitNGEFile))
            if num_lines != 0:
                print(
                    "WARNING: Not enough well populated phi-bins in all mirror combination histograms. Will perform all fits anyway."
                )
                sys.stdout.flush()
                return False
        sys.stdout.flush()
        return True

    def old_hasConverged(self):
        if not os.path.exists(
            self.workdir + "rich" + str(self.whichRich) + "_stop_or_continue.txt"
        ):
            print(
                "ERROR: no rich" + str(self.whichRich) + "_stop_or_continue_txt file."
            )
        rich_stop_or_continue_txt = open(
            self.workdir + "rich" + str(self.whichRich) + "_stop_or_continue.txt"
        )
        verdict = rich_stop_or_continue_txt.readline().strip().upper()
        rich_stop_or_continue_txt.close()
        print("INFO: Verdict: " + str(verdict))
        sys.stdout.flush()
        if verdict == "STOP":
            return True
        if verdict != "STOP" and verdict != "STOP_NAN" and verdict != "STOP_NGE":
            return False

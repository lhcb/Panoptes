###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# https://gitlab.cern.ch/lhcb/Panoptes/-/blob/alex_branch/Rich/Panoptes/options/hlt2_reco_rich2_panoptes.py
# https://gitlab.cern.ch/lhcb/Alignment/-/blob/master/Alignment/Humboldt/options/AlignVPHalvesModules_Analyzer.py
# https://gitlab.cern.ch/lhcb/Online/-/blob/master/Online/FarmConfig/job/AlignVPHalvesModules.sh
# https://gitlab.cern.ch/lhcb/Panoptes/-/blob/alex_branch/Rich/Panoptes/python/Panoptes/alignment.py

import csv
import datetime
import glob
import math
import re
import sys

from ruamel.yaml import YAML

################# currently taking in n_it and tiltName from stdin --- this will need to change #################

arguments = []
if not sys.stdin.isatty():
    for line in sys.stdin:
        for word in line.split():
            arguments.append(word)

count = len(arguments)

# You can pipe in to stdin to change tiltName and n_it
# echo -n <tiltName> <n_it> | Panoptes/run gaudirun.py ...
# OR
# must have pipe in to gaudirun.py to change n_it only
# echo -n <n_it> | Panoptes/run gaudirun.py ...
# otherwise tiltName = "" and n_it = 0

# Since we are doing this by hand, specify the iteration number in stdin
n_it = 0
if count == 2 and arguments[1] in ["0", "1", "2", "3", "4", "5", "6", "7", "8"]:
    n_it = int(arguments[1])
if count == 1 and arguments[0] in ["0", "1", "2", "3", "4", "5", "6", "7", "8"]:
    n_it = int(arguments[0])
print("Iteration: " + str(n_it))

# possible tiltname can be given via stdin
tiltName = ""
if count == 2 and arguments[0] in [
    "priYn",
    "priYp",
    "priZn",
    "priZp",
    "secYn",
    "secYp",
    "secZn",
    "secZp",
]:
    tiltName = arguments[0]
print("Tilt Name: " + tiltName)

################# ############################################################################# #################

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad
from Moore import (
    options,
    run_reconstruction,
)
from Panoptes.alignment import (
    standalone_rich_online_align_reco,
)
from PyConf.Algorithms import (
    PrForwardTrackingVelo,
    VeloRetinaClusterTrackingSIMD,
    VPRetinaFullClusterDecoder,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_PatPV3DFuture_pvs,
    make_reco_pvs,
    make_velo_full_clusters,
    make_VeloClusterTrackingSIMD,
)
from RecoConf.rich_data_monitoring import (
    alignment_rich_monitoring_options,
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import default_rich_reco_options

"""Options for running over data with FT raw bank version 6."""
from RecoConf.decoders import (
    default_ft_decoding_version,
)

useRealData = True

options.n_threads = 10  # Maybe something else will override this anyway?
# options.evt_max = 25000000
if useRealData is True:
    options.simulation = False
    options.input_type = "MDF"

useDD4Hep = True

if useDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    from DDDB.CheckDD4Hep import UseDD4Hep
else:
    UseDD4Hep = False

print("Using DD4Hep: ", UseDD4Hep)

whichRich = 1  # PN - really we want this defined when we call the Analyzer, e.g. RichAnalyzer_Run3(whichRich=1)

# does this bit still work?
if whichRich == 1:
    from Configurables import Rich1MirrAlignOnConf

    alignConf = Rich1MirrAlignOnConf()
elif whichRich == 2:
    from Configurables import Rich2MirrAlignOnConf

    alignConf = Rich2MirrAlignOnConf()
alignConf.__apply_configuration__()

# whichRich = alignConf.getProp('Rich') - not sure why we would ever do this
print("INFO: (RichAnalyzer.py) whichRich = ", whichRich)
workdir = alignConf.getProp("WorkDir")
print("INFO: (RichAnalyzer.py) workdir = ", workdir)
whichData = alignConf.getProp("dataVariant")  # not sure if we will use this or not...
print("INFO: (RichAnalyzer.py) whichData = ", whichData)

rich = "rich" + str(whichRich)
radiator = "Rich" + str(whichRich) + "Gas"

if UseDD4Hep:
    from Configurables import DDDBConf

    dd4hep = DD4hepSvc(
        DetectorList=["/world", "Magnet", "UT", "VP", "FT", "Rich1", "Rich2"]
    )
    dd4hep.ConditionsLocation = (
        "git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"
    )
    dd4hep.OutputLevel = 1

    if useRealData:
        options.conddb_tag = "master"  # used for real data
        options.dddb_tag = "run3/trunk"
        DDDBConf().GeometryVersion = "run3/trunk"  # used for real data

        ## PN - eventually will want to do this all the Online way, for now comment out
        # dd4hep.UseConditionsOverlay = True
        # if (tiltName == "" and n_it == 0):
        #    dd4hep.ConditionsVersion = 'alignment2022'
        # elif (n_it >= 1):
        #    dd4hep.ConditionsVersion = '2022_12_06_Rich'+str(whichRich)+'_i' + str(n_it) + "_new"
        # else:
        #    dd4hep.ConditionsVersion = '2022_12_06_Rich'+str(whichRich)+'_' + tiltName

##### Online Stuff (to be sorted) #####

# from Humboldt.utils import runAlignment
import OnlineEnvBase as OnlineEnv
from PyConf.application import configure_input

# TODO: get these from Online
runNumber = 9999
workerNumber = 0
runType = "RICH" + str(whichRich)
if OnlineEnv.PartitionName == "TEST":
    histofile = "testRICHmonitoringhist.root"  # for now we just use this / later match Anatoly's scheme
else:
    from pathlib import Path

    onlineprefix = "/group/online/dataflow/cmtuser/alignonlinetest"
    Path(f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput_RICH/").mkdir(
        parents=True, exist_ok=True
    )
    histofile = f"{onlineprefix}/{runType}/{runNumber}/analyzerOutput_RICH/{workerNumber}_testRICHmonitoringhist.root"

##### ########################### #####

# set options above this line!
options.histo_file = histofile
configure_input(options)

default_moni_opts = {}
default_reco_opts = {}
align_opts = {}

# Reco opts
wider_bkg = {"PhotonSelection": "None"}
default_reco_opts.update(wider_bkg)
panel_select = {"ActivatePanel": (True, True)}
default_reco_opts.update(panel_select)

# Monitoring opts
if whichRich == 1:
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": 30.0 * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }
else:
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": 60.0 * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }
default_moni_opts.update(tighter_minp)
wider_histo = {"CKResHistoRange": (0.025, 0.005, 0.004)}
default_moni_opts.update(wider_histo)
useUT = {"UseUT": False}
default_moni_opts.update(useUT)

PrForwardTrackingVelo.global_bind(
    MinQuality=0.0,
    DeltaQuality=0.0,
    DecisionLDA=-99.0,
    MinTotalHits=9,
    MaxChi2PerDoF=50.0,
    MaxChi2XProjection=60.0,
    MaxChi2PerDoFFinal=28.0,
    MaxChi2Stereo=16.0,
    MaxChi2StereoAdd=16.0,
)

make_VeloClusterTrackingSIMD.global_bind(algorithm=VeloRetinaClusterTrackingSIMD)
make_velo_full_clusters.global_bind(make_full_cluster=VPRetinaFullClusterDecoder)
make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)

mirror_align_tasks = [
    "Produce",  # fill the production set of histograms
    #'Monitor',   # add various checking histograms
    #'Map',       # add counters for creation of the HLT1 pre-selection line "map"
    #'Optimize',  # add counters for optimization of the RICH2 mirror combinations subset
    #'Select',    # check filling the rest of RICH2 miralignment_rich_reco_optionsror combinations along with 8 poorest
    #'Calibrate', # check filling all RICH2 mirror combinations with elimination when filled
    # 'Explore',   # explore influence of RICH1 coordinate systems on distribution shapes
]

if "md" in options.conddb_tag:
    mp = "md"
elif "mu" in options.conddb_tag:
    mp = "mu"
else:
    mp = "Collision22"  # PN - should fix this later

# PN - I don't think we will know the number of events a priori
# n_kevts = str(math.trunc(options.evt_max / 1000)) + 'k'

if whichRich == 1:
    addinfo = "_minp30_subset_" + mp  # + "_" + n_kevts
    # addinfo = '_minp40_subset_' + mp #+ "_" + n_kevts
else:
    addinfo = "_minp60_subset_" + mp  # + "_" + n_kevts

# cumulative substring with all tasks chosen
tasks = ""
for task in mirror_align_tasks:
    tasks += "_" + task

yaml = YAML()
with (
    open(workdir + "/" + rich + "_session_timestamp.yml") as inp
):  # file made in SetupHelper; need to provide to Analyzer via conf the directory of this file
    ts = yaml.load(inp)

# form the current variant name
current_variant = ts + "_" + "rich" + str(whichRich) + addinfo + tasks

if "subset" in current_variant:
    if whichRich == 1:
        central_mirr_combs = {
            "PrebookHistos": [
                ("p00", "s03"),
                ("p01", "s06"),
                ("p02", "s12"),
                ("p03", "s09"),
            ],
        }
        align_opts.update(central_mirr_combs)
    else:
        central_mirr_combs = {
            "PrebookHistos": [
                ("p17", "s13"),
                ("p47", "s35"),
                ("p17", "s09"),
                ("p47", "s31"),
                ("p16", "s12"),
                ("p46", "s34"),
                ("p16", "s08"),
                ("p46", "s30"),
                ("p13", "s10"),
                ("p43", "s31"),
                ("p13", "s09"),
                ("p43", "s30"),
                ("p12", "s09"),
                ("p42", "s30"),
                ("p12", "s08"),
                ("p42", "s29"),
                ("p09", "s09"),
                ("p39", "s31"),
                ("p09", "s05"),
                ("p39", "s27"),
                ("p08", "s08"),
                ("p38", "s30"),
                ("p08", "s04"),
                ("p38", "s26"),
            ],
        }
        align_opts.update(central_mirr_combs)

if "minp40" in current_variant:
    align_opts.update({"MinP4Align": 40.0 * GeV})
elif "minp30" in current_variant:
    align_opts.update({"MinP4Align": 30.0 * GeV})
elif "minp60" in current_variant:
    align_opts.update({"MinP4Align": 60.0 * GeV})

if "theta2" in current_variant:
    align_opts.update({"DeltaThetaRange": 0.002})

# PN - no idea what this is for
align_opts.update({"PoorestPopulation": 279.0})

if tiltName == "":
    connectStr = ""
else:
    connectStr = "_"

# save output file with histograms
# if UseDD4Hep:
#    options.histo_file = "./" + current_variant + "_histos" + connectStr + tiltName + "_i" + str(n_it) + ".root"

# prepare part of the overridden options in use
# to be recorded into a YAML file
align_opts_dump = alignment_rich_monitoring_options(
    radiator=radiator, init_override_opts=align_opts
)

# add overridden default_rich_monitoring_options
# to the part of the options in use to be recorded into the YAML file
align_opts_dump.update(
    default_rich_monitoring_options(init_override_opts=default_moni_opts)
)

# retrieve the timestamp
ts = re.findall(r"[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}-[\d]{2}", current_variant)[0]

# prepare options in use to be recorded into the YAML file
opts_dump = {}
opts_dump[ts] = {}
opts_dump[ts]["reco_opts"] = align_opts_dump

# append YAML file with the part of the options in use
with open(workdir + "/" + rich + "_reco_opts.yml", "a") as out:
    yaml.dump(opts_dump, out)

## PN - incorporate this later?
## iteration number substring
# with open(workdir + '/' + rich +'_iter_number.yml') as inp:
#    iN = '_i'+str(yaml.load(inp))

with (
    standalone_rich_online_align_reco.bind(
        RichGas=radiator, MirrorAlignTasks=mirror_align_tasks, noUT=True
    ),
    alignment_rich_monitoring_options.bind(
        radiator=radiator, init_override_opts=align_opts
    ),
    default_ft_decoding_version.bind(value=6),
    default_rich_reco_options.bind(init_override_opts=default_reco_opts),
    default_rich_monitoring_options.bind(init_override_opts=default_moni_opts),
):
    run_reconstruction(options, standalone_rich_online_align_reco)

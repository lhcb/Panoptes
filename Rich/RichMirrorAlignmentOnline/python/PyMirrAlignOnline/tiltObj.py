###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Claire Prouve, Paras Naik, Vidar Marsh"
__copyright__ = "University of Bristol, August 2021"


class tiltObj:
    def __init__(self, _alignConf_or_whichRich):
        # _alignConf_or_whichRich can actually be an alignment Configuration *or* just a Rich number.
        # If it is the latter then of course writeChange() will not work.
        # This feature has been enabled to allow this most current tiltObj.py
        #  to also be used with Claire and Sam M.-M.'s plotting software
        self.alignConf = _alignConf_or_whichRich
        if self.alignConf == 1 or self.alignConf == 2:
            self.whichRich = self.alignConf
        else:  # two different types were compared with ==, which should always be false
            self.whichRich = self.alignConf.getProp("Rich")
        import re

        if self.whichRich == 1:
            # self.priRe = re.compile('SphMirror[0-3]_Align')
            # self.secRe = re.compile('SecMirror([0-9]|1[0-5])_Align')
            self.priRe = re.compile("R1M1Seg[0-9][0-9]")
            self.secRe = re.compile("R1M2Seg[0-9][0-9]")
        else:
            # self.priRe = re.compile('Rich2SphMirror[0-9]*_Align')
            # self.secRe = re.compile('Rich2SecMirror[0-9]*_Align')
            self.priRe = re.compile("R2M1Seg[0-9][0-9]")
            self.secRe = re.compile("R2M2Seg[0-9][0-9]")

        self.priY = {}
        self.priZ = {}
        self.secY = {}
        self.secZ = {}

        self.priYtrunc = {}
        self.priZtrunc = {}
        self.secYtrunc = {}
        self.secZtrunc = {}

        self.priYdiv = {}
        self.priZdiv = {}
        self.secYdiv = {}
        self.secZdiv = {}

        if self.whichRich == 1:
            self.maxpri = 3
            self.maxsec = 15
        if self.whichRich == 2:
            self.maxpri = 55
            self.maxsec = 39

        for i in range(0, self.maxpri + 1):
            self.priY[i] = []
            self.priZ[i] = []
        for i in range(0, self.maxsec + 1):
            self.secY[i] = []
            self.secZ[i] = []

    def writeChange(self, n):
        import os

        # this will only work if alignConf is actually alignConf and not a Rich number
        filename = (
            self.alignConf.getProp("WorkDir")
            + "Rich"
            + str(self.whichRich)
            + "_ChangeWRTDB_"
            + str(n)
            + ".txt"
        )
        if os.path.exists(filename):
            os.remove(filename)
        f = open(filename, "w")
        args = "Primary mirrors \n"
        for i in range(0, self.maxpri + 1):
            args += (
                str(i) + "    " + str(self.priY[i]) + "     " + str(self.priZ[i]) + "\n"
            )
        args += "\n"
        args += "Secondary mirrors \n"
        for i in range(0, self.maxsec + 1):
            args += (
                str(i) + "    " + str(self.secY[i]) + "     " + str(self.secZ[i]) + "\n"
            )
        f.write(args)
        f.close()

    def setChangeYAML(self, files):
        import re

        import ruamel.yaml

        class condition:
            yaml_tag = "!alignment"

        yaml = ruamel.yaml.YAML()
        yaml.register_class(condition)
        if self.whichRich == 1:
            # self.priRe = re.compile('SphMirror[0-3]_Align')
            # self.secRe = re.compile('SecMirror([0-9]|1[0-5])_Align')
            self.priRe = re.compile("R1M1Seg[0-9][0-9]")
            self.secRe = re.compile("R1M2Seg[0-9][0-9]")
        else:
            # self.priRe = re.compile('Rich2SphMirror[0-9]*_Align')
            # self.secRe = re.compile('Rich2SecMirror[0-9]*_Align')
            self.priRe = re.compile("R2M1Seg[0-9][0-9]")
            self.secRe = re.compile("R2M2Seg[0-9][0-9]")
        tree = {}
        with open(files[0], "r") as f:
            tree[0] = yaml.load(f)
        with open(files[1], "r") as f:
            tree["N"] = yaml.load(f)

        tilts0 = self.yamlToDict(tree[0])
        tiltsN = self.yamlToDict(tree["N"])

        for i in range(0, self.maxpri + 1):
            self.priY[i] = tilts0[0][i] - tiltsN[0][i]
            self.priZ[i] = tilts0[1][i] - tiltsN[1][i]

        for i in range(0, self.maxsec + 1):
            self.secY[i] = tilts0[2][i] - tiltsN[2][i]
            self.secZ[i] = tilts0[3][i] - tiltsN[3][i]

    def yamlToDict(self, tree):
        priTiltY = {}
        priTiltZ = {}
        secTiltY = {}
        secTiltZ = {}
        for mirrNum in tree:
            if self.priRe.match(mirrNum):
                rotation = tree[mirrNum].rotation[1:]
                mirrNum = mirrNum[4:]
                # mirrNum = float(mirrNum.strip("SphMirror_Align"))
                mirrNum = float(mirrNum.strip("Seg"))
                for i in range(len(rotation)):
                    rotValue = rotation[i]
                    rotValue = str(rotValue).split("*")[0]
                    rotValue = float(rotValue)
                    if i == 0:
                        priTiltY[mirrNum] = rotValue
                    elif i == 1:
                        priTiltZ[mirrNum] = rotValue

            elif self.secRe.match(mirrNum):
                rotation = tree[mirrNum].rotation[1:]
                mirrNum = mirrNum[4:]
                # mirrNum = float(mirrNum.strip("SecMirror_Align"))
                mirrNum = float(mirrNum.strip("Seg"))
                for i in range(len(rotation)):
                    rotValue = rotation[i]
                    rotValue = str(rotValue).split("*")[0]
                    rotValue = float(rotValue)
                    if i == 0:
                        secTiltY[mirrNum] = rotValue
                    elif i == 1:
                        secTiltZ[mirrNum] = rotValue
        return [priTiltY, priTiltZ, secTiltY, secTiltZ]

    def setChange(self, files):
        from lxml import etree

        print("INFO: compareXML ", files[0])
        print("INFO: self.lastXMLFile ", files[1])
        import sys

        sys.stdout.flush()
        parser = etree.XMLParser(remove_blank_text=True)

        tree0 = etree.parse(files[0], parser)
        treeN = etree.parse(files[1], parser)

        tilts0 = self.xmlToDict(tree0)
        tiltsN = self.xmlToDict(treeN)

        for i in range(0, self.maxpri + 1):
            self.priY[i] = tilts0[0][i] - tiltsN[0][i]
            self.priZ[i] = tilts0[1][i] - tiltsN[1][i]

        for i in range(0, self.maxsec + 1):
            self.secY[i] = tilts0[2][i] - tiltsN[2][i]
            self.secZ[i] = tilts0[3][i] - tiltsN[3][i]

    def getChange(self):
        return [self.priY, self.priZ, self.secY, self.secZ]

    def getGraphs(self):
        from ROOT import TGraph, TMultiGraph

        graph_priY = TGraph(self.maxpri)
        graph_priZ = TGraph(self.maxpri)
        graph_secY = TGraph(self.maxsec)
        graph_secZ = TGraph(self.maxsec)

        for i in range(0, self.maxpri + 1):
            graph_priY.SetPoint(i, i, self.priY[i])
            graph_priZ.SetPoint(i, i, self.priZ[i])
        for i in range(0, self.maxsec + 1):
            graph_secY.SetPoint(i, i, self.secY[i])
            graph_secZ.SetPoint(i, i, self.secZ[i])

        graph_priY.SetMarkerStyle(8)
        graph_priY.SetMarkerColor(2)  # red
        graph_priZ.SetMarkerStyle(8)
        graph_priZ.SetMarkerColor(4)  # blue
        graph_secY.SetMarkerStyle(8)
        graph_secY.SetMarkerColor(2)
        graph_secZ.SetMarkerStyle(8)
        graph_secZ.SetMarkerColor(4)

        mg1 = TMultiGraph()
        mg1.Add(graph_priY)
        mg1.Add(graph_priZ)
        mg2 = TMultiGraph()
        mg2.Add(graph_secY)
        mg2.Add(graph_secZ)

        return [mg1, mg2]

    def xmlToDict(self, tree):
        if self.whichRich == 1:
            self.priRe = re.compile("SphMirror[0-3]_Align")
            self.secRe = re.compile("SecMirror([0-9]|1[0-5])_Align")
        else:
            self.priRe = re.compile("Rich2SphMirror[0-9]*_Align")
            self.secRe = re.compile("Rich2SecMirror[0-9]*_Align")
        priTiltY = {}
        priTiltZ = {}
        secTiltY = {}
        secTiltZ = {}
        root = tree.getroot()
        for cond in root.findall("condition"):
            if self.priRe.match(cond.attrib["name"]):
                for paramV in cond.findall("paramVector"):
                    if paramV.attrib["name"] == "dRotXYZ":
                        dRotXYZ = paramV.text.strip("*mrad").split("*mrad")
                        if self.whichRich == 2:
                            mirrNumber = cond.attrib["name"].replace("Rich2", "")
                        else:
                            mirrNumber = cond.attrib["name"]
                        mirrNumber = mirrNumber.strip("SphMirror_Align")
                        priTiltY[int(float(mirrNumber))] = float(dRotXYZ[1])
                        priTiltZ[int(float(mirrNumber))] = float(dRotXYZ[2])

            elif self.secRe.match(cond.attrib["name"]):
                for paramV in cond.findall("paramVector"):
                    if paramV.attrib["name"] == "dRotXYZ":
                        dRotXYZ = paramV.text.strip("*mrad").split("*mrad")
                        if self.whichRich == 2:
                            mirrNumber = cond.attrib["name"].replace("Rich2", "")
                        else:
                            mirrNumber = cond.attrib["name"]
                        mirrNumber = mirrNumber.strip("SecMirror_Align")
                        secTiltY[int(float(mirrNumber))] = float(dRotXYZ[1])
                        secTiltZ[int(float(mirrNumber))] = float(dRotXYZ[2])

        return [priTiltY, priTiltZ, secTiltY, secTiltZ]

    def getAbsMax(self):
        biggestTilt = [0, 0, 0, 0]
        for pri in range(0, self.maxpri + 1):
            if abs(self.priY[pri]) > biggestTilt[0]:
                biggestTilt[0] = abs(self.priY[pri])
            if abs(self.priZ[pri]) > biggestTilt[1]:
                biggestTilt[1] = abs(self.priZ[pri])
        for sec in range(0, self.maxsec + 1):
            if abs(self.secY[sec]) > biggestTilt[2]:
                biggestTilt[2] = abs(self.secY[sec])
            if abs(self.secZ[sec]) > biggestTilt[3]:
                biggestTilt[3] = abs(self.secZ[sec])
        return biggestTilt

    def setTrunc(self, digits=2):
        # can only be done *after* a setChange(files)
        # truncated-not-rounded-version of the mirror compensations already stored in self.priY, self.priZ, etc...
        import math

        if int(digits) < 0:
            import sys

            sys.exit("ERROR: Non-sensical number of digits for setTrunc.")
        divisor = float(math.pow(10, int(digits)))
        for pY in self.priY.keys():
            if not abs(self.priY[pY]) < math.pow(10, int(0 - digits)):
                self.priYtrunc[pY] = (
                    math.floor(self.priY[pY] * divisor) / divisor + 0.0000001
                    if self.priY[pY] >= 0.0
                    else math.ceil(self.priY[pY] * divisor) / divisor - 0.0000001
                )
            elif self.priY[pY] >= 0:
                self.priYtrunc[pY] = 0.0000001
            else:
                self.priYtrunc[pY] = -0.0000001
        for pZ in self.priZ.keys():
            if not abs(self.priZ[pZ]) < math.pow(10, int(0 - digits)):
                self.priZtrunc[pZ] = (
                    math.floor(self.priZ[pZ] * divisor) / divisor + 0.0000001
                    if self.priZ[pZ] >= 0.0
                    else math.ceil(self.priZ[pZ] * divisor) / divisor - 0.0000001
                )
            elif self.priZ[pZ] >= 0:
                self.priZtrunc[pZ] = 0.0000001
            else:
                self.priZtrunc[pZ] = -0.0000001
        for sY in self.secY.keys():
            if not abs(self.secY[sY]) < math.pow(10, int(0 - digits)):
                self.secYtrunc[sY] = (
                    math.floor(self.secY[sY] * divisor) / divisor + 0.0000001
                    if self.secY[sY] >= 0.0
                    else math.ceil(self.secY[sY] * divisor) / divisor - 0.0000001
                )
            elif self.secY[sY] >= 0:
                self.secYtrunc[sY] = 0.0000001
            else:
                self.secYtrunc[sY] = -0.0000001
        for sZ in self.secZ.keys():
            if not abs(self.secZ[sZ]) < math.pow(10, int(0 - digits)):
                self.secZtrunc[sZ] = (
                    math.floor(self.secZ[sZ] * divisor) / divisor + 0.0000001
                    if self.secZ[sZ] >= 0.0
                    else math.ceil(self.secZ[sZ] * divisor) / divisor - 0.0000001
                )
            elif self.secZ[sZ] >= 0:
                self.secZtrunc[sZ] = 0.0000001
            else:
                self.secZtrunc[sZ] = -0.0000001

    def setDiv(self, digits=0, tolerances=None):  # [pYtol,pZtol,sYtol,sZtol]
        # can only be done *after* a setChange(files)
        # truncated-not-rounded-version of the mirror compensations, already stored in self.priY, self.priZ, etc..., divided by the tolerances
        # NOTE: If digits = 0, values within the tolerances will all show up as "-0" or "0"
        import math

        if int(digits) < 0:
            import sys

            sys.exit("ERROR: Non-sensical number of digits for setDiv.")
        divisor = float(math.pow(10, int(digits)))
        if tolerances is None:
            tolerances = [0.1, 0.1, 0.1, 0.1]
        for pY in self.priY.keys():
            if not abs(self.priY[pY] / tolerances[0]) < math.pow(10, int(0 - digits)):
                self.priYdiv[pY] = (
                    math.floor(self.priY[pY] / tolerances[0] * divisor) / divisor
                    + 0.0000001
                    if self.priY[pY] >= 0.0
                    else math.ceil(self.priY[pY] / tolerances[0] * divisor) / divisor
                    - 0.0000001
                )
            elif self.priY[pY] >= 0:
                self.priYdiv[pY] = 0.0000001
            else:
                self.priYdiv[pY] = -0.0000001
        for pZ in self.priZ.keys():
            if not abs(self.priZ[pZ] / tolerances[1]) < math.pow(10, int(0 - digits)):
                self.priZdiv[pZ] = (
                    math.floor(self.priZ[pZ] / tolerances[1] * divisor) / divisor
                    + 0.0000001
                    if self.priZ[pZ] >= 0.0
                    else math.ceil(self.priZ[pZ] / tolerances[1] * divisor) / divisor
                    - 0.0000001
                )
            elif self.priZ[pZ] >= 0:
                self.priZdiv[pZ] = 0.0000001
            else:
                self.priZdiv[pZ] = -0.0000001
        for sY in self.secY.keys():
            if not abs(self.secY[sY] / tolerances[2]) < math.pow(10, int(0 - digits)):
                self.secYdiv[sY] = (
                    math.floor(self.secY[sY] / tolerances[2] * divisor) / divisor
                    + 0.0000001
                    if self.secY[sY] >= 0.0
                    else math.ceil(self.secY[sY] / tolerances[2] * divisor) / divisor
                    - 0.0000001
                )
            elif self.secY[sY] >= 0:
                self.secYdiv[sY] = 0.0000001
            else:
                self.secYdiv[sY] = -0.0000001
        for sZ in self.secZ.keys():
            if not abs(self.secZ[sZ] / tolerances[3]) < math.pow(10, int(0 - digits)):
                self.secZdiv[sZ] = (
                    math.floor(self.secZ[sZ] / tolerances[3] * divisor) / divisor
                    + 0.0000001
                    if self.secZ[sZ] >= 0.0
                    else math.ceil(self.secZ[sZ] / tolerances[3] * divisor) / divisor
                    - 0.0000001
                )
            elif self.secZ[sZ] >= 0:
                self.secZdiv[sZ] = 0.0000001
            else:
                self.secZdiv[sZ] = -0.0000001

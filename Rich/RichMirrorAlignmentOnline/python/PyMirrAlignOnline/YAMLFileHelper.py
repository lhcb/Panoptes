###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Claire Prouve, Paras Naik, Vidar Marsh"
__copyright__ = "University of Bristol, August 2021"

import distutils
import getpass
import math
import os
import re
import shutil
import sys
import threading
import time
from distutils import dir_util
from subprocess import *
from time import gmtime, sleep, strftime, time

import Configurables


class YAMLFileHelper:
    def __init__(self, configuration):
        self.alignConf = configuration
        self.whichRich = self.alignConf.getProp("Rich")
        richFilesDir = self.alignConf.getProp(
            "richFiles"
        )  # '/group/rich/AlignmentFiles/'
        self.base_dir_for_testing_and_maybe = (
            f"{richFilesDir}testingMaybe/Rich" + str(self.whichRich) + "/Mirrors/"
        )

        # PN: Until we are ready to use the real Run 3 Online CondDB location,
        #       we use the testing/maybe basedir.
        #     Note this means we manually must keep track of the vN numbers in the testing/maybe basedir
        #     When we move to the Online basedir, we need to make sure they want us to use vN and not just N or something else
        #
        # This is the online base_dir, we should pick up the current alignment and drop any new alignment here
        # Note that any alignment dropped in .pool is there FOREVER (as it gets swept into the true CondDB quite often)
        # Do not drop an alignment there until you can guarantee it is better than the last alignment!
        self.base_dir = (
            "/group/online/hlt/conditions.run3/lhcb-conditions-database/Conditions/Rich"
            + str(self.whichRich)
            + "/Alignment/Mirrors.yml/.pool/"
        )

        # self.base_dir = '/group/online/AligWork/tempAlignmentFiles/lhcb-conditions-database_Feb24/Conditions/Rich' + str(self.whichRich) + '/Alignment/starting_points/'

        # PN: For now all alignments go into the testingMaybe area
        # Therefore we are OVERWRITING self.base_dir here.
        # If you put a new alignment into alignment2022 or similar branch of the new CondDB,
        #   you must have ALSO placed it here as the latest vN for it to get picked up as the starting mirror alignment!
        """
        Comment this next line out ONLY when ready and wanting to automatically post v-numbers to .pool!
        Uncomment when you want v-number updates to post to the testing_and_maybe area for testing
        """
        self.base_dir = (
            f"{richFilesDir}testingMaybe/Rich" + str(self.whichRich) + "/Mirrors/"
        )  # use the same base_dir as for testing/maybe alignments

        self.latest = None

    def create_coeff_calibration_yml_files(self, inputFile, n_it):
        print("INFO: start create coeff")
        writedir = self.alignConf.getProp("WorkDir")
        coeffCalibTilt = self.alignConf.getProp("coeffCalibTilt")
        thisNameStr = self.alignConf.getProp("nameStr")
        import ruamel.yaml

        class Condition:
            yaml_tag = "!alignment"

        yaml = ruamel.yaml.YAML()
        with open(
            writedir + "/" + "rich" + str(self.whichRich) + "_variant.yml"
        ) as finp:
            variant = yaml.load(finp)

        yaml = ruamel.yaml.YAML()
        yaml.register_class(Condition)
        sys.path.append(inputFile)
        with open(inputFile, "r") as f:
            RichAlignCon = yaml.load(f)
        # signName = dict({'-': 'neg', '+': 'pos'})
        signName = dict({"-": "n", "+": "p"})
        signToShift = dict({"-": -coeffCalibTilt, "+": coeffCalibTilt})
        # mirrTypeName = dict({'pri': 'Sph', 'sec': 'Sec'})
        mirrTypeName = dict({"pri": "1", "sec": "2"})
        if self.whichRich == 2:
            totMirrNum = dict({"pri": 56, "sec": 40})
        elif self.whichRich == 1:
            totMirrNum = dict({"pri": 4, "sec": 16})
        else:
            raise ValueError('RICH detector "{}" does not exist'.format(self.whichRich))
        seqNum = dict({"Y": 1, "Z": 2})
        for mirrType in ["pri", "sec"]:
            for axis in ["Y", "Z"]:
                for sign in ["-", "+"]:
                    with open(inputFile, "r") as f:
                        RichAlignCon = yaml.load(f)
                    for mirrNum in range(totMirrNum[mirrType]):
                        # if self.whichRich == 1:
                        #    mirrName = "{}Mirror{}_Align".format\
                        #        (mirrTypeName[mirrType], mirrNum)
                        # if self.whichRich == 2:
                        #    mirrName = "Rich{}{}Mirror{}_Align".format\
                        #        (self.whichRich, mirrTypeName[mirrType], \
                        #            mirrNum)
                        mirrName = "R{}M{}Seg{}".format(
                            self.whichRich, mirrTypeName[mirrType], mirrNum
                        )
                        # add 0, e.g. R1M1Seg5 -> R1M1Seg05
                        if len(mirrName) == 8:
                            mirrName = mirrName[:-1] + "0" + mirrName[-1:]

                        dRotXYZ_unit = RichAlignCon[mirrName].rotation
                        # NOTE: dRotXYZ_unit it NOT an ordinary list but a
                        # ruamel.yaml object. This list must be re-inserted
                        # into RichAlingCon after the values have been
                        # updated to preserve the output file structure.
                        dRotXYZ_new = []
                        units = []
                        for i in range(len(dRotXYZ_unit)):
                            try:
                                rotValue, unit = dRotXYZ_unit[i].split("*")
                                rotValue = float(rotValue.strip())
                            except:
                                rotValue = float(dRotXYZ_unit[i])
                                unit = None
                            dRotXYZ_new.append(rotValue)
                            units.append(unit)
                        dRotXYZ_new[seqNum[axis]] += signToShift[sign]
                        for i in range(len(dRotXYZ_new)):
                            dRotXYZ_unit[i] = round(dRotXYZ_new[i], 5)
                            if units[i] != None:
                                dRotXYZ_unit[i] = "{} * {}".format(
                                    dRotXYZ_unit[i], units[i]
                                )
                        # Note comment about dRotXYZ_unit
                        RichAlignCon[mirrName].rotation = dRotXYZ_unit
                        if axis == "Y":
                            # signCombinName = signName[sign] + 'YzerZ'
                            signCombinName = "Y" + signName[sign]
                        else:
                            # signCombinName = 'zerY' + signName[sign] + 'Z'
                            signCombinName = "Z" + signName[sign]
                    # with open(writedir + 'Rich' + str(self.whichRich) \
                    #    + 'CondDBUpdate_' + thisNameStr + "_" \
                    #        + mirrType + "_" + signCombinName + "_i" \
                    #            + str(n_it) + ".yml", 'w') as f:
                    #    yaml.dump(RichAlignCon, f)
                    with open(
                        writedir
                        + variant
                        + "_conds_"
                        + mirrType
                        + signCombinName
                        + "_i"
                        + str(n_it)
                        + ".yml",
                        "w",
                    ) as f:
                        yaml.dump(RichAlignCon, f)

    def getStartYAMLOrigFileName(self):
        import os
        import re

        # re_version = re.compile(r"^v([0-9]+)\.yml$")
        re_version = re.compile(
            r"^v([0-9]+)(?:\.yml)?$"
        )  # should match v123 and v123.yml but not v123_testing_maybe_20230505_171822.yml

        def get_version(entry):
            r = re_version.match(entry)
            if not r:
                return -1
            else:
                return int(r.group(1))

        latestName = max(os.listdir(self.base_dir), key=get_version)
        return latestName

    def getStartYAML(self, startyml):
        import os

        self.latest = self.alignConf.getProp("startYAMLFile")

        if (self.latest == "") or (self.alignConf.getProp("testing") is not True):
            self.latest = self.getStartYAMLOrigFileName()
            # self.latest = "200000"
            # self.latest = "rich1sajanpivots_i3"
            self.latest = os.path.join(self.base_dir, self.latest)

        print("INFO: This file was picked up as the starting yml:", self.latest)

        with open(startyml, "w") as outfile:
            # outfile.write('<?xml version=\'1.0\' encoding=\'iso-8859-1\'?> \n')
            # outfile.write(
            #    '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n')
            # outfile.write('<DDDB> \n')
            if not os.path.exists(self.latest):
                print("ERROR: no latest file.")
            with open(self.latest) as infile:
                for line in infile:
                    outfile.write(line)
            #    outfile.write('</DDDB> \n')

    def getCompareYAML(self, compareYAML):
        import os

        compareYAMLFile = self.alignConf.getProp("compareYAMLFile")

        if compareYAMLFile == "":
            compareYAMLFile = self.getStartYAMLOrigFileName()
            compareYAMLFile = os.path.join(self.base_dir, compareYAMLFile)

        print("INFO: This file was picked up as the compareYAMLFile:", compareYAMLFile)

        with open(compareYAML, "w") as outfile:
            #    outfile.write('<?xml version=\'1.0\' encoding=\'iso-8859-1\'?> \n')
            #    outfile.write(
            #        '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n')
            #    outfile.write('<DDDB> \n')
            if not os.path.exists(compareYAMLFile):
                print("ERROR: no compareYAMLFile.")
            with open(compareYAMLFile) as infile:
                for line in infile:
                    outfile.write(line)
        #        outfile.write('</DDDB> \n')

    def finalYAML(self, vN_DB_str, Update):
        workdir = self.alignConf.getProp("WorkDir")

        finalYAML = workdir + "CondDB_Update_Rich" + str(self.whichRich) + ".yml"
        tocopyYAML = workdir + "CondDB_Rich" + str(self.whichRich) + ".yml"

        if not os.path.exists(tocopyYAML):
            print("ERROR: no tocopyYAML file.")
        with open(tocopyYAML, "r") as infile:
            with open(finalYAML, "w") as outfile:
                for line in infile:
                    if (
                        "<?xml version='1.0' encoding='iso-8859-1'?>" not in line
                        and '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd">'
                        not in line
                        and "<DDDB>" not in line
                        and "</DDDB>" not in line
                    ):
                        outfile.write(line)
        if Update:
            if "testing" in vN_DB_str or "maybe" in vN_DB_str:
                self.latest = vN_DB_str + ".yml"
                self.latest = os.path.join(
                    self.base_dir_for_testing_and_maybe, self.latest
                )
            else:
                self.latest = vN_DB_str
                self.latest = os.path.join(self.base_dir, self.latest)
            print("INFO: Copying " + finalYAML + " to " + self.latest)
            shutil.copyfile(finalYAML, self.latest)

    def reformatYAML(self, newYAMLfile, workdir):
        temp = workdir + "temp.yml"
        counter = 0
        if not os.path.exists(newYAMLfile):
            print("ERROR: no newYAMLfile.")
        with open(newYAMLfile, "r") as infile:
            with open(temp, "w") as outfile:
                print("INFO: Reformatting " + newYAMLfile + " in " + temp)
                for line in infile:
                    """
                    counter = counter + 1
                    if counter == 1:  # PN - I am not sure why it was done like this for XML; this would certainly require the first line to be blank
                        #    outfile.write(
                        #        '<?xml version=\'1.0\' encoding=\'iso-8859-1\'?> \n'
                        #    )
                        #    outfile.write(
                        #        '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n'
                        #    )
                        #    outfile.write('<DDDB> \n')
                        pass
                    else:
                        outfile.write(line)
                    """
                    outfile.write(line)
                # Add a newline at the end of the YAML file. This is necessary to pass the lhcb-conditions-database formatting check (GitLab)
                outfile.write("\n")
            print("INFO: Copying " + temp + " to " + newYAMLfile)
            shutil.copyfile(temp, newYAMLfile)
            print("INFO: Removing " + temp)
            os.remove(temp)

# /group/rich/sw/alignment/stack/Panoptes/run gaudirun.py /group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/python/PyMirrAlignOnline/unbiased_reconstruction.py
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import csv
import glob
import math
import random
import sys

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad
from Moore import (
    options,
    run_reconstruction,
)
from Panoptes.alignment import (
    standalone_rich_online_align_reco,
    standalone_rich_panel_align_reco,
)
from PyConf.Algorithms import (
    PrForwardTrackingVelo,
    PrHybridSeeding,
    PrMatchNN,
    VeloRetinaClusterTrackingSIMD,
    VPRetinaFullClusterDecoder,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_PatPV3DFuture_pvs,
    make_reco_pvs,
    make_velo_full_clusters,
    make_VeloClusterTrackingSIMD,
)
from RecoConf.rich_data_monitoring import (
    alignment_rich_monitoring_options,
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import default_rich_reco_options

"""Options for running over data with FT raw bank version 6."""
from RecoConf.decoders import (
    default_ft_decoding_version,
)

# load options from YAML file
from ruamel.yaml import YAML

yaml = YAML()

whichRich = 2
whichPanel = "both"
survey_type = "TESTING"

manual_update = True

useDD4Hep = True
useRealData = True
useHltDecisions = False

options.evt_max = 50000
options.n_threads = 20
options.scheduler_legacy_mode = False
options.use_iosvc = True

import os


def get_directories_with_prefix(directory, prefix):
    file_directories = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if os.path.basename(root).startswith(prefix):
                file_path = os.path.join(root, file)
                file_directories.append(file_path)
    return file_directories


base_dir_files = f"/calib/align/LHCb/Rich{whichRich}"


def obtain_files_from_many_runs(
    base_dir_files,
    start_run,
    end_run,
    num_files_to_select=2,  # this is per run number
    seed_value=100,
):
    files = []

    for folder_number in range(start_run, end_run + 1):
        folder_name = "0000" + str(folder_number)
        folder_path = os.path.join(base_dir_files, folder_name)

        # Check if the folder exists
        if os.path.exists(folder_path) and os.path.isdir(folder_path):
            # List all files in the folder
            files_in_folder = os.listdir(folder_path)

            # Select random files from the folder
            random.seed(seed_value)
            random_files = random.sample(
                files_in_folder, min(num_files_to_select, len(files_in_folder))
            )

            # Print the selected files
            for file_name in random_files:
                files.append(os.path.join(folder_path, file_name))

    return files


options.input_files = obtain_files_from_many_runs(
    # base_dir_files, 299618, 299683)
    base_dir_files,
    298656,
    300124,
)

options.simulation = False
options.input_type = "MDF"

from Configurables import DDDBConf
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from DDDB.CheckDD4Hep import UseDD4Hep

useUT_bool = True
if useUT_bool:
    dd4hep = DD4hepSvc(
        DetectorList=["/world", "Magnet", "UT", "VP", "FT", "Rich1", "Rich2"]
    )
    noUT_bool = False
else:
    dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "VP", "FT", "Rich1", "Rich2"])
    noUT_bool = True
dd4hep.OutputLevel = 1

options.conddb_tag = "master"  # used for real data
options.dddb_tag = "run3/trunk"

# latest scifi and velo alignments
dd4hep.UseConditionsOverlay = True
dd4hep.ConditionsVersion = "master"

# needed for when reconstruction changed to SuperPixels (change was at run 289434)
DDDBConf().GeometryVersion = "run3/2024.Q1.2-v00.00"
options.geometry_version = "run3/2024.Q1.2-v00.00"

conddb_path = "/swdev/marshall/lhcb-conditions-database/"
dd4hep.ConditionsLocation = "file://" + conddb_path

rich = f"rich{whichRich}"
radiator = f"Rich{whichRich}Gas"

param_list = ["p0_x", "p1_x", "p0_y", "p1_y", "p0_z", "p1_z"]
param_dict = dict.fromkeys(param_list)
trans_perf_param_dict = dict.fromkeys(param_list)

# ####### 2nd July 2024 overlay
# dd4hep.UseConditionsOverlay = True
# dd4hep.ConditionsOverlayInitPath = '/group/online/alignment/prelim_align_2024/Modules_and_HalfLayers_TxRzTz_JPsi_contraint_mat3_0107_Run3map'
# condval = "!alignment{ position: [0 * mm, 0 * mm, 0 * mm],  rotation: [-0.0005157705398491993 * rad, 7.011816237252315e-05 * rad, -0.00042666842206765687 * rad]}"
# dd4hep.ConditionsOverride = {"/world/BeforeMagnetRegion/VP:alignment_delta": condval}
# ####### 2nd July 2024 overlay

# ####### 4th July 2024 overlay
dd4hep.UseConditionsOverlay = True
dd4hep.ConditionsOverlayInitPath = (
    "/group/online/alignment/prelim_align_2024/07_01_Run3Map_withUT_0207"
)
condval = "!alignment{ position: [0 * mm, 0 * mm, 0 * mm],  rotation: [-0.0005157705398491993 * rad, 7.011816237252315e-05 * rad, -0.00042666842206765687 * rad]}"
dd4hep.ConditionsOverride = {"/world/BeforeMagnetRegion/VP:alignment_delta": condval}
# ####### 4th July 2024 overlay

panel_nums = []
if whichPanel == "both":
    panel_nums.append(0)
    panel_nums.append(1)
else:
    panel_nums.append(whichPanel)

default_moni_opts = {}
default_reco_opts = {}
align_opts = {}

# Reco opts
wider_bkg = {"PhotonSelection": "None"}
default_reco_opts.update(wider_bkg)

if whichPanel == 0:
    panel_select = {"ActivatePanel": (True, False)}
elif whichPanel == 1:
    panel_select = {"ActivatePanel": (False, True)}
else:
    panel_select = {"ActivatePanel": (True, True)}
default_reco_opts.update(panel_select)

# Monitoring opts
if whichRich == 1:
    minP = 10.0
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": minP * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }
if whichRich == 2:
    minP = 20.0
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": minP * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }

default_moni_opts.update(tighter_minp)

# wider_histo = {'CKResHistoRange': (0.025, 0.005, 0.004)}
wider_histo = {"CKResHistoRange": (0.025, 0.0055, 0.0055)}
default_moni_opts.update(wider_histo)
useUT = {"UseUT": useUT_bool}
default_moni_opts.update(useUT)

nkevts = str(math.trunc(options.evt_max / 1000))

# save output file with histograms
if UseDD4Hep:
    options.histo_file = f"rich{whichRich}_opt_dd4hep_p{whichPanel}.root"
else:
    options.histo_file = f"rich{whichRich}_opt_p{whichPanel}.root"

PrForwardTrackingVelo.global_bind(
    MinQuality=0.0,
    DeltaQuality=0.0,
    MinTotalHits=9,
    MaxChi2PerDoF=50.0,
    MaxChi2XProjection=60.0,
    MaxChi2PerDoFFinal=28.0,
    MaxChi2Stereo=16.0,
    MaxChi2StereoAdd=16.0,
    MinP=minP * GeV,
)

PrMatchNN.global_bind(MinP=minP * GeV)
PrHybridSeeding.global_bind(MinP=minP * GeV)

# commented out when reconstruction changed to SuperPixels (change was at run 289434)
make_VeloClusterTrackingSIMD.global_bind(algorithm=VeloRetinaClusterTrackingSIMD)
make_velo_full_clusters.global_bind(make_full_cluster=VPRetinaFullClusterDecoder)
make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)

from Configurables import (
    ApplicationMgr,
    HltDecReportsDecoder,
    HltSelReportsDecoder,
    LHCb__UnpackRawEvent,
)
from PyConf.application import configured_ann_svc

unpacker = LHCb__UnpackRawEvent(
    "UnpackRawEvent",
    OutputLevel=2,
    RawBankLocations=["DAQ/RawBanks/HltDecReports", "DAQ/RawBanks/HltSelReports"],
    BankTypes=["HltDecReports", "HltSelReports"],
)

decDec = HltDecReportsDecoder(
    "HltDecReportsDecoder/Hlt1DecReportsDecoder",
    SourceID="Hlt1",
    RawBanks=unpacker.RawBankLocations[0],
)

selDec = HltSelReportsDecoder(
    "HltSelReportsDecoder/Hlt1SelReportsDecoder",
    SourceID="Hlt1",
    DecReports=unpacker.RawBankLocations[0],
    RawBanks=unpacker.RawBankLocations[1],
)

app = ApplicationMgr(
    TopAlg=[unpacker, decDec, selDec], ExtSvc=[configured_ann_svc(name="HltANNSvc")]
)

# by default, no additional filtering of the events
event_filter = []

# Prepare filter
import Functors
from PyConf.Algorithms import (
    HltDecReportsDecoder as PyConf_Algorithms_HltDecReportsDecoder,
)
from PyConf.Algorithms import (
    VoidFilter,
)
from PyConf.application import (
    default_raw_banks,
    default_raw_event,
)

lines = (
    ["Hlt1RICH1AlignmentDecision"] if whichRich == 1 else ["Hlt1RICH2AlignmentDecision"]
)

with default_raw_event.bind(raw_event_format=0.5):
    hlt1_dec_reports = PyConf_Algorithms_HltDecReportsDecoder(
        RawBanks=default_raw_banks("HltDecReports"), SourceID="Hlt1"
    )

    hlt1_filter = VoidFilter(
        name="Streaming_filter",
        Cut=Functors.DECREPORTS_FILTER(
            Lines=lines, DecReports=hlt1_dec_reports.OutputHltDecReportsLocation
        ),
    )

# when particular decision of HLT1 line about RICH1 or RICH2 is wanted
# if 'subset' not in current_variant:
if useHltDecisions:
    # prepare filter for selecting events
    # chosen for alignment of mirrors of particular RICH

    print(f"INFO: Using HLT decisions.")
    print("")

    event_filter = [hlt1_filter]

    print(f"INFO: ***Applying Event Filter***")
    print(f"INFO: {lines}")
    print(f"INFO: {hlt1_filter}")
    print(f"INFO: {type(hlt1_filter)}")
    print(f"INFO: {event_filter}")
    print(f"INFO: ***************************")
    sys.stdout.flush()


with (
    standalone_rich_panel_align_reco.bind(
        RichGas=radiator,
        EventFilter=event_filter if useHltDecisions else [],
        noUT=noUT_bool,
    ),
    alignment_rich_monitoring_options.bind(
        radiator=radiator, init_override_opts=align_opts
    ),
    default_ft_decoding_version.bind(value=6),
    default_rich_reco_options.bind(init_override_opts=default_reco_opts),
    default_rich_monitoring_options.bind(init_override_opts=default_moni_opts),
):
    run_reconstruction(options, standalone_rich_panel_align_reco)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Claire Prouve, Paras Naik"
__copyright__ = "University of Bristol, May 2018"

import distutils
import math
import os
import re
import shutil
import sys
import threading
import time
from distutils import dir_util
from subprocess import *
from time import gmtime, sleep, strftime, time

import Configurables


class XMLFileHelper:
    def __init__(self, configuration):
        self.alignConf = configuration
        self.whichRich = self.alignConf.getProp("Rich")
        self.base_dir = (
            "/group/online/alignment/Rich" + str(self.whichRich) + "/MirrorAlign/"
        )

    def create_coeff_calibration_xml_files(self, inputFile, n_it):
        print("INFO: start create coeff")
        writedir = self.alignConf.getProp("WorkDir")
        coeffCalibTilt = self.alignConf.getProp("coeffCalibTilt")
        thisNameStr = self.alignConf.getProp("nameStr")

        from array import array

        import fpformat
        from lxml import etree

        signName = dict({"-": "neg", "+": "pos"})
        signToShift = dict({"-": -coeffCalibTilt, "+": coeffCalibTilt})
        mirrTypeName = dict({"pri": "Sph", "sec": "Sec"})
        totMirrNum = dict({"pri": 56, "sec": 40})
        if self.whichRich == 2:
            totMirrNum = dict({"pri": 56, "sec": 40})
        if self.whichRich == 1:
            totMirrNum = dict({"pri": 4, "sec": 16})

        seqNum = dict({"Y": 1, "Z": 2})
        rotRE = re.compile(r"[\-|\+]?[0-9]+\.?[0-9]*|[\-|\+]?\.[0-9]+")
        rotValues = []

        for mirrType in ["pri", "sec"]:
            for axis in ["Y", "Z"]:
                for sign in ["-", "+"]:
                    parser = etree.XMLParser(remove_blank_text=True)
                    RichAlignmentConditions = etree.parse(inputFile, parser)
                    for mirrNum in range(totMirrNum[mirrType]):
                        floatRotValues = array("f")
                        if self.whichRich == 1:
                            dRotXYZ = RichAlignmentConditions.xpath(
                                "//condition[@name='"
                                + mirrTypeName[mirrType]
                                + "Mirror"
                                + str(mirrNum)
                                + "_Align']/paramVector[@name='dRotXYZ']"
                            )
                        if self.whichRich == 2:
                            dRotXYZ = RichAlignmentConditions.xpath(
                                "//condition[@name='Rich"
                                + str(self.whichRich)
                                + mirrTypeName[mirrType]
                                + "Mirror"
                                + str(mirrNum)
                                + "_Align']/paramVector[@name='dRotXYZ']"
                            )
                        rotValues = rotRE.findall(
                            etree.tostring(dRotXYZ[0], pretty_print=True, method="text")
                        )
                        for i in range(3):
                            floatRotValues.append(float(rotValues[i]))
                        floatRotValues[seqNum[axis]] += signToShift[sign]
                        dRotXYZ[0].text = (
                            fpformat.fix(floatRotValues[0], 5)
                            + "*mrad "
                            + fpformat.fix(floatRotValues[1], 5)
                            + "*mrad "
                            + fpformat.fix(floatRotValues[2], 5)
                            + "*mrad "
                        )
                        if axis == "Y":
                            signCombinName = signName[sign] + "YzerZ"
                        else:
                            signCombinName = "zerY" + signName[sign] + "Z"
                        newXML_File = open(
                            writedir
                            + "Rich"
                            + str(self.whichRich)
                            + "CondDBUpdate_"
                            + thisNameStr
                            + "_"
                            + mirrType
                            + "_"
                            + signCombinName
                            + "_i"
                            + str(n_it)
                            + ".xml",
                            "w",
                        )
                        newXML_File.write(
                            etree.tostring(
                                RichAlignmentConditions,
                                encoding="iso-8859-1",
                                xml_declaration=True,
                                pretty_print=True,
                                with_tail=False,
                            )
                        )
                        newXML_File.close()

    def getStartXMLOrigFileName(self):
        import os
        import re

        re_version = re.compile(r"^v([0-9]+)\.xml$")

        def get_version(entry):
            r = re_version.match(entry)
            if not r:
                return -1
            else:
                return int(r.group(1))

        latestName = max(os.listdir(self.base_dir), key=get_version)
        return latestName

    def getStartXML(self, startxml):
        import os

        latest = self.alignConf.getProp("startXMLFile")

        if (latest == "") or (self.alignConf.getProp("testing") is not True):
            latest = self.getStartXMLOrigFileName()
            latest = os.path.join(self.base_dir, latest)

        print("INFO: This file was picked up as the starting xml: ", latest)

        with open(startxml, "w") as outfile:
            outfile.write("<?xml version='1.0' encoding='iso-8859-1'?> \n")
            outfile.write('<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n')
            outfile.write("<DDDB> \n")
            if not os.path.exists(latest):
                print("ERROR: no latest file.")
            with open(latest) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.write("</DDDB> \n")

    def getCompareXML(self, compareXML):
        import os

        compareXMLFile = self.alignConf.getProp("compareXMLFile")
        if compareXMLFile == "":
            compareXMLFile = self.getStartXMLOrigFileName()
            compareXMLFile = os.path.join(self.base_dir, compareXMLFile)

        print("INFO: This file was picked up as the compareXMLFile: ", compareXMLFile)

        with open(compareXML, "w") as outfile:
            outfile.write("<?xml version='1.0' encoding='iso-8859-1'?> \n")
            outfile.write('<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n')
            outfile.write("<DDDB> \n")
            if not os.path.exists(compareXMLFile):
                print("ERROR: no compareXMLFile.")
            with open(compareXMLFile) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.write("</DDDB> \n")

    def finalXML(self, vN_DB_str, Update):
        workdir = self.alignConf.getProp("WorkDir")

        finalXML = workdir + "CondDB_Update_Rich" + str(self.whichRich) + ".xml"
        tocopyXML = workdir + "CondDB_Rich" + str(self.whichRich) + ".xml"

        if not os.path.exists(tocopyXML):
            print("ERROR: no tocopyXML file.")
        with open(tocopyXML, "r") as infile:
            with open(finalXML, "w") as outfile:
                for line in infile:
                    if (
                        "<?xml version='1.0' encoding='iso-8859-1'?>" not in line
                        and '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd">'
                        not in line
                        and "<DDDB>" not in line
                        and "</DDDB>" not in line
                    ):
                        outfile.write(line)
        if Update:
            latest = vN_DB_str + ".xml"
            latest = os.path.join(self.base_dir, latest)
            shutil.copyfile(finalXML, latest)

    def reformatXML(self, newXMLfile, workdir):
        temp = workdir + "temp.xml"
        counter = 0
        if not os.path.exists(newXMLfile):
            print("ERROR: no newXMLfile.")
        with open(newXMLfile, "r") as infile:
            with open(temp, "w") as outfile:
                for line in infile:
                    counter = counter + 1
                    if counter == 1:
                        outfile.write("<?xml version='1.0' encoding='iso-8859-1'?> \n")
                        outfile.write(
                            '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n'
                        )
                        outfile.write("<DDDB> \n")
                    else:
                        outfile.write(line)
            shutil.copyfile(temp, newXMLfile)
            os.remove(temp)

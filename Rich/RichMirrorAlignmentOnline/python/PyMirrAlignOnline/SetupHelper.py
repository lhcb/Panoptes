###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Claire Prouve, Paras Naik"
__copyright__ = "University of Bristol, November 2022"

import datetime
import getpass
import glob
import math
import os
import re
import shlex
import shutil
import smtplib
import subprocess
import sys
import time
from enum import Enum
from time import gmtime, strftime, time

from MooreOnlineConf.utils import (
    alignment_options,
)
from ROOT import TH1, TFile, TTree
from ruamel.yaml import YAML


class SetupHelper:
    def __init__(self, _alignConf, flat=False):
        self.alignConf = _alignConf
        # self.combAndMirrSubsets = self.alignConf.getProp('combAndMirrSubsets')
        self.nameStr = self.alignConf.getProp("nameStr")
        self.tiltNames = self.alignConf.getProp("tiltNames")
        self.tiltNamesLength = len(self.tiltNames)
        self.workdir = self.alignConf.getProp("WorkDir")
        self.whichRich = self.alignConf.getProp("Rich")
        self.YAMLio = self.alignConf.getProp("YAMLio")
        self.useDD4hep = self.alignConf.getProp("useDD4hep")
        self.autoUpdate = self.alignConf.getProp("autoUpdate")
        self.autoUpdateNoV = self.alignConf.getProp("autoUpdateNoV")
        self.magnFactorsMode = self.alignConf.getProp("magnFactorsMode")
        self.minAverageBinPop = self.alignConf.getProp("minAverageBinPop")
        self.phiBinFactor = self.alignConf.getProp("phiBinFactor")
        self.minFracPhiBinsPopulated = self.alignConf.getProp("minFracPhiBinsPopulated")
        self.deltaThetaWindow = self.alignConf.getProp("deltaThetaWindow")
        self.combinFitMethod = self.alignConf.getProp("combinFitMethod")
        self.solutionMethod = self.alignConf.getProp("solutionMethod")
        self.coeffCalibTilt = self.alignConf.getProp("coeffCalibTilt")
        self.magnifDir = os.path.join(
            self.alignConf.getProp("stackDir"), self.alignConf.getProp("magnifDir")
        )
        self.XML_YAML_configDir = os.path.join(
            self.alignConf.getProp("stackDir"),
            self.alignConf.getProp("XML_YAML_configDir"),
        )
        self.testing = self.alignConf.getProp("testing")
        self.fixSinusoidShift = self.alignConf.getProp("fixSinusoidShift")
        # People who will get the emails during testing
        self.experts = [
            "alex.marshall@cern.ch",
            "jake.reich@cern.ch",
            "zahra.moghaddam@bristol.ac.uk",
            "Anatoly.Solomin@bristol.ac.uk",
            "Paras.Naik@cern.ch",
        ]  # self.experts = ['Paras.Naik@cern.ch','samuel.maddrell-mander@cern.ch','matthew.george.chapman@cern.ch','claire.prouve@cern.ch']
        # People who will get the emails during automated/production running (in principle this is experts + anyone else and/or mailing list who wishes to be notified)
        self.emails = [
            "alex.marshall@cern.ch",
            "jake.reich@cern.ch",
            "zahra.moghaddam@bristol.ac.uk",
            "Anatoly.Solomin@bristol.ac.uk",
            "Paras.Naik@cern.ch",
        ]  # self.emails = ['lhcb-rich-mirror-alignment-development@cern.ch','jonesc@hep.phy.cam.ac.uk','antonis.papanestis@stfc.ac.uk','m.mccann@imperial.ac.uk','silvia.gambetta@cern.ch']
        #
        # Uncomment and adjust these lines should you be testing and don't want to spam people (though usually the experts don't mind)
        # self.experts = ['whoever.you.are@cern.ch']
        # self.emails = ['whoever.you.are@cern.ch']

        self.developers = ["lhcb-rich-mirror-alignment-development@cern.ch"]
        self.directoryTime = strftime("%Y%m%d_%H%M%S", gmtime())
        print(f"INFO: self.directoryTime: {self.directoryTime}")
        self.now = datetime.datetime.now(datetime.timezone.utc)
        self.dt = self.now.isoformat(timespec="minutes").replace(
            "+00:00", ""
        )  # Anatoly's old timestamp
        self.dt = self.dt.replace(":", "-")  # Anatoly's new timestamp
        print(f"INFO: self.dt: {self.dt}")
        self.savedir = os.path.join(
            self.alignConf.getProp("SaveDir")
            + "Rich"
            + str(self.whichRich)
            + "/"
            + self.directoryTime,
            "",
        )
        self.dataVariant = self.alignConf.getProp("dataVariant")
        self.trendDateRange = self.alignConf.getProp("trendDateRange")
        self.finalCKres = None
        self.finalCKresErr = None
        self.flat = flat
        self.usingReducedSubset = self.alignConf.getProp("useReducedSubset")

    def setupMagnifFiles(self):
        print(f"INFO: Copying all files in {self.magnifDir} to {self.workdir}")
        for f in os.listdir(self.magnifDir):
            if self.flat is False:
                shutil.copy(self.magnifDir + f, self.workdir)
            else:
                shutil.copyfile(self.magnifDir + f, self.workdir + f)

    def startMinIt(self, m_it):
        """Remove folders and files from a given directory based on certain criteria.

        Args:
            m_it (int): The iteration number.
        """
        removeIt = (m_it - m_it % self.tiltNamesLength) / self.tiltNamesLength
        files = os.listdir(self.workdir)

        re_fitFolders = re.compile(
            f"Rich{self.whichRich}MirrCombinFit_{self.nameStr}[\w\.]*_i{removeIt}"
        )
        re_alignFiles = re.compile(
            f"Rich{self.whichRich}MirrAlign[a-zA-z]*_i{removeIt}\.txt"
        )
        fitFolders = [x for x in files if re_fitFolders.match(x)]
        alignFiles = [x for x in files if re_alignFiles.match(x)]

        if m_it % self.tiltNamesLength == 0:
            connectStr = ""
        else:
            connectStr = "_"

        yaml = YAML()
        with open(
            os.path.join(self.workdir, f"rich{self.whichRich}_variant.yml")
        ) as finp:
            variant = yaml.load(finp)
        rootFile = (
            variant
            + f"_histos_{self.tiltNames[m_it % self.tiltNamesLength]}_i{removeIt}.root"
        )

        # Remove fit folders, align files, and root file.
        files_to_remove = fitFolders + alignFiles + [rootFile]
        for f in files_to_remove:
            fpath = os.path.join(self.workdir, f)
            if os.path.exists(fpath):
                if os.path.isdir(fpath):
                    print(f"INFO: Removing folder {fpath}")
                    shutil.rmtree(fpath)
                else:
                    print(f"INFO: Removing file {fpath}")
                    os.remove(fpath)

    def monitor(self, n_it, compareFILE, forcedUpdate_ChMagPol):
        from PyMirrAlignOnline.AlignMonitor import AlignMonitor

        alignMonitor = AlignMonitor(self.alignConf, n_it, self.flat)
        alignMonitor.performMonitoring(compareFILE, forcedUpdate_ChMagPol)
        self.finalCKres = alignMonitor.finalCKres
        self.finalCKresErr = alignMonitor.finalCKresErr
        return alignMonitor.isInsane

    def finalize(
        self,
        vN_DB_str,
        conv,
        n_it,
        farm_config_time,
        elapsed_runtimes,
        DBUpdated,
        nEventsListList,
        edit=None,
    ):
        nEventsList = nEventsListList[0]
        nEvents = nEventsListList[1]

        def copy(fromFile, toFile, printouts=True):
            if printouts:
                print(f"INFO: Copying {fromFile} to {toFile}")
            if self.flat is False:
                shutil.copy2(fromFile, toFile)
            else:
                shutil.copyfile(fromFile, toFile)

        summaryFile = f"{self.workdir}summary.txt"
        self.writeSummary(
            vN_DB_str,
            conv,
            n_it,
            farm_config_time,
            elapsed_runtimes,
            summaryFile,
            DBUpdated,
            nEvents,
            nEventsList,
        )

        if self.flat is False:
            if "online" in getpass.getuser():
                pass
            else:
                print(
                    "INFO: TESTBENCH: If your user name is not 'online' will not save in usual place ("
                    + self.savedir
                    + ") to avoid a potential permissions issue."
                )  # PN: Temporary
                print(
                    "INFO: TESTBENCH: Instead will save in the testbench output directory."
                )  # PN: Temporary
                sys.stdout.flush()
                # self.savedir = "/home/pnaik/SCRATCH_TESTBENCH" + '/' + self.directoryTime + '/'  # PN: Temporary
                self.savedir = "." + "/" + self.directoryTime + "/"  # PN: Temporary
            if not os.path.exists(self.savedir):
                os.makedirs(self.savedir)
        else:
            print("INFO: You are running the Iterator in flat (not fully Online) mode.")
            print(
                "INFO: Will not save in usual place ("
                + self.savedir
                + ") to avoid a potential permissions issue."
            )
            print(
                "INFO: Instead will save in the directory you launched the Iterator from."
            )
            sys.stdout.flush()
            self.savedir = "./" + "/" + self.directoryTime + "/"
            if not os.path.exists(self.savedir):
                os.makedirs(self.savedir)
        print(
            f"INFO: Copying all files and directories in {self.workdir} to {self.savedir}"
        )
        sys.stdout.flush()
        for f in os.listdir(self.workdir):
            if os.path.isdir(self.workdir + f):
                try:
                    shutil.copytree(self.workdir + f, self.savedir + f)
                except:
                    print(
                        f"WARNING: failed shutil.copytree(self.workdir + f, self.savedir + f)"
                    )
            else:
                try:
                    copy(self.workdir + f, self.savedir + f, printouts=False)
                except:
                    print(
                        f"WARNING: failed copy(self.workdir + f, self.savedir + f,printouts=False)"
                    )

        print("INFO: The savedir is: " + self.savedir)
        sys.stdout.flush()
        # NOTE: *Any file created after this in the workdir*
        #   needs to be *manually* saved in the savedir

        # Create the trend plots automatically, now that the savedir is written
        import PyMirrAlignOnline.trendHelper as trendHelper

        print(
            "INFO: Now producing the trend plots for RICH" + str(self.whichRich) + "."
        )
        sys.stdout.flush()
        # Files for the Alignment trend plots
        trendFile_Fills_hollow = (
            self.workdir + "Rich" + str(self.whichRich) + "_AlignTrend_Fills_hollow.pdf"
        )
        trendFile_Dates_hollow = (
            self.workdir + "Rich" + str(self.whichRich) + "_AlignTrend_Dates_hollow.pdf"
        )
        # set dates
        minDate = self.trendDateRange[
            "minDate"
        ]  # 2017 data: 20170602 / Rich2: 20170605
        maxDate = self.trendDateRange["maxDate"]  # 2017 data: 20171129
        # now = datetime.datetime.now()
        if minDate is None:
            minDate = int(self.now.year) * 10000 + 1 * 100 + 1  # beginning of run year
        if maxDate is None:
            maxDate = int(self.now.year) * 10000 + 12 * 100 + 31  # end of run year

        print(
            'INFO: Now making the "hollow" trend plot with Fills on the x-axis, with minDate '
            + str(minDate)
            + " and maxDate "
            + str(maxDate)
        )
        P_Fills_hollow = trendHelper.Plotter(
            self.whichRich, "hollow", "Fills", minDate, maxDate, trendFile_Fills_hollow
        )
        alignmentsExist = P_Fills_hollow.GetAlignments()
        if alignmentsExist:
            P_Fills_hollow.calculate_tilts()

        print(
            'INFO: Now making the "hollow" trend plot with Dates on the x-axis, with minDate '
            + str(minDate)
            + " and maxDate "
            + str(maxDate)
        )
        P_Dates_hollow = trendHelper.Plotter(
            self.whichRich, "hollow", "Dates", minDate, maxDate, trendFile_Dates_hollow
        )
        alignmentsExist = P_Dates_hollow.GetAlignments()
        if alignmentsExist:
            P_Dates_hollow.calculate_tilts()

        # Identify important file names
        ChangeWRTDBFile = (
            self.savedir
            + "/Rich"
            + str(self.whichRich)
            + "_ChangeWRTDB_"
            + str(n_it + 1)
            + ".txt"
        )
        AlignSummaryFile = (
            self.savedir + "/Rich" + str(self.whichRich) + "_AlignSummary.pdf"
        )  # For RICH piquet and experts
        AlignMonitorFile = (
            self.savedir + "/Rich" + str(self.whichRich) + "_AlignMonitor.pdf"
        )  # For Monitoring
        AlignTrendFile_Fills_hollow = (
            self.savedir
            + "/Rich"
            + str(self.whichRich)
            + "_AlignTrend_Fills_hollow.pdf"
        )  # Automatic alignment trend plot
        AlignTrendFile_Dates_hollow = (
            self.savedir
            + "/Rich"
            + str(self.whichRich)
            + "_AlignTrend_Dates_hollow.pdf"
        )  # Automatic alignment trend plot

        AlignTestingFile = (
            self.workdir + "/Rich" + str(self.whichRich) + "_AlignTesting.pdf"
        )  # Not used just yet; May not exist

        # Copy the "hollow" trend file with Fills on the x-axis from the workdir to the savedir
        # This is the only one we send to the RICH ELOG
        if os.path.exists(trendFile_Fills_hollow) and os.path.exists(self.savedir):
            print(
                'INFO: storing "hollow" alignment trend plot with Fills on the x-axis in file: '
                + AlignTrendFile_Fills_hollow
            )
            try:
                copy(trendFile_Fills_hollow, AlignTrendFile_Fills_hollow)
            except:
                print(
                    f"WARNING: failed copy(trendFile_Fills_hollow,AlignTrendFile_Fills_hollow)"
                )
        else:
            print(
                "INFO: "
                + trendFile_Fills_hollow
                + " or "
                + self.savedir
                + " does not exist."
            )
        sys.stdout.flush()

        # Copy the "hollow" trend file with Dates on the x-axis from the workdir to the savedir
        # This is not sent to the RICH ELOG, but available for AlignmentView
        if os.path.exists(trendFile_Dates_hollow) and os.path.exists(self.savedir):
            print(
                'INFO: storing "hollow" alignment trend plot with Dates on the x-axis in file: '
                + AlignTrendFile_Dates_hollow
            )
            try:
                copy(trendFile_Dates_hollow, AlignTrendFile_Dates_hollow)
            except:
                print(
                    f"WARNING: failed copy(trendFile_Dates_hollow,AlignTrendFile_Dates_hollow)"
                )
        else:
            print(
                "INFO: "
                + trendFile_Dates_hollow
                + " or "
                + self.savedir
                + " does not exist."
            )
        sys.stdout.flush()

        # Write files and information to the RICH and Alignment Monitor ELOGs, and also send email
        alignment_time = elapsed_runtimes["elapsed_time_run"]
        self.writeInLogbook(
            vN_DB_str,
            conv,
            n_it,
            farm_config_time,
            alignment_time,
            summaryFile,
            ChangeWRTDBFile,
            AlignSummaryFile,
            AlignMonitorFile,
            AlignTrendFile_Fills_hollow,
            DBUpdated,
            nEvents,
            edit,
        )

    def writeInLogbook(
        self,
        vN_DB_str,
        conv,
        n_it,
        farm_config_time,
        alignment_time,
        summaryFile,
        ChangeWRTDBFile,
        AlignSummaryFile,
        AlignMonitorFile,
        AlignTrendFile_Fills_hollow,
        DBUpdated,
        nEvents,
        edit,
    ):
        hh, r = divmod(farm_config_time, 3600)
        mm, ss = divmod(r, 60)
        farm_config_time_str = "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
        hh, r = divmod(alignment_time, 3600)
        mm, ss = divmod(r, 60)
        alignment_time_str = "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)

        if self.flat is False:
            if "online" in getpass.getuser():
                import OnlineEnvBase as Online  # import OnlineEnv as Online

                online_options = alignment_options(Online)
                runs = online_options.runs  # runs = Online.DeferredRuns
                # PN - Extract the run number substring from the full string - WILL NOT WORK IF RUN NUMBERS BECOME 7 DIGITS
                runs = [entry[-6:] for entry in runs]
                FillAlignment = self.getFillNumber(runs[0])
            else:
                FillAlignment = 0  # PN: for testbench
        else:
            FillAlignment = 0

        # RICH ELOG
        host = "logbook.lbdaq.cern.ch"  #'lblogbook.cern.ch'
        port = 8080
        username = "common Common\!"  # used to be 'common Common\\!' but now works with only one escape
        subject = ""
        text = ""
        instruction_file = (
            "https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbRun3RichMirrAlignShiftInfo"
        )
        subjText = ""
        if self.testing:
            logbook = "TestLogbook"
            systemName = "System"
            activity = "Test"
            author = "Generic Elog Account"
        else:
            logbook = "RICH"
            systemName = "System_19"  # Change 19 if Alignment changes its order in the ELOG (ask the current RICH expert)
            activity = "Alignment"
            author = "MirrAlign Monitor"  # Or any other name

        if DBUpdated:
            subjText = " updated,"
        elif self.testing and DBUpdated is None:
            subjText = " not updated due to testing = True,"  # if testing is on, regardless of autoUpdate setting
        elif DBUpdated is None:
            if self.autoUpdate is False:
                subjText = " not updated due to autoUpdate = False, needs MirrAlign expert decision,"  # testing and autoUpdate were off to begin with
            elif self.autoUpdateNoV:
                subjText = " not updated due to sanity check issue, needs MirrAlign expert decision,"  # testing off, but autoUpdate was on at the start, then switched off
            elif self.autoUpdateNoV is False:
                subjText = " not updated due to sanity check issue,"  # existing version was provided
            else:
                subjText += ""
        else:
            subjText += ""

        if self.testing:
            subject = f"TEST [{self.dataVariant}]: "
            text = f"TEST [{self.dataVariant}]: This is a test alignment being run by a mirror alignment expert; RICH piquets may ignore it. \n"

        text += "The Mirror "
        text += str(activity)
        text += " for RICH"
        text += str(self.whichRich)

        text += " ran for "
        text += alignment_time_str
        text += " (excluding an LHCbA configuration time of "
        text += farm_config_time_str
        text += ") and "

        if conv:
            text += "converged. "
            subject += f"RICH{self.whichRich} alignment {self.directoryTime}{subjText} converged"
        else:
            text += "did not converge. "
            subject += f"RICH{self.whichRich} alignment {self.directoryTime}{subjText} incomplete"

        text += f"We processed approximately {nEvents} events per iteration from the RICH{self.whichRich} Decision of our Hlt1CalibRICHMirror HLT line. "
        text += f"{n_it + 1} iteration(s) occured. Mirror {activity} monitoring information and plots are attached; shifter's instructions can be found at {instruction_file} and the output directory timestamp is {self.directoryTime}."

        if DBUpdated is None:
            text += f" There was a significant change from the alignment in the DB, or a change in magnet polarity. However, the final state of autoUpdate was set to False (even if the initial state of autoUpdate was True), or the alignment is in testing mode, so the DB was not updated. If the automatic update feature was disabled, a sanity check was not satisfied. Please notify a MirrAlign expert to investigate whether this alignment ({vN_DB_str}) should be installed."
            if (
                self.autoUpdate and self.autoUpdateNoV
            ):  # We should maybe provide more info here
                text += " Please note: autoUpdate was initially True, and autoUpdateNoV was set to True. This means that an alignment version has NOT been provided to HLT2."
        elif DBUpdated:
            text += f" Due to a significant change from the alignment in the DB, or a change in magnet polarity, the DB was automatically updated to {vN_DB_str}."
        else:
            text += f" The alignment in the DB is still {vN_DB_str}."

        if not self.autoUpdate:
            text += " The initial state of autoUpdate was set to False in the configuration."

        command = ""
        # elog -h "logbook.lbdaq.cern.ch" -p 8080 -l "RICH" -u common Common\! -a "Author=MirrAlign Monitor" -a "System_19=Alignment" -a "Subject=TEST" "Testing the writing of logbook entries from the mirror alignment.\n RICH piquets should ignore all mirror alignments marked TEST in the subject heading.\n (Notes: common / System_19)"
        command += f'/group/online/bin/elog -h "{host}" -p {port}'
        if edit:
            command += f" -e {int(edit)}"
        command += f' -l "{logbook}" -u {username} -a "Author={author}"'
        command += f' -a "{systemName}={activity}" -a "Subject={subject}" "{text}"'

        files = [
            summaryFile,
            ChangeWRTDBFile,
            AlignSummaryFile,
            AlignTrendFile_Fills_hollow,
        ]
        for file in files:
            if os.path.exists(file):
                command += f' -f "{file}"'

        # print(f'DEBUG: command = elog -h "logbook.lbdaq.cern.ch" -p 8080 -l "RICH" -u common Common\! -a "Author=MirrAlign Monitor" -a "System_19=Alignment" -a "Subject=TEST" "Testing the writing of logbook entries from the mirror alignment.\n RICH piquets should ignore all mirror alignments marked TEST in the subject heading.\n (Notes: common / System_19)"')
        print(f"INFO: command = {command}")

        cmd = shlex.split(command)

        try:
            env = os.environ.copy()
            library_path = os.environ["LD_LIBRARY_PATH"]
            env["LD_LIBRARY_PATH"] = (
                f"{library_path}:/opt/nvidia/nsight-compute/2022.4.0/host/target-linux-x64/CollectX/"
            )
            elogret = subprocess.check_output(cmd, stderr=subprocess.STDOUT, env=env)
            messageID_match = re.search(
                r".*Message successfully transmitted, ID=(\d*)", elogret.decode()
            )
            if messageID_match:
                messageID = int(messageID_match.group(1))
            else:
                print("WARNING: Failed to extract message ID (main write to RICH ELOG)")
                messageID = None
        except:
            messageID = None
            print(f"WARNING: Exception (main write to RICH ELOG)")
        print(f"INFO: messageID = {messageID} (main write to RICH ELOG)")

        # Alignment ELOG
        logbookAlignment = "Alignment monitoring"  # always
        authorAlignment = "monibot"  # always
        activityAlignment = "Rich" + str(self.whichRich) + " Mirrors"  # always
        TypeAlignment = "Convergence"  # always
        # subjectAlignment = 'IGNORE-- '
        # textAlignment = 'Please Ignore: \n '
        subjectAlignment = ""
        textAlignment = ""
        StatusAlignment = "Good"  # or can be changed below to 'Unchecked' or 'Bad' (for now 'Bad' never happens)

        if self.testing:
            subjectAlignment += "TEST [" + self.dataVariant + "]: "
            textAlignment += f"A test mirror alignment for Rich{self.whichRich} was performed for fill {FillAlignment} by a RICH mirror alignment expert. No action is necessary; ignore the following. \n"

        if DBUpdated is None:
            subjectAlignment += "No alignment update-- Needs MirrAlign expert decision"
            textAlignment += f"Rich{self.whichRich} mirror alignment for fill {FillAlignment} is requesting an update be made, but for some reason this was not done automatically. Please ask the Rich Mirror Alignment experts to investigate the draft alignment ({vN_DB_str}). Monitoring plots in the attachment; shifter's instructions can be found at: {instruction_file}"
            StatusAlignment = "Unchecked"

        elif DBUpdated:
            subjectAlignment += "Monitoring plots"
            textAlignment += f"Updated Rich{self.whichRich} mirror alignment for fill {FillAlignment} ({vN_DB_str}), monitoring plots in the attachment; shifter's instructions can be found at: {instruction_file}"
            StatusAlignment = "Unchecked"

        else:
            subjectAlignment += "No alignment update"
            textAlignment += f"No Rich{self.whichRich} alignment update needed for fill {FillAlignment}. The DB alignment is still {vN_DB_str}."

        if self.testing:
            StatusAlignment = "Test"

        # Construct the elog command
        command = '/group/online/bin/elog -h "{}" -p {} -l "{}" -u {} -a "Author={}" -a "System={}" -a "Subject={}" -a "Type={}" -a "Fill={}" -a "Status={}" "{}"'.format(
            host,
            port,
            logbookAlignment,
            username,
            authorAlignment,
            activityAlignment,
            subjectAlignment,
            TypeAlignment,
            FillAlignment,
            StatusAlignment,
            textAlignment,
        )

        # Add the attachment file if applicable
        if DBUpdated or DBUpdated is None:
            if os.path.exists(AlignMonitorFile):
                command += ' -f "{}"'.format(AlignMonitorFile)

        cmd = shlex.split(command)
        if self.autoUpdate:  # If we are not in autoUpdate mode, then we do not post to the Alignment Monitoring ELOG
            FNULL = open(os.devnull, "w")
            subprocess.call(cmd, stdout=FNULL, stderr=subprocess.STDOUT)  # test

        # Email

        subj2 = ""
        if self.testing:
            subj2 += f"TEST [{self.dataVariant}]: "
        text2 = f"The Mirror {activity} for RICH{self.whichRich}"
        if conv:
            text2 += f" ran for {alignment_time_str} and converged. "
            subj2 += f"RICH{self.whichRich} alignment {self.directoryTime} converged: New ELOG entry"
        else:
            text2 += (
                f" ran for {alignment_time_str} and did not converge. Approximately "
            )
            subj2 += f"RICH{self.whichRich} alignment {self.directoryTime} incomplete: New ELOG entry"
        text2 += f"{nEvents} events per iteration were processed. {n_it + 1} iteration(s) occured. \n"

        db_msg = ""
        if DBUpdated is None:
            db_msg = (
                "\n There was a significant change from the alignment in the DB, or a change in magnet polarity. However the automatic update feature is off or was disabled during the alignment (The final state of autoUpdate was set to False), so the DB was not updated. If the automatic update feature was disabled, there may not have been enough events processed, or a sanity check was not satisfied. A MirrAlign expert should investigate whether this draft alignment ("
                + vN_DB_str
                + ") should be installed. \n"
            )  # We should probably provide more info here
        elif DBUpdated:
            db_msg = f"\n Due to a significant change from the alignment in the DB, or a change in magnet polarity, the DB was automatically updated to {vN_DB_str}. \n"  # We should probably provide more info here
        else:
            db_msg = f"\n The alignment in the DB is still {vN_DB_str}."
        text2 += db_msg

        links = [
            "https://lblogbook.cern.ch/RICH/?System=%5EAlignment%24&Subject={0}&mode=full".format(
                self.directoryTime
            ),
            "https://lbrich.cern.ch/alignmentview.php",
            "https://lbrich.cern.ch/plot/index.html?rich="
            + str(self.whichRich)
            + "&source=mirror",
        ]
        text2 += "\n More information is available at the following links: \n\n "
        text2 += "\n".join(links)

        text2 += "\n\n Fill number of first run processed: " + str(FillAlignment)

        auto_update_msg = (
            ""
            if self.autoUpdate
            else " \n\n The initial state of autoUpdate was set to False in the configuration."
        )
        text2 += auto_update_msg

        self.sendEmail(subj2, text2, self.emails if not self.testing else self.experts)

        return messageID

    def writeSummary(
        self,
        vN_DB_str,
        conv,
        n_it,
        farm_config_time,
        elapsed_runtimes,
        summaryFile,
        DBUpdated,
        nEvents,
        nEventsList,
    ):
        farm_config_time_str = str(datetime.timedelta(seconds=farm_config_time))
        alignment_time_str = str(
            datetime.timedelta(seconds=elapsed_runtimes["elapsed_time_run"])
        )
        MirrCombinFit_time_str = str(
            elapsed_runtimes["elapsed_times_RichMirrCombinFit"]
        )
        MirrAlign_time_str = str(elapsed_runtimes["elapsed_times_RichMirrAlign"])

        if self.flat is False:
            if "online" in getpass.getuser():
                import OnlineEnvBase as Online  # import OnlineEnv as Online

                online_options = alignment_options(Online)
                runs = online_options.runs  # runs = Online.DeferredRuns
                # PN - Extract the run number substring from the full string - WILL NOT WORK IF RUN NUMBERS BECOME 7 DIGITS
                runs = [entry[-6:] for entry in runs]
            else:
                runs = []  # PN: for testbench
        else:
            runs = []
        runsString = "  ".join(str(run) for run in runs)
        fillsString = "  ".join(
            str(self.getFillNumber(run))
            if self.getFillNumber(run) is not None
            else "  ????  "
            for run in runs
        )
        polaritiesString = "  ".join(
            {"UP": "    UP", "DOWN": "  DOWN", "OFF": "   OFF"}.get(
                self.getPolarity(run), "  ????  "
            )
            for run in runs
        )

        finalCKres = math.ceil(self.finalCKres * 10000) / 10000
        finalCKresErr = math.ceil(self.finalCKresErr * 10000) / 10000

        print("INFO: LHCbA config time (readable): " + farm_config_time_str)
        # print("INFO: Note, the LHCbA config time may be inaccurate if the alignment is run manually with LHCbA AutoPilot OFF.")
        print("INFO: Alignment time    (readable): " + alignment_time_str)
        args = (
            f"richDetector            = {self.whichRich}\n"
            f"DB updated              = {DBUpdated}\n"
        )
        args += (
            f"                          (Alignment changed, but autoUpdate started as False or was disabled)\n"
            if DBUpdated is None
            else f""
        )
        args += (
            f"resulting DB            = {vN_DB_str}\n"
            f"converged               = {conv}\n"
            f"Number of iterations    = {n_it + 1}\n"
            f"Final iteration label   = _i{n_it}\n"
            f"Number of events        = {nEvents}\n"
            f"runs                    = {runsString}\n"
            f"fills                   = {fillsString}\n"
            f"polarities              = {polaritiesString}\n"
            f"dataVariant             = {self.dataVariant}\n"
            f"savedir                 = {self.savedir}\n"
            f"magnFactorsMode         = {self.magnFactorsMode}\n"
            f"magnifDir               = {self.magnifDir}\n"
            f"XML_YAML_configDir      = {self.XML_YAML_configDir}\n"
            # f'combAndMirrSubsets      = {self.combAndMirrSubsets}\n'
            f"LHCbA config time       = {farm_config_time_str}\n"
            f"Alignment time          = {alignment_time_str}\n"
            f"CK angle resolution     = {finalCKres}\n"
            f"         error          = {finalCKresErr}\n"
            f"startXMLFile            = {self.alignConf.getProp('startXMLFile')}\n"
            f"compareXMLFile          = {self.alignConf.getProp('compareXMLFile')}\n"
            f"startYAMLFile           = {self.alignConf.getProp('startYAMLFile')}\n"
            f"compareYAMLFile         = {self.alignConf.getProp('compareYAMLFile')}\n"
            f"YAMLio                  = {self.YAMLio}\n"
            f"useDD4Hep               = {self.useDD4hep}\n"
            f"\n"
            f"autoUpdate (initial)    = {self.autoUpdate}\n"
            f"autoUpdateNoV           = {self.autoUpdateNoV}\n"
            f"testing                 = {self.testing}\n"
            f"requiredEvents          = {self.alignConf.getProp('requiredEvents')}\n"
            f"minAverageBinPop        = {self.minAverageBinPop}\n"
            f"phiBinFactor            = {self.phiBinFactor}\n"
            f"minFracPhiBinsPopulated = {self.minFracPhiBinsPopulated}\n"
            f"deltaThetaWindow        = {self.deltaThetaWindow}\n"
            f"combinFitMethod         = {self.combinFitMethod}\n"
            f"solutionMethod          = {self.solutionMethod}\n"
            f"coeffCalibTilt          = {self.coeffCalibTilt}\n"
            f"fixSinusoidShift        = {self.fixSinusoidShift}\n"
            f"maxIterations           = {self.alignConf.getProp('maxIterations')}\n"
            f"useHltDecisions         = {self.alignConf.getProp('useHltDecisions')}\n"
            f"usingReducedSubset      = {self.usingReducedSubset}\n"
            f"EvtMax                  = {self.alignConf.getProp('EvtMax')}\n"
            f"regularizationMode      = {self.alignConf.getProp('regularizationMode')}\n"
            f"stopDecisionMode        = {self.alignConf.getProp('stopDecisionMode')}\n"
            f"stopToleranceMode       = {self.alignConf.getProp('stopToleranceMode')}\n"
            f"nominalResolutionSigma  = {self.alignConf.getProp('nominalResolutionSigma')}\n"
            f"stopSigmaFraction       = {self.alignConf.getProp('stopSigmaFraction')}\n"
            f"warningFactor           = {self.alignConf.getProp('warningFactor')}\n"
            f"stopTolerancePriY       = {self.alignConf.getProp('stopTolerancePriY')}\n"
            f"stopTolerancePriZ       = {self.alignConf.getProp('stopTolerancePriZ')}\n"
            f"stopToleranceSecY       = {self.alignConf.getProp('stopToleranceSecY')}\n"
            f"stopToleranceSecZ       = {self.alignConf.getProp('stopToleranceSecZ')}\n"
            f"nEventsList             = {nEventsList}\n"
            f"CombinFit times         = {MirrCombinFit_time_str}\n"
            f"MirrAlign times         = {MirrAlign_time_str}\n"
        )
        # not provided yet (not sure if there is need):
        # MajItStart and MinItStart
        # WorkDir, ItNrFile, thisCase, tiltNames, and original SaveDir in configuration
        # initial nameStr

        if os.path.exists(summaryFile):
            print(f"INFO: Removing existing {summaryFile}")
            os.remove(summaryFile)
        with open(summaryFile, "w") as f:
            print(f"INFO: Writing {summaryFile}")
            f.write(args)
        sys.stdout.flush()

        richFilesDir = self.alignConf.getProp(
            "richFiles"
        )  # '/group/rich/AlignmentFiles/'
        CKresTrendFile = (
            f"{richFilesDir}Logging/Rich" + str(self.whichRich) + "_CKresTrend.txt"
        )
        UpdateTrendFile = (
            f"{richFilesDir}Logging/Rich" + str(self.whichRich) + "_UpdateTrend.txt"
        )

        if self.flat is False:
            if "online" in getpass.getuser():
                firstFill = self.getFillNumber(runs[0])
            else:
                firstFill = None  # PN: for testbench
                firstPolarity = None  # PN: for testbench
                firstCollisionInfo = None  # PN: for testbench
        else:
            firstFill = None
            firstPolarity = None
            firstCollisionInfo = None

        if firstFill is not None:
            firstFill_str = "" + str(firstFill) + ""
        else:
            firstFill_str = "????"

        if firstFill is not None:
            firstPolarity = self.getPolarity(runs[0])
            firstCollisionInfo = self.getCollisionInfo(runs[0])

            firstPolarity_str = (
                "  UP"
                if firstPolarity == "UP"
                else "DOWN"
                if firstPolarity == "DOWN"
                else " OFF"
                if firstPolarity == "OFF"
                else "????"
            )

            firstCollisionInfo_str = (
                "" + str(firstCollisionInfo) + ""
                if firstCollisionInfo is not None
                else "????"
            )
        else:
            firstPolarity_str = "????"
            firstCollisionInfo_str = "????"

        if not self.testing:
            with open(CKresTrendFile, "a") as file:
                file.write(
                    f"{firstFill_str} {(self.directoryTime).replace('_', 'T')} {self.finalCKres / 1000} {self.finalCKresErr / 1000}\n"
                )
            with open(UpdateTrendFile, "a") as file:
                file.write(
                    f"{firstFill_str}\t{self.directoryTime}\t{vN_DB_str}\t{firstPolarity_str}\t{firstCollisionInfo_str}\n"
                )
            if "maybe" in vN_DB_str:
                maybeSubj = "A _maybe version has been produced by the mirror alignment"
                maybeText = f"A _maybe version has been produced by the mirror alignment. \n As you address this, also make sure to update the entry in {richFilesDir}Logging/Rich{self.whichRich}_UpdateTrend.txt, with the result (i.e. a proper vN, or the existing vN). Also you may wish to comment out the relevant line in {richFilesDir}Logging/Rich{self.whichRich}_CKresTrend.txt if you do not wish it to appear in the CK angle resolution trend plot; you could also copy the previous line's resolution, or just leave it if it is not a fluctuation."
                self.sendEmail(maybeSubj, maybeText, self.emails)
        else:
            with open(CKresTrendFile, "a") as file:
                file.write(
                    f"# TEST {firstFill_str} {(self.directoryTime).replace('_', 'T')} {self.finalCKres / 1000} {self.finalCKresErr / 1000}\n"
                )
            with open(UpdateTrendFile, "a") as file:
                file.write(
                    f"# TEST {firstFill_str}\t{self.directoryTime}\t{vN_DB_str}\t{firstPolarity_str}\t{firstCollisionInfo_str}\n"
                )
            if "testing_maybe" in vN_DB_str:
                maybeSubj = (
                    "A _testing_maybe version has been produced by the mirror alignment"
                )
                maybeText = f"A _testing_maybe version has been produced by the mirror alignment. \n If you choose to use this alignment for this fill(s), make sure as you address this to update the entry in {richFilesDir}Logging/Rich{self.whichRich}_UpdateTrend.txt, with the result (i.e. a proper vN). Also uncomment the '# TEST ' part of the relevant line in {richFilesDir}Logging/Rich{self.whichRich}_CKresTrend.txt please."
                self.sendEmail(maybeSubj, maybeText, self.experts)

    def sendEmail(self, subject, text, receiversList):
        sender = "lhcb-rich-mirror-alignment-development@cern.ch"
        message = "From: MirrAlign Monitor <{}>\n".format(sender)
        message += "To: {}\n".format(", ".join(receiversList))
        message += "Subject: {}\n\n".format(subject)
        message += "{}\n\n".format(text)

        try:
            smtpObj = smtplib.SMTP("localhost")
            smtpObj.sendmail(sender, receiversList, message)
            print("INFO: Successfully sent email")
            return True  # PN: probably not needed, hopefully harmless
        except smtplib.SMTPException:
            print("ERROR: Unable to send email")
            return False  # PN: probably not needed, hopefully harmless

    # Engineered from https://gitlab.cern.ch/lhcb/Lovell/blob/master/VeloAnalysisFramework/python/veloview/utils/rundb.py
    def getFillNumber(self, runNumber):
        """
        Given a run number, return the fill number for the corresponding fill.
        """
        try:
            run_info = self.queryRunDB(runNumber)
            # Check for null responses
            if not run_info:
                return None
            # Check if fill ID is missing or invalid
            if run_info["fillid"] is None or run_info["fillid"] == 0:
                return None
            fill_number = run_info["fillid"]
        except:
            print(f"WARNING: getFillNumber() = None")
            # Error occurred when querying RunDB API
            return None
        return fill_number

    # Engineered from https://gitlab.cern.ch/lhcb/Lovell/blob/master/VeloAnalysisFramework/python/veloview/utils/rundb.py
    def getCollisionInfo(self, runNumber):
        response = self.queryRunDB(runNumber)
        # Check for null responses
        if not response:
            return None
        try:
            collisionInfo = ""
            # OLD, doesn't work because hlt2TriggerSettings are not assigned in the RunDB until after the mirror alignment version is provided
            # collisionInfo = '|'.join(response['hlt2TriggerSettings'].split('|')[1:2])[7:] # This should extract the L0TCK recipe name, see https://lbtwiki.cern.ch/bin/view/L0/L0TCK for recipes
        except KeyError:
            # CollisionInfo not included in JSON response
            return None
        return collisionInfo

    # Engineered from https://gitlab.cern.ch/lhcb/Lovell/blob/master/VeloAnalysisFramework/python/veloview/utils/rundb.py
    def getPolarity(self, runNumber):
        """Get the polarity of a given run number from the LBRUNDB.

        Args:
            runNumber (int): The run number to query.

        Returns:
            Polarity or None: The polarity of the run, or None if the polarity
            could not be determined.
        """
        run_info = self.queryRunDB(runNumber)
        if not run_info:
            return None
        polarity = run_info.get("magnetState")
        if not polarity:  # Polarity not included in JSON response
            return None
        # API returns UP for magnet up, DOWN for magnet down, OFF for off
        return polarity.upper()

    # Engineered from https://gitlab.cern.ch/lhcb/Lovell/blob/master/VeloAnalysisFramework/python/veloview/utils/rundb.py
    def queryRunDB(self, runNumber):
        import json
        import urllib.request

        RUNDB_API_URL = f"http://rundb-internal.lbdaq.cern.ch/api/run/{runNumber}/"  # e.g. see http://lbrundb.cern.ch/api/run/185380/ for what's available
        # RUNDB_API_URL = f'https://lbrundb.cern.ch/api/run/{runNumber}/'  # e.g. see http://lbrundb.cern.ch/api/run/185380/ for what's available
        request = urllib.request.urlopen(RUNDB_API_URL)
        # Make sure the request was OK, not e.g. 404 not found
        if request.getcode() != 200:
            print(
                f"WARNING: request.getcode() is {request.getcode()}. Unsuccessfully querying LHCb RunDB."
            )
            return None
        try:
            # print(f"DEBUG: request.getcode() is 200. Trying to query the LHCb RunDB.")
            response_data = request.read().decode("utf-8")
            # print(f"DEBUG: Response data: {response_data}")
            run_json = json.loads(response_data)
        except ValueError as e:
            # Couldn't decode the response
            print(
                f"WARNING: Unsuccessfully querying LHCb RunDB. Couldn't decode the response: {str(e)}"
            )
            return None
        # print("INFO: Querying of the LHCb RunDB is Successful.")
        return run_json

    def getNEventsListList(self, n_it):
        # yaml = YAML()
        # with open(f"{self.workdir}/rich{self.whichRich}_variant.yml") as finp:
        #    current_variant = yaml.load(finp)
        # file_prefix = f"{self.workdir}/{current_variant}_histos_i"
        file_prefix = f"{self.workdir}RichDataTuple_i"
        # print(file_prefix)
        event_counts = []
        for j in range(n_it + 1):
            useTextFile = True
            if useTextFile is True:
                # Define the pattern to match the files
                analyzerOutDir = os.path.join(
                    f"{self.workdir}Rich{self.whichRich}AnalyzerOutput", ""
                )
                file_pattern = (
                    f"{analyzerOutDir}Rich{self.whichRich}AnalyzerOut_i{j}*txt"
                )

                # Initialize a variable to store the sum
                total_sum = 0

                # Iterate over the matching files
                for filename in glob.glob(file_pattern):
                    with open(filename, "r") as file:
                        for line in file:
                            if "createODIN/Decode_ODIN" in line and "Sum=" in line:
                                # Extract the sum value from the line
                                sum_value = int(line.split("Sum=")[1].split()[0])
                                total_sum += sum_value

                event_counts.append(total_sum)
            """
            else:
                filename = f"{file_prefix}{j}.root"
                #print(filename)
                if os.path.exists(filename):
                # Open ROOT file
                    with TFile(filename, "read") as rootFile:
                        # Get the TTree object from the ROOT file
                        RichDataTree = rootFile.Get("RichData")  # won't work on MC
                        # Check if RichDataTree is a valid object
                        if RichDataTree:
                            # Get the number of entries in the TTree
                            n_events = int(RichDataTree.GetEntries())
                            event_counts.append(n_events)
                        else:
                            print("ERROR: RichDataTree is not a valid TTree object.")
                else:
                    event_counts.append(0)
            """
        # Calculate the denominator of the average, based on the count of zero values appearing in the event_counts list
        denominator = n_it + 1
        if event_counts.count(0) > 0:
            denominator -= event_counts.count(0)
            print(
                f"INFO: Excluding {event_counts.count(0)} of the iterations from the calculation of the average number of events. This is probably due to re-starting the alignment from a non-'0th' iteration."
            )
        # Calculate the average number of events
        if denominator <= 0:
            average_n_events = 0
            print(
                f"INFO: Average number of events set to zero. See SetupHelper.py to begin understanding why this might have happened."
            )
        else:
            average_n_events = int(
                round(sum(event_counts) / denominator)
            )  # If mag factors float, this only reports for the data with untilted XML/YAML
            print(f"INFO: Average number of events: {average_n_events}")

        return [event_counts, average_n_events]

    def quickWriteInLogbook(
        self,
        subject="",
        text="",
        edit=None,
        instruction_file_url="https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbRun3RichMirrAlignShiftInfo",
    ):
        host = "logbook.lbdaq.cern.ch"
        port = 8080
        username = "common Common\!"  # used to be 'common Common\\!' but now works with only one escape
        if self.testing:
            logbook = "TestLogbook"
            systemName = "System"
            activity = "Test"
            author = "Generic Elog Account"
        else:
            logbook = "RICH"
            systemName = "System_19"  # Change 19 if Alignment changes its order in the ELOG (ask the current RICH expert)
            activity = "Alignment"
            author = "MirrAlign Monitor"  # Or any other name

        if instruction_file_url:
            text += f"\nShifter's instructions can be found at {instruction_file_url}."

        e_string = ""
        try:
            e_string += " -e " + str(int(edit))
        except:
            pass
            # print(f"INFO: e_string failed")

        command = ""
        command += (
            f'/group/online/bin/elog -h "{host}" -p {port}'
            f"{e_string}"
            f' -l "{logbook}" -u {username}'
            f' -a "Author={author}"'
            f' -a "{systemName}={activity}"'
            f' -a "Subject={subject}"'
            f' "{text}"'
        )
        # print(f'DEBUG: command = elog -h "logbook.lbdaq.cern.ch" -p 8080 -l "RICH" -u common Common\! -a "Author=MirrAlign Monitor" -a "System_19=Alignment" -a "Subject=TEST" "Testing the writing of logbook entries from the mirror alignment.\n RICH piquets should ignore all mirror alignments marked TEST in the subject heading.\n (Notes: common / System_19)"')
        # print(f"DEBUG: command = {command}")
        cmd = shlex.split(command)

        try:
            env = os.environ.copy()
            library_path = os.environ["LD_LIBRARY_PATH"]
            env["LD_LIBRARY_PATH"] = (
                f"{library_path}:/opt/nvidia/nsight-compute/2022.4.0/host/target-linux-x64/CollectX/"
            )
            elogret = subprocess.check_output(cmd, stderr=subprocess.STDOUT, env=env)
            messageID_match = re.search(
                r".*Message successfully transmitted, ID=(\d*)", elogret.decode()
            )
            if messageID_match:
                messageID = int(messageID_match.group(1))
            else:
                print(
                    "WARNING: Failed to extract message ID (quick write to RICH ELOG)"
                )
                messageID = None
        except:
            messageID = None
            print(f"WARNING: Exception (quick write to RICH ELOG)")
        """
        # Alternative way
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=env)
        output = p.communicate()[0]
        messageID_match = re.search(r'.*Message successfully transmitted, ID=(\d*)', output.decode())
        if messageID_match:
            messageID = int(messageID_match.group(1))
        else:
            print("WARNING: Failed to extract message ID")
            messageID = None
        """
        print(f"INFO: messageID = {messageID} (quick write to RICH ELOG)")

        sys.stdout.flush()
        return messageID

    ########## OLD functions below here ###########

    def old_startMinIt(self, m_it):
        removeIt = (m_it - m_it % self.tiltNamesLength) / self.tiltNamesLength
        files = os.listdir(self.workdir)

        re_fitFolders = re.compile(
            r"Rich"
            + str(self.whichRich)
            + "MirrCombinFit_"
            + self.nameStr
            + "[\w\.]*_i"
            + str(removeIt)
        )
        re_alignFiles = re.compile(
            r"Rich"
            + str(self.whichRich)
            + "MirrAlign[a-zA-z]*_i"
            + str(removeIt)
            + "\.txt"
        )
        fitFolders = [x for x in files if re_fitFolders.match(x)]
        alignFiles = [x for x in files if re_alignFiles.match(x)]

        if m_it % self.tiltNamesLength == 0:
            connectStr = ""
        else:
            connectStr = "_"
        yaml = YAML()
        with open(self.workdir + "rich" + str(self.whichRich) + "_variant.yml") as finp:
            variant = yaml.load(finp)
        rootFile = (
            variant
            + "_histos_"
            + self.tiltNames[m_it % self.tiltNamesLength]
            + "_i"
            + str(removeIt)
            + ".root"
        )  #

        for f in fitFolders:
            print("INFO: Removing " + self.workdir + f)
            shutil.rmtree(self.workdir + f)
        for f in alignFiles:
            print("INFO: Removing " + self.workdir + f)
            os.remove(self.workdir + f)
        if os.path.exists(self.workdir + rootFile):
            print("INFO: Removing " + self.workdir + rootFile)
            os.remove(self.workdir + rootFile)

    def old_writeInLogbook(
        self,
        vN_DB_str,
        conv,
        n_it,
        farm_config_time,
        alignment_time,
        summaryFile,
        ChangeWRTDBFile,
        AlignSummaryFile,
        AlignMonitorFile,
        AlignTrendFile_Fills_hollow,
        DBUpdated,
        nEvents,
        edit,
        flat=False,
    ):
        hh, r = divmod(farm_config_time, 3600)
        mm, ss = divmod(r, 60)
        farm_config_time_str = "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
        hh, r = divmod(alignment_time, 3600)
        mm, ss = divmod(r, 60)
        alignment_time_str = "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)

        if flat is False:
            # import OnlineEnv as Online
            # FillAlignment = self.getFillNumber(Online.DeferredRuns[0])
            FillAlignment = 0  # PN: temporary
            pass
        else:
            FillAlignment = 0

        # RICH ELOG
        host = "logbook.lbdaq.cern.ch"
        port = 8080
        username = "common Common\\!"
        logbook = "RICH"
        author = "MirrAlign Monitor"  # Or any other name
        activity = "Alignment"
        subject = ""
        text = ""
        instruction_file = (
            "https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbRun3RichMirrAlignShiftInfo"
        )
        subjText = ""
        if DBUpdated:
            subjText += " updated,"
        elif (
            self.testing and DBUpdated is None
        ):  # if testing is on, regardless of autoUpdate setting
            subjText += " not updated due to testing = True,"
        elif (
            DBUpdated is None and self.autoUpdate is False
        ):  # testing and autoUpdate were off to begin with
            subjText += " not updated due to autoUpdate = False, needs MirrAlign expert decision,"
        elif (
            DBUpdated is None and self.autoUpdateNoV is True
        ):  # testing off, but autoUpdate was on at the start, then switched off
            subjText += " not updated due to sanity check issue, needs MirrAlign expert decision,"
        elif (
            DBUpdated is None and self.autoUpdateNoV is False
        ):  # testing off, but autoUpdate was on at the start, then switched off
            subjText += " not updated due to sanity check issue,"  # existing version was provided
        else:
            subjText += ""
        if self.testing:
            subject += "TEST [" + self.dataVariant + "]: "
            text += (
                "TEST ["
                + self.dataVariant
                + "]: This is a test alignment being run by a mirror alignment expert; RICH piquets may ignore it. \n"
            )
        text += "The Mirror "
        text += str(activity)
        text += " for RICH"
        text += str(self.whichRich)
        if conv:
            text += " ran for "
            text += alignment_time_str
            text += " (excluding an LHCbA configuration time of "
            text += farm_config_time_str
            text += ") and converged. "
            subject += (
                "RICH"
                + str(self.whichRich)
                + " alignment "
                + self.directoryTime
                + subjText
                + " converged"
            )
        else:
            text += " ran for "
            text += alignment_time_str
            text += " (excluding an LHCbA configuration time of "
            text += farm_config_time_str
            text += ") and did not converge. "
            subject += (
                "RICH"
                + str(self.whichRich)
                + " alignment "
                + self.directoryTime
                + subjText
                + " incomplete"
            )
        text += "We processed approximately "
        text += str(nEvents)
        text += " events per iteration from the RICH"
        text += str(self.whichRich)
        text += "Decision of our Hlt1CalibRICHMirror HLT line. "
        text += str(n_it + 1)
        text += " iteration(s) occured. Mirror "
        text += str(activity)
        text += " monitoring information and plots are attached; shifter's instructions can be found at "
        text += str(instruction_file)
        text += " and the output directory timestamp is "
        text += self.directoryTime
        text += "."
        if DBUpdated is None:
            text += (
                " There was a significant change from the alignment in the DB, or a change in magnet polarity."
                + "However, the final state of autoUpdate was set to False (even if the initial state of autoUpdate was True), "
                + "or the alignment is in testing mode, so the DB was not updated. "
                + "If the automatic update feature was disabled, a sanity check was not satisfied. "
                + "Please notify a MirrAlign expert to investigate whether this alignment ("
                + vN_DB_str
                + ") should be installed."
            )
            # We should maybe provide more info here
            if self.autoUpdate and self.autoUpdateNoV:
                text += (
                    " Please note: autoUpdate was initially True, and autoUpdateNoV was set to True. "
                    + "This means that an alignment version has NOT been provided to HLT2."
                )
        elif DBUpdated:
            text += (
                " Due to a significant change from the alignment in the DB, or a change in magnet polarity, the DB was automatically updated to "
                + vN_DB_str
                + "."
            )  # We should probably provide more info here
        else:
            text += " The alignment in the DB is still " + vN_DB_str + "."
        if not self.autoUpdate:
            text += " The initial state of autoUpdate was set to False in the configuration."
        command = 'elog -h "'
        command += str(host)
        command += '" -p '
        command += str(port)
        try:
            command += " -e " + str(int(edit))
        except:
            pass
        command += ' -l "'
        command += str(logbook)
        command += '" -u '
        command += str(username)
        command += ' -a "Author='
        command += str(author)
        # command += '" -a "System='
        command += '" -a "System_19='  # Change 19 if Alignment changes its order in the ELOG (ask the current RICH expert)
        command += str(activity)
        command += '" -a "Subject='
        command += str(subject)
        command += '" "'
        command += str(text)
        command += '"'

        if os.path.exists(summaryFile):
            command += ' -f "{0}"'.format(summaryFile)
        if os.path.exists(ChangeWRTDBFile):
            command += ' -f "{0}"'.format(ChangeWRTDBFile)
        if os.path.exists(AlignSummaryFile):
            command += ' -f "{0}"'.format(AlignSummaryFile)
        if os.path.exists(AlignTrendFile_Fills_hollow):
            command += ' -f "{0}"'.format(AlignTrendFile_Fills_hollow)
        cmd = shlex.split(command)
        try:
            elogret = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
            messageID = int(
                re.search(
                    ".*Message successfully transmitted, ID=(\d*)", elogret
                ).groups()[0]
            )
            print(f"INFO: messageID = {messageID}")
        except:
            messageID = None

        # Alignment ELOG
        logbookAlignment = "Alignment monitoring"  # always
        authorAlignment = "monibot"  # always
        activityAlignment = "Rich" + str(self.whichRich) + " Mirrors"  # always
        TypeAlignment = "Convergence"  # always
        # subjectAlignment = 'IGNORE-- '
        # textAlignment = 'Please Ignore: \n '
        subjectAlignment = ""
        textAlignment = ""
        StatusAlignment = "Good"  # or can be changed below to 'Unchecked' or 'Bad' (for now 'Bad' never happens)

        if self.testing:
            subjectAlignment += "TEST [" + self.dataVariant + "]: "
            textAlignment += (
                "A test mirror alignment for Rich"
                + str(self.whichRich)
                + " was performed for fill "
                + str(FillAlignment)
                + " by a RICH mirror alignment expert. No action is necessary; ignore the following. \n "
            )
        if DBUpdated is None:
            subjectAlignment += "No alignment update-- Needs MirrAlign expert decision"
            textAlignment += (
                "Rich"
                + str(self.whichRich)
                + " mirror alignment for fill "
                + str(FillAlignment)
                + " is requesting an update be made, but for some reason this was not done automatically. Please ask the Rich Mirror Alignment experts to investigate the draft alignment ("
                + vN_DB_str
                + "). Monitoring plots in the attachment; shifter's instructions can be found at: "
                + instruction_file
            )
            StatusAlignment = "Unchecked"
        elif DBUpdated:
            subjectAlignment += "Monitoring plots"
            textAlignment += (
                "Updated Rich"
                + str(self.whichRich)
                + " mirror alignment for fill "
                + str(FillAlignment)
                + " ("
                + vN_DB_str
                + "), monitoring plots in the attachment; shifter's instructions can be found at: "
                + instruction_file
            )
            StatusAlignment = "Unchecked"
        else:
            subjectAlignment += "No alignment update"
            textAlignment += (
                "No Rich"
                + str(self.whichRich)
                + " alignment update needed for fill "
                + str(FillAlignment)
                + ". The DB alignment is still "
                + vN_DB_str
                + "."
            )
        if self.testing:
            StatusAlignment = "Test"

        command = '/group/online/bin/elog -h "'
        command += str(host)
        command += '" -p '
        command += str(port)
        command += ' -l "'
        command += str(logbookAlignment)
        command += '" -u '
        command += str(username)
        command += ' -a "Author='
        command += str(authorAlignment)
        command += '" -a "System='
        command += str(activityAlignment)
        command += '" -a "Subject='
        command += str(subjectAlignment)
        command += '" -a "Type='
        command += str(TypeAlignment)
        command += '" -a "Fill='
        command += str(FillAlignment)
        command += '" -a "Status='
        command += str(StatusAlignment)
        command += '" "'
        command += str(textAlignment)
        command += '"'

        if DBUpdated or DBUpdated is None:
            if os.path.exists(AlignMonitorFile):
                command += ' -f "{0}"'.format(AlignMonitorFile)
        cmd = shlex.split(command)
        if self.autoUpdate:  # If we are not in autoUpdate mode, then we do not post to the Alignment Monitoring ELOG
            FNULL = open(os.devnull, "w")
            subprocess.call(cmd, stdout=FNULL, stderr=subprocess.STDOUT)

        # Email
        subj2 = ""
        if self.testing:
            subj2 += "TEST [" + self.dataVariant + "]: "
        text2 = "The Mirror "
        text2 += str(activity)
        text2 += " for RICH"
        text2 += str(self.whichRich)
        if conv:
            text2 += " ran for "
            text2 += alignment_time_str
            text2 += " and converged. "
            subj2 += "RICH{0} alignment {1} converged: New ELOG entry".format(
                self.whichRich, self.directoryTime
            )
        else:
            text2 += " ran for "
            text2 += alignment_time_str
            text2 += " and did not converge. Approximately "
            subj2 += "RICH{0} alignment {1} incomplete: New ELOG entry".format(
                self.whichRich, self.directoryTime
            )
        text2 += str(nEvents)
        text2 += " events per iteration were processed. "
        text2 += str(n_it + 1)
        text2 += " iteration(s) occured. \n"
        if DBUpdated is None:
            text2 += (
                "\n There was a significant change from the alignment in the DB, or a change in magnet polarity. However the automatic update feature is off or was disabled during the alignment (The final state of autoUpdate was set to False), so the DB was not updated. If the automatic update feature was disabled, there may not have been enough events processed, or a sanity check was not satisfied. A MirrAlign expert should investigate whether this draft alignment ("
                + vN_DB_str
                + ") should be installed. \n"
            )  # We should probably provide more info here
        elif DBUpdated:
            text2 += (
                "\n Due to a significant change from the alignment in the DB, or a change in magnet polarity, the DB was automatically updated to "
                + vN_DB_str
                + ". \n"
            )  # We should probably provide more info here
        else:
            text2 += "\n The alignment in the DB is still " + vN_DB_str + "."

        text2 += "\n More information is available at the following links: \n\n "
        text2 += "https://lblogbook.cern.ch/RICH/?System=%5EAlignment%24&Subject={0}&mode=full".format(
            self.directoryTime
        )
        text2 += "\n https://lbrich.cern.ch/alignmentview.php"
        text2 += (
            "\n https://lbrich.cern.ch/plot/index.html?rich="
            + str(self.whichRich)
            + "&source=mirror"
        )

        text2 += "\n\n Fill number of first run processed: " + str(FillAlignment)

        if not self.autoUpdate:
            text2 += " \n\n The initial state of autoUpdate was set to False in the configuration."

        self.sendEmail(subj2, text2, self.emails if not self.testing else self.experts)

        sys.stdout.flush()
        return messageID

    def old_writeSummary(
        self,
        vN_DB_str,
        conv,
        n_it,
        farm_config_time,
        elapsed_runtimes,
        summaryFile,
        DBUpdated,
        nEvents,
        nEventsList,
        flat=False,
    ):
        hh, r = divmod(farm_config_time, 3600)
        mm, ss = divmod(r, 60)
        farm_config_time_str = "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
        hh, r = divmod(elapsed_runtimes["elapsed_time_run"], 3600)
        mm, ss = divmod(r, 60)
        alignment_time_str = "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
        MirrCombinFit_time_str = str(
            elapsed_runtimes["elapsed_times_RichMirrCombinFit"]
        )
        MirrAlign_time_str = str(elapsed_runtimes["elapsed_times_RichMirrAlign"])

        if flat is False:
            # import OnlineEnv as Online
            # runs = Online.DeferredRuns
            runs = []  #  PN: Temporary
            pass
        else:
            runs = []
        runsString = ""
        fillsString = ""
        polaritiesString = ""
        for i in range(0, len(runs)):
            runsString += str(runs[i]) + "  "
            fill = self.getFillNumber(runs[i])
            if fill is not None:
                fillsString += "  " + str(fill) + "  "
            else:
                fillsString += "  ????  "
            polarity = self.getPolarity(runs[i])
            if polarity == "UP":
                polaritiesString += "    UP  "
            elif polarity == "DOWN":
                polaritiesString += "  DOWN  "
            elif polarity == "OFF":
                polaritiesString += "   OFF  "
            else:  # something went wrong
                polaritiesString += "  ????  "

        if os.path.exists(summaryFile):
            print(f"INFO: Removing existing {summaryFile}")
            os.remove(summaryFile)
        f = open(summaryFile, "w")
        args = ""
        args += "richDetector            = " + str(self.whichRich) + "\n"
        args += "DB updated              = " + str(DBUpdated) + "\n"
        if DBUpdated is None:
            args += "                          (Alignment changed, but autoUpdate started as False or was disabled) \n"
        args += "resulting DB            = " + vN_DB_str + "\n"
        args += "converged               = " + str(conv) + "\n"
        args += "Number of iterations    = " + str(n_it + 1) + "\n"
        args += "Final iteration label   = " + "_i" + str(n_it) + "\n"
        args += "Number of events        = " + str(nEvents) + "\n"
        args += "runs                    = " + runsString + "\n"
        args += "fills                   = " + fillsString + "\n"
        args += "polarities              = " + polaritiesString + "\n"
        args += "dataVariant             = " + str(self.dataVariant) + "\n"
        args += "savedir                 = " + self.savedir + "\n"
        args += "magnFactorsMode         = " + str(self.magnFactorsMode) + "\n"
        args += "magnifDir               = " + str(self.magnifDir) + "\n"
        args += "XML_YAML_configDir      = " + str(self.XML_YAML_configDir) + "\n"
        # args += 'combAndMirrSubsets      = ' + str(
        #    self.combAndMirrSubsets) + '\n'
        print("INFO: LHCbA config time (readable): " + farm_config_time_str)
        # print("INFO: Note, the LHCbA config time may be inaccurate if the alignment is run manually with LHCbA AutoPilot OFF.")
        args += "LHCbA config time       = " + farm_config_time_str + "\n"
        print("INFO: Alignment time    (readable): " + alignment_time_str)
        args += "Alignment time          = " + alignment_time_str + "\n"
        finalCKres = math.ceil(self.finalCKres * 10000) / 10000
        finalCKresErr = math.ceil(self.finalCKresErr * 10000) / 10000
        args += "CK angle resolution     = " + str(finalCKres) + "\n"
        args += "         error          = " + str(finalCKresErr) + "\n"
        args += (
            "startXMLFile            = "
            + str(self.alignConf.getProp("startXMLFile"))
            + "\n"
        )
        args += (
            "compareXMLFile          = "
            + str(self.alignConf.getProp("compareXMLFile"))
            + "\n"
        )
        args += (
            "startYAMLFile           = "
            + str(self.alignConf.getProp("startYAMLFile"))
            + "\n"
        )
        args += (
            "compareYAMLFile         = "
            + str(self.alignConf.getProp("compareYAMLFile"))
            + "\n"
        )
        args += "YAMLio                  = " + str(self.YAMLio) + "\n"
        args += "useDD4Hep               = " + str(self.useDD4hep) + "\n"
        args += "\n"
        args += "autoUpdate (initial)    = " + str(self.autoUpdate) + "\n"
        args += "autoUpdateNoV           = " + str(self.autoUpdateNoV) + "\n"
        args += "testing                 = " + str(self.testing) + "\n"
        args += (
            "requiredEvents          = "
            + str(self.alignConf.getProp("requiredEvents"))
            + "\n"
        )
        args += "minAverageBinPop        = " + str(self.minAverageBinPop) + "\n"
        args += "phiBinFactor            = " + str(self.phiBinFactor) + "\n"
        args += "minFracPhiBinsPopulated = " + str(self.minFracPhiBinsPopulated) + "\n"
        args += "deltaThetaWindow        = " + str(self.deltaThetaWindow) + "\n"
        args += "combinFitMethod         = " + str(self.combinFitMethod) + "\n"
        args += "solutionMethod          = " + str(self.solutionMethod) + "\n"
        args += "coeffCalibTilt          = " + str(self.coeffCalibTilt) + "\n"
        args += "fixSinusoidShift        = " + str(self.fixSinusoidShift) + "\n"
        args += (
            "maxIterations           = "
            + str(self.alignConf.getProp("maxIterations"))
            + "\n"
        )
        args += (
            "useHltDecisions         = "
            + str(self.alignConf.getProp("useHltDecisions"))
            + "\n"
        )
        args += "usingReducedSubset        = " + str(self.usingReducedSubset) + "\n"
        args += (
            "EvtMax                  = " + str(self.alignConf.getProp("EvtMax")) + "\n"
        )
        args += (
            "regularizationMode      = "
            + str(self.alignConf.getProp("regularizationMode"))
            + "\n"
        )
        args += (
            "stopDecisionMode        = "
            + str(self.alignConf.getProp("stopDecisionMode"))
            + "\n"
        )
        args += (
            "stopToleranceMode       = "
            + str(self.alignConf.getProp("stopToleranceMode"))
            + "\n"
        )
        args += (
            "nominalResolutionSigma  = "
            + str(self.alignConf.getProp("nominalResolutionSigma"))
            + "\n"
        )
        args += (
            "stopSigmaFraction       = "
            + str(self.alignConf.getProp("stopSigmaFraction"))
            + "\n"
        )
        args += (
            "warningFactor           = "
            + str(self.alignConf.getProp("warningFactor"))
            + "\n"
        )
        args += (
            "stopTolerancePriY       = "
            + str(self.alignConf.getProp("stopTolerancePriY"))
            + "\n"
        )
        args += (
            "stopTolerancePriZ       = "
            + str(self.alignConf.getProp("stopTolerancePriZ"))
            + "\n"
        )
        args += (
            "stopToleranceSecY       = "
            + str(self.alignConf.getProp("stopToleranceSecY"))
            + "\n"
        )
        args += (
            "stopToleranceSecZ       = "
            + str(self.alignConf.getProp("stopToleranceSecZ"))
            + "\n"
        )
        args += "nEventsList             = " + str(nEventsList) + "\n"
        args += "CombinFit times         = " + MirrCombinFit_time_str + "\n"
        args += "MirrAlign times         = " + MirrAlign_time_str + "\n"
        # not provided yet (not sure if there is need):
        # MajItStart and MinItStart
        # WorkDir, ItNrFile, thisCase, tiltNames, and original SaveDir in configuration
        # initial nameStr
        f.write(args)
        f.close()
        sys.stdout.flush()

        CKresTrendFile = (
            "/group/rich/AlignmentFiles/Logging/Rich"
            + str(self.whichRich)
            + "_CKresTrend.txt"
        )
        UpdateTrendFile = (
            "/group/rich/AlignmentFiles/Logging/Rich"
            + str(self.whichRich)
            + "_UpdateTrend.txt"
        )

        if flat is False:
            # firstFill = self.getFillNumber(runs[0])
            firstFill = None  # PN: temporary
            firstPolarity = None  # PN: temporary
            firstCollisionInfo = None  # PN: temporary
            pass
        else:
            firstFill = None
            firstPolarity = None
            firstCollisionInfo = None
        if firstFill is not None:
            firstFill_str = "" + str(firstFill) + ""
        else:
            firstFill_str = "????"
        if firstFill is not None:
            firstPolarity = self.getPolarity(runs[0])
        if firstPolarity == "UP":
            firstPolarity_str = "  UP"
        elif firstPolarity == "DOWN":
            firstPolarity_str = "DOWN"
        elif firstPolarity == "OFF":
            firstPolarity_str = " OFF"
        else:  # something went wrong
            firstPolarity_str = "????"
        if firstFill is not None:
            firstCollisionInfo = self.getCollisionInfo(runs[0])
        if firstCollisionInfo is not None:
            firstCollisionInfo_str = "" + str(firstCollisionInfo) + ""
        else:
            firstCollisionInfo_str = "????"

        if not self.testing:
            with open(CKresTrendFile, "a") as myfile:
                myfile.write(
                    firstFill_str
                    + " "
                    + (self.directoryTime).replace("_", "T")
                    + " "
                    + str(self.finalCKres / 1000)
                    + " "
                    + str(self.finalCKresErr / 1000)
                    + "\n"
                )
            with open(UpdateTrendFile, "a") as myfile:
                myfile.write(
                    firstFill_str
                    + "\t"
                    + self.directoryTime
                    + "\t"
                    + vN_DB_str
                    + "\t"
                    + firstPolarity_str
                    + "\t"
                    + firstCollisionInfo_str
                    + "\n"
                )
            if "maybe" in vN_DB_str:
                maybeSubj = "A _maybe version has been produced by the mirror alignment"
                maybeText = (
                    "A _maybe version has been produced by the mirror alignment. \n As you address this, also make sure to update the entry in /group/rich/AlignmentFiles/Logging/Rich"
                    + str(self.whichRich)
                    + "_UpdateTrend.txt, with the result (i.e. a proper vN, or the existing vN). Also you may wish to comment out the relevant line in /group/rich/AlignmentFiles/Logging/Rich"
                    + str(self.whichRich)
                    + "_CKresTrend.txt if you do not wish it to appear in the CK angle resolution trend plot; you could also copy the previous line's resolution, or just leave it if it is not a fluctuation."
                )
                self.sendEmail(maybeSubj, maybeText, self.emails)
        else:
            with open(CKresTrendFile, "a") as myfile:
                myfile.write(
                    "# TEST "
                    + firstFill_str
                    + " "
                    + (self.directoryTime).replace("_", "T")
                    + " "
                    + str(self.finalCKres / 1000)
                    + " "
                    + str(self.finalCKresErr / 1000)
                    + "\n"
                )
            with open(UpdateTrendFile, "a") as myfile:
                myfile.write(
                    "# TEST "
                    + firstFill_str
                    + "\t"
                    + self.directoryTime
                    + "\t"
                    + vN_DB_str
                    + "\t"
                    + firstPolarity_str
                    + "\t"
                    + firstCollisionInfo_str
                    + "\n"
                )
            if "testing_maybe" in vN_DB_str:
                maybeSubj = (
                    "A _testing_maybe version has been produced by the mirror alignment"
                )
                maybeText = (
                    "A _testing_maybe version has been produced by the mirror alignment. \n If you choose to use this alignment for this fill(s), make sure as you address this to update the entry in /group/rich/AlignmentFiles/Logging/Rich"
                    + str(self.whichRich)
                    + "_UpdateTrend.txt, with the result (i.e. a proper vN). Also uncomment the '# TEST ' part of the relevant line in /group/rich/AlignmentFiles/Logging/Rich"
                    + str(self.whichRich)
                    + "_CKresTrend.txt please."
                )
                self.sendEmail(maybeSubj, maybeText, self.experts)

    def old_sendEmail(self, subject, text, receiversList):
        import smtplib

        sender = "lhcb-rich-mirror-alignment-development@cern.ch"
        message = "From: MirrAlign Monitor <"
        message += sender
        message += ">\n"
        message += "To:"
        for ie in receiversList:
            message += " <"
            message += ie
            message += ">,"
        message = message[:-1]
        message += "\n"
        message += "Subject: "
        message += subject
        message += "\n\n"
        message += "\n{0}\n\n".format(text)
        try:
            smtpObj = smtplib.SMTP("localhost")
            smtpObj.sendmail(sender, receiversList, message)
            print("INFO: Successfully sent email")
        except smtplib.SMTPException:
            print("ERROR: Unable to send email")

    def old_getFillNumber(self, runNumber):
        response = self.queryRunDB(runNumber)
        # Check for null responses
        if not response:
            return None
        try:
            fill = response["fillid"]
        except KeyError:
            # Fill ID not included in JSON response
            return None
        return fill

    def old_getPolarity(self, runNumber):
        # only possible polarity states: UP, DOWN, OFF
        response = self.queryRunDB(runNumber)
        # Check for null responses
        if not response:
            return None
        try:
            polarity = response["magnetState"]
        except KeyError:
            # Polarity not included in JSON response
            return None
        # API returns UP for magnet up, DOWN for magnet down, OFF for off
        return polarity

    def old_getNEventsListList(self, n_it):
        yaml = YAML()
        with open(self.workdir + "rich" + str(self.whichRich) + "_variant.yml") as finp:
            current_variant = yaml.load(finp)
        self.fileprename = self.workdir + current_variant + "_histos_i"
        nEventsList = []
        filename = []
        thisFile = []
        hist = []
        for j in range(0, n_it + 1):
            filename.append(self.fileprename + str(j) + ".root")
            thisFile.append(TFile(filename[j], "read"))
            hist.append(
                thisFile[j].Get("RICH/RichODIN/TriggerType")
            )  # won't work on MC
            nEventsList.append(int(hist[j].GetEntries()))
        nEvents = int(
            round(float(sum(nEventsList)) / float(n_it + 1))
        )  # If mag factors float, this only reports for the data with untilted XML/YAML
        return [nEventsList, nEvents]

    def old_quickWriteInLogbook(self, subject, text, edit=None):
        host = "logbook.lbdaq.cern.ch"
        port = 8080
        username = "common Common\\!"
        logbook = "RICH"
        author = "MirrAlign Monitor"  # Or any other name
        activity = "Alignment"
        instruction_file = (
            "https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbRun3RichMirrAlignShiftInfo"
        )

        text += " \n Shifter's instructions can be found at "
        text += str(instruction_file)
        text += "."

        command = 'elog -h "'
        command += str(host)
        command += '" -p '
        command += str(port)
        try:
            command += " -e " + str(int(edit))
        except:
            pass
        command += ' -l "'
        command += str(logbook)
        command += '" -u '
        command += str(username)
        command += ' -a "Author='
        command += str(author)
        # command += '" -a "System='
        command += '" -a "System_19='  # Change 19 if Alignment changes its order in the ELOG (ask the current RICH expert)
        command += str(activity)
        command += '" -a "Subject='
        command += str(subject)
        command += '" "'
        command += str(text)
        command += '"'
        cmd = shlex.split(command)
        try:
            elogret = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
            messageID = int(
                re.search(
                    ".*Message successfully transmitted, ID=(\d*)", elogret
                ).groups()[0]
            )
            print(f"INFO: messageID = {messageID}")
        except:
            messageID = None

        return messageID

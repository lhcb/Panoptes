###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import datetime
import getpass
import math
import os
import re
import shutil
import subprocess
import sys
import time
from time import gmtime, strftime

from ruamel.yaml import YAML

from PyMirrAlignOnline.Communicator import *

print(f"INFO: RichAnalyzerControl __name__ = {__name__}")


class RichAnalyzerControl:
    def __init__(self):
        # constructor
        pass

    def go(self, whichRich=1):
        self.whichRich = whichRich

        if self.whichRich == 1:
            from Configurables import Rich1MirrAlignOnConf

            # from Configuration import Rich1MirrAlignOnConf
            alignConf = Rich1MirrAlignOnConf()
        elif self.whichRich == 2:
            from Configurables import Rich2MirrAlignOnConf

            # from Configuration import Rich2MirrAlignOnConf
            alignConf = Rich2MirrAlignOnConf()
        alignConf.__apply_configuration__()

        self.workdir = alignConf.getProp("WorkDir")

        # Online_bit
        state = State.NOT_READY
        com = Communicator("ALIGNWRKR")
        print("INFO: Communicator started.")
        com.set_status(state)
        print(f"INFO: Analyzer state = {state}")
        yaml = YAML()
        with open(f"{self.workdir}rich{self.whichRich}_runAnalyzer.yml", "w") as fout:
            yaml.dump("NO", fout)
            print(
                f"INFO: {self.workdir}rich{self.whichRich}_runAnalyzer.yml set to 'NO'"
            )
        while True:
            sys.stdout.flush()  # Always keep this line in
            print("INFO: Waiting for command from Communicator")
            sys.stdout.flush()
            command = com.get_command()
            if "state" in command:  # ignore strange "!state" command from Run Control
                # print('"state" is in command')
                # sys.stdout.flush()
                command = com.get_command()
                # print(command)
                # sys.stdout.flush()
            print(f"INFO: Analyzer Receiving command: {command}")
            print("INFO: ----------------------------")
            if command.startswith("configure") and state == State.NOT_READY:
                print(
                    f"INFO: CONFIGURE command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
                )
                sys.stdout.flush()
                state = State.READY
            elif command.startswith("start") and state == State.READY:
                print(
                    f"INFO: START command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
                )
                sys.stdout.flush()
                state = State.RUNNING
                #            elif (command.startswith('pause') and state == State.RUNNING):
                #            elif (state == State.RUNNING):
                com.set_status(state)
                print(f"INFO: Analyzer state = {state}")
                # print(f"INFO: PAUSE command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC")
                # sys.stdout.flush()
                # self.run()
                runAnalyzer = "UNKNOWN"
                while runAnalyzer != "YES":
                    yaml = YAML()
                    while not os.path.exists(
                        f"{self.workdir}rich{self.whichRich}_runAnalyzer.yml"
                    ):
                        time.sleep(4)
                    with open(
                        f"{self.workdir}rich{self.whichRich}_runAnalyzer.yml"
                    ) as finp:
                        runAnalyzer = yaml.load(finp)
                        print(
                            f"INFO: {self.workdir}rich{self.whichRich}_runAnalyzer.yml set to {runAnalyzer}"
                        )
                    if runAnalyzer != "YES":
                        time.sleep(4)
                sys.stdout.flush()
                yaml = YAML()
                with open(
                    f"{self.workdir}rich{self.whichRich}_iter_number.yml"
                ) as finp:
                    n_it = yaml.load(finp)
                yaml = YAML()
                with open(f"{self.workdir}rich{self.whichRich}_tilt_name.yml") as finp:
                    tiltName = yaml.load(finp)
                if tiltName == "":
                    connectStr = ""
                else:
                    connectStr = "_"
                analyzerOutDir = os.path.join(
                    f"{self.workdir}Rich{self.whichRich}AnalyzerOutput", ""
                )
                if "online" in getpass.getuser():
                    utgid = os.environ["UTGID"]
                    try:
                        worker_id = utgid.split("_")[1]
                    except KeyError:
                        worker_id = utgid
                    richAnalyzerOutFile = f"{analyzerOutDir}Rich{self.whichRich}AnalyzerOut{connectStr}{tiltName}_i{n_it}-{worker_id}.txt"
                    analyzerStdOutFile = f"{analyzerOutDir}Rich{self.whichRich}AnalyzerStdOut{connectStr}{tiltName}_i{n_it}-{worker_id}.txt"
                    analyzerStdErrFile = f"{analyzerOutDir}Rich{self.whichRich}AnalyzerStdErr{connectStr}{tiltName}_i{n_it}-{worker_id}.txt"
                else:
                    richAnalyzerOutFile = f"{analyzerOutDir}Rich{self.whichRich}AnalyzerOut{connectStr}{tiltName}_i{n_it}.txt"
                    analyzerStdOutFile = f"{analyzerOutDir}Rich{self.whichRich}AnalyzerStdOut{connectStr}{tiltName}_i{n_it}.txt"
                    analyzerStdErrFile = f"{analyzerOutDir}Rich{self.whichRich}AnalyzerStdErr{connectStr}{tiltName}_i{n_it}.txt"
                myStdOut = open(analyzerStdOutFile, "w")
                myStdErr = open(analyzerStdErrFile, "w")

                self.dataVariant = alignConf.getProp("dataVariant")
                if self.dataVariant == "Collision24":
                    if self.whichRich == 1:
                        which_data_online = "data_online_Rich1.py"
                    elif self.whichRich == 2:
                        which_data_online = "data_online_Rich2.py"
                else:
                    which_data_online = "data_online.py"

                cmd = f"cd {self.workdir} ; `which gaudirun.py` $RICHMIRRORALIGNMENTONLINEROOT/python/PyMirrAlignOnline/{which_data_online} $RICHMIRRORALIGNMENTONLINEROOT/python/PyMirrAlignOnline/RichAnalyzer.py > {richAnalyzerOutFile} ; cd - "
                print(f"INFO: Starting RichAnalyzer.")
                print(f"INFO: cmd = {cmd}")
                sys.stdout.flush()
                p3 = subprocess.Popen(
                    cmd,
                    shell=True,
                    executable="/bin/bash",
                    stdout=myStdOut,
                    stderr=myStdErr,
                )
                p3.communicate()
                myStdOut.close()
                myStdErr.close()
                sys.stdout.flush()
                print("INFO: RichAnalyzer complete. ")
                sys.stdout.flush()
                state = State.PAUSED
            elif command.startswith("stop") and state in (State.PAUSED):
                print(
                    f"INFO: STOP command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
                )
                sys.stdout.flush()
                state = State.READY
            elif command.startswith("reset"):
                print(
                    f"INFO: RESET command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
                )
                sys.stdout.flush()
                state = State.NOT_READY
                # break
            elif command.startswith("unload"):
                print(
                    f"INFO: UNLOAD command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
                )
                sys.stdout.flush()
                state = State.OFFLINE
                break
            else:
                print(f"ERROR: Analyzer: bad transition from {state} to {command}")
                sys.stdout.flush()
                state = State.ERROR
                break
            # Set our status
            com.set_status(state)
            print(f"INFO: Analyzer state = {state}")
        # Set our status
        com.set_status(state)
        print(f"INFO: Analyzer state = {state}")


if __name__ == "__main__":
    RichAnalyzerControl_instance = RichAnalyzerControl()
    RichAnalyzerControl_instance.go(whichRich=1)

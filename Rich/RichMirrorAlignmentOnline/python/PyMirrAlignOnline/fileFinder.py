###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = "Claire Prouve"
__copyright__ = "University of Bristol, May 2018"


class fileFinder:
    def __init__(
        self,
        _whichRich,
        _minDate=00000000,
        _maxDate=99999999,
        _alignments=[],
        _exclude=[],
    ):
        self.whichRich = _whichRich
        self.minDate = _minDate
        self.maxDate = _maxDate
        self.exclude = _exclude
        self.alignments = _alignments

    def getRich1_20160524to20160824(self):
        print("INFO: getRich1_20160524to20160824")
        alignments = [
            "20160616_205455",
            "20160616_180427",
            "20160616_192744",
            "20160616_182620",
            "20160616_191413",
            "20160616_170644",
            "20160616_174432",
            "20160616_195828",
            "20160616_203146",
            "20160616_185541",
            "20160617_162358",
            "20160824_151337",
            "20160824_123152",
            "20160824_130005",
            "20160824_121406",
        ]
        return alignments

    def findFiles(self):
        fullFiles = {}
        import os
        import re

        startdir = (
            "/group/online/AligWork/MirrorAlignments/Rich" + str(self.whichRich) + "/"
        )
        aligndirs = os.listdir(startdir)

        newalignments = []
        if len(self.alignments) <= 0:
            for a in aligndirs:
                date = a.split("_")
                if self.whichRich == 2:
                    if (
                        int(date[0]) <= self.maxDate
                        and int(date[0]) >= self.minDate
                        and not (a in self.exclude)
                    ):
                        newalignments.append(a)
                elif self.whichRich == 1:
                    if (
                        int(date[0]) >= self.minDate
                        and int(date[0]) <= self.maxDate
                        and not (a in self.exclude)
                    ):
                        newalignments.append(a)
            newalignments.sort()
        else:
            newalignments = self.alignments
        self.alignments = []

        if self.whichRich == 1:
            if self.minDate < 20160824 and self.maxDate > 20160524:
                talignments = self.getRich1_20160524to20160824()
                talignments.extend(newalignments)
                newalignments = talignments

        for i in range(0, len(newalignments)):
            a = newalignments[i]
            alignfiles = os.listdir(startdir + "/" + a)

            # let's see if there are XML files, YAML files, or if both prefer XML
            re_xmlfiles = re.compile(
                r"Rich" + str(self.whichRich) + "CondDBUpdate_[A-Za-z0-9._]*_i[0-9].xml"
            )
            xmlfiles = [x for x in alignfiles if re_xmlfiles.match(x)]
            re_ymlfiles = re.compile(
                r"Rich" + str(self.whichRich) + "CondDBUpdate_[A-Za-z0-9._]*_i[0-9].yml"
            )
            ymlfiles = [x for x in alignfiles if re_ymlfiles.match(x)]

            # check for XML
            file0 = ""
            fileN = ""
            maxit = 0
            for xml in xmlfiles:
                if xml.endswith("_i0.xml"):
                    file0 = startdir + "/" + a + "/" + xml
                else:
                    hasi = xml.split("_")
                    hasi = hasi[len(hasi) - 1].split(".")
                    hasi = hasi[0].replace("i", "")
                    it = int(float(hasi))
                    if it > maxit:
                        maxit = it
                        fileN = startdir + "/" + a + "/" + xml

            # check for YAML
            file0_yml = ""
            fileN_yml = ""
            maxit_yml = 0
            for yml in ymlfiles:
                if yml.endswith("_i0.yml"):
                    file0_yml = startdir + "/" + a + "/" + yml
                else:
                    hasi_yml = yml.split("_")
                    hasi_yml = hasi_yml[len(hasi_yml) - 1].split(".")
                    hasi_yml = hasi_yml[0].replace("i", "")
                    it_yml = int(float(hasi_yml))
                    if it_yml > maxit_yml:
                        maxit_yml = it_yml
                        fileN_yml = startdir + "/" + a + "/" + yml

            if not (file0 == "" or fileN == ""):
                fullFiles[a] = [file0, fileN]
                self.alignments.append(a)
            elif not (file0_yml == "" or fileN_yml == ""):
                fullFiles[a] = [file0_yml, fileN_yml]
                self.alignments.append(a)

        return fullFiles

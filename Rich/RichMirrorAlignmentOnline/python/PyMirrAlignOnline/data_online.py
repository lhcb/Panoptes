###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import getpass
import os
import re
import sys
from datetime import datetime
from pathlib import Path

from MooreOnlineConf.utils import (  # may need to check for online user in offline scenario, let's see
    alignment_options,
    distribute_files,
)
from PyConf.application import ApplicationOptions

filename_expr = re.compile(
    r"Run_(?P<run>\d{10})_HLT(?P<node>\d{5})_(?P<time>(?:\d{8})-(?:\d{6}))-(?P<rest>\d{3})\.mdf"
)

options = ApplicationOptions(
    _enabled=False
)  # Not sure if ApplicationOptions are the same as Moore options, so leave these here.
options.simulation = False
options.conddb_tag = "master"
options.dddb_tag = "run3/trunk"
options.input_type = "MDF"

base_dir = "/calib/align/LHCb/Rich/0000256292/"

# base_dir = "/calib/align/LHCb/Rich/0000261947/"


def sort_names(f):
    f = os.path.basename(f)
    m = filename_expr.match(f)
    if m is None:
        return m
    funs = [int, lambda t: int(datetime.strptime(t, "%Y%m%d-%H%M%S").timestamp()), int]
    return tuple(fun(m.group(k)) for fun, k in zip(funs, ("run", "time", "rest")))


TESTBENCH_single_run = True

absolutely_all_files = (
    True  # not really "absolutely all" if just 2023 data chosen, see below
)
exclude_2022 = False  # True
exclude_2023 = False
divide_2022_2023 = 259566

if absolutely_all_files and exclude_2022 is False and exclude_2023 is False:
    import glob

    base_dir = "/calib/align/LHCb/Rich/*/"
    dirs = glob.glob(base_dir, recursive=True)
    files = []
    for dir_i in dirs:
        files += glob.glob(dir_i + "*")
elif absolutely_all_files and exclude_2022 is True and exclude_2023 is False:
    import glob
    import re

    base_dir = "/calib/align/LHCb/Rich/0000*/"
    dirs = glob.glob(base_dir, recursive=True)
    files = []
    for dir_i in dirs:
        # PN - Extract the number from the directory string using regular expressions - WILL NOT WORK IF RUN NUMBERS BECOME 7 DIGITS
        num = int(re.search(r"/0000(\d{6})/", dir_i).group(1))
        # Check if the number is greater than or equal to 259
        if (
            num >= divide_2022_2023 and num > 265000
        ):  # hack to avoid too many data files
            files += glob.glob(dir_i + "*")
elif absolutely_all_files and exclude_2022 is False and exclude_2023 is True:
    import glob
    import re

    base_dir = "/calib/align/LHCb/Rich/0000*/"
    dirs = glob.glob(base_dir, recursive=True)
    files = []
    for dir_i in dirs:
        # PN - Extract the number from the directory string using regular expressions - WILL NOT WORK IF RUN NUMBERS BECOME 7 DIGITS
        num = int(re.search(r"/0000(\d{6})/", dir_i).group(1))
        # Check if the number is greater than or equal to 259
        if num < divide_2022_2023:
            files += glob.glob(dir_i + "*")
elif absolutely_all_files and exclude_2022 is True and exclude_2023 is True:
    print("You just excluded everything")
else:
    files = [os.path.join(base_dir, f) for f in os.listdir(base_dir)]

# file_size = 0
# for file in files:
#     file_size += os.path.getsize(file)
# print(file_size)
# quit()

# files = files[:1000]
# files = files[:25]

# # files = sorted(files, key=sort_names)

# Files with latest run numbers first, presuming of course that the path naming remains /calib/align/LHCb/Rich/0000######/
files = sorted(files, reverse=True)

options.input_files = files

if "online" in getpass.getuser():
    import OnlineEnvBase as OnlineEnv

    online_options = alignment_options(OnlineEnv)
    if (
        OnlineEnv.PartitionName != "TESTALIGNMENT"
    ):  # usually the case for the Run Control
        print("not on TESTALIGNMENT partition")
        sys.stdout.flush()
        print(f"online_options.runs {online_options.runs}")
        sys.stdout.flush()
        INPUT_DATA_PATH = Path("/calib/align/LHCb/Rich/")
        # INPUT_DATA_PATH = Path(f"/calib/align/LHCb/Rich{whichRich}/")
        files = [
            sorted((INPUT_DATA_PATH / run).iterdir())
            for run in online_options.runs
            if (INPUT_DATA_PATH / run).exists()
        ]
    else:  # testbench, but we never use it from the online account, so we should never get here
        print("on TESTALIGNMENT partition")
        INPUT_DATA_PATH = Path("input_data")
        files = [sorted(INPUT_DATA_PATH.iterdir())]
    print(f"files {files}")
    sys.stdout.flush()
    print(f"{sum(len(sublist) for sublist in files)} .mdf files...")
    sys.stdout.flush()
    file_size = 0
    for file in [item for sublist in files for item in sublist]:
        file_size += os.path.getsize(file)
    print("file_size", file_size)
    sys.stdout.flush()
    print(f"online_options.nodes {online_options.nodes}")
    sys.stdout.flush()

    def distribute_files_alt(nodes, files):
        total_files = sum(files, [])  # Flatten the files into a single list
        n_files_per_node = len(total_files) // len(nodes)  # Number of files per node
        extra_files = len(total_files) % len(nodes)  # Remaining files to distribute

        files_per_node = []
        start_index = 0

        for node in nodes:
            end_index = start_index + n_files_per_node
            if extra_files > 0:
                end_index += 1
                extra_files -= 1

            files_per_node.append(total_files[start_index:end_index])
            start_index = end_index

        return dict(zip(nodes, files_per_node))

    # files_per_node = distribute_files(online_options.nodes, files)
    files_per_node = distribute_files_alt(online_options.nodes, files)
    print(f"files_per_node {files_per_node}")
    sys.stdout.flush()

    utgid = os.environ["UTGID"]
    worker_id = utgid.split("_")[1]
    try:
        input_files = files_per_node[worker_id]
    except KeyError:
        # When testing we run multiple instances on the same node
        # TODO this should probably be done based on the partition name
        #      and also "nodes" should be renamed to workers everywhere.
        print("Trying multiple instances on same node")  # may not work for mirrAlign
        worker_id = utgid
        input_files = files_per_node[worker_id]

    # TODO handle case where input_files is empty (here and in iterator)
    input_files = [str(f) for f in input_files]
    print(f"input_files {input_files}")
    sys.stdout.flush()
    options.input_files = input_files

if TESTBENCH_single_run:
    # TESTBENCH_single_run_number = 256292
    TESTBENCH_single_run_number = 289872
    options.input_files = [
        x for x in options.input_files if f"{TESTBENCH_single_run_number}" in x
    ]

print(f"{len(options.input_files)} .mdf files...")
sys.stdout.flush()
file_size = 0
for file in options.input_files:
    file_size += os.path.getsize(file)
print("file_size", file_size)
sys.stdout.flush()

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = "Paras Naik, Claire Prouve"
__copyright__ = "University of Bristol, June 2023"

import glob
import os
import shutil
import sys
import threading
import time
from time import gmtime, sleep, strftime, time

import pydim
import ROOT
from ruamel.yaml import YAML


class HistoCopier:
    def __init__(self, alignConf):
        self.__tiltNames = alignConf.getProp("tiltNames")
        self.__workdir = alignConf.getProp("WorkDir")
        self.__whichRich = alignConf.getProp("Rich")
        self.__tiltNamesLength = len(self.__tiltNames)

    def resultName(self, n_it, m_it):
        # print(f"n_it {n_it} m_it {m_it}")
        if m_it % self.__tiltNamesLength == 0:
            connectStr = ""
        else:
            connectStr = "_"

        yaml = YAML()
        with open(
            os.path.join(self.__workdir, f"rich{self.__whichRich}_variant.yml")
        ) as finp:
            variant = yaml.load(finp)

        mergename = f"{self.__workdir}{variant}_histos{connectStr}{self.__tiltNames[m_it % self.__tiltNamesLength]}_i{n_it}.root"
        # print(f"mergename {mergename}")

        return mergename

    def getHistoFile(self, n_it, m_it):
        # print(f"n_it {n_it} m_it {m_it}")
        if m_it % self.__tiltNamesLength == 0:
            connectStr = ""
        else:
            connectStr = "_"

        yaml = YAML()
        with open(
            os.path.join(self.__workdir, f"rich{self.__whichRich}_variant.yml")
        ) as finp:
            variant = yaml.load(finp)

        # Specify the file pattern
        file_pattern = f"{self.__workdir}{variant}_histos{connectStr}{self.__tiltNames[m_it % self.__tiltNamesLength]}_i{n_it}-*.root"

        # Get the list of files matching the pattern
        file_list = glob.glob(file_pattern)
        # print(f"file_list {file_list}")

        # Specify the output file name
        resultName = self.resultName(n_it, m_it)
        # self.waitForSaveSet(resultName, m_it)

        ## Create an empty output file
        # outputFile = ROOT.TFile(resultName, "RECREATE")
        # outputFile.Close()

        # Create a TFileMerger object
        merger = ROOT.TFileMerger()

        # Create the output file
        merger.OutputFile(resultName, "RECREATE")

        # Add the files to be merged
        for file_name in file_list:
            merger.AddFile(file_name)

        # Merge the files
        merger.Merge()

        # Close the output file
        del merger
        # merger.OutputFile("")

        # Print a summary of the merged file
        merged_file = ROOT.TFile(resultName, "READ")
        merged_file.ls()

        # Close the merged file
        merged_file.Close()

        # Delete the original files
        for file_name in file_list:
            os.remove(file_name)


"""
class SaveSetRetriever_Run2:
    def __init__(self, part, tsk):
        self.name = part + "/" + tsk + "/SAVESETLOCATION"
        self.format = "C"
        self.SaveSet = ""
        self.lastData = 0.0
        self.__lock = threading.Lock()
        self.__lock.acquire()
        self.__infoID = pydim.dic_info_service(
            self.name,
            self.format,
            self.callback,
            timeout=10000000,
            default_value="Default")
        self.__lock.release()
        print("INFO: Constructed SaveSet Retriever using service name " +
              self.name)

    def callback(self, *args):
        self.__lock.acquire()
        print("INFO: SaveSetRetriever Callback Called with args ", args)
        if len(args) > 1:
            r = args[1]
            r = r[:r.find('\0')]
            self.SaveSet = r
            self.lastData = time()
        self.__lock.release()
        sys.stdout.flush()

    def getSaveSet(self):
        return self.SaveSet

    def getUpdTime(self):
        return self.lastData


class HistoCopier_Run2:
    def __init__(self, alignConf, ssretr):
        self.__tiltNames = alignConf.getProp('tiltNames')
        self.__workdir = alignConf.getProp('WorkDir')
        self.__whichRich = alignConf.getProp('Rich')
        self.__nameStr = alignConf.getProp('nameStr')
        self.__ssretr = ssretr

    def resultName(self, n_it, m_it):
        if m_it % len(self.__tiltNames) == 0:
            mergename = (self.__workdir + "/RichRecQCHistos_rich" + str(
                self.__whichRich) + "_" + self.__nameStr + "_i" + str(n_it) +
                         ".root")
        else:
            mergename = (
                self.__workdir + "/RichRecQCHistos_rich" + str(
                    self.__whichRich) + "_" + self.__nameStr + "_" +
                self.__tiltNames[m_it % self.__tiltNamesLength] + "_i" + str(n_it) + ".root")
        return mergename

    def waitForSaveSet(self, resultName, m_it):
        waitForSs = True
        while (waitForSs):
            SaveSetName = self.__ssretr.getSaveSet()
            if str('00' + str(m_it)) in SaveSetName:
                waitForSs = False
            sleep(1)
        waitForSs = True
        while (waitForSs):
            if os.path.exists(SaveSetName):
                waitForSs = False
            sleep(1)
        print(f"INFO: Copying {SaveSetName} to {resultName}")
        shutil.copyfile(SaveSetName, resultName)

    def getHistoFile(self, n_it, m_it):
        resultName = self.resultName(n_it, m_it)
        self.waitForSaveSet(resultName, m_it)
"""

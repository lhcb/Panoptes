###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

__authors__ = "Sam Maddrell-Mander, Claire Prouve, Paras Naik"
__copyright__ = "University of Bristol, May 2018"

import copy
import getpass
import time
from datetime import datetime

from ROOT import TCanvas, TDatime, TGraph, TLine, TMultiGraph, TPaveText, gStyle

from PyMirrAlignOnline.fileFinder import fileFinder
from PyMirrAlignOnline.LHCbStyle import LHCbStyle
from PyMirrAlignOnline.tiltObj import tiltObj


# class Plotter(object):
class Plotter:
    def __init__(
        self,
        RICH=1,
        plotType="default",
        trendXaxis="Numbers",
        startDate=None,
        endDate=None,
        fileName="",
    ):
        self.whichRich = RICH
        self.plot_type = plotType  # 'default' or 'hollow'
        self.trend_Xaxis = trendXaxis  # "Numbers", "Fills" or "Dates"
        if fileName == "":
            self.file_name = (
                "/home/pnaik/2018trendplots/Rich"
                + str(self.whichRich)
                + "_allin1_"
                + str(self.plot_type)
                + "_"
                + str(self.trend_Xaxis)
                + "_current.pdf"
            )
            # "/home/smaddrel/Mirrors/Rich"+str(self.whichRich)+"_allin1_"+str(self.plot_type)+"_YYYYMMDD.pdf"
        else:
            self.file_name = fileName
        # date ranges and string
        if startDate is None:
            if self.whichRich == 1:
                self.minDate = 20180101  # 20170602
            if self.whichRich == 2:
                self.minDate = 20180101  # 20170605
        else:
            self.minDate = startDate
        if endDate is None:
            if self.whichRich == 1:
                self.maxDate = 99991231
            if self.whichRich == 2:
                self.maxDate = 99991231
        else:
            self.maxDate = endDate

        # Have a list of alignments
        #   If alignmentList is empty (default),
        #     alignments are automatically chosen using minDate and maxDate
        #   If alignmentList has any entries, ONLY those alignments are used
        self.alignmentList = []
        # Have a list of exceptions
        self.exceptionList = []
        if self.whichRich == 1:
            self.exceptionList += [
                "20170606_150337",
                "20170611_143511",
                "20170611_192654",
                "20170612_004007",
                "20170621_114735",
                "20170621_142043",
                "20170621_151343",
                "20170621_153621",
                "20170621_183028",
                "20170705_184436",
                "20170706_093538",
                "20170706_160847",
                "20170708_130756",
                "20170708_145416",
                # '20170714_095506',
                "20170715_161843",
                "20170716_164021",
                "20170717_173507",
                "20170717_174549",
                "20170717_175536",
                "20170717_210458",
                "20170717_212834",
                "20170720_140020",  # found on 18 December 2017, see JIRA RICH-47
                "20170720_215756",  # found on 18 December 2017, see JIRA RICH-47
                "20170808_121449",
                "20170808_150718",
                "20170808_151700",
                "20170808_152604",
                "20170808_153952",
                "20170808_155511",
                "20170810_083727",
                "20170810_085136",
                "20170810_090208",
                "20170810_091600",
                "20170810_092531",
                "20170810_110324",
                "20170810_113408",
                "20170814_144647",
                "20170814_150050",
                "20170814_152611",
                "20171019_174630",
            ]
        if self.whichRich == 2:
            self.exceptionList += [
                "20170606_102357",
                "20170610_211541",
                "20170611_170020",
                "20170611_175943",
                "20170611_190207",
                "20170611_201742",
                "20170613_083436",
                "20170621_180226",
                "20170627_105541",
                "20170705_185414",
                "20170706_100355",
                "20170706_162516",
                "20170708_140804",
                #'20170714_100603',
                "20170715_162726",
                "20170716_165032",
                "20170720_141548",  # found on 18 December 2017, see JIRA RICH-47
                "20170810_115802",
                "20170810_152825",
                "20170810_154107",
                "20170810_155418",
                "20170814_153345",
                "20170814_154437",
            ]

    def GetAlignments(self):
        findFiles = fileFinder(
            self.whichRich,
            self.minDate,
            self.maxDate,
            self.alignmentList,
            self.exceptionList,
        )
        self.files = findFiles.findFiles()
        self.alignments = findFiles.alignments

        print("INFO: Obtaining list of alignments from fileFinder.")
        print("INFO: self.alignments       =", self.alignments)

        self.tiltmaker = tiltObj(self.whichRich)

        # Determine when the DB was updated and when the polarity was flipped
        #   from the _UpdateTrend.txt file
        # Also, reject alignments that don't show up in
        #  *both* self.alignments and the _UpdateTrend.txt file
        self.GetChanges_DB_Polarity()
        """
        # OLD: Manually provide list of alignment numbers
        #        where the DB was updated, and another list of when the polarity was flipped
        self.DB_changes = []
        if self.whichRich == 1:
            self.DB_changes += [0,19,24,49,90,137,151,153,154,155,157]
        if self.whichRich == 2:
            self.DB_changes += [0,19,20,23,48,91,122,138,141,152,154]
        # List of alignments that took place just after a polarity change occured
        self.polarity_changes = []
        if self.whichRich == 1:
            self.polarity_changes += [19,49,90,137,151,153]
        if self.whichRich == 2:
            self.polarity_changes += [19,48,91,138,152,154]
        """

        if not self.alignments:
            print(
                "INFO: self.alignments is now an empty list; thus no trend plot will be produced. This is normal if there were no non-TEST alignments this year."
            )
            return False
        else:
            self.minDateString = datetime.strptime(
                self.alignments[0][0:8], "%Y%m%d"
            ).strftime("%Y/%m/%d")
            self.maxDateString = datetime.strptime(
                self.alignments[-1][0:8], "%Y%m%d"
            ).strftime("%Y/%m/%d")
            # self.minDateString = datetime.strptime(str(self.minDate), '%Y%m%d').strftime('%Y/%m/%d')
            # self.maxDateString = datetime.strptime(str(self.maxDate), '%Y%m%d').strftime('%Y/%m/%d')
            self.dateString = self.minDateString + " - " + self.maxDateString
            return True

    def GetChanges_DB_Polarity(self):
        num = 0
        import sys

        print("INFO: Importing Configuration")
        sys.stdout.flush()
        if self.whichRich == 1:
            from Configurables import Rich1MirrAlignOnConf

            # from Configuration import Rich1MirrAlignOnConf
            alignConf = Rich1MirrAlignOnConf()
        elif self.whichRich == 2:
            from Configurables import Rich2MirrAlignOnConf

            # from Configuration import Rich2MirrAlignOnConf
            alignConf = Rich2MirrAlignOnConf()
        else:
            sys.stderr.write(f"A detector named RICH{self.whichRich} does not exist.\n")
            sys.exit(1)
        alignConf.__apply_configuration__()
        # print("INFO: Configuration Applied")
        sys.stdout.flush()

        richFilesDir = alignConf.getProp("richFiles")  # '/group/rich/AlignmentFiles/'
        f = open(
            f"{richFilesDir}Logging/Rich" + str(self.whichRich) + "_UpdateTrend.txt",
            "r",
        )  # Elements: Fill_Number    Alignment_Directory_Name    Version    Polarity    Collision_Type
        # self.align_num = [] # not needed since it will be the same as the index of self.alignments, after the alignments in self.ignore are removed
        self.align_fill = []
        self.align_name = []
        self.align_version = []
        self.align_polarity = []
        # self.align_collisionType = [] # not yet used, or even stored in the text file
        self.DB_changes = []
        self.polarity_changes = []
        self.align_ignore = []
        print("INFO: Reading in alignments trend file.")
        for line in f.readlines():
            elements = line.split()
            if len(elements) == 0:
                continue  # reject blank lines
            if "#" in elements[0]:
                continue  # reject commented lines
            if elements[1] not in self.alignments:
                self.align_ignore.append(elements[1])
                # print('We removed alignment',elements[1],'because it is not in self.alignments')
                continue  # reject alignments not in self.alignments
            # self.align_num.append(num)
            self.align_fill.append(elements[0])
            self.align_name.append(elements[1])
            self.align_version.append(int(elements[2][1:]))
            if (num == 0) or (
                (num > 0) and (self.align_version[-1] != self.align_version[-2])
            ):
                (self.DB_changes).append(num)
            self.align_polarity.append(elements[3])
            if (num > 0) and (self.align_polarity[-1] != self.align_polarity[-2]):
                (self.polarity_changes).append(num)
            num += 1
        print(
            "INFO: Note that we skip alignments in Rich"
            + str(self.whichRich)
            + "_UpdateTrend.txt that do not appear in self.alignments"
        )
        # print("INFO: Skipped alignments: ", self.align_ignore)
        for i in range(0, len(self.align_ignore), 6):
            batch = self.align_ignore[i : i + 6]
            if i == 0:
                print("INFO: Skipped alignments:", batch)
            else:
                print("                           ", batch)
        # If an alignment is in self.alignments, but not in the _UpdateTrend.txt file
        #  we now reject it.
        # Note that an alignment not present in both self.alignments and _UpdateTrend.txt
        #  is thusly never counted.
        print(
            "INFO: Also, note that we removed entries from self.alignments that do not appear in Rich"
            + str(self.whichRich)
            + "_UpdateTrend.txt"
        )
        print(
            "INFO: Removed alignments:    ",
            self.list_difference(self.alignments, self.align_name),
        )
        sys.stdout.flush()
        self.alignments = self.align_name  # We assign it here so that the difference can be printed in the previous line.
        self.fills = self.align_fill
        print("INFO: self.alignments now:   ", self.alignments)
        # print('Check the alignment numbers:')
        # for i, a in enumerate(self.alignments):
        #     print(i, a)
        print("INFO: Finished reading in alignments trend file, and determined:")
        print("INFO: self.DB_changes       =", self.DB_changes)
        print("INFO: self.polarity_changes =", self.polarity_changes)
        sys.stdout.flush()

        f.close()
        pass

    def list_difference(self, a, b):
        # returns new list of items in a that are not in b
        b = set(b)
        return [x for x in a if x not in b]

    def calculate_tilts(self):
        if self.whichRich == 1:
            self.maxpri = 3
            self.maxsec = 15
        if self.whichRich == 2:
            self.maxpri = 55
            self.maxsec = 39
        lhcbStyle = LHCbStyle()
        lhcbStyle.SetTitleOffset(0.8, "Y")
        lhcbStyle.SetTitleFont(132, "Y")

        graphs_priY = {}
        graphs_priY_flip = {}
        graphs_priZ = {}
        graphs_priZ_flip = {}
        graphs_secY = {}
        graphs_secY_flip = {}
        graphs_secZ = {}
        graphs_secZ_flip = {}

        for j in range(0, self.maxpri + 1):
            graphs_priY[j] = TGraph(len(self.alignments))
            graphs_priY_flip[j] = TGraph(len(self.DB_changes))
            graphs_priZ[j] = TGraph(len(self.alignments))
            graphs_priZ_flip[j] = TGraph(len(self.DB_changes))

        for j in range(0, self.maxsec + 1):
            graphs_secY[j] = TGraph(len(self.alignments))
            graphs_secY_flip[j] = TGraph(len(self.DB_changes))
            graphs_secZ[j] = TGraph(len(self.alignments))
            graphs_secZ_flip[j] = TGraph(len(self.DB_changes))

        for i, alignment in enumerate(self.alignments):
            self.AlignmentSelector(i, self.plot_type)
            print(i, alignment, "is being compared to:", self.alignment_temp)
            if (
                ".yml" in self.files[self.alignment_temp][1]
                and ".yml" in self.files[alignment][1]
            ):  #
                self.tiltmaker.setChangeYAML(
                    [self.files[self.alignment_temp][1], self.files[alignment][1]]
                )
            elif (
                ".xml" in self.files[self.alignment_temp][1]
                and ".xml" in self.files[alignment][1]
            ):  #
                self.tiltmaker.setChange(
                    [self.files[self.alignment_temp][1], self.files[alignment][1]]
                )
            else:
                print("ERROR: Trying to compare XML to YAML or vice versa")
                # self.tilts will be what it was for the previous "alignment in enumerate(self.alignments)" setChange
                # if there was no previous setChange in this loop, then the next line will crash
            self.tilts = self.tiltmaker.getChange()
            markers_filled = [20, 21, 22, 23]
            hollow_markers = [24, 25, 26, 32]
            i_fill = int(self.fills[i])
            dt = TDatime(
                int(self.alignments[i][0 : (0 + 4)]),
                int(self.alignments[i][4 : (4 + 2)]),
                int(self.alignments[i][6 : (6 + 2)]),
                int(self.alignments[i][9 : (9 + 2)]),
                int(self.alignments[i][11 : (11 + 2)]),
                int(self.alignments[i][13 : (13 + 2)]),
            )
            i_date = dt.Convert()
            # print(self.alignments[i], self.alignment_temp)
            for j in range(0, self.maxpri + 1):
                ## print(self.alignment_temp)
                if self.trend_Xaxis == "Fills":
                    graphs_priY[j].SetPoint(i, i_fill, self.tilts[0][j])
                    graphs_priZ[j].SetPoint(i, i_fill, self.tilts[1][j])
                elif self.trend_Xaxis == "Dates":
                    graphs_priY[j].SetPoint(i, i_date, self.tilts[0][j])
                    graphs_priZ[j].SetPoint(i, i_date, self.tilts[1][j])
                else:  # self.trend_Xaxis == "Numbers"
                    graphs_priY[j].SetPoint(i, i, self.tilts[0][j])
                    graphs_priZ[j].SetPoint(i, i, self.tilts[1][j])
                if i in self.DB_changes:
                    if self.trend_Xaxis == "Fills":
                        graphs_priY_flip[j].SetPoint(
                            self.DB_changes.index(i), i_fill, self.tilts[0][j]
                        )
                        graphs_priZ_flip[j].SetPoint(
                            self.DB_changes.index(i), i_fill, self.tilts[1][j]
                        )
                    elif self.trend_Xaxis == "Dates":
                        graphs_priY_flip[j].SetPoint(
                            self.DB_changes.index(i), i_date, self.tilts[0][j]
                        )
                        graphs_priZ_flip[j].SetPoint(
                            self.DB_changes.index(i), i_date, self.tilts[1][j]
                        )
                    else:  # self.trend_Xaxis == "Numbers"
                        graphs_priY_flip[j].SetPoint(
                            self.DB_changes.index(i), i, self.tilts[0][j]
                        )
                        graphs_priZ_flip[j].SetPoint(
                            self.DB_changes.index(i), i, self.tilts[1][j]
                        )
            for j in range(0, self.maxsec + 1):
                if self.trend_Xaxis == "Fills":
                    graphs_secY[j].SetPoint(i, i_fill, self.tilts[2][j])
                    graphs_secZ[j].SetPoint(i, i_fill, self.tilts[3][j])
                elif self.trend_Xaxis == "Dates":
                    graphs_secY[j].SetPoint(i, i_date, self.tilts[2][j])
                    graphs_secZ[j].SetPoint(i, i_date, self.tilts[3][j])
                else:  # self.trend_Xaxis == "Numbers"
                    graphs_secY[j].SetPoint(i, i, self.tilts[2][j])
                    graphs_secZ[j].SetPoint(i, i, self.tilts[3][j])
                if i in self.DB_changes:
                    if self.trend_Xaxis == "Fills":
                        graphs_secY_flip[j].SetPoint(
                            self.DB_changes.index(i), i_fill, self.tilts[2][j]
                        )
                        graphs_secZ_flip[j].SetPoint(
                            self.DB_changes.index(i), i_fill, self.tilts[3][j]
                        )
                    elif self.trend_Xaxis == "Dates":
                        graphs_secY_flip[j].SetPoint(
                            self.DB_changes.index(i), i_date, self.tilts[2][j]
                        )
                        graphs_secZ_flip[j].SetPoint(
                            self.DB_changes.index(i), i_date, self.tilts[3][j]
                        )
                    else:  # self.trend_Xaxis == "Numbers"
                        graphs_secY_flip[j].SetPoint(
                            self.DB_changes.index(i), i, self.tilts[2][j]
                        )
                        graphs_secZ_flip[j].SetPoint(
                            self.DB_changes.index(i), i, self.tilts[3][j]
                        )

        n_minus_1 = len(self.alignments) - 1
        i_fill_0 = int(self.fills[0])
        dt_0 = TDatime(
            int(self.alignments[0][0 : (0 + 4)]),
            int(self.alignments[0][4 : (4 + 2)]),
            int(self.alignments[0][6 : (6 + 2)]),
            int(self.alignments[0][9 : (9 + 2)]),
            int(self.alignments[0][11 : (11 + 2)]),
            int(self.alignments[0][13 : (13 + 2)]),
        )
        i_date_0 = dt_0.Convert()
        i_fill_n_minus_1 = int(self.fills[n_minus_1])
        dt_n_minus_1 = TDatime(
            int(self.alignments[n_minus_1][0 : (0 + 4)]),
            int(self.alignments[n_minus_1][4 : (4 + 2)]),
            int(self.alignments[n_minus_1][6 : (6 + 2)]),
            int(self.alignments[n_minus_1][9 : (9 + 2)]),
            int(self.alignments[n_minus_1][11 : (11 + 2)]),
            int(self.alignments[n_minus_1][13 : (13 + 2)]),
        )
        i_date_n_minus_1 = dt_n_minus_1.Convert()
        if self.trend_Xaxis == "Fills":
            x1 = i_fill_0
            x2 = i_fill_n_minus_1
        elif self.trend_Xaxis == "Dates":
            x1 = i_date_0
            x2 = i_date_n_minus_1
        else:  # self.trend_Xaxis == "Numbers"
            x1 = 0
            x2 = n_minus_1

        multisPriY = TMultiGraph()
        multisPriZ = TMultiGraph()
        multisSecY = TMultiGraph()
        multisSecZ = TMultiGraph()
        markers_filled = [20, 21, 22, 23]
        hollow_markers = [24, 25, 26, 32]
        if self.plot_type == "default":
            markers = markers_filled
        if self.plot_type == "hollow":
            markers = hollow_markers
        if self.whichRich == 1:
            colour_valPri = 2
            colour_valSec = 54
            col_scale = 2  # so [2, 4, 6, 8]
            col_scale2 = 3  # so [54, ..., 99]
        if self.whichRich == 2:
            colour_valPri = 44
            colour_valSec = 60
            col_scale = 1  # so [44, ..., 99]
            col_scale2 = 1  # so [60, ..., 99]

        for j in range(0, self.maxpri + 1):
            graphs_priY[j].SetMarkerStyle(markers[j % 4])
            graphs_priY[j].SetMarkerSize(4)
            graphs_priY[j].SetMarkerColor(colour_valPri + j * col_scale)
            graphs_priY[j].SetLineColor(colour_valPri + j * col_scale)
            graphs_priY_flip[j].SetMarkerStyle(markers_filled[j % 4])
            graphs_priY_flip[j].SetMarkerSize(4)
            graphs_priY_flip[j].SetMarkerColor(colour_valPri + j * col_scale)
            graphs_priY_flip[j].SetLineColor(colour_valPri + j * col_scale)

            graphs_priZ[j].SetMarkerStyle(markers[j % 4])
            graphs_priZ[j].SetMarkerSize(4)
            graphs_priZ[j].SetMarkerColor(colour_valPri + j * col_scale)
            graphs_priZ[j].SetLineColor(colour_valPri + j * col_scale)
            graphs_priZ_flip[j].SetMarkerStyle(markers_filled[j % 4])
            graphs_priZ_flip[j].SetMarkerSize(4)
            graphs_priZ_flip[j].SetMarkerColor(colour_valPri + j * col_scale)
            graphs_priZ_flip[j].SetLineColor(colour_valPri + j * col_scale)

            multisPriY.Add(graphs_priY[j])
            multisPriZ.Add(graphs_priZ[j])
            multisPriY.Add(graphs_priY_flip[j])
            multisPriZ.Add(graphs_priZ_flip[j])

        for j in range(0, self.maxsec + 1):
            graphs_secY[j].SetMarkerStyle(markers[j % 4])
            graphs_secY[j].SetMarkerSize(4)
            graphs_secY[j].SetMarkerColor(colour_valSec + j * col_scale2)
            graphs_secY[j].SetLineColor(colour_valSec + j * col_scale2)
            graphs_secY_flip[j].SetMarkerStyle(markers_filled[j % 4])
            graphs_secY_flip[j].SetMarkerSize(4)
            graphs_secY_flip[j].SetMarkerColor(colour_valSec + j * col_scale2)
            graphs_secY_flip[j].SetLineColor(colour_valSec + j * col_scale2)

            graphs_secZ[j].SetMarkerStyle(markers[j % 4])
            graphs_secZ[j].SetMarkerSize(4)
            graphs_secZ[j].SetMarkerColor(colour_valSec + j * col_scale2)
            graphs_secZ[j].SetLineColor(colour_valSec + j * col_scale2)
            graphs_secZ_flip[j].SetMarkerStyle(markers_filled[j % 4])
            graphs_secZ_flip[j].SetMarkerSize(4)
            graphs_secZ_flip[j].SetMarkerColor(colour_valSec + j * col_scale2)
            graphs_secZ_flip[j].SetLineColor(colour_valSec + j * col_scale2)

            multisSecY.Add(graphs_secY[j])
            multisSecY.Add(graphs_secY_flip[j])
            multisSecZ.Add(graphs_secZ[j])
            multisSecZ.Add(graphs_secZ_flip[j])

        txt = TPaveText(0.20, 0.70, 0.45, 0.90, "NDC")
        if self.whichRich == 1:
            txt.AddText("#scale[1.25]{LHCb RICH 1}")
        else:
            txt.AddText("#scale[1.25]{LHCb RICH 2}")
        txt.SetFillStyle(1001)
        txt.SetBorderSize(0)
        txt.SetFillColor(0)
        prim = TPaveText(0.72, 0.84, 0.86, 0.92, "NDC")
        prim.AddText("Primary Mirrors")
        prim.SetFillColor(0)
        seco = TPaveText(0.72, 0.82, 0.86, 0.90, "NDC")
        seco.AddText("Secondary Mirrors")
        seco.SetFillColor(0)
        date = TPaveText(0.72, 0.22, 0.86, 0.30, "NDC")
        date.AddText(self.dateString)  # e.g. date.AddText( '2017/06/06 - 2017/11/26')
        date.SetFillColor(0)

        from ROOT import TLegend

        leg = TLegend(0.72, 0.71, 0.86, 0.84)
        leg2 = TPaveText(0.72, 0.72, 0.86, 0.82, "NDC")

        if self.whichRich == 1:
            leg.AddEntry(graphs_priY[0], "primary mirror 0", "p")
            leg.AddEntry(graphs_priY[1], "primary mirror 1", "p")
            leg.AddEntry(graphs_priY[2], "primary mirror 2", "p")
            leg.AddEntry(graphs_priY[3], "primary mirror 3", "p")
            leg2.AddText("Markers represent")
            leg2.AddText("the 16 individual mirrors")
        else:
            leg = TPaveText(0.72, 0.72, 0.86, 0.82, "NDC")
            leg.AddText("Markers represent")
            leg.AddText("the 56 individual mirrors")
            leg2.AddText("Markers represent")
            leg2.AddText("the 40 individual mirrors")
        leg.SetFillColor(0)
        leg.SetTextSize(0.04)
        leg.SetTextFont(132)
        leg2.SetFillColor(0)
        leg2.SetTextSize(0.04)  # 0.03
        leg2.SetTextFont(132)

        pri_upper = TLine(-0.5, 0.1, 14.5, 0.1)
        pri_lower = TLine(-0.5, -0.1, 14.5, -0.1)
        pri_upper.SetLineStyle(7)
        pri_lower.SetLineStyle(7)
        sec_upper = TLine(-0.5, 0.2, 14.5, 0.2)
        sec_lower = TLine(-0.5, -0.2, 14.5, -0.2)
        sec_upper.SetLineStyle(7)
        sec_lower.SetLineStyle(7)

        middle = TLine(x1, 0, x2, 0)

        # 2017 thresholds
        if self.whichRich == 1:
            pY_17 = 0.03
            pZ_17 = 0.03
            sY_17 = 0.46
            sZ_17 = 0.37
        if self.whichRich == 2:
            pY_17 = 0.03
            pZ_17 = 0.03
            sY_17 = 0.05
            sZ_17 = 0.06

        multisPriY.SetMinimum(-4 * pY_17)
        multisPriY.SetMaximum(4 * pY_17)
        multisPriZ.SetMinimum(-4 * pZ_17)
        multisPriZ.SetMaximum(4 * pZ_17)
        multisSecY.SetMinimum(-4 * sY_17)
        multisSecY.SetMaximum(4 * sY_17)
        multisSecZ.SetMinimum(-4 * sZ_17)
        multisSecZ.SetMaximum(4 * sZ_17)

        # 2017 Threshold Lines
        pY_17_line1 = TLine(x1, pY_17, x2, pY_17)
        pY_17_line2 = TLine(x1, -pY_17, x2, -pY_17)
        pY_17_line1.SetLineStyle(7)
        pY_17_line2.SetLineStyle(7)
        pZ_17_line1 = TLine(x1, pZ_17, x2, pZ_17)
        pZ_17_line2 = TLine(x1, -pZ_17, x2, -pZ_17)
        pZ_17_line1.SetLineStyle(7)
        pZ_17_line2.SetLineStyle(7)
        sY_17_line1 = TLine(x1, sY_17, x2, sY_17)
        sY_17_line2 = TLine(x1, -sY_17, x2, -sY_17)
        sY_17_line1.SetLineStyle(7)
        sY_17_line2.SetLineStyle(7)
        sZ_17_line1 = TLine(x1, sZ_17, x2, sZ_17)
        sZ_17_line2 = TLine(x1, -sZ_17, x2, -sZ_17)
        sZ_17_line1.SetLineStyle(7)
        sZ_17_line2.SetLineStyle(7)

        # 2017 Magnet polarity switches
        flips_pY_17 = [None] * len(self.polarity_changes)
        flips_pZ_17 = [None] * len(self.polarity_changes)
        flips_sY_17 = [None] * len(self.polarity_changes)
        flips_sZ_17 = [None] * len(self.polarity_changes)
        for x in range(0, len(self.polarity_changes)):
            i_x = self.polarity_changes[x]
            i_fill_x = int(self.fills[i_x])
            dt_x = TDatime(
                int(self.alignments[i_x][0 : (0 + 4)]),
                int(self.alignments[i_x][4 : (4 + 2)]),
                int(self.alignments[i_x][6 : (6 + 2)]),
                int(self.alignments[i_x][9 : (9 + 2)]),
                int(self.alignments[i_x][11 : (11 + 2)]),
                int(self.alignments[i_x][13 : (13 + 2)]),
            )
            i_date_x = dt_x.Convert()
            pre_i_x = i_x - 1
            pre_i_fill_x = int(self.fills[pre_i_x])
            pre_dt_x = TDatime(
                int(self.alignments[pre_i_x][0 : (0 + 4)]),
                int(self.alignments[pre_i_x][4 : (4 + 2)]),
                int(self.alignments[pre_i_x][6 : (6 + 2)]),
                int(self.alignments[pre_i_x][9 : (9 + 2)]),
                int(self.alignments[pre_i_x][11 : (11 + 2)]),
                int(self.alignments[pre_i_x][13 : (13 + 2)]),
            )
            pre_i_date_x = pre_dt_x.Convert()
            if self.trend_Xaxis == "Fills":
                x_flip = i_fill_x - (i_fill_x - pre_i_fill_x) / 2
            elif self.trend_Xaxis == "Dates":
                x_flip = i_date_x - (i_date_x - pre_i_date_x) / 2
            else:  # self.trend_Xaxis == "Numbers"
                x_flip = i_x - 0.5
            flips_pY_17[x] = TLine(
                x_flip, -4 * pY_17, x_flip, 4 * pY_17
            )  # large vertical limits
            flips_pY_17[x].SetLineStyle(7)
            flips_pZ_17[x] = TLine(
                x_flip, -4 * pZ_17, x_flip, 4 * pZ_17
            )  # large vertical limits
            flips_pZ_17[x].SetLineStyle(7)
            flips_sY_17[x] = TLine(
                x_flip, -4 * sY_17, x_flip, 4 * sY_17
            )  # large vertical limits
            flips_sY_17[x].SetLineStyle(7)
            flips_sZ_17[x] = TLine(
                x_flip, -4 * sZ_17, x_flip, 4 * sZ_17
            )  # large vertical limits
            flips_sZ_17[x].SetLineStyle(7)

        ## Magnet polarity switches for Primary [2016?, whichRich?]
        # flip1 = TLine(5.5, -0.3, 5.5, 0.3)
        # flip1.SetLineStyle(7)
        # flip2 = TLine(10.5, -0.3, 10.5, 0.3)
        # flip2.SetLineStyle(7)
        # flip3 = TLine(19.5, -0.3, 19.5, 0.3)
        # flip3.SetLineStyle(7)
        # flip4 = TLine(26.5, -0.3, 26.5, 0.3)
        # flip4.SetLineStyle(7)

        ## Magnet polarity switches for Secondary [2016?, whichRich?]
        # flip1s = TLine(5.5, -0.65, 5.5, 0.65)
        # flip1s.SetLineStyle(7)
        # flip2s = TLine(10.5, -0.65, 10.5, 0.65)
        # flip2s.SetLineStyle(7)
        # flip3s = TLine(19.5, -0.65, 19.5, 0.65)
        # flip3s.SetLineStyle(7)
        # flip4s = TLine(26.5, -0.65, 26.5, 0.65)
        # flip4s.SetLineStyle(7)

        cPlot = TCanvas("hasi", "masi", 7500, 1500)
        cPlot.SetLeftMargin(0.05)
        cPlot.SetRightMargin(0.05)
        cPlot.SaveAs(self.file_name + "[")

        if self.trend_Xaxis == "Fills":
            title_Xaxis = "LHCb fill number"
        elif self.trend_Xaxis == "Dates":
            title_Xaxis = "Date and Time (UTC)"
        else:  # self.trend_Xaxis == "Numbers"
            title_Xaxis = "Alignment number [a.u.]"

        multisPriY.Draw("AP")
        multisPriY.GetXaxis().SetTitle(title_Xaxis)
        if self.trend_Xaxis == "Dates":
            multisPriY.GetXaxis().SetTimeDisplay(1)
            multisPriY.GetXaxis().SetNdivisions(-503)
            multisPriY.GetXaxis().SetTimeFormat("%Y-%m-%d %H:%M")
            multisPriY.GetXaxis().SetTimeOffset(0, "gmt")
        multisPriY.GetYaxis().SetTitle("Local Y rotation [mrad]")
        multisPriY.GetYaxis().SetTitleOffset(0.30)
        for x in range(0, len(flips_pY_17)):
            flips_pY_17[x].Draw("same")
        pY_17_line1.Draw("same")
        pY_17_line2.Draw("same")
        txt.InsertText("#scale[0.75]{Preliminary}")
        txt.Draw("same")
        # prim.InsertText('Primary Mirror')
        prim.Draw("same")
        date.Draw("same")
        leg.Draw("same")
        middle.Draw("same")
        # pri_upper.Draw('same')
        # pri_lower.Draw('same')
        # flip1.Draw('same')
        # flip2.Draw('same')
        # flip3.Draw('same')
        # flip4.Draw('same')
        cPlot.SaveAs(self.file_name)

        multisPriZ.Draw("AP")
        multisPriZ.GetXaxis().SetTitle(title_Xaxis)
        if self.trend_Xaxis == "Dates":
            multisPriZ.GetXaxis().SetTimeDisplay(1)
            multisPriZ.GetXaxis().SetNdivisions(-503)
            multisPriZ.GetXaxis().SetTimeFormat("%Y-%m-%d %H:%M")
            multisPriZ.GetXaxis().SetTimeOffset(0, "gmt")
        multisPriZ.GetYaxis().SetTitle("Local Z rotation [mrad]")
        multisPriZ.GetYaxis().SetTitleOffset(0.30)
        for x in range(0, len(flips_pY_17)):
            flips_pZ_17[x].Draw("same")
        pZ_17_line1.Draw("same")
        pZ_17_line2.Draw("same")
        txt.Draw("same")
        prim.Draw("same")
        date.Draw("same")
        leg.Draw("same")
        # pri_upper.Draw('same')
        # pri_lower.Draw('same')
        # flip1.Draw('same')
        # flip2.Draw('same')
        # flip3.Draw('same')
        # flip4.Draw('same')
        middle.Draw("same")
        cPlot.SaveAs(self.file_name)

        multisSecY.Draw("AP")
        multisSecY.GetXaxis().SetTitle(title_Xaxis)
        if self.trend_Xaxis == "Dates":
            multisSecY.GetXaxis().SetTimeDisplay(1)
            multisSecY.GetXaxis().SetNdivisions(-503)
            multisSecY.GetXaxis().SetTimeFormat("%Y-%m-%d %H:%M")
            multisSecY.GetXaxis().SetTimeOffset(0, "gmt")
        multisSecY.GetYaxis().SetTitle("Local Y rotation [mrad]")
        multisSecY.GetYaxis().SetTitleOffset(0.30)
        for x in range(0, len(flips_sY_17)):
            flips_sY_17[x].Draw("same")
        sY_17_line1.Draw("same")
        sY_17_line2.Draw("same")
        txt.Draw("same")
        leg2.Draw("same")
        # leg2b.Draw('same')
        date.Draw("same")
        middle.Draw("same")
        # sec_upper.Draw('same')
        # sec_lower.Draw('same')
        # flip1s.Draw('same')
        # flip2s.Draw('same')
        # flip3s.Draw('same')
        # flip4s.Draw('same')
        seco.Draw("same")
        cPlot.SaveAs(self.file_name)

        multisSecZ.Draw("AP")
        multisSecZ.GetXaxis().SetTitle(title_Xaxis)
        if self.trend_Xaxis == "Dates":
            multisSecZ.GetXaxis().SetTimeDisplay(1)
            multisSecZ.GetXaxis().SetNdivisions(-503)
            multisSecZ.GetXaxis().SetTimeFormat("%Y-%m-%d %H:%M")
            multisSecZ.GetXaxis().SetTimeOffset(0, "gmt")
        multisSecZ.GetYaxis().SetTitle("Local Z rotation [mrad]")
        multisSecZ.GetYaxis().SetTitleOffset(0.30)
        for x in range(0, len(flips_sZ_17)):
            flips_sZ_17[x].Draw("same")
        sZ_17_line1.Draw("same")
        sZ_17_line2.Draw("same")
        txt.Draw("same")
        leg2.Draw("same")
        # leg2b.Draw('same')
        date.Draw("same")
        middle.Draw("same")
        # sec_upper.Draw('same')
        # sec_lower.Draw('same')
        # flip1s.Draw('same')
        # flip2s.Draw('same')
        # flip3s.Draw('same')
        # flip4s.Draw('same')
        seco.Draw("same")
        cPlot.SaveAs(self.file_name)

        cPlot.SaveAs(self.file_name + "]")

        pass

    def AlignmentSelector(self, i, version="default"):
        self.DB_rangepoints = copy.deepcopy(self.DB_changes)
        self.DB_rangepoints += [999999]  # just some ridiculously large number
        offset = 0
        if version == "hollow":
            offset += 1
        if i == 0:
            self.alignment_temp = self.alignments[self.DB_changes[0]]
        else:
            for x in range(1, len(self.DB_changes)):
                if i >= (self.DB_rangepoints[x] + offset) and (
                    i < self.DB_rangepoints[x + 1] + offset
                ):
                    self.alignment_temp = self.alignments[self.DB_changes[x]]
        pass

    # Not sure what this is supposed to be for yet...
    def limit_check(self):
        pass


def main():
    # Later have both working in the same script with cmd line args
    # for now run both in sequence

    print("INFO: Now making default plot for RICH1 with Numbers on x-axis")
    P1 = Plotter(1, "default", "Numbers", None, None, "")
    P1.GetAlignments()
    P1.calculate_tilts()
    print("INFO: Now making default plot for RICH2 with Numbers on x-axis")
    P2 = Plotter(2, "default", "Numbers", None, None, "")
    P2.GetAlignments()
    P2.calculate_tilts()
    print("INFO: Now making hollow plot for RICH1 with Numbers on x-axis")
    P1_hollow = Plotter(1, "hollow", "Numbers", None, None, "")
    P1_hollow.GetAlignments()
    P1_hollow.calculate_tilts()
    print("INFO: Now making hollow plot for RICH2 with Numbers on x-axis")
    P2_hollow = Plotter(2, "hollow", "Numbers", None, None, "")
    P2_hollow.GetAlignments()
    P2_hollow.calculate_tilts()

    print("INFO: Now making default plot for RICH1 with Fills on x-axis")
    P1 = Plotter(1, "default", "Fills", None, None, "")
    P1.GetAlignments()
    P1.calculate_tilts()
    print("INFO: Now making default plot for RICH2 with Fills on x-axis")
    P2 = Plotter(2, "default", "Fills", None, None, "")
    P2.GetAlignments()
    P2.calculate_tilts()
    print("INFO: Now making hollow plot for RICH1 with Fills on x-axis")
    P1_hollow = Plotter(1, "hollow", "Fills", None, None, "")
    P1_hollow.GetAlignments()
    P1_hollow.calculate_tilts()
    print("INFO: Now making hollow plot for RICH2 with Fills on x-axis")
    P2_hollow = Plotter(2, "hollow", "Fills", None, None, "")
    P2_hollow.GetAlignments()
    P2_hollow.calculate_tilts()

    print("INFO: Now making default plot for RICH1 with Dates on x-axis")
    P1 = Plotter(1, "default", "Dates", None, None, "")
    P1.GetAlignments()
    P1.calculate_tilts()
    print("INFO: Now making default plot for RICH2 with Dates on x-axis")
    P2 = Plotter(2, "default", "Dates", None, None, "")
    P2.GetAlignments()
    P2.calculate_tilts()
    print("INFO: Now making hollow plot for RICH1 with Dates on x-axis")
    P1_hollow = Plotter(1, "hollow", "Dates", None, None, "")
    P1_hollow.GetAlignments()
    P1_hollow.calculate_tilts()
    print("INFO: Now making hollow plot for RICH2 with Dates on x-axis")
    P2_hollow = Plotter(2, "hollow", "Dates", None, None, "")
    P2_hollow.GetAlignments()
    P2_hollow.calculate_tilts()

    pass


if __name__ == "__main__":
    main()

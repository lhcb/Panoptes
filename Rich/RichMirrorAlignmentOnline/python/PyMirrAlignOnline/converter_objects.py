###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Vidar Marsh, Jake Reich"
__copyright__ = "University of Bristol, May 2022"

import os


def load_to_list(file):
    """
    Loads every lines of a .txt file to a list. Any whitespace at the beginning
    of a line or at the end of each line is removed. Any blank lines are also
    removed.

    Args:
        file: raw string, path to the .txt file

    Returns:
        temp, a list of stripped lines from the .txt file
    """
    with open(file, "r") as f:
        lines = f.readlines()
    for i in range(len(lines)):
        lines[i] = str(lines[i]).strip()
        if lines[i] == "":
            del lines[i]
    return lines


def tack_on_off(out_file_path, tack_on, config_path, whichRICH):
    """
    Adds extra lines to the start and end of the converted file.
    These lines are manually added in yaml_tack_on_Rich_X.txt or xml_tack_on_Rich_X.txt (where X = 1 or 2),
    stored in the Configuration folder (defined in 'config_path').

    Everything below '---top' (and above '---bottom') will be prepended to the top of the file.
    Everything below '---bottom' will be appended to the bottom of the file.

    Args:
        out_file_path: raw string, path to the output file we want to write to
    """

    top_text = ""
    bottom_text = ""

    # get correct tack_on file
    file_type = out_file_path[-4:]
    if tack_on == True:
        if file_type == ".xml":
            file_type = "xml"
        elif file_type == ".yml":
            file_type = "yaml"
    else:
        if file_type == ".xml":
            file_type = "yaml"
        elif file_type == ".yml":
            file_type = "xml"

    parse_path = os.path.sep.join(
        (config_path, file_type + "_tack_on_Rich_" + str(whichRICH) + ".txt")
    )
    parse_path = r"{}".format(parse_path)

    if tack_on == True:
        print("adding lines from file: " + parse_path)
    else:
        print("removing lines from file: " + parse_path)

    # read 'tack on' file
    with open(parse_path, "r") as f:
        for line in f:
            l = line.strip()
            if l == "---top":
                temp_where_var = "top"
                continue
            if l == "---bottom":
                temp_where_var = "bottom"
                continue
            if temp_where_var == "top":
                top_text += line
            elif temp_where_var == "bottom":
                bottom_text += line

    # write to output file
    with open(out_file_path, "r+") as f:
        content = f.read()

        if tack_on == False:
            content = content.replace(top_text, "")
            content = content.replace(bottom_text, "")

            f.seek(0)
            f.write(content)
        else:
            f.seek(0)
            f.write(top_text + "\n" + content + "\n" + bottom_text)

    print("done...")


class Conversions:
    """
    Conversions class wrapping all conversion dictionaries and conv_SI
    """

    def __init__(self):
        """
        self.name_xml: dict, parameter name conversions XML -> YAML. The name
        in XML is the key and the "value" is the name in YAML.

        self.name_yml: dict, parameter name conversions YAML -> XML. The name
        in YAML is the key and the "value" is the name in XML.

        self.unit_xml: dict, parameter units in XML. The parameter name in
        XML is the key and the parameter unit in XML is the corresponding
        value

        self.unit_yaml: dict, parameter units in YAML. The parameter name in
        YAML is the key and the unit in YAML is the corresponding value.

        self.unit_conv: dict, unit conversion table. The unit is the key and the
        corresponding value is a tuple containing the unit in SI base units (e.g.
        "kg", "m", "s") and what to multiply the SI base units by to get the
        unit.
        """
        self.name_xml = {}
        self.name_yml = {}
        self.unit_xml = {}
        self.unit_yml = {}
        self.unit_conv = {}

    def get_name_xml(self, name_yml):
        """
        Checks that parameter name is defined for YAML files and then returns the
        parameter name defined for XML files.

        Args:
            name_yml: str, parameter name for YAML files

        Returns:
            string, parameter name for XML files

        Errors:
            If the parameter name defined for XML files does not exist then a
            KeyError will be returned
        """
        errtxt = '"{}" does not exist in YAML database'.format(name_yml)
        try:
            name_xml = self.name_yml[name_yml]
            return name_xml
        except:
            raise KeyError(errtxt)

    def get_name_yml(self, name_xml):
        """
        Checks that parameter name is defined for XML files and then returns the
        parameter name defined for YAML files.

        Args:
            name_xml: str, parameter name for XML files

        Returns:
            str, parameter name for YAML files

        Errors:
            If the parameter name defined for YAML files does not exist then a
            KeyError will be returned
        """
        errtxt = '"{}" does not exist in XML database'.format(name_xml)
        try:
            name_yml = self.name_xml[name_xml]
            return name_yml
        except:
            raise KeyError(errtxt)

    def get_unit_xml(self, name_xml):
        """
        Checks name_xml against unit database and returns the associated XML unit
        if it exist.

        Args:
            name_xml: str, parameter name corresponding to the desired unit in
            XML

        Returns:
            str, unit in XML associated with name_xml
        """
        errtxt = 'no unit associated with "{}" in XML database'.format(name_xml)
        try:
            return self.unit_xml[name_xml]
        except:
            raise KeyError(errtxt)

    def get_unit_yml(self, name_yml):
        """
        Checks name_xml against unit database and returns the associated YAML unit
        if it exist.

        Args:
            name_yml: str, parameter name corresponding to the desired unit in
            YAML

        Returns:
            str, unit in YAML associated with name_yml
        """
        errtxt = 'no unit associated with "{}" in YAMLdatabase'.format(name_yml)
        try:
            return self.unit_yml[name_yml]
        except:
            raise KeyError(errtxt)

    def convert_unit(self, value, u_in, u_out):
        """
        Converting value between units

        Args:
            value: float, number to be converted
            u_in: str, input unit for "value"
            u_out: str, desired unit of "value"

        Returns:
            "Value" in units of u_out
        """
        # Error messages
        err_void = 'the unit "{}" does not exist in database'
        err_incomp = 'the units "{}" and "{}" are incompatible'
        # Check units exist in unit_conv list
        if u_in not in self.unit_conv:
            raise ValueError(err_void.format(u_in))
        elif u_out not in self.unit_conv:
            raise ValueError(err_void.format(u_out))
        else:
            if self.unit_conv[u_in][0] == self.unit_conv[u_out][0]:
                # convert units of "value"
                return value * self.unit_conv[u_in][1] / self.unit_conv[u_out][1]
            else:
                raise ValueError(err_incomp.format(u_in, u_out))


class BlockList:
    def __init__(self, blocklist):
        """
        Create a list of parameter names that are ignored when converting.

        Args:
            blocklist: list, elements of the blocklist
                if element begins with a "-" then checks endings
                if element ends with a "-" then checks beginnings
                if element begins with a "*" then check everywhere
        Returns:
            Blocklist object.
        """
        self.blocklist = []
        self.blocklist_prefix = []
        self.blocklist_suffix = []
        self.blocklist_wc = []

        for item in blocklist:
            item = str(item)
            if item[0] == "-":
                self.blocklist_suffix.append(item[1:])
            elif item[-1] == "-":
                self.blocklist_prefix.append(item[:-1])
            elif item[0] == "*":
                self.blocklist_wc.append(item[1:])
            else:
                self.blocklist.append(item)

    def check(self, item):
        """
        Checks a string against the blocklist.

        Args:
            item: string, item to check against the blocklist

        Returns:
            Boolean: True if item is on the list, False if item is not on the
            list.
        """
        # item = str(item)
        in_list = False

        if item in self.blocklist:
            in_list = True

        for prefix in self.blocklist_prefix:
            if item.startswith(prefix) == True:
                in_list = True
        for suffix in self.blocklist_suffix:
            if item.endswith(suffix) == True:
                in_list = True
        for blocklist_wc in self.blocklist_wc:
            if blocklist_wc in item:
                in_list = True

        return in_list


def custom_constructor(loader, node):
    """
    Create a custom constructor for a single custom tag
    """
    value = loader.construct_mapping(node)
    return value


def find_sigfigs(number):
    """
    Returns the number of sig figs in number
    """
    # Turn number into a float and then a string and split off exponential part
    # Split whole and decimal parts and string respective 0s, then return len
    #
    number = str(float(number)).lower()
    if "e" in number:
        number, exponent = number.split("e")
    tokens = number.split(".")
    whole_num = tokens[0].lstrip("0")
    if len(tokens) > 2:
        raise ValueError("Invalid number {} only 1 decimal allowed".format(number))
    elif len(tokens) == 2:
        decimal_num = tokens[1].rstrip("0")
        return len(whole_num) + len(decimal_num)
    return len(whole_num)

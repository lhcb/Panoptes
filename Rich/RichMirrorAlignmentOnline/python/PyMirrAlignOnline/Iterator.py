###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Claire Prouve, Paras Naik"
__copyright__ = "University of Bristol, July 2023"

# python -c "from PyMirrAlignOnline import Iterator; Iterator.run(1,True,True)"

# if flat is False (same as 'if offline is False')
#     This is the Iterator (for testbench / Run Control) Online
#     Please do not delete, but instead only comment (preferably with ''' blocks),
#        lines of code from "if flat is False" sections,
#        so that we can make sure eventually all Online capabilities are restored.
# if flat is True and runInlineReco is True
#     This is the (flattened iterator + inline reconstruction + automated execution) on one single node
#     In this case, the reconstruction will run on the node where you run the iterator.
#     You will not need to input commands. It will iterate through simulated commands until it converges.
#     Please propagate all generally useful changes to the main iterator part (flat = False)
# if flat is True and runInlineReco is False
#     This is the pure flattened iterator. The reconstruction step must be performed elsewhere.
#     It only has one job: run RichMirrCombinFit and run RichMirrAlign (one iteration only)
#     You have to enter the commands manually (usual order: configure, start, pause, reset)
#     NB: For each single iteration, you must manually put the root file for the current iteration in the right place!
#     Please propagate all generally useful changes to the main iterator part (flat = False)
import distutils
import getpass
import glob
import math
import os
import re
import shutil
import subprocess
import sys
import threading
import time
from distutils import dir_util
from time import gmtime, sleep, strftime, time

# LHCb-specific imports
import Configurables
import ROOT
from MooreOnlineConf.utils import (
    alignment_options,
)
from ruamel.yaml import YAML

from PyMirrAlignOnline.Communicator import *  # gets the State and Communicator classes

print(f"INFO: Iterator.py launched at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC")
sys.stdout.flush()


########################################################################################################################
##### Here is where the party starts!
########################################################################################################################
def run(whichRich, offline=False, runInlineReco=False):
    flat = offline

    def get_file_name(file_path):
        file_name = os.path.basename(file_path)
        return file_name

    def copy_with_timestamp(src_file, dst_file):
        shutil.copy(src_file, dst_file)
        # Update the time stamp of the copied file
        src_stat = os.stat(src_file)
        os.utime(dst_file, (src_stat.st_atime, src_stat.st_mtime))

    def copy(fromFile, toFile):
        print(f"INFO: Copying {fromFile} to {toFile}")
        if flat is False:
            copy_with_timestamp(fromFile, toFile)
        else:
            shutil.copyfile(fromFile, toFile)

    launchTime = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    start_time_config = time()  #  when LHCbA config start time is really set
    # print(f"INFO: The launch directory is: {os.getcwd()}")
    # print(f'INFO: os.getenv("WORKING_DIR") = {os.getenv("WORKING_DIR")}')
    launchDir = os.path.join(os.getcwd(), "")

    ########################################################################################################################
    #####  reading in the settings from the Configuration file
    ########################################################################################################################
    print("INFO: Importing Configuration")
    sys.stdout.flush()
    if whichRich == 1:
        from Configurables import Rich1MirrAlignOnConf

        # from Configuration import Rich1MirrAlignOnConf
        alignConf = Rich1MirrAlignOnConf()
    elif whichRich == 2:
        from Configurables import Rich2MirrAlignOnConf

        # from Configuration import Rich2MirrAlignOnConf
        alignConf = Rich2MirrAlignOnConf()
    else:
        sys.stderr.write(f"A detector named RICH{whichRich} does not exist.\n")
        sys.exit(1)
    alignConf.__apply_configuration__()
    sys.stdout.flush()

    ### grab the stack directory as defined in Configuration.py
    stackDir = alignConf.getProp(
        "stackDir"
    )  # '/group/rich/sw/alignment/stack/' is default

    ### Set up strings needed for logging of hlt02 (the node that the iterator is running on)
    richFilesDir = alignConf.getProp(
        "richFiles"
    )  # '/group/rich/AlignmentFiles/' is default

    loggingDir = f"{richFilesDir}Logging/"
    polarityDir = f"{richFilesDir}Polarity/"
    lastAlignSuccess = f"{loggingDir}Rich{whichRich}_success.txt"
    currentHLT02log = f"{loggingDir}Rich{whichRich}_hlt02.log"
    currentHLT02logReduced = f"{loggingDir}Rich{whichRich}_hlt02reduced.log"
    previousHLT02log = f"{loggingDir}Rich{whichRich}_hlt02_previous.log"
    previousHLT02logReduced = f"{loggingDir}Rich{whichRich}_hlt02reduced_previous.log"
    # cmdStart = 'exec /opt/FMC/bin/logViewer -l 2 -S -b &>'
    # cmdStart = '/group/online/dataflow/scripts/log_viewer.sh -m hlt02 -b /ROLogBridge_HLT2 &>'
    cmdStart = "stdbuf -oL -eL /group/online/dataflow/scripts/log_viewer.sh -m hlt02 &>"

    # list used to help remove lines from the hlt02 log in the hlt02reduced versions,
    removeExtraneous_patterns = [
        "LHCb2_HLT02_Hlt2Adder_0",
        "Hlt2SaverSvc",
        "MARK",
        "BusyPub",
        "BusyMon",
        "JobOptionsSvc",
        "environtment",
    ]  # environtment not a typo
    # PN: Not sure ANY of these patterns appear anymore in Run3
    #     However, we keep this feature in case there is pollution of our log files

    # There are a bunch of permissions issues that come up; use try/except to avoid

    ## Backup the last HLT02 log
    if os.path.exists(currentHLT02log):
        try:
            copy(currentHLT02log, previousHLT02log)
        except:
            print("WARNING: failed copy(currentHLT02log, previousHLT02log)")
    if os.path.exists(previousHLT02log):
        # os.system(f'grep -vE "{removeExtraneous}" {previousHLT02log} > {previousHLT02logReduced}')
        try:
            with (
                open(previousHLT02log, "rb") as input_file,
                open(previousHLT02logReduced, "wb") as output_file,
            ):
                for line in input_file:
                    if not any(
                        pattern in line.decode()
                        for pattern in removeExtraneous_patterns
                    ):
                        output_file.write(line)
        except:
            print("WARNING: failed to produce previousHLT02logReduced")

    ### Check to see if the last mirror alignment was successful
    if os.path.exists(lastAlignSuccess):
        with open(lastAlignSuccess) as lastAlignSuccessFile:
            lastAlignSuccessLine = lastAlignSuccessFile.readline()
    else:
        lastAlignSuccessLine = "UNKNOWN\n"

    ### Now that we have the lastAlignSuccessLine, Reset lastAlignSuccessFile to default
    try:
        lastAlignSuccessFile = open(lastAlignSuccess, "w")
        lastAlignSuccessFile.write("NO\n")  # python will convert \n to os.linesep
        lastAlignSuccessFile.close()
    except:
        print("WARNING: failed open(lastAlignSuccess, 'w')")

    ## Start logging of hlt02 (the node that the iterator is running on)
    if lastAlignSuccessLine == "YES\n":
        logProcess = subprocess.Popen(
            f"{cmdStart} {currentHLT02log}",
            shell=True,
            executable="/bin/bash",
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
    else:
        logProcess = subprocess.Popen(
            f"{cmdStart}> {currentHLT02log}",
            shell=True,
            executable="/bin/bash",
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )

    logProcess.stdout.close()
    logProcess.stderr.close()
    sys.stdout.flush()
    print("INFO: Logger Initializing (8 second pause).")
    sleep(8)  # wait long enough for logger to initialize

    ### Printout (slightly late, so it gets logged)
    print(
        f"INFO: run(whichRich = {whichRich}, flat = {flat}, runInlineReco = {runInlineReco}) command received at {launchTime} UTC"
    )
    print("INFO: Configuration Applied")
    print("INFO: LHCbA config start time set.")
    print(f"INFO: The launch directory is: {os.getcwd()}")
    if lastAlignSuccessLine == "YES\n":
        print(
            f"INFO: The previous Rich{whichRich} alignment was able to copy hlt02.log, overwriting hlt02.log with this alignment's printouts."
        )
    else:
        print(
            f"INFO: The previous Rich{whichRich} alignment was not able to copy hlt02.log, appending this alignment's printouts to hlt02.log."
        )
    print(f"INFO: logging hlt02 in temporary file: {currentHLT02log}")
    print(f"INFO: backing up last hlt02 log in temporary file: {previousHLT02log}")
    print(
        f"INFO: storing reduced backup of last hlt02 log in temporary file: {previousHLT02logReduced}"
    )

    ### make a backup of OnlineEnvBase.py (do it now, in case we need to study the configuration on a single node)
    origOnlineEnvBase = "/group/online/dataflow/options/LHCbA/OnlineEnvBase.py"
    saveOnlineEnvBase = f"{loggingDir}Rich{whichRich}_OnlineEnvBase.py"
    try:
        copy(
            origOnlineEnvBase,  # changed from "LHCbA/HLT/OnlineEnvBase.py" where it was in Run 2
            saveOnlineEnvBase,
        )  #
        print(f"INFO: stored OnlineEnvBase.py in temporary file: {saveOnlineEnvBase}")
    except:
        print("WARNING: failed copy(origOnlineEnvBase, saveOnlineEnvBase)")
    sys.stdout.flush()

    ### make a backup of Configuration.py
    origConfiguration = f"{stackDir}Panoptes/Rich/RichMirrorAlignmentOnline/python/RichMirrorAlignmentOnline/Configuration.py"
    saveConfiguration = f"{loggingDir}Rich{whichRich}_Configuration.py"
    try:
        copy(origConfiguration, saveConfiguration)
        print(f"INFO: stored Configuration.py in temporary file: {saveConfiguration}")
    except:
        print(origConfiguration)
        print(saveConfiguration)
        print(f"WARNING: failed copy(origConfiguration, saveConfiguration)")
    sys.stdout.flush()

    from PyMirrAlignOnline.XMLFileHelper import XMLFileHelper

    xmlHelper = None
    from PyMirrAlignOnline.YAMLFileHelper import YAMLFileHelper

    ymlHelper = None
    from PyMirrAlignOnline.convert_XML_YAML import Conversion

    conversionsHelper = None
    from PyMirrAlignOnline.RichAlignmentHelper import RichAlignmentHelper

    alignHelper = None
    from PyMirrAlignOnline.SetupHelper import SetupHelper

    setupHelper = None
    from PyMirrAlignOnline.UpdateHelper import UpdateHelper

    updateHelper = None
    from PyMirrAlignOnline.HistoHelper import HistoCopier

    histCopier = None
    from PyMirrAlignOnline.convert_magnFactors_txt_YAML import (
        convert_magnFactors_txt_YAML,
    )

    maxIters = int(
        alignConf.getProp("maxIterations")
    )  # This is the maximum "_i" iteration number before the alignment gives up
    workdir = alignConf.getProp("WorkDir")
    tiltNames = alignConf.getProp("tiltNames")
    tiltNamesLength = len(tiltNames)
    thisNameStr = alignConf.getProp("nameStr")
    magnFactorsMode = alignConf.getProp("magnFactorsMode")
    autoUpdate_initial = bool(alignConf.getProp("autoUpdate"))
    print(f"INFO: Initial setting of autoUpdate = {autoUpdate_initial}")
    autoUpdateNoV = False  # this can never be True if autoUpdate_initial is False
    autoUpdate = False
    if autoUpdate_initial:
        autoUpdate = True
        autoUpdateNoV = bool(alignConf.getProp("autoUpdateNoV"))
        print(f"INFO: Initial setting of autoUpdateNoV = {autoUpdateNoV}")
    testing = bool(alignConf.getProp("testing"))
    print(f"INFO: Initial setting of testing = {testing}")
    print(
        f"INFO: Initial setting of useReducedSubset = {alignConf.getProp('useReducedSubset')}"
    )
    YAMLio = bool(alignConf.getProp("YAMLio"))
    print(f"INFO: Initial setting of YAMLio = {YAMLio}")
    if YAMLio:
        print(
            "INFO: Expecting the (v-numbered) starting file to be YAML. Convert to XML for RichAnalyzer if useDD4hep = False. Output YAML."
        )
    else:
        print(
            "INFO: Expecting the (v-numbered) starting file to be XML. Convert to YAML for MirrCombinFit & MirrAlign. Output YAML."
        )
    useDD4hep = bool(alignConf.getProp("useDD4hep"))
    if useDD4hep:
        YAMLio = True  # if using DD4hep we are expecting YAML
        print(
            "INFO: Reconstructing with DD4hep. Forced YAMLio = True. Expect RICH calibrations to be in calib.yml (or Run3 equivalent)."  # PN: Update this message when this check of calib.yml or Run3 equivalent is re-established
        )
    else:
        print(
            "INFO: Reconstructing with DetDesc. Expect RICH calibrations to be in calib.xml"
        )

    ### this is the place where the analyzers will pick up the yml (xml) file for their DD4hep (DetDesc) Moore jobs
    ### before each new minor iteration the new yml (xml) file will be copied here
    currentDBpre = f"{workdir}CondDB_Rich{whichRich}"
    if useDD4hep:
        currentDB = f"{currentDBpre}.yml"
    else:
        currentDB = f"{currentDBpre}.xml"

    ### this is the place where the final output yml (xml) will be expected to be placed at the end of the alignment
    if YAMLio:
        lastDB = f"{currentDBpre}.yml"
    else:
        lastDB = f"{currentDBpre}.xml"

    ### path in the work directory of the database-file that the final database will be compared to in the AlignMonitor
    compareDB = f"{workdir}Rich{whichRich}"
    if YAMLio:
        compareDB = f"{compareDB}_compareYAMLFile.yml"
    else:
        compareDB = f"{compareDB}_compareXMLFile.xml"

    print(f"INFO: getpass.getuser(): {getpass.getuser()}")

    # We do this before a "configure" because we need run and directoryTime info for the next part
    setupHelper = SetupHelper(alignConf, flat)

    # Do not run the mirror alignment if:
    #   Any run's polarity is OFF
    #   The polarities for the runs chosen are mixed
    #   Any of the runs' RICH calibrations are missing
    runs = []  # can remove this when everything works for Run 3
    if flat is False:  # Get List of Runs from Online
        if "online" in getpass.getuser():
            import OnlineEnvBase as Online  # import OnlineEnv as Online

            online_options = alignment_options(Online)
            runs = online_options.runs  # runs = Online.DeferredRuns
            # PN - Extract the run number substring from the full string
            #    - WILL NOT WORK IF RUN NUMBERS BECOME 7 DIGITS
            runs = [entry[-6:] for entry in runs]
        pass
    polarities = []
    fills = []
    for run in runs:
        polarities.append(setupHelper.getPolarity(run))
        fills.append(setupHelper.getFillNumber(run))
    # print(f"INFO: runs = {runs}")
    for i in range(0, len(runs), 14):
        batch = runs[i : i + 14]
        if i == 0:
            print("INFO: runs:", batch)
        else:
            print("             ", batch)
    # print(f"INFO: fills = {fills}")
    for i in range(0, len(fills), 14):
        batch = fills[i : i + 14]
        if i == 0:
            print("INFO: fills:", batch)
        else:
            print("              ", batch)
    polsStr = [
        str(item) for item in polarities
    ]  # str() gets rid of unicode flag preceding each string
    # print(f"INFO: polarities = {polsStr}")
    for i in range(0, len(polarities), 14):
        batch = polarities[i : i + 14]
        if i == 0:
            print("INFO: polarities:", batch)
        else:
            print("                   ", batch)
    cancel = False
    preSubject = ""
    preText = ""
    postSubject = ""
    postText = ""
    polarityMiss = []
    calibMissing = []
    polarityDiff = []
    if (
        flat is False
    ):  # Check RICH calibrations exist # PN: We have no idea how to do this yet for Run 3
        for i in range(0, len(runs)):
            ## PN: We have no idea what the calib locations are. In Run 2 we needed calibrations before we could place alignments. For now comment this part out.
            """
            calibLoc = f"/group/online/hlt/conditions/LHCb/NoYear/{int(math.floor(int(runs[i]) / 1000.0))}/{runs[i]}/rich{whichRich}"
            if useDD4hep:
                calibLoc = f"{calibLoc}calib.yml"  # PN - eventually find out where this is for Run 3
            else:
                calibLoc = f"{calibLoc}calib.xml"
            if not os.path.exists(calibLoc):
                print(f"WARNING: Missing RICH calibration detected: {calibLoc}")
                calibMissing.append(runs[i])
            """
            ## PN: For now, do not check that all data has the same known polarity. We will want to re-enable this during fully automated running.
            """
            if (polarities[i] == 'OFF') or (polarities[i] != polarities[0]):
                polarityDiff.append(runs[i])
            if (polarities[i] is None):
                polarityMiss.append(runs[i])
            """
            pass
    sys.stdout.flush()

    ########################################################################################################################
    ##### define the communicator who will communicate with the run-control
    ########################################################################################################################

    state = State.NOT_READY
    if flat is False:  # Get the Communicator
        com = Communicator("ALIGNITER")
    print("INFO: Communicator started.")
    if flat is False:  # Set the state
        com.set_status(state)
    print(f"INFO: Iterator state = {state}")

    conv = False  ### variable indicates if alignment converged yet

    # If running with runInlineReco = True,
    # we want the iterator to perform fully automatically
    # so we need these flags to track where we are in the sequence
    configureDone = False
    startDone = False
    iterationsDone = False

    print(f"INFO: First expected command is configure (CONFIGURE)")

    while True:
        sys.stdout.flush()  # Always keep this line in
        print("INFO: Waiting for command from Communicator")
        sys.stdout.flush()
        if not cancel:
            if flat is False:  # Get Command
                command = com.get_command()
                if (
                    "state" in command
                ):  # ignore strange "!state" command from Run Control
                    command = com.get_command()
                print(f"INFO: Iterator Receiving command: {command}")
                print("INFO: ----------------------------")
            acceptable_commands = [
                "configure",
                "start",
                "pause",
                "reset",
                "stop",
                "c",
                "s",
                "p",
                "r",
                "t",
            ]
            if flat is False:
                pass
            else:
                command = None
                while command is None:
                    if runInlineReco is False:
                        command = input(
                            "You are the Communicator. Enter command (configure [or c], start [or s], pause [or p], reset [or r], stop [or t]): "
                        )
                        print(
                            f"INFO: You have provided the following command: {command}"
                        )
                    else:
                        # runInlineReco is True, so automate
                        if not configureDone and not startDone:
                            command = "configure"
                        elif configureDone and not startDone:
                            command = "start"
                        elif configureDone and startDone and not iterationsDone:
                            command = "pause"
                        else:
                            command = "reset"
                        print(
                            f"INFO: runInlineReco is True. The automation has provided the following command: {command}"
                        )
                    if command not in acceptable_commands:
                        print("INFO: Please try again.")
                        command = None
                if command == "c":
                    command = "configure"
                if command == "s":
                    command = "start"
                if command == "p":
                    command = "pause"
                if command == "r":
                    command = "reset"
                if command == "t":
                    command = "stop"
        else:
            command = "reset"
        ########################################################################################################################
        ##### this code will be executed when you click configure on the run-control
        ########################################################################################################################
        if command.startswith("configure") and state == State.NOT_READY:
            print(
                f"INFO: CONFIGURE command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
            )
            sys.stdout.flush()
            xmlHelper = XMLFileHelper(alignConf)
            ymlHelper = YAMLFileHelper(alignConf)
            conversionsHelper = Conversion(alignConf)
            if flat is False:
                if "online" in getpass.getuser():
                    histCopier = HistoCopier(alignConf)
            alignHelper = RichAlignmentHelper(alignConf)
            updateHelper = UpdateHelper(alignConf)
            ### start the Mirror Alignment DB update DIM server for this RICH detector
            if not testing:
                updateHelper.startUpdateDIM()
            else:
                print(
                    "INFO: The alignment is in testing mode. DB update DIM service disabled."
                )
            sys.stdout.flush()
            ### get the name of the yml (xml) file that is equivalent to "the DB alignment" within the '/group/online/alignment/' directory on plus
            if YAMLio:
                latestName = ymlHelper.getStartYAMLOrigFileName()
                latest_vN = (latestName.replace(".yml", "")).replace("v", "")
            else:
                latestName = xmlHelper.getStartXMLOrigFileName()
                latest_vN = (latestName.replace(".xml", "")).replace("v", "")
            state = State.READY  # Though LHCbA is still configuring
            sys.stdout.flush()

            if polarityDiff and calibMissing:
                cancel = True
                cancelSubject = (
                    f"TEST [{alignConf.getProp('dataVariant')}]: " if testing else ""
                )
                cancelSubject += f"WARNING: RICH{whichRich} mirror alignment {setupHelper.directoryTime} started, but has been cancelled (polarityDiff and calibMissing)."
                cancelText = f"RICH{whichRich} mirror alignment {setupHelper.directoryTime} started, but has been cancelled. \n "
                cancelText += f"Data for these runs contain magnet OFF data *or* are of mixed polarity: {polarityDiff} \n "
                cancelText += f"Data for these runs do not yet have RICH calibrations available: {calibMissing} \n \n "
                cancelText += f"List of all selected Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr}"
                print("INFO: Sending email: polarityDiff and calibMissing")
                setupHelper.sendEmail(
                    cancelSubject,
                    cancelText,
                    setupHelper.emails if not testing else setupHelper.experts,
                )
                print("WARNING: Mirror alignment will be skipped.")
                print(
                    f"WARNING: These runs contain magnet OFF data *or* data for these runs is of mixed polarity: {polarityDiff}"
                )
                print(
                    f"WARNING: These runs do not yet have RICH calibrations available: {calibMissing}"
                )
                messageID = setupHelper.quickWriteInLogbook(
                    subject=cancelSubject, text=cancelText
                )
            # Mismatched polarity check
            elif polarityDiff:
                cancel = True
                cancelSubject = (
                    f"TEST [{alignConf.getProp('dataVariant')}]: " if testing else ""
                )
                cancelSubject += f"WARNING: RICH{whichRich} mirror alignment {setupHelper.directoryTime} started, but has been cancelled (polarityDiff)."
                cancelText = f"RICH{whichRich} mirror alignment {setupHelper.directoryTime} started, but has been cancelled. \n"
                cancelText += f"Data for these runs contain magnet OFF data *or* are of mixed polarity: {polarityDiff} \n \n"
                cancelText += f"List of all selected Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr}"
                print("INFO: Sending email: polarityDiff")
                setupHelper.sendEmail(
                    cancelSubject,
                    cancelText,
                    setupHelper.emails if not testing else setupHelper.experts,
                )
                print("WARNING: Mirror alignment will be skipped.")
                print(
                    f"WARNING: These runs contain magnet OFF data *or* data for these runs is of mixed polarity: {polarityDiff}"
                )
                messageID = setupHelper.quickWriteInLogbook(
                    subject=cancelSubject, text=cancelText
                )
            # RICH calibrations availability check
            elif calibMissing:
                cancel = True
                cancelSubject = (
                    f"TEST [{alignConf.getProp('dataVariant')}]: " if testing else ""
                )
                cancelSubject += f"WARNING: RICH{whichRich} mirror alignment {setupHelper.directoryTime} started, but has been cancelled (calibMissing)."
                cancelText = f"RICH{whichRich} mirror alignment {setupHelper.directoryTime} started, but has been cancelled. \n "
                cancelText += f"Data for these runs do not yet have RICH calibrations available: {calibMissing} \n \n "
                cancelText += f"List of all selected Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr}"
                print("INFO: Sending email: calibMissing")
                setupHelper.sendEmail(
                    cancelSubject,
                    cancelText,
                    setupHelper.emails if not testing else setupHelper.experts,
                )
                print("WARNING: Mirror alignment will be skipped.")
                print(
                    f"WARNING: These runs do not yet have RICH calibrations available: {calibMissing}"
                )
                messageID = setupHelper.quickWriteInLogbook(
                    subject=cancelSubject, text=cancelText
                )
            # Missing polarity info
            elif polarityMiss:
                preSubject += f"WARNING: Some polarity information was missing, but "
                preText += f"Polarity information for these runs is currently not found: {polarityMiss} \n"
                print(
                    f"WARNING: Polarity information for these runs is currently not found: {polarityMiss}. This alignment will continue as normal."
                )
            forcedUpdate_ChMagPol = False
            ChMagPolSubject = ""
            ChMagPolText = ""
            if flat is False:
                if "online" in getpass.getuser():
                    pass
                else:
                    polarities.append("UP")  # PN: fake a magnet polarity for testbench
            else:
                polarities.append(
                    "UP"
                )  # PN: we must fake a magnet polarity for Iterator_flat
            if (not cancel) and (polarities[0] is not None):
                oldPolarityFilename = (
                    f"{polarityDir}previousPolarity_RICH{whichRich}.txt"
                )
                newPolarityFilename = (
                    f"{polarityDir}currentPolarity_RICH{whichRich}.txt"
                )
                # os.system("quota -s")
                print(newPolarityFilename)
                try:
                    with open(newPolarityFilename, "w") as currentAlignPolarityFile:
                        currentAlignPolarityFile.write(
                            f"{polarities[0]}\n{runs}\n{fills}\n"
                        )
                except:
                    print("Got a Disk Quota Exceeded, continuing...")
                if os.path.exists(oldPolarityFilename):
                    with open(oldPolarityFilename, "r") as previousAlignPolarityFile:
                        first_line = previousAlignPolarityFile.readline()
                    if first_line != f"{polarities[0]}\n":
                        ChMagPolEmailSubject = (
                            f"TEST [{alignConf.getProp('dataVariant')}]: "
                            if testing
                            else ""
                        )
                        ChMagPolEmailSubject += f"WARNING: MAGNET POLARITY CHANGE! RICH{whichRich} mirror alignment {setupHelper.directoryTime}."
                        ChMagPolEmailText = f"WARNING: MAGNET POLARITY CHANGE from \n {first_line} to {polarities[0]} \n RICH{whichRich} mirror alignment {setupHelper.directoryTime}. \n Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr} \n dataVariant: {alignConf.getProp('dataVariant')}"
                        print("INFO: Sending email: Magnet Polarity Change")
                        setupHelper.sendEmail(
                            ChMagPolEmailSubject,
                            ChMagPolEmailText,
                            setupHelper.emails if not testing else setupHelper.experts,
                        )
                        forcedUpdate_ChMagPol = True
                        ChMagPolSubject += (
                            "Magnet Polarity Changed; possibly forcing update."
                        )
                        ChMagPolText += f"If this alignment converges and passes sanity checks, the alignment will be updated due to a change in magnet polarity (unless autoUpdate is/becomes False, or testing is True). If there are any problems with this alignment, please contact the Mirror Alignment expert! They should check previousPolarity_RICH in {polarityDir} to ensure the proper behavior on the next attempt!"
                        postSubject += f" {ChMagPolSubject}"
                        postText += f" \n {ChMagPolText}"
                        print(
                            "INFO: Magnet Polarity Changed; if this alignment converges under normal conditions, then the alignment will be updated."
                        )
                        # PN: This next four-line section confuses me, why are we overwriting here? Should investigate whether this is correct or not.
                        # Next two lines only get used at the end if things fail
                        ChMagPolSubject = " WARNING: The magnet polarity changed!"
                        ChMagPolText = " \n WARNING: The magnet polarity changed! Make sure that everything (e.g. previousPolarity_RICH) is in order for the next alignment!!!"
                        # OUTDATED: The expert should re-flip the magnet polarity for this Rich detector in {polarityDir} in order to have the proper behavior on the next attempt!!!
            sys.stdout.flush()

            if not cancel:
                leadText = ""
                if testing:
                    leadText += f"TEST [{alignConf.getProp('dataVariant')}]: "
                totalSubject = f"{leadText}{preSubject}RICH{whichRich} alignment {setupHelper.directoryTime} has started.{postSubject}"
                if (
                    alignConf.getProp("MajItStart") != 0
                    or alignConf.getProp("MinItStart") != 0
                ):
                    totalSubject = f"{leadText}{preSubject}RICH{whichRich} alignment {setupHelper.directoryTime} has started from Iteration _i{alignConf.getProp('MajItStart')}, Minor Iteration {alignConf.getProp('MinItStart')}.{postSubject}"
                totalText = f"{preText} RICH{whichRich} mirror alignment {setupHelper.directoryTime} has started. \n Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr}{postText} \n dataVariant: {alignConf.getProp('dataVariant')}"
                if (
                    alignConf.getProp("MajItStart") != 0
                    or alignConf.getProp("MinItStart") != 0
                ):
                    totalText = f"{preText} RICH{whichRich} mirror alignment {setupHelper.directoryTime} has started from Iteration _i{alignConf.getProp('MajItStart')}, Minor Iteration {alignConf.getProp('MinItStart')}. \n Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr}{postText} \n dataVariant: {alignConf.getProp('dataVariant')}"
                setupHelper.sendEmail(
                    totalSubject,
                    totalText,
                    setupHelper.emails if not testing else setupHelper.experts,
                )
                postText += " \n \n If this alignment does not finish, and this is not a TEST alignment, please report this via the RICH ELOG. \n "
                totalText = f"{preText} RICH{whichRich} mirror alignment {setupHelper.directoryTime} has started. \n Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr}{postText} \n dataVariant: {alignConf.getProp('dataVariant')}"
                if (
                    alignConf.getProp("MajItStart") != 0
                    or alignConf.getProp("MinItStart") != 0
                ):
                    totalText = f"{preText} RICH{whichRich} mirror alignment {setupHelper.directoryTime} has started from Iteration _i{alignConf.getProp('MajItStart')}, Minor Iteration {alignConf.getProp('MinItStart')}. \n Runs: {runs} \n Fills: {fills} \n Polarities: {polsStr}{postText} \n dataVariant: {alignConf.getProp('dataVariant')}"
                print(
                    f"INFO: RICH{whichRich} mirror alignment {setupHelper.directoryTime} has started, dataVariant: {alignConf.getProp('dataVariant')}"
                )
                sys.stdout.flush()
                messageID = setupHelper.quickWriteInLogbook(
                    subject=totalSubject, text=totalText
                )
            sys.stdout.flush()

            configureDone = True
            print(f"INFO: Next expected command is start (START_RUN)")

        ########################################################################################################################
        ##### setting up the alignment and starting the first minor iteration (this only for the very first iteration)
        ########################################################################################################################
        elif command.startswith("start") and state == State.READY:
            print(
                f"INFO: START command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
            )
            sys.stdout.flush()

            ########################################################################################################################
            ##### iteration counters, n_it for major iteration, m_it for minor iteration
            ########################################################################################################################
            n_it = alignConf.getProp("MajItStart")
            m_it = alignConf.getProp("MinItStart")
            if (n_it != 0) or (m_it != 0):
                print(f"INFO: n_it, m_it = {n_it}, {m_it}")

            try:
                elapsed_time_config
            except NameError:
                elapsed_time_config = (
                    time() - start_time_config
                )  # LHCbA configuration time
            else:
                elapsed_time_config = 0.0  # we were already configured
                if (n_it == 0) and (m_it == 0):
                    setupHelper.directoryTime = strftime(
                        "%Y%m%d_%H%M%S", gmtime()
                    )  # create a new storage directory if we STARTed a second clean alignment from the same Iterator configuration
            print(
                f"INFO: Elapsed time to configure LHCbA set as {elapsed_time_config} seconds."
            )  # only accurate if LHCbA AutoPilot is ON

            elapsed_runtimes = {
                "elapsed_time_run": 0.0,
                "elapsed_times_RichMirrCombinFit": [],
                "elapsed_times_RichMirrAlign": [],
                "elapsed_times_RichMirrRunReco": [],
            }
            start_time_run = time()
            print("INFO: Alignment start time set.")
            sys.stdout.flush()

            if (
                flat is False
            ):  ### write the file for the analyzers to know which iteration they are at - PN: This may no longer be needed or used?
                f = open(alignConf.getProp("ItNrFile"), "w")
                f.write(str(m_it))
                f.close()
            t = time()

            ### if you want to start the alignment from scratch (e.g. at the m_it = 0)
            if m_it == 0:
                # clean up the working directory
                if YAMLio:
                    print("INFO: Setting up the folders and getting the starting YAML")
                else:
                    print("INFO: Setting up the folders and getting the starting XML")
                print(f"INFO: Removing everything in {workdir}")
                for f in os.listdir(workdir):
                    if os.path.isdir(f"{workdir}/{f}"):
                        shutil.rmtree(f"{workdir}/{f}")
                    else:
                        os.remove(f"{workdir}/{f}")
                """
                shutil.copy2(  # flat is False
                shutil.copyfile(  # flat is True
                    alignConf.getProp('combAndMirrSubsets'),
                    combAndMirrSubsets)
                print("INFO: combAndMirrSubsets copied from " +
                      alignConf.getProp('combAndMirrSubsets') + " to " +
                      combAndMirrSubsets)
                """

                # consistency workflow timestamp
                yaml = YAML()
                ts = setupHelper.dt
                with open(
                    f"{workdir}rich{whichRich}_session_timestamp.yml", "w"
                ) as fout:
                    yaml.dump(ts, fout)

                # construction of the consistency workflow variant
                mp = alignConf.getProp("dataVariant")
                useHltDecisions = alignConf.getProp("useHltDecisions")

                # currently does not care if RICH1 or RICH2
                evt_max_for_offline_and_testbench = (
                    25000 * 1000
                )  # this sets a high max when running over a large data sample where we don't use HLT decisions

                minP = alignConf.getProp("minP")

                SubsetStr = ""
                if setupHelper.usingReducedSubset is True:
                    evt_max_for_offline_and_testbench = (
                        50 * 1000
                    )  # 2 * typical for aligning inner mirrors subset only, running on minbias
                    if whichRich == 2:
                        evt_max_for_offline_and_testbench = (
                            400 * 1000
                        )  # 2 * typical for aligning inner mirrors subset only, running on minbias
                    SubsetStr = "_subset"

                n_kevts = f"{math.trunc(evt_max_for_offline_and_testbench / 1000)}k"  # online we do not know this a priori

                if flat is False:
                    if "online" in getpass.getuser():
                        # number of events is a priori unknown
                        n_kevts = f"Ok"  # this is an O not a 0

                #######################################################################
                # The reco will carefully parse the variant name,
                #   do not change the format of these lines without taking great care
                #   in all of the files in PyMirrAlignOnline that use the variant name.
                #
                addinfo = f"_minp{math.trunc(minP)}{SubsetStr}_{mp}_{n_kevts}"
                #
                # PN - Guessing that the missing p in MapExlore is intentional
                mirror_align_tasks = [
                    "Produce",  # fill the production set of histograms
                    #'Monitor',  # add various checking histograms
                    #'MapExlore',  # explore tracks and photons for the HLT1 pre-selection line "map"
                    #'MapConstruct',  # construct exploratory data structure for the HLT1 pre-selection line "map"
                    #'Map',  # create data structure to be used in the HLT1 selection line with entire contents
                    #'MapUse',  # use the "map" data structure a la in the HLT1 pre-selection line "map"
                    #'MapPhi',  # create data structure to be used in the HLT1 selection line with contents in quantiles
                    #'MapPhiUse',  # use the "mapPhi" data structure in the HLT1 pre-selection line "mapPhi"
                    #'AllFillCount',  # add counters for optimization of the RICH2 mirror combinations subset
                    #'AllFillPhi',  # fill 1D phi histos for optimization of the RICH2 mirror combinations subset
                    #'CheckRestFill',  # check filling the rest of RICH2 mirror combinations along with 8 poorest
                    #'SkipFilled',  # check filling all RICH2 mirror combinations with skipping when filled
                ]
                # cumulative substring with all tasks chosen
                tasks = ""
                for task in mirror_align_tasks:
                    tasks += f"_{task}"
                #
                # form the current variant name
                current_variant = f"{ts}_rich{whichRich}{addinfo}{tasks}"
                #######################################################################

                # record the current variant name into the YAML file
                with open(f"{workdir}rich{whichRich}_variant.yml", "w") as out:
                    yaml.dump(current_variant, out)

                currentMirrorDBFilePre = f"{workdir}{current_variant}_conds_i{n_it}"
                if YAMLio:
                    ymlHelper.getStartYAML(
                        f"{currentMirrorDBFilePre}.yml"
                    )  ### get the starting-yml from the database
                    if not useDD4hep:
                        conversionsHelper.conv_YAML(f"{currentMirrorDBFilePre}.yml")
                else:
                    xmlHelper.getStartXML(
                        f"{currentMirrorDBFilePre}.xml"
                    )  ### get the starting-xml from the database
                    if not useDD4hep:
                        conversionsHelper.conv_XML(f"{currentMirrorDBFilePre}.xml")

                if not magnFactorsMode == 0:
                    if useDD4hep:
                        print(
                            "INFO: magnFactorsMode is nonzero. Creating coeff_calibration_yml files."
                        )
                        ymlHelper.create_coeff_calibration_yml_files(
                            f"{currentMirrorDBFilePre}.yml", n_it
                        )  ### create the yml files for the tilted mirror configurations
                    else:
                        print(
                            "INFO: magnFactorsMode is nonzero. Creating coeff_calibration_xml files."
                        )
                        xmlHelper.create_coeff_calibration_xml_files(
                            f"{currentMirrorDBFilePre}.xml", n_it
                        )  ### create the xml files for the tilted mirror configurations
                    sys.stdout.flush()
                else:
                    # copy the predefined magnification factors if need be
                    setupHelper.setupMagnifFiles()
                    # Replace the variant used in the predefined magnification factors, with the current_variant
                    for f in os.listdir(workdir):
                        if "_magnifs" in f and "_predefined" in f:
                            os.rename(
                                os.path.join(workdir, f),
                                os.path.join(
                                    workdir,
                                    f.replace(f[: f.find("_magnifs")], current_variant),
                                ),
                            )
                    # YAMLify magnification factors [currently for records only]
                    if not os.path.exists(
                        f"{workdir}rich{whichRich}_MirrMagnFactors_predefined.yml"
                    ):
                        convert_magnFactors_txt_YAML.to_yaml(workdir)

            ### if you want to start from some arbitrary minor iteration
            else:
                # this method will keep all files needed when starting at m_it > 0
                setupHelper.startMinIt(m_it)

            # shutil.copyfile(  # flat is True
            #    "/group/rich/pnaik/input_Iterator_flat/Claire/rich2_variant.yml",
            #    workdir + "rich2_variant.yml")  #
            # shutil.copyfile(
            #    "/group/rich/pnaik/input_Iterator_flat/Claire/2022-07-20T21-10_rich2_minp60_qnt20f60_rndmpresc_unknown0k_conds_i3.yml",
            #    workdir +
            #    "2022-07-20T21-10_rich2_minp60_qnt20f60_rndmpresc_unknown0k_conds_i3.yml"
            # )  #

            # We have to reload the variant name here, since we may be starting from a non-"zeroth" iteration
            yaml = YAML()
            with open(f"{workdir}/rich{whichRich}_variant.yml") as finp:
                variant = yaml.load(finp)
            print(f"INFO: variant read in as {variant}")

            ### now place the yml (xml) file for the current minor iteration in the place where the analyzers will pick it up
            if m_it % tiltNamesLength == 0:
                pickupDB = f"{workdir}{variant}_conds_i{n_it}"
            else:
                pickupDB = f"{workdir}{variant}_conds_{tiltNames[m_it % tiltNamesLength]}_i{n_it}"
            if useDD4hep:
                pickupDB = f"{pickupDB}.yml"
            else:
                pickupDB = f"{pickupDB}.xml"
            copy(pickupDB, currentDB)

            if m_it % tiltNamesLength == 0:
                print(f"INFO:    ___________________________ ")
                print(f"INFO:    Start new major Iteration {n_it}")
                print(f"INFO:    *************************** ")
            else:
                print(f"INFO:    ___________________________ ")
                print(f"INFO:    Continue major Iteration {n_it}")
                print(f"INFO:    *************************** ")

            yaml = YAML()
            with open(f"{workdir}rich{whichRich}_iter_number.yml", "w") as fout:
                yaml.dump(n_it, fout)

            if m_it > 99:
                print(f"INFO:    Minor iteration {m_it}********")
            elif m_it > 9:
                print(f"INFO:    Minor iteration {m_it}*********")
            else:
                print(f"INFO:    Minor iteration {m_it}**********")

            yaml = YAML()
            with open(f"{workdir}rich{whichRich}_tilt_name.yml", "w") as fout:
                yaml.dump(tiltNames[m_it % tiltNamesLength], fout)

            # Create analyzerOutDir
            analyzerOutDir = f"{workdir}Rich{whichRich}AnalyzerOutput"
            if not os.path.exists(analyzerOutDir):
                try:
                    os.makedirs(analyzerOutDir)
                    try:
                        os.chmod(analyzerOutDir, 0o775)
                    except:
                        print(f"WARNING: failed os.chmod({analyzerOutDir}, 0o775)")
                    print(f"INFO: Creating {analyzerOutDir}")
                except:
                    print(f"WARNING: failed to make {analyzerOutDir}")
            analyzerOutDir = os.path.join(analyzerOutDir, "")

            yaml = YAML()
            with open(f"{workdir}rich{whichRich}_runAnalyzer.yml", "w") as fout:
                yaml.dump("YES", fout)
                print(f"INFO: {workdir}rich{whichRich}_runAnalyzer.yml set to 'YES'")

            if flat and runInlineReco:
                start_time_RichMirrRunReco = time()

                # run the reco here
                tiltNameRecoStr = ""
                if m_it % tiltNamesLength != 0:
                    tiltNameRecoStr = "_" + tiltNames[m_it % tiltNamesLength]
                richAnalyzerOutFile = f"{analyzerOutDir}Rich{whichRich}AnalyzerOut{tiltNameRecoStr}_i{n_it}.txt"
                richAnalyzerStdOutFile = f"{analyzerOutDir}Rich{whichRich}AnalyzerStdOut{tiltNameRecoStr}_i{n_it}.txt"
                richAnalyzerStdErrFile = f"{analyzerOutDir}Rich{whichRich}AnalyzerStdErr{tiltNameRecoStr}_i{n_it}.txt"
                myStdOut = open(richAnalyzerStdOutFile, "w")
                myStdErr = open(richAnalyzerStdErrFile, "w")
                if m_it % tiltNamesLength != 0:
                    tiltNameRecoStr = f" {tiltNames[m_it % tiltNamesLength]}"
                cmd = f"cd {workdir} ; {stackDir}Panoptes/run gaudirun.py {stackDir}Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/data_online.py {stackDir}Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/hlt2_reco_rich{whichRich}_noUT_panoptes_dd4hep_both.py > {richAnalyzerOutFile} ; cd -"
                # cmd = f"cd {workdir} ; echo -n{tiltNameRecoStr} {n_it} | /group/rich/sw/alignment/stack/Panoptes/run gaudirun.py /group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/data_online.py /group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/hlt2_reco_rich{whichRich}_noUT_panoptes_dd4hep-both.py > {richAnalyzerOutFile} ; cd -"
                print(f"INFO: Starting Inline Reconstruction. ")
                print(f"INFO: cmd = {cmd}")
                p4 = subprocess.Popen(
                    cmd,
                    shell=True,
                    executable="/bin/bash",
                    stdout=myStdOut,
                    stderr=myStdErr,
                )
                p4.communicate()
                print("INFO: Inline Reconstruction Complete. ")
                myStdOut.close()
                myStdErr.close()
                sys.stdout.flush()
                hh, r = divmod(time() - start_time_RichMirrRunReco, 3600)
                mm, ss = divmod(r, 60)
                elapsed_runtimes["elapsed_times_RichMirrRunReco"].append(
                    "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
                )  # RichMirrRunReco run time

            # increase the minor iteration counter
            if magnFactorsMode == 0:
                m_it = m_it + tiltNamesLength
            else:
                m_it = m_it + 1
            # set iterator state to running => if running Online, analyzers will execute the Moore jobs now
            state = State.RUNNING
            sys.stdout.flush()
            startDone = True
            print(f"INFO: Next expected command is pause")

        ########################################################################################################################
        ##### here's the code for all the following iterations
        ########################################################################################################################
        elif command.startswith("pause") and state == State.RUNNING:
            print(
                f"INFO: PAUSE command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
            )
            state = State.PAUSED
            if flat is False:  # set the state
                com.set_status(state)

            yaml = YAML()
            with open(f"{workdir}rich{whichRich}_runAnalyzer.yml", "w") as fout:
                yaml.dump("NO", fout)
                print(f"INFO: {workdir}rich{whichRich}_runAnalyzer.yml set to 'NO'")

            if (
                flat is False
            ):  # write new minor iteration number to file for the analyzers to read
                f = open(alignConf.getProp("ItNrFile"), "w")
                f.write(str(m_it))
                f.close()

            # copy the saveset made by the analyzers in the last iteration to the desired place in workdir
            if magnFactorsMode == 0:
                if flat is False:
                    if "online" in getpass.getuser():
                        histCopier.getHistoFile(n_it, m_it - tiltNamesLength)
                    pass
                elif runInlineReco is False:
                    print(
                        "You should have put the root file manually into place by now."
                    )
            else:
                if flat is False:
                    if "online" in getpass.getuser():
                        histCopier.getHistoFile(n_it, m_it - 1)
                    pass
                elif runInlineReco is False:
                    print(
                        "You should have put the root files manually into place by now."
                    )
            sys.stdout.flush()

            allDone = False

            # if the minor iterations for one major iteration have finished perform fits etc:
            if m_it % tiltNamesLength == 0:
                print(f"INFO:    minor iterations for major iteration {n_it} finished")
                print(f"INFO:    now merge the rootfiles and perform alignment")

                if flat is False:
                    pass
                elif runInlineReco is True:
                    pass
                else:
                    # copy pre-made reconstruction files needed to run Iterator_flat
                    if whichRich == 1:
                        input_Iterator_flat = (
                            "/group/rich/pnaik/input_Iterator_flat/Alex/"
                        )
                        for x in [
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i1.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i2.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i3.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i4.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i5.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i6.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_i7.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_priYn_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_priYp_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_priZn_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_priZp_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_secYn_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_secYp_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_secZn_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_conds_secZp_i0.yml",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i0.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i1.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i2.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i3.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i4.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i5.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i6.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i7.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_priYn_i0.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_priYp_i0.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_priZn_i0.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_priZp_i0.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_secYn_i0.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_secYp_i0.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_secZn_i0.root",
                            # "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_secZp_i0.root",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_priYn_predefined.txt",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_priYp_predefined.txt",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_priZn_predefined.txt",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_priZp_predefined.txt",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_secYn_predefined.txt",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_secYp_predefined.txt",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_secZn_predefined.txt",
                            "2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_magnifs_secZp_predefined.txt",
                            "rich1_fit_align_confs.yml",
                            # "rich2_iter_number.yml"                                                ,
                            "rich1_reco_opts.yml",
                            "rich1_session_timestamp.yml",
                            "rich1_variant.yml",
                        ]:
                            print(
                                f"INFO: Copying {input_Iterator_flat + x} to {workdir + x}"
                            )
                            shutil.copyfile(input_Iterator_flat + x, workdir + x)
                        #
                    if whichRich == 2:
                        input_Iterator_flat = (
                            "/group/rich/pnaik/input_Iterator_flat/Paras/"
                        )
                        for x in [
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_conds_i0.yml",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_histos_i0.root",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_conds_i1.yml",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_histos_i1.root",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_conds_i2.yml",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_histos_i2.root",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_conds_i3.yml",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_histos_i3.root",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_conds_i4.yml",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_histos_i4.root",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_conds_i5.yml",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_histos_i5.root",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_priYn_predefined.txt",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_priYp_predefined.txt",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_priZn_predefined.txt",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_priZp_predefined.txt",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_secYn_predefined.txt",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_secYp_predefined.txt",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_secZn_predefined.txt",
                            "2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_magnifs_secZp_predefined.txt",
                            "rich2_fit_align_confs.yml",
                            # "rich2_iter_number.yml"                                                ,
                            "rich2_reco_opts.yml",
                            "rich2_session_timestamp.yml",
                            "rich2_variant.yml",
                        ]:
                            print(
                                f"INFO: Copying {input_Iterator_flat + x} to {workdir + x}"
                            )
                            shutil.copyfile(input_Iterator_flat + x, workdir + x)
                        #

                ### run the mirror Alignment - In Run 3 we have to feed these YAML via the .conf files prepared by RichAlignmentHelper
                start_time_RichMirrCombinFit = time()

                alignHelper.runMirrCombinFit(n_it)
                hh, r = divmod(time() - start_time_RichMirrCombinFit, 3600)
                mm, ss = divmod(r, 60)
                elapsed_runtimes["elapsed_times_RichMirrCombinFit"].append(
                    "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
                )  # RichMirrCombinFit run time

                enoughBinsPopulated = alignHelper.checkEnoughBinsPopulated(n_it)
                print(f"INFO: enoughBinsPopulated = {enoughBinsPopulated}")

                start_time_RichMirrAlign = time()
                alignHelper.runRichAlign(n_it)
                hh, r = divmod(time() - start_time_RichMirrAlign, 3600)
                mm, ss = divmod(r, 60)
                elapsed_runtimes["elapsed_times_RichMirrAlign"].append(
                    "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
                )  # RichMirrAlign run time

                ### pick up the new yml file from RichMirrAlign and format in in a way readable for the analyzers
                yaml = YAML()
                with open(f"{workdir}rich{whichRich}_variant.yml") as finp:
                    variant = yaml.load(finp)
                newDBfilePre = f"{workdir}{variant}_conds_i{n_it + 1}"
                if useDD4hep:
                    ymlHelper.reformatYAML(f"{newDBfilePre}.yml", workdir)
                else:
                    conversionsHelper.conv_YAML(f"{newDBfilePre}.yml")
                    ymlHelper.reformatYAML(f"{newDBfilePre}.yml", workdir)
                    xmlHelper.reformatXML(f"{newDBfilePre}.xml", workdir)

                # ##############################################################################
                # # Lines for git commit, somehow this will need to be implemented at some point - I wonder if we instead copy conditions to some master area which is automatically synced with gitlab - "online conditions flush"
                # fromFile = f"{workdir}" + f"CondDB_Rich{whichRich}.yml"
                # # print(f"align git: copyfile {fromFile} to 200000")
                # shutil.copyfile(fromFile, '/group/online/AligWork/tempAlignmentFiles/lhcb-conditions-database_remote_alignment/Conditions/Rich2/Alignment/Mirrors.yml/0')
                # pwd = os.getcwd()
                # # print(f"align git: pwd {pwd}")
                # os.chdir(f'/group/online/AligWork/tempAlignmentFiles/lhcb-conditions-database_remote_alignment')
                # # print(f"align git: cd /group/online/AligWork/tempAlignmentFiles/lhcb-conditions-database_remote_alignment")
                # os.system(f'git config --global --add safe.directory /group/online/AligWork/tempAlignmentFiles/lhcb-conditions-database_remote_alignment')
                # os.system(f'git add Conditions/Rich2/Alignment/Mirrors.yml/0')
                # # print(f"align git: git add Conditions/Rich2/Alignment/Mirrors.yml/200000")
                # os.system(f'git commit -m "alignment in progress n_it {n_it}"')
                # # print(f"align git: git commit: alignment in progress n_it {n_it}")
                # os.system(f'git push -u origin richAlign2022_testBranch')
                # # print(f"align git: git push -u origin richAlign2022_testBranch")
                # os.chdir(f'{pwd}')
                # # print(f"align git: pwd {pwd}")
                # ##############################################################################

                ### check if the alignment has converged or failed
                conv = alignHelper.hasConverged()
                print(
                    f"INFO: The alignment after Iteration {n_it} returns converged = {conv}"
                )

                if conv or n_it >= maxIters:
                    allDone = True
                    iterationsDone = True
                    if conv and n_it >= maxIters:
                        print(
                            f"INFO: *** The alignment has reached its maximum number of iterations and has converged after Iteration {n_it} ***"
                        )
                    elif conv:
                        print(
                            f"INFO: *** The alignment has converged after Iteration {n_it} ***"
                        )
                    else:
                        print(
                            "INFO: *** The alignment has reached its maximum number of iterations and has not converged ***"
                        )

                    elapsed_runtimes["elapsed_time_run"] = (
                        time() - start_time_run
                    )  # Actual total iterating time
                    print(
                        f"INFO: Elapsed time to run the Mirror Alignment set as {elapsed_runtimes['elapsed_time_run']} seconds."
                    )
                    sys.stdout.flush()

                    # Run the monitoring, send plots to the Presenter, and generate PDF files;
                    #   this returns True if the monitoring sanity check failed.
                    if YAMLio:
                        ymlHelper.getCompareYAML(
                            compareDB
                        )  ### can be different from starting-yml
                    else:
                        xmlHelper.getCompareXML(
                            compareDB
                        )  ### can be different from starting-xml
                    insane = setupHelper.monitor(n_it, compareDB, forcedUpdate_ChMagPol)
                    # insane = False  # If this needs to be faked
                    print(f"INFO: alignMonitor.isInsane = {insane}")
                    sys.stdout.flush()

                    nEventsListList = setupHelper.getNEventsListList(n_it)
                    # nEventsListList = [[10000000], 10000000]  # If this needs to be faked
                    print(
                        f"INFO: Average number of events processed per iteration = {nEventsListList[1]}"
                    )

                    # Take action based on sanity checks
                    alert = f"INFO: Rich{whichRich} mirror alignment is OK."  # Default: The mirror alignment was in autoUpdate mode to start, ran completely normally, and does not request an update, OR autoUpdate mode was False to start.
                    sanitySubject = ""
                    sanityText = ""
                    if not enoughBinsPopulated:
                        if autoUpdate_initial:
                            autoUpdate = False
                            print(
                                f"WARNING: There is poor photon population in at least one of the RICH mirror combinations. Mirror alignment {setupHelper.directoryTime} autoUpdate changed from True to False."
                            )
                            if forcedUpdate_ChMagPol:
                                autoUpdateNoV = True
                                print(
                                    "WARNING: Since the polarity changed as well, we force autoUpdateNoV to be True (HLT2 gets no version)."
                                )
                                sanitySubject += (
                                    " Polarity changed so no version provided to HLT2."
                                )
                                sanityText += "\n Since the polarity changed as well, no version was provided to HLT2. The mirror aligners should investigate."
                            setupHelper.sendEmail(
                                f"autoUpdate disabled: Poor photon population in at least one of the RICH{whichRich} mirror combinations for RICH{whichRich} alignment {setupHelper.directoryTime}.{sanitySubject}",
                                f"RICH{whichRich} mirror alignment {setupHelper.directoryTime} autoUpdate changed from True to False.{sanityText}",
                                setupHelper.emails
                                if not testing
                                else setupHelper.experts,
                            )
                            # This is not important enough to raise a WARNING to the Data Manager
                            alert = f"INFO: Rich{whichRich} mirror alignment could not make a decision due to poor photon population in at least one of the RICH mirror combinations."
                        else:
                            print(
                                f"WARNING: There is poor photon population in at least one of the RICH mirror combinations; autoUpdate was already False."
                            )
                            setupHelper.sendEmail(
                                f"Poor photon population in at least one of the RICH{whichRich} mirror combinations, autoUpdate was already False.",
                                f"RICH{whichRich} mirror alignment {setupHelper.directoryTime} autoUpdate already False.",
                                setupHelper.emails
                                if not testing
                                else setupHelper.experts,
                            )

                    if nEventsListList[1] < int(alignConf.getProp("requiredEvents")):
                        if autoUpdate_initial:
                            autoUpdate = False
                            print(
                                f"WARNING: We processed an average number of events per iteration of {nEventsListList[1]}, which is too low. Mirror alignment {setupHelper.directoryTime} autoUpdate changed from True to False. Investigate HLT farm availability for the LHCbA partition."
                            )
                            if forcedUpdate_ChMagPol:
                                autoUpdateNoV = True
                                print(
                                    "WARNING: Since the polarity changed as well, we force autoUpdateNoV to be True (HLT2 gets no version)."
                                )
                                sanitySubject += (
                                    " Polarity changed so no version provided to HLT2."
                                )
                                sanityText += f"\n Since the polarity changed as well, no version was provided to HLT2. The mirror aligners should investigate."
                            setupHelper.sendEmail(
                                f"autoUpdate disabled: Lack of events processed for RICH{str(whichRich)} alignment {setupHelper.directoryTime}.{sanitySubject}",
                                f"RICH{str(whichRich)} mirror alignment {setupHelper.directoryTime} autoUpdate changed from True to False. \n Investigate HLT farm availability for the LHCbA partition. \n Average number of events processed per iteration: {str(nEventsListList[1])}{sanityText}",
                                setupHelper.emails
                                if not testing
                                else setupHelper.experts,
                            )
                            # This is not important enough to raise a WARNING to the Data Manager
                            alert = f"INFO: Rich{str(whichRich)} mirror alignment could not make a decision due to lack of processed events. Investigate HLT farm availability for the LHCbA partition."  # There may have also been poor photon population.
                        else:
                            print(
                                f"WARNING: We processed an average number of events per iteration of {nEventsListList[1]}, which is too low; autoUpdate was already False."
                            )
                            setupHelper.sendEmail(
                                f"Lack of events processed for RICH{str(whichRich)} alignment {setupHelper.directoryTime}, autoUpdate was already False.",
                                f"RICH{str(whichRich)} mirror alignment {setupHelper.directoryTime} autoUpdate already False. \n Average number of events processed per iteration: {str(nEventsListList[1])}",
                                setupHelper.emails
                                if not testing
                                else setupHelper.experts,
                            )

                    if insane:
                        if autoUpdate_initial:
                            autoUpdate = False
                            print(
                                f"WARNING: Tolerance sanity checks have failed; Mirror alignment {setupHelper.directoryTime} autoUpdate changed from True to False."
                            )
                            if forcedUpdate_ChMagPol:
                                autoUpdateNoV = True
                                print(
                                    "WARNING: Since the polarity changed as well, we force autoUpdateNoV to be True (HLT2 gets no version)."
                                )
                                sanitySubject += (
                                    " Polarity changed so no version provided to HLT2."
                                )
                                sanityText += " Since the polarity changed as well, no version was provided to HLT2. The mirror aligners should investigate."
                            setupHelper.sendEmail(
                                f"autoUpdate disabled: Tolerance sanity checks failed for RICH{str(whichRich)} alignment {setupHelper.directoryTime}.{sanitySubject}",
                                f"RICH{str(whichRich)} mirror alignment {setupHelper.directoryTime} autoUpdate changed from True to False. \n Tolerance sanity checks have failed for this alignment.{sanityText}",
                                setupHelper.emails
                                if not testing
                                else setupHelper.experts,
                            )
                            alert = f"ERROR: Rich{str(whichRich)} mirror alignment converged but constants changes are unreasonably large; 8-21h (21h-8h): please call (email) the Alignment piquet."  # There may have also been a lack of processed events, or poor photon population.
                        else:
                            print(
                                "WARNING: Tolerance sanity checks have failed; autoUpdate was already False."
                            )
                            setupHelper.sendEmail(
                                f"Tolerance sanity checks failed for RICH{str(whichRich)} alignment {setupHelper.directoryTime}, autoUpdate was already False.",
                                f"RICH{str(whichRich)} mirror alignment {setupHelper.directoryTime} autoUpdate already False. \n Tolerance sanity checks have failed for this alignment.",
                                setupHelper.emails
                                if not testing
                                else setupHelper.experts,
                            )

                    sys.stdout.flush()

                    print(
                        f"ALEX {forcedUpdate_ChMagPol}, {updateHelper.updateDecision(n_it)}, {conv}"
                    )
                    # if the alignment has converged AND there was no sanity problem AND we are not testing, overwrite the old polarity file with the new polarity file
                    if (
                        conv
                        and not insane
                        and not nEventsListList[1]
                        < int(alignConf.getProp("requiredEvents"))
                        and not testing
                    ):
                        try:
                            copy(
                                newPolarityFilename,  #
                                oldPolarityFilename,
                            )  #
                        except:
                            print(
                                f"WARNING: failed copy(newPolarityFilename,oldPolarityFilename)"
                            )
                    if (
                        forcedUpdate_ChMagPol or updateHelper.updateDecision(n_it)
                    ) and conv:
                        # if conv:
                        # we now *always* use the newer YAML (XML) file [result of RichMirrAlign (result of RichMirrAlign, converted to XML)] when updating the mirror alignment, not the current YAML (XML)
                        if YAMLio:
                            copy(f"{newDBfilePre}.yml", lastDB)
                        else:
                            copy(f"{newDBfilePre}.xml", lastDB)

                        if autoUpdate:  # was initially True and is still True
                            if testing:
                                vN_DB_str = f"v{int(latest_vN) + 1}_testing_{setupHelper.directoryTime}"
                            else:
                                vN_DB_str = f"v{int(latest_vN) + 1}"
                            if YAMLio:
                                ymlHelper.finalYAML(vN_DB_str, True)  # current + 1
                            else:
                                xmlHelper.finalXML(vN_DB_str, True)  # current + 1
                            if testing:
                                setupHelper.finalize(  #
                                    vN_DB_str,  #
                                    conv,  #
                                    n_it,  #
                                    elapsed_time_config,  #
                                    elapsed_runtimes,  #
                                    None,  #
                                    nEventsListList,  #
                                    messageID,
                                )  #
                            else:  # If not testing, and autoUpdate is still on, it's a good alignment.
                                setupHelper.finalize(  #
                                    vN_DB_str,  #
                                    conv,  #
                                    n_it,  #
                                    elapsed_time_config,  #
                                    elapsed_runtimes,  #
                                    True,  #
                                    nEventsListList,  #
                                    messageID,
                                )  #
                                alert = (
                                    f"INFO: Rich{whichRich} mirror alignment updated."
                                )
                                updateHelper.updateDB(
                                    int(latest_vN) + 1, True, alert
                                )  # current + 1
                        else:  # autoUpdate was never True, or was True but now switched to False
                            if testing:
                                vN_DB_str = f"v{int(latest_vN) + 1}_testing_maybe_{setupHelper.directoryTime}"
                            else:
                                vN_DB_str = f"v{int(latest_vN) + 1}_maybe_{setupHelper.directoryTime}"
                            if YAMLio:
                                ymlHelper.finalYAML(
                                    vN_DB_str, True
                                )  # (current + 1)_maybe
                            else:
                                xmlHelper.finalXML(
                                    vN_DB_str, True
                                )  # (current + 1)_maybe
                            setupHelper.finalize(
                                vN_DB_str,  #
                                conv,  #
                                n_it,  #
                                elapsed_time_config,  #
                                elapsed_runtimes,  #
                                None,  #
                                nEventsListList,  #
                                messageID,
                            )  #

                            if not testing:
                                if not autoUpdateNoV:  # provide the existing version ONLY if autoUpdate was always off, or autoUpdate was on but is now off *and* autoUpdateNoV remains False
                                    print(
                                        f"SENT UPDATE {testing} {autoUpdateNoV} SENT updateDB :{int(latest_vN)} False {alert}:"
                                    )
                                    updateHelper.updateDB(int(latest_vN), False, alert)
                                else:
                                    print(
                                        f"{testing} {autoUpdateNoV} would have run updateDB {int(latest_vN)} {alert}"
                                    )
                            else:
                                print(
                                    f"{testing} {autoUpdateNoV} would have run updateDB {int(latest_vN)} {alert}"
                                )
                        # updateHelper.stopUpdateDIM()

                    else:
                        vN_DB_str = f"v{int(latest_vN)}"
                        setupHelper.finalize(
                            vN_DB_str,
                            conv,
                            n_it,
                            elapsed_time_config,
                            elapsed_runtimes,
                            False,
                            nEventsListList,
                            messageID,
                        )
                        if not testing:
                            if autoUpdate_initial and not conv:
                                alert = f"ERROR: Rich{whichRich} mirror alignment failed to converge; 8-21h (21h-8h): please call (email) the Alignment piquet."  # There may have also been a lack of processed events / sanity check failure.
                            updateHelper.updateDB(
                                int(latest_vN), False, alert
                            )  # current
                            # updateHelper.stopUpdateDIM() #PN: Better check if this is meant to be commented or not

                    print(f"INFO: The Iterator state is about to be set to {state}.")
                    state = State.READY

                    # Final bits: save Configuration, OnlineEnvBase, and hlt02 logs
                    print(
                        f"INFO: The Rich{whichRich} Mirror Alignment sequence is complete. Storing alignment configurations and logs."
                    )
                    sys.stdout.flush()

                    if not cancel:
                        try:
                            copy(
                                origConfiguration,
                                f"{setupHelper.savedir}Configuration.py",
                            )
                            print(
                                f"INFO: Configuration.py stored in file: {setupHelper.savedir}Configuration.py"
                            )
                        except:
                            try:
                                copy(
                                    origConfiguration,
                                    f"{loggingDir}Rich{whichRich}_Configuration_{setupHelper.directoryTime}.py",
                                )
                                print(
                                    f"INFO: Configuration.py stored in file: {loggingDir}Rich{whichRich}_Configuration_{setupHelper.directoryTime}.py"
                                )
                            except:
                                # print(f"WARNING: failed copy(origConfiguration, {loggingDir}Rich{whichRich}_Configuration_{setupHelper.directoryTime}.py)")
                                copy(
                                    origConfiguration,
                                    f"{launchDir}Rich{whichRich}_Configuration_{setupHelper.directoryTime}.py",
                                )
                                print(
                                    f"INFO: Configuration.py stored in file: {launchDir}Rich{whichRich}_Configuration_{setupHelper.directoryTime}.py"
                                )
                        try:
                            copy(
                                origOnlineEnvBase,
                                f"{setupHelper.savedir}OnlineEnvBase.py",
                            )
                            print(
                                f"INFO: OnlineEnvBase.py stored in file: {setupHelper.savedir}OnlineEnvBase.py"
                            )
                        except:
                            try:
                                copy(
                                    origOnlineEnvBase,
                                    f"{loggingDir}Rich{whichRich}_OnlineEnvBase_{setupHelper.directoryTime}.py",
                                )
                                print(
                                    f"INFO: OnlineEnvBase.py stored in file: {loggingDir}Rich{whichRich}_OnlineEnvBase_{setupHelper.directoryTime}.py"
                                )
                            except:
                                # print(f"WARNING: failed copy(origOnlineEnvBase, {loggingDir}Rich{whichRich}_OnlineEnvBase_{setupHelper.directoryTime}.py)")
                                opy(
                                    origOnlineEnvBase,
                                    f"{launchDir}Rich{whichRich}_OnlineEnvBase_{setupHelper.directoryTime}.py",
                                )
                                print(
                                    f"INFO: OnlineEnvBase.py stored in file: {launchDir}Rich{whichRich}_OnlineEnvBase_{setupHelper.directoryTime}.py"
                                )
                    sys.stdout.flush()

                    if not os.path.exists(setupHelper.savedir):
                        print(
                            f"INFO: Expect log file to be written to: {loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log"
                        )
                        print(
                            f"INFO: Expect reduced log file to be written to: {loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02reduced.log"
                        )
                        if cancel:
                            print(
                                "INFO: We choose to not execute the mirror alignment. "
                            )
                            print(
                                f"INFO: The input data is of mixed polarity and/or some it is magnet OFF data and/or some RICH calibrations are missing."
                            )
                        else:
                            setupHelper.sendEmail(
                                f"ERROR: RICH{whichRich} mirror alignment {setupHelper.directoryTime} did not complete normally.{ChMagPolSubject}",
                                f"ERROR: RICH{whichRich} mirror alignment {setupHelper.directoryTime} did not complete normally. \n No output directory was written. \n Log may be found on plus.cern.ch (via lbgw) in {richFilesDir}Logging/ with timestamp {setupHelper.directoryTime}.{ChMagPolText} \n Runs: {str(runs)} \n Fills: {str(fills)} \n Polarities: {str(polsStr)}{postText} \n dataVariant: {alignConf.getProp('dataVariant')}",
                                setupHelper.emails
                                if not testing
                                else setupHelper.experts,
                            )
                            print(
                                f"ERROR: RICH{whichRich} mirror alignment {setupHelper.directoryTime} did not complete normally. No output directory was written."
                            )
                    else:
                        print(
                            f"INFO: Expect log file to be written to: {setupHelper.savedir}hlt02.log"
                        )
                        print(
                            f"INFO: Expect reduced log file to be written to: {setupHelper.savedir}hlt02reduced.log"
                        )
                    sys.stdout.flush()

                    if os.path.exists(setupHelper.savedir):
                        sys.stdout.flush()
                        try:
                            copy(currentHLT02log, f"{setupHelper.savedir}hlt02.log")
                            print(
                                f"INFO: stored hlt02 log in file: {setupHelper.savedir}hlt02.log"
                            )
                            # remove what we currently believe are extraneous items
                            # os.system(f'grep -vE "{removeExtraneous}" {setupHelper.savedir}hlt02.log > {setupHelper.savedir}hlt02reduced.log')
                            with (
                                open(
                                    f"{setupHelper.savedir}hlt02.log", "rb"
                                ) as input_file,
                                open(
                                    f"{setupHelper.savedir}hlt02reduced.log", "wb"
                                ) as output_file,
                            ):
                                for line in input_file:
                                    if not any(
                                        pattern in line.decode()
                                        for pattern in removeExtraneous_patterns
                                    ):
                                        output_file.write(line)
                            print(
                                f"INFO: stored reduced file: {setupHelper.savedir}hlt02reduced.log"
                            )
                        except:
                            print(
                                f"WARNING: failed copy(currentHLT02log, {setupHelper.savedir}hlt02.log)"
                            )
                            copy(currentHLT02log, f"{launchDir}hlt02.log")
                            print(
                                f"INFO: stored hlt02 log in file: {launchDir}hlt02.log"
                            )
                            # remove what we currently believe are extraneous items
                            # os.system(f'grep -vE "{removeExtraneous}" {launchDir}hlt02.log > {launchDir}hlt02reduced.log')
                            with (
                                open(f"{launchDir}hlt02.log", "rb") as input_file,
                                open(
                                    f"{launchDir}hlt02reduced.log", "wb"
                                ) as output_file,
                            ):
                                for line in input_file:
                                    if not any(
                                        pattern in line.decode()
                                        for pattern in removeExtraneous_patterns
                                    ):
                                        output_file.write(line)
                            print(
                                f"INFO: stored reduced file: {launchDir}hlt02reduced.log"
                            )
                        try:
                            lastAlignSuccessFile = open(lastAlignSuccess, "w")
                            lastAlignSuccessFile.write(
                                "YES\n"
                            )  # python will convert \n to os.linesep
                            lastAlignSuccessFile.close()
                        except:
                            print(f"WARNING: failed open(lastAlignSuccess, 'w')")
                    else:
                        print(
                            f"INFO: storing reduced file: {loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02reduced.log"
                        )
                        sys.stdout.flush()
                        try:
                            copy(
                                currentHLT02log,
                                f"{loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log",
                            )
                            print(
                                f"INFO: stored hlt02 log in file: {loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log"
                            )
                            # remove what we currently believe are extraneous items
                            # os.system(f'grep -vE "{removeExtraneous}" {loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log > {loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02reduced.log')
                            with (
                                open(
                                    f"{loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log",
                                    "rb",
                                ) as input_file,
                                open(
                                    f"{loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02reduced.log",
                                    "wb",
                                ) as output_file,
                            ):
                                for line in input_file:
                                    if not any(
                                        pattern in line.decode()
                                        for pattern in removeExtraneous_patterns
                                    ):
                                        output_file.write(line)
                        except:
                            print(
                                f"WARNING: failed copy(currentHLT02log, {loggingDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log)"
                            )
                            copy(
                                currentHLT02log,
                                f"{launchDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log",
                            )
                            print(
                                f"INFO: stored hlt02 log in file: {launchDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log"
                            )
                            # remove what we currently believe are extraneous items
                            # os.system(f'grep -vE "{removeExtraneous}" {launchDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log > {launchDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02reduced.log')
                            with (
                                open(
                                    f"{launchDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02.log",
                                    "rb",
                                ) as input_file,
                                open(
                                    f"{launchDir}Rich{whichRich}_{setupHelper.directoryTime}_hlt02reduced.log",
                                    "wb",
                                ) as output_file,
                            ):
                                for line in input_file:
                                    if not any(
                                        pattern in line.decode()
                                        for pattern in removeExtraneous_patterns
                                    ):
                                        output_file.write(line)
                        if cancel:
                            try:
                                lastAlignSuccessFile = open(lastAlignSuccess, "w")
                                lastAlignSuccessFile.write(
                                    "YES\n"
                                )  # python will convert \n to os.linesep
                                lastAlignSuccessFile.close()
                            except:
                                print(f"WARNING: failed open(lastAlignSuccess, 'w')")

                    # For good measure, make a reduced log in the logging directory
                    # os.system(f'grep -vE "{removeExtraneous}" {currentHLT02log} > {currentHLT02logReduced}')
                    try:
                        with (
                            open(f"{currentHLT02log}", "rb") as input_file,
                            open(f"{currentHLT02logReduced}", "wb") as output_file,
                        ):
                            for line in input_file:
                                if not any(
                                    pattern in line.decode()
                                    for pattern in removeExtraneous_patterns
                                ):
                                    output_file.write(line)
                    except:
                        print("WARNING: failed to produce currentHLT02logReduced")

                    print(f"INFO: Next expected command is reset (RESET)")

                ### if the alignment has not converged and the maximal number of iterations has not been reached,
                ### set up everything for the next major iteration
                if n_it < maxIters and not conv:
                    n_it = n_it + 1
                    print(f"INFO:    ___________________________ ")
                    print(f"INFO:    Start new major Iteration {n_it}")
                    print(f"INFO:    *************************** ")

                    yaml = YAML()
                    with open(f"{workdir}rich{whichRich}_iter_number.yml", "w") as fout:
                        yaml.dump(n_it, fout)

                    yaml = YAML()
                    with open(f"{workdir}/rich{whichRich}_variant.yml") as finp:
                        variant = yaml.load(finp)

                    ### get the yml-file produced by the last fit and create the ones with the tilted mirrors (xml for DetDesc, yml for DD4hep)
                    currentMirrorDBFilePre = f"{workdir}{variant}_conds_i{n_it}"
                    if not magnFactorsMode == 0:
                        if useDD4hep:
                            print(
                                "INFO: magnFactorsMode is nonzero. Creating coeff_calibration_yml files."
                            )
                            ymlHelper.create_coeff_calibration_yml_files(
                                f"{currentMirrorDBFilePre}.yml", n_it
                            )
                        else:
                            print(
                                "INFO: magnFactorsMode is nonzero. Creating coeff_calibration_xml files."
                            )
                            xmlHelper.create_coeff_calibration_xml_files(
                                f"{currentMirrorDBFilePre}.xml", n_it
                            )
                        sys.stdout.flush()

            ### just start the next minor iteration if not converged yet
            if n_it <= maxIters and not conv and not allDone:
                if m_it > 99:
                    print(f"INFO:    Minor iteration {m_it}********")
                elif m_it > 9:
                    print(f"INFO:    Minor iteration {m_it}*********")
                else:
                    print(f"INFO:    Minor iteration {m_it}**********")

                yaml = YAML()
                with open(f"{workdir}/rich{whichRich}_variant.yml") as finp:
                    variant = yaml.load(finp)
                if m_it % tiltNamesLength == 0:
                    pickupDB = f"{workdir}{variant}_conds_i{n_it}"
                else:
                    pickupDB = f"{workdir}{variant}_conds_{tiltNames[m_it % tiltNamesLength]}_i{n_it}"
                if useDD4hep:
                    pickupDB = f"{pickupDB}.yml"
                else:
                    pickupDB = f"{pickupDB}.xml"
                copy(pickupDB, currentDB)

                yaml = YAML()
                with open(f"{workdir}rich{whichRich}_tilt_name.yml", "w") as fout:
                    yaml.dump(tiltNames[m_it % tiltNamesLength], fout)

                yaml = YAML()
                with open(f"{workdir}rich{whichRich}_runAnalyzer.yml", "w") as fout:
                    yaml.dump("YES", fout)
                    print(
                        f"INFO: {workdir}rich{whichRich}_runAnalyzer.yml set to 'YES'"
                    )

                if flat and runInlineReco:
                    start_time_RichMirrRunReco = time()

                    # run the reco here
                    tiltNameRecoStr = ""
                    if m_it % tiltNamesLength != 0:
                        tiltNameRecoStr = f"_{tiltNames[m_it % tiltNamesLength]}"
                    richAnalyzerOutFile = f"{analyzerOutDir}Rich{whichRich}AnalyzerOut{tiltNameRecoStr}_i{n_it}.txt"
                    richAnalyzerStdOutFile = f"{analyzerOutDir}Rich{whichRich}AnalyzerStdOut{tiltNameRecoStr}_i{n_it}.txt"
                    richAnalyzerStdErrFile = f"{analyzerOutDir}Rich{whichRich}AnalyzerStdErr{tiltNameRecoStr}_i{n_it}.txt"
                    myStdOut = open(richAnalyzerStdOutFile, "w")
                    myStdErr = open(richAnalyzerStdErrFile, "w")
                    if m_it % tiltNamesLength != 0:
                        tiltNameRecoStr = f" {tiltNames[m_it % tiltNamesLength]}"
                    cmd = f"cd {workdir} ; {stackDir}Panoptes/run gaudirun.py {stackDir}Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/data_online.py {stackDir}Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/hlt2_reco_rich{whichRich}_noUT_panoptes_dd4hep_both.py > {richAnalyzerOutFile} ; cd - "
                    # cmd = f"cd {workdir} ; echo -n{tiltNameRecoStr} {n_it} | /group/rich/sw/alignment/stack/Panoptes/run gaudirun.py /group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/data_online.py /group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/hlt2_reco_rich{whichRich}_noUT_panoptes_dd4hep-both.py > {richAnalyzerOutFile} ; cd - "
                    print("INFO: Starting Inline Reconstruction. ")
                    print(f"INFO: cmd = {cmd}")
                    p4 = subprocess.Popen(
                        cmd,
                        shell=True,
                        executable="/bin/bash",
                        stdout=myStdOut,
                        stderr=myStdErr,
                    )
                    p4.communicate()
                    print("INFO: Inline Reconstruction Complete. ")
                    myStdOut.close()
                    myStdErr.close()
                    sys.stdout.flush()
                    hh, r = divmod(time() - start_time_RichMirrRunReco, 3600)
                    mm, ss = divmod(r, 60)
                    elapsed_runtimes["elapsed_times_RichMirrRunReco"].append(
                        "{:0>2}:{:0>2}:{:05.2f}".format(int(hh), int(mm), ss)
                    )  # RichMirrRunReco run time

                if magnFactorsMode == 0:
                    m_it = m_it + tiltNamesLength
                else:
                    m_it = m_it + 1

                state = State.RUNNING

                print(f"INFO: Next expected command is pause")

                sys.stdout.flush()
            sleep(2)

        elif command.startswith("stop") and state in (State.RUNNING, State.READY):
            print(
                f"INFO: STOP command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
            )
            elapsed_runtimes["elapsed_time_run"] = (
                time() - start_time_run
            )  # Actual iterating time
            print(
                f"INFO: Elapsed time to run the Mirror Alignment set as {elapsed_runtimes['elapsed_time_run']} seconds."
            )

            nEventsListList = setupHelper.getNEventsListList(n_it)
            # nEventsListList = [[10000000], 10000000] # If this needs to be faked

            vN_DB_str = f"v{int(latest_vN)}"
            # PN: Note to self, or ask Claire [depending on when this was put in]:
            #     Should we really be finalizing the alignment if a STOP occurs?
            setupHelper.finalize(
                vN_DB_str,  #
                conv,  #
                n_it,  #
                elapsed_time_config,  #
                elapsed_runtimes,  #
                False,  #
                nEventsListList,  #
                messageID,
            )  #
            if not testing:
                alert = f"INFO: Rich{whichRich} mirror alignment received an unexpected STOP command."  # There may have also been a lack of processed events / sanity check failure.
                if autoUpdate_initial:
                    alert = f"ERROR: Rich{whichRich} mirror alignment received an unexpected STOP command; 8-21h (21h-8h): please call (email) the Alignment piquet."  # There may have also been a lack of processed events / sanity check failure.
                updateHelper.updateDB(int(latest_vN), False, alert)  # current
                # updateHelper.stopUpdateDIM()
            setupHelper.sendEmail(
                f"STOP command received for RICH{whichRich} alignment {setupHelper.directoryTime}.",
                f"RICH{whichRich} mirror alignment {setupHelper.directoryTime} received an unexpected STOP command. \n Check for source of error, it may have involved a Traceback.",
                setupHelper.emails if not testing else setupHelper.experts,
            )
            state = State.READY
            sys.stdout.flush()

        elif command.startswith("reset"):
            print(
                f"INFO: RESET command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
            )
            sys.stdout.flush()
            state = State.NOT_READY
            if flat is False:
                print(f"INFO: Last expected command is unload")
            else:
                break

        elif command.startswith("unload"):
            print(
                f"INFO: UNLOAD command received at {strftime('%Y-%m-%d %H:%M:%S', gmtime())} UTC"
            )
            sys.stdout.flush()
            state = State.OFFLINE
            break

        else:
            print(f"ERROR: Iterator: bad transition from {state} to {command}")
            sys.stdout.flush()
            state = State.ERROR
            break

        # Set our status
        if flat is False:  # set the state
            com.set_status(state)
        print(f"INFO: Iterator state = {state}")

    ########################################################################################################################
    ##### This is where the party ends! (well, almost...)
    ########################################################################################################################

    print(f"INFO: Iterator state set to {state}.")
    print(f"INFO: End of run(whichRich = {whichRich}).")
    print(f"INFO: Goodbye from the Rich{whichRich} Mirror Alignment!")
    sys.stdout.flush()

    import atexit

    # Register a cleanup function
    def cleanup():
        # Terminate the subprocess if it's still running
        if logProcess.poll() is None:
            logProcess.terminate()

    # Register cleanup function to be called on program exit
    atexit.register(cleanup)
    logProcess.kill()

    print("INFO: hlt02 logProcess for the mirror alignment has been killed.")
    sys.stdout.flush()

    # Set our status one last time
    print(f"INFO: Iterator state = {state}")
    sys.stdout.flush()
    if flat is False:  # set the state
        com.set_status(state)
    print("INFO: Claire says miezmiezmiez. Jake says whatwhatwhat.")
    sys.stdout.flush()


if __name__ == "__main__":
    run()

###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Vidar Marsh, Jake Reich, Paras Naik"
__copyright__ = "University of Bristol, May 2022"

import argparse
import os
import re
import sys
from distutils.util import strtobool
from fileinput import filename

import numpy as np
import yaml
from lxml import etree

import PyMirrAlignOnline.converter_objects as obj

# set flags
applyNewNC = True  # set to True to apply new naming convention when converting
blisted_write_file = True  # Set to True to write blocklisted conditions to file when converting from xml->yaml (to be added back when converting back to xml)
reAdd_blocklisted_conditions = True  # set to True to re-add xml conditions which were removed when converting xml -> yaml
tack_on = False  # tack on or off extra lines (set None to do Nothing; False removes the text that was tacked on when converting)

# set which RICH detector for running via __main__
whichRICH = 1

# set which configuration files path is used when running via __main__ (when running via run control, set this in our Configuration.py)
config_path = r"/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/XML_YAML_config/"
# set which xml_blocklisted_conditions folder is used when running via __main__ (when running via run control, set this in our Configuration.py)
blisted_conds_path = r"/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/XML_YAML_config/"


class Condition(yaml.YAMLObject):
    """
    Configurable YAML object used format data before it is dumped to a YAML
    file.
    """

    yaml_tag = "!alignment"


class Conversion:
    """
    Conversion object that allows for swift changes between XML and YAML
    files.
    """

    def __init__(self, configuration=""):
        self.alignConf = configuration
        self.whichRich = whichRICH
        self.configPath = config_path
        self.blistedCondsPath = blisted_conds_path
        if self.alignConf != "":
            self.whichRich = self.alignConf.getProp("Rich")
            self.configPath = os.path.join(
                self.alignConf.getProp("stackDir"),
                self.alignConf.getProp("XML_YAML_configDir"),
            )  #
            self.blistedCondsPath = os.path.join(
                self.alignConf.getProp("stackDir"),
                self.alignConf.getProp("XML_YAML_configDir"),
            )  # self.alignConf.getProp('WorkDir')  # PN - not sure yet if WorkDir is the best place
        # Add configuration files and xml_blocklisted_conditions folder to path.
        sys.path.append(self.configPath)
        sys.path.append(self.blistedCondsPath)
        blist_path = os.path.sep.join((self.configPath, "blocklist_config.txt"))
        blist_path = r"{}".format(blist_path)
        yaml_parser_path = os.path.sep.join((self.configPath, "yaml_parser_config.txt"))
        yaml_parser_path = r"{}".format(yaml_parser_path)
        param_path = os.path.sep.join((self.configPath, "param_conversion_config.txt"))
        param_path = r"{}".format(param_path)
        unit_conv_path = os.path.sep.join(
            (self.configPath, "unit_conversion_config.txt")
        )
        unit_conv_path = r"{}".format(unit_conv_path)
        # Create blocklist: Parse contentes of blocklist_config.txt list, use
        # to create blocklist object.
        temp = obj.load_to_list(blist_path)
        self.blocklist = obj.BlockList(temp)
        # Configure yaml parser to accept custom tags.
        temp = obj.load_to_list(yaml_parser_path)
        for tag in temp:
            yaml.add_constructor("{}".format(tag), obj.custom_constructor)
        # Configure Conversions for name and unit translation between files:
        #     name_xml(name in xml) -> name in yaml
        #     unit_xml(name in xml) -> unit in xml
        self.conversions = obj.Conversions()
        temp = obj.load_to_list(param_path)
        for line in temp:
            elements = line.split(":")
            self.conversions.name_xml[elements[0]] = elements[2]
            self.conversions.name_yml[elements[2]] = elements[0]
            self.conversions.unit_xml[elements[0]] = elements[1]
            self.conversions.unit_yml[elements[2]] = elements[3]
        # Configure conv_unit list for convert_unit method (allowing for
        # numerical conversions).
        temp = obj.load_to_list(unit_conv_path)
        for line in temp:
            elements = line.split(":")
            self.conversions.unit_conv[elements[0]] = (elements[1], float(elements[2]))

    def conv_XML(self, file_path):
        """
        Convert XML to YAML. This will take the information from and XML file
        and output it in a YAML file. The outputted YAML file will be stored
        in the same location as the XML file with the same filename

        Args:
            file_path: string, absolute path to the XML file
        """
        file_type = file_path[-4:]
        if file_type != ".xml":
            raise TypeError('input file is "{}" not ".xml"'.format(file_type))
        # Generate output file name from input file name.
        file_dir, file_name = os.path.split(file_path)
        file_name = file_name[:-4] + "_converted.yml"
        out_file_path = os.path.sep.join((file_dir, file_name))
        if os.path.exists(out_file_path):
            os.remove(out_file_path)
        print("converting file: " + file_path + " ,(XML->YAML)...")
        # Generate blocklisted list file from input file name.
        if blisted_write_file == True:
            blisted_file_path = os.path.sep.join(
                (self.blistedCondsPath, file_name.split(".")[0] + "_blisted_nodes.txt")
            )
            if os.path.exists(blisted_file_path):
                os.remove(blisted_file_path)

        # Check for root in XML document, create temp file if missing
        try:
            xml_tree = etree.parse(file_path)
        except:
            with open(file_path, "r") as f:
                lines = f.readlines()
            lines.insert(0, "<root>")
            lines.append("</root>")
            current_dir = os.getcwd()
            temp_name = "temp.xml"
            temp_path = os.path.sep.join((current_dir, temp_name))
            exists = os.path.isfile(temp_path)
            while exists == True:
                temp_name = "temp" + temp_name
                temp_path = os.path.sep.join((current_dir, temp_name))
                exists = os.path.isfile(temp_path)
            with open(temp_path, "w") as f:
                f.writelines(lines)
            xml_tree = etree.parse(temp_path)
            os.remove(temp_path)
        # Conversion
        for xml_node in xml_tree.iter("condition"):
            yml_node_name = xml_node.get("name")

            # nodes to remove (save to .txt file to be added when converting back to xml)
            if self.blocklist.check(yml_node_name) == True:
                blisted_node = etree.tostring(
                    xml_node, pretty_print=True, encoding="unicode"
                )
                if reAdd_blocklisted_conditions:
                    with open(blisted_file_path, "a+") as f:
                        f.write("  " + blisted_node)

            # nodes to keep
            if self.blocklist.check(yml_node_name) == False:
                yml_condition = Condition()

                for xml_subnode in xml_node.iter("paramVector"):
                    xml_subnode_name = xml_subnode.get("name")
                    yml_subnode_name = self.conversions.get_name_yml(xml_subnode_name)
                    xml_data = xml_subnode.text.split()
                    yml_data = []
                    for xml_datapoint in xml_data:
                        try:
                            xml_value, xml_unit = xml_datapoint.split("*")
                            xml_value = float(xml_value)
                            yml_datapoint = "{} * {}".format(xml_value, xml_unit)
                        except:
                            xml_value = xml_datapoint
                            sig_figs = obj.find_sigfigs(xml_value)
                            xml_value = float(xml_value)
                            # Workaround for sigfigs == 0 causeing errors
                            # with np.format_float_positional
                            if sig_figs == 0:
                                sig_figs = 1
                            yml_unit = self.conversions.get_unit_yml(yml_subnode_name)
                            yml_value = self.conversions.convert_unit(
                                xml_value, xml_unit, yml_unit
                            )
                            yml_value = np.format_float_positional(
                                yml_value,
                                precision=sig_figs,
                                fractional=False,
                                trim="0",
                            )
                            yml_datapoint = float(yml_value)
                        yml_data.append(yml_datapoint)
                    yml_condition.__dict__[yml_subnode_name] = yml_data

                # apply new naming convention
                if applyNewNC == True:
                    # prepend "R1" or "R2"
                    if self.whichRich == 1:
                        yml_node_name = "R1" + yml_node_name
                    else:
                        yml_node_name = "R2" + yml_node_name

                    yml_node_name = re.sub("Sph", "M1", yml_node_name)
                    yml_node_name = re.sub("Sec", "M2", yml_node_name)
                    yml_node_name = re.sub("Mirror", "Seg", yml_node_name)

                    yml_node_name = re.sub(
                        "_Align", "", yml_node_name
                    )  # remove "_Align"
                    yml_node_name = re.sub(
                        "Rich" + str(self.whichRich), "", yml_node_name
                    )  # remove "Rich1" or "Rich2"

                    # add 0, e.g. R1M1Seg5 -> R1M1Seg05
                    if len(yml_node_name) == 8:
                        yml_node_name = yml_node_name[:-1] + "0" + yml_node_name[-1:]

                yml_obj = {yml_node_name: yml_condition}

                # print(yml_obj)
                with open(out_file_path, "a+") as f:
                    # f.write(yaml.dump(yml_obj, sort_keys=False, \
                    f.write(yaml.dump(yml_obj, default_flow_style=None))

        # tack on extra lines from yaml_tack_on_Rich_X.txt
        if type(tack_on) == bool:
            obj.tack_on_off(out_file_path, tack_on, self.configPath, self.whichRich)

        print("converted. output file: " + out_file_path)

    def conv_YAML(self, file_path):
        """
        Convert YAML to XML. This will take the information in a YAML file and
        output it in an XML file. The XML file will then be stored in the same
        location as the YAML file with the same filename.

        Args:
            file_path: raw string, absolute path to the YAML file
        """
        file_type = file_path[-4:]
        if file_type != ".yml":
            raise TypeError('input file is "{}" not ".yml"'.format(file_type))
        # Generate output file name from input file name.
        file_dir, in_file_name = os.path.split(file_path)
        file_name = in_file_name[:-4] + "_converted.xml"
        out_file_path = os.path.sep.join((file_dir, file_name))
        if os.path.exists(out_file_path):
            os.remove(out_file_path)

        print("converting file: " + file_path + " ,(YAML->XML)...")
        with open(file_path, "r") as f:
            yml_tree = yaml.load(f, Loader=yaml.Loader)

        ### convert back to old naming scheme ###
        nameChange_dict = {}
        for yml_node in yml_tree:
            temp_name = yml_node

            temp_name = re.sub("M1", "Sph", temp_name)
            temp_name = re.sub("M2", "Sec", temp_name)
            temp_name = re.sub("Seg", "Mirror", temp_name)

            if temp_name[11] == "0":
                temp_name = temp_name[0:11:] + temp_name[11 + 1 : :]
            temp_name = re.sub("R1", "", temp_name)
            temp_name = re.sub("R2", "", temp_name)
            temp_name += "_Align"  # re-append "_Align"
            if self.whichRich == 2:
                temp_name = "Rich2" + temp_name  # re-prepend "Rich2"

            nameChange_dict[yml_node] = temp_name  # store name change

        # apply name changes
        for old_yml_node, new_yml_node in nameChange_dict.items():
            yml_tree[new_yml_node] = yml_tree.pop(old_yml_node)

        xml_root = etree.Element("root")
        xml_tree = etree.ElementTree(xml_root)
        # Conversion
        for yml_node in yml_tree:
            if self.blocklist.check(yml_node) == False:
                xml_node = etree.SubElement(xml_root, "condition")
                xml_node.set("classID", "6")

                xml_node.set("name", yml_node)
                for yml_subnode in yml_tree[yml_node]:
                    xml_subnode = etree.SubElement(xml_node, "paramVector")
                    xml_subnode_name = self.conversions.get_name_xml(yml_subnode)
                    xml_subnode.set("name", xml_subnode_name)
                    xml_subnode.set("type", "double")
                    # Counter is used to log the number of 0 entries
                    xml_data = []
                    counter = 0
                    for yml_datapoint in yml_tree[yml_node][yml_subnode]:
                        try:
                            yml_value, yml_unit = yml_datapoint.split("*")
                            yml_unit = yml_unit.strip()
                            yml_value = yml_value.strip()
                        except:
                            yml_value = yml_datapoint.strip()
                            yml_unit = False
                        sig_figs = obj.find_sigfigs(yml_value)
                        yml_value = float(yml_value)
                        if yml_unit == False:
                            xml_unit = self.conversions.get_unit_xml(xml_subnode_name)
                            yml_unit = self.conversions.get_unit_yml(yml_subnode)
                            yml_value = self.conversions.convert_unit(
                                yml_value, yml_unit, xml_unit
                            )
                        else:
                            xml_unit = yml_unit
                        # Format text output
                        if xml_unit == "mrad":
                            yml_value = np.format_float_positional(
                                yml_value,
                                precision=5,
                                fractional=True,
                                trim="k",
                                min_digits=5,
                            )
                            if yml_value == "0.00000":
                                counter += 1
                        elif sig_figs == 0:
                            yml_value = np.format_float_positional(
                                yml_value, precision=1, fractional=False, trim="-"
                            )
                        else:
                            yml_value = np.format_float_positional(
                                yml_value,
                                precision=sig_figs,
                                fractional=False,
                                trim="0",
                            )
                        xml_datapoint = "*".join((str(yml_value), xml_unit))
                        xml_data.append(xml_datapoint)
                    if counter == 3:
                        xml_datapoint = "*".join(("0", xml_unit))
                        text = "{0} {0} {0}".format(xml_datapoint)
                    elif xml_unit == "mrad":
                        text = (
                            xml_data[0].rjust(15)
                            + xml_data[1].rjust(16)
                            + xml_data[2].rjust(16)
                        )
                    else:
                        text = "{} {} {}".format(xml_data[0], xml_data[1], xml_data[2])
                    xml_subnode.text = text

        # Write to file and remove root yml_node.
        if reAdd_blocklisted_conditions == True:
            # read blocklisted list file from input file name.
            try:
                blisted_file_path = os.path.sep.join(
                    (
                        self.blistedCondsPath,
                        in_file_name[:-4].split(".")[0] + "_blisted_nodes.txt",
                    )
                )
            except:
                raise ImportError(
                    "File with blocklisted conditions (when converting from xml to yaml) not found. \n"
                    + "Have you run the xml->yaml converter at some point (to generate the corresponding blocklist file)?\n "
                    + "Have you checked that self.blistedCondsPath is correct?.\n"
                    + "Have you set blisted_write_file to False when converting from XML->YAML?"
                )

            with open(blisted_file_path, "r") as f:
                blisted_content = f.read()

        with open(out_file_path, "wb") as f:
            f.write(etree.tostring(xml_tree, pretty_print=True))
        with open(out_file_path, "r") as f:
            lines = f.readlines()
        lines = lines[1:]
        lines = lines[:-1]
        with open(out_file_path, "w") as f:
            f.writelines(lines)
            if reAdd_blocklisted_conditions:
                f.writelines(blisted_content)

        # tack on (or off) extra lines from yaml_tack_on_Rich_X.txt
        if type(tack_on) == bool:
            obj.tack_on_off(out_file_path, tack_on, self.configPath, self.whichRich)

        print("converted. output file: " + out_file_path)


if __name__ == "__main__":
    c = Conversion()
    c.conv_XML(r"/home/yq21049/Desktop/xml_yaml/Data/v40-R1_master.xml")
    c.conv_YAML(r"/home/yq21049/Desktop/xml_yaml/Data/v40-R1_master_converted.yml")

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__authors__ = "Jake Reich, Claire Prouve, Paras Naik"
__copyright__ = "University of Bristol, June 2023"

import getpass
import sys

import numpy
from GaudiPython import gbl
from ROOT import (
    TF1,
    TH1D,
    TH2D,
    TCanvas,
    TExec,
    TFile,
    TGraph,
    TLine,
    TMultiGraph,
    gStyle,
)
from ruamel.yaml import YAML


class AlignMonitor:
    def __init__(self, _alignConf, _maxIt, flat=False):
        self.alignConf = _alignConf
        self.nameStr = self.alignConf.getProp("nameStr")
        self.whichRich = self.alignConf.getProp("Rich")
        self.workdir = self.alignConf.getProp("WorkDir")
        self.HistoDir = self.alignConf.getProp(
            "HistoDir"
        )  # PN: This will point to the directory to where we send the ROOT file with histograms for Monet
        self.runNumber = self.alignConf.getProp(
            "runNumber"
        )  # earliest run  in fill (identifies the monet plots)
        self.warningFactor = self.alignConf.getProp("warningFactor")
        self.maxIt = _maxIt  # the maximum labeled number of the iterations that we are looping through
        yaml = YAML()
        with open(
            self.workdir + "/" + "rich" + str(self.whichRich) + "_variant.yml"
        ) as finp:
            variant = yaml.load(finp)
        self.prename = variant + "_histos_"
        # RICH1 settings
        self.maxpri = 3
        self.binToPriMirr = [[2, 3], [0, 1]]
        self.maxsec = 15
        self.binToSecMirr = [
            [10, 11, 14, 15],
            [8, 9, 12, 13],
            [2, 3, 6, 7],
            [0, 1, 4, 5],
        ]
        self.fitMin = -0.007
        self.fitMax = 0.007
        # if RICH2, use RICH2 settings
        if self.whichRich == 2:
            self.maxpri = 55
            self.binToPriMirr = [
                [3, 2, 1, 0, 31, 30, 29, 28],
                [7, 6, 5, 4, 35, 34, 33, 32],
                [11, 10, 9, 8, 39, 38, 37, 36],
                [15, 14, 13, 12, 43, 42, 41, 40],
                [19, 18, 17, 16, 47, 46, 45, 44],
                [23, 22, 21, 20, 51, 50, 49, 48],
                [27, 26, 25, 24, 55, 54, 53, 52],
            ]
            self.maxsec = 39
            self.binToSecMirr = [
                [3, 2, 1, 0, 23, 22, 21, 20],
                [7, 6, 5, 4, 27, 26, 25, 24],
                [11, 10, 9, 8, 31, 30, 29, 28],
                [15, 14, 13, 12, 35, 34, 33, 32],
                [19, 18, 17, 16, 39, 38, 37, 36],
            ]
            self.fitMin = -0.0039
            self.fitMax = 0.0035
        self.flat = flat

        # Commented out stuff below is for the Run 2 Monet setup
        """
        if self.flat is False:
            # Initialize the Monitoring Job "Sender" to send plots to the presenter
            from Configurables import MonitoringJob
            from Monitoring.MonitoringJob import start
            self.mj = MonitoringJob()
            self.mj.JobName = "MoniOnlineAlig"
            print("INFO: monitoring_job = " + self.mj.JobName)
            self.mj.Sender = True
            self.mj.Saver = False
            self.gaudi, self.monSvc = start()
            self.monitoring_folder = 'MoniOnlineAligRich' + str(self.whichRich)
            print("INFO: monitoring_folder = " + self.monitoring_folder)
        """

        sys.stdout.flush()
        # The Monitoring Job "Saver" is part of a monitoring script by Giulio Dujany (elsewhere)
        #   That monitoring script is always being run at the pit by the Alignment group
        self.isInsane = False
        self.finalCKres = None
        self.finalCKresErr = None

    def performMonitoring(self, compareFILE, forcedUpdate_ChMagPol):
        import os

        gStyle.SetOptFit(1111)
        gStyle.SetOptStat(000000000)

        # File for the RICH piquet AND for the RICH mirror alignment experts
        expertFile = self.workdir + "Rich" + str(self.whichRich) + "_AlignSummary.pdf"
        # File for the Alignment piquet ("duplicating" what goes to the Presenter for the Data Manager [except CK angle res stuff])
        monitorFile = self.workdir + "Rich" + str(self.whichRich) + "_AlignMonitor.pdf"
        if self.flat is False:
            # obtain file to store histograms for Monet
            monetFile = (
                self.HistoDir + "RichAlignMon-run" + str(self.runNumber) + ".root"
            )
            monetRootFile = TFile(monetFile, "UPDATE")

        theCanvas = TCanvas("theCanvas", "MirrAlign")
        theCanvas.Divide(2, 2, 0.001, 0.001)

        theCanvas2 = TCanvas("theCanvas2", "MirrAlignTest")
        theCanvas2.Divide(2, 2, 0.001, 0.001)

        ######### Start of 2D histos section
        # heatHistos

        from PyMirrAlignOnline.tiltObj import (
            tiltObj,  # PN: this should also work offline if running in a Panoptes environment
        )

        mirrtilts = []
        for n_it in range(0, self.maxIt + 1):
            mirrtilts.append(tiltObj(self.alignConf))
            # iterationFILE = self.workdir + 'Rich' + str(
            #    self.whichRich) + 'CondDBUpdate_' + self.nameStr + '_i' + str(
            #        n_it + 1)
            yaml = YAML()

            with open(
                self.workdir + "/" + "rich" + str(self.whichRich) + "_variant.yml"
            ) as finp:
                variant = yaml.load(finp)

            iterationFILE = self.workdir + variant + "_conds_i" + str(n_it + 1)
            # compare YAML to YAML, or XML to XML, depending on the format of compareFILE
            if ".yml" in compareFILE:
                iterationFILE += ".yml"
                # fills mirrtilts with the exact change in mirror compensation from compareFILE to iterationFILE
                mirrtilts[n_it].setChangeYAML([compareFILE, iterationFILE])
            else:
                iterationFILE += ".xml"
                # fills mirrtilts with the exact change in mirror compensation from compareFILE to iterationFILE
                mirrtilts[n_it].setChange([compareFILE, iterationFILE])
        if self.whichRich == 1:
            heatHistoPriY = TH2D(
                "heatHistoPriY",
                "RICH"
                + str(self.whichRich)
                + " Primary mirrors local Y rotation (mrad)",
                2,
                -1,
                1,
                2,
                -1,
                1,
            )
            heatHistoPriZ = TH2D(
                "heatHistoPriZ",
                "RICH"
                + str(self.whichRich)
                + " Primary mirrors local Z rotation (mrad)",
                2,
                -1,
                1,
                2,
                -1,
                1,
            )
            heatHistoSecY = TH2D(
                "heatHistoSecY",
                "RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Y rotation (mrad)",
                4,
                -1,
                1,
                4,
                -1,
                1,
            )
            heatHistoSecZ = TH2D(
                "heatHistoSecZ",
                "RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Z rotation (mrad)",
                4,
                -1,
                1,
                4,
                -1,
                1,
            )
            numbersHistoPri = TH2D(
                "numbersHistoPri",
                "RICH" + str(self.whichRich) + " Primary mirror numbers",
                2,
                -1,
                1,
                2,
                -1,
                1,
            )
            numbersHistoSec = TH2D(
                "numbersHistoSec",
                "RICH" + str(self.whichRich) + " Secondary mirror numbers",
                4,
                -1,
                1,
                4,
                -1,
                1,
            )

        if self.whichRich == 2:
            heatHistoPriY = TH2D(
                "heatHistoPriY",
                "RICH"
                + str(self.whichRich)
                + " Primary mirrors local Y rotation (mrad)",
                8,
                -1,
                1,
                7,
                -1,
                1,
            )
            heatHistoPriZ = TH2D(
                "heatHistoPriZ",
                "RICH"
                + str(self.whichRich)
                + " Primary mirrors local Z rotation (mrad)",
                8,
                -1,
                1,
                7,
                -1,
                1,
            )
            heatHistoSecY = TH2D(
                "heatHistoSecY",
                "RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Y rotation (mrad)",
                8,
                -1,
                1,
                5,
                -1,
                1,
            )
            heatHistoSecZ = TH2D(
                "heatHistoSecZ",
                "RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Z rotation (mrad)",
                8,
                -1,
                1,
                5,
                -1,
                1,
            )
            numbersHistoPri = TH2D(
                "numbersHistoPri",
                "RICH" + str(self.whichRich) + " Primary mirror numbers",
                8,
                -1,
                1,
                7,
                -1,
                1,
            )
            numbersHistoSec = TH2D(
                "numbersHistoSec",
                "RICH" + str(self.whichRich) + " Secondary mirror numbers",
                8,
                -1,
                1,
                5,
                -1,
                1,
            )

        xTitle = "relative global X position (not to scale)"
        yTitle = "relative global Y position (not to scale)"
        heatHistoPriY.SetXTitle(xTitle)
        heatHistoPriY.SetYTitle(yTitle)
        heatHistoSecY.SetXTitle(xTitle)
        heatHistoSecY.SetYTitle(yTitle)
        heatHistoPriZ.SetXTitle(xTitle)
        heatHistoPriZ.SetYTitle(yTitle)
        heatHistoSecZ.SetXTitle(xTitle)
        heatHistoSecZ.SetYTitle(yTitle)
        numbersHistoPri.SetXTitle(xTitle)
        numbersHistoPri.SetYTitle(yTitle)
        numbersHistoSec.SetXTitle(xTitle)
        numbersHistoSec.SetYTitle(yTitle)

        self.origMarkerSize = (
            heatHistoPriY.GetMarkerSize()
        )  # default MarkerSize should be same for all histos

        # Flexible Range Color scheme before 2017 June 19
        #   self.colorsLow  = [923,921,0,390,622,624,627,631,636,635,634,633,632,807,797,800,400,416]
        #   self.colorsHigh = [416,432,867,857,600,616,617,618,619,620,615,611,608,606,590,920,922,1]
        # Flexible Range Improved Color scheme after 2017 June 19
        # 634 = kRed + 2
        # 632 = kRed
        # 797 = kOrange - 3
        # 400 = kYellow
        # 416 = kGreen
        # 867 = kAzure + 7
        # 857 = kAzure - 3
        # 600 = kBlue
        # 602 = kBlue + 2
        self.colorsLow = [
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
        ]
        self.colorsLow += [
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            602,
            600,
            857,
            867,
            416,
        ]
        self.colorsHigh = [
            416,
            400,
            797,
            632,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
        ]
        self.colorsHigh += [
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
            634,
        ]
        colorsListFlex = self.colorsLow + self.colorsHigh

        #########

        # We need to get a mechanism to get the stopTolerances used if stopToleranceMode is 0
        # The best option, for consistency, is to calculate them in
        #    RichMirrCombinFit and/or RichMirrAlign, then print them to a file and
        #    pick them up in RichMirrAlign and/or RichMirrorAlignmentOnline (via setProp).
        # For now, we just use the stopTolerances from the Configuration in all cases.
        self.sTPY = float(self.alignConf.getProp("stopTolerancePriY"))
        self.sTPZ = float(self.alignConf.getProp("stopTolerancePriZ"))
        self.sTSY = float(self.alignConf.getProp("stopToleranceSecY"))
        self.sTSZ = float(self.alignConf.getProp("stopToleranceSecZ"))
        biggestTilt = [None] * (self.maxIt + 1)

        for n_it in reversed(range(0, self.maxIt + 1)):
            # Compensations Histograms
            # sets priYtrunc, etc... otherwise these will be empty
            mirrtilts[n_it].setTrunc(digits=2)

            rowsPri = len(self.binToPriMirr)
            colsPri = len(self.binToPriMirr[0])
            for row in range(rowsPri):
                for col in range(colsPri):
                    # truncated, not rounded
                    heatHistoPriY.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].priYtrunc[self.binToPriMirr[row][col]],
                    )
                    heatHistoPriZ.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].priZtrunc[self.binToPriMirr[row][col]],
                    )
                    numbersHistoPri.SetBinContent(
                        col + 1,
                        row + 1,
                        self.binToPriMirr[row][col]
                        if self.binToPriMirr[row][col] != 0
                        else 0.0000001,
                    )

            rowsSec = len(self.binToSecMirr)
            colsSec = len(self.binToSecMirr[0])
            for row in range(rowsSec):
                for col in range(colsSec):
                    # truncated, not rounded
                    heatHistoSecY.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].secYtrunc[self.binToSecMirr[row][col]],
                    )
                    heatHistoSecZ.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].secZtrunc[self.binToSecMirr[row][col]],
                    )
                    numbersHistoSec.SetBinContent(
                        col + 1,
                        row + 1,
                        self.binToSecMirr[row][col]
                        if self.binToSecMirr[row][col] != 0
                        else 0.0000001,
                    )

            # Get the absolute value of the maximum tilt in any plot,
            #   this lets us set the scales only as relevant,
            #   and in a controlled manner across all possible mirrors and tilts.
            biggestTilt[n_it] = mirrtilts[n_it].getAbsMax()

            # sanity check
            if n_it == self.maxIt:
                if not forcedUpdate_ChMagPol:
                    crazy = self.warningFactor
                else:
                    # PN: Note to self... we may want to de-hardcode expandCrazyFactor.
                    expandCrazyFactor = 2  # The choice of expandCrazyFactor is completely arbitrary, and should be re-evaluated later.
                    crazy = expandCrazyFactor * self.warningFactor
                if (
                    biggestTilt[n_it][0] >= crazy * self.sTPY
                    or biggestTilt[n_it][1] >= crazy * self.sTPZ
                    or biggestTilt[n_it][2] >= crazy * self.sTSY
                    or biggestTilt[n_it][3] >= crazy * self.sTSZ
                ):
                    self.isInsane = True

            # Flexible range plotting style
            for self.nFlex in reversed(range(0, 35)):
                if (
                    biggestTilt[n_it][0] >= self.nFlex * self.sTPY
                    or biggestTilt[n_it][1] >= self.nFlex * self.sTPZ
                    or biggestTilt[n_it][2] >= self.nFlex * self.sTSY
                    or biggestTilt[n_it][3] >= self.nFlex * self.sTSZ
                ):
                    break
            # max: heatHistoPriY.GetZaxis().SetRangeUser(-36*self.sTPY, 36*self.sTPY)
            heatHistoPriY.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - self.sTPY), (self.nFlex + 2) * (self.sTPY)
            )
            heatHistoPriZ.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - self.sTPZ), (self.nFlex + 2) * (self.sTPZ)
            )
            heatHistoSecY.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - self.sTSY), (self.nFlex + 2) * (self.sTSY)
            )
            heatHistoSecZ.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - self.sTSZ), (self.nFlex + 2) * (self.sTSZ)
            )
            colorsLowClone = list(self.colorsLow)
            colorsLowClone.reverse()
            colorsLowSubset = list(colorsLowClone[0 : self.nFlex + 2])
            colorsLowSubset.reverse()
            colorsListFlex = list(colorsLowSubset + self.colorsHigh[0 : self.nFlex + 2])
            levelsPY = []
            levelsPZ = []
            levelsSY = []
            levelsSZ = []
            # max: for i in range(-(36),(36)+1):
            for i in range(-(self.nFlex + 2), (self.nFlex + 2) + 1):
                levelsPY += [i * self.sTPY]
                levelsPZ += [i * self.sTPZ]
                levelsSY += [i * self.sTSY]
                levelsSZ += [i * self.sTSZ]
            gStyle.SetPalette(
                len(list(colorsListFlex)),
                numpy.array(list(colorsListFlex), dtype="intc"),
            )
            heatHistoPriY.SetContour(
                len(list(levelsPY)), numpy.array(list(levelsPY), dtype="float64")
            )
            heatHistoPriZ.SetContour(
                len(list(levelsPZ)), numpy.array(list(levelsPZ), dtype="float64")
            )
            heatHistoSecY.SetContour(
                len(list(levelsSY)), numpy.array(list(levelsSY), dtype="float64")
            )
            heatHistoSecZ.SetContour(
                len(list(levelsSZ)), numpy.array(list(levelsSZ), dtype="float64")
            )
            heatHistoPriY.SetMarkerSize(1.8 * self.origMarkerSize)
            heatHistoPriZ.SetMarkerSize(1.8 * self.origMarkerSize)
            heatHistoSecY.SetMarkerSize(1.8 * self.origMarkerSize)
            heatHistoSecZ.SetMarkerSize(1.8 * self.origMarkerSize)
            numbersHistoPri.SetMarkerSize(1.2 * self.origMarkerSize)
            numbersHistoSec.SetMarkerSize(1.2 * self.origMarkerSize)
            heatHistoPriY.SetBarOffset(-0.175)
            heatHistoPriZ.SetBarOffset(-0.175)
            heatHistoSecY.SetBarOffset(-0.175)
            heatHistoSecZ.SetBarOffset(-0.175)
            numbersHistoPri.SetBarOffset(0.175)
            numbersHistoSec.SetBarOffset(0.175)
            heatHistoPriY.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Primary mirrors local Y rotation (mrad) after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )
            heatHistoPriZ.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Primary mirrors local Z rotation (mrad) after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )
            heatHistoSecY.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Y rotation (mrad) after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )
            heatHistoSecZ.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Z rotation (mrad) after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )
            # we need TExec in order to have multiple text formats drawn on the same TPad
            execStyle1 = TExec(
                "execStyle1", 'gStyle->SetPaintTextFormat("4.2f");'
            )  # formerly 5.3f
            execStyle2 = TExec("execStyle2", 'gStyle->SetPaintTextFormat("2.0f");')

            theCanvas.cd(1)
            heatHistoPriY.Draw("COLZ")
            execStyle1.Draw()
            heatHistoPriY.Draw("TEXT SAME")
            theCanvas.cd(2)
            heatHistoPriZ.Draw("COLZ")
            execStyle1.Draw()
            heatHistoPriZ.Draw("TEXT SAME")
            theCanvas.cd(3)
            heatHistoSecY.Draw("COLZ")
            execStyle1.Draw()
            heatHistoSecY.Draw("TEXT SAME")
            theCanvas.cd(4)
            heatHistoSecZ.Draw("COLZ")
            execStyle1.Draw()
            heatHistoSecZ.Draw("TEXT SAME")

            theCanvas.cd(1)
            execStyle2.Draw()
            numbersHistoPri.Draw("TEXT SAME")
            theCanvas.cd(2)
            execStyle2.Draw()
            numbersHistoPri.Draw("TEXT SAME")
            theCanvas.cd(3)
            execStyle2.Draw()
            numbersHistoSec.Draw("TEXT SAME")
            theCanvas.cd(4)
            execStyle2.Draw()
            numbersHistoSec.Draw("TEXT SAME")

            theCanvas.Print(expertFile + "(")

            #############

            # Compensations-divided-by-tolerances Histograms
            # sets priYdiv, etc... otherwise these will be empty
            mirrtilts[n_it].setDiv(
                tolerances=[self.sTPY, self.sTPZ, self.sTSY, self.sTSZ]
            )

            rowsPri = len(self.binToPriMirr)
            colsPri = len(self.binToPriMirr[0])
            for row in range(rowsPri):
                for col in range(colsPri):
                    # truncated, not rounded
                    heatHistoPriY.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].priYdiv[self.binToPriMirr[row][col]],
                    )
                    heatHistoPriZ.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].priZdiv[self.binToPriMirr[row][col]],
                    )

            rowsSec = len(self.binToSecMirr)
            colsSec = len(self.binToSecMirr[0])
            for row in range(rowsSec):
                for col in range(colsSec):
                    # truncated, not rounded
                    heatHistoSecY.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].secYdiv[self.binToSecMirr[row][col]],
                    )
                    heatHistoSecZ.SetBinContent(
                        col + 1,
                        row + 1,
                        mirrtilts[n_it].secZdiv[self.binToSecMirr[row][col]],
                    )

            # Flexible range plotting style
            # self.nFlex should be unchanged from previous determination.
            # max: heatHistoPriY.GetZaxis().SetRangeUser(-36*1, 36*1)
            heatHistoPriY.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - 1), (self.nFlex + 2) * (1)
            )
            heatHistoPriZ.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - 1), (self.nFlex + 2) * (1)
            )
            heatHistoSecY.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - 1), (self.nFlex + 2) * (1)
            )
            heatHistoSecZ.GetZaxis().SetRangeUser(
                (self.nFlex + 2) * (0 - 1), (self.nFlex + 2) * (1)
            )
            levelsPY = []
            levelsPZ = []
            levelsSY = []
            levelsSZ = []
            # max: for i in range(-(36),(36)+1):
            for i in range(-(self.nFlex + 2), (self.nFlex + 2) + 1):
                levelsPY += [i * 1]
                levelsPZ += [i * 1]
                levelsSY += [i * 1]
                levelsSZ += [i * 1]
            gStyle.SetPalette(
                len(list(colorsListFlex)),
                numpy.array(list(colorsListFlex), dtype="intc"),
            )
            heatHistoPriY.SetContour(
                len(list(levelsPY)), numpy.array(list(levelsPY), dtype="float64")
            )
            heatHistoPriZ.SetContour(
                len(list(levelsPZ)), numpy.array(list(levelsPZ), dtype="float64")
            )
            heatHistoSecY.SetContour(
                len(list(levelsSY)), numpy.array(list(levelsSY), dtype="float64")
            )
            heatHistoSecZ.SetContour(
                len(list(levelsSZ)), numpy.array(list(levelsSZ), dtype="float64")
            )
            heatHistoPriY.SetMarkerSize(1.8 * self.origMarkerSize)
            heatHistoPriZ.SetMarkerSize(1.8 * self.origMarkerSize)
            heatHistoSecY.SetMarkerSize(1.8 * self.origMarkerSize)
            heatHistoSecZ.SetMarkerSize(1.8 * self.origMarkerSize)
            numbersHistoPri.SetMarkerSize(1.2 * self.origMarkerSize)
            numbersHistoSec.SetMarkerSize(1.2 * self.origMarkerSize)
            heatHistoPriY.SetBarOffset(-0.175)
            heatHistoPriZ.SetBarOffset(-0.175)
            heatHistoSecY.SetBarOffset(-0.175)
            heatHistoSecZ.SetBarOffset(-0.175)
            numbersHistoPri.SetBarOffset(0.175)
            numbersHistoSec.SetBarOffset(0.175)
            heatHistoPriY.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Primary mirrors local Y rotation divided by tolerance, after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )
            heatHistoPriZ.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Primary mirrors local Z rotation divided by tolerance, after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )
            heatHistoSecY.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Y rotation divided by tolerance, after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )
            heatHistoSecZ.SetTitle(
                "#splitline{RICH"
                + str(self.whichRich)
                + " Secondary mirrors local Z rotation divided by tolerance, after It. "
                + str(n_it)
                + "}{Values truncated, not rounded. Mirror number displayed smaller.}"
            )

            gStyle.SetPaintTextFormat("2.0f")
            theCanvas.cd(1)
            heatHistoPriY.Draw("COLZ")
            heatHistoPriY.Draw("TEXT SAME")
            numbersHistoPri.Draw("TEXT SAME")
            theCanvas.cd(2)
            heatHistoPriZ.Draw("COLZ")
            heatHistoPriZ.Draw("TEXT SAME")
            numbersHistoPri.Draw("TEXT SAME")
            theCanvas.cd(3)
            heatHistoSecY.Draw("COLZ")
            heatHistoSecY.Draw("TEXT SAME")
            numbersHistoSec.Draw("TEXT SAME")
            theCanvas.cd(4)
            heatHistoSecZ.Draw("COLZ")
            heatHistoSecZ.Draw("TEXT SAME")
            numbersHistoSec.Draw("TEXT SAME")

            if n_it == self.maxIt:
                theCanvas.Print(monitorFile)

                if self.flat is False:
                    monetRootFile.WriteTObject(
                        heatHistoPriY, f"RICH{self.whichRich}_heatHistoPriY"
                    )
                    monetRootFile.WriteTObject(
                        heatHistoPriZ, f"RICH{self.whichRich}_heatHistoPriZ"
                    )
                    monetRootFile.WriteTObject(
                        heatHistoSecY, f"RICH{self.whichRich}_heatHistoSecY"
                    )
                    monetRootFile.WriteTObject(
                        heatHistoSecZ, f"RICH{self.whichRich}_heatHistoSecZ"
                    )

                    # Commented out stuff below is for the Run 2 Monet setup
                    """
                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        heatHistoPriY.Clone(),
                        add=False)
                    print("INFO: heatHistoPriY sent to Monitoring.")
                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        heatHistoPriZ.Clone(),
                        add=False)
                    print("INFO: heatHistoPriZ sent to Monitoring.")
                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        heatHistoSecY.Clone(),
                        add=False)
                    print("INFO: heatHistoSecY sent to Monitoring.")
                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        heatHistoSecZ.Clone(),
                        add=False)
                    print("INFO: heatHistoSecZ sent to Monitoring.")

                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        numbersHistoPri.Clone("numbersHistoPriY"),
                        add=False)
                    print("INFO: numbersHistoPriY sent to Monitoring.")
                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        numbersHistoPri.Clone("numbersHistoPriZ"),
                        add=False)
                    print("INFO: numbersHistoPriZ sent to Monitoring.")
                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        numbersHistoSec.Clone("numbersHistoSecY"),
                        add=False)
                    print("INFO: numbersHistoSecY sent to Monitoring.")
                    self.monSvc.publishHistogram(
                        self.monitoring_folder,
                        numbersHistoSec.Clone("numbersHistoSecZ"),
                        add=False)
                    print("INFO: numbersHistoSecZ sent to Monitoring.")
                    """

                sys.stdout.flush()

        #############

        # End of 2D histos section #############

        theCanvas2.Clear()
        theCanvas.Clear()

        # generate resHistograms

        if self.maxIt == 0:
            pass
        elif self.maxIt < 2:
            theCanvas.Divide(2, 1, 0.0025, 0.0025)
        elif self.maxIt < 3:
            theCanvas.Divide(3, 1, 0.0025, 0.0025)
        elif self.maxIt < 4:
            theCanvas.Divide(2, 2, 0.0025, 0.0025)
        elif self.maxIt < 6:
            theCanvas.Divide(3, 2, 0.0025, 0.0025)
        elif self.maxIt < 8:
            theCanvas.Divide(4, 2, 0.0025, 0.0025)
        elif self.maxIt < 9:
            theCanvas.Divide(3, 3, 0.0025, 0.0025)
        else:
            theCanvas.Divide(4, 4, 0.0025, 0.0025)

        # reserve spaces for resHistograms
        resHistograms = [None] * (self.maxIt + 1)
        polbkg = [None] * (self.maxIt + 1)
        histFunc = [None] * (self.maxIt + 1)
        hist = [None] * (self.maxIt + 1)
        filename = [None] * (self.maxIt + 1)
        thisFile = [None] * (self.maxIt + 1)
        title = [None] * (self.maxIt + 1)
        fitRes = [None] * (self.maxIt + 1)

        default_column_histo_dict = {
            "resHistograms": [None] * (self.maxIt + 1),
            "polbkg": [None] * (self.maxIt + 1),
            "histFunc": [None] * (self.maxIt + 1),
            "hist": [None] * (self.maxIt + 1),
            "fitRes": [None] * (self.maxIt + 1),
            "resHistoTrend": TH1D(
                "resHistoTrend",
                "RICH"
                + str(self.whichRich)
                + " Cherenkov angle resolution (mrad) per It. ",
                self.maxIt + 1,
                -0.5,
                self.maxIt + 0.5,
            ),
        }

        import copy

        histos = {  # all PMTs in RICH1 are small (R-type)
            "RICH1": {
                "columns": {"top": {}, "bottom": {}},
                "allCkres": copy.deepcopy(default_column_histo_dict),
                "dThetavsPhi": {"top": None, "bottom": None},
            },
            "RICH2": {
                "columns": {"ASide-left": {}, "CSide-right": {}},
                "columns_Rtype": {"ASide-left": {}, "CSide-right": {}},
                "columns_Htype": {"ASide-left": {}, "CSide-right": {}},
                "Rtype": copy.deepcopy(default_column_histo_dict),
                "Htype": copy.deepcopy(default_column_histo_dict),
                "allCkres": copy.deepcopy(default_column_histo_dict),
                "dThetavsPhi": {"ASide-left": None, "CSide-right": None},
            },
        }

        # define arrays with column numbers for each RICH
        column_nums = {
            "RICH1": [i for i in range(0, 11)],
            "RICH2": [i for i in range(1, 13)],
        }

        # define panels names
        panel_names = (
            ["top", "bottom"] if self.whichRich == 1 else ["ASide-left", "CSide-right"]
        )
        # create histos for each column
        for column_num in column_nums[f"RICH{self.whichRich}"]:
            for panel_name in panel_names:
                histos[f"RICH{self.whichRich}"]["columns"][panel_name][
                    f"col{column_num}"
                ] = copy.deepcopy(default_column_histo_dict)
                if self.whichRich == 2:
                    histos[f"RICH{self.whichRich}"]["columns_Rtype"][panel_name][
                        f"col{column_num}"
                    ] = copy.deepcopy(default_column_histo_dict)
                    histos[f"RICH{self.whichRich}"]["columns_Htype"][panel_name][
                        f"col{column_num}"
                    ] = copy.deepcopy(default_column_histo_dict)

        # define placeholder_histogram
        placeholder_histogram = TH1D("placeholder", "placeholder", 100, 0, 1)
        placeholder_histogram.SetStats(0)

        for j in reversed(range(0, self.maxIt + 1)):
            filename[j] = self.workdir + "/" + self.prename + "i" + str(j) + ".root"

            if os.path.exists(filename[j]):
                thisFile[j] = TFile(filename[j], "read")

                def plot_ckres(hist_path, hist_dict, j):
                    try:
                        hist_dict["hist"][j] = thisFile[j].Get(hist_path).Clone()
                    except:
                        print(
                            f"INFO: {hist_path} not produced in {filename[j]} . Skipping..."
                        )
                        return None
                    title[j] = hist_dict["hist"][j].GetTitle() + " It. " + str(j)

                    # check if enough entries to perform fit
                    integral_threshold = 1100.0
                    integral = hist_dict["hist"][j].Integral()
                    if integral < integral_threshold:
                        print("INFO: " + title[j] + ":")
                        print(
                            "INFO: Not enough entries in the histogram to perform the fit."
                        )
                        hist_dict["hist"][j].SetTitle(
                            title[j] + "(NOT ENOUGH ENTRIES TO FIT)"
                        )
                        return hist_dict["hist"][j]

                    if (
                        f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                        == hist_path
                    ):
                        if (j == 0) and (j == self.maxIt):
                            hist_dict["hist"][j].SetName("resHisto0")

                            # Commented out stuff below is for the Run 2 Monet setup
                            """
                            if self.flat is False:
                                self.monSvc.publishHistogram(
                                    self.monitoring_folder, hist_dict["hist"][j].Clone(), add=False)
                                print("INFO: resHisto0 sent to Monitoring.")
                            """

                            hist_dict["hist"][j].SetName("resHistoN")

                            # Commented out stuff below is for the Run 2 Monet setup
                            """
                            if self.flat is False:
                                self.monSvc.publishHistogram(
                                    self.monitoring_folder, hist_dict["hist"][j].Clone(), add=False)
                                print("INFO: resHistoN sent to Monitoring.")
                            """

                        elif j == self.maxIt:
                            hist_dict["hist"][j].SetName("resHistoN")

                            # Commented out stuff below is for the Run 2 Monet setup
                            """
                            if self.flat is False:
                                self.monSvc.publishHistogram(
                                    self.monitoring_folder, hist_dict["hist"][j].Clone(), add=False)
                                print("INFO: resHistoN sent to Monitoring.")
                            """

                        elif j == 0:
                            hist_dict["hist"][j].SetName("resHisto0")

                            # Commented out stuff below is for the Run 2 Monet setup
                            """
                            if self.flat is False:
                                self.monSvc.publishHistogram(
                                    self.monitoring_folder, hist_dict["hist"][j].Clone(), add=False)
                                print("INFO: resHisto0 sent to Monitoring.")
                            """

                    if j != 0:
                        hist_dict["hist"][j].SetName("resHisto" + str(j))

                    hist_dict["fitRes"][j], hist_dict["resHistograms"][j] = (
                        self.fitCherenkovAngle(hist_dict["hist"][j])
                    )

                    hist_dict["histFunc"][j] = hist_dict["fitRes"][j].overallFitFunc
                    hist_dict["polbkg"][j] = hist_dict["fitRes"][j].bkgFitFunc

                    ckres = hist_dict["histFunc"][j].GetParameter(2) * 1000
                    ckres_error = hist_dict["histFunc"][j].GetParError(2) * 1000

                    hist_dict["resHistoTrend"].SetBinContent(j + 1, ckres)
                    hist_dict["resHistoTrend"].SetBinError(j + 1, ckres_error)
                    if (
                        f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                        == hist_path
                    ):
                        print("INFO: " + title[j] + ":")
                        print(
                            "INFO: _i"
                            + str(j)
                            + " CK angle resolution:     "
                            + str(ckres)
                            + " mrad."
                        )
                        print(
                            "INFO: _i"
                            + str(j)
                            + " CK angle resolution err: "
                            + str(ckres_error)
                            + " mrad."
                        )
                    else:
                        print("INFO: subselection " + title[j] + ":")
                        print(
                            "INFO: subselection _i"
                            + str(j)
                            + " CK angle resolution:     "
                            + str(ckres)
                            + " mrad."
                        )
                        print(
                            "INFO: subselection _i"
                            + str(j)
                            + " CK angle resolution err: "
                            + str(ckres_error)
                            + " mrad."
                        )

                    if (
                        j == self.maxIt
                        and f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                        == hist_path
                    ):
                        self.finalCKres = ckres
                        self.finalCKresErr = ckres_error

                    hist_dict["resHistograms"][j].SetTitle(title[j])
                    hist_dict["resHistograms"][j].SetXTitle(
                        r"#Delta#theta_{Cherenkov} (rad)"
                    )
                    hist_dict["resHistograms"][j].SetYTitle("Entries")

                    hist_dict["histFunc"][j].SetLineColor(2)
                    hist_dict["polbkg"][j].SetLineColor(4)

                    return None

                for hist_dict_name in histos[f"RICH{self.whichRich}"].keys():
                    if "columns" in hist_dict_name:
                        for column_num in column_nums[f"RICH{self.whichRich}"]:
                            for panel_name in panel_names:
                                if "Rtype" in hist_dict_name:
                                    hist_path = f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/{panel_name}/cols/RPMT/ckResAllCol{column_num}"
                                elif "Htype" in hist_dict_name:
                                    hist_path = f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/{panel_name}/cols/HPMT/ckResAllCol{column_num}"
                                else:
                                    hist_path = f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/{panel_name}/cols/ckResAllCol{column_num}"

                                # perform fit
                                try:
                                    res = plot_ckres(
                                        hist_path,
                                        histos[f"RICH{self.whichRich}"][hist_dict_name][
                                            panel_name
                                        ][f"col{column_num}"],
                                        j,
                                    )
                                    print(res)
                                except:
                                    res = None
                                # store histogram without fit
                                if res is not None:
                                    histos[f"RICH{self.whichRich}"][hist_dict_name][
                                        panel_name
                                    ][f"col{column_num}"]["resHistograms"][j] = res
                    elif "Rtype" in hist_dict_name:
                        hist_path = f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAllRTypePD"
                        res = plot_ckres(
                            hist_path,
                            histos[f"RICH{self.whichRich}"][hist_dict_name],
                            j,
                        )
                    elif "Htype" in hist_dict_name:
                        hist_path = f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAllHTypePD"
                        res = plot_ckres(
                            hist_path,
                            histos[f"RICH{self.whichRich}"][hist_dict_name],
                            j,
                        )
                    elif "allCkres" in hist_dict_name:  # allCkres
                        hist_path = (
                            f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                        )
                        res = plot_ckres(
                            hist_path,
                            histos[f"RICH{self.whichRich}"][hist_dict_name],
                            j,
                        )
                    elif "dThetavsPhi" in hist_dict_name:
                        # define primary and secondary mirrors
                        mirr_combs = {
                            "RICH1": {
                                "top": {
                                    "pri": ["00", "01"],
                                    "sec": [
                                        "00",
                                        "01",
                                        "02",
                                        "03",
                                        "04",
                                        "05",
                                        "06",
                                        "07",
                                    ],
                                },
                                "bottom": {
                                    "pri": ["02", "03"],
                                    "sec": [
                                        "08",
                                        "09",
                                        "10",
                                        "11",
                                        "12",
                                        "13",
                                        "14",
                                        "15",
                                    ],
                                },
                            },
                            "RICH2": {
                                "ASide-left": {
                                    "pri": ["{:02d}".format(num) for num in range(28)],
                                    "sec": ["{:02d}".format(num) for num in range(20)],
                                },
                                "CSide-right": {
                                    "pri": [
                                        "{:02d}".format(num) for num in range(28, 56)
                                    ],
                                    "sec": [
                                        "{:02d}".format(num) for num in range(20, 40)
                                    ],
                                },
                            },
                        }

                        # combine mirror comb histos for each panel
                        for panel in mirr_combs[f"RICH{self.whichRich}"].keys():
                            histos[f"RICH{self.whichRich}"]["dThetavsPhi"][panel] = (
                                TH2D(
                                    rf"#Delta#theta vs #phi (R{self.whichRich} {panel}, it {j})",
                                    rf"#Delta#theta vs #phi (R{self.whichRich} {panel}, it {j})",
                                    60,
                                    0,
                                    6.283185307179586,
                                    50,
                                    -0.012000000104308128
                                    if self.whichRich == 1
                                    else -0.004000000189989805,
                                    0.012000000104308128
                                    if self.whichRich == 1
                                    else 0.004000000189989805,
                                )
                            )

                            for pri in mirr_combs[f"RICH{self.whichRich}"][panel][
                                "pri"
                            ]:
                                for sec in mirr_combs[f"RICH{self.whichRich}"][panel][
                                    "sec"
                                ]:
                                    # check if this pri and sec combination is correct
                                    hist_path = f"RICH/MirrorAlignLong/Rich{self.whichRich}Gas/dThetaPhiRecSaturTrR{self.whichRich}p{pri}s{sec}"
                                    try:
                                        mirr_comb_hist = (
                                            thisFile[j].Get(hist_path).Clone()
                                        )
                                        histos[f"RICH{self.whichRich}"]["dThetavsPhi"][
                                            panel
                                        ].Add(mirr_comb_hist)
                                    except:
                                        pass  # this mirror combination doesn't exist in the root file

                            ## write to expert pdf
                            theCanvas2.cd()
                            gStyle.SetNumberContours(20)
                            histos[f"RICH{self.whichRich}"]["dThetavsPhi"][
                                panel
                            ].GetXaxis().SetTitle(rf"#phi (rad)")
                            histos[f"RICH{self.whichRich}"]["dThetavsPhi"][
                                panel
                            ].GetYaxis().SetTitle(rf"#Delta#theta (mrad)")
                            histos[f"RICH{self.whichRich}"]["dThetavsPhi"][panel].Draw(
                                "COLZ"
                            )
                            theCanvas2.Print(expertFile)

                            ## write to Monet root file
                            if self.flat is False:
                                if j == 0:
                                    monetRootFile.WriteTObject(
                                        histos[f"RICH{self.whichRich}"]["dThetavsPhi"][
                                            panel
                                        ],
                                        f"RICH{self.whichRich}_{hist_dict_name}_RICH{self.whichRich}_{panel}_It_{j}",
                                    )
                                if j == self.maxIt:
                                    monetRootFile.WriteTObject(
                                        histos[f"RICH{self.whichRich}"]["dThetavsPhi"][
                                            panel
                                        ],
                                        f"RICH{self.whichRich}_{hist_dict_name}_RICH{self.whichRich}_{panel}_It_final",
                                    )

                        theCanvas2.Clear()

                    ## add plots to pdfs
                    if "columns" in hist_dict_name:
                        theCanvas2.Divide(3, 4)

                        for panel_name in panel_names:
                            column_counter = (
                                0 if self.whichRich == 1 else 1
                            )  # RICH 2 has no 0th column, so we start it at 1st column
                            for irow in range(3):
                                for icol in range(4):
                                    theCanvas2.cd(
                                        irow * 4 + icol + 1
                                    )  # go to current pad

                                    # deal with cases where no column for the corresponding TPad index
                                    if (
                                        column_counter
                                        >= len(column_nums[f"RICH{self.whichRich}"])
                                        and self.whichRich == 1
                                    ):  # RICH 1 has no 11th column
                                        column_counter += 1
                                        placeholder_histogram.Draw()
                                        continue
                                    try:
                                        histos[f"RICH{self.whichRich}"][hist_dict_name][
                                            panel_name
                                        ][f"col{column_counter}"]["resHistograms"][
                                            j
                                        ].Draw()
                                    except:
                                        pass
                                    # only show fit, if there were enough events to perform the fit
                                    try:
                                        if (
                                            histos[f"RICH{self.whichRich}"][
                                                hist_dict_name
                                            ][panel_name][f"col{column_counter}"][
                                                "histFunc"
                                            ][j]
                                            is not None
                                        ):
                                            try:
                                                histos[f"RICH{self.whichRich}"][
                                                    hist_dict_name
                                                ][panel_name][f"col{column_counter}"][
                                                    "histFunc"
                                                ][j].Draw("SAME")
                                            except:
                                                pass
                                            try:
                                                histos[f"RICH{self.whichRich}"][
                                                    hist_dict_name
                                                ][panel_name][f"col{column_counter}"][
                                                    "polbkg"
                                                ][j].Draw("SAME")
                                            except:
                                                pass
                                    except:
                                        pass

                                    column_counter += 1

                            theCanvas2.Print(expertFile)

                        theCanvas2.Clear()

                    elif (
                        "resHistograms"
                        in histos[f"RICH{self.whichRich}"][hist_dict_name].keys()
                    ):
                        theCanvas2.Divide(1, 1)

                        # add to canvas
                        theCanvas2.cd()
                        histos[f"RICH{self.whichRich}"][hist_dict_name][
                            "resHistograms"
                        ][j].Draw()
                        histos[f"RICH{self.whichRich}"][hist_dict_name]["histFunc"][
                            j
                        ].Draw("SAME")
                        histos[f"RICH{self.whichRich}"][hist_dict_name]["polbkg"][
                            j
                        ].Draw("SAME")
                        theCanvas2.Print(expertFile)
                        theCanvas2.Clear()

                        # add first and last iteration ckresall to show in monet
                        if self.flat is False:
                            if j == 0:
                                monetRootFile.WriteTObject(
                                    histos[f"RICH{self.whichRich}"][hist_dict_name][
                                        "resHistograms"
                                    ][j],
                                    f"RICH{self.whichRich}_{hist_dict_name}_It_{j}",
                                )
                            if j == self.maxIt:
                                monetRootFile.WriteTObject(
                                    histos[f"RICH{self.whichRich}"][hist_dict_name][
                                        "resHistograms"
                                    ][j],
                                    f"RICH{self.whichRich}_{hist_dict_name}_It_final",
                                )

                ####### from fitCherenkovAngle_old  #################
                """
                resHistograms[j] = fitRes[j][0]
                resHistoTrend.SetBinContent(j + 1, fitRes[j][1] * 1000)
                resHistoTrend.SetBinError(j + 1, fitRes[j][2] * 1000)
                print("_i" + str(j) + " CK angle resolution:     " +
                    str(fitRes[j][1] * 1000) + " mrad.")
                print("_i" + str(j) + " CK angle resolution err: " +
                    str(fitRes[j][2] * 1000) + " mrad.")
                if j == self.maxIt:
                    self.finalCKres = fitRes[j][1] * 1000
                    self.finalCKresErr = fitRes[j][2] * 1000

                title[j] = 'RICH ' + str(
                    self.whichRich) + ' Cherenkov angle resolution It. ' + str(
                        j)
                resHistograms[j].SetTitle(title[j])
                resHistograms[j].SetXTitle('#Delta#theta_{Cherenkov} (rad)')
                resHistograms[j].SetYTitle('Entries')

                histFunc[j] = resHistograms[j].GetFunction(
                    "Rich" + str(self.whichRich) + "fFitF3")
                polbkg[j] = TF1("polBKG" + str(j), "pol3(0)", self.fitMin,
                                self.fitMax)
                polbkg[j].SetParameter(0, histFunc[j].GetParameter(3))
                polbkg[j].SetParameter(1, histFunc[j].GetParameter(4))
                polbkg[j].SetParameter(2, histFunc[j].GetParameter(5))
                polbkg[j].SetParameter(3, histFunc[j].GetParameter(6))
                polbkg[j].SetLineColor(4)

                theCanvas2.cd()
                resHistograms[j].Draw()
                polbkg[j].Draw("SAME")

                theCanvas2.Print(expertFile)

                theCanvas.cd()
                theCanvas.cd(1 + j)
                resHistograms[j].Draw()
                polbkg[j].Draw("SAME")
                """

        sys.stdout.flush()

        # Make resHistoTrend iteration trend plot
        for hist_dict_name in histos[f"RICH{self.whichRich}"].keys():
            if "columns" in hist_dict_name:
                for panel_name in panel_names:
                    for column_num in column_nums[f"RICH{self.whichRich}"]:
                        trendTitle = histos[f"RICH{self.whichRich}"][hist_dict_name][
                            panel_name
                        ][f"col{column_num}"]["resHistoTrend"].GetTitle()
                        histos[f"RICH{self.whichRich}"][hist_dict_name][panel_name][
                            f"col{column_num}"
                        ]["resHistoTrend"].SetTitle(
                            f"{trendTitle}, {hist_dict_name} ({column_num}), {panel_name}"
                        )
                        histos[f"RICH{self.whichRich}"][hist_dict_name][panel_name][
                            f"col{column_num}"
                        ]["resHistoTrend"].SetMarkerStyle(8)
                        histos[f"RICH{self.whichRich}"][hist_dict_name][panel_name][
                            f"col{column_num}"
                        ]["resHistoTrend"].SetXTitle("iteration number")
                        histos[f"RICH{self.whichRich}"][hist_dict_name][panel_name][
                            f"col{column_num}"
                        ]["resHistoTrend"].SetYTitle(
                            "#splitline{Cherenkov angle resolution (mrad)}{}"
                        )

                        theCanvas.Clear()
                        histos[f"RICH{self.whichRich}"][hist_dict_name][panel_name][
                            f"col{column_num}"
                        ]["resHistoTrend"].DrawCopy()

                        theCanvas.Print(expertFile)
            elif (
                "resHistoTrend"
                in histos[f"RICH{self.whichRich}"][hist_dict_name].keys()
            ):
                trendTitle = histos[f"RICH{self.whichRich}"][hist_dict_name][
                    "resHistoTrend"
                ].GetTitle()
                histos[f"RICH{self.whichRich}"][hist_dict_name][
                    "resHistoTrend"
                ].SetTitle(f"{trendTitle}, {hist_dict_name}")
                histos[f"RICH{self.whichRich}"][hist_dict_name][
                    "resHistoTrend"
                ].SetMarkerStyle(8)
                histos[f"RICH{self.whichRich}"][hist_dict_name][
                    "resHistoTrend"
                ].SetXTitle("iteration number")
                histos[f"RICH{self.whichRich}"][hist_dict_name][
                    "resHistoTrend"
                ].SetYTitle("#splitline{Cherenkov angle resolution (mrad)}{}")

                theCanvas.Clear()
                histos[f"RICH{self.whichRich}"][hist_dict_name][
                    "resHistoTrend"
                ].DrawCopy()

                if self.flat is False:
                    monetRootFile.WriteTObject(
                        histos[f"RICH{self.whichRich}"][hist_dict_name][
                            "resHistoTrend"
                        ],
                        f"RICH{self.whichRich}_{hist_dict_name}_resHistoTrend",
                    )

                theCanvas.Print(expertFile)

        # Commented out stuff below is for the Run 2 Monet setup
        """
        if self.flat is False:
            # PN: This should be written to ONLY send a CKres plot to Monet if the histo name is exactly ckResAll
            self.monSvc.publishHistogram(
                self.monitoring_folder, resHistoTrend.Clone(), add=False)
            print("INFO: resHistoTrend sent to Monitoring.")
        """

        sys.stdout.flush()

        # compensations graphs

        mgraphs = mirrtilts[self.maxIt].getGraphs()

        maxPriY = TLine(0, self.sTPY, self.maxpri, self.sTPY)
        minPriY = TLine(0, -self.sTPY, self.maxpri, -self.sTPY)
        maxPriZ = TLine(0, self.sTPZ, self.maxpri, self.sTPZ)
        minPriZ = TLine(0, -self.sTPZ, self.maxpri, -self.sTPZ)
        maxSecY = TLine(0, self.sTSY, self.maxsec, self.sTSY)
        minSecY = TLine(0, -self.sTSY, self.maxsec, -self.sTSY)
        maxSecZ = TLine(0, self.sTSZ, self.maxsec, self.sTSZ)
        minSecZ = TLine(0, -self.sTSZ, self.maxsec, -self.sTSZ)
        maxPriY.SetLineColor(2)  # red
        minPriY.SetLineColor(2)
        maxPriZ.SetLineColor(4)  # blue
        minPriZ.SetLineColor(4)
        maxSecY.SetLineColor(2)
        minSecY.SetLineColor(2)
        maxSecZ.SetLineColor(4)
        minSecZ.SetLineColor(4)

        gStyle.SetLineStyleString(20, "4 16")
        gStyle.SetLineStyleString(21, "8 16")
        maxPriY.SetLineStyle(20)
        minPriY.SetLineStyle(20)
        maxPriZ.SetLineStyle(21)
        minPriZ.SetLineStyle(21)
        maxSecY.SetLineStyle(20)
        minSecY.SetLineStyle(20)
        maxSecZ.SetLineStyle(21)
        minSecZ.SetLineStyle(21)

        theCanvas.Clear()
        tops = self.sTPZ
        if tops < self.sTPY:
            tops = self.sTPY
        if tops < biggestTilt[self.maxIt][0]:
            tops = biggestTilt[self.maxIt][0]
        if tops < biggestTilt[self.maxIt][1]:
            tops = biggestTilt[self.maxIt][1]
        mgraphs[0].SetMinimum(-(tops + 0.01))
        mgraphs[0].SetMaximum(tops + 0.01)
        mgraphs[0].Draw("AP")
        maxPriY.Draw("SAME")
        minPriY.Draw("SAME")
        maxPriZ.Draw("SAME")
        minPriZ.Draw("SAME")
        mgraphs[0].SetTitle(
            "RICH"
            + str(self.whichRich)
            + " final primary mirror compensations w.r.t. to comparison DB"
        )
        mgraphs[0].GetXaxis().SetTitle("primary mirror number")
        mgraphs[0].GetYaxis().SetTitle(
            "change in compensation (mrad) [{red, blue}: local {Y, Z}]"
        )

        theCanvas.Print(expertFile)

        theCanvas.Clear()
        tops = self.sTSZ
        if tops < self.sTSY:
            tops = self.sTSY
        if tops < biggestTilt[self.maxIt][2]:
            tops = biggestTilt[self.maxIt][2]
        if tops < biggestTilt[self.maxIt][3]:
            tops = biggestTilt[self.maxIt][3]
        mgraphs[1].SetMinimum(-(tops + 0.01))
        mgraphs[1].SetMaximum(tops + 0.01)
        mgraphs[1].Draw("AP")
        maxSecY.Draw("SAME")
        minSecY.Draw("SAME")
        maxSecZ.Draw("SAME")
        minSecZ.Draw("SAME")
        mgraphs[1].SetTitle(
            "RICH"
            + str(self.whichRich)
            + " final secondary mirror compensations w.r.t. to comparison DB"
        )
        mgraphs[1].GetXaxis().SetTitle("secondary mirror number")
        mgraphs[1].GetYaxis().SetTitle(
            "change in compensation (mrad) [{red, blue}: local {Y, Z}]"
        )

        theCanvas.Print(expertFile + ")")
        for n_it in range(0, self.maxIt + 1):
            # mirrtilts[self.maxIt].writeChange(self.maxIt + 1) # PN: This does not look right, let me try to fix:
            mirrtilts[n_it].writeChange(n_it + 1)
        mgraphs[0].Delete()
        mgraphs[1].Delete()

        if self.flat is False:
            monetRootFile.Close()
        # Commented out stuff below is for the Run 2 Monet setup
        """
        if self.flat is False:
            # Close the Monitoring Job; the histograms are now in the presenter (Page Editor Online).
            # New Histograms need to be added to the Histogram Database https://lbhistogramdb.cern.ch
            #   in order to be viewed at the pit.
            # This database is only accessible on the CERN network (or ssh to plus, start Firefox).
            # One can adjust the histo styles on the presenter view there,
            #   but not all normal ROOT options are available.
            self.gaudi.stop()
            self.gaudi.finalize()
        """

    def fitCherenkovAngle(self, hist):
        # get Chris Jones's C++ fitter
        fitter = gbl.Rich.Rec.CKResolutionFitter()

        # set signal (AsymNormal) and background (FreeNPol) models
        fitter.params().RichFitTypes[self.whichRich - 1].clear()
        fitter.params().RichFitTypes[self.whichRich - 1].push_back(
            "AsymNormal:FreeNPol"
        )

        # change fit ranges
        fitter.params().RichFitMin[0] = self.fitMin
        fitter.params().RichFitMin[1] = self.fitMin
        fitter.params().RichFitMax[0] = self.fitMax
        fitter.params().RichFitMax[1] = self.fitMax

        res = fitter.fit(hist, self.whichRich)

        return res, hist

    def fitCherenkovAngle_old(self, hist):
        richStr = "Rich" + str(self.whichRich)
        m_maxErrorForOK = 1e-3

        xPeak = hist.GetBinCenter(hist.GetMaximumBin())
        delta = 0
        if richStr == "Rich1":
            delta = 0.0025
        else:
            delta = 0.00105

        fitMin = xPeak - delta
        fitMax = xPeak + delta

        preFitFName = richStr + "PreFitF"
        preFitF = TF1(preFitFName, "gaus", fitMin, fitMax)
        preFitF.SetParameter(1, 0)

        if richStr == "Rich1":
            preFitF.SetParameter(2, 0.0015)
        else:
            preFitF.SetParameter(2, 0.0007)

        hist.Fit(preFitF, "RQ")
        fitOK = False

        lastFitF = preFitF
        bestFitF = None
        nPolFull = 3
        bestNPol = 0

        fFitF = None

        for nPol in range(1, nPolFull + 1):
            fFitFName = richStr + "fFitF" + str(nPol)
            fFitFType = "gaus(0)+pol" + str(nPol) + "(3)"
            fFitF = TF1(fFitFName, fFitFType, self.fitMin, self.fitMax)

            fFitF.SetParName(0, "Gaus Constant")
            fFitF.SetParName(1, "Gaus Mean")
            fFitF.SetParName(2, "Gaus Sigma")
            nParamsToSet = 3
            if nPol > 1:
                nParamsToSet = 3 + nPol

            for p in range(0, nParamsToSet):
                fFitF.SetParameter(p, lastFitF.GetParameter(p))

            hist.Fit(fFitF, "RFQM")
            lastFitF = fFitF
            fitOK = fFitF.GetParError(1) < m_maxErrorForOK
            if fitOK:
                bestFitF = fFitF
                bestNPol = nPol
            else:
                if nPol == nPolFull:
                    hist.Fit(fFitF, "RFSE0QM")
                    fitOK = True
                if nPol > 1:
                    break

        fitRes = [hist, fFitF.GetParameter(2), fFitF.GetParError(2)]
        return fitRes

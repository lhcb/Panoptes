Configuration.py contains the configurables and default values for the online RICH mirror alignment.

Currently alternate values are not provided via an external file.
Thus the default values in Configuration.py need to be changed everytime you would like to use alternative settings.

Unfortunately, it then it becomes slightly harder to keep track of configurations
(though one could look at the output to see what the the settings were)

So we have decided for now to keep track of Configurations by storing them in the directory SavedConfigurations.

To create a new configuration, copy Configuration.py to SavedConfigurations/20XX_Configurations/MyConfig.txt
where "MyConfig" is an appropriate name and 20XX is the current year.

Then when you want to use your new configuration, simply copy your MyConfig.txt into Configuration.py

This also will allow you to reuse certain configurations or apply quick changes to a set of configurations
(e.g. by using the sed command)

Some notes on naming conventions I am using for the saved configurations are below. Enjoy aligning!

~ Paras

_____________________________

Configurations typically include both Rich1 and Rich2 configurations, even though typically only one is run at a time during alignment tests.

Paras is currently using this key to identify configurations:

Rx        ,"Rich"            : """ Rich1 or Rich2. """
CCTx.x    ,"coeffCalibTilt"  : """ Mirror-tilts applied for the calculation of the magnification coefficients. """
ABPx.x    ,"minAverageBinPop": """ Demanded minimal entries per bin in x-y-bin; atm 6: for 20 phi-bins, 4.8: for 25 phi-bins. """
PBFx      ,"phiBinFactor"    : """ Factor by which the number of phi-bins is reduced. The histograms should now come with 60 phi bins, factor 3 reduced this to 20 bin in the fit."""
DTWx.x    ,"deltaThetaWindow": """ dTheta-range in the histograms; 4.0 for Rich1 and 3.0 for Rich2. """
CFVx      ,"combinFitMethod" : """ Method for fitting the 2D histograms; 1: first fit the slices of dTheta with a Gaussian and then fit to phi using only the mean of the Gaussian, 3: fit a 2D function """
MCMx      ,"magnFactorsMode" : """ Method for calculating the magnification coefficents; 0: magnefication coeff. pre-determined, 1: individual for all mirror pairs, determined in the 0-th iteration and not updated any further, 2: individual for all mirrorpairs, determined on-the-fly on the data. """
MDs       ,"magnifDir"       : """ directory that contains the pre-determined magnification coeff., files in this folder have to be named Rich[1,2]_MirrMagn _[tiltName]_predefined.txt """
SMx       ,"solutionMethod"  : """ Solution method for calculating the mirror tilts after the individual fits; 0: Minuit (unreliable), 1: Algebraic """

where x or x.x or similar represents the relevant number. s represents a string
Note that x *must* be an integer from 0 to 9, that of course is appropriate (e.g. there is no R3 ;).

A default configuration during a certain time period might be called
YYMMDD_DefaultConfiguration.txt *or* YYMMDD.txt
where YY is the two digit year, and so on.

Variations upon the default configuration settings then might be (examples for modifications to 160221_DefaultConfiguration.txt):
160221_SM0.txt                  Default config but solutionMethod changed from default to 0
                                 NOTE, since no Rich was specified the change is assumed to have occured for BOTH RICH1 and RICH2
160221_R1_CCT0.8.txt            Default config, but ONLY RICH1's coeffCalibTilt changed from default to 0.8
160221_SM0_R1_CCT0.8_MCM2.txt   Default config but solutionMethod changed from default to 0 for BOTH RICH1 and RICH2
                                 and ONLY for RICH1:
                                  coeffCalibTilt changed from default to 0.8
                                  magnifCoeffMode changed from default to 2
160221_R2_ABP4.0_PBF5.txt       Default config, but Rich2 minAverageBinPop changed from default to 4.0
                                                 AND phiBinFactor changed from default to 5 [i.e. will have 12 phi bins]

Clearly if the default configuration changes, a new default configuration should be saved (e.g. 160315_DefaultConfiguration.txt)

We currently assume these don't get touched (unless something breaks)

       ,"HistoDir"        : """ Directory where the savesets are being written to. """
       ,"WorkDir"         : """ Directory in which all the output will be written. """
        "MajItStart"      : """ Start the alignment at this major iteration. Please make sure this is consitent with MinItStart. """
       ,"MinItStart"      : """ Start the alignment at this minor iteration. Please make sure this is consitent with MajItStart. """
       ,"EvtMax"          : """ maximal number of events processed by brunel PER NODE (hlt fram has ~1500 nodes) """
       ,"tiltNames"       : """ Combination of tilts for the calculation and usage of the magnification factors. This is needed even when using predefined magnification factors!!! """

Not sure what should happen with these.

       ,"dataVariant"     : """ Right now this is used only for the naming convention. """
       ,"thisCase"        : """ Right now this is used only for the naming convention. """
       ,"nameStr"         : """ Right now this is used only for the naming convention, but will be set when initiating the Configuration. """

It's good to provide naming info that gives detail about alignment being done, though excellent documentation would mitigate this need.
Will be decided later, presumably.

Lastly, original/Configuration_orig.txt is the file that Claire was using before I started doing things; in case all fails somehow.

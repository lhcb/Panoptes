###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# @package RichMirrorAlignmentOnline
# @author Claire Prouve <Claire.Prouve@cern.ch>
# @date   08/07/2015

__authors__ = "Claire Prouve, Paras Naik"
__copyright__ = "University of Bristol, June 2023"

### The Configurables below can be picked up and used by RichMirrorAlignmentOnline
###   The Configurables below can also be sent by RichMirrorAlignmentOnline to RichMirrCombinFit and RichMirrAlign
### There are SEPARATE classes for configuring RICH1 and RICH2.
###   If you are changing a global setting affecting both RICHes (e.g. testing = True), change it in both classes.

### NOTE: these are not installed yet, but they exist and can be provided to MirrCombinFit if deemed necessary:
# ("sinusoidShift"       , po::value<double>(& m_sinusoidShift         )->default_value( 0.0 ), "shift parameter in fitting formula; if fixSinusoidShift !=False, it is fixed at this value; othrewise this is just initial value"    )
# ("zeroGlobalFitMean"   , po::value<int   >(& m_zeroGlobalFitMean     )->default_value( 0   ), "default =False : \"mean\" in global fit is not fixed; if !=False it is fixed at zero"                                                    )
# ("useGlobalFitMean"    , po::value<int   >(& m_useGlobalFitMean      )->default_value( 0   ), "default =False : don't; otherwise, call GetGlobalFitMean and use that mean as initial (or fixed) value of m_sinusoidShift"           )
# ("backgroundOrder"     , po::value<int   >(& m_backgroundOrder       )->default_value( 2   ), "order for the polynomial background"                                                                                             )
# ("plotOutputLevel"     , po::value<int   >(& m_plotOutputLevel       )->default_value( 2   ), "what plots should be saved (0) plot nothing (1) plot ...(2) only plot fits with chi2 worse than 3.0 (3) plot everything"         )
### NOTE: these are not installed yet, but they exist and can be provided to RichMirrAlign if deemed necessary:
# ("usePremisaligned"    , po::bool_switch  (&usePremisaligned         )->default_value( false, "no"), "if true, then pre-misaligned xml files will be used (MC case), default=false"                                             )

### Some details on regularizationMode are listed at the end of this file

import os

import GaudiKernel.ProcessJobOptions
from Configurables import LHCbConfigurableUser
from Gaudi.Configuration import *


class Rich1MirrAlignOnConf(LHCbConfigurableUser):
    __used_configurables__ = []
    __slots__ = {
        #### Do not change this!!! Just DON'T!
        "Rich": 1,
        #### Directory where we write the ROOT file that Monet picks up for the monitoring
        "HistoDir": "",
        #### Only change these if you want to pick up from where you left off in iterations, experts only
        "MajItStart":  # will be automatically forced to be the integer part of dividing MinItStart by the length of tiltNames, if magnFactorModes != 0
        0,  #  2,
        "MinItStart":  # will be automatically forced to be MajItStart multiplied by the length of tiltNames, if magnFactorModes = 0
        0,  # 18
        ### Better not change these, only for very very VERY good reasons
        "dataVariant": "Collision24",
        "useHltDecisions": False,
        "useReducedSubset": False,
        "stackDir": "/group/rich/sw/alignment/stack/",
        "magnFactorsMode": 0,  # 0,
        "magnifDir":  # relative to stackDir (i.e. remove /group/rich/sw/alignment/stack/)
        # "Panoptes/Rich/RichMirrorAlignmentOnline/files/MagnifFactors/Rich1/2024-03-13T11-26/", # use for sajans detector branch
        "Panoptes/Rich/RichMirrorAlignmentOnline/files/MagnifFactors/Rich1/20230514_202521/",
        "richFiles": "/group/online/AligWork/tempAlignmentFiles",  # "/group/rich/AlignmentFiles/",
        "maxIterations": 8,  # 8,
        "testing": True,  # False,
        "WorkDir": "/group/online/AligWork/Rich1/",
        # "/home/marshall/Bristol_RICH/testbench/Rich1/", # If running in the testbench this path must be changed
        "SaveDir": "/group/online/AligWork/MirrorAlignments/",
        # "/home/marshall/Bristol_RICH/testbench/MirrorAlignments/", # If running in the testbench this path must be changed
        "ItNrFile": "/group/online/dataflow/options/LHCbA/Rich1_Alignment_Reference_File.txt",
        "thisCase": "online",
        "tiltNames": [
            # "", "pri_negYzerZ", "pri_posYzerZ", "pri_zerYnegZ", "pri_zerYposZ",
            # "sec_negYzerZ", "sec_posYzerZ", "sec_zerYnegZ", "sec_zerYposZ"
            "",
            "priYn",
            "priYp",
            "priZn",
            "priZp",
            "secYn",
            "secYp",
            "secZn",
            "secZp",
        ],
        # "combAndMirrSubsets":
        # "/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/CombAndMirrSubsets/Rich1CombAndMirrSubsets.txt",
        ### May be changed if you know what you are doing, explanations below
        "startXMLFile": "",  # "/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/ZeroDB/Rich1_Zeroed.xml" # if testing is not True OR this is blank, it automatically uses the DB XML file
        "compareXMLFile": "",  # "/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/ZeroDB/Rich1_Zeroed.xml" # if this is blank, it automatically uses the DB XML file
        "startYAMLFile": "",
        # "/group/rich/sw/alignment/lhcb-conditions-database-new/Conditions/Rich1/Alignment/Mirrors.yml/.pool/v10" # if testing is not True OR this is blank, it automatically uses the DB XML file
        # "/group/rich/sw/alignment/yaml_files/starting_points/v4_altered.yml" # if testing is not True OR this is blank, it automatically uses the DB XML file
        "compareYAMLFile": "",
        # "/group/rich/sw/alignment/lhcb-conditions-database-new/Conditions/Rich1/Alignment/Mirrors.yml/.pool/v10" # if this is blank, it automatically uses the DB YAML file
        # "/group/rich/sw/alignment/yaml_files/starting_points/v4_altered.yml" # if this is blank, it automatically uses the DB YAML file
        "YAMLio": True,
        "useDD4hep": True,
        "minP": 30.0,
        "coeffCalibTilt": 0.7,
        "minAverageBinPop": 6.0,
        "phiBinFactor": 3,
        "minFracPhiBinsPopulated": 0.8,
        "deltaThetaWindow": 12.0,
        "combinFitMethod": 5,
        "XML_YAML_configDir":  # relative to stackDir
        "Panoptes/Rich/RichMirrorAlignmentOnline/files/XML_YAML_config/",
        "solutionMethod": 0,
        "regularizationMode": 0,
        "stopDecisionMode": 1,
        "stopToleranceMode": 1,
        "nominalResolutionSigma": 1.11,  # Run 3: best so far
        "stopSigmaFraction": 0,
        "requiredEvents":
        # 1180000  # Run 2: ~70% of 1684783
        1180000,
        "autoUpdate": False,  # True,
        "autoUpdateNoV": False,  # True,
        "trendDateRange": {"minDate": None, "maxDate": None},
        # 2017 tolerances: [0.027, 0.030, 0.455, 0.370], adjusted to [0.03, 0.03, 0.46, 0.37]
        "stopTolerancePriY": 0.015,
        "stopTolerancePriZ": 0.015,
        "stopToleranceSecY": 0.23,
        "stopToleranceSecZ": 0.185,
        "warningFactor": 3,
        "EvtMax": -1,
        "fixSinusoidShift": True,
        "nameStr": "",
        "runNumber": "",
    }

    _propertyDocDct = {
        "MajItStart": """ Start the alignment at this major iteration. Please make sure this is consitent with MinItStart. """,
        "MinItStart": """ Start the alignment at this minor iteration. Please make sure this is consitent with MajItStart. """,
        "Rich": """ Rich1 or Rich2. """,
        "HistoDir": """ Directory where we write the ROOT file that Monet picks up for the monitoring. """,
        "WorkDir": """ Directory in which all the output will be written. """,
        "SaveDir": """ Directory in which all output files of the alignment are saved after it has finished """,
        "ItNrFile": """ File which contains only the number of the current iteration written by the iterator, to be read by the analyzers """,
        "thisCase": """ Right now this is used only for the naming convention. """,
        "tiltNames": """ Combination of tilts for the calculation and usage of the magnification factors. This is needed even when using predefined magnification factors!!! """,
        # "combAndMirrSubsets":
        # """ File that lists the subset of combinations of primary and secondary mirror segments chosen for alignment monitoring, and all mirror segments under alignment monitoring """,
        "startXMLFile": """ ONLY IF testing = True, this is the XML file with the mirror tilts that we start with for the alignment; if it is blank or testing is not True, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "compareXMLFile": """ XML file with mirror tilts, for comparison of mirror tilts before and after alignment; if it is blank, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "startYAMLFile": """ ONLY IF testing = True, this is the YAML file with the mirror tilts that we start with for the alignment; if it is blank or testing is not True, it uses the latest v-numbered yml (which is what should be currently in the DB) """,
        "compareYAMLFile": """ YAML file with mirror tilts, for comparison of mirror tilts before and after alignment; if it is blank, it uses the latest v-numbered yml (which is what should be currently in the DB) """,
        "YAMLio": """ If True, then startYAMLFile and compareYAMLFile are considered instead of startXMLFile and compareXMLFile, the input is expected to be YAML, the output will be YAML, and it is assumed that the latest v-numbered file is yml and not xml """,
        "useDD4hep": """ If True, then use DD4hep (YAML-based) for reconstruction instead of DetDesc (XML-based). Also YAMLio will be set to True within the Iterator. """,
        "useHltDecisions": """ If True, then only process events with a RICH HLT line trigger decision (for the particular RICH being aligned) """,
        "useReducedSubset": """ If True, then use a reduced subset of mirror combinations allowing only for the alignment of inner mirrors """,
        "minP": """ minimum momentum cut for the tracks that we wish to reconstruct (in GeV) """,
        "coeffCalibTilt": """ Mirror-tilts applied for the calculation of the magnification coefficients. """,
        "minAverageBinPop": """ Demanded minimal entries per bin in x-y-bin; atm 6: for 20 phi-bins, 4.8: for 25 phi-bins. """,
        "phiBinFactor": """ Factor by which the number of phi-bins is reduced. The histograms should now come with 60 phi bins, factor 3 reduced this to 20 bin in the fit.""",
        "minFracPhiBinsPopulated": """ Fraction of the phi-bins that should be sufficiently populated, based on the current minAverageBinPop setting. Ideally this is 0.8, but sometimes we lower it if the mirror alignment still seems OK with a lower fraction, though we really should adjust the HLT line instead.""",
        "deltaThetaWindow": """ dTheta-range in the histograms; 4.0 for Rich1 and 3.0 for Rich2. """,
        "combinFitMethod": """ Method for fitting the 2D histograms; 1: first fit the slices of dTheta with a Gaussian and then fit to phi using only the mean of the Gaussian, 3: fit a 2D function, 5: same as 3 but with unifyWidths """,
        "stackDir": """ directory that contains the compiled code stack """,
        "magnFactorsMode": """ Method for calculating the magnification coefficents; 0: magnefication coeff. pre-determined, 1: individual for all mirror pairs, determined in the 0-th iteration and not updated any further, 2: individual for all mirrorpairs, determined on-the-fly on the data. """,
        "magnifDir": """ directory that contains the pre-determined magnification coeff., files in this folder have to be named Rich[1,2]_MirrMagn _[tiltName]_predefined.txt """,
        "richFiles": """ directory (usually in /group/rich/) that contains the Logging, Polarity, testingMaybe, and rolling CKtrend and Update files """,
        "XML_YAML_configDir": """ directory that contains the configuration files for the XML-YAML converter """,
        "solutionMethod": """ Solution method for calculating the mirror tilts after the individual fits; 0: Minuit, 1: Algebraic """,
        "regularizationMode": """ regularization mode (0/10/1/11): regularization terms magnified (weighted) with: 0(def)-> major & minor per-mirror MFs; 10-> only averaged per-kind major MFs; 1 (and 11, respectively)-> all weights = 1.       """,
        "stopDecisionMode": """ decision mode: default 0: use sqrt of the final regularization term, divided by the number of mirrors, 1: for each solution component use its own tolerance""",
        "stopToleranceMode": """ tolerance mode: default 0: calculate the stopTolerances from nominal sigma, average magn factors and stopSigmaFraction, 1 use stopTolerances given below""",
        "nominalResolutionSigma": """ RICH-dependent nominal Cherenkov angle resolution (in mrad)""",
        "stopSigmaFraction": """ tolerance for total tilt of mirror pair in terms of the fraction of the nominal Cherenkov angle resolution """,
        "maxIterations": """ The number of iterations the mirror alignment will run, before giving up """,
        "requiredEvents": """ If this many events are not procesed by the farm, on average, we do run the alignment but we will never update it. """,
        "autoUpdate": """ If True, then the fully automated updating system is activated. If False, our DB update service is still active but returns the starting alignment version. """,
        "autoUpdateNoV": """ Only looked at if autoUpdate is True. Then, if autoUpdateNoV is False, nothing changes. If autoUpdateNoV is True, then rather than sending the existing alignment version if autoUpdate is disabled *and* there is a _maybe_ alignment, we will send no version through the DB update service. HLT2 will have to wait for us to assign one. """,
        "testing": """ If True, then we are running a test alignment; we also disable our DB update service completely. """,
        "trendDateRange": """ Set the date range for the automatic trend plots. Format is YYYYMMDD. Additionally, a minDate(maxDate) of None defaults to the first(last) day of the current year (see AlignMonitor.py).""",
        "stopTolerancePriY": """ RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopTolerancePriZ": """ RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment """,
        "stopToleranceSecY": """ RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopToleranceSecZ": """ RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment """,
        "warningFactor": """ alerts the alignment if any of the mirrors have shifted more than warningFactor times a particular mirror rotation stopTolerance """,
        "EvtMax": """ maximal number of events processed by Moore PER NODE (hlt farm has ~1500 nodes), -1 = infinite """,
        "dataVariant": """ Helps us decide which Moore options to use (see RichAnalyzer.py); also used for the naming convention. """,
        "fixSinusoidShift": """ True : fix at sinusoidShift; False : it is not fixed """,
        "nameStr": """ Right now this is used only for the naming convention, but will be set when initiating the Configuration. """,
        "runNumber": """ Defined as the earliest run in the fill """,
    }

    def setNameStr(self):
        thisTuning = f"Mp{self.getProp('minAverageBinPop')}"
        thisTuning += f"Wi{self.getProp('deltaThetaWindow')}"
        thisTuning += f"Fm{self.getProp('combinFitMethod')}"
        thisTuning += f"Mm{self.getProp('magnFactorsMode')}"
        thisTuning += f"Sm{self.getProp('solutionMethod')}"
        thisNameStr = (
            f"{thisTuning}_{self.getProp('thisCase')}_{self.getProp('dataVariant')}"
        )
        self.setProp("nameStr", thisNameStr)

    #### Dont mess with this filename!!!
    # this sends the histograms to the correct place /hist/Savesets/ByRun/ to show up in Monet
    def setHistoDir(self):
        # take earliest run in fill as the runNumber (for Monet's structure)
        runs = []  # can remove this when everything works for Run 3
        import getpass

        if "online" in getpass.getuser():
            import OnlineEnvBase as Online  # import OnlineEnv as Online
            from MooreOnlineConf.utils import alignment_options

            online_options = alignment_options(Online)
            runs = online_options.runs  # runs = Online.DeferredRuns
            # PN - Extract the run number substring from the full string - WILL NOT WORK IF RUN NUMBERS BECOME 7 DIGITS
            runs = [entry[-6:] for entry in runs]
            sorted_runs = sorted(runs, key=lambda x: int(x))
            runNumber = sorted_runs[0]
        else:
            runNumber = "000000"  # placeholder run number

        first_dir = runNumber[:2] + "0" * (
            len(runNumber) - 2
        )  # returns 250000 for 256112
        second_dir = runNumber[:3] + "0" * (
            len(runNumber) - 3
        )  # returns 256000 for 256112
        histodir = f"/hist/Savesets/ByRun/RichAlignMon/{first_dir}/{second_dir}/"
        os.makedirs(
            histodir, exist_ok=True
        )  # create subdirectory path if it doesnt exist
        self.setProp("HistoDir", histodir)
        self.setProp("runNumber", runNumber)

    def checkStartIt(self):
        if not self.getProp("magnFactorsMode") == 0:
            integerDivides = self.getProp("MinItStart") // len(
                self.getProp("tiltNames")
            )
            if integerDivides != self.getProp("MajItStart"):
                print(
                    "WARNING: Major Iteration did not match minor iteration. Major iteration is being set to match minor iteration."
                )
                self.setProp("MajItStart", integerDivides)
        if self.getProp("magnFactorsMode") == 0 and self.getProp("magnifDir") == "":
            self.setProp(
                "magnifDir", "/group/rich/AlignmentFiles/MagnifFactors/Rich1/"
            )  # PN - This whole thing needs to be changed for Run 3, but indeed good to have a default directory for MagFactors
            print(
                "WARNING: Mode for calculating magnification coefficients was chosen to be 0 but no directory with pre-determined coefficients provided. Directory set to default."
            )
        if self.getProp("magnFactorsMode") == 0:
            print(
                'INFO: Since magnFactorsMode == 0, Minor Iteration is being forced to be self.getProp("MajItStart") * len(self.getProp("tiltNames")).'
            )
            self.setProp(
                "MinItStart",
                self.getProp("MajItStart") * len(self.getProp("tiltNames")),
            )

    def __apply_configuration__(self):
        self.setHistoDir()
        self.setNameStr()
        self.checkStartIt()
        # Next 3 lines add the trailing slash in key directory names
        self.setProp("WorkDir", os.path.join(self.getProp("WorkDir"), ""))
        self.setProp("SaveDir", os.path.join(self.getProp("SaveDir"), ""))
        self.setProp("stackDir", os.path.join(self.getProp("stackDir"), ""))
        self.setProp("magnifDir", os.path.join(self.getProp("magnifDir"), ""))
        self.setProp("richFiles", os.path.join(self.getProp("richFiles"), ""))
        self.setProp(
            "XML_YAML_configDir", os.path.join(self.getProp("XML_YAML_configDir"), "")
        )
        self.setProp("HistoDir", os.path.join(self.getProp("HistoDir"), ""))
        print("INFO: Rich1MirrAlignOnConf applied")


class Rich2MirrAlignOnConf(LHCbConfigurableUser):
    __used_configurables__ = []
    __slots__ = {
        #### Do not ever change this!!! Just DON'T!
        "Rich": 2,
        #### Directory where we write the ROOT file that Monet picks up for the monitoring
        "HistoDir": "",
        #### Only change these if you want to pick up from where you left off in iterations, experts only
        "MajItStart":  # will be automatically forced to be the integer part of dividing MinItStart by the length of tiltNames, if magnFactorModes != 0
        0,  # 4,
        "MinItStart":  # will be automatically forced to be MajItStart multiplied by the length of tiltNames, if magnFactorModes = 0
        0,  # 36
        ### Better not change these, only for very very VERY good reasons
        "dataVariant": "Collision24",
        "useHltDecisions": False,
        "useReducedSubset": False,
        "stackDir": "/group/rich/sw/alignment/stack/",
        "magnFactorsMode": 0,
        "richFiles": "/group/online/AligWork/tempAlignmentFiles",  # "/group/rich/AlignmentFiles/",
        "magnifDir":  # relative to stackDir (i.e. remove /group/rich/sw/alignment/stack/)
        "Panoptes/Rich/RichMirrorAlignmentOnline/files/MagnifFactors/Rich2/2024-03-22T14-48/",
        "maxIterations": 8,  # 8,
        "testing": True,  # False,
        "WorkDir": "/group/online/AligWork/Rich2/",
        # "/home/marshall/Bristol_RICH/testbench/Rich2/", # If running in the testbench this path must be changed
        "SaveDir": "/group/online/AligWork/MirrorAlignments/",
        # "/home/marshall/Bristol_RICH/testbench/MirrorAlignments/", # If running in the testbench this path must be changed
        "ItNrFile": "/group/online/dataflow/options/LHCbA/Rich2_Alignment_Reference_File.txt",
        "thisCase": "online",
        "tiltNames": [
            # "", "pri_negYzerZ", "pri_posYzerZ", "pri_zerYnegZ", "pri_zerYposZ",
            # "sec_negYzerZ", "sec_posYzerZ", "sec_zerYnegZ", "sec_zerYposZ"
            "",
            "priYn",
            "priYp",
            "priZn",
            "priZp",
            "secYn",
            "secYp",
            "secZn",
            "secZp",
        ],
        # "combAndMirrSubsets":
        # "/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/CombAndMirrSubsets/Rich2CombAndMirrSubsets.txt",
        ### May be changed if you know what you are doing
        "startXMLFile": "",  # "/group/rich/sw/alignment/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/ZeroDB/Rich2_Zeroed.xml" # if testing is not True OR this is blank, it automatically uses the DB XML file
        "compareXMLFile": "",  # if this is blank, it automatically uses the DB XML file
        "startYAMLFile": "",
        # "/group/rich/sw/alignment/lhcb-conditions-database-new/Conditions/Rich2/Alignment/Mirrors.yml/.pool/v9" # if testing is not True OR this is blank, it automatically uses the DB XML file
        # "" # if testing is not True OR this is blank, it automatically uses the DB YAML file
        # "/group/rich/sw/alignment/yaml_files/starting_points/RICH2_2024_preliminary_updated.yml" # if testing is not True OR this is blank, it automatically uses the DB YAML file
        "compareYAMLFile": "",
        # "/group/rich/sw/alignment/lhcb-conditions-database-new/Conditions/Rich2/Alignment/Mirrors.yml/.pool/v9" # if testing is not True OR this is blank, it automatically uses the DB XML file
        # "" # if this is blank, it automatically uses the DB YAML file
        # "/group/rich/sw/alignment/yaml_files/starting_points/RICH2_2024_preliminary_updated.yml" # if this is blank, it automatically uses the DB YAML file
        "YAMLio": True,
        "useDD4hep": True,
        "minP": 60.0,
        "coeffCalibTilt": 0.3,
        "minAverageBinPop": 6.0,
        "phiBinFactor": 3,
        "minFracPhiBinsPopulated": 0.7,  # adjusted from 0.8 due to inefficiencies in our existing choice of HLT line prescales
        "deltaThetaWindow": 4.0,
        "combinFitMethod": 5,
        "XML_YAML_configDir":  # relative to stackDir
        "Panoptes/Rich/RichMirrorAlignmentOnline/files/XML_YAML_config/",
        "solutionMethod": 0,
        "regularizationMode": 0,
        "stopDecisionMode": 1,
        "stopToleranceMode": 1,
        "nominalResolutionSigma": 0.625,  # Run 3: best so far
        "stopSigmaFraction": 0,
        "requiredEvents": 942500,  # Run 2: ~70% of 1346563
        "autoUpdate": False,  # True,
        "autoUpdateNoV": False,  # True,
        "trendDateRange": {"minDate": None, "maxDate": None},
        # 2017 tolerances: [0.032, 0.030, 0.043, 0.057], adjusted to [0.03, 0.03, 0.05, 0.06]
        "stopTolerancePriY": 0.03,
        "stopTolerancePriZ": 0.03,
        "stopToleranceSecY": 0.05,
        "stopToleranceSecZ": 0.06,
        "warningFactor": 3,
        "EvtMax": -1,
        "fixSinusoidShift": True,
        "nameStr": "",
        "runNumber": "",
    }

    _propertyDocDct = {
        "MajItStart": """ Start the alignment at this major iteration. Please make sure this is consitent with MinItStart. """,
        "MinItStart": """ Start the alignment at this minor iteration. Please make sure this is consitent with MajItStart. """,
        "Rich": """ Rich1 or Rich2. """,
        "HistoDir": """ Directory where we write the ROOT file that Monet picks up for the monitoring. """,
        "WorkDir": """ Directory in which all the output will be written. """,
        "SaveDir": """ Directory in which all output files of the alignment are saved after it has finished """,
        "ItNrFile": """ File which contains only the number of the current iteration written by the iterator, to be read by the analyzers """,
        "thisCase": """ Right now this is used only for the naming convention. """,
        "tiltNames": """ Combination of tilts for the calculation and usage of the magnification factors. This is needed even when using predefined magnification factors!!! """,
        # "combAndMirrSubsets":
        # """ File that lists the subset of combinations of primary and secondary mirror segments chosen for alignment monitoring, and all mirror segments under alignment monitoring """,
        "startXMLFile": """ ONLY IF testing = True, this is the XML file with the mirror tilts that we start with for the alignment; if it is blank or testing is not True, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "compareXMLFile": """ XML file with mirror tilts, for comparison of mirror tilts before and after alignment; if it is blank, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "startYAMLFile": """ ONLY IF testing = True, this is the YAML file with the mirror tilts that we start with for the alignment; if it is blank or testing is not True, it uses the latest v-numbered yml (which is what should be currently in the DB) """,
        "compareYAMLFile": """ YAML file with mirror tilts, for comparison of mirror tilts before and after alignment; if it is blank, it uses the latest v-numbered yml (which is what should be currently in the DB) """,
        "YAMLio": """ If True, then startYAMLFile and compareYAMLFile are considered instead of startXMLFile and compareXMLFile, the input is expected to be YAML, the output will be YAML, and it is assumed that the latest v-numbered file is yml and not xml """,
        "useDD4hep": """ If True, then use DD4hep (YAML-based) for reconstruction instead of DetDesc (XML-based). Also YAMLio will be set to True within the Iterator. """,
        "useHltDecisions": """ If True, then only process events with a RICH HLT line trigger decision (for the particular RICH being aligned) """,
        "useReducedSubset": """ If True, then use a reduced subset of mirror combinations allowing only for the alignment of inner mirrors """,
        "minP": """ minimum momentum cut for the tracks that we wish to reconstruct (in GeV) """,
        "coeffCalibTilt": """ Mirror-tilts applied for the calculation of the magnification coefficients. """,
        "minAverageBinPop": """ Demanded minimal entries per bin in x-y-bin; atm 6: for 20 phi-bins, 4.8: for 25 phi-bins. """,
        "phiBinFactor": """ Factor by which the number of phi-bins is reduced. The histograms should now come with 60 phi bins, factor 3 reduced this to 20 bin in the fit.""",
        "minFracPhiBinsPopulated": """ Fraction of the phi-bins that should be sufficiently populated, based on the current minAverageBinPop setting. Ideally this is 0.8, but sometimes we lower it if the mirror alignment still seems OK with a lower fraction, though we really should adjust the HLT line instead.""",
        "deltaThetaWindow": """ dTheta-range in the histograms; 4.0 for Rich1 and 3.0 for Rich2. """,
        "combinFitMethod": """ Method for fitting the 2D histograms; 1: first fit the slices of dTheta with a Gaussian and then fit to phi using only the mean of the Gaussian, 3: fit a 2D function, 5: Same as 3 but with unifyWidths """,
        "stackDir": """ directory that contains the compiled code stack """,
        "magnFactorsMode": """ Method for calculating the magnification coefficents; 0: magnefication coeff. pre-determined, 1: individual for all mirror pairs, determined in the 0-th iteration and not updated any further, 2: individual for all mirrorpairs, determined on-the-fly on the data. """,
        "magnifDir": """ directory that contains the pre-determined magnification coeff., files in this folder have to be named Rich[1,2]_MirrMagn _[tiltName]_predefined.txt """,
        "richFiles": """ directory (usually in /group/rich/) that contains the Logging, Polarity, testingMaybe, and rolling CKtrend and Update files """,
        "XML_YAML_configDir": """ directory that contains the configuration files for the XML-YAML converter """,
        "solutionMethod": """ Solution method for calculating the mirror tilts after the individual fits; 0: Minuit, 1: Algebraic """,
        "regularizationMode": """ regularization mode (0/10/1/11): regularization terms magnified (weighted) with: 0(def)-> major & minor per-mirror MFs; 10-> only averaged per-kind major MFs; 1 (and 11, respectively)-> all weights = 1.       """,
        "stopDecisionMode": """ decision mode: default 0: use sqrt of the final regularization term, divided by the number of mirrors, 1: for each solution component use its own tolerance""",
        "stopToleranceMode": """ tolerance mode: default 0: calculate the stopTolerances from nominal sigma, average magn factors and stopSigmaFraction, 1 use stopTolerances given below""",
        "nominalResolutionSigma": """ RICH-dependent nominal Cherenkov angle resolution (in mrad)""",
        "stopSigmaFraction": """ tolerance for total tilt of mirror pair in terms of the fraction of the nominal Cherenkov angle resolution """,
        "maxIterations": """ The number of iterations the mirror alignment will run, before giving up """,
        "requiredEvents": """ If this many events are not procesed by the farm, on average, we do run the alignment but we will never update it. """,
        "autoUpdate": """ If True, then the fully automated updating system is activated. If False, our DB update service is still active but returns the starting alignment version. """,
        "autoUpdateNoV": """ Only looked at if autoUpdate is True. Then, if autoUpdateNoV is False, nothing changes. If autoUpdateNoV is True, then rather than sending the existing alignment version if autoUpdate is disabled *and* there is a _maybe_ alignment, we will send no version through the DB update service. HLT2 will have to wait for us to assign one. """,
        "testing": """ If True, then we are running a test alignment; we also disable our DB update service completely. """,
        "trendDateRange": """ Set the date range for the automatic trend plots. Format is YYYYMMDD. Additionally, a minDate(maxDate) of None defaults to the first(last) day of the current year (see AlignMonitor.py).""",
        "stopTolerancePriY": """ RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopTolerancePriZ": """ RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment """,
        "stopToleranceSecY": """ RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopToleranceSecZ": """ RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment """,
        "warningFactor": """ alerts the alignment if any of the mirrors have shifted more than warningFactor times a particular mirror rotation stopTolerance """,
        "EvtMax": """ maximal number of events processed by Moore PER NODE (hlt farm has ~1500 nodes), -1 = infinite """,
        "dataVariant": """ Helps us decide which Moore options to use (see RichAnalyzer.py); also used for the naming convention. """,
        "fixSinusoidShift": """ True : fix at sinusoidShift; False : it is not fixed """,
        "nameStr": """ Right now this is used only for the naming convention, but will be set when initiating the Configuration. """,
        "runNumber": """ Defined as the earliest run in the fill """,
    }

    def setNameStr(self):
        thisTuning = f"Mp{self.getProp('minAverageBinPop')}"
        thisTuning += f"Wi{self.getProp('deltaThetaWindow')}"
        thisTuning += f"Fm{self.getProp('combinFitMethod')}"
        thisTuning += f"Mm{self.getProp('magnFactorsMode')}"
        thisTuning += f"Sm{self.getProp('solutionMethod')}"
        thisNameStr = (
            f"{thisTuning}_{self.getProp('thisCase')}_{self.getProp('dataVariant')}"
        )
        self.setProp("nameStr", thisNameStr)

    #### Dont mess with this filename!!!
    # this sends the histograms to the correct place /hist/Savesets/ByRun/ to show up in Monet
    def setHistoDir(self):
        # take earliest run in fill as the runNumber (for Monet's structure)
        runs = []  # can remove this when everything works for Run 3
        import getpass

        if "online" in getpass.getuser():
            import OnlineEnvBase as Online  # import OnlineEnv as Online
            from MooreOnlineConf.utils import alignment_options

            online_options = alignment_options(Online)
            runs = online_options.runs  # runs = Online.DeferredRuns
            # PN - Extract the run number substring from the full string - WILL NOT WORK IF RUN NUMBERS BECOME 7 DIGITS
            runs = [entry[-6:] for entry in runs]
            sorted_runs = sorted(runs, key=lambda x: int(x))
            runNumber = sorted_runs[0]
        else:
            runNumber = "000000"  # placeholder run number

        first_dir = runNumber[:2] + "0" * (
            len(runNumber) - 2
        )  # returns 250000 for 256112
        second_dir = runNumber[:3] + "0" * (
            len(runNumber) - 3
        )  # returns 256000 for 256112
        histodir = f"/hist/Savesets/ByRun/RichAlignMon/{first_dir}/{second_dir}/"
        os.makedirs(
            histodir, exist_ok=True
        )  # create subdirectory path if it doesnt exist
        self.setProp("HistoDir", histodir)
        self.setProp("runNumber", runNumber)

    def checkStartIt(self):
        if not self.getProp("magnFactorsMode") == 0:
            integerDivides = self.getProp("MinItStart") // len(
                self.getProp("tiltNames")
            )
            if integerDivides != self.getProp("MajItStart"):
                print(
                    "WARNING: Major Iteration did not match minor iteration. Major iteration is being set to match minor iteration."
                )
                self.setProp("MajItStart", integerDivides)
        if self.getProp("magnFactorsMode") == 0 and self.getProp("magnifDir") == "":
            self.setProp(
                "magnifDir", "/group/rich/AlignmentFiles/MagnifFactors/Rich2/"
            )  # PN - This whole thing needs to be changed for Run 3, but indeed good to have a default directory for MagFactors
            print(
                "WARNING: Mode for calculating magnification coefficients was chosen to be 0 but no directory with pre-determined coefficients provided. Directory set to default."
            )
        if self.getProp("magnFactorsMode") == 0:
            print(
                'INFO: Since magnFactorsMode == 0, Minor Iteration is being forced to be self.getProp("MajItStart") * len(self.getProp("tiltNames")).'
            )
            self.setProp(
                "MinItStart",
                self.getProp("MajItStart") * len(self.getProp("tiltNames")),
            )

    def __apply_configuration__(self):
        self.setHistoDir()
        self.setNameStr()
        self.checkStartIt()
        # Next 3 lines add the trailing slash in key directory names
        self.setProp("WorkDir", os.path.join(self.getProp("WorkDir"), ""))
        self.setProp("SaveDir", os.path.join(self.getProp("SaveDir"), ""))
        self.setProp("stackDir", os.path.join(self.getProp("stackDir"), ""))
        self.setProp("magnifDir", os.path.join(self.getProp("magnifDir"), ""))
        self.setProp("richFiles", os.path.join(self.getProp("richFiles"), ""))
        self.setProp(
            "XML_YAML_configDir", os.path.join(self.getProp("XML_YAML_configDir"), "")
        )
        self.setProp("HistoDir", os.path.join(self.getProp("HistoDir"), ""))
        print("INFO: Rich2MirrAlignOnConf applied")


# ===============================================================================
# regularizationMode has changed from 1 (used in 2016) to 0 (used in 2017).
# Looking back at the functional we minimize:
#   https://indico.cern.ch/event/477524/contributions/2174895/attachments/1276535/1894128/richmirralign_update_2016-05-20_rich_sw.pdf
# Page 3 of the PDF has the 2016 functional
# Page 4 of the PDF has the 2017 functional
# One can see that the mirror rotations (alpha, beta) in the last term of the 2017 functional get weighted by the average magnification factors, whereas in 2017 they did not.
#
# From a quick look at the last terms of those functionals, and using the typical average magnification factors shown on slide 4,
# Paras concludes that our tolerances should be adjusted for 2017 as follows:
#
# [primary y, primary z, secondary y, secondary z] tolerances
# What we decided for RICH1 from 2016 study: [0.050, 0.060, 0.250, 0.300] mrad
# In light of this, the RICH1 tolerances for 2017 should be set to: [0.027, 0.030, 0.455, 0.370] mrad
# What we decided for RICH2 from 2016 study: [0.065, 0.055, 0.045, 0.035] mrad
# In light of this, the RICH2 tolerances for 2017 should be set to: [0.032, 0.030, 0.043, 0.057] mrad
# ===============================================================================

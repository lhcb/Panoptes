Note that since 2017, the version of Configuration.py used for each alignment is now stored
with the the result of each alignment in /group/online/AligWork/MirrorAlignments/
so we no longer save configurations unless we need to do local editing (e.g. quick test alignments)

The Active Configuration is always Rich/RichMirrorAlignmentOnline/python/RichMirrorAlignmentOnline/Configuration.py

Info about Archived Configurations:

2018_Configurations/Configuration.py: the Configuration in the master/run2-patches branch at the end of 2018

2018_Configurations/Configuration_FinalPit.py: the Configuration last used at the pit in 2018 (PN: I think this is when I attempted to get an alignment from ion collisions)

2018_Configurations/Configuration_2018_runOn2017.py: a Configuration for running on 2017 data

2016_Configurations/2016_LastKnownConfig.txt - Unfortunately changing configurations were not saved for this year, but this is the Configuration.py that was
                                                    available before key changes for 2017
                                               Differences from 16xxxx_InitialConfig.txt:
                                                    Automatically adds trailing slash if needed to directory names that we are allowed to edit
                                                    Added:
<        ,"dbXMLFile"              : """ XML file with mirror tilts that is currently in the DB, for comparison of mirror tilts before and after alignment """
<        ,"regularizationMode"     : """ regularization mode: default 0: regularization term contains average magnification coefficients as weight; !=0: all weights in the regularization term are 1."""
<        ,"stopToleranceMode"      : """ tolerance mode: default 0: calculate the stopTolerances from nominal sigma, average magn factors and stopSigmaFraction, 1 use stopTolerances given below"""
<        ,"nominalResolutionSigma" : """ RICH-dependent nominal Cherenkov angle resolution"""
<        ,"stopSigmaFraction"      : """ tolerance for total tilt of mirror pair in terms of the fraction of the nominal Cherenkov angle resolution"""
<        ,"stopTolerancePriY"      : """ RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment """
<        ,"stopTolerancePriZ"      : """ RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment """
<        ,"stopToleranceSecY"      : """ RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment """
<        ,"stopToleranceSecZ"      : """ RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment """
                                                    Removed:
>        ,"stopTolerance"   : """ tolerance for mirrors (primary mirrors only if stopToleranceSec = 0), in mrad, to stop the alignment because all mirror tilts have converged; historically 0.1 """
>        ,"stopToleranceSec": """ if non-zero [see above], tolerance for secondary mirrors, in mrad, to stop the alignment because all mirror tilts have converged; historically 0.1 """

2016_Configurations/16xxxx_InitialConfig.txt - First Configuration for 2016 running, after starting from nullified CONDDB and then fixing magnification factors
                                                    should have RICH1 coeffCalibTilt 0.7
                                                    magnifCoeffMode 0 after fixing mag factors, 2 when starting from nullified CONDDB to find new mag factors
                                                    solutionMethod 0
                                                    fitMethod 5

2016_Configurations/16xxxx_DraftInitialConfig.txt - Draft initial configuration for 2016 running (can ignore now)


v1r1_Configurations/160221_DefaultConfiguration.txt - same as original/Configuration_orig.txt, but added a couple help lines which are hopefully harmless
                                                      (but need to be updated with the real information)

v1r1_Configurations/160221_SM0.txt - Change to Anatoly's minuit method with regularization for both RICHes.
                                     Start from existing FIXED magnification coefficients.

v1r1_Configurations/16xxxx_DraftInitialConfig.txt - Draft initial configuration for 2016 running
                                                    should have RICH1 coeffCalibTilt 0.7
                                                    magnifCoeffMode 2 [need to recalculate magnification factors before stable running!]
                                                    solutionMethod 0
                                                    fitMethod 5
                                                    RECOMMENDED TO START FROM NULLIFIED CONDDB!!!

original/Configuration_orig.txt - what was there when we started SavedConfigurations

Removed Configurations:

160221_SM0_R1_CCT0.8_MCM2.txt - Change to Anatoly's minuit method with regularization for both RICHes.
                                 and ONLY for RICH1:
                                  coeffCalibTilt changed from default to 0.8
                                  magnifCoeffMode changed from default to 2 (recalculate magnification factors  in each iteration)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# @package RichMirrorAlignmentOnline
# @author Claire Prouve <Claire.Prouve@cern.ch>
# @date   08/07/2015
__authors__ = "Claire Prouve <Claire.Prouve@cern.ch>, Paras Naik <Paras.Naik@cern.ch>"

### The Configurables below can be picked up and used by RichMirrorAlignmentOnline
###   The Configurables below can also be sent by RichMirrorAlignmentOnline to RichMirrCombinFit and RichMirrAlign
### There are SEPARATE classes for configuring RICH1 and RICH2.
###   If you are changing a global setting affecting both RICHes (e.g. testing = True), change it in both classes.

### NOTE: these are not installed yet, but they exist and can be provided to MirrCombinFit if deemed necessary:
# ("sinusoidShift"       , po::value<double>(& m_sinusoidShift         )->default_value( 0.0 ), "shift parameter in fitting formula; if fixSinusoidShift !=0, it is fixed at this value; othrewise this is just initial value"    )
# ("zeroGlobalFitMean"   , po::value<int   >(& m_zeroGlobalFitMean     )->default_value( 0   ), "default =0 : \"mean\" in global fit is not fixed; if !=0 it is fixed at zero"                                                    )
# ("useGlobalFitMean"    , po::value<int   >(& m_useGlobalFitMean      )->default_value( 0   ), "default =0 : don't; otherwise, call GetGlobalFitMean and use that mean as initial (or fixed) value of m_sinusoidShift"           )
# ("backgroundOrder"     , po::value<int   >(& m_backgroundOrder       )->default_value( 2   ), "order for the polynomial background"                                                                                             )
# ("plotOutputLevel"     , po::value<int   >(& m_plotOutputLevel       )->default_value( 2   ), "what plots should be saved (0) plot nothing (1) plot ...(2) only plot fits with chi2 worse than 3.0 (3) plot everything"         )
### NOTE: these are not installed yet, but they exist and can be provided to RichMirrAlign if deemed necessary:
# ("usePremisaligned"    , po::bool_switch  (&usePremisaligned         )->default_value( false, "no"), "if true, then pre-misaligned xml files will be used (MC case), default=false"                                             )

### Some details on regularizationMode are listed at the end of this file

import os

import GaudiKernel.ProcessJobOptions
from Configurables import LHCbConfigurableUser
from Gaudi.Configuration import *


class Rich1MirrAlignOnConf(LHCbConfigurableUser):
    __used_configurables__ = []
    __slots__ = {
        #### Do not change these!!! Just DON'T!
        "Rich": 1,
        "HistoDir": "",
        #### Only change these if you want to pick up from where you left off in iterations, experts only
        "MajItStart": 0,
        "MinItStart": 0,
        ### Better not change these, only for very very VERY good reasons
        "WorkDir": "/group/online/AligWork/Rich1/",
        "SaveDir": "/group/online/AligWork/MirrorAlignments/",
        "ItNrFile": "/group/online/dataflow/options/LHCbA/Rich1_Alignment_Reference_File.txt",
        "thisCase": "online",
        "tiltNames": [
            "",
            "pri_negYzerZ",
            "pri_posYzerZ",
            "pri_zerYnegZ",
            "pri_zerYposZ",
            "sec_negYzerZ",
            "sec_posYzerZ",
            "sec_zerYnegZ",
            "sec_zerYposZ",
        ],
        ### May be changed if you know what you are doing, explanations below
        "combAndMirrSubsets": "/group/rich/sw/cmtuser/AlignmentRelease/Rich/RichMirrorAlignmentOnline/files/Rich1CombAndMirrSubsets.txt",
        "startXMLFile": "",  # "/group/rich/AlignmentFiles/databaseAlignments/Rich1/Rich1_Zeroed.xml" # if testing is not True OR this is blank, it automatically uses the DB XML file
        "compareXMLFile": "",  # if this is blank, it automatically uses the DB XML file
        "coeffCalibTilt": 0.7,
        "minAverageBinPop": 6,
        "phiBinFactor": 3,
        "minFracPhiBinsPopulated": 0.8,
        "deltaThetaWindow": 8.0,
        "combinFitMethod": 5,
        "magnFactorsMode": 0,
        "magnifDir": "/group/rich/AlignmentFiles/MagnifFactors/Rich1/20170612_004007/",
        "solutionMethod": 0,
        "regularizationMode": 0,
        "stopToleranceMode": 1,
        "nominalResolutionSigma": 1.66,  # by eye, looking at mirror resolutions between fills 6130 and 6280 (e.g. post- best MDCS corrections for 2017)
        #       ,"nominalResolutionSigma" : 1.639 # from https://lblogbook.cern.ch/RICH/13677
        "stopSigmaFraction": 0,
        "maxIterations": 8,
        "requiredEvents": 1180000,  # ~70% of 1684783
        "autoUpdate": True,
        "autoUpdateNoV": False,
        "testing": True,
        "trendDateRange": {"minDate": 20170602, "maxDate": 20171129},
        # 2017 tolerances: [0.027, 0.030, 0.455, 0.370], adjusted to [0.03, 0.03, 0.46, 0.37]
        "stopTolerancePriY": 0.03,
        "stopTolerancePriZ": 0.03,
        "stopToleranceSecY": 0.46,
        "stopToleranceSecZ": 0.37,
        "warningFactor": 3,
        "EvtMax": -1,
        "dataVariant": "Collision17",
        "fixSinusoidShift": 1,
        "nameStr": "",
        "displayMode": 1,
        "xmlMDCS": "",  # "/group/rich/AlignmentFiles/MDCS/Conditions/Rich1/Environment/HPD.xml"
    }

    _propertyDocDct = {
        "MajItStart": """ Start the alignment at this major iteration. Please make sure this is consitent with MinItStart. """,
        "MinItStart": """ Start the alignment at this minor iteration. Please make sure this is consitent with MajItStart. """,
        "Rich": """ Rich1 or Rich2. """,
        "HistoDir": """ Directory where the savesets are being written to. """,
        "WorkDir": """ Directory in which all the output will be written. """,
        "SaveDir": """ Directory in which all output files of the alignment are saved after it has finished """,
        "ItNrFile": """ File which contains only the number of the current iteration written by the iterator, to be read by the analyzers """,
        "thisCase": """ Right now this is used only for the naming convention. """,
        "tiltNames": """ Combination of tilts for the calculation and usage of the magnification factors. This is needed even when using predefined magnification factors!!! """,
        "combAndMirrSubsets": """ File that lists the subset of combinations of primary and secondary mirror segments chosen for alignment monitoring, and all mirror segments under alignment monitoring """,
        "startXMLFile": """ ONLY IF testing = True, this is the XML file with the mirror tilts that we start with for the alignment; if it is blank or testing is not True, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "compareXMLFile": """ XML file with mirror tilts, for comparison of mirror tilts before and after alignment; if it is blank, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "coeffCalibTilt": """ Mirror-tilts applied for the calculation of the magnification coefficients. """,
        "minAverageBinPop": """ Demanded minimal entries per bin in x-y-bin; atm 6: for 20 phi-bins, 4.8: for 25 phi-bins. """,
        "phiBinFactor": """ Factor by which the number of phi-bins is reduced. The histograms should now come with 60 phi bins, factor 3 reduced this to 20 bin in the fit.""",
        "minFracPhiBinsPopulated": """ Fraction of the phi-bins that should be sufficiently populated, based on the current minAverageBinPop setting. Ideally this is 0.8, but sometimes we lower it if the mirror alignment still seems OK with a lower fraction, though we really should adjust the HLT line instead.""",
        "deltaThetaWindow": """ dTheta-range in the histograms; 4.0 for Rich1 and 3.0 for Rich2. """,
        "combinFitMethod": """ Method for fitting the 2D histograms; 1: first fit the slices of dTheta with a Gaussian and then fit to phi using only the mean of the Gaussian, 3: fit a 2D function, 5: same as 3 but with unifyWidths """,
        "magnFactorsMode": """ Method for calculating the magnification coefficents; 0: magnefication coeff. pre-determined, 1: individual for all mirror pairs, determined in the 0-th iteration and not updated any further, 2: individual for all mirrorpairs, determined on-the-fly on the data. """,
        "magnifDir": """ directory that contains the pre-determined magnification coeff., files in this folder have to be named Rich[1,2]_MirrMagn _[tiltName]_predefined.txt """,
        "solutionMethod": """ Solution method for calculating the mirror tilts after the individual fits; 0: Minuit, 1: Algebraic """,
        "regularizationMode": """ regularization mode (0/10/1/11): regularization terms magnified (weighted) with: 0(def)-> major & minor per-mirror MFs; 10-> only averaged per-kind major MFs; 1 (and 11, respectively)-> all weights = 1.       """,
        "stopToleranceMode": """ tolerance mode: default 0: calculate the stopTolerances from nominal sigma, average magn factors and stopSigmaFraction, 1 use stopTolerances given below""",
        "nominalResolutionSigma": """ RICH-dependent nominal Cherenkov angle resolution (in mrad)""",
        "stopSigmaFraction": """ tolerance for total tilt of mirror pair in terms of the fraction of the nominal Cherenkov angle resolution """,
        "maxIterations": """ The number of iterations the mirror alignment will run, before giving up """,
        "requiredEvents": """ If this many events are not procesed by the farm, on average, we do run the alignment but we will never update it. """,
        "autoUpdate": """ If True, then the fully automated updating system is activated. If False, our DB update service is still active but returns the starting alignment version. """,
        "autoUpdateNoV": """ Only looked at if autoUpdate is True. Then, if autoUpdateNoV is False, nothing changes. If autoUpdateNoV is True, then rather than sending the existing alignment version if autoUpdate is disabled *and* there is a _maybe_ alignment, we will send no version through the DB update service. HLT2 will have to wait for us to assign one. """,
        "testing": """ If True, then we are running a test alignment; we also disable our DB update service completely. """,
        "trendDateRange": """ Set the date range for the automatic trend plots. Format is YYYYMMDD. Additionally, a minDate(maxDate) of None defaults to the first(last) day of the current year.""",
        "stopTolerancePriY": """ RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopTolerancePriZ": """ RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment """,
        "stopToleranceSecY": """ RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopToleranceSecZ": """ RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment """,
        "warningFactor": """ alerts the alignment if any of the mirrors have shifted more than warningFactor times a particular mirror rotation stopTolerance """,
        "EvtMax": """ maximal number of events processed by brunel PER NODE (hlt farm has ~1500 nodes), -1 = infinite """,
        "dataVariant": """ Helps us decide which Brunel options to use; also used for the naming convention. """,
        "fixSinusoidShift": """ 1 : fix at sinusoidShift; 0 : it is not fixed """,
        "nameStr": """ Right now this is used only for the naming convention, but will be set when initiating the Configuration. """,
        "displayMode": """ 1 : Current TH2D display style (auto-scaling Z-axis, granular colors, using same colors for > 4*threshold); 0: Anatoly's development TH2D display style (similar, but with printed mirror number also, and fixed scale)""",
        "xmlMDCS": """ Location of MDCS conditions XML file, for override if needed. Not used if blank. """,
    }

    def setNameStr(self):
        thisTuning = "Mp" + str(self.getProp("minAverageBinPop"))
        thisTuning += "Wi" + str(self.getProp("deltaThetaWindow"))
        thisTuning += "Fm" + str(self.getProp("combinFitMethod"))
        thisTuning += "Mm" + str(self.getProp("magnFactorsMode"))
        thisTuning += "Sm" + str(self.getProp("solutionMethod"))
        thisNameStr = (
            thisTuning
            + "_"
            + str(self.getProp("thisCase"))
            + "_"
            + str(self.getProp("dataVariant"))
        )
        self.setProp("nameStr", thisNameStr)

    ### Dont mess with this filename!!!
    def setHistoDir(self):
        import time

        year = time.strftime("%Y")
        histodir = (
            "/hist/Savesets/"
            + str(year)
            + "/LHCbA/AligWrk_Rich"
            + str(self.getProp("Rich"))
            + "/"
        )
        self.setProp("HistoDir", histodir)

    def checkStartIt(self):
        if not self.getProp("magnFactorsMode") == 0:
            if self.getProp("MinItStart") < 9:
                if not self.getProp("MajItStart") == 0:
                    self.setProp("MajItStart", 0)
                    print(
                        "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
                    )
            else:
                majstartit = (
                    self.getProp("MinItStart")
                    - (self.getProp("MinItStart") % self.getProp("MajItStart"))
                ) / self.getProp("MajItStart")
                if not self.getProp("MajItStart") == majstartit:
                    self.setProp("MajItStart", majstartit)
                    print(
                        "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
                    )
        if self.getProp("magnFactorsMode") == 0 and self.getProp("magnifDir") == "":
            self.setProp("magnifDir", "/group/rich/AlignmentFiles/MagnifFactors/Rich1/")
            print(
                "Warning: Mode for calculating magnification coefficients was chosen to be 0 but no directory with pre-determined coefficients provided. Directory set to default."
            )
        if self.getProp("magnFactorsMode") == 0:
            self.setProp("MinItStart", self.getProp("MajItStart") * 9)

    def __apply_configuration__(self):
        self.setHistoDir()
        self.setNameStr()
        self.checkStartIt()
        # Next 3 lines add the trailing slash in key directory names
        self.setProp("WorkDir", os.path.join(self.getProp("WorkDir"), ""))
        self.setProp("SaveDir", os.path.join(self.getProp("SaveDir"), ""))
        self.setProp("magnifDir", os.path.join(self.getProp("magnifDir"), ""))
        print("INFO: Rich1MirrAlignOnConf applied")


class Rich2MirrAlignOnConf(LHCbConfigurableUser):
    __used_configurables__ = []

    __slots__ = {
        #### Do not ever change these!!! Just DONT!
        "Rich": 2,
        "HistoDir": "",
        #### Only change these if you want to pick up from where you left off in iterations, experts only
        "MajItStart": 0,
        "MinItStart": 0,
        ### Better not change these, only for very very VERY good reasons
        "WorkDir": "/group/online/AligWork/Rich2/",
        "SaveDir": "/group/online/AligWork/MirrorAlignments/",
        "ItNrFile": "/group/online/dataflow/options/LHCbA/Rich2_Alignment_Reference_File.txt",
        "thisCase": "online",
        "tiltNames": [
            "",
            "pri_negYzerZ",
            "pri_posYzerZ",
            "pri_zerYnegZ",
            "pri_zerYposZ",
            "sec_negYzerZ",
            "sec_posYzerZ",
            "sec_zerYnegZ",
            "sec_zerYposZ",
        ],
        ### May be changed if you know what you are doing
        "combAndMirrSubsets": "/group/rich/sw/cmtuser/AlignmentRelease/Rich/RichMirrorAlignmentOnline/files/Rich2CombAndMirrSubsets.txt",
        "startXMLFile": "",  # "/group/rich/AlignmentFiles/databaseAlignments/Rich2/Rich2_Zeroed.xml" # if testing is not True OR this is blank, it automatically uses the DB XML file
        "compareXMLFile": "",  # if this is blank, it automatically uses the DB XML file
        "coeffCalibTilt": 0.3,
        "minAverageBinPop": 6,
        "phiBinFactor": 3,
        "minFracPhiBinsPopulated": 0.7,  # adjusted from 0.8 due to inefficiencies in our existing choice of HLT line prescales
        "deltaThetaWindow": 4.0,
        "combinFitMethod": 5,
        "magnFactorsMode": 0,
        "magnifDir": "/group/rich/AlignmentFiles/MagnifFactors/Rich2/20170613_083436/",
        "solutionMethod": 0,
        "regularizationMode": 0,
        "stopToleranceMode": 1,
        "nominalResolutionSigma": 0.656,  # from https://lblogbook.cern.ch/RICH/13678 (and still seems reasonable throughout 2017)
        "stopSigmaFraction": 0,
        "maxIterations": 8,
        "requiredEvents": 942500,  # ~70% of 1346563
        "autoUpdate": True,
        "autoUpdateNoV": False,
        "testing": True,
        "trendDateRange": {"minDate": 20170605, "maxDate": 20171129},
        # 2017 tolerances: [0.032, 0.030, 0.043, 0.057], adjusted to [0.03, 0.03, 0.05, 0.06]
        "stopTolerancePriY": 0.03,
        "stopTolerancePriZ": 0.03,
        "stopToleranceSecY": 0.05,
        "stopToleranceSecZ": 0.06,
        "warningFactor": 3,
        "EvtMax": -1,
        "dataVariant": "Collision17",
        "fixSinusoidShift": 1,
        "nameStr": "",
        "displayMode": 1,
        "xmlMDCS": "",  # "/group/rich/AlignmentFiles/MDCS/Conditions/Rich1/Environment/HPD.xml" # 'Rich1' is not a typo!
    }

    _propertyDocDct = {
        "MajItStart": """ Start the alignment at this major iteration. Please make sure this is consitent with MinItStart. """,
        "MinItStart": """ Start the alignment at this minor iteration. Please make sure this is consitent with MajItStart. """,
        "Rich": """ Rich1 or Rich2. """,
        "HistoDir": """ Directory where the savesets are being written to. """,
        "WorkDir": """ Directory in which all the output will be written. """,
        "SaveDir": """ Directory in which all output files of the alignment are saved after it has finished """,
        "ItNrFile": """ File which contains only the number of the current iteration written by the iterator, to be read by the analyzers """,
        "thisCase": """ Right now this is used only for the naming convention. """,
        "tiltNames": """ Combination of tilts for the calculation and usage of the magnification factors. This is needed even when using predefined magnification factors!!! """,
        "combAndMirrSubsets": """ File that lists the subset of combinations of primary and secondary mirror segments chosen for alignment monitoring, and all mirror segments under alignment monitoring """,
        "startXMLFile": """ ONLY IF testing = True, this is the XML file with the mirror tilts that we start with for the alignment; if it is blank or testing is not True, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "compareXMLFile": """ XML file with mirror tilts, for comparison of mirror tilts before and after alignment; if it is blank, it uses the latest v-numbered xml (which is what should be currently in the DB) """,
        "coeffCalibTilt": """ Mirror-tilts applied for the calculation of the magnification coefficients. """,
        "minAverageBinPop": """ Demanded minimal entries per bin in x-y-bin; atm 6: for 20 phi-bins, 4.8: for 25 phi-bins. """,
        "phiBinFactor": """ Factor by which the number of phi-bins is reduced. The histograms should now come with 60 phi bins, factor 3 reduced this to 20 bin in the fit.""",
        "minFracPhiBinsPopulated": """ Fraction of the phi-bins that should be sufficiently populated, based on the current minAverageBinPop setting. Ideally this is 0.8, but sometimes we lower it if the mirror alignment still seems OK with a lower fraction, though we really should adjust the HLT line instead.""",
        "deltaThetaWindow": """ dTheta-range in the histograms; 4.0 for Rich1 and 3.0 for Rich2. """,
        "combinFitMethod": """ Method for fitting the 2D histograms; 1: first fit the slices of dTheta with a Gaussian and then fit to phi using only the mean of the Gaussian, 3: fit a 2D function, 5: Same as 3 but with unifyWidths """,
        "magnFactorsMode": """ Method for calculating the magnification coefficents; 0: magnefication coeff. pre-determined, 1: individual for all mirror pairs, determined in the 0-th iteration and not updated any further, 2: individual for all mirrorpairs, determined on-the-fly on the data. """,
        "magnifDir": """ directory that contains the pre-determined magnification coeff., files in this folder have to be named Rich[1,2]_MirrMagn _[tiltName]_predefined.txt """,
        "solutionMethod": """ Solution method for calculating the mirror tilts after the individual fits; 0: Minuit, 1: Algebraic """,
        "regularizationMode": """ regularization mode (0/10/1/11): regularization terms magnified (weighted) with: 0(def)-> major & minor per-mirror MFs; 10-> only averaged per-kind major MFs; 1 (and 11, respectively)-> all weights = 1.       """,
        "stopToleranceMode": """ tolerance mode: default 0: calculate the stopTolerances from nominal sigma, average magn factors and stopSigmaFraction, 1 use stopTolerances given below""",
        "nominalResolutionSigma": """ RICH-dependent nominal Cherenkov angle resolution (in mrad)""",
        "stopSigmaFraction": """ tolerance for total tilt of mirror pair in terms of the fraction of the nominal Cherenkov angle resolution """,
        "maxIterations": """ The number of iterations the mirror alignment will run, before giving up """,
        "requiredEvents": """ If this many events are not procesed by the farm, on average, we do run the alignment but we will never update it. """,
        "autoUpdate": """ If True, then the fully automated updating system is activated. If False, our DB update service is still active but returns the starting alignment version. """,
        "autoUpdateNoV": """ Only looked at if autoUpdate is True. Then, if autoUpdateNoV is False, nothing changes. If autoUpdateNoV is True, then rather than sending the existing alignment version if autoUpdate is disabled *and* there is a _maybe_ alignment, we will send no version through the DB update service. HLT2 will have to wait for us to assign one. """,
        "testing": """ If True, then we are running a test alignment; we also disable our DB update service completely. """,
        "trendDateRange": """ Set the date range for the automatic trend plots. Format is YYYYMMDD. Additionally, a minDate(maxDate) of None defaults to the first(last) day of the current year.""",
        "stopTolerancePriY": """ RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopTolerancePriZ": """ RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment """,
        "stopToleranceSecY": """ RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment """,
        "stopToleranceSecZ": """ RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment """,
        "warningFactor": """ alerts the alignment if any of the mirrors have shifted more than warningFactor times a particular mirror rotation stopTolerance """,
        "EvtMax": """ maximal number of events processed by brunel PER NODE (hlt farm has ~1500 nodes), -1 = infinite """,
        "dataVariant": """ Helps us decide which Brunel options to use; also used for the naming convention. """,
        "fixSinusoidShift": """ 1 : fix at sinusoidShift; 0 : it is not fixed """,
        "nameStr": """ Right now this is used only for the naming convention, but will be set when initiating the Configuration. """,
        "displayMode": """ 1 : Current TH2D display style (auto-scaling Z-axis, granular colors, using same colors for > 4*threshold); 0: Anatoly's development TH2D display style (similar, but with printed mirror number also, and fixed scale)""",
        "xmlMDCS": """ Location of MDCS conditions XML file, for override if needed. Not used if blank. """,
    }

    ### Dont mess with this filename!!!
    def setHistoDir(self):
        import time

        year = time.strftime("%Y")
        histodir = (
            "/hist/Savesets/"
            + str(year)
            + "/LHCbA/AligWrk_Rich"
            + str(self.getProp("Rich"))
            + "/"
        )
        self.setProp("HistoDir", histodir)

    def setNameStr(self):
        thisTuning = "Mp" + str(self.getProp("minAverageBinPop"))
        thisTuning += "Wi" + str(self.getProp("deltaThetaWindow"))
        thisTuning += "Fm" + str(self.getProp("combinFitMethod"))
        thisTuning += "Mm" + str(self.getProp("magnFactorsMode"))
        thisTuning += "Sm" + str(self.getProp("solutionMethod"))
        thisNameStr = (
            thisTuning
            + "_"
            + str(self.getProp("thisCase"))
            + "_"
            + str(self.getProp("dataVariant"))
        )
        self.setProp("nameStr", thisNameStr)

    def checkStartIt(self):
        if not self.getProp("magnFactorsMode") == 0:
            if self.getProp("MinItStart") < 9:
                if not self.getProp("MajItStart") == 0:
                    self.setProp("MajItStart", 0)
                    print(
                        "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
                    )
            else:
                majstartit = (
                    self.getProp("MinItStart")
                    - (self.getProp("MinItStart") % self.getProp("MajItStart"))
                ) / self.getProp("MajItStart")
                if not self.getProp("MajItStart") == majstartit:
                    self.setProp("MajItStart", majstartit)
                    print(
                        "Warning: Major Iteration did not match minor itertion. Major iteration was set to match minor iteration."
                    )
        if self.getProp("magnFactorsMode") == 0 and self.getProp("magnifDir") == "":
            self.setProp("magnifDir", "/group/rich/AlignmentFiles/MagnifFactors/Rich2/")
            print(
                "Warning: Mode for calculating magnification coefficients was chosen to be 0 but no directory with pre-determined coefficients provided. Directory set to default."
            )
        if self.getProp("magnFactorsMode") == 0:
            self.setProp("MinItStart", self.getProp("MajItStart") * 9)

    def __apply_configuration__(self):
        self.setHistoDir()
        self.setNameStr()
        self.checkStartIt()
        # Next 3 lines add the trailing slash in key directory names
        self.setProp("WorkDir", os.path.join(self.getProp("WorkDir"), ""))
        self.setProp("SaveDir", os.path.join(self.getProp("SaveDir"), ""))
        self.setProp("magnifDir", os.path.join(self.getProp("magnifDir"), ""))
        print("INFO: Rich2MirrAlignOnConf applied")


# ===============================================================================
# regularizationMode has changed from 1 (used in 2016) to 0 (used in 2017).
# Looking back at the functional we minimize:
#   https://indico.cern.ch/event/477524/contributions/2174895/attachments/1276535/1894128/richmirralign_update_2016-05-20_rich_sw.pdf
# Page 3 of the PDF has the 2016 functional
# Page 4 of the PDF has the 2017 functional
# One can see that the mirror rotations (alpha, beta) in the last term of the 2017 functional get weighted by the average magnification factors, whereas in 2017 they did not.
#
# From a quick look at the last terms of those functionals, and using the typical average magnification factors shown on slide 4,
# Paras concludes that our tolerances should be adjusted for 2017 as follows:
#
# [primary y, primary z, secondary y, secondary z] tolerances
# What we decided for RICH1 from 2016 study: [0.050, 0.060, 0.250, 0.300] mrad
# In light of this, the RICH1 tolerances for 2017 should be set to: [0.027, 0.030, 0.455, 0.370] mrad
# What we decided for RICH2 from 2016 study: [0.065, 0.055, 0.045, 0.035] mrad
# In light of this, the RICH2 tolerances for 2017 should be set to: [0.032, 0.030, 0.043, 0.057] mrad
# ===============================================================================

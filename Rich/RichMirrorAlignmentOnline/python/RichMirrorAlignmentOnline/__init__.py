###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Import required modules
import glob
import inspect
import os

# Get this module's source code directory
module_file = inspect.getfile(inspect.currentframe())
module_dir = os.path.dirname(module_file)

print(f"INFO: RichMirrorAlignmentOnline package directory: {module_dir}")

# Get the list of all .py file names in the package directory
files = glob.glob(os.path.join(module_dir, "*.py"))

# Extract the module names from the file names
module_names = [os.path.basename(file)[:-3] for file in files if not "__init__" in file]

# Define the __all__ variable as the list of module names
__all__ = module_names
print(f"INFO: RichMirrorAlignmentOnline package modules: {__all__}")

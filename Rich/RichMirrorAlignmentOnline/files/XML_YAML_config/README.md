# XML <-> YAML conversion tool
## Introduction

The XML YAML conversions tool allows for quick conversions between XML and YAML.
This tool will take any XML or YAML file and output the data in the same location
with the same filename, but the opposite file type.

The conversion tool supports additional features:
* A blocklist of items that should not be included in the output file
* A conversion table for converting parameter names and units between file types
* A numerical unit conversions table for numerical conversions between units
* A custom yaml parser that allows the converter to parse custom yaml tags


**WARNING:** Do not parse or convert YAML files from untrusted sources. The
parser used in this tool has the ability to construct arbitrary python objects
which may be harmful.
## REQUIREMENTS
The tool requires a working version of Python 3 with additional modules:
* PyYaml
* lxml
* numpy version 1.21.0 or later
## CONFIGURATION
The blocklist, conversion table, unit conversions table, and custom yaml parser
can be configured through the text files in "Configuration_files".
### Conversion Tool
Conversion is a python object that handles all file conversions between XML and

YAML. The object is initiated as a class instance and can then be used to make
any necessary conversions. A XML <-> YAML conversions are run by calling
"conv_XML" and "conv_YAML" respectively and passing the relevant file path (see
examples below).

**Note:** Conversion requires the user to add the absolute path to the directory
"Configuration_files" on line 5 in "convert_XML_YAML.py" prior to any use.
#### Examples

The conversions tool may be initiated under the variable "conversion_variable":

    conversions_variable = Conversion()

To convert an XML file to YAML the absolute file path is passed to the conv_XML:

    conversions_variable.conv_XML(r"absolute_path_to_file_dir/filename.xml")

This results in the file "absolute_path_to_file_dir/filename.yml" being created.

To convert a YAML file to XML the absolute file path is passed to conv_YAML:

    conversions_variable.conv_YAML(r"absolute_path_to_file_dir/filename.yml")

### Blocklist
The blocklist is a list of illegal node names that will be blocked when
converting. It is configured through the file "blocklist_config.txt" which
contains a list of names to ignore. Each new line in the document denotes a new
illegal node name, node beginning, node ending or node wildcard. Illegal node names are denoted
by typing in the full node name. Illegal node beginnings are denoted by typing in
the beginning followed by a "-". Illegal node endings are denoted by typing in
the ending and adding a "-" to the front of it. Illegal node wildcards are denoted by typing in a "*"
followed by the wildcard (if the node contains the wildcard string, the node will be removed). ```blisted_write_file``` can be set to ```True``` or ```False``` to write blocklisted conditions to file when converting from xml->yaml (to be added back when converting back to xml). ```reAdd_blocklisted_conditions``` can be set to ```True``` or ```False``` to re-add xml conditions which were removed when converting from xml->yaml (created with ```blisted_write_file = True```).



**Note:** All node names are case sensitive and "-" ("*") is an illegal character in a
node name because it is used to define beginnings and endings (wildcards).
#### Examples

* "and" will block "and" but not "hand", "andrew", or "And"

* "and-" will block "and" and "andrew" but not "hand" or "And"

* "-and" will block "and" and "hand" but not "andrew" or "And"

* "*and" will block "and" and "hand" but not "And"

### Parameter and Unit conversions
The param_conversion_config.txt file contains translations for parameter names
and units between XML and YAML. Each line is defines a set containing the
parameter name in XML followed by a ":", then the base unit of that parameter in
XML followed by a ":", then the corresponding parameter in YAML followed by a
":", and finally the base unit of that parameter in YAML followed by a ":".
Everything after the 4th ":" on that line is ignored.

**Note:** If the values in the XML file have specified units when converting XML
-> YAML then this will be used instead of the XML base unit.
### Numerical Unit Conversions
The unit_conversions_config.txt file contains the numerical conversions between
units. Each line contains the a unit symbol followed by a ":" then the dimensions
of the unit in base SI units followed by a ":" then the value of the unit
relative to its base unit followed by a ":". Everything after the 3rd ":" is
ignored.

**Note:** all unit names and SI unit names are case sensitive and whitespace is
NOT ignored. If complex units with more than one base SI unit are used then
make sure that compatible units have IDENTICAL base units.
#### Examples
* "mrad:rad:0.001:" a milliradian has its base in radians and is 0.001 radians
* "rad:rad:1.0:" a radian has its base in radians and is 1.0 radians
* "N:kg\*m\*s^2:1.0:" a newton has its base in kg*m*s^2 and is 1.0 kg*m*s^2
    * Note: "kg\*m\*s^2" will be interpreted differently from "kg m s^2" and
    "kg\*m\*s\*s" as compatible base units must be identical.
### Custom YAML parser and tags
The custom_tags.txt file contains a list of custom tags used in the YAML
files where each line contains a single custom tag.

**Note:** The YAML parser will only work if all custom tags are added.

### Tacking on (or off) extra lines of text
The ```tack_on_off``` function in ```objects.py``` prepends or appends (or removes) text to the start or end of the file. These lines are manually added in yaml_tack_on_Rich_X.txt or xml_tack_on_Rich_X.txt (where X = 1 or 2), stored in the Configuration folder (defined in ```config_path```). Setting ```tack_on == True``` tacks on the lines and setting ```tack_on == False``` removes the lines.

* Everything below ```'---top'``` (and above ```'---bottom'```) will be prepended to the top of the file (or removed, if ```tack_on == False```).
* Everything below ```'---bottom'``` will be appended to the bottom of the file (or removed, if ```tack_on == False```).

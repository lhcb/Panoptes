###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Uncomment this to change the timestamp (you do not want to do this when the timestamp YAML has been provided,
#     and you are working on the same mirror alignment procedure [whole set of iterations]):
#
## initialize (once per an entire alignment)
#../stack/Panoptes/run python rich1_session_timestamp.py
#
# Check hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py file to make sure you will be using the right branch of the conditions database for each iteration!
#
# Uncomment these to run i0 (untilted) and make the summary plot [change 0 where needed to another integer <= 8 for a different iteration]:
#
#echo -n 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_i0.txt
#
# summary plot for _i0 (optional)
#../stack/Panoptes/run python fit_hist.py 1 2022-12-06T17-55_rich1_minp30_subset_Collision22_25k_Produce_histos_i0.root
#mv Rich1_AlignSummary.pdf Rich1_AlignSummary_i0.pdf
#
# Uncomment to perform one of the 8 needed mirror tilts for any iteration where we recalculate the magnification factors (priYn for iteration 0):
#
#echo -n priYn 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_priYn_i0.txt
#
# Here are the other 7 tilts for i0 also commented out:
#
#echo -n priYp 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_priYp_i0.txt
#echo -n priZn 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_priZn_i0.txt
#echo -n priZp 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_priZp_i0.txt
#echo -n secYn 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_secYn_i0.txt
#echo -n secYp 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_secYp_i0.txt
#echo -n secZn 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_secZn_i0.txt
#echo -n secZp 0 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_secZp_i0.txt
#
# Second iteration (i1)
#
#echo -n 1 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_i1.txt
#
# Third iteration (i2)
#
#echo -n 2 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich1_noUT_panoptes_dd4hep-both.py | tee logR1_i2.txt

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
from datetime import datetime

from PyConf.application import ApplicationOptions

filename_expr = re.compile(
    r"Run_(?P<run>\d{10})_HLT(?P<node>\d{5})_(?P<time>(?:\d{8})-(?:\d{6}))-(?P<rest>\d{3})\.mdf"
)

options = ApplicationOptions(_enabled=False)
options.simulation = False
options.conddb_tag = "master"
options.dddb_tag = "run3/trunk"
options.input_type = "MDF"

base_dir = "/eos/lhcb/hlt2/LHCb/0000255358/"


def sort_names(f):
    f = os.path.basename(f)
    m = filename_expr.match(f)
    if m is None:
        return m
    funs = [int, lambda t: int(datetime.strptime(t, "%Y%m%d-%H%M%S").timestamp()), int]
    return tuple(fun(m.group(k)) for fun, k in zip(funs, ("run", "time", "rest")))


files = [os.path.join(base_dir, f) for f in os.listdir(base_dir)]
files = sorted(files, key=sort_names)

options.input_files = files[:5]
print(options.input_files)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import csv
import datetime
import getpass
import glob
import math
import os
import re
import shutil
import sys
import time
from time import gmtime, strftime

from PyMirrAlignOnline.Communicator import *
from ruamel.yaml import YAML

print(f"RichAnalyzer __name__ = {__name__}")


class hlt2_reco_rich_noUT_panoptes_dd4hep_both:
    def __init__(self):
        self.whichRich = self.Rich()

    def Rich(
        self,
    ):  # RichAnalyzer must be running in the workDir for this to be correct
        def check_directory():
            cwd = os.getcwd()
            if "Rich1" in cwd:
                return 1
            elif "Rich2" in cwd:
                return 2
            else:
                return 2  # assume Rich2 if trying to somehow test this independently

        result = check_directory()
        print(
            f"INFO: RichAnalyzer whichRich = {result}, determined from current working directory (default = 2)"
        )
        return result

    def run(self):
        # main code
        whichRich = self.whichRich
        rich = f"rich{whichRich}"
        radiator = f"Rich{whichRich}Gas"

        if whichRich == 1:
            from Configurables import Rich1MirrAlignOnConf

            # from Configuration import Rich1MirrAlignOnConf
            alignConf = Rich1MirrAlignOnConf()
        elif whichRich == 2:
            from Configurables import Rich2MirrAlignOnConf

            # from Configuration import Rich2MirrAlignOnConf
            alignConf = Rich2MirrAlignOnConf()
        alignConf.__apply_configuration__()

        EvtMax = alignConf.getProp("EvtMax")  # this isn't currently used
        workdir = alignConf.getProp("WorkDir")
        useHltDecisions = alignConf.getProp("useHltDecisions")
        dataVariant = alignConf.getProp("dataVariant")

        runAnalyzer = "UNKNOWN"
        while runAnalyzer != "YES":
            yaml = YAML()
            while not os.path.exists(f"{workdir}rich{whichRich}_runAnalyzer.yml"):
                time.sleep(4)
            with open(f"{workdir}rich{whichRich}_runAnalyzer.yml") as finp:
                runAnalyzer = yaml.load(finp)
                print(
                    f"INFO: {workdir}rich{whichRich}_runAnalyzer.yml set to {runAnalyzer}"
                )
            if runAnalyzer != "YES":
                time.sleep(4)
        sys.stdout.flush()

        arguments = []
        if not sys.stdin.isatty():
            for line in sys.stdin:
                for word in line.split():
                    arguments.append(word)

        count = len(arguments)

        n_it = 0
        tiltName = ""

        if count == 0:
            yaml = YAML()
            with open(f"{workdir}rich{whichRich}_iter_number.yml") as finp:
                n_it = yaml.load(finp)
            yaml = YAML()
            with open(f"{workdir}rich{whichRich}_tilt_name.yml") as finp:
                tiltName = yaml.load(finp)
        else:
            # You can pipe in to stdin to change tiltName and n_it
            # echo -n <tiltName> <n_it> | Panoptes/run gaudirun.py ...
            # OR
            # must have pipe in to gaudirun.py to change n_it only
            # echo -n <n_it> | Panoptes/run gaudirun.py ...
            # otherwise tiltName = "" and n_it = 0

            # Since we are doing this by hand, specify the iteration number in stdin
            if count == 2 and arguments[1] in [
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
            ]:
                n_it = int(arguments[1])
            if count == 1 and arguments[0] in [
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
            ]:
                n_it = int(arguments[0])

            # possible tiltname can be given via stdin
            if count == 2 and arguments[0] in [
                "priYn",
                "priYp",
                "priZn",
                "priZp",
                "secYn",
                "secYp",
                "secZn",
                "secZp",
            ]:
                tiltName = arguments[0]
            # For Rich1
            # tiltName = "" # uses 'alignment2022'
            # tiltName = "priYn" # uses '2022_12_06_Rich1_priYn', a branch of 'alignment2022'
            # tiltName = "priYp" # uses '2022_12_06_Rich1
            # tiltName = "priZn" # uses '2022_12_06_Rich1
            # tiltName = "priZp" # uses '2022_12_06_Rich1
            # tiltName = "secYn" # uses '2022_12_06_Rich1
            # tiltName = "secYp" # uses '2022_12_06_Rich1
            # tiltName = "secZn" # uses '2022_12_06_Rich1
            # tiltName = "secZp" # uses '2022_12_06_Rich1
            # For Rich2
            # tiltName = "" # uses '2022_12_08_Rich2_2018'
            # tiltName = "priYn" # uses '2022_??_??_Rich2_priYn', a branch of '2022_12_08_Rich2_2018'
            # tiltName = "priYp" # uses '2022_??_??_Rich2
            # tiltName = "priZn" # uses '2022_??_??_Rich2
            # tiltName = "priZp" # uses '2022_??_??_Rich2
            # tiltName = "secYn" # uses '2022_??_??_Rich2
            # tiltName = "secYp" # uses '2022_??_??_Rich2
            # tiltName = "secZn" # uses '2022_??_??_Rich2
            # tiltName = "secZp" # uses '2022_??_??_Rich2

        print(f"INFO: Iteration: {n_it}")
        print(f"INFO: Tilt Name: {tiltName}")
        if tiltName not in [
            "",
            "priYn",
            "priYp",
            "priZn",
            "priZp",
            "secYn",
            "secYp",
            "secZn",
            "secZp",
        ]:
            print(f"ERROR:   This is not a valid tiltName")
        sys.stdout.flush()

        from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad
        from Moore import (
            options,
            run_reconstruction,
        )
        from Panoptes.alignment import (
            standalone_rich_online_align_reco,
            standalone_rich_panel_align_reco,
        )
        from PyConf.Algorithms import (
            PrForwardTrackingVelo,
            PrHybridSeeding,
            PrMatchNN,
            VeloRetinaClusterTrackingSIMD,
            VPRetinaFullClusterDecoder,
        )
        from RecoConf.legacy_rec_hlt1_tracking import (
            make_PatPV3DFuture_pvs,
            make_reco_pvs,
            make_velo_full_clusters,
            make_VeloClusterTrackingSIMD,
        )
        from RecoConf.rich_data_monitoring import (
            alignment_rich_monitoring_options,
            default_rich_monitoring_options,
        )
        from RecoConf.rich_reconstruction import default_rich_reco_options

        """Options for running over data with FT raw bank version 6."""
        from RecoConf.decoders import (
            default_ft_decoding_version,
        )

        options.scheduler_legacy_mode = False
        # options.use_iosvc = False
        options.n_threads = 20  # 1

        print(f"Using HLT decisions: {useHltDecisions}")

        useRealData = True

        print(f"INFO: Using Real Data: {useRealData}")

        if useRealData is True:
            options.simulation = False
            options.input_type = "MDF"
        else:
            options.set_input_and_conds_from_testfiledb(
                "upgrade_Sept2022_minbias_0fb_md_xdigi"
            )
            options.input_type = "ROOT"

        useDD4Hep = True

        print(f"INFO: Using DD4Hep: {useDD4Hep}")

        if useDD4Hep:
            from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
            from DDDB.CheckDD4Hep import UseDD4Hep
        else:
            UseDD4Hep = False

        useCondDBPath = True

        print(f"INFO: Using CondDBPath: {useCondDBPath}")
        sys.stdout.flush()

        if UseDD4Hep:
            from Configurables import DDDBConf

            if "Collision23" in dataVariant:
                dd4hep = DD4hepSvc(
                    DetectorList=["/world", "Magnet", "VP", "FT", "Rich1", "Rich2"]
                )
            elif "Collision22" in dataVariant:
                dd4hep = DD4hepSvc(
                    DetectorList=[
                        "/world",
                        "Magnet",
                        "UT",
                        "VP",
                        "FT",
                        "Rich1",
                        "Rich2",
                    ]
                )
            dd4hep.OutputLevel = 1

            richFilesDir = alignConf.getProp(
                "richFiles"
            )  # '/group/rich/AlignmentFiles/'
            if useCondDBPath:
                if "Collision23" in dataVariant:
                    conddb_path = (
                        f"{richFilesDir}lhcb-conditions-database_master_CloneForRich"
                    )
                elif "Collision22" in dataVariant:
                    conddb_path = f"{richFilesDir}lhcb-conditions-database_alignment2022_SingleNodeIterator"
                dd4hep.ConditionsLocation = f"file://{conddb_path}"
            else:
                dd4hep.ConditionsLocation = "git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"

            if useRealData:
                options.conddb_tag = "master"  # used for real data
                options.dddb_tag = "run3/trunk"
                DDDBConf().GeometryVersion = "run3/trunk"  # used for real data

                # Which branch?
                dd4hep.UseConditionsOverlay = True
                if useCondDBPath:
                    if "Collision23" in dataVariant:
                        dd4hep.ConditionsVersion = "master"
                    elif "Collision22" in dataVariant:
                        dd4hep.ConditionsVersion = "alignment2022"

                    # PN - Here we only want to replace the
                    #      latest Mirrors YAML in the git clone DB
                    #      with the one that is currently staged
                    #      for reconstruction in the workdir
                    #
                    # We don't really need this part, unless we later choose to
                    # get the latest DB from the git clone DB
                    # (we currently get it from the online DB, I think)
                    # from subprocess import *
                    # cmd = "cd " + conddb_path + " ; git checkout . ; cd - "
                    # p = Popen(
                    #    cmd,
                    #    shell=True,
                    #    executable="/bin/bash")
                    # p.communicate()
                    # sys.stdout.flush()

                    re_version = re.compile(r"^([0-9]+)$")

                    def get_version(entry):
                        r = re_version.match(entry)
                        if not r:
                            return -1
                        else:
                            return int(r.group(0))

                    fromFile = f"{workdir}" + f"CondDB_Rich{whichRich}.yml"
                    latestName = max(
                        os.listdir(
                            f"{conddb_path}/Conditions/Rich{whichRich}/Alignment/Mirrors.yml/"
                        ),
                        key=get_version,
                    )
                    toFile = f"{conddb_path}/Conditions/Rich{whichRich}/Alignment/Mirrors.yml/{latestName}"
                    print(f"INFO: Copying {fromFile} to {toFile}")
                    shutil.copyfile(fromFile, toFile)
                    sys.stdout.flush()

                else:
                    if whichRich == 1:
                        if tiltName == "" and n_it == 0:
                            dd4hep.ConditionsVersion = f"alignment2022"
                        elif n_it >= 1:
                            dd4hep.ConditionsVersion = (
                                f"2022_12_06_Rich{whichRich}_i{n_it}_new"
                            )
                        else:
                            dd4hep.ConditionsVersion = (
                                f"2022_12_06_Rich{whichRich}_{tiltName}"
                            )
                    else:
                        if tiltName == "" and n_it == 0:
                            dd4hep.ConditionsVersion = (
                                f"2022_12_08_Rich{whichRich}_2018"
                            )
                        elif n_it >= 1:
                            dd4hep.ConditionsVersion = (
                                f"2022_12_08_Rich{whichRich}_i{n_it}_new"
                            )
                        else:
                            dd4hep.ConditionsVersion = (
                                f"2022_12_08_Rich{whichRich}_{tiltName}"
                            )

            else:
                options.conddb_tag = (
                    "jonrob/all-pmts-active"  # used for DetDesc MC samples with DD4Hep
                )
                DDDBConf().GeometryVersion = "before-rich1-geom-update-26052022"  # used for DetDesc MC samples with DD4Hep

        else:
            options.conddb_tag = "upgrade/sim-20220612-vc-md100-RICHcustomFTv6"
            options.simulation = True

        param_list = ["p0_x", "p1_x", "p0_y", "p1_y", "p0_z", "p1_z"]
        param_dict = dict.fromkeys(param_list)

        # Only edit bad_align to play with alignment parameters
        if whichRich == 1:
            perf_align = [0.0, 0.0, -30.15, 30.15, 0.0, 0.0]
            bad_align = [0.0, 0.0, -30.15, 30.15, 0.0, 0.0]
        else:
            perf_align = [-1.32, 1.41, -4.74, -3.92, 4.79, 2.57]
            bad_align = [-1.32, 1.41, -4.74, -3.92, 4.79, 2.57]
        for row, param in zip(perf_align, param_dict.keys()):
            param_dict[param] = float(row)

        if UseDD4Hep:
            pass
        else:
            if whichRich == 1:
                UpdateManagerSvc().ConditionsOverride += [
                    f"Conditions/Alignment/Rich1/PDPanel0_Align :=  double_v dPosXYZ = {param_dict['p0_x']} {param_dict['p0_y']} {param_dict['p0_z']};",
                    f"Conditions/Alignment/Rich1/PDPanel1_Align :=  double_v dPosXYZ = {param_dict['p1_x']} {param_dict['p1_y']} {param_dict['p1_z']};",
                ]
            else:
                UpdateManagerSvc().ConditionsOverride += [
                    f"Conditions/Alignment/Rich2/PDPanel0_Align :=  double_v dPosXYZ = {param_dict['p0_x']} {param_dict['p0_y']} {param_dict['p0_z']};",
                    f"Conditions/Alignment/Rich2/PDPanel1_Align :=  double_v dPosXYZ = {param_dict['p1_x']} {param_dict['p1_y']} {param_dict['p1_z']};",
                ]

        default_moni_opts = {}
        default_reco_opts = {}
        align_opts = {}

        # Reco opts
        wider_bkg = {"PhotonSelection": "None"}
        default_reco_opts.update(wider_bkg)
        panel_select = {"ActivatePanel": (True, True)}
        default_reco_opts.update(panel_select)

        # Get the variant from the Iterator
        yaml = YAML()
        with open(f"{workdir}/rich{whichRich}_variant.yml") as finp:
            current_variant = yaml.load(finp)

        # Determine evt_max

        # Find index of "k_"
        k_index = current_variant.find("k_")
        # Check if "k_" is found in the input string
        if k_index != -1:
            # Find index of last "_" before "k_"
            underscore_index = current_variant.rfind("_", 0, k_index)
            # Check if "_" is found before "k_"
            if underscore_index != -1:
                # Extract the substring between the last "_" and "k_"
                num_str = current_variant[underscore_index + 1 : k_index]
                try:
                    if "O" in num_str:  # this is O not 0
                        options.evt_max = EvtMax
                        if options.evt_max == -1:
                            print(
                                f"INFO: evt_max is set to -1 (all events) from Configuration."
                            )
                        else:
                            print(
                                f"INFO: evt_max is set to {options.evt_max} events from Configuration."
                            )
                    else:
                        # Convert the extracted substring to an integer
                        num = int(num_str)
                        # Multiply the extracted number by 1000 and set options.evt_max
                        options.evt_max = num * 1000
                        # Print the result
                        print(
                            f"INFO: evt_max is extracted as {options.evt_max} events from current_variant."
                        )
                except ValueError:
                    print("ERROR: Unable to convert extracted substring to integer.")
            else:
                print("ERROR: '_' not found before 'k_'.")
        else:
            print("ERROR: 'k_' not found in current_variant.")
        sys.stdout.flush()

        # Monitoring opts

        # Find index of "minp"
        minp_index = current_variant.find("minp")
        # Check if "minp" is found in the input string
        if minp_index != -1:
            # Find index of "_" after "minp"
            underscore_index = current_variant.find("_", minp_index)
            # Check if "_" is found after "minp"
            if underscore_index != -1:
                # Extract the substring between "minp" and "_"
                num_str = current_variant[minp_index + len("minp") : underscore_index]
                try:
                    # Convert the extracted substring to a float (double)
                    minP = float(num_str)
                    print(
                        f"INFO: Extracted track momentum cut as {minP} GeV from current_variant."
                    )
                except ValueError:
                    print("ERROR: Unable to convert extracted substring to float.")
            else:
                print("ERROR: Underscore not found after minp.")
        else:
            print("ERROR: minp not found in input string.")
        sys.stdout.flush()

        PrForwardTrackingVelo.global_bind(MinP=minP * GeV)
        PrMatchNN.global_bind(MinP=minP * GeV)
        PrHybridSeeding.global_bind(MinP=minP * GeV)

        tighter_minp = {
            "TightTrackSelection": {
                "MinP": minP * GeV,
                "MinPt": 0.5 * GeV,
                "MaxChi2": 2.0,
                "MaxGhostProb": 0.1,
            }
        }
        default_moni_opts.update(tighter_minp)
        wider_histo = {"CKResHistoRange": (0.025, 0.005, 0.004)}
        default_moni_opts.update(wider_histo)
        useUT = {"UseUT": False}
        default_moni_opts.update(useUT)

        PrForwardTrackingVelo.global_bind(
            MinQuality=0.0,
            DeltaQuality=0.0,
            MinTotalHits=9,
            MaxChi2PerDoF=50.0,
            MaxChi2XProjection=60.0,
            MaxChi2PerDoFFinal=28.0,
            MaxChi2Stereo=16.0,
            MaxChi2StereoAdd=16.0,
        )

        make_VeloClusterTrackingSIMD.global_bind(
            algorithm=VeloRetinaClusterTrackingSIMD
        )
        make_velo_full_clusters.global_bind(
            make_full_cluster=VPRetinaFullClusterDecoder
        )
        make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)
        """
        # Possible mirror align tasks are:
        mirror_align_tasks = [
            'Produce',    # fill the production set of histograms
            #'Monitor',   # add various checking histograms
            #'Map',       # add counters for creation of the HLT1 pre-selection line "map"
            #'Optimize',  # add counters for optimization of the RICH2 mirror combinations subset
            #'Select',    # check filling the rest of RICH2 miralignment_rich_reco_optionsror combinations along with 8 poorest
            #'Calibrate', # check filling all RICH2 mirror combinations with elimination when filled
            # 'Explore',   # explore influence of RICH1 coordinate systems on distribution shapes
        ]
        """

        # Define the regular expression pattern to match the desired substring
        pattern = r"_(O|\d+)k_([^_]+)"

        # Search for the pattern in the input string
        match = re.search(pattern, current_variant)

        if match:
            # Extract the number and subsequent substring from the match object
            # number = match.group(1)
            substr = match.group(2)

            # Split the subsequent substring by "_" and store in a list
            mirror_align_tasks = substr.split("_")

            # print("Number:", number)
            print(
                f"INFO: mirror_align_tasks = {mirror_align_tasks} extracted from current_variant."
            )
        else:
            print("ERROR: mirror_align_tasks pattern not found in current_variant.")
        sys.stdout.flush()
        """
        if 'md' in options.conddb_tag:
            mp = 'md'
        elif 'mu' in options.conddb_tag:
            mp = 'mu'
        else:
            mp = 'Collision22'

        n_kevts = f"{math.trunc(options.evt_max / 1000)}k"

        addinfo = f'_minp{math.trunc(minP)}_subset_' + mp + "_" + n_kevts
        if useHltDecisions:
            addinfo = f"_minp{math.trunc(minP)}_{mp}_{n_kevts}"

        # cumulative substring with all tasks chosen
        tasks = ''
        for task in mirror_align_tasks:
            tasks += f"_{task}"

        # create timestamp
        # now = datetime.datetime.now()
        # dt = now.isoformat(timespec='minutes')  # Anatoly's old timestamp
        # dt = dt.replace(":", "-")  # Anatoly's new timestamp
        # retrieve the timestamp
        yaml = YAML()
        with open(f"./rich{whichRich}_session_timestamp.yml") as inp:
            ts = yaml.load(inp)

        # form the current variant name
        current_variant = f"{ts}_rich{whichRich}{addinfo}{tasks}"
        """

        if "subset" in current_variant:
            if whichRich == 1:
                central_mirr_combs = {
                    "PrebookHistos": [
                        ("p00", "s03"),
                        ("p01", "s06"),
                        ("p02", "s12"),
                        ("p03", "s09"),
                    ],
                }
            else:
                central_mirr_combs = {
                    "PrebookHistos": [
                        ("p17", "s13"),
                        ("p47", "s35"),
                        ("p17", "s09"),
                        ("p47", "s31"),
                        ("p16", "s12"),
                        ("p46", "s34"),
                        ("p16", "s08"),
                        ("p46", "s30"),
                        ("p13", "s10"),
                        ("p43", "s31"),
                        ("p13", "s09"),
                        ("p43", "s30"),
                        ("p12", "s09"),
                        ("p42", "s30"),
                        ("p12", "s08"),
                        ("p42", "s29"),
                        ("p09", "s09"),
                        ("p39", "s31"),
                        ("p09", "s05"),
                        ("p39", "s27"),
                        ("p08", "s08"),
                        ("p38", "s30"),
                        ("p08", "s04"),
                        ("p38", "s26"),
                    ],
                }
            align_opts.update(central_mirr_combs)

        align_opts.update({"MinP4Align": minP * GeV})

        # if 'theta2' in current_variant:
        #    align_opts.update({"DeltaThetaRange": 0.002})

        align_opts.update({"PoorestPopulation": 279.0})

        if tiltName == "":
            connectStr = ""
        else:
            connectStr = "_"

        # save output file with histograms
        if UseDD4Hep:
            # options.histo_file = f'rich{whichRich}_opt_dd4hep.root'
            options.histo_file = (
                f"{workdir}{current_variant}_histos{connectStr}{tiltName}_i{n_it}.root"
            )
        else:
            options.histo_file = f"rich{whichRich}_opt.root"

        # prepare part of the overridden options in use
        # to be recorded into a YAML file
        align_opts_dump = alignment_rich_monitoring_options(
            radiator=radiator, init_override_opts=align_opts
        )

        # add overridden default_rich_monitoring_options
        # to the part of the options in use to be recorded into the YAML file
        align_opts_dump.update(
            default_rich_monitoring_options(init_override_opts=default_moni_opts)
        )

        # retrieve the timestamp
        ts = re.findall(r"[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}-[\d]{2}", current_variant)[0]

        # prepare options in use to be recorded into the YAML file
        opts_dump = {}
        opts_dump[ts] = {}
        opts_dump[ts]["reco_opts"] = align_opts_dump

        # append YAML file with the part of the options in use
        with open(f"{workdir}{rich}_reco_opts.yml", "a") as out:
            yaml.dump(opts_dump, out)
        """
        # To get the JSON right (old)
        from AllenCore.configuration_options import is_allen_standalone
        is_allen_standalone.global_bind(standalone=True)
        sys.path.append(os.environ.get('ALLEN_INSTALL_DIR')+'/python/AllenSequences/')
        import hlt1_pp_no_gec_no_ut
        #import hlt1_pp_forward_then_matching_no_ut_no_gec
        #import passthrough
        """
        """
        # To get the JSON right
        from PyConf.Algorithms import LHCb__UnpackRawEvent, HltDecReportsDecoder, HltSelReportsDecoder  #, ApplicationMgr
        #from PyConf.application import configured_ann_svc
        unpacker = LHCb__UnpackRawEvent(
            "UnpackRawEvent",
            OutputLevel=2,
            RawBankLocations=[
                "DAQ/RawBanks/HltDecReports", "DAQ/RawBanks/HltSelReports"
            ],
            BankTypes=["HltDecReports", "HltSelReports"])
        decDec = HltDecReportsDecoder(
            "HltDecReportsDecoder/Hlt1DecReportsDecoder",
            SourceID="Hlt1",
            RawBanks=unpacker.RawBankLocations[0])
        selDec = HltSelReportsDecoder(
            "HltSelReportsDecoder/Hlt1SelReportsDecoder",
            SourceID="Hlt1",
            DecReports=unpacker.RawBankLocations[0],
            RawBanks=unpacker.RawBankLocations[1])
        #app = ApplicationMgr(
        #    TopAlg=[unpacker, decDec, selDec],
        #    ExtSvc=[configured_ann_svc(name='HltANNSvc')])
        """

        # by default, no additional filtering of the events
        event_filter = []

        # Prepare filter
        lines = (
            ["Hlt1RICH1AlignmentDecision"]
            if whichRich == 1
            else ["Hlt1RICH2AlignmentDecision"]
        )

        from PyConf.application import (
            default_raw_banks,
            default_raw_event,
        )

        options.input_raw_format = 0.5
        default_raw_event.global_bind(raw_event_format=options.input_raw_format)

        import Functors
        from PyConf.Algorithms import (
            HltDecReportsDecoder as PyConf_Algorithms_HltDecReportsDecoder,
        )
        from PyConf.Algorithms import (
            VoidFilter,
        )

        hlt1_dec_reports = PyConf_Algorithms_HltDecReportsDecoder(
            SourceID="Hlt1", RawBanks=default_raw_banks("HltDecReports")
        )

        hlt1_filter = VoidFilter(
            name="Streaming_filter",
            OutputLevel=1,
            Cut=Functors.DECREPORTS_FILTER(
                Lines=lines, DecReports=hlt1_dec_reports.OutputHltDecReportsLocation
            ),
        )

        # when particular decision of HLT1 line about RICH1 or RICH2 is wanted
        if useHltDecisions:
            # prepare filter for selecting events
            # chosen for alignment of mirrors of particular RICH
            print(f"INFO: Using HLT decisions.")
            print("")

            event_filter = [hlt1_filter]

            print(f"INFO: ***Applying Event Filter***")
            print(f"INFO: lines: {lines}")
            print(f"INFO: hlt1_filter: {hlt1_filter}")
            print(f"INFO: type(hlt1_filter): {type(hlt1_filter)}")
            print(f"INFO: event_filter: {event_filter}")
            print(f"INFO: ***************************")
            sys.stdout.flush()

        if "online" in getpass.getuser():
            utgid = os.environ["UTGID"]
            worker_id = utgid.split("_")[1]
            options.histo_file = f"{workdir}{current_variant}_histos{connectStr}{tiltName}_i{n_it}-{worker_id}.root"
            print(f"options.histo_file {options.histo_file}")
            sys.stdout.flush()

        with (
            standalone_rich_online_align_reco.bind(
                RichGas=radiator,
                MirrorAlignTasks=mirror_align_tasks,
                EventFilter=event_filter,
                noUT=True,
            ),
            alignment_rich_monitoring_options.bind(
                radiator=radiator, init_override_opts=align_opts
            ),
            default_ft_decoding_version.bind(value=6),
            default_rich_reco_options.bind(init_override_opts=default_reco_opts),
            default_rich_monitoring_options.bind(init_override_opts=default_moni_opts),
        ):
            run_reconstruction(options, standalone_rich_online_align_reco)


if __name__ == "builtins":
    RichAnalyzer_instance = hlt2_reco_rich_noUT_panoptes_dd4hep_both()
    RichAnalyzer_instance.run()

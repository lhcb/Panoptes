###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# initialize (once per an entire alignment)
#../stack/Panoptes/run python rich2_session_timestamp.py

# Check hlt2_reco_rich2_noUT_panoptes_dd4hep-both.py file to make sure you will be using the right branch of the conditions database for each iteration!

# First iteration (i0) -
#../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich2_noUT_panoptes_dd4hep-both.py | tee logR2_i0.txt

# Montioring example for i0 (optional)
#../stack/Panoptes/run python fit_hist.py 2 2022-12-06T17-55_rich2_minp60_subset_Collision22_200k_Produce_histos_i0.root
#mv Rich2_AlignSummary.pdf Rich2_AlignSummary_i0.pdf

# Second iteration (i1)
#echo -n 1 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich2_noUT_panoptes_dd4hep-both.py | tee logR2_i1.txt

#  (i2)
#echo -n 2 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich2_noUT_panoptes_dd4hep-both.py | tee logR2_i2.txt

#  (i3)
#echo -n 3 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich2_noUT_panoptes_dd4hep-both.py | tee logR2_i3.txt

#  (i4)
#echo -n 4 | ../stack/Panoptes/run gaudirun.py data_255358.py hlt2_reco_rich2_noUT_panoptes_dd4hep-both.py | tee logR2_i4.txt

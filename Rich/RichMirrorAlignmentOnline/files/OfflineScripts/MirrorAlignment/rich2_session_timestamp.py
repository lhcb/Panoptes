###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from datetime import datetime

from ruamel.yaml import YAML

yaml = YAML()
dt = datetime.now().isoformat(timespec="minutes")
dt = dt.replace(":", "-")  # Anatoly's new timestamp

with open("rich2_session_timestamp.yml", "w") as fout:
    yaml.dump(dt, fout)

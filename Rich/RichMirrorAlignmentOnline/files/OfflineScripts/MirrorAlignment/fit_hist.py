###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os
import re
import sys

import numpy
from ROOT import (
    TF1,
    TH1D,
    TH2D,
    TCanvas,
    TExec,
    TFile,
    TGraph,
    TLine,
    TMultiGraph,
    gStyle,
)


class AlignMonitor:
    def __init__(
        self,
        whichRich,
        file_nm,
        #            _alignConf,
        #            _maxIt,
    ):
        """
        self.alignConf = _alignConf
        self.nameStr = self.alignConf.getProp('nameStr')
        self.whichRich = self.alignConf.getProp('Rich')
        self.displayMode = self.alignConf.getProp('displayMode')
        self.warningFactor = self.alignConf.getProp('warningFactor')
        self.maxIt = _maxIt  # the maximum labeled number of the iterations that we are looping through
        """
        self.workdir = "./"
        self.whichRich = whichRich
        self.maxIt = 0
        self.prename = file_nm
        self.cuts = True

        # RICH1 settings
        self.maxpri = 3
        self.binToPriMirr = [[2, 3], [0, 1]]
        self.maxsec = 15
        self.binToSecMirr = [
            [10, 11, 14, 15],
            [8, 9, 12, 13],
            [2, 3, 6, 7],
            [0, 1, 4, 5],
        ]
        self.fitMin = -0.005
        self.fitMax = 0.005
        # self.fitMin = -0.01
        # self.fitMax = 0.01
        # if RICH2, use RICH2 settings
        if self.whichRich == 2:
            self.maxpri = 55
            self.binToPriMirr = [
                [3, 2, 1, 0, 31, 30, 29, 28],
                [7, 6, 5, 4, 35, 34, 33, 32],
                [11, 10, 9, 8, 39, 38, 37, 36],
                [15, 14, 13, 12, 43, 42, 41, 40],
                [19, 18, 17, 16, 47, 46, 45, 44],
                [23, 22, 21, 20, 51, 50, 49, 48],
                [27, 26, 25, 24, 55, 54, 53, 52],
            ]
            self.maxsec = 39
            self.binToSecMirr = [
                [3, 2, 1, 0, 23, 22, 21, 20],
                [7, 6, 5, 4, 27, 26, 25, 24],
                [11, 10, 9, 8, 31, 30, 29, 28],
                [15, 14, 13, 12, 35, 34, 33, 32],
                [19, 18, 17, 16, 39, 38, 37, 36],
            ]
            # Original fit range
            self.fitMin = -0.0039
            self.fitMax = 0.0035
            # self.fitMin = -0.002
            # self.fitMax = 0.002
        self.isInsane = False
        self.finalCKres = None
        self.finalCKresErr = None
        self.bkg_params = None

    def performMonitoring(self):
        import os

        gStyle.SetOptFit(1111)
        gStyle.SetOptStat(000000000)

        # File for the RICH piquet AND for the RICH mirror alignment experts
        expertFile = self.workdir + "Rich" + str(self.whichRich) + "_AlignSummary.pdf"
        # File for the Alignment piquet ("duplicating" what goes to the Presenter for the Data Manager [except CK angle res stuff])
        monitorFile = self.workdir + "Rich" + str(self.whichRich) + "_AlignMonitor.pdf"
        # File with for testing purposes
        testingFile = self.workdir + "Rich" + str(self.whichRich) + "_AlignTesting.pdf"
        # Data Manager can only get histograms sent via self.monSvc

        theCanvas = TCanvas("theCanvas", "MirrAlign")
        theCanvas.Divide(2, 2, 0.001, 0.001)

        theCanvas2 = TCanvas("theCanvas2", "MirrAlignTest")
        theCanvas2.Divide(2, 2, 0.001, 0.001)

        theCanvas2.Clear()
        theCanvas.Clear()

        # generate resHistograms

        if self.maxIt == 0:
            pass
        elif self.maxIt < 2:
            theCanvas.Divide(2, 1, 0.0025, 0.0025)
        elif self.maxIt < 3:
            theCanvas.Divide(3, 1, 0.0025, 0.0025)
        elif self.maxIt < 4:
            theCanvas.Divide(2, 2, 0.0025, 0.0025)
        elif self.maxIt < 6:
            theCanvas.Divide(3, 2, 0.0025, 0.0025)
        elif self.maxIt < 8:
            theCanvas.Divide(4, 2, 0.0025, 0.0025)
        elif self.maxIt < 9:
            theCanvas.Divide(3, 3, 0.0025, 0.0025)
        else:
            theCanvas.Divide(4, 4, 0.0025, 0.0025)

        # reserve spaces for resHistograms
        resHistograms = [None] * (self.maxIt + 1)
        polbkg = [None] * (self.maxIt + 1)
        histFunc = [None] * (self.maxIt + 1)
        hist = [None] * (self.maxIt + 1)
        filename = [None] * (self.maxIt + 1)
        thisFile = [None] * (self.maxIt + 1)
        title = [None] * (self.maxIt + 1)
        fitRes = [None] * (self.maxIt + 1)

        # define resHistoTrend
        resHistoTrend = TH1D(
            "resHistoTrend",
            "RICH"
            + str(self.whichRich)
            + " Cherenkov angle resolution (mrad) per It. ",
            self.maxIt + 1,
            -0.5,
            self.maxIt + 0.5,
        )

        for j in reversed(range(0, self.maxIt + 1)):
            filename[j] = self.prename
            if os.path.exists(filename[j]):
                thisFile[j] = TFile(filename[j], "read")
                if self.cuts:
                    hist[j] = (
                        thisFile[j].Get(
                            f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                        )
                    ).Clone()

                else:
                    hist[j] = (
                        thisFile[j].Get(
                            f"RICH/RiCKResLong/Rich{self.whichRich}Gas/ckResAll"
                        )
                    ).Clone()

                if (j == 0) and (j == self.maxIt):
                    hist[j].SetName("resHisto0")
                    hist[j].SetName("resHistoN")
                elif j == self.maxIt:
                    hist[j].SetName("resHistoN")
                elif j == 0:
                    hist[j].SetName("resHisto0")
                if j != 0:
                    hist[j].SetName("resHisto" + str(j))

                fitRes[j] = self.fitCherenkovAngle(hist[j])
                resHistograms[j] = fitRes[j][0]
                resHistoTrend.SetBinContent(j + 1, fitRes[j][1] * 1000)
                resHistoTrend.SetBinError(j + 1, fitRes[j][2] * 1000)
                print(
                    "_i"
                    + str(j)
                    + " CK angle resolution:     "
                    + str(fitRes[j][1] * 1000)
                    + " mrad."
                )
                print(
                    "_i"
                    + str(j)
                    + " CK angle resolution err: "
                    + str(fitRes[j][2] * 1000)
                    + " mrad."
                )
                if j == self.maxIt:
                    self.finalCKres = fitRes[j][1] * 1000
                    self.finalCKresErr = fitRes[j][2] * 1000

                title[j] = (
                    "RICH"
                    + str(self.whichRich)
                    + " Cherenkov angle resolution It. "
                    + str(j)
                )
                resHistograms[j].SetTitle(title[j])
                resHistograms[j].SetXTitle("#Delta#theta_{Cherenkov} (rad)")
                resHistograms[j].SetYTitle("Entries")

                histFunc[j] = resHistograms[j].GetFunction(
                    "Rich" + str(self.whichRich) + "fFitF3"
                )
                polbkg[j] = TF1("polBKG" + str(j), "pol3(0)", self.fitMin, self.fitMax)
                polbkg[j].SetParameter(0, histFunc[j].GetParameter(3))
                polbkg[j].SetParameter(1, histFunc[j].GetParameter(4))
                polbkg[j].SetParameter(2, histFunc[j].GetParameter(5))
                polbkg[j].SetParameter(3, histFunc[j].GetParameter(6))
                polbkg[j].SetLineColor(4)

                gStyle.SetStatW(0.11)

                theCanvas2.cd()
                resHistograms[j].Draw()
                polbkg[j].Draw("SAME")

                theCanvas2.Print(expertFile)

                theCanvas.cd()
                theCanvas.cd(1 + j)
                resHistograms[j].Draw()
                polbkg[j].Draw("SAME")

        # theCanvas.Print(monitorFile)

        sys.stdout.flush()

        # Make resHistoTrend iteration trend plot
        resHistoTrend.SetMarkerStyle(8)
        resHistoTrend.SetXTitle("iteration number")
        resHistoTrend.SetYTitle("#splitline{Cherenkov angle resolution (mrad)}{}")

        theCanvas2.Clear()
        theCanvas.Clear()
        resHistoTrend.DrawCopy()
        sys.stdout.flush()

    def fitCherenkovAngle(self, hist):
        richStr = "Rich" + str(self.whichRich)
        m_maxErrorForOK = 1e-3

        xPeak = hist.GetBinCenter(hist.GetMaximumBin())
        delta = 0
        if richStr == "Rich1":
            delta = 0.0025
        else:
            delta = 0.00105

        fitMin = xPeak - delta
        fitMax = xPeak + delta

        preFitFName = richStr + "PreFitF"
        preFitF = TF1(preFitFName, "gaus", fitMin, fitMax)
        preFitF.SetParameter(1, 0)

        if richStr == "Rich1":
            preFitF.SetParameter(2, 0.0015)
        else:
            preFitF.SetParameter(2, 0.0007)

        hist.Fit(preFitF, "RQ")
        fitOK = False

        lastFitF = preFitF
        bestFitF = None
        nPolFull = 3
        bestNPol = 0

        fFitF = None

        for nPol in range(1, nPolFull + 1):
            fFitFName = richStr + "fFitF" + str(nPol)
            fFitFType = "gaus(0)+pol" + str(nPol) + "(3)"
            fFitF = TF1(fFitFName, fFitFType, self.fitMin, self.fitMax)

            fFitF.SetParName(0, "Gaus Constant")
            fFitF.SetParName(1, "Gaus Mean")
            fFitF.SetParName(2, "Gaus Sigma")
            nParamsToSet = 3
            if nPol > 1:
                nParamsToSet = 3 + nPol

            for p in range(0, nParamsToSet):
                fFitF.SetParameter(p, lastFitF.GetParameter(p))

            hist.Fit(fFitF, "RFQM")
            lastFitF = fFitF
            fitOK = fFitF.GetParError(1) < m_maxErrorForOK
            if fitOK:
                bestFitF = fFitF
                bestNPol = nPol
            else:
                if nPol == nPolFull:
                    hist.Fit(fFitF, "RFSE0QM")
                    fitOK = True
                if nPol > 1:
                    break
        print(
            [
                fFitF.GetParameter(3),
                fFitF.GetParameter(4),
                fFitF.GetParameter(5),
                fFitF.GetParameter(6),
            ]
        )
        fitRes = [hist, fFitF.GetParameter(2), fFitF.GetParError(2)]
        return fitRes


def monitor(whichRich, file_nm):
    alignMonitor = AlignMonitor(whichRich, file_nm)
    alignMonitor.performMonitoring()
    finalCKres = alignMonitor.finalCKres
    finalCKresErr = alignMonitor.finalCKresErr
    return finalCKres


# can minimise using monitor() as function to minimise if returns finalCKres and has arg inputs

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Fits histogram to obtain Rich CK angle resolution"
    )
    parser.add_argument(
        "whichRich", choices=[1, 2], type=int, help="Which Rich detector to use"
    )
    parser.add_argument("file_nm", help="Name of root file to use")
    args = parser.parse_args()
    whichRich = args.whichRich
    file_nm = args.file_nm

    ckres = monitor(whichRich, file_nm)
    print("CKres:" + str(ckres))
    sys.stdout.flush()

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import hlt2_reco_rich_noUT_panoptes_dd4hep_both

print(__name__)
"""
if __name__ == 'builtins':
    my_class_instance = hlt2_reco_rich_noUT_panoptes_dd4hep_both(whichRich=1)
    print(my_class_instance)
    my_class_instance.run()
"""

my_class_instance = hlt2_reco_rich_noUT_panoptes_dd4hep_both(whichRich=1)
print(my_class_instance)
my_class_instance.run()

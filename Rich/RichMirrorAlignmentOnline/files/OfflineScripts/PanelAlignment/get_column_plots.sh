###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


#!/bin/bash
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

read -p 'whichRich: ' whichRich


if [ "$whichRich" -eq 1 ]; then
    n_cols=10
elif [ "$whichRich" -eq 2 ]; then
    n_cols=13
fi

read -p 'Do column fits? (1=yes, 0=no): ' do_column_fits
read -p 'whichPanel: ' whichPanel
read -p 'Run number: ' runNum


if [ "$do_column_fits" -eq 1 ]; then
    for i in $(seq 0 "$n_cols"); do
        echo "fitting column $i for RICH $whichRich Panel $whichPanel"

        if [ "$whichRich" -eq 1 ] && [ "$whichPanel" -eq 0 ]; then
            histo_path="RICH/RiCKResLongTight/Rich1Gas/top/cols/ckResAllCol${i}"
        elif [ "$whichRich" -eq 1 ] && [ "$whichPanel" -eq 1 ]; then
            histo_path="RICH/RiCKResLongTight/Rich1Gas/bottom/cols/ckResAllCol${i}"
        elif [ "$whichRich" -eq 2 ] && [ "$whichPanel" -eq 0 ]; then
            histo_path="RICH/RiCKResLongTight/Rich2Gas/ASide-left/cols/ckResAllCol${i}"
        elif [ "$whichRich" -eq 2 ] && [ "$whichPanel" -eq 1 ]; then
            histo_path="RICH/RiCKResLongTight/Rich2Gas/CSide-right/cols/ckResAllCol${i}"
        fi

        python fit_hist.py "$whichRich" "$whichPanel" rich"${whichRich}"_opt_dd4hep_p"$whichPanel".root --hist_path "${histo_path} --write_results --extra_info $runNum,$i"

    done
fi

if [ "$whichRich" -eq 2 ]; then
    for pmt in R H; do

        if [ "$whichRich" -eq 2 ] && [ "$whichPanel" -eq 0 ]; then
            histo_path="RICH/RiCKResLongTight/Rich2Gas/ASide-left/ckResAllPerPanel${pmt}TypePD"
        elif [ "$whichRich" -eq 2 ] && [ "$whichPanel" -eq 1 ]; then
            histo_path="RICH/RiCKResLongTight/Rich2Gas/CSide-right/ckResAllPerPanel${pmt}TypePD"
        fi

        echo "fitting PMT type $pmt for RICH $whichRich Panel $whichPanel"

        python fit_hist.py "$whichRich" "$whichPanel" rich"${whichRich}"_opt_dd4hep_p"$whichPanel".root --hist_path "$histo_path"

    done
fi

# create folder structure and move pdf files over


current_dir=$(pwd)
runNum_dir="${current_dir}/column_plots/${runNum}"
if [ ! -d "${runNum_dir}" ]; then
    mkdir -p "${runNum_dir}"
fi

# Define the subdirectory names in an array
subdirs=("r1_p0_top" "r1_p1_bottom" "r2_p0_aside" "r2_p1_cside")

# Create the directory structure using a loop
for subdir in "${subdirs[@]}"; do
  output_dir="${runNum_dir}/${subdir}"
  if [ ! -d "${output_dir}" ]; then
    mkdir -p "${output_dir}"
  fi
done

if [ "${whichRich}" = 1 ] && [ "${whichPanel}" = 0 ]; then
    mv Rich${whichRich}*.pdf "${runNum_dir}/${subdirs[0]}"
fi
if [ "${whichRich}" = 1 ] && [ "${whichPanel}" = 1 ]; then
    mv Rich${whichRich}*.pdf "${runNum_dir}/${subdirs[1]}"
fi
if [ "${whichRich}" = 2 ] && [ "${whichPanel}" = 0 ]; then
    mv Rich${whichRich}*.pdf "${runNum_dir}/${subdirs[2]}"
fi
if [ "${whichRich}" = 2 ] && [ "${whichPanel}" = 1 ]; then
    mv Rich${whichRich}*.pdf "${runNum_dir}/${subdirs[3]}"
fi

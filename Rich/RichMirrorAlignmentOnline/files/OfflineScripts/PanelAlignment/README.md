# Offline Panel Alignment Steering Scripts

This folder contains various scripts to investigate the RICH panel alignment. Steering scripts called `hlt2_reco_rich...` perform the histogram generation. `fit_hist.py` fits these histograms to obtain the CKthetaRes. Scripts in the folder 'surveys' perform rich surveys, but need to be edited with the correct paths to work properly.

## Running Reconstruction

To perform a standalone Rich reconstruction run, for example:

```
./stack_moore/Moore/run gaudirun.py hlt2_reco_rich1.py
```

To fit the output, run `fit_hist.py` in the same folder as the histograms, like:

```
python fit_hist.py whichRich file_nm

args:
whichRich - which Rich detector to use, choice of [1,2]
file_nm   - name of histogram (root) file

```

## Running Surveys

To run a survey, edit `optimise_RICH.py` to select starting points and step size. Also be sure to change the paths in `hlt2_reco_rich?_opt.py` to the same directory as `optimise_RICH.py`. Run `optimise_both.sh` to perform the survey for both rich detectors. Run `plot_cktheta.py` over the resulting csv files to plot the final result.

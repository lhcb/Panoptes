###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os
import re
import sys

import numpy
import ROOT
from GaudiPython import gbl
from ROOT import (
    TF1,
    TH1D,
    TH2D,
    TCanvas,
    TExec,
    TFile,
    TGraph,
    TLine,
    TMultiGraph,
    gStyle,
)


class AlignMonitor:
    def __init__(
        self,
        whichRich,
        file_nm,
        #            _alignConf,
        #            _maxIt,
    ):
        """
        self.alignConf = _alignConf
        self.nameStr = self.alignConf.getProp('nameStr')
        self.whichRich = self.alignConf.getProp('Rich')
        self.displayMode = self.alignConf.getProp('displayMode')
        self.warningFactor = self.alignConf.getProp('warningFactor')
        self.maxIt = _maxIt  # the maximum labeled number of the iterations that we are looping through
        """
        self.workdir = "./"
        self.whichRich = whichRich
        self.maxIt = 0
        self.prename = file_nm
        self.cuts = True

        # RICH1 settings
        self.maxpri = 3
        self.binToPriMirr = [[2, 3], [0, 1]]
        self.maxsec = 15
        self.binToSecMirr = [
            [10, 11, 14, 15],
            [8, 9, 12, 13],
            [2, 3, 6, 7],
            [0, 1, 4, 5],
        ]
        self.fitMin = -0.0055
        self.fitMax = 0.0055
        # self.fitMin = -0.01
        # self.fitMax = 0.01
        # if RICH2, use RICH2 settings
        if self.whichRich == 2:
            self.maxpri = 55
            self.binToPriMirr = [
                [3, 2, 1, 0, 31, 30, 29, 28],
                [7, 6, 5, 4, 35, 34, 33, 32],
                [11, 10, 9, 8, 39, 38, 37, 36],
                [15, 14, 13, 12, 43, 42, 41, 40],
                [19, 18, 17, 16, 47, 46, 45, 44],
                [23, 22, 21, 20, 51, 50, 49, 48],
                [27, 26, 25, 24, 55, 54, 53, 52],
            ]
            self.maxsec = 39
            self.binToSecMirr = [
                [3, 2, 1, 0, 23, 22, 21, 20],
                [7, 6, 5, 4, 27, 26, 25, 24],
                [11, 10, 9, 8, 31, 30, 29, 28],
                [15, 14, 13, 12, 35, 34, 33, 32],
                [19, 18, 17, 16, 39, 38, 37, 36],
            ]
            # Original fit range
            self.fitMin = -0.0055
            self.fitMax = 0.0055
            # self.fitMin = -0.002
            # self.fitMax = 0.002
        self.isInsane = False
        self.finalCKres = None
        self.finalCKresErr = None
        self.bkg_params = None

    def performMonitoring(self):
        import os

        if not plot_nice:
            gStyle.SetOptFit(1111)
            gStyle.SetOptStat(000000000)

        if hist_path == "":
            pdf_suff = ""
        else:
            pdf_suff = (
                "_"
                + hist_path.split("/")[-4]
                + "_"
                + hist_path.split("/")[-3]
                + "_"
                + hist_path.split("/")[-2]
                + "_"
                + hist_path.split("/")[-1]
            )

        # including figures doesn't work for .pdfs
        file_ext = "png" if plot_nice else "pdf"

        # File for the RICH piquet AND for the RICH mirror alignment experts
        expertFile = (
            f"{self.workdir}Rich{self.whichRich}_AlignSummary{pdf_suff}.{file_ext}"
        )
        # File for the Alignment piquet ("duplicating" what goes to the Presenter for the Data Manager [except CK angle res stuff])
        monitorFile = f"{self.workdir}Rich{self.whichRich}_AlignMonitor{pdf_suff}.pdf"
        # File with for testing purposes
        testingFile = f"{self.workdir}Rich{self.whichRich}_AlignTesting{pdf_suff}.pdf"
        # Data Manager can only get histograms sent via self.monSvc

        theCanvas = TCanvas("theCanvas", "MirrAlign")
        theCanvas.Divide(2, 2, 0.001, 0.001)

        theCanvas2 = TCanvas("theCanvas2", "MirrAlignTest")
        theCanvas2.Divide(2, 2, 0.001, 0.001)

        theCanvas2.Clear()
        theCanvas.Clear()

        # generate resHistograms

        if self.maxIt == 0:
            pass
        elif self.maxIt < 2:
            theCanvas.Divide(2, 1, 0.0025, 0.0025)
        elif self.maxIt < 3:
            theCanvas.Divide(3, 1, 0.0025, 0.0025)
        elif self.maxIt < 4:
            theCanvas.Divide(2, 2, 0.0025, 0.0025)
        elif self.maxIt < 6:
            theCanvas.Divide(3, 2, 0.0025, 0.0025)
        elif self.maxIt < 8:
            theCanvas.Divide(4, 2, 0.0025, 0.0025)
        elif self.maxIt < 9:
            theCanvas.Divide(3, 3, 0.0025, 0.0025)
        else:
            theCanvas.Divide(4, 4, 0.0025, 0.0025)

        # reserve spaces for resHistograms
        resHistograms = [None] * (self.maxIt + 1)
        polbkg = [None] * (self.maxIt + 1)
        histFunc = [None] * (self.maxIt + 1)
        hist = [None] * (self.maxIt + 1)
        filename = [None] * (self.maxIt + 1)
        thisFile = [None] * (self.maxIt + 1)
        title = [None] * (self.maxIt + 1)
        fitRes = [None] * (self.maxIt + 1)
        fitHist = [None] * (self.maxIt + 1)

        # define resHistoTrend
        resHistoTrend = TH1D(
            "resHistoTrend",
            "RICH"
            + str(self.whichRich)
            + " Cherenkov angle resolution (mrad) per It. ",
            self.maxIt + 1,
            -0.5,
            self.maxIt + 0.5,
        )

        for j in reversed(range(0, self.maxIt + 1)):
            filename[j] = self.prename
            if os.path.exists(filename[j]):
                thisFile[j] = TFile(filename[j], "read")
                if hist_path == "":
                    if self.cuts:
                        hist[j] = (
                            thisFile[j].Get(
                                f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                            )
                        ).Clone()
                    else:
                        hist[j] = (
                            thisFile[j].Get(
                                f"RICH/RiCKResLong/Rich{self.whichRich}Gas/ckResAll"
                            )
                        ).Clone()
                else:
                    hist[j] = (thisFile[j].Get(f"{hist_path}")).Clone()

                if (j == 0) and (j == self.maxIt):
                    hist[j].SetName("resHisto0")
                    hist[j].SetName("resHistoN")
                elif j == self.maxIt:
                    hist[j].SetName("resHistoN")
                elif j == 0:
                    hist[j].SetName("resHisto0")
                if j != 0:
                    hist[j].SetName("resHisto" + str(j))

                # perform the fit and extract fit models
                fitRes[j], resHistograms[j] = self.fitCherenkovAngle(hist[j])
                histFunc[j] = fitRes[j].overallFitFunc
                polbkg[j] = fitRes[j].bkgFitFunc

                ckres = histFunc[j].GetParameter(2) * 1000
                ckres_error = histFunc[j].GetParError(2) * 1000

                resHistoTrend.SetBinContent(j + 1, ckres)
                resHistoTrend.SetBinError(j + 1, ckres_error)

                print(
                    "_i" + str(j) + " CK angle resolution:     " + str(ckres) + " mrad."
                )
                print(
                    "_i"
                    + str(j)
                    + " CK angle resolution err: "
                    + str(ckres_error)
                    + " mrad."
                )

                if j == self.maxIt:
                    self.finalCKres = ckres
                    self.finalCKresErr = ckres_error

                title[j] = (
                    "RICH"
                    + str(self.whichRich)
                    + " Cherenkov angle resolution It. "
                    + str(j)
                )
                resHistograms[j].SetTitle(title[j])
                resHistograms[j].SetXTitle(r"#Delta#theta_{Cherenkov} (rad)")
                resHistograms[j].SetYTitle("Entries")

                histFunc[j].SetLineColor(2)
                polbkg[j].SetLineColor(4)

                gStyle.SetStatW(0.11)

                if plot_nice:
                    polbkg[j].SetLineWidth(5)
                    histFunc[j].SetLineWidth(5)

                    resHistograms[j].SetMarkerSize(0.006)

                    # rad -> mrad on xaxis
                    resHistograms[j].GetXaxis().SetMaxDigits(1)
                    # Get the number of bins
                    nbins = resHistograms[j].GetNbinsX()

                    # set y axis label to refer to the bin width
                    resHistograms[j].GetYaxis().SetTitle(
                        "Entries/{:.4f} rad".format(resHistograms[j].GetBinWidth(1))
                    )

                theCanvas2.cd()
                if plot_nice:
                    resHistograms[j].Draw("E1")
                else:
                    resHistograms[j].Draw()
                histFunc[j].Draw("SAME")
                polbkg[j].Draw("SAME")

                if plot_nice:
                    # this increases dpi of pngs
                    theCanvas2.SetCanvasSize(
                        theCanvas2.GetWw() * 6, theCanvas2.GetWh() * 6
                    )

                    # for whatever reason things get cut off on the right
                    theCanvas2.SetRightMargin(0.1)

                    # RICH text
                    text_rich = ROOT.TLatex()
                    text_rich.SetNDC(True)
                    text_rich.SetTextSize(0.09)
                    text_rich.DrawLatex(0.17, 0.7, f"RICH {whichRich}")

                    # resolution text
                    text_res1 = ROOT.TLatex()
                    text_res1.SetNDC(True)
                    text_res1.SetTextSize(0.023)
                    text_res1.DrawLatex(
                        0.16,
                        0.88,
                        r"#sigma[#Delta#theta_{Cherenkov}] = "
                        + f"({self.finalCKres:.3f}"
                        + r" #pm "
                        + f"{self.finalCKresErr:.3f})"
                        + r" #times 10^{-3}"
                        + r" rad",
                    )
                    text_res2 = ROOT.TLatex()
                    text_res2.SetNDC(True)
                    text_res2.SetTextSize(0.023)
                    if whichRich == 1:
                        text_res2.DrawLatex(
                            0.16,
                            0.85,
                            r"(#sigma[#Delta#theta_{Cherenkov}](Run 2) = "
                            + r"(1.640 #pm 0.003) #times 10^{-3} "
                            + r" rad)",
                        )
                    else:
                        text_res2.DrawLatex(
                            0.16,
                            0.85,
                            r"(#sigma[#Delta#theta_{Cherenkov}](Run 2) = "
                            + r"(0.650 #pm 0.001) #times 10^{-3} "
                            + r" rad)",
                        )

                    # Create the TPad with the calculated position and size
                    pad = ROOT.TPad("pad", "pad", 0.63, 0.6, 0.85, 0.85)

                    # Draw the TPad
                    pad.Draw("SAME")
                    pad.cd()

                    # Load the figure and draw it on the new pad
                    figure = ROOT.TImage.Open("lhcb_prelim.png")
                    figure.SetConstRatio(True)
                    figure.Draw()

                theCanvas2.Print(expertFile)

                theCanvas.cd()
                theCanvas.cd(1 + j)
                resHistograms[j].Draw("E1")
                polbkg[j].Draw("SAME")

        # theCanvas.Print(monitorFile)

        sys.stdout.flush()

        # Make resHistoTrend iteration trend plot
        resHistoTrend.SetMarkerStyle(8)
        resHistoTrend.SetXTitle("iteration number")
        resHistoTrend.SetYTitle("#splitline{Cherenkov angle resolution (mrad)}{}")

        theCanvas2.Clear()
        theCanvas.Clear()
        resHistoTrend.DrawCopy()
        sys.stdout.flush()

    def fitCherenkovAngle(self, hist):
        # get Chris Jones's C++ fitter
        fitter = gbl.Rich.Rec.CKResolutionFitter()

        # set signal (AsymNormal) and background (FreeNPol) models
        fitter.params().RichFitTypes[whichRich - 1].clear()
        fitter.params().RichFitTypes[whichRich - 1].push_back("AsymNormal:FreeNPol")

        # change fit ranges
        fitter.params().RichFitMin[0] = self.fitMin
        fitter.params().RichFitMin[1] = self.fitMin
        fitter.params().RichFitMax[0] = self.fitMax
        fitter.params().RichFitMax[1] = self.fitMax

        res = fitter.fit(hist, whichRich)

        return res, hist


def monitor(whichRich, file_nm):
    alignMonitor = AlignMonitor(whichRich, file_nm)
    alignMonitor.performMonitoring()
    finalCKres = alignMonitor.finalCKres
    finalCKresErr = alignMonitor.finalCKresErr

    if write_results:
        import csv

        f = open(f"./ckres_results_R{whichRich}.csv", "a+")
        writer = csv.writer(f)
        writer.writerow([finalCKres, finalCKresErr, extra_info])
        f.close()

    return finalCKres


# can minimise using monitor() as function to minimise if returns finalCKres and has arg inputs

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Fits histogram to obtain Rich CK angle resolution"
    )
    parser.add_argument(
        "whichRich", choices=[1, 2], type=int, help="Which Rich detector to use"
    )
    parser.add_argument("file_nm", help="Name of root file to use")
    parser.add_argument("--hist_path", default="", help="Histogram path in root file")
    parser.add_argument(
        "--write_results", action="store_true", help="Write results to textfile"
    )
    parser.add_argument(
        "--extra_info",
        help="Any extra text to be written to the text file (e.g. run number)",
    )
    parser.add_argument(
        "--plot_nice",
        default=False,
        action="store_true",
        help="Plot the histogram nicely, using lhcbstyle",
    )
    args = parser.parse_args()
    whichRich = args.whichRich
    file_nm = args.file_nm
    write_results = args.write_results
    extra_info = args.extra_info
    plot_nice = args.plot_nice
    global hist_path
    hist_path = args.hist_path

    if plot_nice:
        ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
        ROOT.lhcbStyle()
        ROOT.gStyle.SetOptStat(0)

    ckres = monitor(whichRich, file_nm)
    print("CKres:" + str(ckres))
    sys.stdout.flush()

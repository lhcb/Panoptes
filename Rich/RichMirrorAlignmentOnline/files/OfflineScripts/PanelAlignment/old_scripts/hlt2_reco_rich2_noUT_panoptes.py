###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import csv
import glob
import math

from Configurables import UpdateManagerSvc
from GaudiKernel.SystemOfUnits import (
    GeV,
)
from Moore import (
    options,
    run_reconstruction,
)
from Panoptes.alignment import standalone_rich_panel_align_reco
from RecoConf.rich_data_monitoring import (
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import default_rich_reco_options

"""Options for running over data with FT raw bank version 6."""
from RecoConf.decoders import (
    default_ft_decoding_version,
)

options.n_threads = 10
options.evt_max = 10000
options.input_type = "MDF"
options.dddb_tag = "upgrade/dddb-20220612"
options.conddb_tag = "upgrade/sim-20220612-vc-md100-RICHcustomFTv6"
options.input_files = sorted(
    [
        "mdf:root://eoslhcb.cern.ch/%s" % f
        for f in glob.glob(
            r"/eos/lhcb/grid/prod/lhcb/MC/Upgrade/MDF/00146082/000*/00146082_*"
        )
    ]
)

whichPanel = 0  # options: 0, 1, "both"

rich = "rich2"
radiator = "Rich2Gas"

param_list = ["p0_x", "p1_x", "p0_y", "p1_y", "p0_z", "p1_z"]
rot_perf_param_dict = dict.fromkeys(param_list)
trans_perf_param_dict = dict.fromkeys(param_list)

# optimum translation alignment
trans_perf_align = [-1.32, 1.41, -4.74, -3.92, 4.79, 2.57]
for val, param in zip(trans_perf_align, param_list):
    trans_perf_param_dict[param] = float(val)

# optimum rotation alignment
rot_perf_align = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
for val, param in zip(rot_perf_align, param_list):
    rot_perf_param_dict[param] = float(val)

panel_nums = []
if whichPanel == "both":
    panel_nums.append(0)
    panel_nums.append(1)
else:
    panel_nums.append(whichPanel)

cond_update_list = []
for panel_num in panel_nums:
    pre_cond_str = f"Conditions/Alignment/Rich2/PDPanel{panel_num}_Align :=  "
    optimum_trans_cond_str = f"double_v dPosXYZ = {trans_perf_param_dict['p' + str(panel_num) + '_x']} {trans_perf_param_dict['p' + str(panel_num) + '_y']} {trans_perf_param_dict['p' + str(panel_num) + '_z']}; "
    optimum_rot_cond_str = f"double_v dRotXYZ = {rot_perf_param_dict['p' + str(panel_num) + '_x']} {rot_perf_param_dict['p' + str(panel_num) + '_y']} {rot_perf_param_dict['p' + str(panel_num) + '_z']};"
    cond_update_list.append(
        pre_cond_str + optimum_trans_cond_str + optimum_rot_cond_str
    )

UpdateManagerSvc().ConditionsOverride = cond_update_list

default_opts = {}
default_level2_opts = {}
default_reco_opts = {}

# Reco opts
wider_bkg = {"PhotonSelection": "None"}
default_reco_opts.update(wider_bkg)

if whichPanel == 0:
    panel_select = {"ActivatePanel": (True, False)}
elif whichPanel == 1:
    panel_select = {"ActivatePanel": (False, True)}
else:
    panel_select = {"ActivatePanel": (True, True)}

default_reco_opts.update(panel_select)

# Monitoring opts
tighter_minp = {
    "TightTrackSelection": {
        "MinP": 60.0 * GeV,
        "MinPt": 0.5 * GeV,
        "MaxChi2": 2.0,
        "MaxGhostProb": 0.1,
    }
}

default_opts.update(tighter_minp)
wider_histo = {"CKResHistoRange": (0.025, 0.008, 0.012)}
default_level2_opts.update(wider_histo)

align_tasks = [
    "Produce",  # fill the production set of histograms
    "Monitor",  # add various checking histograms
    #'Map',       # add counters for creation of the HLT1 pre-selection line "map"
    #'Optimize',  # add counters for optimization of the RICH2 mirror combinations subset
    #'Select',    # check filling the rest of RICH2 mirror combinations along with 8 poorest
    #'Calibrate', # check filling all RICH2 mirror combinations with elimination when filled
    #'Explore',   # explore influence of RICH1 coordinate systems on distribution shapes
]

nkevts = str(math.trunc(options.evt_max / 1000))

# save output file with histograms
options.histo_file = "rich2_opt.root"


with (
    standalone_rich_panel_align_reco.bind(RichGas=radiator, noUT=True),
    default_ft_decoding_version.bind(value=6),
    default_rich_reco_options.bind(default_rich_reco_opts_modify=default_reco_opts),
    default_rich_monitoring_options.bind(
        default_rich_moni_opts_modify=default_opts,
        default_rich_moni_opts_modify_level2=default_level2_opts,
    ),
):
    run_reconstruction(options, standalone_rich_panel_align_reco)

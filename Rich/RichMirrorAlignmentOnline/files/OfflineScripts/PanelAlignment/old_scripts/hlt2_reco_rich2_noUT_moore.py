###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import csv
import glob
import math

from Configurables import UpdateManagerSvc
from GaudiKernel.SystemOfUnits import (
    GeV,
)
from Moore import (
    options,
    run_reconstruction,
)
from RecoConf.rich_add_reconstruction_monitoring_checking import (
    add_hlt2_rich,
)
from RecoConf.rich_data_monitoring import (
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import default_rich_reco_options
from RecoConf.standalone import (
    standalone_hlt2_light_reco_without_UT,
)

"""Options for running over data with FT raw bank version 6."""
from RecoConf.decoders import (
    default_ft_decoding_version,
)

options.n_threads = 6
options.evt_max = 10000

options.input_files = sorted(
    [
        "mdf:root://eoslhcb.cern.ch/%s" % f
        for f in glob.glob(
            r"/eos/lhcb/grid/prod/lhcb/MC/Upgrade/MDF/00146082/000*/00146082_*"
        )
    ]
)
options.input_type = "MDF"
# options.dddb_tag = 'dddb-20220612'
# options.conddb_tag = 'sim-20220612-vc-md100'

options.dddb_tag = "upgrade/dddb-20220612"
options.conddb_tag = "upgrade/sim-20220612-vc-md100-RICHcustomFTv6"

rich = "rich2"
radiator = "Rich2Gas"

track_types = [
    "Long",
    #'Upstream',
    #'Downstream',
]

# Only edit bad_align to play with alignment parameters
param_list = ["p0_x", "p1_x", "p0_y", "p1_y", "p0_z", "p1_z"]
param_dict = dict.fromkeys(param_list)
perf_align = [-1.32, 1.41, -4.74, -3.92, 4.79, 2.57]
bad_align = [-0.32, 2.41, -2.74, -5.92, 4.79, 2.57]
for row, param in zip(perf_align, param_dict.keys()):
    param_dict[param] = row

UpdateManagerSvc().ConditionsOverride += [
    f"Conditions/Alignment/Rich2/PDPanel0_Align :=  double_v dPosXYZ = {param_dict['p0_x']} {param_dict['p0_y']} {param_dict['p0_z']};",
    f"Conditions/Alignment/Rich2/PDPanel1_Align :=  double_v dPosXYZ = {param_dict['p1_x']} {param_dict['p1_y']} {param_dict['p1_z']};",
]

default_opts = {}
default_level2_opts = {}
default_reco_opts = {}

# Reco opts
wider_bkg = {"PhotonSelection": "None"}
default_reco_opts.update(wider_bkg)
panel_select = {"ActivatePanel": (True, False)}
default_reco_opts.update(panel_select)

# Monitoring opts
tighter_minp = {
    "TightTrackSelection": {
        "MinP": 60.0 * GeV,
        "MinPt": 0.5 * GeV,
        "MaxChi2": 2.0,
        "MaxGhostProb": 0.1,
    }
}

default_opts.update(tighter_minp)
wider_histo = {"CKResHistoRange": (0.025, 0.008, 0.012)}
default_level2_opts.update(wider_histo)

align_tasks = [
    "Produce",  # fill the production set of histograms
    "Monitor",  # add various checking histograms
    #'Map',       # add counters for creation of the HLT1 pre-selection line "map"
    #'Optimize',  # add counters for optimization of the RICH2 mirror combinations subset
    #'Select',    # check filling the rest of RICH2 mirror combinations along with 8 poorest
    #'Calibrate', # check filling all RICH2 mirror combinations with elimination when filled
    #'Explore',   # explore influence of RICH1 coordinate systems on distribution shapes
]

task = align_tasks[0]

nkevts = str(math.trunc(options.evt_max / 1000))

# save output file with histograms
options.histo_file = "rich2_opt.root"

moni_set = "PhotonCherenkovAngles"

with (
    add_hlt2_rich.bind(
        do_data_monitoring_rich=True,
        moni_set=moni_set,
        radiator=radiator,
        mirror_align_tasks=align_tasks,
        track_types=track_types,
    ),
    default_ft_decoding_version.bind(value=6),
    default_rich_reco_options.bind(default_rich_reco_opts_modify=default_reco_opts),
    default_rich_monitoring_options.bind(
        default_rich_moni_opts_modify=default_opts,
        default_rich_moni_opts_modify_level2=default_level2_opts,
    ),
    standalone_hlt2_light_reco_without_UT.bind(skip_Muon=True),
):
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)

# old -> make_hlt2_tracks_without_UT.bind(use_pr_kf=True, light_reco=True, fast_reco=True), \

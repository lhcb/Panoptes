###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import csv
import glob
import math
import random
import shutil
import sys

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad
from Moore import (
    options,
    run_reconstruction,
)
from Panoptes.alignment import (
    standalone_rich_online_align_reco,
    standalone_rich_panel_align_reco,
)
from PyConf.Algorithms import (
    PrForwardTrackingVelo,
    PrHybridSeeding,
    PrMatchNN,
    VeloRetinaClusterTrackingSIMD,
    VPRetinaFullClusterDecoder,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_PatPV3DFuture_pvs,
    make_reco_pvs,
    make_velo_full_clusters,
    make_VeloClusterTrackingSIMD,
)
from RecoConf.rich_data_monitoring import (
    alignment_rich_monitoring_options,
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import default_rich_reco_options

# from Configurables import (
#      UpdateManagerSvc
# )
"""Options for running over data with FT raw bank version 6."""
from RecoConf.decoders import (
    default_ft_decoding_version,
)

# load options from YAML file
from ruamel.yaml import YAML

yaml = YAML()

with open("survey_opts.yml") as opts_yml_file:
    yml_opts = yaml.load(opts_yml_file)

whichRich = yml_opts["whichRich"]
whichPanel = yml_opts["whichPanel"]
survey_type = yml_opts["survey_type"]

manual_update = True

useDD4Hep = True
useRealData = True
# useHltDecisions = True if whichRich == 1 else False
useHltDecisions = False

options.evt_max = yml_opts["evt_max"]
options.n_threads = yml_opts["n_threads"]
# options.n_event_slots = options.n_threads * 3
options.scheduler_legacy_mode = False
options.use_iosvc = True

if useRealData is not True:
    options.set_input_and_conds_from_testfiledb("upgrade_Sept2022_minbias_0fb_md_xdigi")
    options.input_type = "ROOT"
else:
    if useHltDecisions:
        exec(
            open(
                "/home/jreich/stack_dd4hep_v3/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/data_online.py"
            ).read()
        )
    else:
        # exec(open("/home/jreich/stack_dd4hep_v3/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/MirrorAlignment/data_online.py").read())

        import os

        def get_directories_with_prefix(directory, prefix):
            file_directories = []
            for root, dirs, files in os.walk(directory):
                for file in files:
                    if os.path.basename(root).startswith(prefix):
                        file_path = os.path.join(root, file)
                        file_directories.append(file_path)
            return file_directories

        # options.input_files = get_directories_with_prefix("/calib/align/LHCb/Rich", "0000256292")

        base_dir_files = f"/calib/align/LHCb/Rich{whichRich}"

        def obtain_files_from_many_runs(
            base_dir_files,
            start_run,
            end_run,
            num_files_to_select=2,  # this is per run number
            seed_value=100,
        ):
            files = []

            for folder_number in range(start_run, end_run + 1):
                folder_name = "0000" + str(folder_number)
                folder_path = os.path.join(base_dir_files, folder_name)

                # Check if the folder exists
                if os.path.exists(folder_path) and os.path.isdir(folder_path):
                    # List all files in the folder
                    files_in_folder = os.listdir(folder_path)

                    # Select random files from the folder
                    random.seed(seed_value)
                    random_files = random.sample(
                        files_in_folder, min(num_files_to_select, len(files_in_folder))
                    )

                    # Print the selected files
                    for file_name in random_files:
                        files.append(os.path.join(folder_path, file_name))

            return files

        options.input_files = obtain_files_from_many_runs(
            base_dir_files, 299618, 299683
        )
        # base_dir_files, 298656, 300124)

        # base_dir = "/calib/align/LHCb/Rich/0000255353/"
        # import os
        # files = [os.path.join(base_dir, f) for f in os.listdir(base_dir)]
        # options.input_files = files

    options.simulation = False
    options.input_type = "MDF"

if useDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    from DDDB.CheckDD4Hep import UseDD4Hep
else:
    UseDD4Hep = False

print("Using DD4Hep: ", UseDD4Hep)

if UseDD4Hep:
    from Configurables import DDDBConf

    useUT_bool = True
    if useUT_bool:
        dd4hep = DD4hepSvc(
            DetectorList=["/world", "Magnet", "UT", "VP", "FT", "Rich1", "Rich2"]
        )
        noUT_bool = False
    else:
        dd4hep = DD4hepSvc(
            DetectorList=["/world", "Magnet", "VP", "FT", "Rich1", "Rich2"]
        )
        noUT_bool = True
    dd4hep.OutputLevel = 1

    if useRealData:
        options.conddb_tag = "master"  # used for real data
        options.dddb_tag = "run3/trunk"
        # DDDBConf().GeometryVersion = 'run3/trunk'  # used for real data

        # latest scifi and velo alignments
        dd4hep.UseConditionsOverlay = True
        dd4hep.ConditionsVersion = "master"

        # needed for when reconstruction changed to SuperPixels (change was at run 289434)
        DDDBConf().GeometryVersion = "run3/2024.Q1.2-v00.00"
        options.geometry_version = "run3/2024.Q1.2-v00.00"
    else:
        options.conddb_tag = (
            "jonrob/all-pmts-active"  # used for DetDesc MC samples with DD4Hep
        )
        DDDBConf().GeometryVersion = "before-rich1-geom-update-26052022"  # used for DetDesc MC samples with DD4Hep

    if manual_update:
        dd4hep.ConditionsVersion = "master"
        dd4hep.ConditionsLocation = (
            "git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"
        )

    # if manual_update:
    #     conddb_path = yml_opts["conddb_path"]
    #     if os.path.isdir(conddb_path):
    #         dd4hep.ConditionsLocation = "file://" + conddb_path
    #     else:
    #         raise Exception(
    #             f"Error: Trying to manually update conditions. No local clone of condDB found in {conddb_path}."
    #         )
    # else:
    #     dd4hep.ConditionsLocation = 'git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git'

else:
    options.conddb_tag = "upgrade/sim-20220612-vc-md100-RICHcustomFTv6"
    options.simulation = True

rich = f"rich{whichRich}"
radiator = f"Rich{whichRich}Gas"

param_list = ["p0_x", "p1_x", "p0_y", "p1_y", "p0_z", "p1_z"]
param_dict = dict.fromkeys(param_list)
trans_perf_param_dict = dict.fromkeys(param_list)

# Change file name if running elsewhere
file_name = f"rich{whichRich}_{survey_type}_params_p{whichPanel}.csv"
f = open(file_name, "r")
reader = csv.reader(f)
for row, param in zip(reader, param_dict.keys()):
    param_dict[param] = float(row[0])

f.close()

# start at optimum translation alignment
perf_align = [
    yml_opts[f"R{whichRich}"]["P0"]["perf_trans"]["x"],
    yml_opts[f"R{whichRich}"]["P1"]["perf_trans"]["x"],
    yml_opts[f"R{whichRich}"]["P0"]["perf_trans"]["y"],
    yml_opts[f"R{whichRich}"]["P1"]["perf_trans"]["y"],
    yml_opts[f"R{whichRich}"]["P0"]["perf_trans"]["z"],
    yml_opts[f"R{whichRich}"]["P1"]["perf_trans"]["z"],
]

for val, param in zip(perf_align, param_dict.keys()):
    trans_perf_param_dict[param] = float(val)

panel_nums = []
if whichPanel == "both":
    panel_nums.append(0)
    panel_nums.append(1)
else:
    panel_nums.append(whichPanel)

# update the conditons using ConditionsOverlay
if UseDD4Hep:
    # dd4hep.DumpConditions = True

    if manual_update:
        fromFile = f"./this_iter_conditions_R{whichRich}P{whichPanel}.yml"
        f = open(fromFile, "w")

        # create yml file for conditions overlay
        for p in [0, 1]:
            if survey_type == "translation":
                f.write(f"PDPanel{p}: !alignment\n")
                if p == int(
                    whichPanel
                ):  # update conditions for panel you want to survey
                    f.write(
                        f"  position: [{param_dict['p' + str(p) + '_x']} * mm, {param_dict['p' + str(p) + '_y']} * mm, {param_dict['p' + str(p) + '_z']} * mm]\n"
                        + f"  rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]"
                    )
                else:  # take optimimum alignment for panel you arent surveying
                    f.write(
                        f"  position: [{trans_perf_param_dict['p' + str(p) + '_x']} * mm, {trans_perf_param_dict['p' + str(p) + '_y']} * mm, {trans_perf_param_dict['p' + str(p) + '_z']} * mm]\n"
                        + f"  rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]"
                    )
            elif survey_type == "rotation":
                f.write(f"PDPanel{p}: !alignment\n")
                if p == int(
                    whichPanel
                ):  # update conditions for panel you want to survey
                    f.write(
                        f"  position: [{trans_perf_param_dict['p' + str(p) + '_x']} * mm, {trans_perf_param_dict['p' + str(p) + '_y']} * mm, {trans_perf_param_dict['p' + str(p) + '_z']} * mm]\n"
                        + f"  rotation: [{param_dict['p' + str(p) + '_x']} * mrad, {param_dict['p' + str(p) + '_y']} * mrad, {param_dict['p' + str(p) + '_z']} * mrad]"
                    )
                else:  # take optimimum alignment for panel you arent surveying
                    f.write(
                        f"  position: [{trans_perf_param_dict['p' + str(p) + '_x']} * mm, {trans_perf_param_dict['p' + str(p) + '_y']} * mm, {trans_perf_param_dict['p' + str(p) + '_z']} * mm]\n"
                        + f"  rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]"
                    )

            if (
                p == 0
            ):  # yaml file doesnt get read properly if there is a newline at the end
                f.write("\n")

        f.close()

        dd4hep.UseConditionsOverlay = True
        dd4hep.ConditionsOverlayInitPath = (
            "/group/rich/sw/alignment/ConditionsOverlayForPanels/"
        )

        toFile = f"/group/rich/sw/alignment/ConditionsOverlayForPanels/Conditions/Rich{whichRich}/Alignment/PDPanels.yml"

        shutil.copyfile(fromFile, toFile)

else:
    cond_update_list = []
    for panel_num in panel_nums:
        pre_cond_str = f"Conditions/Alignment/Rich1/PDPanel{panel_num}_Align :=  "
        optimum_trans_cond_str = f"double_v dPosXYZ = {trans_perf_param_dict['p' + str(panel_num) + '_x']} {trans_perf_param_dict['p' + str(panel_num) + '_y']} {trans_perf_param_dict['p' + str(panel_num) + '_z']}; "

        update_cond = f"double_v {param_key} = {param_dict['p' + str(panel_num) + '_x']} {param_dict['p' + str(panel_num) + '_y']} {param_dict['p' + str(panel_num) + '_z']};"
        cond_update_list.append(pre_cond_str + optimum_trans_cond_str + update_cond)

    UpdateManagerSvc().ConditionsOverride = cond_update_list

# # ####### 2nd July 2024 overlay
# dd4hep.UseConditionsOverlay = True
# dd4hep.ConditionsOverlayInitPath = '/group/online/alignment/prelim_align_2024/Modules_and_HalfLayers_TxRzTz_JPsi_contraint_mat3_0107_Run3map'
# condval = "!alignment{ position: [0 * mm, 0 * mm, 0 * mm],  rotation: [-0.0005157705398491993 * rad, 7.011816237252315e-05 * rad, -0.00042666842206765687 * rad]}"
# dd4hep.ConditionsOverride = {"/world/BeforeMagnetRegion/VP:alignment_delta": condval}
# # ####### 2nd July 2024 overlay

# ####### 4th July 2024 overlay
## NB: these overlays already included in /group/rich/sw/alignment/ConditionsOverlayForPanels/
# dd4hep.UseConditionsOverlay = True
# dd4hep.ConditionsOverlayInitPath = '/group/online/alignment/prelim_align_2024/07_01_Run3Map_withUT_0207'
condval = "!alignment{ position: [0 * mm, 0 * mm, 0 * mm],  rotation: [-0.000511080865103135 * rad, -0.00021828767680766154 * rad, -0.000866016317444813 * rad]}"
dd4hep.ConditionsOverride = {"/world/BeforeMagnetRegion/VP:alignment_delta": condval}
# ####### 4th July 2024 overlay

default_moni_opts = {}
default_reco_opts = {}
align_opts = {}

# Reco opts
wider_bkg = {"PhotonSelection": "None"}
default_reco_opts.update(wider_bkg)

if whichPanel == 0:
    panel_select = {"ActivatePanel": (True, False)}
elif whichPanel == 1:
    panel_select = {"ActivatePanel": (False, True)}
else:
    panel_select = {"ActivatePanel": (True, True)}
default_reco_opts.update(panel_select)

# Monitoring opts
if whichRich == 1:
    minP = 30.0
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": minP * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }
if whichRich == 2:
    minP = 60.0
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": minP * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }
default_moni_opts.update(tighter_minp)

# wider_histo = {'CKResHistoRange': (0.025, 0.005, 0.004)}
wider_histo = {"CKResHistoRange": (0.025, 0.0055, 0.0055)}
default_moni_opts.update(wider_histo)
useUT = {"UseUT": useUT_bool}
default_moni_opts.update(useUT)

nkevts = str(math.trunc(options.evt_max / 1000))

# save output file with histograms
if UseDD4Hep:
    options.histo_file = f"rich{whichRich}_opt_dd4hep_p{whichPanel}.root"
else:
    options.histo_file = f"rich{whichRich}_opt_p{whichPanel}.root"

PrForwardTrackingVelo.global_bind(
    MinQuality=0.0,
    DeltaQuality=0.0,
    MinTotalHits=9,
    MaxChi2PerDoF=50.0,
    MaxChi2XProjection=60.0,
    MaxChi2PerDoFFinal=28.0,
    MaxChi2Stereo=16.0,
    MaxChi2StereoAdd=16.0,
    MinP=minP * GeV,
)

PrMatchNN.global_bind(MinP=minP * GeV)
PrHybridSeeding.global_bind(MinP=minP * GeV)

# commented out when reconstruction changed to SuperPixels (change was at run 289434)
make_VeloClusterTrackingSIMD.global_bind(algorithm=VeloRetinaClusterTrackingSIMD)
make_velo_full_clusters.global_bind(make_full_cluster=VPRetinaFullClusterDecoder)
make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)

from Configurables import (
    ApplicationMgr,
    HltDecReportsDecoder,
    HltSelReportsDecoder,
    LHCb__UnpackRawEvent,
)
from PyConf.application import configured_ann_svc

unpacker = LHCb__UnpackRawEvent(
    "UnpackRawEvent",
    OutputLevel=2,
    RawBankLocations=["DAQ/RawBanks/HltDecReports", "DAQ/RawBanks/HltSelReports"],
    BankTypes=["HltDecReports", "HltSelReports"],
)

decDec = HltDecReportsDecoder(
    "HltDecReportsDecoder/Hlt1DecReportsDecoder",
    SourceID="Hlt1",
    RawBanks=unpacker.RawBankLocations[0],
)

selDec = HltSelReportsDecoder(
    "HltSelReportsDecoder/Hlt1SelReportsDecoder",
    SourceID="Hlt1",
    DecReports=unpacker.RawBankLocations[0],
    RawBanks=unpacker.RawBankLocations[1],
)

app = ApplicationMgr(
    TopAlg=[unpacker, decDec, selDec], ExtSvc=[configured_ann_svc(name="HltANNSvc")]
)

# by default, no additional filtering of the events
event_filter = []

# Prepare filter
import Functors
from PyConf.Algorithms import (
    HltDecReportsDecoder as PyConf_Algorithms_HltDecReportsDecoder,
)
from PyConf.Algorithms import (
    VoidFilter,
)
from PyConf.application import (
    default_raw_banks,
    default_raw_event,
)

lines = (
    ["Hlt1RICH1AlignmentDecision"] if whichRich == 1 else ["Hlt1RICH2AlignmentDecision"]
)

with default_raw_event.bind(raw_event_format=0.5):
    hlt1_dec_reports = PyConf_Algorithms_HltDecReportsDecoder(
        RawBanks=default_raw_banks("HltDecReports"), SourceID="Hlt1"
    )

    hlt1_filter = VoidFilter(
        name="Streaming_filter",
        Cut=Functors.DECREPORTS_FILTER(
            Lines=lines, DecReports=hlt1_dec_reports.OutputHltDecReportsLocation
        ),
    )

# when particular decision of HLT1 line about RICH1 or RICH2 is wanted
# if 'subset' not in current_variant:
if useHltDecisions:
    # prepare filter for selecting events
    # chosen for alignment of mirrors of particular RICH

    print(f"INFO: Using HLT decisions.")
    print("")

    event_filter = [hlt1_filter]

    print(f"INFO: ***Applying Event Filter***")
    print(f"INFO: {lines}")
    print(f"INFO: {hlt1_filter}")
    print(f"INFO: {type(hlt1_filter)}")
    print(f"INFO: {event_filter}")
    print(f"INFO: ***************************")
    sys.stdout.flush()


with (
    standalone_rich_panel_align_reco.bind(
        RichGas=radiator,
        EventFilter=event_filter if useHltDecisions else [],
        noUT=noUT_bool,
    ),
    alignment_rich_monitoring_options.bind(
        radiator=radiator, init_override_opts=align_opts
    ),
    default_ft_decoding_version.bind(value=6),
    default_rich_reco_options.bind(init_override_opts=default_reco_opts),
    default_rich_monitoring_options.bind(init_override_opts=default_moni_opts),
):
    run_reconstruction(options, standalone_rich_panel_align_reco)

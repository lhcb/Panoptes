###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import csv
import json

import matplotlib as mpl
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import griddata

mpl.rcParams["savefig.dpi"] = 300  # Set the DPI to 300

parser = argparse.ArgumentParser()

parser.add_argument(
    "--whichRich",
    default="both",
    type=str,
    help="Set to both for both. Set to 1 for RICH1. Set to 2 for RICH2)",
)
parser.add_argument(
    "--whichPanel",
    default="both",
    type=str,
    help="Set to both for both. Set to 0 for Panel0. Set to 1 for Panel1)",
)
parser.add_argument(
    "--survey_type",
    default="translation",
    choices=["translation", "rotation"],
    type=str,
    help="Choose which survey study to plot",
)
parser.add_argument("--zoom", default="False", type=str, help="Set to True to zoom in")
parser.add_argument(
    "--columns", default="False", type=str, help="Perform scans of columns"
)
parser.add_argument(
    "--twoD_scan", type=str, help="Perform 2D or 1D scans", default="False"
)
parser.add_argument("--csv_path", default="", help="path to csv file with results")
# parser.add_argument('--axis_name', type=str, default = "", help='Which axis to translate along or rotate about',  choices = ["x", "y", "z", ""])

args = parser.parse_args()
whichRich = int(args.whichRich)
whichPanel = int(args.whichPanel)
survey_type = args.survey_type
zoom = json.loads(args.zoom.lower())
twoD_scan = json.loads(args.twoD_scan.lower())
columns = json.loads(args.columns.lower())
csv_path = args.csv_path
# axis_name = args.axis_name if not twoD_scan else "xy"

### load options from YAML
from ruamel.yaml import YAML

yaml = YAML()

# read
with open("survey_opts.yml") as opts_yml_file:
    yml_opts = yaml.load(opts_yml_file)

perf_align = {
    "p0_x": yml_opts[f"R{whichRich}"]["P0"]["perf_trans"]["x"],
    "p1_x": yml_opts[f"R{whichRich}"]["P1"]["perf_trans"]["x"],
    "p0_y": yml_opts[f"R{whichRich}"]["P0"]["perf_trans"]["y"],
    "p1_y": yml_opts[f"R{whichRich}"]["P1"]["perf_trans"]["y"],
    "p0_z": yml_opts[f"R{whichRich}"]["P0"]["perf_trans"]["z"],
    "p1_z": yml_opts[f"R{whichRich}"]["P1"]["perf_trans"]["z"],
}

if ".csv" not in csv_path:
    stack_path = yml_opts["stack_path"]
    prepath = f"{stack_path}/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/PanelAlignment/surveys"
    path_to_csvs = f"{prepath}/survey_results/{survey_type}/panel_{whichPanel}"
    path_to_csvs = prepath  # temp for now
else:
    path_to_csvs = csv_path

if survey_type == "translation":
    opt_pos = {
        "R1": {"x": [0.0, 0.0], "y": [0.0, 0.0], "z": [0.0, 0.0]},
        "R2": {"x": [0.0, 0.0], "y": [0.0, 0.0], "z": [0.0, 0.0]},
    }
if survey_type == "rotation":
    opt_pos = {
        "R1": {"x": [0.0, 0.0], "y": [0.0, 0.0], "z": [0.0, 0.0]},
        "R2": {"x": [0.0, 0.0], "y": [0.0, 0.0], "z": [0.0, 0.0]},
    }

panel_names = ["top", "bottom"] if whichRich == 1 else ["ASide-left", "CSide-right"]


def get_csv(rich, p_axis, path_to_csvs):
    res_dict = {}
    # obtain axes and panel number
    split_p_axis = list(map(str, p_axis))
    p = split_p_axis[0]
    axes = split_p_axis[1:]
    if ".csv" in path_to_csvs:
        file_name = path_to_csvs
    else:
        if twoD_scan:
            file_name = f"{path_to_csvs}/ck_vs_param_R{rich}_{survey_type}_panel{whichPanel}.csv"
        else:
            file_name = (
                f"{path_to_csvs}/ck_vs_param_R{rich}_{survey_type}_panel{p_axis}.csv"
            )

    import threading

    lock = threading.Lock()
    lock.acquire()

    try:
        print(f"Reading file: \n {file_name}")
        with open(file_name, "r") as f:
            reader = csv.reader(f)
            plotting_vars = list(reader)
    except:
        print(f"File: {file_name} not found.")
        print("Setting Ckres to linspace of zeros")
        res_dict[f"ckRes"] = np.zeros(30)
        res_dict[f"ckRes_err"] = np.zeros(30)
        res_dict[f"{survey_type}_{axes[0]}"] = np.zeros(30)
        return res_dict

    lock.release()

    p_array = np.array(plotting_vars)
    ckRes_str = p_array[:, 0]
    conds_str = p_array[:, 1]
    ckRes_err_str = p_array[:, 2]
    ck_gauss_mean_str = p_array[:, 3]
    if columns:
        col_num_str = p_array[:, 4]
        res_dict[f"col_num"] = np.array(col_num_str, dtype=int)

    res_dict[f"ckRes"] = np.array(ckRes_str, dtype=np.float32)
    res_dict[f"ckRes_err"] = np.array(ckRes_err_str, dtype=np.float32)
    res_dict[f"ck_gauss_mean"] = np.array(ck_gauss_mean_str, dtype=np.float32)

    # TODO change this when I change the _ when writing results
    for axis in axes:
        res_dict[f"{survey_type}_{axis}"] = []

    for cond in conds_str:
        cond_dict = {
            "p0_x": None,
            "p1_x": None,
            "p0_y": None,
            "p1_y": None,
            "p0_z": None,
            "p1_z": None,
        }

        cond_split_str = cond.split(":")

        # store respective conditions from string
        cond_dict["p0_x"] = cond_split_str[0].replace("p0_x", "")  # e.g p0_x-1.5
        cond_dict["p1_x"] = cond_split_str[1].replace("p1_x", "")
        cond_dict["p0_y"] = cond_split_str[2].replace("p0_y", "")
        cond_dict["p1_y"] = cond_split_str[3].replace("p1_y", "")
        cond_dict["p0_z"] = cond_split_str[4].replace("p0_z", "")
        cond_dict["p1_z"] = cond_split_str[5].replace("p1_z", "")

        for axis in axes:
            res_dict[f"{survey_type}_{axis}"].append(float(cond_dict[f"p{p}_{axis}"]))

    return res_dict


if twoD_scan:

    def plot_2d_scan(res_dict, extra_text=""):
        x, y, z, ck_gauss_mean, ck_res_err = (
            res_dict[f"{survey_type}_x"],
            res_dict[f"{survey_type}_y"],
            res_dict[f"ckRes"],
            res_dict[f"ck_gauss_mean"],
            res_dict[f"ckRes_err"],
        )

        gauss_mean_thres, default_value = 1, 2.5
        print(
            f"Setting Ckres with gaussian mean > {gauss_mean_thres} to {default_value} rad...."
        )
        x, y, z = [], [], []
        bad_fit_counter = 0
        for i in range(0, len(res_dict[f"{survey_type}_y"])):
            x.append(res_dict[f"{survey_type}_x"][i])
            y.append(res_dict[f"{survey_type}_y"][i])
            if abs(res_dict[f"ck_gauss_mean"][i]) > gauss_mean_thres:
                z.append(default_value)
                bad_fit_counter += 1
            else:
                z.append(res_dict["ckRes"][i])

        print(
            f"Number of CKres fits with gaussian mean > {gauss_mean_thres}: {bad_fit_counter} ({bad_fit_counter / len(res_dict[f'{survey_type}_y']) * 100} %)."
        )

        if columns:
            print("Taking absolute values of the resolutions.")
            z = [abs(res) for res in z]

        for i in range(len(z)):
            if whichRich == 1:
                if z[i] > 3 or z[i] < 0.4:
                    z[i] = 3
            if whichRich == 2:
                if z[i] > 2 or z[i] < 0.2:
                    z[i] = 2

        xi = np.linspace(np.min(x), np.max(x), 1000)
        yi = np.linspace(np.min(y), np.max(y), 1000)

        zi = griddata((x, y), z, (xi[None, :], yi[:, None]), method="linear")

        center_val = z[int(0.35 * len(z))]  # center_val is at 35% of the colour bar
        if whichRich == 1 and whichPanel == 0:
            # center_val = 1.185
            n_levels = 50
            n_divs = 200
            zi = np.nan_to_num(zi, nan=1.24)
        if whichRich == 2 and whichPanel == 0:
            # center_val = 0.69
            n_levels = 50
            n_divs = 250
            zi = np.nan_to_num(zi, nan=2)
        if whichRich == 1 and whichPanel == 1:
            # center_val = 1.3
            n_levels = 45
            n_divs = 150
            zi = np.nan_to_num(zi, nan=3)
        if whichRich == 2 and whichPanel == 1:
            # center_val = 0.694
            n_levels = 200
            n_divs = 160
            zi = np.nan_to_num(zi, nan=1)

        fig, ax = plt.subplots()

        bounds = np.linspace(zi.min(), center_val, n_divs)
        bounds = np.append(bounds, zi.max())
        norm = colors.BoundaryNorm(boundaries=bounds, ncolors=256)

        if columns:
            norm = None
            n_levels = 50
        """
        if you get ValueError: bins must be monotonically increasing or decreasing,
        adjust center_val
        """
        pcm = ax.contourf(
            xi,
            yi,
            zi,
            norm=norm,
            # vmin = 0.0, vmax =2,
            cmap="viridis",
            levels=n_levels,
        )

        cbar = fig.colorbar(pcm)
        cbar.set_label(r"$\sigma[\Delta\theta_{{\mathrm{{Cherenkov}}}}]$")
        # add tick where current alignment lies
        ticks = cbar.get_ticks().tolist()  # Get the default ticks as a list
        alignment_index = next(
            (
                i
                for i, (a, b) in enumerate(zip(x, y))
                if a == perf_align[f"p{whichPanel}_x"]
                and b == perf_align[f"p{whichPanel}_y"]
            ),
            None,
        )

        if alignment_index is None:
            alignment_index = 0
        ticks.append(z[alignment_index])
        ticks.sort()  # Sort the ticks in ascending order
        labels = []
        for t in ticks:
            labels.append("{:.3f}".format(t))
        cbar.set_ticks(ticks)
        cbar.set_ticklabels(labels)

        # Find the "Current Alignment" tick label and set its color to red
        for label in cbar.ax.get_yticklabels():
            if label.get_text() == "{:.3f}".format(z[alignment_index]):
                label.set_color("r")
                break

        # obtain minima and write
        ckres_min_index = np.where(z == min(z))[0][0]
        ckres_pot_improv = abs(z[alignment_index] - z[ckres_min_index])
        bbox_props = dict(boxstyle="round,pad=0.3", fc="white", ec="gray", lw=1)
        ax.text(
            0.8,
            0.97,
            rf"Potential Improvement in $\sigma[\Delta\theta_{{\mathrm{{Cherenkov}}}}]$ = {'{:.4f}'.format(ckres_pot_improv)} ({'{:.2f}'.format(ckres_pot_improv / z[alignment_index] * 100.0)}%)",
            transform=fig.transFigure,
            ha="right",
            va="top",
            fontsize=8,
            bbox=bbox_props,
        )

        if zoom:
            ax.set_xlim(
                perf_align[f"p{whichPanel}_x"] - 3, perf_align[f"p{whichPanel}_x"] + 3
            )
            ax.set_ylim(
                perf_align[f"p{whichPanel}_y"] - 3, perf_align[f"p{whichPanel}_y"] + 3
            )

        # ax.set_ylim(0.0,-10.5)
        # ax.set_xlim(-6,6)

        ax.scatter(x, y, c="k", edgecolor="w", s=3)
        ax.scatter(
            perf_align[f"p{whichPanel}_x"],
            perf_align[f"p{whichPanel}_y"],
            marker="*",
            s=40,
            c="red",
            label="Current alignment",
        )

        ax.scatter(
            x[ckres_min_index],
            y[ckres_min_index],
            marker="*",
            s=40,
            c="m",
            label="Update",
        )

        ax.legend()
        ax.set_xlabel("x (mm)")
        ax.set_ylabel("y (mm)")
        ax.set_title(f"RICH {whichRich} {panel_names[whichPanel]} {extra_text}")
        fig.savefig(
            f"CKRes_2DScan_R{whichRich}_{panel_names[whichPanel]}_{extra_text}_zoom_{zoom}.png"
        )

        # print minima
        print(
            f"R{whichRich} {panel_names[whichPanel]} min Ckres: {z[ckres_min_index]} +\- {ck_res_err[ckres_min_index]} mrad"
        )
        print(f"x: {x[ckres_min_index]} mm, y: {y[ckres_min_index]} mm")

    res_dict = get_csv(whichRich, f"{whichPanel}xy", path_to_csvs)
    if columns:
        # extract per column results
        if whichRich == 1:
            column_nums = [i for i in range(0, 11)]
        if whichRich == 2:
            column_nums = [i for i in range(1, 13)]

        for col_num in column_nums:
            print(f"Column {col_num}/{column_nums[-1]}")
            res_dict_col = {key: [] for key in res_dict.keys()}
            for i in range(len(res_dict["ckRes"])):
                if res_dict["col_num"][i] == col_num:
                    for key in res_dict_col.keys():
                        res_dict_col[key].append(res_dict[key][i])
            if len(res_dict_col["ckRes"]) == 0:
                continue
            try:
                plot_2d_scan(res_dict_col, extra_text=f"Column{col_num}")
            except Exception as e:
                print(f"Plotting failed for column {col_num}.")
                print("Error: " + str(e))
                print("Skipping...")
    else:
        plot_2d_scan(res_dict)

    quit()

else:
    pdf = PdfPages(
        f"RICH_paneltests_RICH-{whichRich}_panel-{whichPanel}_surveyType-{survey_type}_zoom-{zoom}.pdf"
    )

    x_res_dict = get_csv(whichRich, f"{whichPanel}x", path_to_csvs)
    y_res_dict = get_csv(whichRich, f"{whichPanel}y", path_to_csvs)
    z_res_dict = get_csv(whichRich, f"{whichPanel}z", path_to_csvs)

ncols = 1
nrows = 3

fig, ax = plt.subplots(figsize=(16, 16), nrows=nrows, ncols=ncols)
kwargs = {"fmt": ".", "color": "black"}

axes_dict = {
    "R1": {"x": [None, None], "y": [None, None], "z": [None, None]},
    "R2": {"x": [None, None], "y": [None, None], "z": [None, None]},
}

ax[0].errorbar(
    x_res_dict[f"{survey_type}_x"],
    x_res_dict[f"ckRes"],
    yerr=x_res_dict[f"ckRes_err"],
    label=f"Panel {whichPanel} x {survey_type}, RICH{whichRich}",
    **kwargs,
)
ax[1].errorbar(
    y_res_dict[f"{survey_type}_y"],
    y_res_dict[f"ckRes"],
    yerr=y_res_dict[f"ckRes_err"],
    label=f"Panel {whichPanel} y {survey_type}, RICH{whichRich}",
    **kwargs,
)
ax[2].errorbar(
    z_res_dict[f"{survey_type}_z"],
    z_res_dict[f"ckRes"],
    yerr=z_res_dict[f"ckRes_err"],
    label=f"Panel {whichPanel} z {survey_type}, RICH{whichRich}",
    **kwargs,
)
axes_dict[f"R{whichRich}"]["x"][whichPanel] = ax[0]
axes_dict[f"R{whichRich}"]["y"][whichPanel] = ax[1]
axes_dict[f"R{whichRich}"]["z"][whichPanel] = ax[2]

subplot_counter = 0
subplot_threshold = 3
for direction in axes_dict[f"R{whichRich}"].keys():
    for panel_num in [0, 1]:
        axs = axes_dict[f"R{whichRich}"][direction][panel_num]
        if axs is None:
            plt.close()
            continue

        axs.legend(fontsize=22)

        opt_pos_axs = opt_pos[f"R{whichRich}"][direction][panel_num]
        axs.axvline(x=opt_pos_axs, ls="--", c="r")

        if zoom:
            if whichRich == 1 and panel_num == 0:
                axs.set_ylim(1, 1.2)
                if "z" in direction:
                    axs.set_ylim(0.8, 1.8)
            if whichRich == 1 and panel_num == 1:
                axs.set_ylim(1.05, 1.25)
                if "z" in direction:
                    axs.set_ylim(0.8, 1.8)
            if whichRich == 2 and panel_num == 0:
                axs.set_ylim(0.6, 0.7)
                if "z" in direction:
                    axs.set_ylim(0.5, 0.9)
            if whichRich == 2 and panel_num == 1:
                axs.set_ylim(0.5, 0.66)
                if "z" in direction:
                    axs.set_ylim(0.5, 0.9)

            # if whichRich == 1:
            #     axs.set_ylim(0.8,2)
            # if whichRich == 2:
            #     axs.set_ylim(0.4,0.9)

        if survey_type == "translation":
            axs.set_xlabel("Panel alignment shift / mm", fontsize=20)
        if survey_type == "rotation":
            axs.set_xlabel("Panel alignment rotation / mrad", fontsize=20)
        axs.set_ylabel(r"$\theta_{C}$ resolution / mrad", fontsize=20)

        subplot_counter += 1
        if subplot_counter == subplot_threshold:
            fig.tight_layout()
            pdf.savefig(fig)
            plt.close()
            subplot_counter = 0

pdf.close()

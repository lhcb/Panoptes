###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse

parser = argparse.ArgumentParser()

parser.add_argument(
    "--whichRich",
    default="both",
    type=str,
    help="Set to both for both. Set to 1 for RICH1. Set to 2 for RICH2)",
)
parser.add_argument(
    "--whichPanel",
    default="both",
    type=str,
    help="Set to both for both. Set to 0 for Panel0. Set to 1 for Panel1)",
)
parser.add_argument(
    "--conddb_path",
    default="",
    type=str,
    help="Directory of local copy of conditions database",
)
parser.add_argument(
    "--pos_condition", default="", type=str, help="Position condition to update"
)
parser.add_argument(
    "--rot_condition", default="", type=str, help="Rotation condition to update"
)

args = parser.parse_args()

whichRich = args.whichRich
whichPanel = args.whichPanel
conddb_path = args.conddb_path
pos_condition = (args.pos_condition).replace("_", " ")
rot_condition = (args.rot_condition).replace("_", " ")

# filename = f'{conddb_path}Conditions/Rich{whichRich}/Alignment/PDPanels.yml/255000'
filename = f"{conddb_path}Conditions/Rich{whichRich}/Alignment/PDPanels.yml/0"

new_file = ""

import threading

lock = threading.Lock()
lock.acquire()
with open(filename, "r") as f:
    panel_num = None
    for line in f:
        newline = line

        if "Panel0" in line:
            panel_num = 0
        if "Panel1" in line:
            panel_num = 1

        if int(whichPanel) == panel_num:
            if "position" in line:
                newline = "  " + pos_condition + " \n"
            if "rotation" in line:
                newline = "  " + rot_condition + " \n"

        new_file += newline

lock.release()

fout = open(filename, "w")
fout.write(new_file)
fout.close()

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import csv
import os
import random
import re
import subprocess
import sys

import matplotlib.pyplot as plt
import numpy as np
from ruamel.yaml import YAML

yaml = YAML()

n_aligments = 4  # number of alignments
n_aligments_list = list(range(1, n_aligments + 1))
alignments_dir = "/home/jreich/stack_dd4hep_v3/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/PanelAlignment/alignment_conditions"

### obtain results
results_dict = {f"v{alignment_num}": {} for alignment_num in n_aligments_list}

for whichRich in [1, 2]:
    for whichPanel in [0, 1]:
        for alignment_num in n_aligments_list:
            # read yaml file
            with open(
                f"{alignments_dir}/Rich{whichRich}/v{alignment_num}.yml"
            ) as yml_file:
                alignment_yml = yaml.load(yml_file)

                results_dict[f"v{alignment_num}"][f"R{whichRich}P{whichPanel}"] = {
                    "px": float(
                        alignment_yml[f"PDPanel{whichPanel}"]["position"][0].split(" ")[
                            0
                        ]
                    ),
                    "py": float(
                        alignment_yml[f"PDPanel{whichPanel}"]["position"][1].split(" ")[
                            0
                        ]
                    ),
                    "pz": float(
                        alignment_yml[f"PDPanel{whichPanel}"]["position"][2].split(" ")[
                            0
                        ]
                    ),
                }

### plot

for whichRich in [1, 2]:
    panel_names = ["top", "bottom"] if whichRich == 1 else ["ASide-left", "CSide-right"]
    for whichPanel in [0, 1]:
        fig = plt.figure()

        # obtain each position for each alignment version in a list (e.g posx = [v1res,v2res,v3res,v4res])
        xpos, ypos, zpos = [], [], []
        xtick_labels = []
        for alignment_num in n_aligments_list:
            xpos.append(
                results_dict[f"v{alignment_num}"][f"R{whichRich}P{whichPanel}"]["px"]
            )
            ypos.append(
                results_dict[f"v{alignment_num}"][f"R{whichRich}P{whichPanel}"]["py"]
            )
            zpos.append(
                results_dict[f"v{alignment_num}"][f"R{whichRich}P{whichPanel}"]["pz"]
            )
            xtick_labels.append(f"v{alignment_num}")

        plt.scatter(range(len(xpos)), xpos, label="x", marker="s", color="red")
        plt.scatter(range(len(ypos)), ypos, label="y", marker="o", color="blue")
        plt.scatter(range(len(zpos)), zpos, label="z", marker="*", color="orange")

        plt.xticks(range(len(xtick_labels)), xtick_labels)

        plt.xlabel("alignment version")
        plt.ylabel("position [mm] (from zero positions in condDB)")
        plt.title(f"RICH{whichRich} {panel_names[whichPanel]}")
        plt.ylim(-7, 13)
        plt.legend(loc="best")

        plt.savefig(
            f"alignment_version_movements_R{whichRich}_{panel_names[whichPanel]}.pdf"
        )

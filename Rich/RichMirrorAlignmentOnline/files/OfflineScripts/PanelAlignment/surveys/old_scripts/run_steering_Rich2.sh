###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
echo "Running reconstruction"
/afs/cern.ch/work/j/jreich/stack/Panoptes/run  gaudirun.py /afs/cern.ch/work/j/jreich/stack/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/PanelAlignment/surveys/hlt2_reco_rich2_noUT_panoptes_survey.py > steeringR2_log.log 2>&1
echo "Done"

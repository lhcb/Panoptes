#!/bin/bash
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

echo "################    Running RICH 1    #################"
echo " "
python optimise_RICH.py 1

echo " "
echo "################    Running RICH 2    #################"
echo " "
python optimise_RICH.py 2

echo "DONEDONEDONEDONE       DONE       DONEDONEDONEDONE"

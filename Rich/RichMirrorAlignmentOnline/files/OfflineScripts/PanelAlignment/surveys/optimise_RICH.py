###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import csv
import json
import os
import random
import re
import subprocess
import sys

import numpy as np
from GaudiPython import gbl
from ROOT import (
    TF1,
    TH1D,
    TH2D,
    TCanvas,
    TExec,
    TFile,
    TGraph,
    TLine,
    TMultiGraph,
    gROOT,
    gStyle,
)
from scipy.optimize import minimize

# Set batch mode so histograms aren't printed
# sys.argv.append('-b')
gROOT.SetBatch(True)


class AlignMonitor:
    def __init__(
        self,
        whichRich,
        floated,
        #            _alignConf,
        #            _maxIt,
    ):
        """
        self.alignConf = _alignConf
        self.nameStr = self.alignConf.getProp('nameStr')
        self.whichRich = self.alignConf.getProp('Rich')
        self.displayMode = self.alignConf.getProp('displayMode')
        self.warningFactor = self.alignConf.getProp('warningFactor')
        self.maxIt = _maxIt  # the maximum labeled number of the iterations that we are looping through
        """
        self.workdir = "./"
        self.whichRich = whichRich
        self.maxIt = 0
        self.floated_str = "_".join(str(round(f, 2)) for f in floated)
        self.prename = f"{prepath}/rich{self.whichRich}_opt_dd4hep_p{whichPanel}.root"

        # RICH1 settings
        self.maxpri = 3
        self.binToPriMirr = [[2, 3], [0, 1]]
        self.maxsec = 15
        self.binToSecMirr = [
            [10, 11, 14, 15],
            [8, 9, 12, 13],
            [2, 3, 6, 7],
            [0, 1, 4, 5],
        ]
        self.fitMin = -0.0055
        self.fitMax = 0.0055
        # if RICH2, use RICH2 settings
        if self.whichRich == 2:
            self.maxpri = 55
            self.binToPriMirr = [
                [3, 2, 1, 0, 31, 30, 29, 28],
                [7, 6, 5, 4, 35, 34, 33, 32],
                [11, 10, 9, 8, 39, 38, 37, 36],
                [15, 14, 13, 12, 43, 42, 41, 40],
                [19, 18, 17, 16, 47, 46, 45, 44],
                [23, 22, 21, 20, 51, 50, 49, 48],
                [27, 26, 25, 24, 55, 54, 53, 52],
            ]
            self.maxsec = 39
            self.binToSecMirr = [
                [3, 2, 1, 0, 23, 22, 21, 20],
                [7, 6, 5, 4, 27, 26, 25, 24],
                [11, 10, 9, 8, 31, 30, 29, 28],
                [15, 14, 13, 12, 35, 34, 33, 32],
                [19, 18, 17, 16, 39, 38, 37, 36],
            ]
            # Original fit range
            self.fitMin = -0.0055
            self.fitMax = 0.0055
            # self.fitMin = -0.0025
            # self.fitMax = 0.0025
        self.isInsane = False
        self.finalCKres = None
        self.finalCKresErr = None
        self.finalCKgaussianmean = None

    def performMonitoring(self, p, hist_path=""):
        import os

        gStyle.SetOptFit(1111)
        gStyle.SetOptStat(000000000)

        if hist_path == "":
            pdf_suff = ""
        else:
            pdf_suff = "_" + hist_path.split("/")[-1]

        # File for the RICH piquet AND for the RICH mirror alignment experts
        if num_chunks != 0:
            expertFile = f"Rich{self.whichRich}_{survey_type}_{p}_({self.floated_str})_{pdf_suff}.pdf"
        else:
            if os.path.isdir(self.workdir + "plots") is not True:
                os.mkdir(self.workdir + "plots")

            expertFile = (
                self.workdir
                + f"plots/Rich{self.whichRich}_{survey_type}_{p}_({self.floated_str})_{pdf_suff}.pdf"
            )
        # File for the Alignment piquet ("duplicating" what goes to the Presenter for the Data Manager [except CK angle res stuff])
        monitorFile = self.workdir + f"Rich{self.whichRich}_AlignMonitor.pdf"
        # File with for testing purposes
        testingFile = self.workdir + f"Rich{self.whichRich}_AlignTesting.pdf"
        # Data Manager can only get histograms sent via self.monSvc

        theCanvas = TCanvas("theCanvas", "MirrAlign")
        theCanvas.Divide(2, 2, 0.001, 0.001)

        theCanvas2 = TCanvas("theCanvas2", "MirrAlignTest")
        theCanvas2.Divide(2, 2, 0.001, 0.001)

        theCanvas2.Clear()
        theCanvas.Clear()

        # generate resHistograms

        if self.maxIt == 0:
            pass
        elif self.maxIt < 2:
            theCanvas.Divide(2, 1, 0.0025, 0.0025)
        elif self.maxIt < 3:
            theCanvas.Divide(3, 1, 0.0025, 0.0025)
        elif self.maxIt < 4:
            theCanvas.Divide(2, 2, 0.0025, 0.0025)
        elif self.maxIt < 6:
            theCanvas.Divide(3, 2, 0.0025, 0.0025)
        elif self.maxIt < 8:
            theCanvas.Divide(4, 2, 0.0025, 0.0025)
        elif self.maxIt < 9:
            theCanvas.Divide(3, 3, 0.0025, 0.0025)
        else:
            theCanvas.Divide(4, 4, 0.0025, 0.0025)

        # reserve spaces for resHistograms
        resHistograms = [None] * (self.maxIt + 1)
        polbkg = [None] * (self.maxIt + 1)
        histFunc = [None] * (self.maxIt + 1)
        hist = [None] * (self.maxIt + 1)
        filename = [None] * (self.maxIt + 1)
        thisFile = [None] * (self.maxIt + 1)
        title = [None] * (self.maxIt + 1)
        fitRes = [None] * (self.maxIt + 1)

        # define resHistoTrend
        resHistoTrend = TH1D(
            "resHistoTrend",
            "RICH"
            + str(self.whichRich)
            + " Cherenkov angle resolution (mrad) per It. ",
            self.maxIt + 1,
            -0.5,
            self.maxIt + 0.5,
        )

        for j in reversed(range(0, self.maxIt + 1)):
            filename[j] = self.prename
            if os.path.exists(filename[j]):
                thisFile[j] = TFile(filename[j], "read")

                if hist_path == "":
                    hist[j] = (
                        thisFile[j].Get(
                            f"RICH/RiCKResLongTight/Rich{self.whichRich}Gas/ckResAll"
                        )
                    ).Clone()
                else:
                    hist[j] = (thisFile[j].Get(f"{hist_path}")).Clone()

                if (j == 0) and (j == self.maxIt):
                    hist[j].SetName("resHisto0")
                    hist[j].SetName("resHistoN")
                elif j == self.maxIt:
                    hist[j].SetName("resHistoN")
                elif j == 0:
                    hist[j].SetName("resHisto0")
                if j != 0:
                    hist[j].SetName("resHisto" + str(j))

                # perform the fit and extract fit models
                fitRes[j], resHistograms[j] = self.fitCherenkovAngle(hist[j])

                histFunc[j] = fitRes[j].overallFitFunc
                polbkg[j] = fitRes[j].bkgFitFunc

                try:
                    ckres = histFunc[j].GetParameter(2) * 1000
                except:
                    print(
                        f"WARNING: Fit failed (NEntries = {hist[j].GetEntries()}). Setting fit results to large numbers and continuing."
                    )

                    if j == self.maxIt:
                        self.finalCKres = 100
                        self.finalCKresErr = 100
                        self.finalCKgaussianmean = 100

                    resHistoTrend.SetBinContent(j + 1, 100)
                    resHistoTrend.SetBinError(j + 1, 100)

                    title[j] = (
                        "RICH"
                        + str(self.whichRich)
                        + " Cherenkov angle resolution It. "
                        + str(j)
                    )
                    hist[j].SetTitle(title[j])
                    hist[j].SetXTitle("#Delta#theta_{Cherenkov} (rad)")
                    hist[j].SetYTitle("Entries")

                    gStyle.SetStatW(0.11)

                    theCanvas2.cd()
                    hist[j].Draw()
                    theCanvas2.Print(expertFile)

                    theCanvas.cd()
                    theCanvas.cd(1 + j)
                    hist[j].Draw()
                    thisFile[j].Close("R")

                    # Make resHistoTrend iteration trend plot
                    resHistoTrend.SetMarkerStyle(8)
                    resHistoTrend.SetXTitle("iteration number")
                    resHistoTrend.SetYTitle(
                        "#splitline{Cherenkov angle resolution (mrad)}{}"
                    )

                    theCanvas2.Clear()
                    theCanvas.Clear()
                    theCanvas.Close()
                    theCanvas2.Close()
                    resHistoTrend.DrawCopy()
                    sys.stdout.flush()

                    return

                ckres_error = histFunc[j].GetParError(2) * 1000
                gaussianmean = histFunc[j].GetParameter(1) * 1000

                resHistoTrend.SetBinContent(j + 1, ckres)
                resHistoTrend.SetBinError(j + 1, ckres_error)

                print(
                    "_i" + str(j) + " CK angle resolution:     " + str(ckres) + " mrad."
                )
                print(
                    "_i"
                    + str(j)
                    + " CK angle resolution err: "
                    + str(ckres_error)
                    + " mrad."
                )
                print(
                    "_i"
                    + str(j)
                    + " CK angle gaussian mean: "
                    + str(gaussianmean)
                    + " mrad."
                )

                if j == self.maxIt:
                    self.finalCKres = ckres
                    self.finalCKresErr = ckres_error
                    self.finalCKgaussianmean = gaussianmean

                title[j] = (
                    "RICH"
                    + str(self.whichRich)
                    + " Cherenkov angle resolution It. "
                    + str(j)
                )
                resHistograms[j].SetTitle(title[j])
                resHistograms[j].SetXTitle("#Delta#theta_{Cherenkov} (rad)")
                resHistograms[j].SetYTitle("Entries")

                histFunc[j].SetLineColor(2)
                polbkg[j].SetLineColor(4)

                gStyle.SetStatW(0.11)

                theCanvas2.cd()
                resHistograms[j].Draw()
                histFunc[j].Draw("SAME")
                polbkg[j].Draw("SAME")

                theCanvas2.Print(expertFile)

                theCanvas.cd()
                theCanvas.cd(1 + j)
                resHistograms[j].Draw()
                histFunc[j].Draw("SAME")
                polbkg[j].Draw("SAME")

                thisFile[j].Close("R")
            else:
                print("WARNING: " + filename[j] + ": does not exist")

        # theCanvas.Print(monitorFile)

        # sys.stdout.flush()

        # Make resHistoTrend iteration trend plot
        resHistoTrend.SetMarkerStyle(8)
        resHistoTrend.SetXTitle("iteration number")
        resHistoTrend.SetYTitle("#splitline{Cherenkov angle resolution (mrad)}{}")

        theCanvas2.Clear()
        theCanvas.Clear()
        theCanvas.Close()
        theCanvas2.Close()
        resHistoTrend.DrawCopy()
        sys.stdout.flush()

    def fitCherenkovAngle(self, hist):
        # get Chris Jones's C++ fitter
        fitter = gbl.Rich.Rec.CKResolutionFitter()

        # set signal (AsymNormal) and background (FreeNPol) models
        fitter.params().RichFitTypes[whichRich - 1].clear()
        fitter.params().RichFitTypes[whichRich - 1].push_back("AsymNormal:FreeNPol")

        # change fit ranges
        fitter.params().RichFitMin[0] = self.fitMin
        fitter.params().RichFitMin[1] = self.fitMin
        fitter.params().RichFitMax[0] = self.fitMax
        fitter.params().RichFitMax[1] = self.fitMax

        res = fitter.fit(hist, whichRich)

        return res, hist


def write_to_csv(x):
    with open(f"rich{whichRich}_{survey_type}_params_p{whichPanel}.csv", "w") as f:
        wtr = csv.writer(f)
        for axis in x.keys():
            wtr.writerow([x[axis]])


def check_if_already_run(x, csv_name):
    conditions = []
    for axis in x.keys():
        if x[axis] is not None:
            conditions.append(f"{axis}{x[axis]}")

    conds_to_write = ":".join(str(d) for d in conditions)
    # extract conditions from current iteration
    pattern = r"(?:x|y|z)(-?\d+\.\d+)"
    match1 = re.findall(pattern, conds_to_write)
    match1 = [float(cond) for cond in match1]

    # check if within 0.1mm of an already run reconstruction
    def is_close_to_match(match1, match2):
        tolerance = 0.001
        for i in range(len(match1)):
            if not (abs(match2[i] - match1[i]) <= tolerance):
                return False

        return True

    if os.path.exists(f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv"):
        reconstruction_already_run = False
        with open(f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv", "r") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                # extract conditions for row in results file
                match2 = re.findall(pattern, row[1])
                match2 = [float(cond) for cond in match2]

                reconstruction_already_run = is_close_to_match(match1, match2)

                if reconstruction_already_run:
                    print("INFO: Reconstruction has already completed. Skipping...")
                    return reconstruction_already_run

        return reconstruction_already_run
    else:
        print(
            f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv , does not exist. This must be the 1st time you have run this configuration."
        )
        return


def check_for_reco_issues(finalCKres, finalCKresErr, csv_name):
    issue_present = False
    if os.path.exists(f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv"):
        with open(f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv", "r") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if float(finalCKresErr) == 0.0:
                    issue_present = True
                    print(
                        "INFO: CK resolution error is 0.0. Rerunning reconstruction..."
                    )

                if issue_present:
                    return issue_present

    return issue_present


def write_plotting(ck, x, ckerr, ck_gaussian_mean, csv_name, col_num):
    conditions = []

    for axis in x.keys():
        if x[axis] is not None:
            conditions.append(f"{axis}{x[axis]}")

    conds_to_write = ":".join(str(d) for d in conditions)

    with open(f"ck_vs_param_R{whichRich}_{survey_type}_{csv_name}.csv", "a") as f:
        writer = csv.writer(f)
        if "all" in col_num:
            writer.writerow([ck, conds_to_write, ckerr, ck_gaussian_mean])
        else:
            writer.writerow([ck, conds_to_write, ckerr, ck_gaussian_mean, col_num])


def monitor(x, panel):
    """
    Function to be minimised
    x - tuple of alignment parameters as:
        (panel0_x, panel1_x, panel0_y, panel1_y, panel0_z, panel1_z)
    panel - str of panel to shift, choose from '0x', '1x', '0y', '1y'

    returns resolution (need to minimise)
    """

    start_time = time.time()
    # create starting dict of 0.0 for each condition
    param_list = ["p0_x", "p1_x", "p0_y", "p1_y", "p0_z", "p1_z"]
    param_dict = dict.fromkeys(param_list)
    for p_axis in param_dict.keys():
        param_dict[p_axis] = 0.0

    # set optimum alignment conditions
    if survey_type == "translation":
        param_dict[f"p{whichPanel}_x"] = yml_opts[f"R{whichRich}"][f"P{whichPanel}"][
            "perf_trans"
        ]["x"]
        param_dict[f"p{whichPanel}_y"] = yml_opts[f"R{whichRich}"][f"P{whichPanel}"][
            "perf_trans"
        ]["y"]
        param_dict[f"p{whichPanel}_z"] = yml_opts[f"R{whichRich}"][f"P{whichPanel}"][
            "perf_trans"
        ]["z"]
    elif survey_type == "rotation":
        param_dict[f"p{whichPanel}_x"] = yml_opts[f"R{whichRich}"][f"P{whichPanel}"][
            "perf_rot"
        ]["x"]
        param_dict[f"p{whichPanel}_y"] = yml_opts[f"R{whichRich}"][f"P{whichPanel}"][
            "perf_rot"
        ]["y"]
        param_dict[f"p{whichPanel}_z"] = yml_opts[f"R{whichRich}"][f"P{whichPanel}"][
            "perf_rot"
        ]["z"]

    for axis in ["x", "y", "z"]:
        if x[axis] is not None:  # we want to update this condition
            param_dict[f"p{whichPanel}_{axis}"] = x[axis]

    print(x)
    write_to_csv(param_dict)

    check_if_already_run_res = check_if_already_run(
        param_dict, f"panel{panel}{axis_name}"
    )
    if check_if_already_run_res:
        return

    def perform_one_fit_pipeline(hist_path=""):
        alignMonitor = AlignMonitor(
            whichRich=whichRich,
            floated=[
                param_dict[f"p{whichPanel}_x"],
                param_dict[f"p{whichPanel}_y"],
                param_dict[f"p{whichPanel}_z"],
            ],
        )
        alignMonitor.performMonitoring(panel, hist_path)
        finalCKres = alignMonitor.finalCKres
        finalCKresErr = alignMonitor.finalCKresErr
        finalCKgaussianmean = alignMonitor.finalCKgaussianmean
        check_for_reco_issues_res = check_for_reco_issues(
            finalCKres, finalCKresErr, f"panel{panel}{axis_name}"
        )
        return check_for_reco_issues_res, finalCKres, finalCKresErr, finalCKgaussianmean

    check_for_reco_issues_res = True
    while check_for_reco_issues_res:
        # Wait for the file to be released, with a timeout of 100 seconds
        try:
            wait_for_file("hlt2_reco_rich2_noUT_survey_dd4hep_survey.py", timeout=100)
            print("INFO: File is free. Proceed with running the file.")
            # Your code to run the file goes here
        except TimeoutError:
            print("INFO: Timeout waiting for the file to be released.")
            quit()

        print("INFO: Running Reconstruction...")
        reco_run_str = (
            f"{stack_path}/Panoptes/run gaudirun.py {stack_path}/"
            + f"Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/PanelAlignment/surveys/"
        )
        if verbose:
            reco_run_str += f"hlt2_reco_rich2_noUT_survey_dd4hep_survey.py | tee steeringR{whichRich}_p{whichPanel}_log.log"
        else:
            reco_run_str += f"hlt2_reco_rich2_noUT_survey_dd4hep_survey.py > steeringR{whichRich}_p{whichPanel}_log.log 2>&1"

        result = os.system(reco_run_str)

        print("INFO: Done.")
        results_dict = {}

        if columns:
            for col_num in column_nums:
                print(f"Column: {col_num}/{column_nums[-1]}")
                (
                    check_for_reco_issues_res,
                    finalCKres,
                    finalCKresErr,
                    finalCKgaussianmean,
                ) = perform_one_fit_pipeline(hist_path=f"{hist_path_prefix}{col_num}")

                results_dict[f"{col_num}"] = {
                    "finalCKres": finalCKres,
                    "finalCKresErr": finalCKresErr,
                    "finalCKgaussianmean": finalCKgaussianmean,
                }
        else:
            (
                check_for_reco_issues_res,
                finalCKres,
                finalCKresErr,
                finalCKgaussianmean,
            ) = perform_one_fit_pipeline()

            results_dict["all_cols"] = {
                "finalCKres": finalCKres,
                "finalCKresErr": finalCKresErr,
                "finalCKgaussianmean": finalCKgaussianmean,
            }

        # sys.stdout.flush()
        # os.remove(f'rich{whichRich}_opt_dd4hep_p{whichPanel}.root')
        # os.remove(f'rich{whichRich}_opt_dd4hep_p{whichPanel}_new.root')

    os.remove(f"rich{whichRich}_{survey_type}_params_p{whichPanel}.csv")
    for col_num in results_dict.keys():
        write_plotting(
            results_dict[col_num]["finalCKres"],
            param_dict,
            results_dict[col_num]["finalCKresErr"],
            results_dict[col_num]["finalCKgaussianmean"],
            f"panel{panel}{axis_name}",
            col_num,
        )

    diff_time = time.time() - start_time
    print(f"INFO: Time taken for this iteration: {round(diff_time)} seconds")
    return finalCKres


class minimizer:
    def __init__(self, p_axis, iter_num, step_size):
        self.split_p_axis = list(map(str, p_axis))
        self.p = self.split_p_axis[0]
        self.axes = self.split_p_axis[1:]
        self.step_size = step_size
        self.iter_num = iter_num
        self.offset = int(self.iter_num / 2) * self.step_size
        self.conds_update = {"x": None, "y": None, "z": None}
        self.init_conds = {"x": None, "y": None, "z": None}

        for axis in self.axes:
            self.conds_update[axis] = init_dict[f"R{whichRich}"][f"{self.p}{axis}"][
                f"{survey_type}"
            ]
            self.init_conds[axis] = init_dict[f"R{whichRich}"][f"{self.p}{axis}"][
                f"{survey_type}"
            ]

        if not twoD_scan:  # for 1D scans
            pass
        else:  # for 2D scans
            self.num_iter = {}
            self.num_iter["x"] = self.iter_num
            self.num_iter["y"] = self.iter_num

        if whichRich == 1 and whichPanel == 0:
            self.manual_points = {"x": [], "y": []}

        if whichRich == 1 and whichPanel == 1:
            self.manual_points = {"x": [], "y": []}

        if whichRich == 2 and whichPanel == 0:
            self.manual_points = {"x": [], "y": []}

        if whichRich == 2 and whichPanel == 1:
            self.manual_points = {"x": [], "y": []}

    def perform_scans(self):
        if twoD_scan is False:
            for i in range(self.iter_num):
                self.conds_update[self.axes[0]] += self.step_size
                res = monitor(self.conds_update, self.p)
        else:
            if run_manual_points:
                for i in range(0, len(self.manual_points["x"])):
                    self.conds_update[self.axes[0]] = self.manual_points["x"][i]
                    self.conds_update[self.axes[1]] = self.manual_points["y"][i]

                    res = monitor(self.conds_update, self.p)
            else:
                total_iter = (self.num_iter["x"] + 1) * (self.num_iter["y"] + 1)
                iter_counter = 0

                for i_x in range(self.num_iter["x"] + 1):
                    if i_x != 0:
                        self.conds_update[self.axes[0]] += self.step_size
                    self.conds_update[self.axes[1]] = self.init_conds[self.axes[1]]

                    for i_y in range(self.num_iter["y"] + 1):
                        if i_y != 0:
                            self.conds_update[self.axes[1]] += self.step_size

                        print(
                            f"INFO: {iter_counter}/{total_iter} ({round(iter_counter / total_iter * 100.0)}%) complete. "
                        )
                        res = monitor(self.conds_update, self.p)
                        iter_counter += 1
        return

    def check_if_reached_threshold(self):
        # look at all ckres in the full scan and check if we have reached threshold
        results_list = []
        ckres_list = []
        with open(
            f"ck_vs_param_R{whichRich}_{survey_type}_panel{whichPanel}{axis_name}.csv",
            "r",
        ) as f:
            for line in f:
                stripped_line = line.strip()
                results_list.append(stripped_line)  # store all info
                parts = stripped_line.split(",")
                ckres_list.append(float(parts[0]))

        # check if we have reached threshold
        for i in range(0, len(ckres_list)):
            if (ckres_list[i] < 0.4 and whichRich == 1) or (
                ckres_list[i] < 0.2 and whichRich == 2
            ):
                ckres_list[i] = 100

        min_ckres = min(ckres_list)
        if min_ckres < threshold:
            print(
                f"INFO: CKres: {min_ckres} is less than predefined threshold ({threshold}). Stopping iterations."
            )
            return True
        else:
            # extract conditions for the iteration with min ckres
            min_ckres_index = ckres_list.index(min_ckres)
            min_ckres_conds = results_list[min_ckres_index].split(",")[
                1
            ]  # looks like p0_x1.9:p1_x0.0:p0_y-5.8:p1_y0.0:p0_z0.0
            min_ckres_conds_splitted = min_ckres_conds.split(":")
            if whichPanel == 0:
                new_x_start = float(min_ckres_conds_splitted[0].split("x")[1])
                new_y_start = float(min_ckres_conds_splitted[2].split("y")[1])
                new_z_start = float(min_ckres_conds_splitted[4].split("z")[1])
            elif whichPanel == 1:
                new_x_start = float(min_ckres_conds_splitted[1].split("x")[1])
                new_y_start = float(min_ckres_conds_splitted[3].split("y")[1])
                new_z_start = float(min_ckres_conds_splitted[5].split("z")[1])

            # check if new minimum is the same and the previous
            if (
                (self.init_conds["x"] == new_x_start - self.offset)
                and (self.init_conds["y"] == new_y_start - self.offset)
                and (self.init_conds["z"] == new_z_start - self.offset)
            ):
                # reduce the step size
                self.step_size = 0.5 * self.step_size  # (in mm)
                self.offset = int(self.iter_num / 2) * self.step_size
                print(
                    f"INFO: Minimum hasn't moved, decreasing step size. (new step size = {self.step_size})"
                )
            else:
                print(
                    f"INFO: Setting new scan starting position at x = {new_x_start}, y = {new_y_start}, z = {new_z_start}"
                )

            # set new starting point as minimum
            self.init_conds["x"] = new_x_start - self.offset
            self.init_conds["y"] = new_y_start - self.offset
            self.init_conds["z"] = new_z_start - self.offset

            for axis in self.axes:
                self.conds_update[axis] = self.init_conds[axis]
            print(f"INFO: Rerunning scan.")

            return False

    def align_panels(self):
        self.perform_scans()

        n_scans = 1

        if stop_on_threshold:
            while not self.check_if_reached_threshold():  # have not reached threshold
                print(f"INFO: Starting scan number {n_scans}.")
                self.perform_scans()
                n_scans += 1
                print(f"INFO: {n_scans} scans have completed.")
                if max_iter == n_scans:
                    print(
                        f"INFO: Have reached maximum number of scans ({max_iter}). Stopping."
                    )
                    break


def get_chunk_list(x0):
    iter_list = [x0]
    update_x0 = x0[0]
    # obtain list of conditions to iterate over
    for i in range(iter_num):
        update_x0 += step_size
        iter_list.append([update_x0])

    iter_chunk_list = np.array_split(iter_list, num_chunks)
    this_iter_chunk_list = iter_chunk_list[chunk_number]

    return this_iter_chunk_list


import fcntl
import time


def is_file_locked(file_path):
    try:
        file_descriptor = open(file_path, "r")
        fcntl.flock(file_descriptor, fcntl.LOCK_EX | fcntl.LOCK_NB)
        fcntl.flock(file_descriptor, fcntl.LOCK_UN)
        file_descriptor.close()
        return False
    except IOError:
        return True


def wait_for_file(file_path, interval=1, timeout=None):
    start_time = time.time()

    while is_file_locked(file_path):
        if timeout and (time.time() - start_time) >= timeout:
            raise TimeoutError(f"Timeout waiting for file {file_path} to be released.")

        time.sleep(interval)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run optimisation on RICH panel alignment"
    )
    parser.add_argument(
        "--whichRich", type=int, choices=[1, 2], help="Which RICH panel to optimise"
    )
    parser.add_argument(
        "--whichPanel",
        type=str,
        choices=["both", "0", "1"],
        help="Which Panel panel to use",
    )
    parser.add_argument(
        "--survey_type",
        type=str,
        help="Which tests to run",
        choices=["translation", "rotation"],
    )
    parser.add_argument(
        "--twoD_scan", type=str, help="Perform 2D or 1D scans", default="False"
    )
    parser.add_argument(
        "--columns", default="False", type=str, help="Perform scans of columns"
    )
    parser.add_argument(
        "--axis_name",
        type=str,
        default="",
        help="Which axis to translate along or rotate about",
        choices=["x", "y", "z", ""],
    )
    parser.add_argument(
        "--verbose", default="False", type=str, help="Show output of reconstruction"
    )
    parser.add_argument(
        "--stop_on_threshold",
        default="False",
        type=str,
        help="Stop iterating when you hit the ckres threshold",
    )

    # arguments for batch submission
    parser.add_argument(
        "--num_chunks",
        type=int,
        default=0,
        help="Number of chunks to split up survey into (each chunk runs on a separate node)",
    )
    parser.add_argument("--chunk_number", type=int, help="Chunk identifier")

    args = parser.parse_args()
    whichRich = int(args.whichRich)
    whichPanel = int(args.whichPanel)
    survey_type = args.survey_type
    twoD_scan = json.loads(args.twoD_scan.lower())
    columns = json.loads(args.columns.lower())
    verbose = json.loads(args.verbose.lower())
    stop_on_threshold = json.loads(args.stop_on_threshold.lower())

    # arguments for batch submission
    axis_name = args.axis_name
    num_chunks = args.num_chunks
    chunk_number = args.chunk_number

    ### write argparse options to YAML file ###
    from ruamel.yaml import YAML

    yaml = YAML()

    # read
    with open("survey_opts.yml") as opts_yml_file:
        yml_opts = yaml.load(opts_yml_file)

    # update
    yml_opts["whichRich"] = whichRich
    yml_opts["whichPanel"] = whichPanel
    yml_opts["survey_type"] = survey_type

    # write
    with open("survey_opts.yml", "w") as opts_yml_file:
        yaml.dump(yml_opts, opts_yml_file)

    stack_path = yml_opts["stack_path"]
    prepath = f"{stack_path}/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/PanelAlignment/surveys"

    threshold = yml_opts[f"RICH{whichRich}_p{whichPanel}_CKres_threshold"]
    max_iter = yml_opts["max_iter"]

    run_manual_points = False

    if survey_type == "translation":
        if columns:
            iter_num = 8
            # step_size = 0.5  #(in mm)
            step_size = 0.25  # (in mm)
            offset = int(iter_num / 2) * step_size
        else:
            iter_num = 2
            step_size = 0.8  # (in mm)
            offset = int(iter_num / 2) * step_size

    elif survey_type == "rotation":
        iter_num = 3
        # step_size = 0.5
        step_size = 0.25
        offset = int(iter_num / 2) * step_size

    # define starting conditons of each survey
    init_dict = {
        "R1": {
            "0x": {
                "translation": yml_opts["R1"]["P0"]["perf_trans"]["x"] - offset,
                "rotation": -1.0,
            },
            "0y": {
                "translation": yml_opts["R1"]["P0"]["perf_trans"]["y"] - offset,
                "rotation": -1.0,
            },
            "0z": {
                "translation": yml_opts["R1"]["P0"]["perf_trans"]["z"],
                "rotation": -1.0,
            },
            "1x": {
                "translation": yml_opts["R1"]["P1"]["perf_trans"]["x"] - offset,
                "rotation": -1.0,
            },
            "1y": {
                "translation": yml_opts["R1"]["P1"]["perf_trans"]["y"] - offset,
                "rotation": -1.0,
            },
            "1z": {
                "translation": yml_opts["R1"]["P1"]["perf_trans"]["z"],
                "rotation": -1.0,
            },
        },
        "R2": {
            "0x": {
                "translation": yml_opts["R2"]["P0"]["perf_trans"]["x"] - offset,
                "rotation": -25.0,
            },
            "0y": {
                "translation": yml_opts["R2"]["P0"]["perf_trans"]["y"] - offset,
                "rotation": -25.0,
            },
            "0z": {
                "translation": yml_opts["R2"]["P0"]["perf_trans"]["z"],
                "rotation": -25.0,
            },
            "1x": {
                "translation": yml_opts["R2"]["P1"]["perf_trans"]["x"] - offset,
                "rotation": -25.0,
            },
            "1y": {
                "translation": yml_opts["R2"]["P1"]["perf_trans"]["y"] - offset,
                "rotation": -25.0,
            },
            "1z": {
                "translation": yml_opts["R2"]["P1"]["perf_trans"]["z"],
                "rotation": -25.0,
            },
        },
    }

    ### change these to alter starting positions of scan (centre of scan)
    # init_dict["R1"]["0x"]["translation"] = 1.1 - offset
    # init_dict["R1"]["0y"]["translation"] = -5.8 - offset
    # init_dict["R1"]["1z"]["translation"] = -1.0 - offset
    # init_dict["R1"]["1x"]["translation"] = -3.4 - offset
    # init_dict["R1"]["1y"]["translation"] = 9.35 - offset
    # init_dict["R2"]["0x"]["translation"] = 0.35 - offset
    # init_dict["R2"]["0y"]["translation"] = 1.1 - offset
    # init_dict["R2"]["1x"]["translation"] = 10.35 - offset
    # init_dict["R2"]["1y"]["translation"] = 5.55 - offset

    if columns:
        if whichRich == 1 and whichPanel == 0:
            hist_path_prefix = "RICH/RiCKResLongTight/Rich1Gas/top/cols/ckResAllCol"
            column_nums = [i for i in range(0, 11)]
        if whichRich == 1 and whichPanel == 1:
            hist_path_prefix = "RICH/RiCKResLongTight/Rich1Gas/bottom/cols/ckResAllCol"
            column_nums = [i for i in range(0, 11)]
        if whichRich == 2 and whichPanel == 0:
            hist_path_prefix = (
                "RICH/RiCKResLongTight/Rich2Gas/ASide-left/cols/ckResAllCol"
            )
            column_nums = [i for i in range(1, 13)]
        if whichRich == 2 and whichPanel == 1:
            hist_path_prefix = (
                "RICH/RiCKResLongTight/Rich2Gas/CSide-right/cols/ckResAllCol"
            )
            column_nums = [i for i in range(1, 13)]

    if num_chunks != 0:
        minimizer(f"{whichPanel}{axis}")
    else:
        if twoD_scan:
            panel_aligner = minimizer(f"{whichPanel}xy", iter_num, step_size)
        else:
            panel_aligner = minimizer(f"{whichPanel}{axis_name}", iter_num, step_size)

    panel_aligner.align_panels()

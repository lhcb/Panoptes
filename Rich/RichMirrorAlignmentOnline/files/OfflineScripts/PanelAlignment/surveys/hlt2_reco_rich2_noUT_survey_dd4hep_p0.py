###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import csv
import glob
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad
from Moore import (
    options,
    run_reconstruction,
)
from Panoptes.alignment import (
    standalone_rich_online_align_reco,
    standalone_rich_panel_align_reco,
)
from PyConf.Algorithms import (
    PrForwardTrackingVelo,
    VeloRetinaClusterTrackingSIMD,
    VPRetinaFullClusterDecoder,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_PatPV3DFuture_pvs,
    make_reco_pvs,
    make_velo_full_clusters,
    make_VeloClusterTrackingSIMD,
)
from RecoConf.rich_data_monitoring import (
    alignment_rich_monitoring_options,
    default_rich_monitoring_options,
)
from RecoConf.rich_reconstruction import default_rich_reco_options

# from Configurables import (
#      UpdateManagerSvc
# )
"""Options for running over data with FT raw bank version 6."""
from RecoConf.decoders import (
    default_ft_decoding_version,
)

whichRich = 2
whichPanel = 0
survey_type = "translation"

manual_update = True

useDD4Hep = True
useRealData = True

options.evt_max = 1000
options.n_threads = 1
# options.n_event_slots = options.n_threads * 3
options.scheduler_legacy_mode = False

if useRealData is not True:
    options.set_input_and_conds_from_testfiledb("upgrade_Sept2022_minbias_0fb_md_xdigi")
    options.input_type = "ROOT"
else:
    exec(
        open(
            "/eos/user/j/jreich/stack_dd4hep_december/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/PanelAlignment/data_255358.py"
        ).read()
    )

    options.simulation = False
    options.input_type = "MDF"

if useDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    from DDDB.CheckDD4Hep import UseDD4Hep
else:
    UseDD4Hep = False

print("Using DD4Hep: ", UseDD4Hep)

if UseDD4Hep:
    from Configurables import DDDBConf

    dd4hep = DD4hepSvc(
        DetectorList=["/world", "Magnet", "UT", "VP", "FT", "Rich1", "Rich2"]
    )
    dd4hep.OutputLevel = 1

    if useRealData:
        options.conddb_tag = "master"  # used for real data
        options.dddb_tag = "run3/trunk"
        DDDBConf().GeometryVersion = "run3/trunk"  # used for real data

        # latest scifi and velo alignments
        dd4hep.UseConditionsOverlay = True
        dd4hep.ConditionsVersion = "alignment2022"

        options.simulation = False
    else:
        options.conddb_tag = (
            "jonrob/all-pmts-active"  # used for DetDesc MC samples with DD4Hep
        )
        DDDBConf().GeometryVersion = "run3/before-rich1-geom-update-26052022"  # used for DetDesc MC samples with DD4Hep

    if manual_update:
        conddb_path = "/eos/user/j/jreich/stack_dd4hep_december/Panoptes/Rich/RichMirrorAlignmentOnline/files/OfflineScripts/PanelAlignment/lhcb-conditions-database"
        dd4hep.ConditionsLocation = "file://" + conddb_path
    else:
        dd4hep.ConditionsLocation = (
            "git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"
        )

else:
    options.conddb_tag = "upgrade/sim-20220612-vc-md100-RICHcustomFTv6"
    options.simulation = True

rich = f"rich{whichRich}"
radiator = f"Rich{whichRich}Gas"

param_list = ["p0_x", "p1_x", "p0_y", "p1_y", "p0_z", "p1_z"]
param_dict = dict.fromkeys(param_list)
trans_perf_param_dict = dict.fromkeys(param_list)

# Change file name if running elsewhere
file_name = f"rich{whichRich}_{survey_type}_params_p{whichPanel}.csv"
f = open(file_name, "r")
reader = csv.reader(f)
for row, param in zip(reader, param_dict.keys()):
    param_dict[param] = float(row[0])

f.close()

# start at optimum translation alignment
perf_align = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
for val, param in zip(perf_align, param_dict.keys()):
    trans_perf_param_dict[param] = float(val)

panel_nums = []
if whichPanel == "both":
    panel_nums.append(0)
    panel_nums.append(1)
else:
    panel_nums.append(whichPanel)

# update the conditons
if UseDD4Hep:
    # dd4hep.DumpConditions = True

    if manual_update:
        # "_" here so argparse can interpret it (gets replaced by a space later)
        pos_condition = f"position:_[{param_dict['p' + str(whichPanel) + '_x']}_*_mm,_{param_dict['p' + str(whichPanel) + '_y']}_*_mm,_{param_dict['p' + str(whichPanel) + '_z']}_*_mm]"
        rot_condition = "rotation:_[0.0_*_mrad,_0.0_*_mrad,_0.0_*_mrad]"

        import os

        os.system(
            f"python manual_cond_update.py --whichRich 1 --whichPanel {whichPanel} --conddb_path {conddb_path}/ --pos_condition {pos_condition} --rot_condition {rot_condition}  "
        )

    else:
        condval = "!alignment {position: [0.0 * mm, 0.0 * mm, 0.0 * mm] , rotation: [0.0 * mrad, 0.0 * mrad, 0.0 * mrad]}"
        dd4hep.ConditionsOverride.update(
            {
                "/world/BeforeMagnetRegion/Rich1/Rich1PmtPanel1:Panel": condval
                #         "/world/BeforeMagnetRegion/Rich1:Rich1System":
                # condval
            }
        )

else:
    cond_update_list = []
    for panel_num in panel_nums:
        pre_cond_str = f"Conditions/Alignment/Rich1/PDPanel{panel_num}_Align :=  "
        optimum_trans_cond_str = f"double_v dPosXYZ = {trans_perf_param_dict['p' + str(panel_num) + '_x']} {trans_perf_param_dict['p' + str(panel_num) + '_y']} {trans_perf_param_dict['p' + str(panel_num) + '_z']}; "

        update_cond = f"double_v {param_key} = {param_dict['p' + str(panel_num) + '_x']} {param_dict['p' + str(panel_num) + '_y']} {param_dict['p' + str(panel_num) + '_z']};"
        cond_update_list.append(pre_cond_str + optimum_trans_cond_str + update_cond)

    UpdateManagerSvc().ConditionsOverride = cond_update_list

default_moni_opts = {}
default_reco_opts = {}
align_opts = {}

# Reco opts
wider_bkg = {"PhotonSelection": "None"}
default_reco_opts.update(wider_bkg)

if whichPanel == 0:
    panel_select = {"ActivatePanel": (True, False)}
elif whichPanel == 1:
    panel_select = {"ActivatePanel": (False, True)}
else:
    panel_select = {"ActivatePanel": (True, True)}
default_reco_opts.update(panel_select)

# Monitoring opts
if whichRich == 1:
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": 30.0 * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }
if whichRich == 2:
    tighter_minp = {
        "TightTrackSelection": {
            "MinP": 60.0 * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2.0,
            "MaxGhostProb": 0.1,
        }
    }
default_moni_opts.update(tighter_minp)

wider_histo = {"CKResHistoRange": (0.025, 0.005, 0.004)}
default_moni_opts.update(wider_histo)
useUT = {"UseUT": False}
default_moni_opts.update(useUT)

align_tasks = [
    "Produce",  # fill the production set of histograms
    "Monitor",  # add various checking histograms
    #'Map',       # add counters for creation of the HLT1 pre-selection line "map"
    #'Optimize',  # add counters for optimization of the RICH2 mirror combinations subset
    #'Select',    # check filling the rest of RICH2 mirror combinations along with 8 poorest
    #'Calibrate', # check filling all RICH2 mirror combinations with elimination when filled
    #'Explore',   # explore influence of RICH1 coordinate systems on distribution shapes
]

nkevts = str(math.trunc(options.evt_max / 1000))

# save output file with histograms
if UseDD4Hep:
    options.histo_file = f"rich{whichRich}_opt_dd4hep_p{whichPanel}.root"
else:
    options.histo_file = f"rich{whichRich}_opt_p{whichPanel}.root"

PrForwardTrackingVelo.global_bind(
    MinQuality=0.0,
    DeltaQuality=0.0,
    DecisionLDA=-99.0,
    MinTotalHits=9,
    MaxChi2PerDoF=50.0,
    MaxChi2XProjection=60.0,
    MaxChi2PerDoFFinal=28.0,
    MaxChi2Stereo=16.0,
    MaxChi2StereoAdd=16.0,
)

make_VeloClusterTrackingSIMD.global_bind(algorithm=VeloRetinaClusterTrackingSIMD)
make_velo_full_clusters.global_bind(make_full_cluster=VPRetinaFullClusterDecoder)
make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)


with (
    standalone_rich_online_align_reco.bind(
        RichGas=radiator, MirrorAlignTasks=align_tasks, noUT=True
    ),
    alignment_rich_monitoring_options.bind(
        radiator=radiator, init_override_opts=align_opts
    ),
    default_ft_decoding_version.bind(value=6),
    default_rich_reco_options.bind(init_override_opts=default_reco_opts),
    default_rich_monitoring_options.bind(init_override_opts=default_moni_opts),
):
    run_reconstruction(options, standalone_rich_online_align_reco)

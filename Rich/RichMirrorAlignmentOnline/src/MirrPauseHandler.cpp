/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * MirrPauseHandler.cpp
 *
 *  Created on: Oct 23, 2014
 *      Author: beat
 */

#include "DetDesc/RunChangeIncident.h"
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Gaucho/IGauchoMonitorSvc.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IMonitorSvc.h"
#include "GaudiKernel/IUpdateable.h"
#include "GaudiKernel/SmartIF.h"
#include <stdio.h>
#include <stdlib.h>

using namespace LHCb;

class MirrPauseHandler : public GaudiAlgorithm, virtual public IIncidentListener {
public:
  MirrPauseHandler( const std::string& name, ISvcLocator* sl );
  virtual ~MirrPauseHandler() = default;
  StatusCode         initialize() override;
  StatusCode         start() override;
  StatusCode         stop() override;
  void               handle( const Incident& inc ) override;
  StatusCode         execute() override;
  void               readReference();
  IGauchoMonitorSvc* m_MonSvc;
  std::string        m_PartitionName;
  std::string        m_RefFileName;
  unsigned long      m_Reference;
  ISvcLocator*       m_sl;
  IIncidentSvc*      m_is;
  unsigned long      m_currentRun;
  bool               m_fireRunChange;
  int                m_refRunNr;
  std::string        m_itNrFile;
};

DECLARE_COMPONENT( MirrPauseHandler )

MirrPauseHandler::MirrPauseHandler( const std::string& name, ISvcLocator* sl )
    : GaudiAlgorithm( name, sl ), m_MonSvc( 0 ), m_Reference( 0 ), m_fireRunChange( false ) {
  declareProperty( "PartitionName", m_PartitionName = "LHCbA" );
  declareProperty( "ReferenceRunNr", m_refRunNr );
  declareProperty( "ItNrFile", m_itNrFile );
  m_sl = sl;
}

StatusCode MirrPauseHandler::initialize() {
  auto sc = GaudiAlgorithm::initialize();
  if ( !sc ) return sc;
  m_MonSvc = svc<IGauchoMonitorSvc>( "MonitorSvc" );
  if ( m_MonSvc == 0 ) //! sc.isSuccess())
  {
    error() << "Cannot access monitoring service of type MonitorSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  m_is = svc<IIncidentSvc>( "IncidentSvc" );
  if ( m_is == 0 ) //! sc.isSuccess())
  {
    error() << "Cannot access monitoring service of type IncidentSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  m_is->addListener( this, "UpdateConstants" );
  m_is->addListener( this, "DAQ_PAUSE" );
  m_is->addListener( this, "BeginEvent" );
  m_RefFileName = m_itNrFile;
  return StatusCode::SUCCESS;
}

void MirrPauseHandler::handle( const Incident& inc ) {
  //  if (inc.type() == "DAQ_PAUSE")
  //  {
  //    readReference();
  //    SmartIF<IUpdateableIF> aaa(m_MonSvc);
  //    if (aaa.isValid())aaa->update(m_refRunNr*10000+m_Reference);
  //  }
  if ( m_fireRunChange && "BeginEvent" == inc.type() ) {
    // throw a runchange incident to make sure it reads the xml
    m_is->fireIncident( RunChangeIncident( name(), m_currentRun ) );
    m_fireRunChange = false;
  }
}
StatusCode MirrPauseHandler::execute() {
  LHCb::RawEvent* m_rawEvt;
  if ( exist<LHCb::RawEvent>( LHCb::RawEventLocation::Default ) ) {
    m_rawEvt = get<LHCb::RawEvent>( LHCb::RawEventLocation::Default );

    LHCb::RawBank::BankType i         = LHCb::RawBank::BankType::ODIN; //(16); // 16 is Odin bank ID...
    std::string             bname     = LHCb::RawBank::typeName( i );
    std::string::size_type  odinfound = bname.find( "ODIN", 0 );

    if ( odinfound != std::string::npos ) {
      auto b = m_rawEvt->banks( i );
      if ( b.size() > 0 ) {
        for ( auto itB = b.begin(); itB != b.end(); itB++ ) {
          m_currentRun = LHCb::ODIN{ ( *itB )->range<std::uint32_t>() }.runNumber();
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}

void MirrPauseHandler::readReference() {
  FILE* f = fopen( m_RefFileName.c_str(), "r" );
  fscanf( f, "%lu", &m_Reference );
  fclose( f );
}

StatusCode MirrPauseHandler::stop() {
  readReference();
  SmartIF<IUpdateableIF> aaa( m_MonSvc );
  StatusCode             sc = StatusCode::SUCCESS;
  if ( aaa.isValid() ) { sc = aaa->update( m_refRunNr * 10000 + m_Reference ); }
  m_fireRunChange = true;
  return sc;
}

StatusCode MirrPauseHandler::start() {
  m_MonSvc->resetHistos( this );
  return StatusCode::SUCCESS;
}

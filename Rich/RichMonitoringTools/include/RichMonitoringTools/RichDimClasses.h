/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <memory>
#include <ostream>
#include <stdlib.h>
#include <string>
#include <time.h>

// DIM
#include "RTL/rtl.h"
#include "dim/dic.hxx"
#include "dim/dis.hxx"

namespace Rich {
  namespace Mon {

    /// Class to monitor the LHC state via DIM
    class LHCState : public DimInfo {
    private:
      std::string m_lhcState;       ///< The State string
      std::string m_lhcLastState;   ///< The previous LHC State
      bool        m_newState;       ///< Flag to say if the state has changed
      time_t      m_lastUpdateTime; ///< Time of last update
    private:
      /// Update handler
      void infoHandler() override {
        m_lhcLastState   = m_lhcState;
        m_lhcState       = std::string( getString() );
        m_newState       = true;
        m_lastUpdateTime = time( NULL );
      }

    public:
      /// Constructor
      LHCState()
          : DimInfo( "LHC/State", (char*)"" )
          , m_lhcState()
          , m_lhcLastState()
          , m_newState( false )
          , m_lastUpdateTime( 0 ) {}

    public:
      /// Read the current LHC State
      inline const std::string& getState() const { return m_lhcState; }
      /// Read the last LHC State
      inline const std::string& getLastState() const { return m_lhcLastState; }

    public:
      /// Read the last update time
      inline time_t lastUpdateTime() const { return m_lastUpdateTime; }
      /// Time size last update
      inline time_t timeSinceLastUpdate() const { return ( time( NULL ) - lastUpdateTime() ); }

    public:
      /// HAs the state changed ?
      bool newState() const { return m_newState; }
      /// Set the state read status
      void setRead( const bool read = true ) { m_newState = !read; }
      /// Reset
      void reset() { setRead( false ); }
    };

    /// Class to monitor the LHCb state via DIM
    class LHCbState : public DimInfo {
    public:
      /// Status enum
      enum State { Undefined = -2, NotAvailable = -1, NotInPhysics = 0, InPhysics = 1 };

    private:
      State m_lhcbState; ///< The current LHCb state
      bool  m_newState;  ///< State change flag
    private:
      /// Update handler
      void infoHandler() override {
        m_lhcbState = (State)getInt();
        m_newState  = true;
      }

    public:
      /// Constructor
      LHCbState() : DimInfo( "LHCbStatus/LHC", -1 ), m_lhcbState( Undefined ), m_newState( false ) {}

    public:
      /// Access the current LHCb state
      inline LHCbState::State getState() const { return m_lhcbState; };

    public:
      /// Do we have a new state
      bool newState() const { return m_newState; }
      /// Set the read status
      void setRead( const bool read = true ) { m_newState = !read; }
      /// reset
      void reset() { setRead( false ); }

    public:
      /// printout
      friend inline std::ostream& operator<<( std::ostream& s, const LHCbState::State& state ) {
        switch ( state ) {
        case LHCbState::Undefined:
          return s << "UNDEFINED";
        case LHCbState::NotAvailable:
          return s << "NO DIM SERVICE";
        case LHCbState::NotInPhysics:
          return s << "NOT PHYSICS";
        case LHCbState::InPhysics:
          return s << "PHYSICS";
        default:
          return s << "SHOULD NEVER SEE THIS";
        }
      }
    };

    /// Base class to provide some basic methods to check state changes etc.
    class CheckLHCState {

    public:
      /// Default constructor
      CheckLHCState() = default;

      /// Default destructor
      ~CheckLHCState() = default;

    public:
      void initialiseDIMStates() {
        // Get DIM node
        const char* dimNode = getenv( "DIM_DNS_NODE" );
        // LHC and LHCb DIM State objects
        if ( dimNode && !m_lhcbState.get() ) { m_lhcbState = std::make_unique<Rich::Mon::LHCbState>(); }
        if ( dimNode && !m_lhcState.get() ) { m_lhcState = std::make_unique<Rich::Mon::LHCState>(); }
      }

      void finaliseDIMStates() {
        // Delete the LHC and LHCb State objects
        m_lhcbState.reset( nullptr );
        m_lhcState.reset( nullptr );
      }

    public:
      void setDimStatesAsRead() {
        if ( m_lhcbState ) { m_lhcbState->setRead(); }
        if ( m_lhcState ) { m_lhcState->setRead(); }
      }

    public:
      /// Access the LHC state object
      const LHCState* lhcStateObj() const { return m_lhcState.get(); }

      /// Access the LHCb state object
      const LHCbState* lhcbStateObj() const { return m_lhcbState.get(); }

      /// Access the LHCb State via DIM
      inline LHCbState::State lhcbState() const {
        return ( m_lhcbState ? m_lhcbState->getState() : LHCbState::Undefined );
      }

      /// Access the LHC State via DIM
      inline const std::string& lhcState() const {
        static const std::string defS = "DIM Not Available";
        return ( m_lhcState ? m_lhcState->getState() : defS );
      }

      /// Access the previous LHC State via DIM
      inline const std::string& lastLhcState() const {
        static const std::string defS = "DIM Not Available";
        return ( m_lhcState ? m_lhcState->getLastState() : defS );
      }

      /// LHCb State change ?
      inline bool lhcbStateChange() const { return ( m_lhcbState ? m_lhcbState->newState() : false ); }

      /// LHC State change ?
      inline bool lhcStateChange() const { return ( m_lhcState ? m_lhcState->newState() : false ); }

      /// Any state change ?
      inline bool stateChange() const { return ( lhcStateChange() || lhcbStateChange() ); }

      /// Are we running in PHYSICS, or 30mins since physics adjust
      inline bool inPhysics() const {
        return (
            // Normal PHYSICS State
            lhcbState() == LHCbState::InPhysics || lhcState() == "PHYSICS" ||
            // Stay active 30 mins after PHYSICS ends
            ( lhcStateObj() && lastLhcState() == "PHYSICS" && lhcStateObj()->timeSinceLastUpdate() > 30 * 60 ) ||
            // Become active 5 mins into PHYS_ADJUST
            ( lhcStateObj() && lhcState() == "PHYS_ADJUST" && lhcStateObj()->timeSinceLastUpdate() > 5 * 60 ) );
      }

    private:
      /// LHCb State object
      std::unique_ptr<Rich::Mon::LHCbState> m_lhcbState;

      /// LHC State object
      std::unique_ptr<Rich::Mon::LHCState> m_lhcState;
    };

    /// get DIM utgid string
    std::string get_utgid();

  } // namespace Mon
} // namespace Rich

/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <unistd.h>
#include <utility>
#include <vector>

#include <boost/format.hpp>

#include <Minuit2/FunctionMinimum.h>
#include <Minuit2/MnContours.h>
#include <Minuit2/MnMigrad.h>
#include <Minuit2/MnMinos.h>
#include <Minuit2/MnPlot.h>
#include <Minuit2/MnPrint.h>
#include <Minuit2/MnUserParameterState.h>
#include <Minuit2/MnUserParameters.h>

#include "yaml-cpp/yaml.h"

#include "RichMirrAlignFcn.h"

using namespace std;
using boost::format;

//------------------------------------------------------------------------------
/*
void FetchRichOpts(string& richOptsFile, vector<string>& mirrCombsSubset) {
  // read richOptsFile
  cout <<"Open file with mirror combinations: "+richOptsFile << endl;
  ifstream combsFile_ifs { richOptsFile };
  string cont {""};
  string line {""};
  if (combsFile_ifs.is_open()) {
      while(getline(combsFile_ifs, line)) {
          cout <<line<< endl;
          cont += line;
      }
    //cout <<cont<< endl;
      combsFile_ifs.close(); }
  else {
      cout <<"Unable to open file with mirror combinations: "+richOptsFile << endl;
  }
  vector<string> combsSubset;
  regex comb_regex("\'(p[\\d]{2})\',[ ]*\'(s[\\d]{2})\'");
  auto  combs_begin = sregex_iterator(cont.begin(), cont.end(), comb_regex);
  auto  combs_end   = sregex_iterator();
  for ( auto& it = combs_begin; it != combs_end; it++ ) {
      smatch match = *it;
      auto comb = match[1].str()+match[2].str();
    //cout <<comb<< endl;
      combsSubset.push_back(comb);
  }
  sort( combsSubset.begin(), combsSubset.end() );
  mirrCombsSubset = combsSubset;
  cout <<"Got mirrCombsSubset vector"<< endl;
}
*/

void FetchRichOpts( string& richOptsFile, vector<string>& mirrCombsSubset, string& timeStamp ) {
  // read richOptsFile
  cout << "Opening file with RICH align options: " + richOptsFile << endl;
  YAML::Node     opts = YAML::LoadFile( richOptsFile );
  vector<string> combsSubset;
  for ( const auto& pair : opts[timeStamp]["reco_opts"]["PrebookHistos"] ) {
    string pri  = pair[0].as<string>();
    string sec  = pair[1].as<string>();
    auto   comb = pri + sec;
    combsSubset.push_back( comb );
  }
  sort( combsSubset.begin(), combsSubset.end() );
  // for ( auto& comb : mirrCombsSubset )
  //  cout << comb << endl;
  mirrCombsSubset = combsSubset;
  cout << "Got mirrCombsSubset vector" << endl;
}

//------------------------------------------------------------------------------

void MirrNamesSubsets( vector<string>& mirrCombsSubset, vector<string>& priMirrNamesSubset,
                       vector<string>& secMirrNamesSubset ) {
  for ( const auto& comb : mirrCombsSubset ) {
    auto priName = comb.substr( 0, 3 );
    if ( none_of( priMirrNamesSubset.begin(), priMirrNamesSubset.end(), [&]( string& s ) { return s == priName; } ) ) {
      priMirrNamesSubset.push_back( priName );
    }
    auto secName = comb.substr( 3, 3 );
    if ( none_of( secMirrNamesSubset.begin(), secMirrNamesSubset.end(), [&]( string& s ) { return s == secName; } ) ) {
      secMirrNamesSubset.push_back( secName );
    }
  }
  sort( priMirrNamesSubset.begin(), priMirrNamesSubset.end() );
  sort( secMirrNamesSubset.begin(), secMirrNamesSubset.end() );
}

//------------------------------------------------------------------------------

void print_yz_table( string& rich, string& mirrType, vector<string>& mirrLay, map<string, double>& pname_value,
                     int& printStyle, double& stopTolY, double& stopTolZ ) {
  // This table printer can print a table of "sigmas" w.r.t. "stop tolerances",
  // stopTolY / stopTolZ. We stop aligning after this iteration, if all
  // additional mirror compensations yielded by this iteration are less than
  // stopTolY / stopTolZ mrad.
  //
  // Therefore, to make the printout easily interpretable with this in mind, the
  // following algorithm is used.
  //
  // If a particular mirror compensation is at, say, -0.148 mrad and the stop tolerance is, say, 0.07,
  // the printout will return -2 (-0.148/0.07 truncated before decimal point) i.e. ~ 2 times the allowed tolerance).
  //
  // But if a particular mirror compensation is at, say, -0.034 mrad,
  // the printout will return 0 (-0.034/0.07 truncated before decimal point and without sign) i.e. within the allowed
  // tolerance).

  unsigned int priRowsN = 0;
  unsigned int priColsN = 0;
  unsigned int secRowsN = 0;
  unsigned int secColsN = 0;

  if ( rich == "rich1" ) {
    priRowsN = 2;
    priColsN = 2;

    secRowsN = 4;
    secColsN = 4;
  } else if ( rich == "rich2" ) {
    priRowsN = 7;
    priColsN = 8;

    secRowsN = 5;
    secColsN = 8;
  }

  unsigned int rowsN( 0 );
  unsigned int colsN( 0 );

  if ( mirrType == "p" ) {
    cout << "                          Y                                                  Z" << endl;

    rowsN = priRowsN;
    colsN = priColsN;
  } else if ( mirrType == "s" ) {
    cout << "                          Y                                                  Z" << endl;

    rowsN = secRowsN;
    colsN = secColsN;
  }

  stringstream table;

  string firstSpace;
  string secondSpace;

  string boostFormat;

  regex  patternPrefix;
  string substPrefix;

  regex  patternSuffix;
  string substSuffix;

  regex  patternMinus0;
  string subst0;

  if ( printStyle == 1 ) {
    // Detailed Anatoly-style printout with one digit after decimal point:
    // when, e.g. stop tolerance = 0.07,
    // the following substitutions are prepared so that
    // " 0.1520/0.07 =  2.171..." is printed as " 2.1",
    // "-0.1520/0.07 = -2.171..." is printed as "-2.1",
    // " 0.0680/0.07 =  0.971..." is printed as "  .9"
    // "-0.0680/0.07 = -0.971..." is printed as " -.9"
    // " 0.0069/0.07 =  0.097..." is printed as "  .0"
    // "-0.0069/0.07 = -0.097..." is printed as "  .0"
    if ( rich == "rich1" ) {
      if ( mirrType == "p" ) {
        firstSpace  = "                  ";
        secondSpace = "                                       ";
      } else if ( mirrType == "s" ) {
        firstSpace  = "            ";
        secondSpace = "                           ";
      }
    } else if ( rich == "rich2" ) {
      firstSpace  = "";
      secondSpace = "   ";
    }

    boostFormat = "%| 17.12f|";

    patternPrefix = "([ ]+[-]?)0\\.";
    substPrefix   = " $1.";

    patternSuffix = "\\.(\\d)[\\d]+";
    substSuffix   = ".$1";

    patternMinus0 = "-\\.0";
    subst0        = " .0";
  } else if ( printStyle == 0 ) {
    // New-style printout with zero digits after decimal point:
    // when, e.g. stop tolerance = 0.07,
    // the following substitutions are prepared so that
    // " 0.1520/0.07 =  2.171..." is printed as " 2",
    // "-0.1520/0.07 = -2.171..." is printed as "-2",
    // " 0.0680/0.07 =  0.971..." is printed as " 0"
    // "-0.0680/0.07 = -0.971..." is printed as " 0"
    // " 0.0069/0.07 =  0.097..." is printed as " 0"
    // "-0.0069/0.07 = -0.097..." is printed as " 0"
    if ( rich == "rich1" ) {
      if ( mirrType == "p" ) {
        firstSpace  = "                    ";
        secondSpace = "                                       ";
      } else if ( mirrType == "s" ) {
        firstSpace  = "              ";
        secondSpace = "                           ";
      }
    } else if ( rich == "rich2" ) {
      firstSpace  = "  ";
      secondSpace = "   ";
    }

    boostFormat = "%| 17.12f|";

    patternPrefix = "0";
    substPrefix   = "0";

    patternSuffix = "\\.[\\d]+";
    substSuffix   = "  ";

    patternMinus0 = "-0";
    subst0        = " 0";
  }

  string truncatedValueStg;

  vector<string>::const_iterator mirrItY = mirrLay.begin();
  vector<string>::const_iterator mirrItZ = mirrLay.begin();

  for ( unsigned int row = 0; row < rowsN; row++ ) {
    table << firstSpace;
    for ( unsigned int col = 0; col < colsN; col++ ) {
      stringstream longValueStgStm;
      double       scaledValue = pname_value[*mirrItY + "Y"] / stopTolY;
      longValueStgStm << format( boostFormat ) % scaledValue;
      truncatedValueStg = regex_replace( longValueStgStm.str(), patternPrefix, substPrefix );
      truncatedValueStg = regex_replace( truncatedValueStg, patternSuffix, substSuffix );
      truncatedValueStg = regex_replace( truncatedValueStg, patternMinus0, subst0 );
      table << truncatedValueStg;
      mirrItY++;
    }
    table << secondSpace;
    for ( unsigned int col = 0; col < colsN; col++ ) {
      stringstream longValueStgStm;
      double       scaledValue = pname_value[*mirrItZ + "Z"] / stopTolZ;
      longValueStgStm << format( boostFormat ) % scaledValue;
      truncatedValueStg = regex_replace( longValueStgStm.str(), patternPrefix, substPrefix );
      truncatedValueStg = regex_replace( truncatedValueStg, patternSuffix, substSuffix );
      truncatedValueStg = regex_replace( truncatedValueStg, patternMinus0, subst0 );
      table << truncatedValueStg;
      mirrItZ++;
    }
    table << endl;
  }
  cout << table.str() << endl;
}

//------------------------------------------------------------------------------

/*
int main( int argc, char* argv[] ) {

  if ( argc == 0 ) {
    cout << "Configuration file was not set!" << endl;
    return 1;
  }
  string confFile = (string)argv[1];
  cout << endl << "Configuration file       "+confFile << endl;
*/
int main( int argc, char* argv[] ) {

  // string   variant;
  // getline("variant.txt", variant);

  // string confFile = "MirrAlign_"+variant+".conf
  // cout << endl << "Configuration file       "+confFile << endl;

  // the configuration file may look something like follows:
  // workDir            = /afs/cern.ch/user/a/asolomin/public/rich_align/workdir/

  // variant            = 48m47c_p12p43fix_Gs1Gb1Mp9Wi4.0Cm2Sm0McUni_129kPreMC10
  // variant            = 48m47c_p12p43fix_Gs1Gb1Mp9Wi4.0Cm2Sm0McFix_129kPreMC10
  // variant            = 48m47c_p12p43fix_Gs1Gb1Mp9Wi4.0Cm2Sm0McCt0.3_129kPreMC10

  // richDetector       = 1
  // magnFactorsMode    = 2
  // solutionMethod     = 0
  // usePremisaligned   = true
  // iterationNumber    = 0
  // extract the rich number as a string, e.g. "2":

  cout << "argc is " << argc << endl;
  cout << " " << endl;

  string rich = string( argv[1] );
  cout << "this is " + rich << endl;
  string rN   = regex_replace( rich, regex( "rich([1/2])" ), "$1" );
  string RICH = "RICH" + rN;

  string initIterationInputYAML;
  // int    iterationNumber;
  int    magnFactorsMode;
  string mirrCombinFitResultsFile;
  string nextIterationInputYAML;
  double nominalResolutionSigma;
  int    printMode;
  int    regularizationMode;
  string richOptsFile;
  int    solutionMethod;
  int    stopDecisionMode;
  double stopSigmaFraction;
  int    stopToleranceMode;
  double stopTolerancePriY;
  double stopTolerancePriZ;
  double stopToleranceSecY;
  double stopToleranceSecZ;
  string thisIterationInputYAML;
  bool   usePremisaligned;
  string workDir;

  const YAML::Node& ts        = YAML::LoadFile( rich + "_session_timestamp.yml" );
  string            timeStamp = ts.as<string>();

  const YAML::Node& conf = YAML::LoadFile( rich + "_fit_align_confs.yml" );

  magnFactorsMode        = conf[timeStamp]["align_conf"]["magnFactorsMode"].as<int>();
  nominalResolutionSigma = conf[timeStamp]["fit_conf"]["nominalResolutionSigma"].as<double>();
  printMode              = conf[timeStamp]["align_conf"]["printMode"].as<int>();
  regularizationMode     = conf[timeStamp]["align_conf"]["regularizationMode"].as<int>();
  solutionMethod         = conf[timeStamp]["align_conf"]["solutionMethod"].as<int>();
  stopDecisionMode       = conf[timeStamp]["align_conf"]["stopDecisionMode"].as<int>();
  stopSigmaFraction      = conf[timeStamp]["fit_conf"]["stopSigmaFraction"].as<double>();
  stopToleranceMode      = conf[timeStamp]["align_conf"]["stopToleranceMode"].as<int>();
  stopTolerancePriY      = conf[timeStamp]["align_conf"]["stopTolerancePriY"].as<double>();
  stopTolerancePriZ      = conf[timeStamp]["align_conf"]["stopTolerancePriZ"].as<double>();
  stopToleranceSecY      = conf[timeStamp]["align_conf"]["stopToleranceSecY"].as<double>();
  stopToleranceSecZ      = conf[timeStamp]["align_conf"]["stopToleranceSecZ"].as<double>();
  usePremisaligned       = conf[timeStamp]["align_conf"]["usePremisaligned"].as<bool>();
  workDir                = conf[timeStamp]["fit_conf"]["workDir"].as<string>();

  const YAML::Node& var     = YAML::LoadFile( rich + "_variant.yml" );
  string            variant = var.as<string>();
  ;

  const YAML::Node& iter       = YAML::LoadFile( rich + "_iter_number.yml" );
  int               iterNumber = iter.as<int>();

  string iN = "_i" + to_string( iterNumber );

  richOptsFile             = rich + "_reco_opts.yml";            // file with subset of mirror combinations
  mirrCombinFitResultsFile = variant + "_fit_res" + iN + ".txt"; // ioutput: histos fit results

  float priMirrsTotN = 0.;
  float secMirrsTotN = 0.;
  // float combsTotN    = 0.;
  if ( rN == "1" ) {
    priMirrsTotN = 4.;
    secMirrsTotN = 16.;
    // combsTotN    = 16.;
  } else if ( rN == "2" ) {
    priMirrsTotN = 56.;
    secMirrsTotN = 40.;
    // combsTotN    = 94.;
  }

  vector<string> combsSubset;
  FetchRichOpts( richOptsFile, combsSubset, timeStamp );

  initIterationInputYAML = variant + "_conds_i0.yml"; // input:  initial YAML file used during zeroth iteration" )
  thisIterationInputYAML =
      variant + "_conds_i" + to_string( iterNumber ) + ".yml"; // input:  YAML file in use during current iteration" )
  nextIterationInputYAML =
      variant + "_conds_i" + to_string( iterNumber + 1 ) +
      ".yml"; // output: YAML file produced at the end of this iteration, to be used during next iteration" )

  //----------------------------------------------------------------------------
  // The following is commented out by Matt Coombes for the simplification, but
  // the correct mode of operation is when the file names contain also necessary
  // meta-information, permitting to figure out the settings and - to reproduce
  // the results if necessary. That's exactly what was eliminated here.

  // regex  mirrCombinSubset_matcher = "(\\d+m\\d+c(_[\\w\\d]+fix)?)_";
  // string mirrCombinSubset_str;
  // smatch what;
  // regex_search( variant, what, mirrCombinSubset_matcher );
  // mirrCombinSubset_str = what[1].str();
  // string  combAndMirrSubsetsFile("Rich"+rN+"combAndMirrSubsets_"+mirrCombinSubset_str+".txt");

  vector<string> priNamesSubset;
  vector<string> secNamesSubset;

  cout << "      Mirror combinations in subset:" << endl;
  for ( const auto& comb : combsSubset ) { cout << format( "%|6s|" ) % comb << endl; }

  MirrNamesSubsets( combsSubset, priNamesSubset, secNamesSubset );

  cout << "      Primary   mirrors in subset: " << endl;
  for ( const auto& mirr : priNamesSubset ) { cout << format( "%|3s|" ) % mirr << endl; }
  cout << "      Secondary mirrors in subset: " << endl;
  for ( const auto& mirr : secNamesSubset ) { cout << format( "   %|3s|" ) % mirr << endl; }

  float combsSubsetN = (float)combsSubset.size();
  // float priNamesSubsetN = (float)priNamesSubset.size();
  // float secNamesSubsetN = (float)secNamesSubset.size();

  // Prepare names of files with the magnification factors:
  vector<string> magnFactors_files;

  if ( magnFactorsMode == -1 ) {
    // Foolish "universal" magnification factors,
    // the same for all mirrors, just to prove their absurdness:
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_priYn_universal.txt" );
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_priYp_universal.txt" );
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_priZn_universal.txt" );
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_priZp_universal.txt" );
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_secYn_universal.txt" );
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_secYp_universal.txt" );
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_secZn_universal.txt" );
    magnFactors_files.push_back( "run3_" + rich + "_magnifs_secZp_universal.txt" );
  } else if ( magnFactorsMode == 0 ) {
    // Magnification factors are from MC or from previous times,
    // and not updated in any way here:
    string online_str( "online" );
    if ( variant.find( online_str ) != string::npos ) {
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_priYn_predefined.txt" );
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_priYp_predefined.txt" );
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_priZn_predefined.txt" );
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_priZp_predefined.txt" );
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_secYn_predefined.txt" );
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_secYp_predefined.txt" );
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_secZn_predefined.txt" );
      magnFactors_files.push_back( "run3_" + rich + "_magnifs_secZp_predefined.txt" );
    } else {
      magnFactors_files.push_back( variant + "_magnifs_priYn_predefined.txt" );
      magnFactors_files.push_back( variant + "_magnifs_priYp_predefined.txt" );
      magnFactors_files.push_back( variant + "_magnifs_priZn_predefined.txt" );
      magnFactors_files.push_back( variant + "_magnifs_priZp_predefined.txt" );
      magnFactors_files.push_back( variant + "_magnifs_secYn_predefined.txt" );
      magnFactors_files.push_back( variant + "_magnifs_secYp_predefined.txt" );
      magnFactors_files.push_back( variant + "_magnifs_secZn_predefined.txt" );
      magnFactors_files.push_back( variant + "_magnifs_secZp_predefined.txt" );
    }
  } else if ( magnFactorsMode == 1 ) {
    // Magnification factors are from the zeroth iteration,
    // but not updated further on:
    magnFactors_files.push_back( variant + "_magnifs_priYn_i0.txt" );
    magnFactors_files.push_back( variant + "_magnifs_priYp_i0.txt" );
    magnFactors_files.push_back( variant + "_magnifs_priZn_i0.txt" );
    magnFactors_files.push_back( variant + "_magnifs_priZp_i0.txt" );
    magnFactors_files.push_back( variant + "_magnifs_secYn_i0.txt" );
    magnFactors_files.push_back( variant + "_magnifs_secYp_i0.txt" );
    magnFactors_files.push_back( variant + "_magnifs_secZn_i0.txt" );
    magnFactors_files.push_back( variant + "_magnifs_secZp_i0.txt" );
  } else if ( magnFactorsMode == 2 ) {
    // Magnification factors are updated from iteration to iteration,
    // starting from the zeroth iteration:
    magnFactors_files.push_back( variant + "_magnifs_priYn" + iN + ".txt" );
    magnFactors_files.push_back( variant + "_magnifs_priYp" + iN + ".txt" );
    magnFactors_files.push_back( variant + "_magnifs_priZn" + iN + ".txt" );
    magnFactors_files.push_back( variant + "_magnifs_priZp" + iN + ".txt" );
    magnFactors_files.push_back( variant + "_magnifs_secYn" + iN + ".txt" );
    magnFactors_files.push_back( variant + "_magnifs_secYp" + iN + ".txt" );
    magnFactors_files.push_back( variant + "_magnifs_secZn" + iN + ".txt" );
    magnFactors_files.push_back( variant + "_magnifs_secZp" + iN + ".txt" );
  }

  map<string, double> magnFactor;

  // magnification factors:
  ifstream magnFactors_ifs;

  regex  tiltPattern{ "[prisec]{3}[YZ][np]" };
  smatch tiltMatch;
  for ( const auto& mffname : magnFactors_files ) {
    regex_search( mffname, tiltMatch, tiltPattern );
    string tiltName = tiltMatch.str();
    cout << workDir + "/" + mffname << endl;
    magnFactors_ifs.open( workDir + "/" + mffname );
    string comb;
    double magnFactorY, magnFactorZ;
    while ( magnFactors_ifs >> comb >> magnFactorY >> magnFactorZ ) {
      if ( any_of( combsSubset.begin(), combsSubset.end(), [&]( const std::string& c ) { return c == comb; } ) ) {
        if ( ( tiltName.find( "Y" ) ) != string::npos ) {
          // effect of rotations around Y on Ch-kov angle tilts in Y and Z directions:
          magnFactor[comb + tiltName + "_Y"] = magnFactorY; // e.g. p12s09priYn_Y // major effect
          magnFactor[comb + tiltName + "_z"] = magnFactorZ;
        } // e.g. p12s09priYn_z // minor effect
        else {
          // effect of rotations around Z on Ch-kov angle tilts in Z and Y directions:
          magnFactor[comb + tiltName + "_Z"] = magnFactorZ; // e.g. p12s09priZn_Z // major effect
          magnFactor[comb + tiltName + "_y"] = magnFactorY; // e.g. p12s09priZn_y // minor effect
        }
      }
    }
    magnFactors_ifs.close();
  }

  // map: [shortened combination+kind name] -> [averaged over +/- calibrational rotations magnification factor]
  map<string, double> magnFactComb;

  // Now we should average pairs of magnification factors
  // one     - with negative calibrational rotation and
  // another - with positive calibrational rotation,
  // e.g. instead of pair
  //                        p12s09priYn_Y and p12s09priYp_Y
  // we will have now
  //     p12s09priY_Y = 0.5(p12s09priYn_Y  +  p12s09priYp_Y)

  // map: [kind name (only major magnfication factors, i.e. Y_Y and Z_Z)]
  // ->   [averaged over +/- calibrational rotations and over all mirror combinations]
  map<string, double> magnFactAver;

  // Prepare variables for the total rotations of mirror combinations detected
  // after fitting of the histograms and the corresponding magnification
  // factors:
  for ( const auto& comb : combsSubset ) {

    magnFactComb[comb + "priY_Y"] = 0.5 * ( magnFactor.at( comb + "priYn_Y" ) + magnFactor.at( comb + "priYp_Y" ) );
    magnFactComb[comb + "priY_z"] = 0.5 * ( magnFactor.at( comb + "priYn_z" ) + magnFactor.at( comb + "priYp_z" ) );
    magnFactComb[comb + "priZ_Z"] = 0.5 * ( magnFactor.at( comb + "priZn_Z" ) + magnFactor.at( comb + "priZp_Z" ) );
    magnFactComb[comb + "priZ_y"] = 0.5 * ( magnFactor.at( comb + "priZn_y" ) + magnFactor.at( comb + "priZp_y" ) );
    magnFactComb[comb + "secY_Y"] = 0.5 * ( magnFactor.at( comb + "secYn_Y" ) + magnFactor.at( comb + "secYp_Y" ) );
    magnFactComb[comb + "secY_z"] = 0.5 * ( magnFactor.at( comb + "secYn_z" ) + magnFactor.at( comb + "secYp_z" ) );
    magnFactComb[comb + "secZ_Z"] = 0.5 * ( magnFactor.at( comb + "secZn_Z" ) + magnFactor.at( comb + "secZp_Z" ) );
    magnFactComb[comb + "secZ_y"] = 0.5 * ( magnFactor.at( comb + "secZn_y" ) + magnFactor.at( comb + "secZp_y" ) );

    magnFactAver["priY_Y"] += magnFactComb.at( comb + "priY_Y" );
    magnFactAver["priY_z"] += magnFactComb.at( comb + "priY_z" );
    magnFactAver["priZ_Z"] += magnFactComb.at( comb + "priZ_Z" );
    magnFactAver["priZ_y"] += magnFactComb.at( comb + "priZ_y" );
    magnFactAver["secY_Y"] += magnFactComb.at( comb + "secY_Y" );
    magnFactAver["secY_z"] += magnFactComb.at( comb + "secY_z" );
    magnFactAver["secZ_Z"] += magnFactComb.at( comb + "secZ_Z" );
    magnFactAver["secZ_y"] += magnFactComb.at( comb + "secZ_y" );
  }

  // and now we divide them by the number of combinations in use to finalize
  // the averaging; they are approximately:     RICH1  RICH2
  magnFactAver.at( "priY_Y" ) /= combsSubsetN; //  1.85   2.08
  magnFactAver.at( "priY_z" ) /= combsSubsetN; //  0.00   0.00
  magnFactAver.at( "priZ_Z" ) /= combsSubsetN; //  2.04   1.87
  magnFactAver.at( "priZ_y" ) /= combsSubsetN; //  0.00  -0.00

  magnFactAver.at( "secY_Y" ) /= combsSubsetN; //  1.85   2.08
  magnFactAver.at( "secY_z" ) /= combsSubsetN; //  0.00   0.00
  magnFactAver.at( "secZ_Z" ) /= combsSubsetN; //  2.04   1.87
  magnFactAver.at( "secZ_y" ) /= combsSubsetN; //  0.00  -0.00

  // map: [kind name (all magnfication factors, i.e. Y_Y Z_Z and Y_z Z_y))]
  // ->   [averaged over +/- calibrational rotations and individual for
  // each primary/secondary mirror but averaged over its secondary/primary
  // partners in all its combinations in use]
  map<string, double> magnFactMirr;

  //----------------------------------------------------------------------------

  // Prepare ordered combinations subset lists:
  vector<string> combsSubsetOrdPri( combsSubset ); // already sorted according to primary number
  vector<string> combsSubsetOrdSec( combsSubset ); // will be sorted according to secondary number

  // Before sorting combsSubsetOrdSec first swap order from prisec to secpri:
  for ( auto& comb : combsSubsetOrdSec ) {
    auto priname = comb.substr( 0, 3 );
    auto secname = comb.substr( 3, 3 );
    comb         = secname + priname;
  }
  // now sort according to secondary number
  sort( combsSubsetOrdSec.begin(), combsSubsetOrdSec.end() );
  // And now swap them back;
  for ( auto& comb : combsSubsetOrdSec ) {
    auto priname = comb.substr( 3, 3 );
    auto secname = comb.substr( 0, 3 );
    comb         = priname + secname;
  }

  // Prepare vectors with particular kinds of magnification factors to be sorted
  // according to their values:
  vector<pair<string, double>> magnFactCombOrdPriY_Y;
  vector<pair<string, double>> magnFactCombOrdPriY_z;
  vector<pair<string, double>> magnFactCombOrdPriZ_Z;
  vector<pair<string, double>> magnFactCombOrdPriZ_y;
  vector<pair<string, double>> magnFactCombOrdSecY_Y;
  vector<pair<string, double>> magnFactCombOrdSecY_z;
  vector<pair<string, double>> magnFactCombOrdSecZ_Z;
  vector<pair<string, double>> magnFactCombOrdSecZ_y;

  // Prepare vectors of pairs for idividual per-mirror particular kinds of
  // magnification factors to be sorted according to their values:
  vector<pair<string, double>> magnFactMirrOrdPriY_Y;
  vector<pair<string, double>> magnFactMirrOrdPriY_z;
  vector<pair<string, double>> magnFactMirrOrdPriZ_Z;
  vector<pair<string, double>> magnFactMirrOrdPriZ_y;
  vector<pair<string, double>> magnFactMirrOrdSecY_Y;
  vector<pair<string, double>> magnFactMirrOrdSecY_z;
  vector<pair<string, double>> magnFactMirrOrdSecZ_Z;
  vector<pair<string, double>> magnFactMirrOrdSecZ_y;

  cout << "==================================================================" << endl;
  cout << "Analysis of the magnification factors (MFs) for" << endl;
  cout << RICH + " primary mirrors" << endl;
  cout << "==================================================================" << endl;
  cout << "MFs are denoted in the following way:" << endl;
  cout << "e.g.     priY_Y                  is MF for primary mirror" << endl;
  cout << "rotation around Y producing tilt in Y direction" << endl;
  cout << "while                     priY_z is MF for primary mirror" << endl;
  cout << "rotation around Y producing tilt in Z direction" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "MFs for each combination are:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << " comb    priY_Y           priY_z           priZ_Z           priZ_y" << endl;
  for ( auto& comb : combsSubsetOrdPri ) {
    cout << format( "%|6s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % comb %
                magnFactComb.at( comb + "priY_Y" ) % magnFactComb.at( comb + "priY_z" ) %
                magnFactComb.at( comb + "priZ_Z" ) % magnFactComb.at( comb + "priZ_y" )
         << endl;
    magnFactCombOrdPriY_Y.push_back( pair<string, double>( comb, magnFactComb.at( comb + "priY_Y" ) ) );
    magnFactCombOrdPriY_z.push_back( pair<string, double>( comb, magnFactComb.at( comb + "priY_z" ) ) );
    magnFactCombOrdPriZ_Z.push_back( pair<string, double>( comb, magnFactComb.at( comb + "priZ_Z" ) ) );
    magnFactCombOrdPriZ_y.push_back( pair<string, double>( comb, magnFactComb.at( comb + "priZ_y" ) ) );
  }

  // Sort the vectors with particular magnification factors according to their values:
  sort( magnFactCombOrdPriY_Y.begin(), magnFactCombOrdPriY_Y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactCombOrdPriY_z.begin(), magnFactCombOrdPriY_z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactCombOrdPriZ_Z.begin(), magnFactCombOrdPriZ_Z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactCombOrdPriZ_y.begin(), magnFactCombOrdPriZ_y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  // Iterators:
  auto priY_Y_It = magnFactCombOrdPriY_Y.begin();
  auto priY_z_It = magnFactCombOrdPriY_z.begin();
  auto priZ_Z_It = magnFactCombOrdPriZ_Z.begin();
  auto priZ_y_It = magnFactCombOrdPriZ_y.begin();
  // These will be printed at the bottom:
  auto priY_Y_Mn = priY_Y_It->second;
  auto priY_z_Mn = priY_z_It->second;
  auto priZ_Z_Mn = priZ_Z_It->second;
  auto priZ_y_Mn = priZ_y_It->second;
  // And now print them side-by-side:
  cout << "------------------------------------------------------------------" << endl;
  cout << "For " + RICH + ", each MF-per-mirror-combination column is now ordered," << endl;
  cout << "and" << endl;
  cout << "on the left its resulting combinations column is given:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << " comb    priY_Y   comb    priY_z   comb    priZ_Z   comb    priZ_y" << endl;
  while ( priY_Y_It != magnFactCombOrdPriY_Y.end() ) {
    cout << format( "%|6s|%| 8.2f|   %|6s|%| 8.2f|   %|6s|%| 8.2f|   %|6s|%| 8.2f|" ) % priY_Y_It->first %
                priY_Y_It->second % priY_z_It->first % priY_z_It->second % priZ_Z_It->first % priZ_Z_It->second %
                priZ_y_It->first % priZ_y_It->second
         << endl;
    priY_Y_It++;
    priY_z_It++;
    priZ_Z_It++;
    priZ_y_It++;
  }
  priY_Y_It--;
  priY_z_It--;
  priZ_Z_It--;
  priZ_y_It--;
  auto priY_Y_Mx = priY_Y_It->second;
  auto priY_z_Mx = priY_z_It->second;
  auto priZ_Z_Mx = priZ_Z_It->second;
  auto priZ_y_Mx = priZ_y_It->second;

  auto priY_Y_Av = magnFactAver.at( "priY_Y" );
  auto priY_z_Av = magnFactAver.at( "priY_z" );
  auto priZ_Z_Av = magnFactAver.at( "priZ_Z" );
  auto priZ_y_Av = magnFactAver.at( "priZ_y" );

  auto priY_Y_Dl = priY_Y_Mx - priY_Y_Mn;
  auto priY_z_Dl = priY_z_Mx - priY_z_Mn;
  auto priZ_Z_Dl = priZ_Z_Mx - priZ_Z_Mn;
  auto priZ_y_Dl = priZ_y_Mx - priZ_y_Mn;
  cout << "------------------------------------------------------------------" << endl;

  cout << "Summary over all " + RICH + " combinations in use:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "         priY_Y           priY_z           priZ_Z           priZ_y" << endl;
  cout << format( "min    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Mn % priY_z_Mn %
              priZ_Z_Mn % priZ_y_Mn
       << endl;
  cout << format( "average%| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Av % priY_z_Av %
              priZ_Z_Av % priZ_y_Av
       << endl;
  cout << format( "max    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Mx % priY_z_Mx %
              priZ_Z_Mx % priZ_y_Mx
       << endl;
  cout << format( "delta  %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Dl % priY_z_Dl %
              priZ_Z_Dl % priZ_y_Dl
       << endl;
  // Here comes formation of map of magnification factors, individual for
  // each primary mirror but averaged over its secondary partners in all its
  // combinations in use.

  // Initialize the working things:
  string combPriLast = combsSubsetOrdPri.back();
  string priNamePrev = priNamesSubset.at( 0 ).substr( 0, 3 );

  int    secPartnersCount = 0;
  double mfmPriY_Y        = 0.;
  double mfmPriY_z        = 0.;
  double mfmPriZ_Z        = 0.;
  double mfmPriZ_y        = 0.;
  cout << "------------------------------------------------------------------" << endl;
  cout << "Now for each " + RICH + " primary mirror, let's calculate its" << endl;
  cout << "individual MF by averaging its MFs over all its secondary" << endl;
  cout << "partners in all combinations in use, it participates in:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "pri      priY_Y           priY_z           priZ_Z           priZ_y" << endl;
  for ( auto& comb : combsSubsetOrdPri ) {
    string priName = comb.substr( 0, 3 );
    string secName = comb.substr( 3, 3 );
    if ( priName == priNamePrev ) {
      // The same priName as before - just do ahead and coninue accumulating
      // things:
      secPartnersCount++;
      mfmPriY_Y += magnFactComb.at( comb + "priY_Y" );
      mfmPriY_z += magnFactComb.at( comb + "priY_z" );
      mfmPriZ_Z += magnFactComb.at( comb + "priZ_Z" );
      mfmPriZ_y += magnFactComb.at( comb + "priZ_y" );

      cout << format( "%|6s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % comb %
                  magnFactComb.at( comb + "priY_Y" ) % magnFactComb.at( comb + "priY_z" ) %
                  magnFactComb.at( comb + "priZ_Z" ) % magnFactComb.at( comb + "priZ_y" )
           << endl;
      // In case it is the last element, we must finalize the last magnFactMirr
      // entry:
      if ( comb == combPriLast ) {
        mfmPriY_Y /= secPartnersCount;
        mfmPriY_z /= secPartnersCount;
        mfmPriZ_Z /= secPartnersCount;
        mfmPriZ_y /= secPartnersCount;
        magnFactMirr[priName + "Y_Y"] = mfmPriY_Y;
        magnFactMirr[priName + "Y_z"] = mfmPriY_z;
        magnFactMirr[priName + "Z_Z"] = mfmPriZ_Z;
        magnFactMirr[priName + "Z_y"] = mfmPriZ_y;
        magnFactMirrOrdPriY_Y.push_back( pair<string, double>( priName, mfmPriY_Y ) );
        magnFactMirrOrdPriY_z.push_back( pair<string, double>( priName, mfmPriY_z ) );
        magnFactMirrOrdPriZ_Z.push_back( pair<string, double>( priName, mfmPriZ_Z ) );
        magnFactMirrOrdPriZ_y.push_back( pair<string, double>( priName, mfmPriZ_y ) );
        cout << format( "%|3s|   %| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % priName % mfmPriY_Y %
                    mfmPriY_z % mfmPriZ_Z % mfmPriZ_y
             << endl;
      }
    } else {
      // New priName
      // First finalize magnFactMirr entries for the previous priName:
      mfmPriY_Y /= secPartnersCount;
      mfmPriY_z /= secPartnersCount;
      mfmPriZ_Z /= secPartnersCount;
      mfmPriZ_y /= secPartnersCount;
      magnFactMirr[priNamePrev + "Y_Y"] = mfmPriY_Y;
      magnFactMirr[priNamePrev + "Y_z"] = mfmPriY_z;
      magnFactMirr[priNamePrev + "Z_Z"] = mfmPriZ_Z;
      magnFactMirr[priNamePrev + "Z_y"] = mfmPriZ_y;
      magnFactMirrOrdPriY_Y.push_back( pair<string, double>( priNamePrev, mfmPriY_Y ) );
      magnFactMirrOrdPriY_z.push_back( pair<string, double>( priNamePrev, mfmPriY_z ) );
      magnFactMirrOrdPriZ_Z.push_back( pair<string, double>( priNamePrev, mfmPriZ_Z ) );
      magnFactMirrOrdPriZ_y.push_back( pair<string, double>( priNamePrev, mfmPriZ_y ) );
      cout << format( "%|3s|   %| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % priNamePrev %
                  mfmPriY_Y % mfmPriY_z % mfmPriZ_Z % mfmPriZ_y
           << endl;
      // And then reset the things:
      secPartnersCount = 1;
      mfmPriY_Y        = magnFactComb.at( comb + "priY_Y" );
      mfmPriY_z        = magnFactComb.at( comb + "priY_z" );
      mfmPriZ_Z        = magnFactComb.at( comb + "priZ_Z" );
      mfmPriZ_y        = magnFactComb.at( comb + "priZ_y" );
      cout << format( "%|6s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % comb %
                  magnFactComb.at( comb + "priY_Y" ) % magnFactComb.at( comb + "priY_z" ) %
                  magnFactComb.at( comb + "priZ_Z" ) % magnFactComb.at( comb + "priZ_y" )
           << endl;
      // In case it is the last element, we must finalize the last magnFactMirr
      // entry:
      if ( comb == combPriLast ) {
        mfmPriY_Y /= secPartnersCount;
        mfmPriY_z /= secPartnersCount;
        mfmPriZ_Z /= secPartnersCount;
        mfmPriZ_y /= secPartnersCount;
        magnFactMirr[priName + "Y_Y"] = mfmPriY_Y;
        magnFactMirr[priName + "Y_z"] = mfmPriY_z;
        magnFactMirr[priName + "Z_Z"] = mfmPriZ_Z;
        magnFactMirr[priName + "Z_y"] = mfmPriZ_y;
        magnFactMirrOrdPriY_Y.push_back( pair<string, double>( priName, mfmPriY_Y ) );
        magnFactMirrOrdPriY_z.push_back( pair<string, double>( priName, mfmPriY_z ) );
        magnFactMirrOrdPriZ_Z.push_back( pair<string, double>( priName, mfmPriZ_Z ) );
        magnFactMirrOrdPriZ_y.push_back( pair<string, double>( priName, mfmPriZ_y ) );
        cout << format( "%|3s|   %| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % priName % mfmPriY_Y %
                    mfmPriY_z % mfmPriZ_Z % mfmPriZ_y
             << endl;
      } else { // not last
        priNamePrev = priName;
      }
    }
  }
  // Sort four vectors of pairs for idividual per-mirror magnification factors:
  sort( magnFactMirrOrdPriY_Y.begin(), magnFactMirrOrdPriY_Y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactMirrOrdPriY_z.begin(), magnFactMirrOrdPriY_z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactMirrOrdPriZ_Z.begin(), magnFactMirrOrdPriZ_Z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactMirrOrdPriZ_y.begin(), magnFactMirrOrdPriZ_y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  // Calculate the average values per kind-of-factor:
  // Iterators:
  priY_Y_It = magnFactMirrOrdPriY_Y.begin();
  priY_z_It = magnFactMirrOrdPriY_z.begin();
  priZ_Z_It = magnFactMirrOrdPriZ_Z.begin();
  priZ_y_It = magnFactMirrOrdPriZ_y.begin();
  // These will be printed at the bottom:
  priY_Y_Mn = priY_Y_It->second;
  priY_z_Mn = priY_z_It->second;
  priZ_Z_Mn = priZ_Z_It->second;
  priZ_y_Mn = priZ_y_It->second;
  // Averages:
  priY_Y_Av = 0.;
  priY_z_Av = 0.;
  priZ_Z_Av = 0.;
  priZ_y_Av = 0.;
  // And now print the result:
  cout << "------------------------------------------------------------------" << endl;
  cout << "For " + RICH + ", each MF-per-primary-mirror column is now ordered," << endl;
  cout << "and" << endl;
  cout << "on the left its resulting primary-mirror-numbers column is given:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "pri      priY_Y  pri      priY_z  pri      priZ_Z  pri      priZ_y" << endl;
  while ( priY_Y_It != magnFactMirrOrdPriY_Y.end() ) {
    cout << format( "%|3s|   %| 8.2f|%|6s|   %| 8.2f|%|6s|   %| 8.2f|%|6s|   %| 8.2f|" ) % priY_Y_It->first %
                priY_Y_It->second % priY_z_It->first % priY_z_It->second % priZ_Z_It->first % priZ_Z_It->second %
                priZ_y_It->first % priZ_y_It->second
         << endl;
    priY_Y_Av += priY_Y_It->second;
    priY_z_Av += priY_z_It->second;
    priZ_Z_Av += priZ_Z_It->second;
    priZ_y_Av += priZ_y_It->second;
    priY_Y_It++;
    priY_z_It++;
    priZ_Z_It++;
    priZ_y_It++;
  }
  priY_Y_It--;
  priY_z_It--;
  priZ_Z_It--;
  priZ_y_It--;
  priY_Y_Mx = priY_Y_It->second;
  priY_z_Mx = priY_z_It->second;
  priZ_Z_Mx = priZ_Z_It->second;
  priZ_y_Mx = priZ_y_It->second;

  priY_Y_Av /= priMirrsTotN;
  priY_z_Av /= priMirrsTotN;
  priZ_Z_Av /= priMirrsTotN;
  priZ_y_Av /= priMirrsTotN;

  priY_Y_Dl = priY_Y_Mx - priY_Y_Mn;
  priY_z_Dl = priY_z_Mx - priY_z_Mn;
  priZ_Z_Dl = priZ_Z_Mx - priZ_Z_Mn;
  priZ_y_Dl = priZ_y_Mx - priZ_y_Mn;
  cout << "------------------------------------------------------------------" << endl;
  cout << "Finally, summary over all " + RICH + " primary mirrors:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "         priY_Y           priY_z           priZ_Z           priZ_y" << endl;
  cout << format( "min    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Mn % priY_z_Mn %
              priZ_Z_Mn % priZ_y_Mn
       << endl;
  cout << format( "average%| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Av % priY_z_Av %
              priZ_Z_Av % priZ_y_Av
       << endl;
  cout << format( "max    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Mx % priY_z_Mx %
              priZ_Z_Mx % priZ_y_Mx
       << endl;
  cout << format( "delta  %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % priY_Y_Dl % priY_z_Dl %
              priZ_Z_Dl % priZ_y_Dl
       << endl;

  cout << "==================================================================" << endl;
  cout << "Analysis of the magnification factors (MFs) for" << endl;
  cout << RICH + " secondary mirrors" << endl;
  cout << "==================================================================" << endl;
  cout << "MFs are denoted in the following way:" << endl;
  cout << "e.g.     secY_Y                  is MF for secondary mirror" << endl;
  cout << "rotation around Y producing tilt in Y direction" << endl;
  cout << "while                     secY_z is MF for secondary mirror" << endl;
  cout << "rotation around Y producing tilt in Z direction" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "MFs for each combination are:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << " comb    secY_Y           secY_z           secZ_Z           secZ_y" << endl;
  for ( auto& comb : combsSubsetOrdSec ) {
    cout << format( "%|6s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % comb %
                magnFactComb.at( comb + "secY_Y" ) % magnFactComb.at( comb + "secY_z" ) %
                magnFactComb.at( comb + "secZ_Z" ) % magnFactComb.at( comb + "secZ_y" )
         << endl;
    magnFactCombOrdSecY_Y.push_back( pair<string, double>( comb, magnFactComb.at( comb + "secY_Y" ) ) );
    magnFactCombOrdSecY_z.push_back( pair<string, double>( comb, magnFactComb.at( comb + "secY_z" ) ) );
    magnFactCombOrdSecZ_Z.push_back( pair<string, double>( comb, magnFactComb.at( comb + "secZ_Z" ) ) );
    magnFactCombOrdSecZ_y.push_back( pair<string, double>( comb, magnFactComb.at( comb + "secZ_y" ) ) );
  }

  // Sort the vectors with particular magnification factors according to their values:
  sort( magnFactCombOrdSecY_Y.begin(), magnFactCombOrdSecY_Y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactCombOrdSecY_z.begin(), magnFactCombOrdSecY_z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactCombOrdSecZ_Z.begin(), magnFactCombOrdSecZ_Z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactCombOrdSecZ_y.begin(), magnFactCombOrdSecZ_y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  // Iterators:
  auto secY_Y_It = magnFactCombOrdSecY_Y.begin();
  auto secY_z_It = magnFactCombOrdSecY_z.begin();
  auto secZ_Z_It = magnFactCombOrdSecZ_Z.begin();
  auto secZ_y_It = magnFactCombOrdSecZ_y.begin();
  // These will be printed at the bottom:
  auto secY_Y_Mn = secY_Y_It->second;
  auto secY_z_Mn = secY_z_It->second;
  auto secZ_Z_Mn = secZ_Z_It->second;
  auto secZ_y_Mn = secZ_y_It->second;
  // And now print them side-by-side:
  cout << "------------------------------------------------------------------" << endl;
  cout << "For " + RICH + ", each MF-per-mirror-combination column is now ordered," << endl;
  cout << "and" << endl;
  cout << "on the left its resulting combinations column is given:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << " comb    secY_Y   comb    secY_z   comb    secZ_Z   comb    secZ_y" << endl;
  while ( secY_Y_It != magnFactCombOrdSecY_Y.end() ) {
    cout << format( "%|6s|%| 8.2f|   %|6s|%| 8.2f|   %|6s|%| 8.2f|   %|6s|%| 8.2f|" ) % secY_Y_It->first %
                secY_Y_It->second % secY_z_It->first % secY_z_It->second % secZ_Z_It->first % secZ_Z_It->second %
                secZ_y_It->first % secZ_y_It->second
         << endl;
    secY_Y_It++;
    secY_z_It++;
    secZ_Z_It++;
    secZ_y_It++;
  }
  secY_Y_It--;
  secY_z_It--;
  secZ_Z_It--;
  secZ_y_It--;
  auto secY_Y_Mx = secY_Y_It->second;
  auto secY_z_Mx = secY_z_It->second;
  auto secZ_Z_Mx = secZ_Z_It->second;
  auto secZ_y_Mx = secZ_y_It->second;

  auto secY_Y_Av = magnFactAver.at( "secY_Y" );
  auto secY_z_Av = magnFactAver.at( "secY_z" );
  auto secZ_Z_Av = magnFactAver.at( "secZ_Z" );
  auto secZ_y_Av = magnFactAver.at( "secZ_y" );

  auto secY_Y_Dl = secY_Y_Mx - secY_Y_Mn;
  auto secY_z_Dl = secY_z_Mx - secY_z_Mn;
  auto secZ_Z_Dl = secZ_Z_Mx - secZ_Z_Mn;
  auto secZ_y_Dl = secZ_y_Mx - secZ_y_Mn;

  cout << "------------------------------------------------------------------" << endl;
  cout << "Summary over all " + RICH + " combinations in use:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "         secY_Y           secY_z           secZ_Z           secZ_y" << endl;
  cout << format( "min    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Mn % secY_z_Mn %
              secZ_Z_Mn % secZ_y_Mn
       << endl;
  cout << format( "average%| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Av % secY_z_Av %
              secZ_Z_Av % secZ_y_Av
       << endl;
  cout << format( "max    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Mx % secY_z_Mx %
              secZ_Z_Mx % secZ_y_Mx
       << endl;
  cout << format( "delta  %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Dl % secY_z_Dl %
              secZ_Z_Dl % secZ_y_Dl
       << endl;
  // Here comes formation of map of magnification factors, individual for
  // each secondary mirror but averaged over its primary partners in all its
  // combinations in use.

  // Initialize the working things:
  string combSecLast = combsSubsetOrdSec.back();
  string secNamePrev = secNamesSubset.at( 0 );

  int    priPartnersCount = 0;
  double mfmSecY_Y        = 0.;
  double mfmSecY_z        = 0.;
  double mfmSecZ_Z        = 0.;
  double mfmSecZ_y        = 0.;
  cout << "------------------------------------------------------------------" << endl;
  cout << "Now for each " + RICH + " secondary mirror, let's calculate its" << endl;
  cout << "individual MF by averaging its MFs over all its primary" << endl;
  cout << "partners in all combinations in use, it participates in:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "   sec   secY_Y           secY_z           secZ_Z           secZ_y" << endl;
  for ( auto& comb : combsSubsetOrdSec ) {
    string priName = comb.substr( 0, 3 );
    string secName = comb.substr( 3, 3 );
    if ( secName == secNamePrev ) {
      // The same secName as before - just do ahead and coninue accumulating
      // things:
      priPartnersCount++;
      mfmSecY_Y += magnFactComb.at( comb + "secY_Y" );
      mfmSecY_z += magnFactComb.at( comb + "secY_z" );
      mfmSecZ_Z += magnFactComb.at( comb + "secZ_Z" );
      mfmSecZ_y += magnFactComb.at( comb + "secZ_y" );

      cout << format( "%|6s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % comb %
                  magnFactComb.at( comb + "secY_Y" ) % magnFactComb.at( comb + "secY_z" ) %
                  magnFactComb.at( comb + "secZ_Z" ) % magnFactComb.at( comb + "secZ_y" )
           << endl;
      // In case it is the last element, we must finalize the last magnFactMirr
      // entry:
      if ( comb == combSecLast ) {
        mfmSecY_Y /= priPartnersCount;
        mfmSecY_z /= priPartnersCount;
        mfmSecZ_Z /= priPartnersCount;
        mfmSecZ_y /= priPartnersCount;
        magnFactMirr[secName + "Y_Y"] = mfmSecY_Y;
        magnFactMirr[secName + "Y_z"] = mfmSecY_z;
        magnFactMirr[secName + "Z_Z"] = mfmSecZ_Z;
        magnFactMirr[secName + "Z_y"] = mfmSecZ_y;
        magnFactMirrOrdSecY_Y.push_back( pair<string, double>( secName, mfmSecY_Y ) );
        magnFactMirrOrdSecY_z.push_back( pair<string, double>( secName, mfmSecY_z ) );
        magnFactMirrOrdSecZ_Z.push_back( pair<string, double>( secName, mfmSecZ_Z ) );
        magnFactMirrOrdSecZ_y.push_back( pair<string, double>( secName, mfmSecZ_y ) );
        cout << format( "   %|3s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % secName % mfmSecY_Y %
                    mfmSecY_z % mfmSecZ_Z % mfmSecZ_y
             << endl;
      }
    } else {
      // New secName
      // First finalize magnFactMirr entries for the previous secName:
      mfmSecY_Y /= priPartnersCount;
      mfmSecY_z /= priPartnersCount;
      mfmSecZ_Z /= priPartnersCount;
      mfmSecZ_y /= priPartnersCount;
      magnFactMirr[secNamePrev + "Y_Y"] = mfmSecY_Y;
      magnFactMirr[secNamePrev + "Y_z"] = mfmSecY_z;
      magnFactMirr[secNamePrev + "Z_Z"] = mfmSecZ_Z;
      magnFactMirr[secNamePrev + "Z_y"] = mfmSecZ_y;
      magnFactMirrOrdSecY_Y.push_back( pair<string, double>( secNamePrev, mfmSecY_Y ) );
      magnFactMirrOrdSecY_z.push_back( pair<string, double>( secNamePrev, mfmSecY_z ) );
      magnFactMirrOrdSecZ_Z.push_back( pair<string, double>( secNamePrev, mfmSecZ_Z ) );
      magnFactMirrOrdSecZ_y.push_back( pair<string, double>( secNamePrev, mfmSecZ_y ) );
      cout << format( "   %|3s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % secNamePrev %
                  mfmSecY_Y % mfmSecY_z % mfmSecZ_Z % mfmSecZ_y
           << endl;
      // And then reset the things:
      priPartnersCount = 1;
      mfmSecY_Y        = magnFactComb.at( comb + "secY_Y" );
      mfmSecY_z        = magnFactComb.at( comb + "secY_z" );
      mfmSecZ_Z        = magnFactComb.at( comb + "secZ_Z" );
      mfmSecZ_y        = magnFactComb.at( comb + "secZ_y" );
      cout << format( "%|6s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % comb %
                  magnFactComb.at( comb + "secY_Y" ) % magnFactComb.at( comb + "secY_z" ) %
                  magnFactComb.at( comb + "secZ_Z" ) % magnFactComb.at( comb + "secZ_y" )
           << endl;
      // In case it is the last element, we must finalize the last magnFactMirr
      // entry:
      if ( comb == combSecLast ) {
        mfmSecY_Y /= priPartnersCount;
        mfmSecY_z /= priPartnersCount;
        mfmSecZ_Z /= priPartnersCount;
        mfmSecZ_y /= priPartnersCount;
        magnFactMirr[secName + "Y_Y"] = mfmSecY_Y;
        magnFactMirr[secName + "Y_z"] = mfmSecY_z;
        magnFactMirr[secName + "Z_Z"] = mfmSecZ_Z;
        magnFactMirr[secName + "Z_y"] = mfmSecZ_y;
        magnFactMirrOrdSecY_Y.push_back( pair<string, double>( secName, mfmSecY_Y ) );
        magnFactMirrOrdSecY_z.push_back( pair<string, double>( secName, mfmSecY_z ) );
        magnFactMirrOrdSecZ_Z.push_back( pair<string, double>( secName, mfmSecZ_Z ) );
        magnFactMirrOrdSecZ_y.push_back( pair<string, double>( secName, mfmSecZ_y ) );
        cout << format( "   %|3s|%| 8.2f|         %| 8.2f|         %| 8.2f|         %| 8.2f|" ) % secName % mfmSecY_Y %
                    mfmSecY_z % mfmSecZ_Z % mfmSecZ_y
             << endl;
      } else { // not last
        secNamePrev = secName;
      }
    }
  }
  // Sort four vectors of pairs for idividual per-mirror magnification factors:
  sort( magnFactMirrOrdSecY_Y.begin(), magnFactMirrOrdSecY_Y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactMirrOrdSecY_z.begin(), magnFactMirrOrdSecY_z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactMirrOrdSecZ_Z.begin(), magnFactMirrOrdSecZ_Z.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  sort( magnFactMirrOrdSecZ_y.begin(), magnFactMirrOrdSecZ_y.end(),
        []( const pair<string, double>& a, const pair<string, double>& b ) { return a.second < b.second; } );
  // Calculate the average values per kind-of-factor:
  // Iterators:
  secY_Y_It = magnFactMirrOrdSecY_Y.begin();
  secY_z_It = magnFactMirrOrdSecY_z.begin();
  secZ_Z_It = magnFactMirrOrdSecZ_Z.begin();
  secZ_y_It = magnFactMirrOrdSecZ_y.begin();
  // These will be secnted at the bottom:
  secY_Y_Mn = secY_Y_It->second;
  secY_z_Mn = secY_z_It->second;
  secZ_Z_Mn = secZ_Z_It->second;
  secZ_y_Mn = secZ_y_It->second;
  // Averages:
  secY_Y_Av = 0.;
  secY_z_Av = 0.;
  secZ_Z_Av = 0.;
  secZ_y_Av = 0.;
  // And now print the result:
  cout << "------------------------------------------------------------------" << endl;
  cout << "For " + RICH + ", each MF-per-secondary-mirror column is now ordered," << endl;
  cout << "and" << endl;
  cout << "on the left its resulting secondary-mirror-numbers column is given:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "   sec   secY_Y     sec   secY_z     sec   secZ_Z     sec   secZ_y" << endl;
  while ( secY_Y_It != magnFactMirrOrdSecY_Y.end() ) {
    cout << format( "   %|3s|%| 8.2f|   %|6s|%| 8.2f|   %|6s|%| 8.2f|   %|6s|%| 8.2f|" ) % secY_Y_It->first %
                secY_Y_It->second % secY_z_It->first % secY_z_It->second % secZ_Z_It->first % secZ_Z_It->second %
                secZ_y_It->first % secZ_y_It->second
         << endl;
    secY_Y_Av += secY_Y_It->second;
    secY_z_Av += secY_z_It->second;
    secZ_Z_Av += secZ_Z_It->second;
    secZ_y_Av += secZ_y_It->second;
    secY_Y_It++;
    secY_z_It++;
    secZ_Z_It++;
    secZ_y_It++;
  }
  secY_Y_It--;
  secY_z_It--;
  secZ_Z_It--;
  secZ_y_It--;
  secY_Y_Mx = secY_Y_It->second;
  secY_z_Mx = secY_z_It->second;
  secZ_Z_Mx = secZ_Z_It->second;
  secZ_y_Mx = secZ_y_It->second;

  secY_Y_Av /= secMirrsTotN;
  secY_z_Av /= secMirrsTotN;
  secZ_Z_Av /= secMirrsTotN;
  secZ_y_Av /= secMirrsTotN;

  secY_Y_Dl = secY_Y_Mx - secY_Y_Mn;
  secY_z_Dl = secY_z_Mx - secY_z_Mn;
  secZ_Z_Dl = secZ_Z_Mx - secZ_Z_Mn;
  secZ_y_Dl = secZ_y_Mx - secZ_y_Mn;
  cout << "------------------------------------------------------------------" << endl;
  cout << "Finally, summary over all " + RICH + " secondary mirrors:" << endl;
  cout << "------------------------------------------------------------------" << endl;
  cout << "         secY_Y           secY_z           secZ_Z           secZ_y" << endl;
  cout << format( "min    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Mn % secY_z_Mn %
              secZ_Z_Mn % secZ_y_Mn
       << endl;
  cout << format( "average%| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Av % secY_z_Av %
              secZ_Z_Av % secZ_y_Av
       << endl;
  cout << format( "max    %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Mx % secY_z_Mx %
              secZ_Z_Mx % secZ_y_Mx
       << endl;
  cout << format( "delta  %| 7.2f|          %| 7.2f|          %| 7.2f|          %| 7.2f|" ) % secY_Y_Dl % secY_z_Dl %
              secZ_Z_Dl % secZ_y_Dl
       << endl;
  cout << "==================================================================" << endl;
  cout << endl;
  cout << endl;

  //----------------------------------------------------------------------------

  double stopToleranceTotal;
  // total stop tolerance is equal to a fraction (typically 0.5)
  // of the "ideal" sigma of the Ch-kov angle distribution:
  stopToleranceTotal = nominalResolutionSigma * stopSigmaFraction;

  // let's calculate the tolerances if stopToleranceMode == 0
  if ( stopToleranceMode == 0 ) {

    // now we calculate tolerances fairly among the contributors,
    // inverse-proportional to their average magnification factors,
    // i.e. each contribution = 1/2 of the total stop tolerance

    stopTolerancePriY = 0.5 * stopToleranceTotal / fabs( magnFactAver["priY_Y"] );
    stopToleranceSecY = 0.5 * stopToleranceTotal / fabs( magnFactAver["secY_Y"] );
    stopTolerancePriZ = 0.5 * stopToleranceTotal / fabs( magnFactAver["priZ_Z"] );
    stopToleranceSecZ = 0.5 * stopToleranceTotal / fabs( magnFactAver["secZ_Z"] );
  }

  cout << format( "stopTolerancePriY = %| 5.2f|" ) % stopTolerancePriY << endl;
  cout << format( "stopToleranceSecY = %| 5.2f|" ) % stopToleranceSecY << endl;

  cout << format( "stopTolerancePriZ = %| 5.2f|" ) % stopTolerancePriZ << endl;
  cout << format( "stopToleranceSecZ = %| 5.2f|" ) % stopToleranceSecZ << endl;
  /*
  for (const autto& mfc : magnFactComb) {
    cout <<format("magnFactComb["+mfc.first+"] = %| 5.2f|")%mfc.second<< endl;
  //}
  */
  // Read-in fit results from RichMirrCombinFit, i.e. detected combined primary
  // and secondary mirror rotations and constant terms:
  ifstream mirrCombFitRes_ifs;

  string variant_iN = variant + iN;
  mirrCombFitRes_ifs.open( mirrCombinFitResultsFile.c_str() );

  map<string, double> combFitParName_val;

  string comb;
  double rotRoundY, rotRoundY_err, rotRoundZ, rotRoundZ_err, constTerm, constTerm_err;

  while ( mirrCombFitRes_ifs >> comb >> rotRoundY >> rotRoundY_err >> rotRoundZ >> rotRoundZ_err >> constTerm >>
          constTerm_err ) {
    combFitParName_val[comb + "Y"]    = rotRoundY;
    combFitParName_val[comb + "Yerr"] = rotRoundY_err;
    combFitParName_val[comb + "Z"]    = rotRoundZ;
    combFitParName_val[comb + "Zerr"] = rotRoundZ_err;
    combFitParName_val[comb + "C"]    = constTerm;
    combFitParName_val[comb + "Cerr"] = constTerm_err;
    combsSubset.push_back( comb );
    /*
    cout
    <<format("comb: %s") %comb
    <<format(    "    rotY=%| 6.3f|")%combFitParName_val[comb+"Y"   ]
    <<format( "    rotYErr=%| 6.3f|")%combFitParName_val[comb+"Yerr"]
    <<format(    "    rotZ=%| 6.3f|")%combFitParName_val[comb+"Z"   ]
    <<format( "    rotZErr=%| 6.3f|")%combFitParName_val[comb+"Zerr"]
    <<format(   "    const=%| 6.3f|")%combFitParName_val[comb+"C"   ]
    <<format("    constErr=%| 6.3f|")%combFitParName_val[comb+"Cerr"]
    <<endl;
    */
  }
  mirrCombFitRes_ifs.close();

  cout << endl << endl << "Start of the case  " + variant_iN << endl;
  //==========================================================================
  map<string, double> parName_value_init;
  map<string, double> parName_value_prev;
  map<string, double> parName_value_this;

  vector<string> priNames;
  vector<string> secNames;

  if ( rN == "1" ) {
    //-------------------------------------
    //         RICH1 mirror segments
    //      under alignment monitoring
    //=====================================
    //               primary
    //
    //   quadrant 0           quadrant 1
    //         00     side 0    01
    //   ---------------o---------------
    //         03     side 1    02
    //   quadrant 3           quadrant 2
    //=====================================
    //              secondary
    //
    //   quadrant 0           quadrant 1
    //        00    01     04    05
    //                side 0
    //        02    03     06    07
    //   ---------------o---------------
    //        08    09     12    13
    //                side 1
    //        10    11     14    15
    //   quadrant 3           quadrant 2
    //=====================================

    string priLay[4]{
        "p00",
        "p01",
        "p03",
        "p02",
    };
    for ( const auto& name : priLay ) priNames.push_back( name );

    sort( priNames.begin(), priNames.end() );

    for ( const auto& name : priNames ) {
      parName_value_init[name + "Y"] = 0.;
      parName_value_prev[name + "Y"] = 0.;
      parName_value_this[name + "Y"] = 0.;
      parName_value_init[name + "Z"] = 0.;
      parName_value_prev[name + "Z"] = 0.;
      parName_value_this[name + "Z"] = 0.;
    }
    string secLay[16]{
        "s00", "s01", "s04", "s05", "s02", "s03", "s06", "s07", "s08", "s09", "s12", "s13", "s10", "s11", "s14", "s15",
    };
    for ( const auto& name : secLay ) secNames.push_back( name );

    sort( secNames.begin(), secNames.end() );

    for ( const auto& name : secNames ) {
      parName_value_init[name + "Y"] = 0.;
      parName_value_prev[name + "Y"] = 0.;
      parName_value_this[name + "Y"] = 0.;
      parName_value_init[name + "Z"] = 0.;
      parName_value_prev[name + "Z"] = 0.;
      parName_value_this[name + "Z"] = 0.;
    }
  } else if ( rN == "2" ) {
    //---------------------------------------------------------
    //    RICH2 mirror segments under alignment monitoring
    //=========================================================
    //                         primary
    //           side 0                       side 1
    //
    //    27    26    25    24    |    55    54    53    52
    //    23    22    21    20    |    51    50    49    48
    //    19    18    17    16    |    47    46    45    44
    //    15    14    13    12    o    43    42    41    40
    //    11    10    09    08    |    39    38    37    36
    //    07    06    05    04    |    35    34    33    32
    //    03    02    01    00    |    31    30    29    28
    //=========================================================
    //                        secondary
    //           side 0                       side 1
    //
    //    19    18    17    16    |    39    38    37    36
    //    15    14    13    12    |    35    34    33    32
    //    11    10    09    08    o    31    30    29    28
    //    07    06    05    04    |    27    26    25    24
    //    03    02    01    00    |    23    22    21    20
    //=========================================================

    string priLay[56]{
        "p27", "p26", "p25", "p24", "p55", "p54", "p53", "p52", "p23", "p22", "p21", "p20", "p51", "p50",
        "p49", "p48", "p19", "p18", "p17", "p16", "p47", "p46", "p45", "p44", "p15", "p14", "p13", "p12",
        "p43", "p42", "p41", "p40", "p11", "p10", "p09", "p08", "p39", "p38", "p37", "p36", "p07", "p06",
        "p05", "p04", "p35", "p34", "p33", "p32", "p03", "p02", "p01", "p00", "p31", "p30", "p29", "p28",
    };
    for ( const auto& name : priLay ) priNames.push_back( name );

    sort( priNames.begin(), priNames.end() );

    for ( const auto& name : priNames ) {
      parName_value_init[name + "Y"] = 0.;
      parName_value_prev[name + "Y"] = 0.;
      parName_value_this[name + "Y"] = 0.;
      parName_value_init[name + "Z"] = 0.;
      parName_value_prev[name + "Z"] = 0.;
      parName_value_this[name + "Z"] = 0.;
    }
    string secLay[40]{
        "s19", "s18", "s17", "s16", "s39", "s38", "s37", "s36", "s15", "s14", "s13", "s12", "s35", "s34",
        "s33", "s32", "s11", "s10", "s09", "s08", "s31", "s30", "s29", "s28", "s07", "s06", "s05", "s04",
        "s27", "s26", "s25", "s24", "s03", "s02", "s01", "s00", "s23", "s22", "s21", "s20",
    };
    for ( const auto& name : secLay ) secNames.push_back( name );

    sort( secNames.begin(), secNames.end() );

    for ( const auto& name : secNames ) {
      parName_value_init[name + "Y"] = 0.;
      parName_value_prev[name + "Y"] = 0.;
      parName_value_this[name + "Y"] = 0.;
      parName_value_init[name + "Z"] = 0.;
      parName_value_prev[name + "Z"] = 0.;
      parName_value_this[name + "Z"] = 0.;
    }
    // The following is only proof-of-the-concept testing with RICH2 pre-misaligned mirrors:
    if ( usePremisaligned && iterNumber == 0 ) {
      parName_value_init["p55Y"] = -1.0;
      parName_value_init["p54Y"] = 0.7;
      parName_value_init["p53Y"] = 0.0;
      parName_value_init["p52Y"] = 1.0;
      parName_value_init["p51Y"] = -0.7;
      parName_value_init["p50Y"] = 0.0;
      parName_value_init["p49Y"] = -1.0;
      parName_value_init["p48Y"] = 0.7;
      parName_value_init["p47Y"] = 0.0;
      parName_value_init["p46Y"] = 1.0;
      parName_value_init["p45Y"] = -0.7;
      parName_value_init["p44Y"] = 0.0;
      parName_value_init["p43Y"] = -1.0;
      parName_value_init["p42Y"] = 0.7;
      parName_value_init["p41Y"] = 0.0;
      parName_value_init["p40Y"] = 1.0;
      parName_value_init["p39Y"] = -0.7;
      parName_value_init["p38Y"] = 0.0;
      parName_value_init["p37Y"] = -1.0;
      parName_value_init["p36Y"] = 0.7;
      parName_value_init["p35Y"] = 0.0;
      parName_value_init["p34Y"] = 1.0;
      parName_value_init["p33Y"] = -0.7;
      parName_value_init["p32Y"] = 0.0;
      parName_value_init["p31Y"] = -1.0;
      parName_value_init["p30Y"] = 0.7;
      parName_value_init["p29Y"] = 0.0;
      parName_value_init["p28Y"] = 1.0;
      parName_value_init["p27Y"] = -1.0;
      parName_value_init["p26Y"] = 0.7;
      parName_value_init["p25Y"] = 0.0;
      parName_value_init["p24Y"] = 1.0;
      parName_value_init["p23Y"] = -0.7;
      parName_value_init["p22Y"] = 0.0;
      parName_value_init["p21Y"] = -1.0;
      parName_value_init["p20Y"] = 0.7;
      parName_value_init["p19Y"] = 0.0;
      parName_value_init["p18Y"] = 1.0;
      parName_value_init["p17Y"] = -0.7;
      parName_value_init["p16Y"] = 0.0;
      parName_value_init["p15Y"] = -1.0;
      parName_value_init["p14Y"] = 0.7;
      parName_value_init["p13Y"] = 0.0;
      parName_value_init["p12Y"] = 1.0;
      parName_value_init["p11Y"] = -0.7;
      parName_value_init["p10Y"] = 0.0;
      parName_value_init["p09Y"] = -1.0;
      parName_value_init["p08Y"] = 0.7;
      parName_value_init["p07Y"] = 0.0;
      parName_value_init["p06Y"] = 1.0;
      parName_value_init["p05Y"] = -0.7;
      parName_value_init["p04Y"] = 0.0;
      parName_value_init["p03Y"] = -1.0;
      parName_value_init["p02Y"] = 0.7;
      parName_value_init["p01Y"] = 0.0;
      parName_value_init["p00Y"] = 1.0;

      parName_value_init["s39Y"] = -0.7;
      parName_value_init["s38Y"] = 0.0;
      parName_value_init["s37Y"] = -1.0;
      parName_value_init["s36Y"] = 0.7;
      parName_value_init["s35Y"] = 0.0;
      parName_value_init["s34Y"] = 1.0;
      parName_value_init["s33Y"] = -0.7;
      parName_value_init["s32Y"] = 0.0;
      parName_value_init["s31Y"] = -1.0;
      parName_value_init["s30Y"] = 0.7;
      parName_value_init["s29Y"] = 0.0;
      parName_value_init["s28Y"] = 1.0;
      parName_value_init["s27Y"] = -0.7;
      parName_value_init["s26Y"] = 0.0;
      parName_value_init["s25Y"] = -1.0;
      parName_value_init["s24Y"] = 0.7;
      parName_value_init["s23Y"] = 0.0;
      parName_value_init["s22Y"] = 1.0;
      parName_value_init["s21Y"] = -0.7;
      parName_value_init["s20Y"] = 0.0;
      parName_value_init["s19Y"] = -0.7;
      parName_value_init["s18Y"] = 0.0;
      parName_value_init["s17Y"] = -1.0;
      parName_value_init["s16Y"] = 0.7;
      parName_value_init["s15Y"] = 0.0;
      parName_value_init["s14Y"] = 1.0;
      parName_value_init["s13Y"] = -0.7;
      parName_value_init["s12Y"] = 0.0;
      parName_value_init["s11Y"] = -1.0;
      parName_value_init["s10Y"] = 0.7;
      parName_value_init["s09Y"] = 0.0;
      parName_value_init["s08Y"] = 1.0;
      parName_value_init["s07Y"] = -0.7;
      parName_value_init["s06Y"] = 0.0;
      parName_value_init["s05Y"] = -1.0;
      parName_value_init["s04Y"] = 0.7;
      parName_value_init["s03Y"] = 0.0;
      parName_value_init["s02Y"] = 1.0;
      parName_value_init["s01Y"] = -0.7;
      parName_value_init["s00Y"] = 0.0;

      parName_value_init["p55Z"] = 1.0;
      parName_value_init["p54Z"] = 0.0;
      parName_value_init["p53Z"] = 0.7;
      parName_value_init["p52Z"] = -1.0;
      parName_value_init["p51Z"] = 0.0;
      parName_value_init["p50Z"] = -0.7;
      parName_value_init["p49Z"] = 1.0;
      parName_value_init["p48Z"] = 0.0;
      parName_value_init["p47Z"] = 0.7;
      parName_value_init["p46Z"] = -1.0;
      parName_value_init["p45Z"] = 0.0;
      parName_value_init["p44Z"] = -0.7;
      parName_value_init["p43Z"] = 1.0;
      parName_value_init["p42Z"] = 0.0;
      parName_value_init["p41Z"] = 0.7;
      parName_value_init["p40Z"] = -1.0;
      parName_value_init["p39Z"] = 0.0;
      parName_value_init["p38Z"] = -0.7;
      parName_value_init["p37Z"] = 1.0;
      parName_value_init["p36Z"] = 0.0;
      parName_value_init["p35Z"] = 0.7;
      parName_value_init["p34Z"] = -1.0;
      parName_value_init["p33Z"] = 0.0;
      parName_value_init["p32Z"] = -0.7;
      parName_value_init["p31Z"] = 1.0;
      parName_value_init["p30Z"] = 0.0;
      parName_value_init["p29Z"] = 0.7;
      parName_value_init["p28Z"] = -1.0;
      parName_value_init["p27Z"] = 1.0;
      parName_value_init["p26Z"] = 0.0;
      parName_value_init["p25Z"] = 0.7;
      parName_value_init["p24Z"] = -1.0;
      parName_value_init["p23Z"] = 0.0;
      parName_value_init["p22Z"] = -0.7;
      parName_value_init["p21Z"] = 1.0;
      parName_value_init["p20Z"] = 0.0;
      parName_value_init["p19Z"] = 0.7;
      parName_value_init["p18Z"] = -1.0;
      parName_value_init["p17Z"] = 0.0;
      parName_value_init["p16Z"] = -0.7;
      parName_value_init["p15Z"] = 1.0;
      parName_value_init["p14Z"] = 0.0;
      parName_value_init["p13Z"] = 0.7;
      parName_value_init["p12Z"] = -1.0;
      parName_value_init["p11Z"] = 0.0;
      parName_value_init["p10Z"] = -0.7;
      parName_value_init["p09Z"] = 1.0;
      parName_value_init["p08Z"] = 0.0;
      parName_value_init["p07Z"] = 0.7;
      parName_value_init["p06Z"] = -1.0;
      parName_value_init["p05Z"] = 0.0;
      parName_value_init["p04Z"] = -0.7;
      parName_value_init["p03Z"] = 1.0;
      parName_value_init["p02Z"] = 0.0;
      parName_value_init["p01Z"] = 0.7;
      parName_value_init["p00Z"] = -1.0;

      parName_value_init["s39Z"] = 0.0;
      parName_value_init["s38Z"] = -0.7;
      parName_value_init["s37Z"] = 1.0;
      parName_value_init["s36Z"] = 0.0;
      parName_value_init["s35Z"] = 0.7;
      parName_value_init["s34Z"] = -1.0;
      parName_value_init["s33Z"] = 0.0;
      parName_value_init["s32Z"] = -0.7;
      parName_value_init["s31Z"] = 1.0;
      parName_value_init["s30Z"] = 0.0;
      parName_value_init["s29Z"] = 0.7;
      parName_value_init["s28Z"] = -1.0;
      parName_value_init["s27Z"] = 0.0;
      parName_value_init["s26Z"] = -0.7;
      parName_value_init["s25Z"] = 1.0;
      parName_value_init["s24Z"] = 0.0;
      parName_value_init["s23Z"] = 0.7;
      parName_value_init["s22Z"] = -1.0;
      parName_value_init["s21Z"] = 0.0;
      parName_value_init["s20Z"] = -0.7;
      parName_value_init["s19Z"] = 0.0;
      parName_value_init["s18Z"] = -0.7;
      parName_value_init["s17Z"] = 1.0;
      parName_value_init["s16Z"] = 0.0;
      parName_value_init["s15Z"] = 0.7;
      parName_value_init["s14Z"] = -1.0;
      parName_value_init["s13Z"] = 0.0;
      parName_value_init["s12Z"] = -0.7;
      parName_value_init["s11Z"] = 1.0;
      parName_value_init["s10Z"] = 0.0;
      parName_value_init["s09Z"] = 0.7;
      parName_value_init["s08Z"] = -1.0;
      parName_value_init["s07Z"] = 0.0;
      parName_value_init["s06Z"] = -0.7;
      parName_value_init["s05Z"] = 1.0;
      parName_value_init["s04Z"] = 0.0;
      parName_value_init["s03Z"] = 0.7;
      parName_value_init["s02Z"] = -1.0;
      parName_value_init["s01Z"] = 0.0;
      parName_value_init["s00Z"] = -0.7;
    }
  }

  //==========================================================================
  // The following section makes use of Minuit2 to solve the system of equations
  // by the least squares method.

  // The least squares method of solution of the system of equations is only
  // applicable to RICH2. In the RICH1 case we have just equations, each for
  // one unknown, and the solution is straightforward, i.e. algebraic.

  if ( solutionMethod == 0 ) {

    ROOT::Minuit2::MnUserParameters params;

    string priName;
    string priNameY;
    string priNameZ;

    string secName;
    string secNameY;
    string secNameZ;

    map<string, unsigned int> parName_ordNr;

    unsigned int ordNr = 0;

    for ( const auto& priName : priNames ) {

      if ( any_of( priNamesSubset.begin(), priNamesSubset.end(), [&]( string& s ) { return s == priName; } ) ) {
        priNameY = priName + "Y";
        priNameZ = priName + "Z";

        params.Add( priNameY.c_str(), parName_value_init[priNameY], 0.001 );
        params.Add( priNameZ.c_str(), parName_value_init[priNameZ], 0.001 );
        parName_ordNr[priNameY] = ordNr;
        // cout <<format("priNameY  = %s   "
        //             +"parName_ordNr.at(priNameY) = %|2d|")
        //              %parName
        //              %parName_ordNr.at(priNameY)<< endl;
        ordNr++;
        parName_ordNr[priNameZ] = ordNr;
        // cout <<format("priNameY  = %s   "
        //             +"parName_ordNr.at(priNameZ) = %|2d|")
        //              %parName
        //              %parName_ordNr.at(priNameZ)<< endl;
        ordNr++;
        // params.Release(priNameY.c_str());
        // params.Release(priNameZ.c_str());
      }
    }
    for ( const auto& secName : secNames ) {

      if ( any_of( secNamesSubset.begin(), secNamesSubset.end(), [&]( string& s ) { return s == secName; } ) ) {
        secNameY = secName + "Y";
        secNameZ = secName + "Z";

        params.Add( secNameY.c_str(), parName_value_init[secNameY], 0.001 );
        params.Add( secNameZ.c_str(), parName_value_init[secNameZ], 0.001 );
        parName_ordNr[secNameY] = ordNr;
        // cout <<format("secNameY  = %s   "
        //             +"parName_ordNr.at(secNameY) = %|2d|")
        //              %parName
        //              %parName_ordNr.at(secNameY)<< endl;
        ordNr++;
        parName_ordNr[secNameZ] = ordNr;
        // cout <<format("secNameY  = %s   "
        //             +"parName_ordNr.at(secNameZ) = %|2d|")
        //              %parName
        //              %parName_ordNr.at(secNameZ)<< endl;
        ordNr++;
        // params.Release(secNameY.c_str());
        // params.Release(secNameZ.c_str());
      }
    }

    // for (const auto& name_nr : parName_ordNr) {
    //  cout <<format("name_nr = %s   name_nr.at(name_nr) = %|2d|")
    //                %name_nr       %name_nr.at(name_nr)<< endl;
    //}

    // Instantiate the fitting function:
    RichMirrAlignFcn myFcn( combsSubset, combFitParName_val, magnFactComb, magnFactMirr, magnFactAver, parName_ordNr,
                            regularizationMode );

    // Instantiate Migrad:
    ROOT::Minuit2::MnMigrad migrad( myFcn, params );

    // Print parameters before fit
    cout << "Parameters before fit : " << params << endl;

    // Based on the printout from the line above, mirrors that are not in the subset are never included in the params.
    // Thus it is appropriate to comment out this next section.
    /*
    // Fix parameters of the mirrors that are not in the subset:
    for ( const auto& priName : priNames ) {
      if ( none_of( priNamesSubset.begin(), priNamesSubset.end(), [&]( string& s ) { return s == priName; } ) ) {
        priNameY = priName + "Y";
        priNameZ = priName + "Z";
        cout << "fixing " + priNameY << endl;
        // PN: The code has been crashing (apparently) on the next line when running within a python subprocess.
        migrad.Fix( migrad.Index( priNameY.data() ) );
        cout << "fixing " + priNameZ << endl;
        migrad.Fix( migrad.Index( priNameZ.data() ) );
      }
    }
    for ( const auto& secName : secNames ) {
      if ( none_of( secNamesSubset.begin(), secNamesSubset.end(), [&]( string& s ) { return s == secName; } ) ) {
        secNameY = secName + "Y";
        secNameZ = secName + "Z";
        cout << "fixing " + secNameY << endl;
        migrad.Fix( migrad.Index( secNameY.data() ) );
        cout << "fixing " + secNameZ << endl;
        migrad.Fix( migrad.Index( secNameZ.data() ) );
      }
    }
    */

    // Do the minimization:
    ROOT::Minuit2::FunctionMinimum min = migrad();

    // Print the results:
    cout << "Minimum   : " << min.Fval() << endl;
    cout << "Parameters: " << min.UserParameters() << endl;

    // Store the result in our private vector for further convenience:
    const ROOT::Minuit2::MnUserParameters outParams = min.UserParameters();

    for ( const auto& priName : priNames ) {
      priNameY = priName + "Y";
      priNameZ = priName + "Z";
      if ( any_of( priNamesSubset.begin(), priNamesSubset.end(), [&]( string& s ) { return s == priName; } ) ) {
        parName_value_this[priNameY] = outParams.Value( priNameY );
        cout << format( "outParams.Value(" + priNameY + ")) = %| 7.3f|   " ) % outParams.Value( priNameY );
        cout << format( "parName_value_this[" + priNameY + "]  = %| 7.3f|" ) % parName_value_this[priNameY] << endl;
        parName_value_this[priNameZ] = outParams.Value( priNameZ );
        cout << format( "outParams.Value(" + priNameZ + ")) = %| 7.3f|   " ) % outParams.Value( priNameZ );
        cout << format( "parName_value_this[" + priNameZ + "]  = %| 7.3f|" ) % parName_value_this[priNameZ] << endl;
      } else {
        parName_value_this[priNameY] = 0.;
        parName_value_this[priNameZ] = 0.;
      }
    }
    for ( const auto& secName : secNames ) {
      secNameY = secName + "Y";
      secNameZ = secName + "Z";
      if ( any_of( secNamesSubset.begin(), secNamesSubset.end(), [&]( string& s ) { return s == secName; } ) ) {
        parName_value_this[secNameY] = outParams.Value( secNameY );
        cout << format( "outParams.Value(" + secNameY + ")) = %| 7.3f|   " ) % outParams.Value( secNameY );
        cout << format( "parName_value_this[" + secNameY + "]  = %| 7.3f|" ) % parName_value_this[secNameY] << endl;
        parName_value_this[secNameZ] = outParams.Value( secNameZ );
        cout << format( "outParams.Value(" + secNameZ + ")) = %| 7.3f|   " ) % outParams.Value( secNameZ );
        cout << format( "parName_value_this[" + secNameZ + "]  = %| 7.3f|" ) % parName_value_this[secNameZ] << endl;
      } else {
        parName_value_this[secNameY] = 0.;
        parName_value_this[secNameZ] = 0.;
      }
    }

    // for (const auto& pname_val : parName_value_this) {
    //  cout <<format("pname_val.first = %s   pname_val.second = %| 7.3f|")%pname_val.first %pname_val.second<< endl;
    //}
  }
  // End of the Minuit-based solution section
  //==========================================================================

  // Next section is the algebraic method:
  else if ( solutionMethod == 1 ) {
    cout << "The algebraic method is not supported any more!" << endl;
  }

  //==========================================================================
  // Now that the solution is found, we will raise the flag to end the
  // alignment iterations loop if all the newly obtained in this iteration
  // additional corrections are < stopTolerancePriY ... etc. mrad

  string stop_or_continue( "STOP" );

  if ( stopDecisionMode == 0 ) {
    // NB! it is taken care at the beginning, that
    // if stopDecisionMode==0 the regularizationMode==0;
    // in this case sqrt of the final regularization term, divided by the number
    // of mirrors, should not exceed stopToleranceTotal
    double R = 0.0;
    for ( const auto& pname_val : parName_value_this ) {

      // cout <<format("pname_val.first = %s   pname_val.second = %|2d|")
      //              %pname_val.first       %pname_val.second<< endl;
      // pname_val.first   looks like e.g. p27Y
      // pname_val.second  looks like e.g. 0
      string axisKind = pname_val.first.substr( 3, 1 ); // e.g. Y
      string alteAxis;
      if ( axisKind == "Y" )
        alteAxis = "z";
      else { alteAxis = "y"; }
      // construct magnFactMirrName_sameAxis like e.g. p27Y_Y
      // construct magnFactMirrName_alteAxis like e.g. p27Y_z
      string magnFactMirrName_sameAxis = pname_val.first + "_" + axisKind;
      string magnFactMirrName_alteAxis = pname_val.first + "_" + alteAxis;
      // cout <<format("magnFactMirrName_sameAxis = %s")%magnFactAverIndName_sameAxis;
      // cout <<format("magnFactMirrName_alteAxis = %s")%magnFactAverIndName_alteAxis;

      if ( !std::isnan( magnFactMirr.find( magnFactMirrName_sameAxis )->second ) ) {
        R += pow( pname_val.second, 2. ) * ( pow( magnFactMirr.find( magnFactMirrName_sameAxis )->second, 2. ) +
                                             pow( magnFactMirr.find( magnFactMirrName_alteAxis )->second, 2. ) );
      }
    }
    double averSqrtR = sqrt( R ) / ( priMirrsTotN + secMirrsTotN );

    if ( averSqrtR > stopToleranceTotal ) {
      stop_or_continue = "CONTINUE";
      cout << format( "sqrt(R)/(priMirrsTotN + secMirrsTotN) =%|8.3f|  >  stopToleranceTotal =%|8.3f| -> " ) %
                  averSqrtR % stopToleranceTotal
           << stop_or_continue << endl;
    } else {
      cout << format( "sqrt(R)/(priMirrsTotN + secMirrsTotN) =%|8.3f|  <  stopToleranceTotal =%|8.3f| -> " ) %
                  averSqrtR % stopToleranceTotal
           << stop_or_continue << endl;
    }
  } else if ( stopDecisionMode == 1 ) {
    // if ( regularizationMode == 0   ) { // Anatoly's original from RICH-53
    if ( regularizationMode == 999 ) { // Changed to reproduce pre RICH-53 behavior when stopDecisionMode == 1 and
                                       // regularizationMode == 0, see below
      for ( const auto& pname_val : parName_value_this ) {
        string axisKind = pname_val.first.substr( 3, 1 ); // e.g. Y
        string alteAxis;
        if ( axisKind == "Y" )
          alteAxis = "z";
        else { alteAxis = "y"; }
        string magnFactMirrName_sameAxis = pname_val.first + "_" + axisKind;
        string magnFactMirrName_alteAxis = pname_val.first + "_" + alteAxis;
        if ( fabs( pname_val.second ) * sqrt( pow( magnFactMirr.find( magnFactMirrName_sameAxis )->second, 2. ) +
                                              pow( magnFactMirr.find( magnFactMirrName_alteAxis )->second, 2. ) ) >
             0.5 * stopToleranceTotal ) {
          stop_or_continue = "CONTINUE";
          break;
        }
      }
    }
    // else if ( regularizationMode == 1  ||  regularizationMode == 10 ) { // Anatoly's original from RICH-53
    else if ( regularizationMode == 1 || regularizationMode == 10 ||
              regularizationMode == 0 ) { // now using this also for regularizationMode == 0
      for ( const auto& pname_val : parName_value_this ) {
        if ( ( pname_val.first.substr( 0, 1 ) == "p" &&
               ( ( pname_val.first.substr( 3, 1 ) == "Y" && fabs( pname_val.second ) > stopTolerancePriY ) ||
                 ( pname_val.first.substr( 3, 1 ) == "Z" && fabs( pname_val.second ) > stopTolerancePriZ ) ) ) ||
             ( pname_val.first.substr( 0, 1 ) == "s" &&
               ( ( pname_val.first.substr( 3, 1 ) == "Y" && fabs( pname_val.second ) > stopToleranceSecY ) ||
                 ( pname_val.first.substr( 3, 1 ) == "Z" && fabs( pname_val.second ) > stopToleranceSecZ ) ) ) ) {
          stop_or_continue = "CONTINUE";
          break;
        }
      }
    }
  }

  cout << endl << "Verdict is " + stop_or_continue << endl << endl;

  // write the verdict to the stop_or_continue.txt file
  ofstream stop_or_continue_txt;
  stop_or_continue_txt.open( ( workDir + "/rich" + rN + "_stop_or_continue.txt" ).c_str() );
  stop_or_continue_txt << stop_or_continue << endl;
  stop_or_continue_txt.close();

  cout << "File rich" + rN + "_stop_or_continue.txt written" << endl << endl;

  //==========================================================================
  // Mirror rotational misalignment compensation corrections:
  // we assign with the opposite sign the obtained parameters (which are the
  // measured misalignments) to the this-iteration corrections of the
  // misalignment compensations; these corrections will be added to the
  // compensations used during this iteration and will be used during the next
  // iteration if they are not yet small enough.

  map<string, double> misalignCompensation_init; // previous misalignment compensations, used in zeroth iteration
  map<string, double> misalignCompensation_this; // misalignment compensations, used in this iteration
  map<string, double> misalignCompensation_next; // misalignment compensations updated after this iteration

  map<string, double> misalCompensCorrection_all_iters; // cumulative misalignment compensation corrections from all
                                                        // iterations, including this
  map<string, double> misalCompensCorrection_this_iter; // only this iteration misalignment compensation additional
                                                        // corrections

  for ( const auto& pname_val : parName_value_this ) {

    misalCompensCorrection_this_iter[pname_val.first] = -( pname_val.second );
    // cout <<format("misalCompensCorrection_this_iter.at("+pname_val.first+") = %| 8.5f|")
    //              %misalCompensCorrection_this_iter.at(  pname_val.first)<< endl;
  }

  //==========================================================================
  //
  //
  //
  //
  // YAML stuff
  //
  //
  //
  //

  // the resulting yml file will get iteration number incremented,
  // equal to the next iteration number:

  cout << endl;

  // string variant_init = variant+"_i0";
  // string variant_this = variant+"_i"+to_string(iterationNumber);
  // string variant_next = variant+"_i"+to_string(iterationNumber+1);

  // string                     init_yaml_str = workDir+"/Mirrors_rich"+rN+"_"+variant_init+".yml";
  // cout <<    "init_yaml:   "+init_yaml_str<< endl;
  // const char* init_yaml    = init_yaml_str.c_str();
  // string                     this_yaml_str = workDir+"/Mirrors_rich"+rN+"_"+variant_this+".yml";
  // cout <<    "this_yaml:   "+this_yaml_str<< endl;
  // const char* this_yaml    = this_yaml_str.c_str();
  // string                     next_yaml_str = workDir+"/Mirrors_rich"+rN+"_"+variant_next+".yml";
  // cout <<    "next_yaml:   "+next_yaml_str<< endl;
  // const char* next_yaml    = next_yaml_str.c_str();

  string this_yaml = thisIterationInputYAML;
  cout << "this_yaml:   " + thisIterationInputYAML << endl;

  string init_yaml = initIterationInputYAML;
  cout << "init_yaml:   " + initIterationInputYAML << endl;

  string next_yaml = nextIterationInputYAML;
  cout << "next_yaml:   " + nextIterationInputYAML << endl;

  cout << endl;

  // mirror types:
  string mirrTypes[2]{ "p", "s" };

  // prepare dictionary of mirror type names
  map<string, string> mirrType_typeName;
  mirrType_typeName["p"] = "M1";
  mirrType_typeName["s"] = "M2";

  string mirrType;
  string typeName;

  // Prepare dictionary of total mirror numbers:
  map<string, vector<string>> mirrType_mirrNames;
  mirrType_mirrNames["p"] = priNames;
  mirrType_mirrNames["s"] = secNames;

  try {
    YAML::Node conds = YAML::LoadFile( this_yaml );

    for ( const auto& mirrType : mirrTypes ) {
      typeName = mirrType_typeName[mirrType];

      // Loop over all mirrors:
      for ( const auto& mirrName : mirrType_mirrNames[mirrType] ) {

        string mirrNameY = mirrName + "Y";
        string mirrNameZ = mirrName + "Z";

        string mirrNr  = mirrName.substr( 1, 2 );
        string segName = "R" + rN + typeName + "Seg" + mirrNr;
        // cout <<"segName = "+segName<< endl;

        string rY = conds[segName]["rotation"][1].as<string>();
        string rZ = conds[segName]["rotation"][2].as<string>();

        smatch rotsma;
        regex  rotreg{ "[-\\d]+.[\\d]+" };

        regex_search( rY, rotsma, rotreg );
        double rotY = stod( rotsma.str() );
        regex_search( rZ, rotsma, rotreg );
        double rotZ = stod( rotsma.str() );
        ;

        // cout <<"rotY = "<<rotY<<"   rotZ = "<<rotZ<< endl;

        // Update the used in this iteration misalignment compensations with the
        // newly calculated misalignment compensations that are simply corrections
        // to the used ones. In other words, improve the misalignment
        // compensations that will be used in a next iteration, or simply will
        // stay like this from now on, if current alignment session finishes after
        // this iteration:

        // Used in this iteration compensations:
        misalignCompensation_this[mirrNameY] = rotY;
        misalignCompensation_this[mirrNameZ] = rotZ;

        // Updated compensations resulting from this iteration:
        misalignCompensation_next[mirrNameY] =
            misalignCompensation_this[mirrNameY] + misalCompensCorrection_this_iter[mirrNameY];
        misalignCompensation_next[mirrNameZ] =
            misalignCompensation_this[mirrNameZ] + misalCompensCorrection_this_iter[mirrNameZ];

        // if ( mirrNr == "00" ) {
        //  cout << format( "misalCompensCorrection_this_iter["+mirrNameY+"] =%| 8.5f| " )
        //                  %misalCompensCorrection_this_iter[  mirrNameY]
        //       << endl;
        //  cout << format( "misalignCompensation_this[       "+mirrNameY+"] =%| 8.5f| " )
        //                  %misalignCompensation_this[         mirrNameY]
        //       << endl;
        //  cout << format( "misalignCompensation_next[       "+mirrNameY+"] =%| 8.5f| " )
        //                  %misalignCompensation_next[         mirrNameY]
        //       << endl;
        //
        //  cout << format( "misalCompensCorrection_this_iter["+mirrNameZ+"] =%| 8.5f| " )
        //                  %misalCompensCorrection_this_iter[  mirrNameZ]
        //       << endl;
        //  cout << format( "misalignCompensation_this[       "+mirrNameZ+"] =%| 8.5f| " )
        //                  %misalignCompensation_this[         mirrNameZ]
        //       << endl;
        //  cout << format( "misalignCompensation_next[       "+mirrNameZ+"] =%| 8.5f| " )
        //                  %misalignCompensation_next[         mirrNameZ]
        //       << endl;
        //}

        // Update the rotation body:
        rotY                          = misalignCompensation_next[mirrNameY];
        rotZ                          = misalignCompensation_next[mirrNameZ];
        conds[segName]["rotation"][1] = to_string( rotY ) + " * mrad";
        conds[segName]["rotation"][2] = to_string( rotZ ) + " * mrad";

      } // End loop over mirror numbers.
    }   // End loop over mirror types.
    // Finally, write out the new YAML file for the next iteration:

    cout << "===========================================" << endl;
    cout << "Write output file" << endl;

    YAML::Emitter next_emit;
    next_emit << conds;
    cout << "Here's the output YAML:                 " << next_yaml << endl;
    ofstream fout( next_yaml );
    fout << next_emit.c_str();
    fout.close();
    cout << "Output file complete" << endl;
    cout << "===========================================" << endl;
  } catch ( ... ) {
    cout << "Error" << endl;
    return 1;
  }

  // Now read the file with the previous compensations used by
  // the very first (zeroth) iteration of this alignment session,
  // and then calculate the cumulative corrections resulted from
  // the current iteration:
  try {
    YAML::Node conds = YAML::LoadFile( init_yaml );

    for ( const auto& mirrType : mirrTypes ) {
      typeName = mirrType_typeName[mirrType];

      // Loop over all mirrors:
      for ( const auto& mirrName : mirrType_mirrNames[mirrType] ) {

        string mirrNameY = mirrName + "Y";
        string mirrNameZ = mirrName + "Z";

        string mirrNr  = mirrName.substr( 1, 2 );
        string segName = "R" + rN + typeName + "Seg" + mirrNr;
        // cout <<"segName = "+segName<< endl;

        string rY = conds[segName]["rotation"][1].as<string>();
        string rZ = conds[segName]["rotation"][2].as<string>();

        smatch rotsma;
        regex  rotreg{ "[-\\d]+.[\\d]+" };

        regex_search( rY, rotsma, rotreg );
        double rotY = stod( rotsma.str() );
        regex_search( rZ, rotsma, rotreg );
        double rotZ = stod( rotsma.str() );
        ;

        // cout <<"rotY = "<<rotY<<"   rotZ = "<<rotZ<< endl;

        // Previous compensations, still in the CONDDB, that were used during the very first iteration:
        misalignCompensation_init[mirrNameY] = rotY;
        misalignCompensation_init[mirrNameZ] = rotZ;
        // Accumulated compensation corrections from all iterations, including this iteration:
        misalCompensCorrection_all_iters[mirrNameY] =
            misalignCompensation_next[mirrNameY] - misalignCompensation_init[mirrNameY];
        misalCompensCorrection_all_iters[mirrNameZ] =
            misalignCompensation_next[mirrNameZ] - misalignCompensation_init[mirrNameZ];

        // if ( mirrNr == "00" ) {
        //  cout << mirrType+mirrNr << endl;
        //  cout << format("previous compensations (used in zeroth iteration)         round Y: %| 10.5f|")
        //  %misalignCompensation_init[       mirrNameY]; cout << format( ",  round Z: %| 10.5f|")
        //  %misalignCompensation_init[       mirrNameZ]<< endl; cout << format("used in this iteration compensations
        //  round Y: %| 10.5f|") %misalignCompensation_this[       mirrNameY]; cout << format( ",  round Z: %| 10.5f|")
        //  %misalignCompensation_this[       mirrNameZ]<< endl; cout << format("cumulative corrections to previous
        //  compensations          round Y: %| 10.5f|") %misalCompensCorrection_all_iters[mirrNameY]; cout << format( ",
        //  round Z: %| 10.5f|") %misalCompensCorrection_all_iters[mirrNameZ]<< endl; cout << format("updated
        //  compensations                                     round Y: %| 10.5f|") %misalignCompensation_next[
        //  mirrNameY]; cout << format(                                                       ",  round Z: %| 10.5f|")
        //  %misalignCompensation_next[       mirrNameZ]<< endl; cout << format("this iteration's additional corrections
        //  to compensations  round Y: %| 10.5f|") %misalCompensCorrection_this_iter[mirrNameY]; cout << format( ",
        //  round Z: %| 10.5f|") %misalCompensCorrection_this_iter[mirrNameZ]<< endl;
        //}
      } // End loop over mirror numbers.
    }   // End loop over mirror types.
  } catch ( ... ) {
    cout << "Error" << endl;
    return 1;
  }
  cout << "===========================================" << endl;

  //==========================================================================
  //
  //
  //
  //
  // Final summary printout for monitoring:
  //
  //
  //
  //
  cout << endl;
  // cout <<"                                                  "+RICH<< endl;
  cout << endl;
  cout << "Summary  of the case  " + variant_iN << endl;
  cout << endl;
  cout << endl;

  // define how to print
  vector<int> printStyles;

  if ( printMode == 0 )
    printStyles.push_back( 0 );
  else if ( printMode == 1 )
    printStyles.push_back( 1 );
  else if ( printMode == 2 ) {
    printStyles.push_back( 1 );
    printStyles.push_back( 0 );
  }

  string printStyle_str[] = { "                          One-glance printout with no digits after decimal point",
                              "                           Detailed printout with one digit after decimal point" };
  for ( auto& printStyle : printStyles ) {

    cout << endl;
    cout << endl;
    cout << printStyle_str[printStyle] << endl;

    for ( auto mirrType : mirrTypes ) {

      vector<string> mirrLay;

      cout << endl;

      double stopToleranceY;
      double stopToleranceZ;

      string stopTolY;
      string stopTolZ;

      if ( mirrType == "p" ) {
        mirrLay = priNames;
        cout << "                                              " + RICH + " Primary" << endl;
        stopToleranceY = stopTolerancePriY;
        stringstream stopTolY_stgstm;
        stopTolY_stgstm << format( "%|4.2f|" ) % stopTolerancePriY;
        stopTolY       = stopTolY_stgstm.str();
        stopToleranceZ = stopTolerancePriZ;
        stringstream stopTolZ_stgstm;
        stopTolZ_stgstm << format( "%|4.2f|" ) % stopTolerancePriZ;
        stopTolZ = stopTolZ_stgstm.str();
        cout << "                 divided by the tolerances  Y:" + stopTolY + "  Z:" + stopTolZ +
                    " mrad, truncated, not rounded"
             << endl;
      } else if ( mirrType == "s" ) {
        mirrLay = secNames;
        cout << "                                             " + RICH + " Secondary" << endl;
        stopToleranceY = stopToleranceSecY;
        stringstream stopTolY_stgstm;
        stopTolY_stgstm << format( "%|4.2f|" ) % stopToleranceSecY;
        stopTolY       = stopTolY_stgstm.str();
        stopToleranceZ = stopToleranceSecZ;
        stringstream stopTolZ_stgstm;
        stopTolZ_stgstm << format( "%|4.2f|" ) % stopToleranceSecZ;
        stopTolZ = stopTolZ_stgstm.str();
        cout << "                 divided by the tolerances  Y:" + stopTolY + "  Z:" + stopTolZ +
                    " mrad, truncated, not rounded"
             << endl;
      }

      cout << endl;

      cout << "                             previous compensations (used in 0th iteration)" << endl;
      print_yz_table( rich, mirrType, mirrLay, misalignCompensation_init, printStyle, stopToleranceY, stopToleranceZ );

      cout << "                    cumulative corrections to previous compensations" << endl;
      print_yz_table( rich, mirrType, mirrLay, misalCompensCorrection_all_iters, printStyle, stopToleranceY,
                      stopToleranceZ );

      cout << "                                  updated compensations" << endl;
      print_yz_table( rich, mirrType, mirrLay, misalignCompensation_next, printStyle, stopToleranceY, stopToleranceZ );

      cout << "              only this iteration's additional corrections to compensations" << endl;
      print_yz_table( rich, mirrType, mirrLay, misalCompensCorrection_this_iter, printStyle, stopToleranceY,
                      stopToleranceZ );
    }
  }
  //==========================================================================

  cout << "End" << endl;
  //
  //
  //
  //
  //
  //
  //
  //
  return ( 0 );
}

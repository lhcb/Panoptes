/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class RichMirrAlignFcn RichMirrAlignFcn.h
 *
 *  @author Antonis Papanestis
 *  @date   2004-11-08
 */
#pragma once

#include <Minuit2/FCNBase.h>
#include <map>
#include <string>
#include <utility>
#include <vector>

class RichMirrAlignFcn : public ROOT::Minuit2::FCNBase {

public:
  /// Standard constructor
  RichMirrAlignFcn( const std::vector<std::string>&            combsSubset,
                    const std::map<std::string, double>&       combFitParName_val,
                    const std::map<std::string, double>&       magnFactComb,
                    const std::map<std::string, double>&       magnFactMirr,
                    const std::map<std::string, double>&       magnFactAver,
                    const std::map<std::string, unsigned int>& parName_ordNr, const unsigned int& regularizationMode );

  virtual ~RichMirrAlignFcn(); ///< Destructor

  double Up() const override { return m_theErrorDef; }

  double operator()( const std::vector<double>& params ) const override;

private:
  std::vector<std::string>            m_combsSubset;
  std::map<std::string, double>       m_combFitParName_val;
  std::map<std::string, double>       m_magnFactComb;
  std::map<std::string, double>       m_magnFactMirr;
  std::map<std::string, double>       m_magnFactAver;
  std::map<std::string, unsigned int> m_parName_ordNr;
  unsigned int                        m_regularizationMode;

  double m_theErrorDef{ 0 };
};

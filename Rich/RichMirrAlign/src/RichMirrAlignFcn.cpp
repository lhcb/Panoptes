/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichMirrAlignFcn.h"
#include <boost/format.hpp>
#include <cmath>
#include <iostream>

using boost::format;
using std::cout;
using std::endl;
using std::map;
using std::pair;
using std::string;
using std::vector;
//-----------------------------------------------------------------------------
// Implementation file for class : RichMirrAlignFcn
//
// 2004-11-08 : Antonis Papanestis
//-----------------------------------------------------------------------------

// Standard constructor, initializes variables
RichMirrAlignFcn::RichMirrAlignFcn( const vector<string>& combsSubset, const map<string, double>& combFitParName_val,
                                    const map<string, double>& magnFactComb, const map<string, double>& magnFactMirr,
                                    const map<string, double>&       magnFactAver,
                                    const map<string, unsigned int>& parName_ordNr,
                                    const unsigned int&              regularizationMode )
    : m_combsSubset( combsSubset )
    , m_combFitParName_val( combFitParName_val )
    , m_magnFactComb( magnFactComb )
    , m_magnFactMirr( magnFactMirr )
    , m_magnFactAver( magnFactAver )
    , m_parName_ordNr( parName_ordNr )
    , m_regularizationMode( regularizationMode )
    ,

    m_theErrorDef( 1.0 ) {

  // cout <<"Hello from RichMirrAlignFcn! Initialization."<< endl;
}

// Destructor:
RichMirrAlignFcn::~RichMirrAlignFcn() {}

// operator():
double RichMirrAlignFcn::operator()( const vector<double>& par ) const {
  // cout <<"Hello from RichMirrAlignFcn! Iteration."<< endl;

  /*
  unsigned int i=0;
  for ( const auto& parIt : par ) {
    cout <<format("par[%|02d|] =%| 6.3f|")
                       %i       %parIt<< endl;
    i++;
  }
  */
  //

  double D = 0.0;
  double R = 0.0;
  double S = 0.0;

  for ( const auto& comb : m_combsSubset ) {

    double combY = m_combFitParName_val.find( comb + "Y" )->second;
    // double combYerr = m_combFitParName_val.find(comb+"Yerr")->second;
    double combZ = m_combFitParName_val.find( comb + "Z" )->second;
    // double combZerr = m_combFitParName_val.find(comb+"Zerr")->second;
    /*
    cout <<comb+"   ";
    cout <<format("combY =%| 6.3f|    combYerr =%| 6.3f|    combZ =%| 6.3f|    combZerr =%| 6.3f|    ")
                  %combY             %combYerr             %combZ             %combZerr<< endl;
    */
    //
    string priName = comb.substr( 0, 3 );
    string secName = comb.substr( 3, 3 );
    // cout <<priName+"   "+secName+"   ";
    string priNameY = priName + "Y";
    string priNameZ = priName + "Z";
    string secNameY = secName + "Y";
    string secNameZ = secName + "Z";
    // cout <<priNameY+"   "+secNameY+"   "+priNameZ+"   "+secNameZ+"   "<< endl;
    unsigned int priYNr = m_parName_ordNr.find( priNameY )->second;
    unsigned int priZNr = m_parName_ordNr.find( priNameZ )->second;
    unsigned int secYNr = m_parName_ordNr.find( secNameY )->second;
    unsigned int secZNr = m_parName_ordNr.find( secNameZ )->second;
    /*
    cout <<format("priYNr =%|2d|    secYNr =%|2d|    priZNr =%|2d|    secZNr =%|2d|    ")
                  %priYNr          %secYNr          %priZNr          %secZNr;
    */
    //
    double priY = par[priYNr];
    double priZ = par[priZNr];
    double secY = par[secYNr];
    double secZ = par[secZNr];
    /*
    cout <<format("priY =%| 6.3f|    secY =%| 6.3f|    priZ =%| 6.3f|    secZ =%| 6.3f|    ")
                  %priY             %secY             %priZ             %secZ<< endl;
    */
    //
    // double priYerr = err[priNrYi];
    // double priZerr = err[priNrZi];
    // double secYerr = err[secNrYi];
    // double secZerr = err[secNrZi];

    double mf_priY_Y = m_magnFactComb.find( comb + "priY_Y" )->second;
    double mf_priY_z = m_magnFactComb.find( comb + "priY_z" )->second;
    double mf_priZ_Z = m_magnFactComb.find( comb + "priZ_Z" )->second;
    double mf_priZ_y = m_magnFactComb.find( comb + "priZ_y" )->second;
    double mf_secY_Y = m_magnFactComb.find( comb + "secY_Y" )->second;
    double mf_secY_z = m_magnFactComb.find( comb + "secY_z" )->second;
    double mf_secZ_Z = m_magnFactComb.find( comb + "secZ_Z" )->second;
    double mf_secZ_y = m_magnFactComb.find( comb + "secZ_y" )->second;
    /*
    cout <<format("mf_priY_Y =%| 6.3f|    mf_secY_Y =%| 6.3f|    mf_priZ_y =%| 6.3f|    mf_secZ_y =%| 6.3f|    ")
                  %mf_priY_Y             %mf_secY_Y             %mf_priZ_y             %mf_secZ_y;
    cout <<format("mf_priY_z =%| 6.3f|    mf_secY_z =%| 6.3f|    mf_priZ_Z =%| 6.3f|    mf_secZ_Z =%| 6.3f|    ")
                  %mf_priY_z             %mf_secY_z             %mf_priZ_Z             %mf_secZ_Z<< endl;
    */
    //

    // Sum of squares of the differences between the right- and left-hand sides
    // divided by their dispersions <-- only if histogram populations are forced to be equal
    // for Y and Z separately:

    double diffY = combY - ( mf_priY_Y * priY + mf_secY_Y * secY +  // major terms
                             mf_priZ_y * priZ + mf_secZ_y * secZ ); // minor terms
    /*
    // either <-- only if histogram populations are forced to be equal:
    D += pow( diffY, 2. ) / ( pow( combYerr, 2. ) + pow( mf_priY_Y*priYerr, 2. ) + pow( mf_secY_Y*secYerr, 2. ) +  //
    major terms pow( mf_priZ_y*priZerr, 2. ) + pow( mf_secZ_y*secZerr, 2. ));  // minor terms D += pow( diffY, 2. ) /
    pow( combYerr, 2. );
    */

    // or:
    D += pow( diffY, 2. );

    // cout <<format("D diffY =%|8.3f|   ")%D;

    double diffZ = combZ - ( mf_priZ_Z * priZ + mf_secZ_Z * secZ +  // major terms
                             mf_priY_z * priY + mf_secY_z * secY ); // minor terms
    /*
    // either <-- only if histogram populations are forced to be equal:
    D += pow( diffZ, 2. ) / ( pow( combZerr, 2. ) + pow( mf_priZ_Z*priZerr, 2. ) + pow( mf_secZ_Z*secZerr, 2. ) +  //
    major terms pow( mf_priY_z*priYerr, 2. ) + pow( mf_secY_z*secYerr, 2. ));  // minor terms D += pow( diffZ, 2. ) /
    pow( combZerr, 2. );
    */

    // or:
    D += pow( diffZ, 2. );

    // cout <<format("D diffZ =%|8.3f|   ")%D;
  }

  // The regularization term.

  // If m_regularizationMode == 0 (default), add the regularizing sum of the
  // squares of the "magnified" solution vector components. Each component of
  // the solution vector is multiplied by the respective individual
  // magnification factor, both by "major" MF and also by "minor" (AKA
  // "cross-talk") MF, for each mirror averaged over its partners in the mirror
  // combinations it participates and of course, over the respective
  // calibrational tilts.

  // If m_regularizationMode == 10 (obsolete), add the regularizing sum of the
  // squares of the "magnified" solution vector components. Each component of
  // the solution vector is multiplied only by the respective "major"
  // magnification factor, just per mirror kind and axis kind, averaged over all
  // mirror combinations and of course, over the respective calibrational tilts.
  // This is retained for tests and comparisons.

  // Otherwise (m_regularizationMode ==  1), (just for the records) add the
  // regularizing sum of just the square of the Eucleadian norm of the solution
  // (i.e. "non-magnified" regularization term).

  if ( m_regularizationMode == 0 ) {
    for ( const auto& name_nr : m_parName_ordNr ) {
      // cout <<format("name_nr.first = %s   name_nr.second = %|2d|")%name_nr.first %name_nr.second<< endl;
      // name_nr.first   looks like e.g. p27Y
      // name_nr.second  looks like e.g. 0
      string axisKind = name_nr.first.substr( 3, 1 ); // e.g. Y
      string alteAxis;
      if ( axisKind == "Y" )
        alteAxis = "z";
      else { alteAxis = "y"; }
      // construct magnFactMirrName_sameAxis like e.g. pri27Y_Y
      // construct magnFactMirrName_alteAxis like e.g. pri27Y_z
      string magnFactMirrName_sameAxis = name_nr.first + "_" + axisKind;
      string magnFactMirrName_alteAxis = name_nr.first + "_" + alteAxis;
      // cout <<format("magnFactMirrName_sameAxis = %s")%magnFactMirrName_sameAxis;
      // cout <<format("magnFactMirrName_alteAxis = %s")%magnFactMirrName_alteAxis;
      R += pow( par[name_nr.second], 2. ) * ( pow( m_magnFactMirr.find( magnFactMirrName_sameAxis )->second, 2. ) +
                                              pow( m_magnFactMirr.find( magnFactMirrName_alteAxis )->second, 2. ) );
    }
  } else if ( m_regularizationMode == 10 ) {
    for ( const auto& name_nr : m_parName_ordNr ) {
      // name_nr.first   looks like e.g. pri27Y
      // name_nr.second  looks like e.g. 0
      string mirrKind = name_nr.first.substr( 0, 1 );
      string axisKind = name_nr.first.substr( 3, 1 );
      // construct magnFactAverName like e.g. priY_Y
      string magnFactAverName = mirrKind + axisKind + "_" + axisKind;
      // cout <<format("magnFactAverName = %s")%magnFactAverName;
      R += pow( par[name_nr.second], 2. ) * pow( m_magnFactAver.find( magnFactAverName )->second, 2. );
    }
  } else if ( m_regularizationMode == 1 ) {
    for ( const auto& name_nr : m_parName_ordNr ) { R += pow( par[name_nr.second], 2. ); }
  }
  // and now the final total sum:
  S = D + R;
  // cout <<format("D =%|8.3f|   R =%|8.3f|   S =%|8.3f|")%D %R %S<< endl;
  return S;
}

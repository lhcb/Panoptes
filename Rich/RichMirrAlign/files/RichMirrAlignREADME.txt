ssh  username@lxplus.cern.ch
setenv  User_release_area /afs/cern.ch/user/u/asolomin/public/rich_align
SetupProject  Panoptes HEAD --nightly lhcb-head
cd   ${User_release_area}/../rich_align_test
RichMirrAlign.exe  Rich1MirrAlign_Mp6Wi8.0Fm5Mm0Sm0_online_Collision15_i0.conf >  Rich1MirrAlign_Mp6Wi8.0Fm5Mm0Sm0_online_Collision15_i0.out
RichMirrAlign.exe  Rich2MirrAlign_Mp6Wi4.0Fm5Mm0Sm0_online_Collision15_i0.conf >  Rich2MirrAlign_Mp6Wi4.0Fm5Mm0Sm0_online_Collision15_i0.out

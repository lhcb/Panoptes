/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

void GetHPD( string line ) {
  string str( "  <condition classID=\"6\" name=\"HPD" );
  size_t found;
  found = line.find_first_not_of( str );
  line.erase( 0, found );

  string str2( "_Align\">" );
  size_t str2Length = str2.length();
  found             = line.find( str2 );
  line.erase( found, str2Length );

  cout << line << endl;
}

void GetInitalValue( string line, double& x, double& y ) {
  string str( "    <paramVector name=\"dPosXYZ\" type=\"double\">  " );
  size_t found;
  found = line.find_first_not_of( str );
  line.erase( 0, found );
  //    cout << line << endl;

  string str2( "   0*mm </paramVector>" );
  found = line.find( str2 );
  //	cout << found << endl;
  size_t len = line.length();
  line.erase( found, len );
  //    cout << line << endl;

  string str3( "*mm" );
  found = line.find( str3 );
  //    cout << found << endl;
  //    cout << found+2 << endl;

  line.erase( found, 3 );

  found = line.find( str3 );
  ////    cout << found << endl;
  line.erase( found, 3 );

  //    cout << line << endl;
  // split
  std::vector<double> values;
  double              value;
  istringstream       iss( line );
  while ( iss >> value ) {
    //    	cout << value << endl;
    values.push_back( value );
  }
  x = values[0];
  y = values[1];
}

int main() {
  // find out how Anatoly does it
  // Have to read two XML files here
  string              inputXMLFile = "HPDsP1.xml";
  string              line;
  size_t              found;
  string              str2( "<condition" );
  std::vector<string> lines;
  ifstream            xmlFile( inputXMLFile.c_str() );
  if ( xmlFile.is_open() ) {
    while ( xmlFile.good() ) // while good
    {
      while ( getline( xmlFile, line ) ) {
        found = line.find( str2 );
        if ( found != string::npos ) GetHPD( line );
        lines.push_back( line );
        // Find out what the line starts with
      }
    }
    xmlFile.close(); // close xml file
  }

  for ( int i = 0; i < lines.size(); i++ ) {
    found = lines[i].find( str2 );
    if ( found != string::npos ) {
      double x = 0;
      double y = 0;
      GetHPD( lines[i] );
      int j = i + 1;
      GetInitalValue( lines[j], x, y );
      cout << "x: " << x << "  y: " << y << endl;
    }
  }
  return 1;
}
